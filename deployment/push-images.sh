#!/usr/bin/env bash

###############################################################################
# Display help message
###############################################################################
function display_help() {
    echo "usage: $0 <TAG>"
    echo ""
    echo "Build images tagged with the specified tag."

    exit 1
}

###############################################################################
# Parse arguments
###############################################################################
if [[ -z $1 ]]; then
    display_help
    exit 0
fi

export TAG=$1
echo "Pushing images with ${TAG} tag"

###############################################################################
# Login to Azure only if not already logged in
###############################################################################
output=$(az account show --output=tsv  2> /dev/null)
if [[ $output == "" ]]; then
    az login
fi

###############################################################################
# Push images
###############################################################################

# Login into image registry
az acr login --name archlet

# Push container images to ACR
docker-compose push
