import os

try:
    from azure.cli.core import get_default_cli, CLIError
except:
    print("azure-cli package not found, please install it using the 'pip install azure-cli' command")
    exit(1)

################################################################################
# Helper function to invoke Azure CLI
################################################################################
def az_cli (args_str):
    args = args_str.split()
    cli = get_default_cli()
    cli.invoke(args, out_file = open(os.devnull, 'w'))
    if cli.result.result:
        return cli.result.result
    elif cli.result.error:
        raise cli.result.error
    return True

################################################################################
# Main function
################################################################################
# List clusters on Azure
clusters = az_cli("aks list")

# Prompt user for which cluster they wish to install
print("Select the AKS clusters for which you wish to setup a k8s context on your machine:")
for i, cluster in enumerate(clusters):
    print(f"\t{i}) {cluster['name']} (resource group: {cluster['resourceGroup']})")
user_input = input("Please enter a comma-separated list (e.g. 0, 5, 8): ")

# In case no clusters were selected, abort
if not user_input:
    print("No clusters selected, aborting")
    exit(0)

# Check if valid user input and parse list of indices
try:
    selected_clusters = [int(arg) for arg in user_input.split(",")]
except ValueError as e:
    print(f"Invalid list of indices passed: {e}, aborting")
    exit(1)

# Setup contexts
for idx in selected_clusters:
    try:
        name, resource_group = clusters[idx]['name'], clusters[idx]['resourceGroup']
        az_cli(f"aks get-credentials --resource-group {resource_group} --name {name}")
    except CLIError as e:
        continue