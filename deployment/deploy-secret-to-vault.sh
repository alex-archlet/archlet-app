#!/usr/bin/env bash

###############################################################################
# Parse arguments
###############################################################################
if [[ $1 == "" ]] || [[ $2 == "" ]]; then
    echo "Usage: $0 <secret-name> <secret-value>"
    exit 1
fi

###############################################################################
# Login to Azure only if not already logged in
###############################################################################
output=$(az account show --output=tsv  2> /dev/null)
if [[ $output == "" ]]; then
    az login
fi

###############################################################################
# Deploy to vault
###############################################################################

# Add secret to secret vault
az keyvault secret set --vault-name "archlet-app-secrets" \
    --name $1 --value $2

# Check the stored value is correct
VAL=$(az keyvault secret show --name "database-admin-password-$1" \
    --vault-name "archlet-app-secrets"\
    --query "value" \
    --output "tsv")
echo "Stored secret with the following value in Azure Key Vault (AKV): ${VAL}" 

