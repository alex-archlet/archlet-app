#!/usr/bin/env bash

###############################################################################
# Parse arguments
###############################################################################
if [[ $1 == "" ]]; then
    echo "usage: $0 <CN (e.g. localhost.archlet.io)>"
    exit 1
fi

###############################################################################
# OpenSSL parameters
###############################################################################
country="CH"
state="Zurich"
location="Zurich"
organization="Archlet GmbH"
orgunit="Engineering"
cn=$1

mkdir -p "./config/ssl-certificates/openssl/${cn}"

###############################################################################
# Generate certificate and private key
###############################################################################
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
    -keyout "./config/ssl-certificates/openssl/${cn}/privkey.pem" \
    -out "./config/ssl-certificates/openssl/${cn}/fullchain.pem" \
    -subj "/C=${country}/ST=${state}/L=${location}/O=${organization}/OU=${orgunit}/CN=${cn}"

chmod 777 "./config/ssl-certificates/openssl/${cn}/privkey.pem"
chmod 777 "./config/ssl-certificates/openssl/${cn}/fullchain.pem"