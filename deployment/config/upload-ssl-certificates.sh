#!/usr/bin/env bash

###############################################################################
# Parse arguments
###############################################################################
if [[ $1 == "" || $2 == "" || $3 == "" ]]; then
    echo "usage: $0 <cluster (e.g. archlet-sandbox)> <CN (e.g. localhost-archlet-io)> <path to fullchain file> <path to privkey file>"
    exit 1
fi

###############################################################################
# Login to Azure only if not already logged in
###############################################################################
output=$(az account show --output=tsv  2> /dev/null)
if [[ $output == "" ]]; then
    az login
fi

###############################################################################
# Deploy secrets to vault
###############################################################################
# Replace . by - in FDQN since Azure secret names can't contain . character
PARSED_FDQN=$(echo "$2" | tr . -)

# Add fullchain.pem to secret vault
az keyvault secret set --vault-name "$1-vault" \
    --name "ssl-certificate-fullchain" --value $(cat $3 | base64 -b 0)

# Add privkey.pem to secret vault
az keyvault secret set --vault-name "$1-vault" \
    --name "ssl-certificate-privkey" --value $(cat $4 | base64 -b 0)