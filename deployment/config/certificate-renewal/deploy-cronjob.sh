#!/usr/bin/env bash

###############################################################################
# Display help message
###############################################################################
function display_help() {
    echo "usage: $0 ( --sandbox | --development | --staging | --production ) [options]"
    echo ""
    echo "Deploy SSL certificate renewal CronJob on one of the following"
    echo "Kubernetes clusters running on Azure:"
    echo "    -s --sandbox      Tag container image with 'sandbox' and deploy to sandbox.archlet.io"
    echo "    -d --development  Tag container image with 'dev' and deploy to dev.archlet.io"
    echo "    -g --staging      Tag container image with 'staging' and deploy to demo.archlet.io"
    echo "    -p --production   Tag container image with 'prod' and deploy to app.archlet.io"
    echo "Options:"
    echo "    -h --help   Show this help message."
    echo "    -m --manual Run a one off instance of the CronJob immediately."
    echo "    -b --build  Build and push the container image of the certificate renewal job before deploying."

    exit 1
}

###############################################################################
# Parse arguments
###############################################################################
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    # Display help
    -h|--help)
        display_help
        exit 0
        ;;

    # List of supported modes
    -d|--development)
        export CLUSTER_NAME="archlet-dev"
        export TAG="dev"
        shift
        ;;
    -p|--production)
        export CLUSTER_NAME="archlet-prod"
        export TAG="prod"
        shift
        ;;
    -g|--staging)
        export CLUSTER_NAME="archlet-staging"
        export TAG="staging"
        shift
        ;;
    -s|--sandbox)
        export CLUSTER_NAME="archlet-sandbox"
        export TAG="sandbox"
        shift
        ;;

    # Build before deploying
    -b|--build)
        BUILD_ARG="true"
        shift
        ;;

    # Build before deploying
    -m|--manual)
        MANUAL_ARG="true"
        shift
        ;;

    # Any other value is unsupported
    *)
        display_help
        exit 1
esac
done

case $TAG in
    "dev"|"prod"|"staging"|"sandbox")
        echo "Deploying to ${TAG} cluster"
        ;;
    *)
        display_help
        exit 0
esac

###############################################################################
# Login to Azure only if not already logged in
###############################################################################
output=$(az account show --output=tsv  2> /dev/null)
if [[ $output == "" ]]; then
    az login
fi

###############################################################################
# Azure KeyVault Credentials
###############################################################################
export AZURE_CLIENT_ID=$(az keyvault secret show \
    --name "sp-keyvault-client-id" \
    --vault-name "${CLUSTER_NAME}-vault" \
    --query "value" --output tsv)
export AZURE_CLIENT_SECRET=$(az keyvault secret show \
    --name "sp-keyvault-password" \
    --vault-name "${CLUSTER_NAME}-vault" \
    --query "value" --output tsv)
export AZURE_TENANT_ID=$(az keyvault secret show \
    --name "sp-keyvault-tenant-id" \
    --vault-name "${CLUSTER_NAME}-vault" \
    --query "value" --output tsv)
export KEY_VAULT_NAME="${CLUSTER_NAME}-vault"

export SLACK_API_TOKEN=$(az keyvault secret show \
    --name "azure-cloud-app-slack-token" \
    --vault-name "${CLUSTER_NAME}-vault" \
    --query "value" --output tsv)
export SLACK_CHANNEL="azure-cloud"

###############################################################################
# Build and push image
###############################################################################
if [[ -n "$BUILD_ARG" ]]; then
    # Login into image registry
    az acr login --name archlet

    # Build and push container image
    docker image build -t archlet.azurecr.io/ssl-certificate-renewer:${TAG} .
    docker image push     archlet.azurecr.io/ssl-certificate-renewer:${TAG}
fi

###############################################################################
# Manually renew certificates
###############################################################################
if [[ -n "$MANUAL_ARG" ]]; then
    kubectl create job --from=cronjob/ssl-certificate-renewer ssl-certificate-renewer-manual
    exit 0
fi

###############################################################################
# Deploy cron job to cluster
###############################################################################
kubectl config use-context "${CLUSTER_NAME}"
envsubst < "./cronjob.yaml" | kubectl apply -f -