# Kubernetes Python client API
# Information: https://github.com/kubernetes-client/python
# Example taken from: https://github.com/kubernetes-client/python/blob/master/examples/in_cluster_config.py

# Azure Python API
# Code taken from: https://docs.microsoft.com/en-us/azure/python/python-sdk-azure-authenticate#mgmt-auth-msi
# With mode explanation here: 
# https://docs.microsoft.com/en-ca/azure/active-directory/managed-identities-azure-resources/overview
# https://docs.microsoft.com/en-us/azure/key-vault/quick-create-python?toc=/azure/python/toc.json&bc=/azure/python/breadcrumb/toc.json

###############################################################################
# Import dependencies
###############################################################################
import os
import cmd
import base64

import slack

import OpenSSL.SSL as ssl
import OpenSSL.crypto as crypto

from datetime import datetime

from kubernetes import client, config
from kubernetes.stream import stream

from azure.keyvault.secrets import SecretClient
from azure.identity import DefaultAzureCredential


###############################################################################
# Custom types
###############################################################################
class FatalError(BaseException):
   """
   Raised when something went wrong and the current task had to be aborted as a
   result.
   """
   pass


###############################################################################
# Main functions
###############################################################################
def connect_to_k8s_api():
    """
    This function runs the following tasks:
        1. Sets up Kubernetes API
        2. Connects to a frontend pod
    If these run successfully, it returns the  frontend pod name and some helper
    functions to interact with the K8s cluster.
    """
    
    ###############################################################################
    # Setup Kubernetes API config
    ###############################################################################
    config.load_incluster_config()
    v1 = client.CoreV1Api()
    
    ###############################################################################
    # Create helper functions
    ###############################################################################
    def exec_command(pod, command):
        """ Small function to execute in a pod by name """
        return stream(v1.connect_get_namespaced_pod_exec,
            pod,
            'default',
            command=command,
            stderr=True, stdin=False,
            stdout=True, tty=False
        )

    def patch_secret(name, value):
        """ Small function to patch a secret value """
        v1.patch_namespaced_secret(
            name='sslcertificatevolume', 
            namespace='default',
            body={"data":{name: value}}
        )

    ###############################################################################
    # Find frontend pod
    ###############################################################################
    ret = v1.list_pod_for_all_namespaces(label_selector="app=frontend", watch=False)
    print(f"[INFO] Found {len(ret.items)} frontend pod(s):")
    for i, pod in enumerate(ret.items):
        print(f"[INFO]\t{i:2d} {pod.status.pod_ip:15s} {pod.metadata.namespace} {pod.metadata.name}")

    if ret.items == 0:
        raise FatalError("No frontend pod found in cluster")

    frontend = ret.items[0].metadata.name
    print(f"[INFO] Selected first result ({frontend})")

    return exec_command, patch_secret, frontend


def renew_ssl_certificates(exec_command, frontend):
    """
    This function runs the following tasks:
        1. Runs the certbot utility on this frontend pod
        2. Fetches the newly created certificates on the frontend pod
        3. Checks validity of the newly created SSL certificates
        4. Stores the value of the new certificates in the Azure KeyVault
    If any of these tasks fail, the function returns false, otherwise it
    returns true.
    """

    ###############################################################################
    # Run certbot command
    ###############################################################################
    def parse_time(datestring):
        """ Small function to parse the output of the 'date' command """
        date = datetime.strptime(datestring.replace('\n',''), "%a, %d %b %Y %H:%M:%S %Z")
        return date

    # Record start time
    command        = ["/bin/sh", "-c", "date -R -u"]
    job_start_time = parse_time(exec_command(frontend, command))

    ###############################################################################
    # Run certbot command
    ###############################################################################
    print(f"[INFO] Running certbot utility")
    command = ["/bin/sh", "-c", """certbot certonly \
        --webroot \
        --webroot-path /var/www/certbot \
        --email contact@archlet.ch \
        -d ${PUBLIC_DOMAIN_CN} \
        --rsa-key-size 4096 \
        --agree-tos \
        --force-renewal \
        --non-interactive"""
    ]
    resp = exec_command(frontend, command)
    print(resp)

    command  = ["/bin/sh", "-c", "echo $?"]
    exitcode = exec_command(frontend, command)

    if exitcode != "0":
        raise FatalError("An error occured while running certbot")

    print(f"[INFO] Successfully ran certbot utility")

    ###############################################################################
    # Check if new files were created
    ###############################################################################
    def file_exists(path):
        """ Execute command in remote pod to check if file exists """
        command = ["/bin/sh", "-c", f"if test -f {path}; then echo 1; else echo 0; fi"]
        exists  = exec_command(frontend, command)
        return exists == "1"

    def file_last_modified(path):
        """ Execute command in remote pod to check file last modified date """
        command  = ["/bin/sh", "-c", f"date -R -u -r {path}"]
        return parse_time(exec_command(frontend, command))

    def more_recent_than_start(path):
        """ Check if file was modified after start time of this script """
        return file_last_modified(path) > job_start_time

    # Define fullchain and privkey file paths
    fullchain_path  = "/etc/letsencrypt/live/${PUBLIC_DOMAIN_CN}/fullchain.pem"
    privatekey_path = "/etc/letsencrypt/live/${PUBLIC_DOMAIN_CN}/privkey.pem"

    # Check both files exist
    if not (file_exists(fullchain_path) and file_exists(privatekey_path)):
        print("[WARNING] Unable to find 'fullchain.pem' and 'privkey.pem' files, perhaps you hit the Let's Encrypt rate limit")
        return False

    # Check both files were modified after job start time
    if not (more_recent_than_start(fullchain_path) and more_recent_than_start(privatekey_path)):
        print("[WARNING] The 'fullchain.pem' and 'privkey.pem' files were not modified, perhaps you hit the Let's Encrypt rate limit")
        return False

    ###############################################################################
    # Read fullchain.pem and privkey.pem files from frontend pod
    ###############################################################################

    def read_file_contents(path):
        """ Execute command in remote pod to read contents of file """
        command = ["/bin/sh", "-c", f"cat {path}"]
        content = exec_command(frontend, command)
        return content

    fullchain_content  = read_file_contents(fullchain_path)
    privatekey_content = read_file_contents(privatekey_path)

    print(f"[INFO] Successfully fetched private key and certificate from frontend")

    ###############################################################################
    # Check validity of certificate and private key
    ###############################################################################

    # After reading the files, parse their information
    try:
        certificate = crypto.load_certificate(ssl.FILETYPE_PEM, fullchain_content)
        privatekey  = crypto.load_privatekey(ssl.FILETYPE_PEM,  privatekey_content)
    except crypto.Error:
        raise FatalError("Unable to parse SSL certificate and private key")

    # Make sure the private key matches the certificate
    context = ssl.Context(ssl.TLSv1_METHOD)
    context.use_privatekey(privatekey)
    context.use_certificate(certificate)

    try:
        context.check_privatekey()
    except ssl.Error:
        raise FatalError("Private key does not match the certificate")

    # Check if certificate is expired
    if crypto.X509.has_expired(certificate):
        raise FatalError("Certificate is expired")

    print(f"[INFO] New certificates successfully checked")

    ###############################################################################
    # Check if staging certificates
    ###############################################################################
    issuer = crypto.X509.get_issuer(certificate).CN

    # This is what we expect
    if   issuer == "Let's Encrypt Authority X3":
        pass
    # These certificates are issued with the staging environment, don't upload them
    elif issuer == "Fake LE Intermediate X1":
        print(f"[WARNING] Certificates were issued with '--staging' flag, not saving in Azure KeyVault")
        return False
    # Unknown issuing authority, abort
    else:
        raise FatalError(f"Certificates were issued by unknown authority '{issuer}', not saving in Azure KeyVault")

    ###############################################################################
    # Store in Azure keyvault
    ###############################################################################

    # Convert to base64
    fullchain_base64  = base64.b64encode(fullchain_content.encode('utf-8')).decode('ascii')
    privatekey_base64 = base64.b64encode(privatekey_content.encode('utf-8')).decode('ascii')

    # Get access to Azure KeyVault
    keyVaultName = os.environ["KEY_VAULT_NAME"]
    KVUri = f"https://{keyVaultName}.vault.azure.net"

    credential = DefaultAzureCredential()
    client = SecretClient(vault_url=KVUri, credential=credential)

    print(f"[INFO] Updating certificate values in Azure KeyVault")

    client.set_secret(f"ssl-certificate-fullchain", fullchain_base64)
    client.set_secret(f"ssl-certificate-privkey",   privatekey_base64)

    print(f"[INFO] Successfully updated new certificate value in Azure KeyVault")

    return True


def fetch_ssl_certificates_from_keyvault():
    """
    This function fetches the certificate values from the Azure KeyVault and
    returns them
    """

    ###############################################################################
    # Get access to Azure KeyVault
    ###############################################################################
    keyVaultName = os.environ["KEY_VAULT_NAME"]
    KVUri = f"https://{keyVaultName}.vault.azure.net"

    try:
        credential = DefaultAzureCredential()
        client = SecretClient(vault_url=KVUri, credential=credential)
    except Exception as e:
        print(str(e))
        raise FatalError("Something went wrong while logging in to Azure KeyVault")

    print(f"[INFO] Successfully logged in to Azure KeyVault")

    ###############################################################################
    # Fetch secrets from Azure KeyVault
    ###############################################################################
    try:
        fullchain_base64  = client.get_secret(f"ssl-certificate-fullchain").value
        privatekey_base64 = client.get_secret(f"ssl-certificate-privkey").value
    except Exception as e:
        print(str(e))
        raise FatalError("Something went wrong while fetching secrets from Azure KeyVault")

    print(f"[INFO] Successfully fetched certificate values from Azure KeyVault")

    return fullchain_base64, privatekey_base64


def patch_k8s_ssl_secret_values(patch_secret, fullchain_base64, privatekey_base64):
    """
    This function updates the values of the SSL certificate and private key
    stored in the Kubernetes secret 'sslcertificatevolume' in the current
    cluster
    """
    print(f"[INFO] Patching k8s secret values")

    try:
        patch_secret('fullchain.pem', fullchain_base64)
        patch_secret('privkey.pem', privatekey_base64)
    except Exception as e:
        print(str(e))
        raise FatalError("Something went wrong while patching k8s secrets")

    print(f"[INFO] Successfully patched k8s secret values")

    return True


def display_certificate_information(fullchain_base64, privatekey_base64):
    """
    This function takes in the SSL certificate and private key in Base64 and
    prints a quick overview of their expiry date, issuing authority, etc. It
    also sends out a Slack message with the certificate expiry date.
    """

    ###############################################################################
    # Helper functions to parse dates
    ###############################################################################
    def parse_date(datestring):
        """ Parse OpenSSL date format """
        date = datetime.strptime(datestring.decode("utf-8"), "%Y%m%d%H%M%SZ")
        return date.strftime("%a, %d %b %Y %H:%M:%S")

    def time_left(end):
        """ Calculate how much longer the certificate is valid for """
        start = datetime.now()
        end   = datetime.strptime(end.decode("utf-8"), "%Y%m%d%H%M%SZ")
        return str(end-start)

    ###############################################################################
    # Parse certificate and private key
    ###############################################################################

    # Decode from base64
    fullchain_content  = base64.b64decode(fullchain_base64).decode('ascii')
    privatekey_content = base64.b64decode(privatekey_base64).decode('ascii')

    # Parse certificate and private key
    try:
        certificate = crypto.load_certificate(ssl.FILETYPE_PEM, fullchain_content)
        privatekey  = crypto.load_privatekey(ssl.FILETYPE_PEM,  privatekey_content)
    except crypto.Error:
        raise FatalError("Unable to parse SSL certificate and private key")

    # Check validity of certificate
    issuer      = crypto.X509.get_issuer(certificate)
    subject     = crypto.X509.get_subject(certificate)
    not_before  = crypto.X509.get_notBefore(certificate)
    not_after   = crypto.X509.get_notAfter(certificate)
    has_expired = crypto.X509.has_expired(certificate)

    ###############################################################################
    # Print info
    ###############################################################################
    print(f"[INFO] Currently using the following certificate for \"{subject.CN}\"")
    print(f"       * ISSUED BY:        {issuer.CN}")
    print(f"       * HAS EXPIRED:      {has_expired}")
    print(f"       * NOT VALID BEFORE: {parse_date(not_before)}")
    print(f"       * NOT VALID AFTER:  {parse_date(not_after)}")
    print(f"       * TIME LEFT:        {time_left(not_after)}")

    return True


def reload_certificates(exec_command, frontend):
    """ 
    This function tells the Nginx server in the frontend pod to reload its
    SSL certificates.
    """
    print(f"[INFO] Reloading certificate in frontend")

    # Issue Nginx reload command
    resp = exec_command(frontend, ["/bin/sh", "-c", "nginx -s reload"])
    print(resp)

    # Check exit code of reload command
    command  = ["/bin/sh", "-c", "echo $?"]
    exitcode = exec_command(frontend, command)

    if exitcode != "0":
        raise FatalError("An error occured while reloading certificate")

    print(f"[INFO] Nginx successfully reloaded updated certificate")

    return True


def slack_notify(text):
    """
    Send a text message to the Slack channel specified by env variables.
    Assumes a valid Slack API token stored in the corresponding env variable.
    """
    client   = slack.WebClient(token=os.environ["SLACK_API_TOKEN"])
    
    channel  = f"#{os.environ['SLACK_CHANNEL']}"
    response = client.chat_postMessage(channel=channel, text=text)

    if not response["ok"]:
        print(f"[ERROR] Something went wrong while sending Slack message")


###############################################################################
# Main
###############################################################################
def notify_renewal_failure(message):
    print(f"[ERROR] {message}, resorting to using old certificates")
    slack_notify(
        f"[ERROR] Something went wrong while renwing SSL certificate for "
        f"{os.environ['CLUSTER_NAME']} cluster: {message}, please check "
        f"logs for additional information. Resorting to using old "
        f"certificates instead.")

def notify_abort(message):
    print(f"[ERROR] {message}, aborting job")
    slack_notify(
        f"[ERROR] Something went wrong while renwing SSL certificate for "
        f"{os.environ['CLUSTER_NAME']} cluster: {message}, please check "
        f"logs for additional information. Had to abort job.")

if __name__ == "__main__":

    ###############################################################################
    # Connect to Kubernetes API and find frontend pod
    ###############################################################################
    try:
        exec_command, patch_secret, frontend = connect_to_k8s_api()
    except FatalError as e:
        notify_abort(str(e))
        exit(1)
    except Exception as e:
        print(str(e))
        notify_abort("an unknown exception occurred")
        exit(1)

    ###############################################################################
    # Try to renew certificates
    ###############################################################################
    try:
        if not renew_ssl_certificates(exec_command, frontend):
            print(f"[WARNING] Unable to renew SSL certificates, resorting to using old ones")
    except FatalError as e:
        notify_renewal_failure(str(e))
    except Exception as e:
        print(str(e))
        notify_renewal_failure("an unknown exception occurred")

    ###############################################################################
    # Reload latest certificates from secret vault
    ###############################################################################
    try:
        # Regardless of whether certificates were renewed, fetch certificate 
        # values from Azure KeyVault and update value stored in secret resource
        fullchain_base64, privatekey_base64 = fetch_ssl_certificates_from_keyvault()
        patch_k8s_ssl_secret_values(patch_secret, fullchain_base64, privatekey_base64)

        # Print some information about the certificates we are using
        display_certificate_information(fullchain_base64, privatekey_base64)

        # Finally, tell Nginx to reload the certificates
        # TODO: if multiple frontends, reload certificate on all of them
        reload_certificates(exec_command, frontend)
    except FatalError as e:
        notify_abort(str(e))
        exit(1)
    except Exception as e:
        print(str(e))
        notify_abort("an unknown exception occurred")
        exit(1)
