#!/usr/bin/env bash

###############################################################################
# Parse arguments
###############################################################################
if [[ $1 == "" || $2 == "" ]]; then
    echo "usage: $0 <cluster (e.g. archlet-sandbox)> <CN (e.g. localhost-archlet-io)> <path to nginx whitelist file>"
    exit 1
fi

###############################################################################
# Login to Azure only if not already logged in
###############################################################################
output=$(az account show --output=tsv  2> /dev/null)
if [[ $output == "" ]]; then
    az login
fi

###############################################################################
# Deploy secrets to vault
###############################################################################
# Replace . by - in FDQN since Azure secret names can't contain . character
PARSED_FDQN=$(echo "$2" | tr . -)

# Add whitelist.conf to secret vault
az keyvault secret set --vault-name "$1-vault" \
    --name "nginx-whitelist-conf" --value $(cat $2 | base64 -b 0)