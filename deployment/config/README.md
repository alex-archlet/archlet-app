# Production and Development Environments Access

## Azure Environment Domain Names and IP addresses

The Azure environments in the table below are all accessible via `https` (with 
`http` redirection).

| Environment | FQDN                                             | IPv4 Address  |
| ----------- | ------------------------------------------------ | ------------- |
| Sandbox     | [sandbox.archlet.io](https://sandbox.archlet.io) | 40.68.196.98  |
| Development | [dev.archlet.io](https://dev.archlet.io)         | 51.136.36.227 |
| Production  | [app.archlet.io](https://app.archlet.io)         | INACTIVE      |

## Local Environments Access

Note, you may have to accept an "unsafe" connection since the local deployment
modes use a self-signed SSL certificate which is considered invalid by most browsers.

### Minikube

The Minikube environment is accessible via the IP address output by the following
command

    minikube service list

Simply enter the URL that appears next to the desired service.

### Docker Compose

The Docker Compose environment is available at 
[localhost.archlet.io](http://localhost.archlet.io). 