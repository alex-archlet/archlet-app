#!/usr/bin/env bash

###############################################################################
# Display help message
###############################################################################
function display_help() {
    echo "usage: $0 <cluster name>"
    echo ""
    echo "Deploy a new cluster on Azure with cluster name <cluster name>, e.g."
    echo "\"archlet-dev\" or \"archlet-prod\""
    exit 1
}

###############################################################################
# Parse arguments
###############################################################################
if [[ -n $1 ]]; then
    CLUSTER_NAME=$1
else
    display_help
    exit 0
fi

###############################################################################
# Ask confirmation from user
###############################################################################
read -p "Are you sure you want to create a new Kubernetes cluster named \"$CLUSTER_NAME\" (y/n)? " choice
case "$choice" in 
  y|Y ) echo "yes";;
  n|N ) exit 0;;
  * ) exit 1;;
esac

###############################################################################
# Login to Azure only if not already logged in
###############################################################################
output=$(az account show --output=tsv  2> /dev/null)
if [[ $output == "" ]]; then
    az login
fi

###############################################################################
# Create new resource group
###############################################################################
RESOURCE_GROUP="${CLUSTER_NAME}"
az group create --name ${RESOURCE_GROUP} --location westeurope

###############################################################################
# Create new cluster
###############################################################################
AKS_NAME="${CLUSTER_NAME}"
ACR_NAME="archlet"

# NOTE: To see why we need to enable managed identities, read here:
#       https://docs.microsoft.com/en-us/azure/aks/use-managed-identity
az aks create \
    --resource-group ${RESOURCE_GROUP} \
    --name $AKS_NAME \
    --node-count 1 \
    --enable-addons monitoring \
    --generate-ssh-keys \
    --attach-acr ${ACR_NAME}

###############################################################################
# Configure cluster credentials
###############################################################################
# Install dependencies to be able to run locally
az aks install-cli

# Create kubctl context i.e. if this context is selected you control the newly created cluster on Azure
az aks get-credentials --resource-group ${RESOURCE_GROUP} --name ${AKS_NAME}
kubectl config use-context ${AKS_NAME}

###############################################################################
# Create KeyVault and storage account
###############################################################################
ASA_NAME=$(echo "${CLUSTER_NAME}-fs" | tr -cd '[:alnum:]' | tr '[:upper:]' '[:lower:]')
AKV_NAME="${CLUSTER_NAME}-vault"

az keyvault create        --resource-group ${RESOURCE_GROUP} --name ${AKV_NAME} --location westeurope
az storage account create --resource-group ${RESOURCE_GROUP} --name ${ASA_NAME} --location westeurope --sku Premium_LRS --kind FileStorage

STORAGEKEY=$(az storage account keys list \
    --resource-group ${RESOURCE_GROUP} \
    --account-name ${ASA_NAME} \
    --query "[0].value" \
    --output tsv)
export AZURE_STORAGE_ACCOUNT_KEY=$STORAGEKEY

az storage share create \
    --account-name "${ASA_NAME}" \
    --account-key "${AZURE_STORAGE_ACCOUNT_KEY}" \
    --name "files"

###############################################################################
# Create static IP
###############################################################################
NODE_RESOURCE_GROUP=$(az aks show --resource-group "${RESOURCE_GROUP}" --name "${AKS_NAME}" --query nodeResourceGroup -o tsv)
az network public-ip create \
    --resource-group ${NODE_RESOURCE_GROUP} \
    --name frontend-public-ip \
    --allocation-method static \
    --sku Standard

###############################################################################
# Create Service Principal to connect to ACR
###############################################################################
SERVICE_PRINCIPAL_NAME="${CLUSTER_NAME}-sp-acr"

# Populate the ACR login server and resource id
ACR_LOGIN_SERVER=$(az acr show --name $ACR_NAME --query loginServer --output tsv)
ACR_REGISTRY_ID=$(az acr show --name $ACR_NAME --query id --output tsv)

# Create acrpull role assignment with a scope of the ACR resource and get the service principal client ID
SP_PASSWD=$(az ad sp create-for-rbac --name http://$SERVICE_PRINCIPAL_NAME --role acrpull --scopes $ACR_REGISTRY_ID --query password --output tsv)
CLIENT_ID=$(az ad sp show --id http://$SERVICE_PRINCIPAL_NAME --query appId --output tsv)

# Store these values in Azure KeyVault
az keyvault secret set --vault-name "${AKV_NAME}" --name "sp-acr-client-id" --value "${CLIENT_ID}"
az keyvault secret set --vault-name "${AKV_NAME}" --name "sp-acr-password"  --value "${SP_PASSWD}"

###############################################################################
# Create Service Principal to connect to Azure KeyVault
###############################################################################
SERVICE_PRINCIPAL_NAME="${CLUSTER_NAME}-sp-keyvault"

# Create acrpull role assignment with a scope of the ACR resource and get service principal client and tenants IDs
OUTPUT=$(az ad sp create-for-rbac --name "http://$SERVICE_PRINCIPAL_NAME" --skip-assignment)

# Extract from JSON output
CLIENT_ID=$(echo $OUTPUT | jp appId)
SP_PASSWD=$(echo $OUTPUT | jp password)
TENANT_ID=$(echo $OUTPUT | jp tenant)

# Remove surounding quotes
CLIENT_ID=$(echo "${CLIENT_ID//\"}")
SP_PASSWD=$(echo "${SP_PASSWD//\"}")
TENANT_ID=$(echo "${TENANT_ID//\"}")

# Store these values in Azure KeyVault
az keyvault secret set --vault-name "${AKV_NAME}" --name "sp-keyvault-password"  --value "${SP_PASSWD}"
az keyvault secret set --vault-name "${AKV_NAME}" --name "sp-keyvault-client-id" --value "${CLIENT_ID}"
az keyvault secret set --vault-name "${AKV_NAME}" --name "sp-keyvault-tenant-id" --value "${TENANT_ID}"

# Set permissions
az keyvault set-policy --name "${AKV_NAME}" --spn "${CLIENT_ID}" --secret-permissions get set list delete backup recover restore purge

###############################################################################
# Create database
###############################################################################
DB_NAME=${CLUSTER_NAME}-db
DB_PASS=$(openssl rand -base64 20)

az postgres server create \
    --resource-group ${RESOURCE_GROUP} \
    --name ${DB_NAME} \
    --location westeurope \
    --admin-user archlet \
    --admin-password ${DB_PASS} \
    --sku-name GP_Gen5_2 \
    --version 11 \
    --ssl-enforcement Enabled

###############################################################################
# Create firewall rules
###############################################################################
az postgres server firewall-rule create \
    --resource-group ${RESOURCE_GROUP} \
    --server ${DB_NAME} \
    --name ArchletOfficeHohlstrasse \
    --start-ip-address 85.6.240.190 \
    --end-ip-address 85.6.240.190

###############################################################################
# Deploy password to AKV
###############################################################################
az keyvault secret set --vault-name ${AKV_NAME} \
    --name "database-admin-password" \
    --value ${DB_PASS}
