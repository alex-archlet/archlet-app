#!/usr/bin/env bash

###############################################################################
# Display help message
###############################################################################
function display_help() {
    echo "usage: $0 <TAG> [options]"
    echo ""
    echo "Build images tagged with the specified tag."
    echo ""
    echo "Options:"
    echo "    -u --push     Push the latest version of the images for the selected mode to Azure"
    echo "    -m --minikube Build the images within the Minikube docker env"
    echo "    -q --server   By default we use the regular Dockerfiles, but if this flag is selected"
    echo "                  we instead the \"Dockerfile-server\" files which are meant to use with"
    echo "                  compose mode "

    exit 1
}

###############################################################################
# Parse arguments
###############################################################################
if [[ -z $1 ]]; then
    display_help
    exit 0
fi

export TAG=$1
shift

while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    # Build before deploying
    -p|--push)
        PUSH_ARG="true"
        shift
        ;;
    # Build before deploying
    -m|--minikube)
        MINIKUBE_ARG="true"
        shift
        ;;
    # User Dockerfile-server file instead
    -q|--server)
        SERVER_DOCKERFILE_ARG="true"
        shift
        ;;

    # Any other value is unsupported
    *)
        echo $key
        display_help
        exit 0
esac
done

echo "Building images with ${TAG} tag"

###############################################################################
# Build in Minikube Docker environment context
###############################################################################
if [[ -n "$MINIKUBE_ARG" ]]; then
    # Minikube docker env (local cluster)
    # This makes the docker images built here accessible within Minikube
    eval $(minikube docker-env)
fi

###############################################################################
# Select correct Dockerfile name
###############################################################################
if [[ -n "$SERVER_DOCKERFILE_ARG" ]]; then
    export DOCKERFILE="Dockerfile-server"
fi

###############################################################################
# Build container images
###############################################################################

# Build container images
docker-compose build --parallel

###############################################################################
# Optionally push images before deploying
###############################################################################
if [[ -n "$PUSH_ARG" ]]; then
    ./push-images.sh ${TAG}
fi

###############################################################################
# Go back to host docker env
###############################################################################
if [[ -n "$MINIKUBE_ARG" ]]; then
    eval $(minikube docker-env -u)
fi
