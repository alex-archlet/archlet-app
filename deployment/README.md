# Archlet App Docker, Kubernetes and Azure Scripts Guide

## Setting up your environment

### Cloning the repository on your machine

    git clone https://gitlab.com/merixstudio/archlet/archlet.git

### Installing dependencies

First install Docker following instructions on the Docker website for [macOS](https://docs.docker.com/docker-for-mac/install/)
or [Linux](https://docs.docker.com/install/linux/docker-ce/ubuntu/).

Next, install the Kubernetes command line tool `kubectl` following the 
[instructions on the Kubernetes website](https://kubernetes.io/docs/tasks/tools/install-kubectl/).

Finally, follow the instructions [here](https://direnv.net/) to install `direnv`.

### Seting up Azure CLI

First, install the Azure CLI following the instructions [here](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest),
then install the Azure Kubernetes command line tool

    az login
    az aks install-cli

## Building, Pushing and Deploying Containers

This section describes the correct script usage to be able to build container 
images from source code, push them to the Archlet container registry and finally
deploy the application on your machine or on Azure.

### Building images

#### Description

Build the container images from the source code on your machine. 

Images will be tagged with the mode they are built with. `local` mode tags the
images with `loc`, `development` mode tags the images with `dev` and `production`
mode tags the images with `prod`. For example, the frontend image in development
mode will be tagged as `archlet.azurecr.io/archlet-app-frontend:dev`.

#### Synposis

    build-images (--local|--development|--production) [<options>]

#### Options

`--push`

> If selected, once the images are built they will also be pushed to the Archlet
> Azure registry `archlet.azurecr.io`.

### Pushing container images to the registry

#### Description

Push the container images present on your machine to the Archlet Azure Container
Registry (ACR) `archlet.azurecr.io`. Only the images with the matching mode
(i.e. `local`, `development` or `production`) are pushed to the registry.

#### Synposis

    push-images (--local|--development|--production)

### Deploying the Application

#### Description

Deploy the application locally in a Docker Swarm or on Azure to either the deployment
or production Kubernetes clusters (`dev.archlet.io` or `app.archlet.io` respectively).

Depending on the mode selected the deployment action is different. If the `local`
mode is used, the application will be started in a Docker Swarm on your machine
and available on `localhost`.
If the `development` or `production` modes are used the application is deployed
on the AKS clusters available at `dev.archlet.io` and `app.archlet.io` respectively.

#### Synopsis

    deploy-app (--local|--development|--production) [<options>]

#### Options

`--build`
> If selected, build the container images before deploying the application.

`--push`
> If selected, push the container images to the Archlet ACR before deploying
> the application.

`--restart`
> If the AKS configuration was not changed and you simply wish to restart the
> containers with the latest versions of the images pushed to the Archlet ACR,
> select this flag.

`--ssl-certs-path=<path>`
> Specify the path to the directory containing the SSL certificate `.pem` files.
> If this argument is not provided and no value for `SSL_CERTS_PATH` is
> specified in the `.env` file, the default value of
> `./azure-scripts/config/ssl-certificates/openssl/localhost.archlet.io` is
> used.

`--whitelist-path=<path>`
> Specify the path to the directory containing the IP address whitelist.
> If this argument is not provided and no value for `WHITELIST_PATH` is
> specified in the `.env` file, the default value of 
> `./azure-scripts/config/access-control/loc` is used.

`--filestorage-path=<path>`
> Specify the path to the directory used for shared file storage by the engine
> and backend.
> If this argument is not provided and no value for `FILESTORAGE_PATH` is
> specified in the `.env` file, the default value of 
> `~/Documents/archlet-app-filestorage"` is used.

`--postgresql-path=<path>`
> Specify the path to the directory where the PostgreSQL database files will 
> be stored.
> If this argument is not provided and no value for `POSTGRESQL_PATH` is
> specified in the `.env` file, the default value of 
> `~/Documents/archlet-app-postgresql` is used.

#### Providing a `.env` file

In order to streamline the calling of the `deploy-app` script, you can optionally
create a `.env` file located in the `azure-scripts` directory. An example file
is provided as `.env.example` file. The contents of the file should be as follows:

    SSL_CERTS_PATH="./azure-scripts/config/ssl-certificates/openssl/localhost.archlet.io"
    WHITELIST_PATH="./azure-scripts/config/access-control/loc"
    FILESTORAGE_PATH="${HOME}/Documents/archlet-app-filestorage"
    POSTGRESQL_PATH="${HOME}/Documents/archlet-app-postgresql"

You may provide only a subset of the variable values in your `.env` file.

Note the following rules to follow for this file to be properly interpreted:
* Make sure not to inlcude spaces between variable names and values, i.e. 
`VAR="value"` and not `VAR = "value"`.
* `$(pwd)` evaluates to `~` which corresponds to your home directory, e.g. 
`/Users/lpahlavi` on macOS or `/home/lpahlavi` on Linux.

### Accessing the application in `local` mode

The application runs inside a Linux virtual machine managed by `minikube`. To
access the application you just deployed you need to ask `minikube` for a URI
of the services you wish to access (web application, database, etc.).

Before being able to access the application in `local` mode you need to deploy
it on your machine using the following command

    ./deploy-app --local

#### Accessing the web application

To access the user interface of the web application (i.e. the frontend) type in
the following in the command line

    minikube service --https frontend

This will open the application in your browser.

#### Accessing the database

To obtain the URI of the database running inside `minikube` type the following
command in the command line

    minikube service --url database

You will get in return a URI in the form `http://<ip address>:<port>` such as

    http://192.168.99.103:32641

Where the IP address is `192.168.99.103` and the port `32641`. This replaces
the usual `localhost:5432` of a PostgreSQL database running on your host
machine. To access the database simply replace `localhost` by `<ip address>`
and 5432 by `<port>` in your GUI of choice (i.e. PGAdmin, DBeaver, etc.).

#### Accessing the queue manager

To access the the management user interface of the RabbitMQ queue manager in
your browser type the following command in your command line

    minikube service queue

#### Accessing the Kubernetes dashboard

To open the kubernetes dashboard in your browser type the following command in
your command line

    minikube dashboard

### Accessing the application in `development` or `production` mode

In development and production modes, the application runs on Azure.

## Deploying Azure Resources

### Azure Kubernetes Services (AKS) Cluster

Once the application has been deployed to Azure, it is accessible through the
following URLs
* https://app.archlet.io in production mode
* https://kube.archlet.io in development mode

#### Description

The AKS cluster is the ecosystem the application lives in on Azure. Each cluster
corresponds to one instance of the Archlet App, e.g. production or dev. The clusters
host the running container instances and make sure they are running properly.

References:
* For more information about Kubernetes clusters, see the book [Kubernetes in Action]()
* For more information about Azure Kubernetes Services (AKS), see [Microsoft documentation]()

#### Deployment Instructions

First, create the AKS cluster and specify the name (e.g. `archlet-dev`)

    deploy-aks-cluster <cluster-name>

Next, configure the Kubernetes command line tool `kubectl` to communicate with
this cluster

    connect-aks-cluster <cluster-name>

Finally, assign a static IP address to the cluster

    assign-aks-static-ip <cluster-name>

### Azure Container Registry (ACR)

#### Description

The Azure Container Registry (ACR) is a registry that contains private container
images hosted on Azure, namely the frontend, backend and engine. Another example
of a container registry is the public [Docker Hub](https://hub.docker.com/) where
images like RabbitMQ, Nginx, and Node.js are hosted. 

#### Deployment Instructions

Deploy a new [Azure Container Registry (ACR)](https://azure.microsoft.com/en-us/services/container-registry/)
(default ACR `archlet`)

    deploy-acr <registry-name>

### Azure PostgreSQL Database

#### Description

The PostgreSQL database is a SQL database hosted on Azure, accessible via a static
URI and credentials. It can be accessed on yur local machine using this URI and
valid credentials.

#### Deployment Instructions

Deploy a new [Azure PostgreSQL Database](https://docs.microsoft.com/en-us/azure/postgresql/)
(default databases `archlet-dev` and `archlet-prod`)

    deploy-postgres-database <database-name> <admin-password>

### Azure File Storage (AFS)

#### Description

The Azure File Storage (AFS) hosts all the files (`.xlsx`, `.csv`, etc.) used by
the engine and backend. It is accessible to any machine with its URI and valid
credentials.

#### Deployment Instructions

Deploy a new [Azure File Storage (AFS) Account](https://docs.microsoft.com/en-us/azure/storage/common/storage-account-overview)
(default AFSs `archlet-dev` and `archlet-prod`)

    deploy-storage-account <storage-account-name>

### Azure Key Vault (AKV)

#### Description

The Azure Key Vault (AKV) contains secret values that are too sensitive to be
stored as clear text on a personal machine. This inlcudes sensitive data such
as SSL certificates and admin credentials to the database.

#### Deployment Instructions

Deploy a new [Azure Key Vault (AKV)](https://docs.microsoft.com/en-us/azure/key-vault/)
(default AKV `archlet`)

    deploy-key-vault.sh <vault-name>

## Managing SSL Certificates

### Generating dummy SSL certificates using `openssl`

To run the web application on your machine you need to first create self-signed
certificates using [OpenSSL](https://www.openssl.org/). To do so simply run

    generate-local-certificates <CN (e.g. localhost.archlet.io)>

This will create self-signed SSL certificates and store them in 
`./config/ssl-certificates/openssl/<cn>`.

The first time you upload the application to AKS, you will need to use this
certificate, you will then be able to use `certbot` on the container instances
running on Azure to create valid certificates (see next section).

### Generating valid SSL certificates using `certbot`

Run the [Let's Encrypt](https://letsencrypt.org/) tool on the container instances
runnning on Azure and update the certificates stored in the respective AKV secrets
(default AKV `archlet`)

    renew-ssl-certificates (--development|--production)

Restart the cluster with the newly created SSL certificates

    deploy-app (--development|--production) --restart