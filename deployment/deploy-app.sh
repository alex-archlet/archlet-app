#!/usr/bin/env bash

###############################################################################
# Display help message
###############################################################################
function display_help() {
    echo "usage: $0 ( --sandbox | --development | --staging | --production | --compose | --minikube ) [options]"
    echo ""
    echo "Modes:"
    echo "    The application can be built and deployed in different modes described"
    echo "    below, they are classed in two main categories: cloud and local."
    echo "    Cloud:"
    echo "        -s --sandbox      Tag images with 'sandbox' and deploy to sandbox.archlet.io"
    echo "        -d --development  Tag images with 'dev' and deploy to dev.archlet.io"
    echo "        -g --staging      Tag images with 'staging' and deploy to demo.archlet.io"
    echo "        -p --production   Tag images with 'prod' and deploy to app.archlet.io"
    echo "    Local:"
    echo "        -c --compose   Tag images with 'loc' and deploy using Docker Compose."
    echo "        -m --minikube  Tag images with 'loc' and deploy to local Minikube Kubernetes cluster."
    echo ""
    echo "Options:"
    echo "    General options:"
    echo "        -h --help     Show this help message."
    echo "        -b --build    Build the images of the selected mode before deployment."
    echo "        -u --push     Push the latest version of the images for the selected mode to Azure"
    echo "                      Container Registry (ACR) after build and before deployment."
    echo "        -r --restart  When deploying to a Kubernetes cluster, only restart the containers, "
    echo "                      hence pulling the latest images from the Azure Container Registry (ACR)."
    echo "        -t --compile  When deploying in compose mode, select this option to compile code and"
    echo "                      serve static files instead of using a development server."
    echo "        -tag          When deploying to a Kubernetes cluster, you can provide a specific build"
    echo "                      that has been previously already been built and pushed to the container"
    echo "                      registry. So that various environments can be set up with the same image."
    echo ""
    echo "    The following options are only relevant for local development (i.e. compose and minikube"
    echo "    modes) and can be instead specified in the .env file:"
    echo "        --ssl-certs-path <PATH>    Specify path to the directory containing SSL certificates."
    echo "        --whitelist-path <PATH>    Specify path to the IP address whitelist file for Nginx."
    echo "        --filestorage-path <PATH>  Specify path to the directory that will be used as a file"
    echo "                                   share volume."
    echo "        --postgresql-path <PATH>   Specify path to the directory containg persistentPostgreSQL"
    echo "                                   data."

    exit 1
}

###############################################################################
# Parse arguments
###############################################################################
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    # Display help
    -h|--help)
        display_help
        exit 0
        ;;

    # List of modes
    -d|--development)
        MODE="development"
        shift
        ;;
    -p|--production)
        MODE="production"
        shift
        ;;
    -s|--sandbox)
        MODE="sandbox"
        shift
        ;;
    -g|--staging)
        MODE="staging"
        shift
        ;;
    -c|--compose)
        MODE="compose"
        shift
        ;;
    -m|--minikube)
        MODE="minikube"
        shift
        ;;

    # Build before deploying
    -b|--build)
        BUILD_ARG="true"
        shift
        ;;
    # Push before deploying
    -p|--push)
        PUSH_ARG="true"
        shift
        ;;
    # Only restart application
    -r|--restart)
        RESTART_ARG="true"
        shift
        ;;
    # Compile code in compose mode
    -t|--compile)
        COMPILE_ARG="true"
        shift
        ;;

    # SSL certificates path
    --ssl-certs-path)
        SSL_CERTS_PATH_ARG="$2"
        shift
        shift
        ;;
    # Whitelist path
    --whitelist-path)
        WHITELIST_PATH_ARG="$2"
        shift
        shift
        ;;
    # Filestorage path
    --filestorage-path)
        FILESTORAGE_PATH_ARG="$2"
        shift
        shift
        ;;
    # PostgreSQL path
    --postgresql-path)
        POSTGRESQL_PATH_ARG="$2"
        shift
        shift
        ;;
    # Explicit deployed tag
    --tag)
        EXPLICIT_TAG="$2"
        shift
        shift
        ;;
    # Any other value is unsupported
    *)
        display_help
        exit 0
esac
done

case $MODE in
    "production"|"development"|"sandbox"|"staging"|"compose"|"minikube")
        echo "Deploying in ${MODE} mode"
        ;;
    *)
        display_help
        exit 0
esac

###############################################################################
# Set mode-specific settings
###############################################################################

# used for Sentry error tracking
export ENVIRONMENT_STRING="${MODE}"
export FRONTEND_DSN="https://ac580607d5c8426e88bfd2fc507d0f15@o441468.ingest.sentry.io/5411679"
export BACKEND_DSN="https://00b50b39e5d7420ebdf859a68ed066b5@o441468.ingest.sentry.io/5411803"
export AGGREVATOR_DSN="https://3f535b18c4c445c5aa33ac12ba7ccce5@o441468.ingest.sentry.io/5412035"
export EMAIL_DSN="https://090a677d0cd24493b5de94fbb901a0e5@o441468.ingest.sentry.io/5412069"
export ENGINE_DSN="https://abee51b2e14e4734aa51670137c09eec@o441468.ingest.sentry.io/5412041"
export PARSER_DSN="https://32b7acc33f144524a9941b6f33cfd371@o441468.ingest.sentry.io/5463503"

# used for auth0 in non-production cases & will be overwritten for PROD env
# TODO: Use a custom domain when it is available in November 2020
export AUTH0_DOMAIN="archlet-playground.eu.auth0.com"

export SLACK_SIGNUP_URL="https://hooks.slack.com/services/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"

# [LOCAL] Compose
if   [[ $MODE == "compose" ]]; then
    export PUBLIC_DOMAIN_CN="localhost.archlet.io"

    export AUTH0_CLIENT_ID="2miJMhb456aSTG68bb7GnXuG25QZKlbz"
    export AUTH0_CLIENT_SECRET="RjXlb9akId8478gy5Vdd8-08JoG0jICCpY1AwouUNkAuuoArWTfn-5iySJRVpPTH"

    # If compile argument is NOT selected, build using the development servers
    # which use a different Dockerfile so we use a different tag
    if [[ -n $COMPILE_ARG ]]; then
        export RELEASE="loc"  # container images tag
        export AUTH0_CALLBACK_URL="https://localhost/api/v1/auth/callback"
        export AUTH0_FAILED_URL="https://localhost/sso-error"
    else
        export RELEASE="server"
        export AUTH0_CALLBACK_URL="http://localhost:4201/api/v1/auth/callback"
        export AUTH0_FAILED_URL="http://localhost:8080/sso-error"
    fi

    export ENVIRONMENT_STRING=""

# [LOCAL] Minikube
elif [[ $MODE == "minikube" ]]; then
    export PUBLIC_DOMAIN_CN="localhost.archlet.io"
    export RELEASE="loc"  # container images tag

    # Kubernetes specific
    CLUSTER="minikube"

    export ENVIRONMENT_STRING=""

# [Azure] Sandbox
elif [[ $MODE == "sandbox" ]]; then
    export PUBLIC_DOMAIN_CN="sandbox.archlet.io"
    export RELEASE="sandbox" # container images tag

    # Kubernetes specific
    CLUSTER="archlet-sandbox"

    # Azure specific
    DATABASE_NAME="archlet-sandbox"
    STORAGE_ACCOUNT="archletsandbox"
    RESOURCE_GROUP="archlet-sandbox"

    export AUTH0_CALLBACK_URL="https://sandbox.archlet.io/api/v1/auth/callback"
    export AUTH0_FAILED_URL="https://sandbox.archlet.io/sso-error"

# [Azure] Development
elif [[ $MODE == "development" ]]; then
    export PUBLIC_DOMAIN_CN="dev.archlet.io"
    export RELEASE="dev" # container images tag

    # Kubernetes specific
    CLUSTER="archlet-dev"

    # Azure specific
    DATABASE_NAME="archlet-dev-db" # dev-archlet
    STORAGE_ACCOUNT="archletdevfs"
    RESOURCE_GROUP="archlet-dev"

    export AUTH0_CALLBACK_URL="https://dev.archlet.io/api/v1/auth/callback"
    export AUTH0_FAILED_URL="https://dev.archlet.io/sso-error"

# [Azure] Staging / Demo
elif [[ $MODE == "staging" ]]; then
    export PUBLIC_DOMAIN_CN="demo.archlet.io"
    export RELEASE="staging" # container images tag

    # Kubernetes specific
    CLUSTER="archlet-staging"

    # Azure specific
    DATABASE_NAME="archlet-staging-db"
    STORAGE_ACCOUNT="archletstagingfs"
    RESOURCE_GROUP="archlet-staging"

    export AUTH0_CALLBACK_URL="https://demo.archlet.io/api/v1/auth/callback"
    export AUTH0_FAILED_URL="https://demo.archlet.io/sso-error"

# [Azure] Production
elif [[ $MODE == "production" ]]; then
    export PUBLIC_DOMAIN_CN="app.archlet.io"
    export RELEASE="prod" # container images tag

    # Kubernetes specific
    CLUSTER="archlet-prod"

    # Azure specific
    DATABASE_NAME="archlet-prod-db" # dev-archlet
    STORAGE_ACCOUNT="archletprodfs"
    RESOURCE_GROUP="archlet-prod"

    export AUTH0_DOMAIN="archlet-production.eu.auth0.com"
    export AUTH0_CALLBACK_URL="https://app.archlet.io/api/v1/auth/callback"
    export AUTH0_FAILED_URL="https://app.archlet.io/sso-error"

fi

###############################################################################
# Optionally build/push images before deploying
###############################################################################
if [[ -n "$BUILD_ARG" ]]; then
    ARGS="${RELEASE}"

    if [[ -n "$PUSH_ARG" ]]; then
        ARGS="${ARGS} --push"
    fi

    if [[ $MODE == "compose" && -z "$COMPILE_ARG" ]]; then
        ARGS="${ARGS} --server"
    fi

    if [[ $MODE == "minikube" ]]; then
        ARGS="${ARGS} --minikube"
    fi

    ./build-images.sh $ARGS
fi

###############################################################################
# Read arguments for local deployment
###############################################################################
if [[ $MODE == "compose" || $MODE == "minikube" ]]; then
    # Parse user ags
    export SSL_CERTS_PATH="${SSL_CERTS_PATH_ARG:-$(pwd)/config/ssl-certificates/openssl/localhost.archlet.io}"
    export WHITELIST_PATH="${WHITELIST_PATH_ARG:-$(pwd)/config/access-control/loc}"
    export FILESHARE_VOLUME_HOST_PATH="${FILESTORAGE_PATH_ARG:-${HOME}/Documents/archlet-filestorage}"
    export POSTGRESQL_VOLUME_HOST_PATH="${POSTGRESQL_PATH_ARG:-${HOME}/Documents/archlet-postgresql}"
    export SOURCE_CODE_PATH="${SOURCE_CODE_PATH_ARG:-$(dirname $(pwd))}"

    mkdir -p ${FILESHARE_VOLUME_HOST_PATH}
    mkdir -p ${POSTGRESQL_VOLUME_HOST_PATH}

    # Read values from.env file
    source .env
fi

###############################################################################
# Select kubectl context if applicable
###############################################################################
if [[ $MODE != "compose" ]]; then
    kubectl config use-context "${CLUSTER}"
fi

###############################################################################
# If --restart was selected, only restart resources
###############################################################################
if [[ -n "$RESTART_ARG" ]]; then
    if [[ $MODE == "compose" ]]; then
        docker-compose restart
    else
        echo "Restarting services on Kubernetes cluster..."
        kubectl rollout restart deployment frontend
        kubectl rollout restart deployment backend
        kubectl rollout restart deployment engine
        # queue restart not needed since we are'nt deploying anything custom there
        # kubectl rollout restart deployment queue
        kubectl rollout restart deployment email
        kubectl rollout restart deployment aggrevator
        kubectl rollout restart deployment parser

        exit 0
    fi
fi

###############################################################################
# If applicable, login to Azure
###############################################################################
# Login to Azure CLI when not local mode
if [[ $MODE != "compose" && $MODE != "minikube" ]]; then
    output=$(az account show --output=tsv  2> /dev/null)
    if [[ $output == "" ]]; then
        az login
    fi
fi

###############################################################################
# Storage account, database, ACR passwords
###############################################################################
if [[ $MODE != "compose" && $MODE != "minikube" ]]; then
    AZURE_PUBLIC_IP_NAME="frontend-public-ip"
    AZURE_KEY_VAULT="${CLUSTER}-vault"

    # Load password for Azure container registry
    # NOTE: Same registry for development and production modes but different
    #       container tags.
    ACR_PASSWORD=$(az acr credential show \
        --name "archlet" \
        --query "passwords[0].value" \
        --output tsv)
    export AZURE_CONTAINER_REGISTRY_PASSWORD=$ACR_PASSWORD

    # Azure Service Principal ID
    CLIENT_ID=$(az aks list --resource-group archlet \
        --query "[0].servicePrincipalProfile.clientId" \
        --output tsv)
    export AZURE_SERVICE_PRINCIPAL_ID=$CLIENT_ID

    # Load storage account key to env variable
    export AZURE_STORAGE_ACCOUNT_NAME="${STORAGE_ACCOUNT}"

    # Load storage account key to env variable
    STORAGEKEY=$(az storage account keys list \
        --resource-group "${RESOURCE_GROUP}" \
        --account-name "${AZURE_STORAGE_ACCOUNT_NAME}" \
        --query "[0].value" \
        --output tsv)
    export AZURE_STORAGE_ACCOUNT_KEY=$STORAGEKEY

    # Load username for Azure PostgreSQL database
    export AZURE_POSTGRESQL_DATABASE_USER="archlet@${DATABASE_NAME}"

    # Load password for Azure PostgreSQL database
    DB_PASSWORD=$(az keyvault secret show \
        --name "database-admin-password" \
        --vault-name "${AZURE_KEY_VAULT}" \
        --query "value" \
        --output tsv)
    export AZURE_POSTGRESQL_DATABASE_PASSWORD=$DB_PASSWORD

    # Load URL of Azure database
    export AZURE_POSTGRESQL_DATABASE_URL="${DATABASE_NAME}.postgres.database.azure.com"

    # Public IP address
    NODE_RESOURCE_GROUP=$(az aks show \
        --resource-group ${RESOURCE_GROUP} \
        --name ${CLUSTER} \
        --query nodeResourceGroup \
        --output tsv)
    PUBLIC_IP=$(az network public-ip show \
        --resource-group ${NODE_RESOURCE_GROUP} \
        --name ${AZURE_PUBLIC_IP_NAME} \
        --query "ipAddress" \
        --output tsv)
    export AZURE_CLUSTER_PUBLIC_IP=$PUBLIC_IP

    # Get SendGrid API key
    export SENDGRID_API_KEY=$(az keyvault secret show \
        --name "sendgrid-api-access" \
        --vault-name "${AZURE_KEY_VAULT}"  \
        --query "value" --output tsv)

    # get Auth0 ID and secret for the current environment
    export AUTH0_CLIENT_ID=$(az keyvault secret show \
        --name "auth0-client-id" \
        --vault-name "${AZURE_KEY_VAULT}"  \
        --query "value" --output tsv)

    export AUTH0_CLIENT_SECRET=$(az keyvault secret show \
        --name "auth0-client-secret" \
        --vault-name "${AZURE_KEY_VAULT}"  \
        --query "value" --output tsv)
fi

###############################################################################
# SSL certificates and Whitelist
###############################################################################
if [[ $MODE != "compose" && $MODE != "minikube" ]]; then

    # Replace . by - in FDQN since Azure secret names can't contain . character
    PARSED_FDQN=$(echo "${PUBLIC_DOMAIN_CN}" | tr . -)

    # Load SSL certificate and key from key vault
    BASE64_FULLCHAIN=$(az keyvault secret show \
        --name "ssl-certificate-fullchain" \
        --vault-name "${AZURE_KEY_VAULT}" \
        --query "value" \
        --output tsv)
    export BASE64_LETSENCRYPT_SSL_FULLCHAIN=$BASE64_FULLCHAIN

    BASE64_PRIVKEY=$(az keyvault secret show \
        --name "ssl-certificate-privkey" \
        --vault-name "${AZURE_KEY_VAULT}" \
        --query "value" \
        --output tsv)
    export BASE64_LETSENCRYPT_SSL_PRIVKEY=$BASE64_PRIVKEY

    BASE64_WHITELIST=$(az keyvault secret show \
        --name "nginx-whitelist-conf" \
        --vault-name "${AZURE_KEY_VAULT}" \
        --query "value" \
        --output tsv)
    export BASE64_NGINX_WHITELIST_CONF=$BASE64_PRIVKEY
elif [[ $MODE == "minikube" ]]; then
    # Read SSL certificate/key and whitelist into base64
    export BASE64_LETSENCRYPT_SSL_FULLCHAIN=$(cat ${SSL_CERTS_PATH}/fullchain.pem | base64 -b 0)
    export BASE64_LETSENCRYPT_SSL_PRIVKEY=$(cat ${SSL_CERTS_PATH}/privkey.pem | base64 -b 0)
    export BASE64_NGINX_WHITELIST_CONF=$(cat ${WHITELIST_PATH}/whitelist.conf | base64 -b 0)
fi

###############################################################################
# Ports
###############################################################################
export BACKEND_REST_API_SERVER_PORT="4201"

###############################################################################
# Handle Docker Compose deployment separately
###############################################################################
if [[ $MODE == "compose" ]]; then

    if [[ -n $COMPILE_ARG ]]; then
        docker-compose --file "./templates/compose/compile/docker-compose.yaml" up --force-recreate
    else
        docker-compose --file "./templates/compose/server/docker-compose.yaml" up --force-recreate
    fi

    exit 0
fi

###############################################################################
# Deploy resources to AKS cluster
###############################################################################
if [[ $MODE == "minikube" ]]; then
    TEMPLATES_FOLDER="minikube"
else
    TEMPLATES_FOLDER="azure"
fi

# Deploy secrets containing whitelist and SSL certificate for Nginx as well as
# storage account credentials
envsubst < "./templates/${TEMPLATES_FOLDER}/secrets.yaml"  | kubectl apply -f -

if [[ $MODE == "minikube" ]]; then
    # Database runs inside cluster in minikube
    envsubst < "./templates/${TEMPLATES_FOLDER}/database.yaml" | kubectl apply -f -
else
    # Fetch ACR credentials
    export SP_ACR_CLIENT_ID=$(az keyvault secret show \
        --name "sp-acr-client-id" \
        --vault-name "${AZURE_KEY_VAULT}"  \
        --query "value" --output tsv)
    export SP_ACR_SP_PASSWD=$(az keyvault secret show \
        --name "sp-acr-password" \
        --vault-name "${AZURE_KEY_VAULT}"  \
        --query "value" --output tsv)

    # Create secret containing ACR credentials
    kubectl create secret docker-registry acr-auth \
        --docker-server archlet.azurecr.io \
        --docker-username $SP_ACR_CLIENT_ID \
        --docker-password $SP_ACR_SP_PASSWD \
        --docker-email contact@archlet.ch
fi

if [[ ! -z "$EXPLICIT_TAG" ]]; then
    echo "Deploying explicit tag: ${EXPLICIT_TAG}"
    export RELEASE=${EXPLICIT_TAG}
fi

# Deploy frontend, backend, engine and queue services
envsubst < "./templates/${TEMPLATES_FOLDER}/queue.yaml"    | kubectl apply -f -
envsubst < "./templates/${TEMPLATES_FOLDER}/engine.yaml"   | kubectl apply -f -
envsubst < "./templates/${TEMPLATES_FOLDER}/backend.yaml"  | kubectl apply -f -
envsubst < "./templates/${TEMPLATES_FOLDER}/email.yaml"  | kubectl apply -f -
envsubst < "./templates/${TEMPLATES_FOLDER}/frontend.yaml" | kubectl apply -f -
envsubst < "./templates/${TEMPLATES_FOLDER}/aggrevator.yaml"  | kubectl apply -f -
envsubst < "./templates/${TEMPLATES_FOLDER}/parser.yaml"  | kubectl apply -f -
