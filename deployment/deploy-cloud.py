# -*- coding: utf-8 -*-
from __future__ import print_function, unicode_literals

import os
from PyInquirer import prompt
from git import Repo

environments = [
  'sandbox',
  'development',
  'staging',
  'production',
]

def get_all_tags(repo):
  return list(reversed(repo.git.tag(sort='creatordate').split('\n')))

def main():
  repo = Repo(os.getcwd()+'/..')
  assert not repo.bare

  tags = get_all_tags(repo)

  questions = [
      {
          'type': 'list',
          'name': 'tag',
          'message': 'What version do you want to deploy?',
          'choices': tags,
      },
      {
          'type': 'list',
          'name': 'environment',
          'message': 'Which environment do you want to deploy to?',
          'choices': environments,
      },
  ]

  answers = prompt(questions)
  print("Selected parameters:")
  print(answers)

  environment = answers['environment']
  tag = answers['tag']
  command = f"sh deploy-app.sh --{environment} --tag {tag}"

  print("Resulting command:")
  print(command)

  os.system(command)

if __name__ == "__main__":
  main()
