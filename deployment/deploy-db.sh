#!/usr/bin/env bash

###############################################################################
# Display help message
###############################################################################
function display_help() {
    echo "usage: $0 [options]"
    echo ""
    echo "Options:"
    echo "    --postgresql-path <PATH>   Specify path to the directory containg persistentPostgreSQL"
    echo "                               data."

    exit 1
}

###############################################################################
# Parse arguments
###############################################################################
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    # Display help
    -h|--help)
        display_help
        exit 0
        ;;
    # PostgreSQL path
    --postgresql-path)
        POSTGRESQL_PATH_ARG="$2"
        shift
        shift
        ;;
    # Any other value is unsupported
    *)
        display_help
        exit 0
esac
done

###############################################################################
# Read arguments and do local deployment
###############################################################################
export POSTGRESQL_VOLUME_HOST_PATH="${POSTGRESQL_PATH_ARG:-${HOME}/Documents/archlet-postgresql}"

docker-compose --file "./templates/compose/database/docker-compose.yaml" up --force-recreate
