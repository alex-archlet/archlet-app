#!/usr/bin/env bash

###############################################################################
# Parse arguments
###############################################################################
case $1 in
    # Development mode
    -d|--development)
        MODE="dev"
        ;;
    # Production mode
    -p|--production)
        MODE="prod"
        ;;
    # Local mode
    -l|--local)
        MODE="loc"
        ;;
    # Any other value is unsupported
    *)
        echo "usage: $0 (--sandbox|--development|--production)"
        exit 1
esac

###############################################################################
# Dump database
###############################################################################

if [[ $MODE == "loc" ]]; then
    echo "Not supported yet!"
    exit 1
else
    # Login to Azure only if not already logged in
    output=$(az account show --output=tsv  2> /dev/null)
    if [[ $output == "" ]]; then
        az login
    fi

    # Load URL of Azure database
    DB_URL="archlet-${MODE}.postgres.database.azure.com"

    # Load username for Azure PostgreSQL database
    DB_USER="archlet@archlet-${MODE}"

    # Load password for Azure PostgreSQL database
    DB_PASSWORD=$(az keyvault secret show \
        --name "database-admin-password-archlet-${MODE}" \
        --vault-name "archlet-app-secrets" \
        --query "value" \
        --output tsv)
fi

# Run dump command
PGPASSWORD="${DB_PASSWORD}" pg_dump -Fc -COx -v --no-password \
    --host="${DB_URL}" \
    --username="${DB_USER}" \
    --dbname=postgres \
    -f "azure-archlet-${MODE}-psql.dump"

# Run restore command
PGPASSWORD="postgres" pg_restore -Ox -c -v --no-password \
    --dbname=postgres \
    --host=localhost \
    --port=15432 \
    --username=postgres \
    "azure-archlet-dev-psql.dump"