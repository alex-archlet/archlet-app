import { BindOrReplacements } from 'sequelize/types';
import { Query } from './Query';

export const queryString = `
select round_id, count(id) offers_count, count(DISTINCT supplier) suppliers_count
from public.offers
where is_historic = false and round_id IN (:roundIds)
group by round_id
`;

interface BiddingRoundListKpiResult {
  round_id: string;
  offers_count: number;
  suppliers_count: number;
}

type BiddingRoundListKpiReplacements = BindOrReplacements & {
  roundIds: string[];
}

export const BIDDING_ROUNDS_LIST_KPI_QUERY = new Query<
  BiddingRoundListKpiResult,
  BiddingRoundListKpiReplacements
>(queryString);
