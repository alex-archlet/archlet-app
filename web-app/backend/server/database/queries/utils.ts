import {
  QueryTypes,
  BindOrReplacements,
} from 'sequelize';

import { sequelize } from '../instance';
import { Query } from './Query';

export async function runSelectQuery<
  T extends object,
  D extends BindOrReplacements
>(query: Query<T, D>, replacements: D) {
  return sequelize.query<T>(query.query, {
    type: QueryTypes.SELECT,
    replacements,
  });
}
