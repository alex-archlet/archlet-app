import { BindOrReplacements } from 'sequelize/types';

export class Query<T extends object, D extends BindOrReplacements> {
  public query: string;

  constructor(query: string) {
    this.query = query;
  }
}
