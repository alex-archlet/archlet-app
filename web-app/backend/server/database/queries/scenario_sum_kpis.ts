import { BindOrReplacements } from 'sequelize/types';
import { Query } from './Query';

export const queryString = `
select scenario_id, project_kpi_defs.id as kpi_id, SUM(value) as value
from public.scenario_kpis
left join public.project_kpi_defs on project_kpi_defs.id = scenario_kpis.project_kpi_def_id
where scenario_id IN (:scenarioIds)
group by scenario_id, project_kpi_defs.id
`;

export interface ScenarioSumKpiResult {
  scenario_id: string;
  kpi_id: string;
  value: number;
}

type ScenarioSumReplacements = BindOrReplacements & {
  scenarioIds: string[];
}

export const SCENARIO_SUM_KPIS = new Query<
  ScenarioSumKpiResult,
  ScenarioSumReplacements
>(queryString);
