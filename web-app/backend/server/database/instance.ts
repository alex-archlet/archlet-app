import { Sequelize } from 'sequelize';
import cls from 'cls-hooked';

import configs from './config/config';

const namespace = cls.createNamespace('my-very-own-namespace');

Sequelize.useCLS(namespace);

const env = process.env.NODE_ENV || 'development';
// eslint-disable-next-line security/detect-object-injection
const config = configs[env];

export const sequelize = new Sequelize(
  config.database,
  config.username,
  config.password,
  config
);
