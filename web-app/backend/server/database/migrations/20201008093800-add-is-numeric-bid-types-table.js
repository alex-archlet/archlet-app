module.exports = {
  up: (queryInterface, Sequelize) => (
    queryInterface.addColumn(
      'column_types',
      'is_numeric',
      {
        defaultValue: false,
        type: Sequelize.BOOLEAN,
      }
    )
  ),
  down: (queryInterface, Sequelize) => (
    queryInterface.removeColumn(
      'column_types',
      'is_numeric',
      {
        defaultValue: false,
        type: Sequelize.BOOLEAN,
      }
    )
  ),
};
