module.exports = {
  up: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.sequelize.query(`
        UPDATE bid_columns
        SET new_type_id = column_types.new_id
        FROM column_types 
        WHERE type_id = column_types.id
      `, { transaction });
      await queryInterface.sequelize.query(`
        UPDATE bid_columns
        SET new_initial_type_id = column_types.new_id
        FROM column_types 
        WHERE initial_type_id = column_types.id
      `, { transaction });

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: () => Promise.resolve(),
};
