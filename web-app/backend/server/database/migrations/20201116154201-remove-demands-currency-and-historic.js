module.exports = {
  up: async (queryInterface) => {
    await queryInterface.removeColumn(
      'demands',
      'historic_price'
    );
    await queryInterface.removeColumn(
      'demands',
      'historic_supplier'
    );
    await queryInterface.removeColumn(
      'demands',
      'historic_supplier_id'
    );
    await queryInterface.removeColumn(
      'demands',
      'currency'
    );
    await queryInterface.removeColumn(
      'demands',
      'unit'
    );
  },
  down: () => Promise.resolve(),
};
