module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.createTable(
        'allocations',
        {
          id: {
            allowNull: false,
            primaryKey: true,
            type: Sequelize.UUID,
            defaultValue: Sequelize.literal('uuid_generate_v4()'),
          },
          created_at: {
            allowNull: false,
            type: Sequelize.DATE,
          },
          updated_at: {
            allowNull: false,
            type: Sequelize.DATE,
          },
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'allocations',
        'offer_id',
        {
          type: Sequelize.UUID,
          references: {
            model: 'offers',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'allocations',
        'demand_id',
        {
          type: Sequelize.UUID,
          references: {
            model: 'demands',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'allocations',
        'scenario_id',
        {
          type: Sequelize.UUID,
          references: {
            model: 'scenarios',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeColumn('allocations', 'scenario_id', { transaction });
      await queryInterface.removeColumn('allocations', 'demand_id', { transaction });
      await queryInterface.removeColumn('allocations', 'offer_id', { transaction });
      await queryInterface.dropTable('allocations', { transaction });
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
};
