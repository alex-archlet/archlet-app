module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.createTable(
        'feature_flags',
        {
          id: {
            allowNull: false,
            primaryKey: true,
            type: Sequelize.UUID,
            defaultValue: Sequelize.literal('uuid_generate_v4()'),
          },
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'feature_flags',
        'flag_type_id',
        {
          type: Sequelize.INTEGER,
          references: {
            model: 'feature_flag_types',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
          allowNull: false,
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'feature_flags',
        'branch_id',
        {
          type: Sequelize.UUID,
          references: {
            model: 'branches',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
          allowNull: false,
        },
        { transaction }
      );

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: async queryInterface => queryInterface.dropTable('feature_flags'),
};
