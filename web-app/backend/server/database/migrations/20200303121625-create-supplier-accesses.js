module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.createTable(
        'supplier_accesses',
        {
          access_type: {
            allowNull: false,
            type: Sequelize.STRING,
          },
          round_id: {
            allowNull: false,
            primaryKey: true,
            type: Sequelize.UUID,
          },
          user_id: {
            allowNull: false,
            primaryKey: true,
            type: Sequelize.UUID,
          },
          created_at: {
            allowNull: false,
            type: Sequelize.DATE,
          },
          updated_at: {
            allowNull: false,
            type: Sequelize.DATE,
          },
        },
        { transaction }
      );

      await queryInterface.addConstraint(
        'supplier_accesses',
        ['user_id'],
        {
          type: 'FOREIGN KEY',
          name: 'supplier_accesses_user_id_fkey',
          references: {
            table: 'users',
            field: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
          transaction,
        }
      );

      await queryInterface.addConstraint(
        'supplier_accesses',
        ['round_id'],
        {
          type: 'FOREIGN KEY',
          name: 'supplier_accesses_round_id_fkey',
          references: {
            table: 'bidding_rounds',
            field: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
          transaction,
        }
      );

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeConstraint('supplier_accesses', 'supplier_accesses_user_id_fkey', { transaction });
      await queryInterface.removeConstraint('supplier_accesses', 'supplier_accesses_round_id_fkey', { transaction });
      await queryInterface.dropTable('supplier_accesses', { transaction });
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
};
