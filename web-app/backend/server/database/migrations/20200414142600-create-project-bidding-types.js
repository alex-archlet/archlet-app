module.exports = {
  up: async (queryInterface, Sequelize) => {
    try {
      await queryInterface.createTable(
        'project_bidding_types',
        {
          id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER,
          },
          code: {
            allowNull: false,
            type: Sequelize.STRING,
            unique: true,
          },
          name: {
            allowNull: false,
            type: Sequelize.STRING,
            unique: true,
          },
          created_at: {
            allowNull: false,
            type: Sequelize.DATE,
          },
          updated_at: {
            allowNull: false,
            type: Sequelize.DATE,
          },
        }
      );
    } catch (err) {
      throw err;
    }
  },
  down: async (queryInterface) => {
    try {
      await queryInterface.dropTable('project_bidding_types');
    } catch (err) {
      throw err;
    }
  },
};
