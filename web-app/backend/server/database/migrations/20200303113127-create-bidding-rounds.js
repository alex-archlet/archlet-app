module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.createTable(
        'bidding_rounds',
        {
          id: {
            allowNull: false,
            primaryKey: true,
            type: Sequelize.UUID,
            defaultValue: Sequelize.literal('uuid_generate_v4()'),
          },
          name: {
            allowNull: false,
            type: Sequelize.STRING,
          },
          description: {
            type: Sequelize.TEXT,
          },
          start_at: {
            type: Sequelize.DATE,
          },
          end_at: {
            type: Sequelize.DATE,
          },
          reminder_at: {
            type: Sequelize.DATE,
          },
          created_at: {
            allowNull: false,
            type: Sequelize.DATE,
          },
          updated_at: {
            allowNull: false,
            type: Sequelize.DATE,
          },
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'bidding_rounds',
        'project_id',
        {
          type: Sequelize.UUID,
          references: {
            model: 'projects',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'bidding_rounds',
        'status_id',
        {
          type: Sequelize.UUID,
          references: {
            model: 'bid_round_statuses',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'bidding_rounds',
        'created_by',
        {
          type: Sequelize.UUID,
          references: {
            model: 'users',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'bidding_rounds',
        'updated_by',
        {
          type: Sequelize.UUID,
          references: {
            model: 'users',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'scenarios',
        'round_id',
        {
          type: Sequelize.UUID,
          references: {
            model: 'bidding_rounds',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeColumn('scenarios', 'round_id', { transaction });
      await queryInterface.removeColumn('bidding_rounds', 'updated_by', { transaction });
      await queryInterface.removeColumn('bidding_rounds', 'created_by', { transaction });
      await queryInterface.removeColumn('bidding_rounds', 'status_id', { transaction });
      await queryInterface.removeColumn('bidding_rounds', 'project_id', { transaction });
      await queryInterface.dropTable('bidding_rounds', { transaction });
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
};
