module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.createTable(
        'offers',
        {
          id: {
            allowNull: false,
            primaryKey: true,
            type: Sequelize.UUID,
            defaultValue: Sequelize.literal('uuid_generate_v4()'),
          },
          volume: {
            type: Sequelize.FLOAT,
          },
          price: {
            type: Sequelize.FLOAT,
          },
          total_price: {
            type: Sequelize.FLOAT,
          },
          currency: {
            type: Sequelize.STRING,
          },
          supplier: {
            type: Sequelize.STRING,
          },
          attributes: {
            type: Sequelize.JSONB,
          },
          created_at: {
            allowNull: false,
            type: Sequelize.DATE,
          },
          updated_at: {
            allowNull: false,
            type: Sequelize.DATE,
          },
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'offers',
        'created_by',
        {
          type: Sequelize.UUID,
          references: {
            model: 'users',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'offers',
        'updated_by',
        {
          type: Sequelize.UUID,
          references: {
            model: 'users',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'offers',
        'supplier_id',
        {
          type: Sequelize.UUID,
          references: {
            model: 'users',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'offers',
        'round_id',
        {
          type: Sequelize.UUID,
          references: {
            model: 'bidding_rounds',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'offers',
        'demand_id',
        {
          type: Sequelize.UUID,
          references: {
            model: 'demands',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeColumn('offers', 'demand_id', { transaction });
      await queryInterface.removeColumn('offers', 'round_id', { transaction });
      await queryInterface.removeColumn('offers', 'supplier_id', { transaction });
      await queryInterface.removeColumn('offers', 'updated_by', { transaction });
      await queryInterface.removeColumn('offers', 'created_by', { transaction });
      await queryInterface.dropTable('offers', { transaction });
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
};
