module.exports = {
  up: async (queryInterface, Sequelize) => queryInterface
    .sequelize
    .transaction(async (transaction) => {
      await queryInterface.changeColumn(
        'bid_columns',
        'calculation',
        {
          allowNull: true,
          type: Sequelize.TEXT,
        },
        { transaction }
      );
      await queryInterface.changeColumn(
        'column_types',
        'text',
        {
          allowNull: true,
          type: Sequelize.TEXT,
        },
        { transaction }
      );
      await queryInterface.changeColumn(
        'optimization_files',
        'description',
        {
          allowNull: true,
          type: Sequelize.TEXT,
        },
        { transaction }
      );
      await queryInterface.changeColumn(
        'optimization_files',
        'file_path',
        {
          allowNull: true,
          type: Sequelize.TEXT,
        },
        { transaction }
      );
    }),
  // nothing really valuable to take back here. even rather dangerous since the columns could lose data on truncation
  down: Promise.resolve,
};
