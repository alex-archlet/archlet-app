module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      'discounts',
      'description',
      {
        allowNull: true,
        type: Sequelize.STRING,
      }
    );
  },
  down: () => Promise.resolve(),
};
