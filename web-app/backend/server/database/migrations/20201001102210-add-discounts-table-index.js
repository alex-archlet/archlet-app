module.exports = {
  up: async (queryInterface) => {
    await queryInterface.addIndex(
      'discounts',
      {
        fields: ['round_id'],
      }
    );
  },
  down: Promise.resolve,
};
