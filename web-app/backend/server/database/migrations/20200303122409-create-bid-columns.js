module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.createTable(
        'bid_columns',
        {
          id: {
            allowNull: false,
            primaryKey: true,
            type: Sequelize.UUID,
            defaultValue: Sequelize.literal('uuid_generate_v4()'),
          },
          name: {
            allowNull: false,
            type: Sequelize.STRING,
          },
          description: {
            type: Sequelize.TEXT,
          },
          calculation: {
            type: Sequelize.STRING,
          },
          is_default: {
            defaultValue: false,
            type: Sequelize.BOOLEAN,
          },
          is_mandatory: {
            defaultValue: false,
            type: Sequelize.BOOLEAN,
          },
          is_visible: {
            defaultValue: true,
            type: Sequelize.BOOLEAN,
          },
          created_at: {
            allowNull: false,
            type: Sequelize.DATE,
          },
          updated_at: {
            allowNull: false,
            type: Sequelize.DATE,
          },
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'bid_columns',
        'input_by',
        {
          type: Sequelize.UUID,
          references: {
            model: 'users',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'bid_columns',
        'round_id',
        {
          type: Sequelize.UUID,
          references: {
            model: 'bidding_rounds',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'bid_columns',
        'type_id',
        {
          type: Sequelize.UUID,
          references: {
            model: 'column_types',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeColumn('bid_columns', 'type_id', { transaction });
      await queryInterface.removeColumn('bid_columns', 'round_id', { transaction });
      await queryInterface.removeColumn('bid_columns', 'input_by', { transaction });
      await queryInterface.dropTable('bid_columns', { transaction });
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
};
