module.exports = {
  up: async queryInterface => queryInterface.removeColumn(
    'offers',
    'currency'
  ),
  down: () => Promise.resolve(),
};
