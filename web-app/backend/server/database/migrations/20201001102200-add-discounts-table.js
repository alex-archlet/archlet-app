module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.createTable(
        'discounts',
        {
          id: {
            allowNull: false,
            primaryKey: true,
            type: Sequelize.UUID,
            defaultValue: Sequelize.literal('uuid_generate_v4()'),
          },
          threshold: {
            allowNull: false,
            type: Sequelize.DOUBLE,
          },
          discount_amount: {
            allowNull: false,
            type: Sequelize.DOUBLE,
          },
          discount_type: {
            allowNull: false,
            type: Sequelize.STRING,
          },
          unit: {
            type: Sequelize.STRING,
            allowNull: false,
          },
          volume_name: {
            type: Sequelize.STRING,
            allowNull: true,
          },
          price_component: {
            type: Sequelize.STRING,
            allowNull: true,
          },
          bucket_id: {
            type: Sequelize.STRING,
            allowNull: false,
          },
          supplier: {
            type: Sequelize.STRING,
            allowNull: true,
          },
          created_at: {
            allowNull: false,
            type: Sequelize.DATE,
          },
          updated_at: {
            allowNull: false,
            type: Sequelize.DATE,
          },
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'discounts',
        'round_id',
        {
          type: Sequelize.UUID,
          allowNull: false,
          references: {
            model: 'bidding_rounds',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE',
        },
        { transaction }
      );

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: async queryInterface => queryInterface.dropTable('discounts'),
};
