// this method of adding indexes is used because
// we aren't using sequelize.sync(), which would
// create it from the model attributes

module.exports = {
  up: async (queryInterface) => {
    // bid_columns
    await queryInterface.addIndex(
      'bid_columns',
      {
        fields: ['project_id'],
      }
    );
    await queryInterface.addIndex(
      'bid_columns',
      {
        fields: ['type_id'],
      }
    );
    // bidding_rounds
    await queryInterface.addIndex(
      'bidding_rounds',
      {
        fields: ['status_id'],
      }
    );
    await queryInterface.addIndex(
      'bidding_rounds',
      {
        fields: ['project_id'],
      }
    );
    // jobs
    await queryInterface.addIndex(
      'jobs',
      {
        fields: ['scenario_id'],
      }
    );
    await queryInterface.addIndex(
      'jobs',
      {
        fields: ['project_id'],
      }
    );
    await queryInterface.addIndex(
      'jobs',
      {
        fields: ['round_id'],
      }
    );
    // optimization_files
    await queryInterface.addIndex(
      'optimization_files',
      {
        fields: ['scenario_id'],
      }
    );
    await queryInterface.addIndex(
      'optimization_files',
      {
        fields: ['project_id'],
      }
    );
    await queryInterface.addIndex(
      'optimization_files',
      {
        fields: ['round_id'],
      }
    );
    // scenarios
    await queryInterface.addIndex(
      'scenarios',
      {
        fields: ['project_id'],
      }
    );
    await queryInterface.addIndex(
      'scenarios',
      {
        fields: ['round_id'],
      }
    );
  },
  down: Promise.resolve,
};
