module.exports = {
  up: async (queryInterface, Sequelize) => {
    // update values
    await queryInterface.sequelize.query(`
      UPDATE
        public.bid_columns b_c
      SET
        project_id = b_r.project_id
      FROM
        public.bidding_rounds b_r
      WHERE
        b_c.round_id = b_r.id;
    `);

    // DEV: delete all bid columns whose round/project was deleted
    await queryInterface.bulkDelete(
      'bid_columns',
      {
        project_id: null,
      }
    );

    // set as non-null
    await queryInterface.changeColumn(
      'bid_columns',
      'project_id',
      {
        type: Sequelize.UUID,
        allowNull: false,
      }
    );
  },
  down: Promise.resolve,
};
