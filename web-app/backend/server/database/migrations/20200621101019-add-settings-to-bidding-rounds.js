module.exports = {
  up: async (queryInterface, Sequelize) => queryInterface
    .sequelize
    .transaction(async (transaction) => {
      await queryInterface.addColumn(
        'bidding_rounds',
        'settings',
        {
          allowNull: true,
          type: Sequelize.JSONB,
        },
        { transaction }
      );
    }),
  down: async queryInterface => queryInterface
    .sequelize
    .transaction(async (transaction) => {
      await queryInterface.removeColumn(
        'bidding_rounds',
        'settings',
        { transaction }
      );
    }),
};
