module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      'column_types',
      'is_buyer',
      {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      }
    );
    await queryInterface.addColumn(
      'column_types',
      'is_supplier',
      {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      }
    );
  },
  down: () => Promise.resolve(),
};
