// this method of adding indexes is used because
// we aren't using sequelize.sync(), which would
// create it from the model attributes

module.exports = {
  up: async (queryInterface) => {
    // demands
    await queryInterface.addIndex(
      'demands',
      {
        fields: ['round_id'],
      }
    );
    // offers
    await queryInterface.addIndex(
      'offers',
      {
        fields: ['round_id'],
      }
    );
    await queryInterface.addIndex(
      'offers',
      {
        fields: ['demand_id'],
      }
    );
    // allocations
    await queryInterface.addIndex(
      'allocations',
      {
        fields: ['scenario_id'],
      }
    );
    await queryInterface.addIndex(
      'allocations',
      {
        fields: ['demand_id'],
      }
    );
    await queryInterface.addIndex(
      'allocations',
      {
        fields: ['offer_id'],
      }
    );
  },
  down: Promise.resolve,
};
