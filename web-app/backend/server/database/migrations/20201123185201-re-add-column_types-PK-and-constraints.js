module.exports = {
  up: async (queryInterface) => {
    await queryInterface.sequelize.query('ALTER TABLE column_types add constraint "column_types_pkey" PRIMARY KEY ("id")');
    await queryInterface.addConstraint(
      'bid_columns',
      ['type_id'],
      {
        type: 'FOREIGN KEY',
        name: 'bid_columns_type_id_fk',
        references: {
          table: 'column_types',
          field: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      }
    );
    await queryInterface.addConstraint(
      'bid_columns',
      ['initial_type_id'],
      {
        type: 'FOREIGN KEY',
        name: 'bid_columns_initial_type_id_fk',
        references: {
          table: 'column_types',
          field: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      }
    );
  },
  down: () => Promise.resolve(),
};
