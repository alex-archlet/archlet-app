module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.createTable(
        'scenario_kpis',
        {
          id: {
            allowNull: false,
            primaryKey: true,
            type: Sequelize.UUID,
            defaultValue: Sequelize.literal('uuid_generate_v4()'),
          },
          value: {
            allowNull: false,
            type: Sequelize.FLOAT,
          },
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'scenario_kpis',
        'scenario_id',
        {
          type: Sequelize.UUID,
          references: {
            model: 'scenarios',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
          allowNull: false,
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'scenario_kpis',
        'project_kpi_def_id',
        {
          type: Sequelize.UUID,
          references: {
            model: 'project_kpi_defs',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
          allowNull: false,
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'scenario_kpis',
        'allocation_id',
        {
          type: Sequelize.UUID,
          references: {
            model: 'allocations',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: async queryInterface => queryInterface.dropTable('scenario_kpis'),
};
