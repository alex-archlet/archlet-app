module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      'bid_columns',
      'new_type_id',
      {
        allowNull: true,
        type: Sequelize.INTEGER,
        defaultValue: null,
      }
    );
    await queryInterface.addColumn(
      'bid_columns',
      'new_initial_type_id',
      {
        allowNull: true,
        type: Sequelize.INTEGER,
        defaultValue: null,
      }
    );
  },
  down: () => Promise.resolve(),
};
