function setIsXValues(queryInterface, type, isBuyer, isSupplier, transaction) {
  return queryInterface.sequelize.query(`
    UPDATE column_types
    SET is_buyer = ${isBuyer}, is_supplier = ${isSupplier}
    WHERE type = '${type}'
  `, { transaction });
}

// type, is_buyer, is_supplier
const VALUES = [
  ['item', true, false],
  ['item_type', false, true],
  ['supplier', false, true],
  ['contributing_to', false, true],
  ['discounted_by', false, true],
  ['price_total', false, true],
  ['capacity', false, true],
];

module.exports = {
  up: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await Promise.all(VALUES.map(val => setIsXValues(queryInterface, ...val, transaction)));

      await queryInterface.bulkDelete('column_types', { type: ['buyer_bundle', 'supplier_bundle'] }, { transaction });

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: () => Promise.resolve(),
};
