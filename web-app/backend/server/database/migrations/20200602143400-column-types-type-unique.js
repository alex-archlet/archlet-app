module.exports = {
  up: async (queryInterface, Sequelize) => queryInterface.sequelize.transaction(async (transaction) => {
    const columnTypeIdsToRemove = [];
    const columnTypeTypeMap = new Map();
    const [columnTypes] = await queryInterface
      .sequelize
      .query('select * from public."column_types" ORDER BY "created_at" ASC', { transaction });

    columnTypes.forEach(({
      id,
      type,
    }) => {
      if (columnTypeTypeMap.has(type)) {
        columnTypeIdsToRemove.push(id);
      } else {
        columnTypeTypeMap.set(type, true);
      }
    });

    if (columnTypeIdsToRemove.length === 0) {
      await queryInterface.bulkDelete(
        'column_types',
        {
          id: columnTypeIdsToRemove,
        }
      );
    }

    return queryInterface.changeColumn(
      'column_types',
      'type',
      {
        // needs to be unique
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      { transaction }
    );
  }),
  down: Promise.resolve,
};
