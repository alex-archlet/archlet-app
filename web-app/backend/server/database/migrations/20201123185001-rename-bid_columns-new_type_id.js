module.exports = {
  up: async (queryInterface) => {
    await queryInterface.removeColumn(
      'bid_columns',
      'type_id'
    );
    await queryInterface.renameColumn(
      'bid_columns',
      'new_type_id',
      'type_id'
    );
    await queryInterface.removeColumn(
      'bid_columns',
      'initial_type_id'
    );
    await queryInterface.renameColumn(
      'bid_columns',
      'new_initial_type_id',
      'initial_type_id'
    );
    await queryInterface.removeColumn(
      'column_types',
      'id'
    );
    await queryInterface.renameColumn(
      'column_types',
      'new_id',
      'id'
    );
  },
  down: () => Promise.resolve(),
};
