module.exports = {
  async up(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.addColumn(
        'optimization_files',
        'sheet_name',
        {
          allowNull: true,
          type: Sequelize.STRING,
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'optimization_files',
        'row_number',
        {
          allowNull: true,
          type: Sequelize.INTEGER,
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'demands',
        'optimization_file_id',
        {
          type: Sequelize.UUID,
          references: {
            model: 'optimization_files',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'offers',
        'optimization_file_id',
        {
          type: Sequelize.UUID,
          references: {
            model: 'optimization_files',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  async down(queryInterface) {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeColumn(
        'optimization_files',
        'sheet_name',
        { transaction }
      );
      await queryInterface.removeColumn(
        'optimization_files',
        'row_number',
        { transaction }
      );

      await queryInterface.removeConstraint('demands', 'demands_optimization_file_id_fkey', { transaction });
      await queryInterface.removeColumn('demands', 'optimization_file_id', { transaction });

      await queryInterface.removeConstraint('offers', 'offers_optimization_file_id_fkey', { transaction });
      await queryInterface.removeColumn('offers', 'optimization_file_id', { transaction });

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
};
