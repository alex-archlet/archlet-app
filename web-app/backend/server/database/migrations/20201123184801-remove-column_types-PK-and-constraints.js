module.exports = {
  up: async (queryInterface) => {
    await queryInterface.removeConstraint('bid_columns', 'bid_columns_type_id_fkey');
    await queryInterface.removeConstraint('column_types', 'column_types_pkey');
  },
  down: () => Promise.resolve(),
};
