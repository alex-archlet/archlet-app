module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.createTable(
        'users',
        {
          id: {
            allowNull: false,
            primaryKey: true,
            type: Sequelize.UUID,
            defaultValue: Sequelize.literal('uuid_generate_v4()'),
          },
          account_type: {
            allowNull: false,
            type: Sequelize.STRING,
          },
          email: {
            allowNull: false,
            type: Sequelize.STRING,
          },
          first_name: {
            allowNull: false,
            type: Sequelize.STRING,
          },
          json: {
            defaultValue: null,
            type: Sequelize.JSONB,
          },
          last_login: {
            defaultValue: Sequelize.NOW,
            type: Sequelize.DATE,
          },
          last_name: {
            allowNull: false,
            type: Sequelize.STRING,
          },
          notes: {
            defaultValue: null,
            type: Sequelize.STRING,
          },
          password_hash: {
            allowNull: false,
            type: Sequelize.STRING,
          },
          password_salt: {
            allowNull: false,
            type: Sequelize.STRING,
          },
          preferences: {
            defaultValue: null,
            type: Sequelize.JSONB,
          },
          role: {
            allowNull: false,
            type: Sequelize.STRING,
          },
          total_login: {
            allowNull: false,
            defaultValue: 0,
            type: Sequelize.INTEGER,
          },
          username: {
            allowNull: false,
            type: Sequelize.STRING,
            unique: true,
          },
          created_at: {
            allowNull: false,
            type: Sequelize.DATE,
          },
          updated_at: {
            allowNull: false,
            type: Sequelize.DATE,
          },
        },
        { transaction }
      );

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.dropTable('users', { transaction });
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
};
