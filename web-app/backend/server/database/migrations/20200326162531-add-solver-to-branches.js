module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.addColumn(
      'branches',
      'solver', {
        type: Sequelize.STRING,
        defaultValue: 'CBC',
      }
    );
  },
  down(queryInterface) {
    return queryInterface.removeColumn(
      'branches',
      'solver'
    );
  },
};
