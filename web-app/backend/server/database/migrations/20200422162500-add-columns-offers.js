module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.addColumn(
        'offers',
        'total_volume', {
          allowNull: true,
          type: Sequelize.FLOAT,
        }
      );

      await queryInterface.addColumn(
        'offers',
        'total_fixed_price', {
          allowNull: true,
          type: Sequelize.FLOAT,
        }
      );

      await queryInterface.addColumn(
        'offers',
        'total_var_price', {
          allowNull: true,
          type: Sequelize.FLOAT,
        }
      );

      await queryInterface.addColumn(
        'offers',
        'is_processed', {
          allowNull: false,
          type: Sequelize.BOOLEAN,
        }
      );

      await queryInterface.addColumn(
        'offers',
        'processing_messages', {
          allowNull: true,
          type: Sequelize.JSONB,
        }
      );

      await queryInterface.removeColumn(
        'offers',
        'price'
      );

      await queryInterface.removeColumn(
        'offers',
        'volume'
      );
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeColumn(
        'offers',
        'total_volume'
      );

      await queryInterface.removeColumn(
        'offers',
        'total_fixed_price'
      );

      await queryInterface.removeColumn(
        'offers',
        'total_var_price'
      );

      await queryInterface.removeColumn(
        'offers',
        'is_processed'
      );

      await queryInterface.removeColumn(
        'offers',
        'processing_messages'
      );
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
};
