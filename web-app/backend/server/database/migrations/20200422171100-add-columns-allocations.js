module.exports = {
  up: async (queryInterface, Sequelize) => {
    try {
      await queryInterface.addColumn(
        'allocations',
        'volume', {
          allowNull: true,
          type: Sequelize.FLOAT,
        }
      );

      await queryInterface.addColumn(
        'allocations',
        'total_price', {
          allowNull: true,
          type: Sequelize.FLOAT,
        }
      );
    } catch (err) {
      throw err;
    }
  },
  down: async (queryInterface) => {
    try {
      queryInterface.removeColumn(
        'allocations',
        'volume'
      );

      queryInterface.removeColumn(
        'allocations',
        'total_price'
      );
    } catch (err) {
      throw err;
    }
  },
};
