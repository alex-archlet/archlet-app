module.exports = {
  up: async (queryInterface, Sequelize) => queryInterface
    .sequelize
    .transaction(async (transaction) => {
      await queryInterface.removeColumn(
        'bid_columns',
        'is_default',
        { transaction }
      );

      await queryInterface.addColumn(
        'bid_columns',
        'is_automatic',
        {
          defaultValue: false,
          type: Sequelize.BOOLEAN,
        },
        { transaction }
      );
    }),
  down: async (queryInterface, Sequelize) => queryInterface
    .sequelize
    .transaction(async (transaction) => {
      await queryInterface.addColumn(
        'bid_columns',
        'is_default',
        {
          defaultValue: false,
          type: Sequelize.BOOLEAN,
        },
        { transaction }
      );

      await queryInterface.removeColumn(
        'bid_columns',
        'is_automatic',
        { transaction }
      );
    }),
};
