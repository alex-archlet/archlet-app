module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      'projects',
      'external_project_id',
      {
        allowNull: true,
        type: Sequelize.STRING,
      }
    );
  },
  down: () => Promise.resolve(),
};
