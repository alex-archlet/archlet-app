// we need the order of items so that the IDs are consistent in every environment
const ITEM_ORDER = [
  'item',
  'volume',
  'unit',
  'price',
  'currency',
  'price_total',
  'number',
  'single_choice',
  'text',
  'buyer_bundle',
  'supplier_bundle',
  'price_historic',
  'currency_historic',
  'supplier_historic',
  'supplier',
  'location',
  'item_type',
  'capacity',
  'contributing_to',
  'discounted_by',
];

const COUNTER_VALUE = ITEM_ORDER.length;

function setNewIdValues(queryInterface, type, transaction) {
  const index = ITEM_ORDER.indexOf(type) + 1;

  const query = `
    UPDATE column_types
    SET new_id = ${index}
    WHERE type = '${type}'
  `;

  return queryInterface.sequelize.query(query, { transaction });
}

module.exports = {
  up: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await Promise.all(ITEM_ORDER.map(item => setNewIdValues(queryInterface, item, transaction)));

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
    await queryInterface.sequelize.query('CREATE SEQUENCE column_types_id_seq OWNED BY column_types.new_id;');
    // TODO: what value should we set it to?
    await queryInterface.sequelize.query(`SELECT setval('column_types_id_seq', ${COUNTER_VALUE}, true);`);
    await queryInterface.sequelize.query('ALTER TABLE column_types ALTER COLUMN new_id SET DEFAULT nextval(\'column_types_id_seq\');');
  },
  down: async () => {},
};
