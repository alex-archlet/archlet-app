module.exports = {
  up(queryInterface) {
    return queryInterface.sequelize.query('UPDATE public."jobs" SET progress = 100 WHERE status_id = 5');
  },
  down() {},
};
