const { types } = require('../seeders/20200309084929-add-column-types');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.addColumn(
        'column_types',
        'input_by',
        {
          type: Sequelize.STRING,
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.removeConstraint('demands', 'demands_currency_id_fkey', { transaction });
      await queryInterface.removeColumn('demands', 'currency_id', { transaction });
      await queryInterface.addColumn(
        'demands',
        'currency',
        {
          type: Sequelize.INTEGER,
          references: {
            model: 'project_currencies',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'demands',
        'unit',
        {
          type: Sequelize.INTEGER,
          references: {
            model: 'project_units',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.renameColumn('offers', 'total_price', 'price_total', { transaction });

      await queryInterface.changeColumn(
        'offers',
        'currency',
        {
          type: 'INTEGER USING CAST("currency" as INTEGER)',
        },
        { transaction }
      );
      await queryInterface.addConstraint(
        'offers',
        ['currency'],
        {
          type: 'FOREIGN KEY',
          name: 'offers_currency_fkey',
          references: {
            table: 'project_currencies',
            field: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
          transaction,
        }
      );

      const promises = types.map(replacements => queryInterface.sequelize.query(`
        UPDATE column_types
        SET input_by = :input_by
        WHERE type = :type
      `, {
        replacements,
        transaction,
      }));

      await Promise.all(promises);

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeConstraint('offers', 'offers_currency_fkey', { transaction });
      await queryInterface.changeColumn(
        'offers',
        'currency',
        {
          type: Sequelize.STRING,
        },
        { transaction }
      );

      await queryInterface.renameColumn('offers', 'price_total', 'total_price', { transaction });

      await queryInterface.removeConstraint('demands', 'demands_unit_fkey', { transaction });
      await queryInterface.removeColumn('demands', 'unit', { transaction });
      await queryInterface.removeColumn('demands', 'currency', { transaction });

      await queryInterface.addColumn(
        'demands',
        'currency_id',
        {
          type: Sequelize.INTEGER,
          references: {
            model: 'project_currencies',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.removeColumn('column_types', 'input_by', { transaction });

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
};
