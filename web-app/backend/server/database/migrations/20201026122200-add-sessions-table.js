module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.createTable(
        'sessions',
        {
          sid: {
            allowNull: false,
            primaryKey: true,
            type: Sequelize.STRING,
          },
          expires: {
            allowNull: true,
            type: Sequelize.DATE,
          },
          data: {
            allowNull: true,
            type: Sequelize.TEXT,
          },
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'sessions',
        'user_id',
        {
          type: Sequelize.UUID,
          allowNull: true,
          references: {
            model: 'users',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE',
        },
        { transaction }
      );

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: async queryInterface => queryInterface.dropTable('sessions'),
};
