module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      'column_types',
      'new_id',
      {
        allowNull: false,
        type: Sequelize.INTEGER,
        defaultValue: 1,
      }
    );
  },
  down: () => Promise.resolve(),
};
