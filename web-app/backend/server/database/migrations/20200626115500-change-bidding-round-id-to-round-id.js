module.exports = {
  up: async queryInterface => queryInterface
    .sequelize
    .transaction(async (transaction) => {
      await queryInterface.renameColumn(
        'optimization_files',
        'bidding_round_id',
        'round_id',
        { transaction }
      );
      await queryInterface.renameColumn(
        'jobs',
        'bidding_round_id',
        'round_id',
        { transaction }
      );
      await queryInterface.renameColumn(
        'allocations',
        'bidding_round_id',
        'round_id',
        { transaction }
      );
    }),
  down: async queryInterface => queryInterface
    .sequelize
    .transaction(async (transaction) => {
      await queryInterface.renameColumn(
        'optimization_files',
        'round_id',
        'bidding_round_id',
        { transaction }
      );
      await queryInterface.renameColumn(
        'jobs',
        'round_id',
        'bidding_round_id',
        { transaction }
      );
      await queryInterface.renameColumn(
        'allocations',
        'round_id',
        'bidding_round_id',
        { transaction }
      );
    }),
};
