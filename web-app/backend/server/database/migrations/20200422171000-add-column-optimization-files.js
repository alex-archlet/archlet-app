module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.addColumn(
      'optimization_files',
      'matchings', {
        allowNull: true,
        type: Sequelize.JSONB,
      }
    );
  },
  down(queryInterface) {
    return queryInterface.removeColumn(
      'optimization_files',
      'matchings'
    );
  },
};
