module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.createTable(
        'project_kpi_defs',
        {
          id: {
            allowNull: false,
            primaryKey: true,
            type: Sequelize.UUID,
            defaultValue: Sequelize.literal('uuid_generate_v4()'),
          },
          name: {
            allowNull: false,
            type: Sequelize.STRING,
            defaultValue: '',
          },
          description: {
            allowNull: false,
            type: Sequelize.TEXT,
            defaultValue: '',
          },
          calculation: {
            allowNull: false,
            type: Sequelize.TEXT,
            defaultValue: '',
          },
          is_default: {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
          },
          created_at: {
            allowNull: false,
            type: Sequelize.DATE,
          },
          updated_at: {
            allowNull: false,
            type: Sequelize.DATE,
          },
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'project_kpi_defs',
        'project_id',
        {
          type: Sequelize.UUID,
          references: {
            model: 'projects',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: async queryInterface => queryInterface.dropTable('project_kpi_defs'),
};
