module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn(
      'discounts',
      'discount_type'
    );

    await queryInterface.addColumn(
      'discounts',
      'discount_type',
      {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'discount_types',
          key: 'id',
        },
      }
    );

    await queryInterface.removeColumn(
      'discounts',
      'unit'
    );

    await queryInterface.addColumn(
      'discounts',
      'discount_unit',
      {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'discount_units',
          key: 'id',
        },
      }
    );
  },
  down: () => Promise.resolve(),
};
