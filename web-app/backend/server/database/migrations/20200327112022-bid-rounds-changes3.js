const { types } = require('../seeders/20200309084929-add-column-types');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      const promises = types.map(replacements => queryInterface.sequelize.query(`
        UPDATE column_types
        SET is_unique = :is_unique
        WHERE type = :type
      `, {
        replacements,
        transaction,
      }));

      await Promise.all(promises);

      await queryInterface.addColumn(
        'bidding_rounds',
        'deleted_at',
        {
          type: Sequelize.DATE,
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'optimization_files',
        'deleted_at',
        {
          type: Sequelize.DATE,
        },
        { transaction }
      );

      const replacements = { deleted_at: new Date() };

      await queryInterface.sequelize.query(`
        UPDATE optimization_files
        SET deleted_at = :deleted_at
        WHERE is_deleted = true
      `, {
        replacements,
        transaction,
      });

      await queryInterface.removeColumn('optimization_files', 'is_deleted', { transaction });

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.addColumn(
        'optimization_files',
        'is_deleted',
        {
          type: Sequelize.BOOLEAN,
        },
        { transaction }
      );

      await queryInterface.sequelize.query(`
        UPDATE optimization_files
        SET is_deleted = true
        WHERE deleted_at is not null
      `, {
        transaction,
      });

      await queryInterface.removeColumn('optimization_files', 'deleted_at', { transaction });

      await queryInterface.removeColumn('bidding_rounds', 'deleted_at', { transaction });

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
};
