module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn(
      'discounts',
      'price_component'
    );

    await queryInterface.addColumn(
      'discounts',
      'price_component',
      {
        type: Sequelize.UUID,
        allowNull: true,
        references: {
          model: 'bid_columns',
          key: 'id',
        },
      }
    );
  },
  down: () => Promise.resolve(),
};
