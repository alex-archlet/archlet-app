module.exports = {
  up: async (queryInterface) => {
    await queryInterface.removeConstraint('scenario_kpis', 'scenario_kpis_allocation_id_fkey');
    await queryInterface.removeConstraint('scenario_kpis', 'scenario_kpis_project_kpi_def_id_fkey');
    await queryInterface.removeConstraint('scenario_kpis', 'scenario_kpis_scenario_id_fkey');

    await queryInterface.addConstraint(
      'scenario_kpis',
      {
        fields: ['allocation_id'],
        type: 'FOREIGN KEY',
        references: {
          table: 'allocations',
          field: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      }
    );
    await queryInterface.addConstraint(
      'scenario_kpis',
      {
        fields: ['project_kpi_def_id'],
        type: 'FOREIGN KEY',
        references: {
          table: 'project_kpi_defs',
          field: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      }
    );
    await queryInterface.addConstraint(
      'scenario_kpis',
      {
        fields: ['scenario_id'],
        type: 'FOREIGN KEY',
        references: {
          table: 'scenarios',
          field: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      }
    );
  },
  down: Promise.resolve,
};
