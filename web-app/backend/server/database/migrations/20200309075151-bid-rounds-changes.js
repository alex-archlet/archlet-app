module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.addColumn(
        'column_types',
        'example',
        {
          type: Sequelize.STRING,
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.removeConstraint('bid_columns', 'bid_columns_input_by_fkey', { transaction });

      await queryInterface.changeColumn(
        'bid_columns',
        'input_by',
        {
          type: Sequelize.STRING,
          allowNull: false,
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'bid_columns',
        'order',
        {
          type: Sequelize.INTEGER,
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'bid_columns',
        'limit_min',
        {
          type: Sequelize.FLOAT,
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'bid_columns',
        'limit_max',
        {
          type: Sequelize.FLOAT,
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'bid_columns',
        'single_choice',
        {
          type: Sequelize.ARRAY(Sequelize.TEXT),
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.removeColumn('demands', 'currency', { transaction });

      await queryInterface.addColumn(
        'demands',
        'currency_id',
        {
          type: Sequelize.INTEGER,
          references: {
            model: 'project_currencies',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeColumn('demands', 'currency_id', { transaction });

      await queryInterface.addColumn(
        'demands',
        'currency',
        {
          type: Sequelize.STRING,
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.removeColumn('bid_columns', 'single_choice', { transaction });
      await queryInterface.removeColumn('bid_columns', 'limit_max', { transaction });
      await queryInterface.removeColumn('bid_columns', 'limit_min', { transaction });
      await queryInterface.removeColumn('bid_columns', 'order', { transaction });

      await queryInterface.changeColumn(
        'bid_columns',
        'input_by',
        {
          type: 'INTEGER USING CAST("input_by" as INTEGER)',
        },
        { transaction }
      );

      await queryInterface.addConstraint(
        'bid_columns',
        ['input_by'],
        {
          type: 'FOREIGN KEY',
          name: 'bid_columns_input_by_fkey',
          references: {
            table: 'users',
            field: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
          transaction,
        }
      );

      await queryInterface.removeColumn('column_types', 'example', { transaction });

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
};
