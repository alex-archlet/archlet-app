module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.createTable(
        'scenarios',
        {
          id: {
            allowNull: false,
            primaryKey: true,
            type: Sequelize.UUID,
            defaultValue: Sequelize.literal('uuid_generate_v4()'),
          },
          invalidate: {
            allowNull: false,
            type: Sequelize.BOOLEAN,
          },
          is_default: {
            defaultValue: false,
            type: Sequelize.BOOLEAN,
          },
          json: {
            defaultValue: null,
            type: Sequelize.JSONB,
          },
          last_optimization: {
            defaultValue: null,
            type: Sequelize.DATE,
          },
          name: {
            allowNull: false,
            type: Sequelize.STRING,
          },
          optimization_algorithm: {
            allowNull: false,
            type: Sequelize.STRING,
          },
          status: {
            allowNull: false,
            type: Sequelize.STRING,
          },
          created_at: {
            allowNull: false,
            type: Sequelize.DATE,
          },
          updated_at: {
            allowNull: false,
            type: Sequelize.DATE,
          },
        },
        { transaction }
      );

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.dropTable('scenarios', { transaction });
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
};
