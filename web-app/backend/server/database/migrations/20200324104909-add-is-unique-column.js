const { types } = require('../seeders/20200309084929-add-column-types');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.addColumn(
        'column_types',
        'is_unique',
        {
          type: Sequelize.BOOLEAN,
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      const promises = types.map(replacements => queryInterface.sequelize.query(`
        UPDATE column_types
        SET is_unique = :is_unique
        WHERE type = :type
      `, {
        replacements,
        transaction,
      }));

      await Promise.all(promises);

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeColumn('column_types', 'is_unique', { transaction });

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
};
