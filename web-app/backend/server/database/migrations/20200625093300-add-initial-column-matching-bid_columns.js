module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      'bid_columns',
      'initial_matching_confidence',
      {
        allowNull: true,
        type: Sequelize.FLOAT,
      }
    );
  },
  down: async (queryInterface) => {
    await queryInterface.removeColumn(
      'bid_columns',
      'initial_matching_confidence'
    );
  },
};

