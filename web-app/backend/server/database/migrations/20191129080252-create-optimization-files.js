module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.createTable(
        'optimization_files',
        {
          id: {
            allowNull: false,
            primaryKey: true,
            type: Sequelize.UUID,
            defaultValue: Sequelize.literal('uuid_generate_v4()'),
          },
          content_hash: {
            allowNull: false,
            type: Sequelize.STRING,
          },
          description: {
            type: Sequelize.STRING,
          },
          file_extension: {
            allowNull: false,
            type: Sequelize.STRING,
          },
          file_path: {
            allowNull: false,
            type: Sequelize.STRING,
          },
          file_type: {
            allowNull: false,
            type: Sequelize.STRING,
          },
          file_version: {
            defaultValue: null,
            type: Sequelize.INTEGER,
          },
          is_deleted: {
            allowNull: false,
            defaultValue: false,
            type: Sequelize.BOOLEAN,
          },
          json: {
            defaultValue: null,
            type: Sequelize.JSONB,
          },
          name: {
            allowNull: false,
            type: Sequelize.STRING,
          },
          created_at: {
            allowNull: false,
            type: Sequelize.DATE,
          },
          updated_at: {
            allowNull: false,
            type: Sequelize.DATE,
          },
        },
        { transaction }
      );

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.dropTable('optimization_files', { transaction });
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
};
