module.exports = {
  up: async (queryInterface, Sequelize) => {
    const tableDefinition = await queryInterface.describeTable('offers');

    if (!tableDefinition.is_historic) {
      queryInterface.addColumn(
        'offers',
        'is_historic',
        {
          defaultValue: false,
          type: Sequelize.BOOLEAN,
        }
      );
    }
  },
  down: (queryInterface, Sequelize) => (
    queryInterface.removeColumn(
      'offers',
      'is_historic',
      {
        defaultValue: false,
        type: Sequelize.BOOLEAN,
      }
    )
  ),
};
