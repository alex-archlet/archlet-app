// this method of adding indexes is used because
// we aren't using sequelize.sync(), which would
// create it from the model attributes

module.exports = {
  up: async (queryInterface) => {
    // project_kpi_defs
    await queryInterface.addIndex(
      'project_kpi_defs',
      {
        fields: ['project_id'],
      }
    );
    // scenario_kpis
    await queryInterface.addIndex(
      'scenario_kpis',
      {
        fields: ['project_kpi_def_id'],
      }
    );
    await queryInterface.addIndex(
      'scenario_kpis',
      {
        fields: ['allocation_id'],
      }
    );
    await queryInterface.addIndex(
      'scenario_kpis',
      {
        fields: ['scenario_id'],
      }
    );
  },
  down: Promise.resolve,
};
