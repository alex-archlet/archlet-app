module.exports = {
  up: async (queryInterface, Sequelize) => {
    const tableDefinition = await queryInterface.describeTable('allocations');

    if (!tableDefinition.is_manual) {
      queryInterface.addColumn(
        'allocations',
        'is_manual',
        {
          defaultValue: false,
          type: Sequelize.BOOLEAN,
        }
      );
    }
  },
  down: (queryInterface, Sequelize) => (
    queryInterface.removeColumn(
      'allocations',
      'is_manual',
      {
        defaultValue: false,
        type: Sequelize.BOOLEAN,
      }
    )
  ),
};
