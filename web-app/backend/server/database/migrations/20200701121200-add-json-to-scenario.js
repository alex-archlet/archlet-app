module.exports = {
  up: async (queryInterface, Sequelize) => {
    const tableDefinition = await queryInterface.describeTable('scenarios');

    if (!tableDefinition.output) {
      queryInterface.addColumn(
        'scenarios',
        'output',
        {
          allowNull: true,
          type: Sequelize.JSONB,
        }
      );
    }
  },
  down: (queryInterface, Sequelize) => (
    queryInterface.removeColumn(
      'scenarios',
      'output',
      {
        allowNull: true,
        type: Sequelize.JSONB,
      }
    )
  ),
};
