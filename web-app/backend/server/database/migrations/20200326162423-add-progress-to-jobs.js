module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.addColumn(
      'jobs',
      'progress',
      {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
      }
    );
  },
  down(queryInterface) {
    return queryInterface.removeColumn(
      'jobs',
      'progress'
    );
  },
};
