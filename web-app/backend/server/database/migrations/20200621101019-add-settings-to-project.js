module.exports = {
  up: async (queryInterface, Sequelize) => queryInterface
    .sequelize
    .transaction(async (transaction) => {
      await queryInterface.addColumn(
        'projects',
        'settings',
        {
          allowNull: true,
          type: Sequelize.JSONB,
        },
        { transaction }
      );
    }),
  down: async queryInterface => queryInterface
    .sequelize
    .transaction(async (transaction) => {
      await queryInterface.removeColumn(
        'projects',
        'settings',
        { transaction }
      );
    }),
};
