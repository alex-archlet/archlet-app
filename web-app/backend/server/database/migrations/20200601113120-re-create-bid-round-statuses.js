module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.bulkDelete(
        'bidding_rounds',
        {
          created_at: {
            [Sequelize.Op.not]: null,
          },
        }
      );

      await queryInterface.removeConstraint('bidding_rounds', 'bidding_rounds_status_id_fkey', { transaction });

      await queryInterface.removeColumn(
        'bidding_rounds',
        'status_id',
        { transaction }
      );

      await queryInterface.dropTable('bid_round_statuses', { transaction });

      await queryInterface.createTable(
        'bid_round_statuses',
        {
          id: {
            allowNull: false,
            primaryKey: true,
            type: Sequelize.INTEGER,
            autoIncrement: true,
          },
          name: {
            allowNull: false,
            type: Sequelize.STRING,
          },
          created_at: {
            allowNull: false,
            type: Sequelize.DATE,
          },
          updated_at: {
            allowNull: false,
            type: Sequelize.DATE,
          },
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'bidding_rounds',
        'status_id',
        {
          type: Sequelize.INTEGER,
          defaultValue: 1,
          references: {
            model: 'bid_round_statuses',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.dropTable('bid_round_statuses', { transaction });
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
};
