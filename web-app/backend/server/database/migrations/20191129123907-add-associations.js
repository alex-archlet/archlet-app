module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.addColumn(
        'branches',
        'company_id',
        {
          type: Sequelize.UUID,
          references: {
            model: 'companies',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'users',
        'branch_id',
        {
          allowNull: false,
          type: Sequelize.UUID,
          references: {
            model: 'branches',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'project_categories',
        'type_id',
        {
          type: Sequelize.INTEGER,
          references: {
            model: 'project_types',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'projects',
        'category_id',
        {
          type: Sequelize.INTEGER,
          references: {
            model: 'project_categories',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'scenarios',
        'project_id',
        {
          type: Sequelize.UUID,
          references: {
            model: 'projects',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addConstraint(
        'project_accesses',
        ['user_id'],
        {
          type: 'FOREIGN KEY',
          name: 'project_accesses_user_id_fkey',
          references: {
            table: 'users',
            field: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
          transaction,
        }
      );

      await queryInterface.addConstraint(
        'project_accesses',
        ['project_id'],
        {
          type: 'FOREIGN KEY',
          name: 'project_accesses_project_id_fkey',
          references: {
            table: 'projects',
            field: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
          transaction,
        }
      );

      await queryInterface.addColumn(
        'jobs',
        'scenario_id',
        {
          type: Sequelize.UUID,
          references: {
            model: 'scenarios',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'translators',
        'branch_id',
        {
          allowNull: false,
          type: Sequelize.UUID,
          references: {
            model: 'branches',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'translators',
        'category_id',
        {
          type: Sequelize.INTEGER,
          references: {
            model: 'project_categories',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'translators',
        'optimization_file_id',
        {
          type: Sequelize.UUID,
          references: {
            model: 'optimization_files',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'optimization_files',
        'project_id',
        {
          type: Sequelize.UUID,
          references: {
            model: 'projects',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'optimization_files',
        'scenario_id',
        {
          type: Sequelize.UUID,
          references: {
            model: 'scenarios',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'jobs',
        'project_id',
        {
          type: Sequelize.UUID,
          references: {
            model: 'projects',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'jobs',
        'type_id',
        {
          type: Sequelize.INTEGER,
          references: {
            model: 'job_types',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'jobs',
        'status_id',
        {
          type: Sequelize.INTEGER,
          references: {
            model: 'job_statuses',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeColumn('branches', 'company_id', { transaction });
      await queryInterface.removeColumn('users', 'branch_id', { transaction });
      await queryInterface.removeColumn('project_categories', 'type_id', { transaction });
      await queryInterface.removeColumn('projects', 'category_id', { transaction });
      await queryInterface.removeColumn('scenarios', 'project_id', { transaction });
      await queryInterface.removeConstraint('project_accesses', 'project_accesses_user_id_fkey', { transaction });
      await queryInterface.removeConstraint('project_accesses', 'project_accesses_project_id_fkey', { transaction });
      await queryInterface.removeColumn('scenario_actions', 'scenario_id', { transaction });
      await queryInterface.removeColumn('scenario_comments', 'scenario_id', { transaction });
      await queryInterface.removeColumn('jobs', 'scenario_id', { transaction });
      await queryInterface.removeColumn('scenario_versions', 'scenario_id', { transaction });
      await queryInterface.removeColumn('translators', 'branch_id', { transaction });
      await queryInterface.removeColumn('translators', 'category_id', { transaction });
      await queryInterface.removeColumn('translators', 'optimization_file_id', { transaction });
      await queryInterface.removeColumn('optimization_files', 'project_id', { transaction });
      await queryInterface.removeColumn('optimization_files', 'scenario_id', { transaction });
      await queryInterface.removeColumn('jobs', 'project_id', { transaction });
      await queryInterface.removeColumn('jobs', 'type_id', { transaction });
      await queryInterface.removeColumn('jobs', 'status_id', { transaction });

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
};
