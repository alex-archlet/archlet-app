module.exports = {
  up: async (queryInterface, Sequelize) => queryInterface.addColumn(
    'bid_columns',
    'project_id',
    {
      type: Sequelize.UUID,
      references: {
        model: 'projects',
        key: 'id',
      },
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL',
      allowNull: true,
    }
  ),
  down: async queryInterface => queryInterface.removeColumn('bid_columns', 'project_id'),
};
