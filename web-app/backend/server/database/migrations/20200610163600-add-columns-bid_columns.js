module.exports = {
  up: async (queryInterface, Sequelize) => queryInterface
    .sequelize
    .transaction(async (transaction) => {
      await queryInterface.addColumn(
        'bid_columns',
        'initial_type_id',
        {
          allowNull: true,
          type: Sequelize.UUID,
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'bid_columns',
        'matching_confidence',
        {
          allowNull: true,
          type: Sequelize.FLOAT,
        },
        { transaction }
      );

      await queryInterface.sequelize.query('UPDATE public.bid_columns set "initial_type_id" = "type_id"', { transaction });
    }),
  down: async queryInterface => queryInterface
    .sequelize
    .transaction(async (transaction) => {
      await queryInterface.removeColumn(
        'bid_columns',
        'initial_type_id',
        { transaction }
      );

      await queryInterface.removeColumn(
        'bid_columns',
        'matching_confidence',
        { transaction }
      );
    }),
};
