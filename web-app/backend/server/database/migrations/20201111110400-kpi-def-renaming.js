module.exports = {
  up: async (queryInterface) => {
    await queryInterface.sequelize.query(`UPDATE project_kpi_defs
                                          SET calculation = REPLACE(calculation, 'allocations.''volume''', 'allocations.''total_volume''')
                                          WHERE calculation like '%allocations%'`);

    await queryInterface.sequelize.query(`UPDATE project_kpi_defs
                                          SET calculation = REPLACE(calculation,
                                                                    'allocations.''discount_amounts''',
                                                                    'allocations.''discount_amount''')
                                          WHERE calculation like '%allocations%'`);
  },
  down: () => Promise.resolve(),
};
