module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.addColumn(
        'allocations',
        'bidding_round_id',
        {
          type: Sequelize.UUID,
          references: {
            model: 'bidding_rounds',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'bidding_rounds',
        'branch_id',
        {
          allowNull: false,
          type: Sequelize.UUID,
          references: {
            model: 'branches',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.removeColumn(
        'companies',
        'json',
        { transaction }
      );

      await queryInterface.addColumn(
        'demands',
        'branch_id',
        {
          allowNull: false,
          type: Sequelize.UUID,
          references: {
            model: 'branches',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'jobs',
        'bidding_round_id',
        {
          type: Sequelize.UUID,
          references: {
            model: 'bidding_rounds',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'jobs',
        'branch_id',
        {
          allowNull: false,
          type: Sequelize.UUID,
          references: {
            model: 'branches',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'jobs',
        'user_id',
        {
          type: Sequelize.UUID,
          references: {
            model: 'users',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.removeColumn(
        'optimization_files',
        'json',
        { transaction }
      );

      await queryInterface.addColumn(
        'optimization_files',
        'bidding_round_id',
        {
          type: Sequelize.UUID,
          references: {
            model: 'bidding_rounds',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'optimization_files',
        'branch_id',
        {
          allowNull: false,
          type: Sequelize.UUID,
          references: {
            model: 'branches',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.removeColumn(
        'projects',
        'json',
        { transaction }
      );

      await queryInterface.removeColumn(
        'scenarios',
        'optimization_algorithm',
        { transaction }
      );

      await queryInterface.addColumn(
        'scenarios',
        'branch_id',
        {
          allowNull: false,
          type: Sequelize.UUID,
          references: {
            model: 'branches',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        },
        { transaction }
      );

      await queryInterface.removeColumn(
        'suppliers',
        'json',
        { transaction }
      );

      await queryInterface.removeColumn(
        'translators',
        'json',
        { transaction }
      );

      await queryInterface.removeColumn(
        'users',
        'json',
        { transaction }
      );

      await queryInterface.removeColumn(
        'users',
        'notes',
        { transaction }
      );

      await queryInterface.removeColumn(
        'users',
        'preferences',
        { transaction }
      );

      return transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: async (queryInterface, Sequelize) => {
    const jsonColumn = {
      defaultValue: null,
      type: Sequelize.JSONB,
    };
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeColumn(
        'allocations',
        'bidding_round_id',
        { transaction }
      );

      await queryInterface.removeColumn(
        'bidding_rounds',
        'branch_id',
        { transaction }
      );

      await queryInterface.addColumn(
        'companies',
        'json',
        jsonColumn,
        { transaction }
      );

      await queryInterface.removeColumn(
        'demands',
        'branch_id',
        { transaction }
      );

      await queryInterface.removeColumn(
        'jobs',
        'bidding_round_id',
        { transaction }
      );

      await queryInterface.removeColumn(
        'jobs',
        'branch_id',
        { transaction }
      );

      await queryInterface.removeColumn(
        'jobs',
        'user_id',
        { transaction }
      );

      await queryInterface.addColumn(
        'optimization_files',
        'json',
        jsonColumn,
        { transaction }
      );

      await queryInterface.removeColumn(
        'optimization_files',
        'bidding_round_id',
        { transaction }
      );

      await queryInterface.removeColumn(
        'optimization_files',
        'branch_id',
        { transaction }
      );

      await queryInterface.addColumn(
        'projects',
        'json',
        jsonColumn,
        { transaction }
      );

      await queryInterface.removeColumn(
        'scenarios',
        'optimization_algorithm',
        {
          type: Sequelize.STRING,
        },
        { transaction }
      );

      await queryInterface.removeColumn(
        'scenarios',
        'branch_id',
        { transaction }
      );

      await queryInterface.addColumn(
        'suppliers',
        'json',
        jsonColumn,
        { transaction }
      );

      await queryInterface.addColumn(
        'translators',
        'json',
        jsonColumn,
        { transaction }
      );

      await queryInterface.addColumn(
        'users',
        'json',
        jsonColumn,
        { transaction }
      );

      await queryInterface.removeColumn(
        'users',
        'notes',
        {
          type: Sequelize.STRING,
        },
        { transaction }
      );

      await queryInterface.removeColumn(
        'users',
        'preferences',
        jsonColumn,
        { transaction }
      );

      return transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
};
