const names = [
  'total_spend',
  'savings',
  'volume',
  'number_of_changes',
  'items_allocated',
  'winning_bidders',
];

module.exports = {
  up: async (queryInterface) => {
    const toInsert = names.map(name => ({
      name,
      is_default: true,
      created_at: new Date(),
      updated_at: new Date(),
    }));

    return queryInterface.bulkInsert('project_kpi_defs', toInsert);
  },
  down: Promise.resolve,
};
