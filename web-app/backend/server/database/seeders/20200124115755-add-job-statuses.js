const statuses = ['pending', 'running', 'canceled', 'aborted', 'succeeded'];

module.exports = {
  up: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      const inDB = await queryInterface.rawSelect(
        'job_statuses',
        {
          where: { name: statuses },
          transaction,
        },
        ['id']
      );

      if (!inDB) {
        const statusesRow = statuses.map(status => ({
          name: status,
          created_at: new Date(),
          updated_at: new Date(),
        }));

        await queryInterface.bulkInsert('job_statuses', statusesRow, { transaction });
      }

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.bulkDelete(
        'job_statuses', {
          name: statuses,
        },
        { transaction }
      );

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
};
