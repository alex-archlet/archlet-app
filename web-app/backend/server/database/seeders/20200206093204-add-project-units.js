const units = [
  {
    code: 'kg',
    name: 'Kilograms',
  },
  {
    code: 'ton',
    name: 'Tons',
  },
  {
    code: 'm',
    name: 'Meters',
  },
  {
    code: 'l',
    name: 'Liters',
  },
  {
    code: 'm3',
    name: 'Cubic Meters',
  },
  {
    code: 'ftl',
    name: 'Full Containers',
  },
  {
    code: 'ltl',
    name: 'Partial Containers',
  },
  {
    code: 'pallets',
    name: 'Pallets',
  },
  {
    code: 'quantity',
    name: 'Quantity',
  },
];
const codes = units.map(unit => unit.code);

module.exports = {
  up: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      const inDB = await queryInterface.rawSelect(
        'project_units',
        {
          where: {
            code: codes,
          },
          transaction,
        },
        ['id']
      );

      if (!inDB) {
        const unitRows = units.map(({
          code,
          name,
        }) => ({
          code,
          name,
          created_at: new Date(),
          updated_at: new Date(),
        }));

        await queryInterface.bulkInsert('project_units', unitRows, { transaction });
      }

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.bulkDelete(
        'project_units',
        {
          code: codes,
        },
        { transaction }
      );

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
};
