const flagNames = ['Analytics Pivot Table visible'];

module.exports = {
  up: async (queryInterface) => {
    const statusRows = flagNames.map(name => ({
      name,
      created_at: new Date(),
      updated_at: new Date(),
    }));

    return queryInterface.bulkInsert('feature_flag_types', statusRows);
  },
  down: Promise.resolve,
};
