const types = ['column_matching', 'expression_evaluation'];

module.exports = {
  up: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      const inDB = await queryInterface.rawSelect(
        'job_types',
        {
          where: { name: types },
          transaction,
        },
        ['id']
      );

      if (!inDB) {
        const typesRow = types.map(status => ({
          name: status,
          created_at: new Date(),
          updated_at: new Date(),
        }));

        await queryInterface.bulkInsert('job_types', typesRow, { transaction });
      }

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.bulkDelete(
        'job_types',
        {
          name: types,
        },
        { transaction }
      );

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
};
