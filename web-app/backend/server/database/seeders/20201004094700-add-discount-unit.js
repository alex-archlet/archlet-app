const discountUnits = ['spend', 'volume'];

module.exports = {
  up: async (queryInterface) => {
    const discountUnitsRow = discountUnits.map(discountUnit => ({
      name: discountUnit,
      created_at: new Date(),
      updated_at: new Date(),
    }));

    await queryInterface.bulkInsert('discount_units', discountUnitsRow);
  },
  down: async queryInterface => queryInterface.bulkDelete('discount_units', {
    name: discountUnits,
  }),
};
