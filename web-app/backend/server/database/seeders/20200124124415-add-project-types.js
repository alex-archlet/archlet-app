const types = ['Materials', 'Logistics', 'Manufactured Goods', 'Services'];

module.exports = {
  up: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      const inDB = await queryInterface.rawSelect(
        'project_types',
        {
          where: { name: types },
          transaction,
        },
        ['id']
      );

      if (!inDB) {
        const typeRows = types.map(type => ({
          name: type,
          created_at: new Date(),
          updated_at: new Date(),
        }));

        await queryInterface.bulkInsert('project_types', typeRows, { transaction });
      }

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.bulkDelete(
        'project_types',
        {
          name: types,
        },
        { transaction }
      );

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
};
