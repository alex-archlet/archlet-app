const company = {
  name: 'Archlet Capa',
  created_at: new Date(),
  updated_at: new Date(),
};
const branch = {
  account_type: 'admin',
  name: 'Archlet Capa',
  location: '',
  json: {
    file_types: {
      offers: {
        name: 'Offers',
        example: null,
        mandatory: true,
      },
      demands: {
        name: 'Demands',
        example: null,
        mandatory: true,
      },
      capacities: {
        name: 'Capacities',
        example: null,
        mandatory: false,
      },
    },
  },
  created_at: new Date(),
  updated_at: new Date(),
};
const user = {
  account_type: 'admin',
  email: 'capa@archlet.ch',
  username: 'capa@archlet.ch',
  first_name: 'Test',
  last_name: 'User',
  password_hash: '$2b$11$TrTqYecpmZRYFf5OOnL31upgyPxgS.r6SlVmO/AL4.rF3mnb1OWKi',
  password_salt: '$2b$11$TrTqYecpmZRYFf5OOnL31u',
  created_at: new Date(),
  updated_at: new Date(),
};
const file = {
  content_hash: '',
  description: 'Capa translator',
  file_extension: '.json',
  file_path: 'translators',
  file_type: 'Json',
  file_version: 0,
  name: 'archlet_capa_translator',
  created_at: new Date(),
  updated_at: new Date(),
};
const translator = {
  name: 'Capa Translator',
  created_at: new Date(),
  updated_at: new Date(),
};

const getId = async (queryInterface, table, name, transaction, column = 'name') => {
  const id = await queryInterface.rawSelect(
    table,
    {
      where: { [column]: name },
      transaction,
    },
    ['id']
  );

  return id;
};

module.exports = {
  up: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      const adminRoleId = await getId(queryInterface, 'roles', 'Admin', transaction);
      const translatorId = await getId(queryInterface, 'translators', translator.name, transaction);
      const categoryId = await getId(queryInterface, 'project_categories', 'Raw Materials', transaction);
      let userId = await getId(queryInterface, 'users', user.username, transaction, 'username');
      let branchId = await getId(queryInterface, 'branches', branch.name, transaction);
      let fileId = await getId(queryInterface, 'optimization_files', file.name, transaction);
      let companyId = await getId(queryInterface, 'companies', company.name, transaction);

      if (!companyId || !branchId || !userId || !fileId || !translatorId) {
        return Promise.resolve();
      }

      if (!companyId) {
        await queryInterface.bulkInsert(
          'companies',
          [{ ...company }],
          { transaction }
        );
        companyId = await getId(queryInterface, 'companies', company.name, transaction);
      }

      if (!branchId) {
        await queryInterface.bulkInsert(
          'branches',
          [{
            ...branch,
            json: JSON.stringify(branch.json),
            company_id: companyId,
            role_id: adminRoleId,
          }],
          { transaction }
        );
        branchId = await getId(queryInterface, 'branches', branch.name, transaction);
      }

      if (!userId) {
        await queryInterface.bulkInsert(
          'users',
          [{
            ...user,
            branch_id: branchId,
            role_id: adminRoleId,
          }],
          { transaction }
        );
        userId = await getId(queryInterface, 'users', user.username, transaction, 'username');
      }

      if (!fileId) {
        await queryInterface.bulkInsert(
          'optimization_files',
          [{
            ...file,
            branch_id: branchId,
          }],
          { transaction }
        );
        fileId = await getId(queryInterface, 'optimization_files', file.name, transaction);
      }

      if (!translatorId) {
        await queryInterface.bulkInsert(
          'translators',
          [{
            ...translator,
            branch_id: branchId,
            category_id: categoryId,
            optimization_file_id: fileId,
          }],
          { transaction }
        );
      }

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }

    return Promise.resolve();
  },
  down: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.bulkDelete(
        'translators',
        { name: translator.name },
        { transaction }
      );

      await queryInterface.bulkDelete(
        'users',
        { username: user.username },
        { transaction }
      );

      await queryInterface.bulkDelete(
        'optimization_files',
        { name: file.name },
        { transaction }
      );

      await queryInterface.bulkDelete(
        'branches',
        { name: branch.name },
        { transaction }
      );

      await queryInterface.bulkDelete(
        'companies',
        { name: company.name },
        { transaction }
      );

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
};
