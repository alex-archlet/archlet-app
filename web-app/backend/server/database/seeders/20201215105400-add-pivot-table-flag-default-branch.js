const PIVOT_TABLE_FLAG_ID = 1;

module.exports = {
  up: async (queryInterface) => {
    const [[defaultBranch]] = await queryInterface.sequelize.query('select * from public.branches order by created_at asc limit 1');

    if (!defaultBranch) {
      return;
    }

    const flagsToInsert = [{
      branch_id: defaultBranch.id,
      flag_type_id: PIVOT_TABLE_FLAG_ID,
    }];

    await queryInterface.bulkInsert('feature_flags', flagsToInsert);
  },
  down: Promise.resolve,
};
