const discountTypes = ['percentage', 'flat', 'unit'];

module.exports = {
  up: async (queryInterface) => {
    const discountTypesRow = discountTypes.map(discountType => ({
      name: discountType,
      created_at: new Date(),
      updated_at: new Date(),
    }));

    await queryInterface.bulkInsert('discount_types', discountTypesRow);
  },
  down: async queryInterface => queryInterface.bulkDelete('discount_types', {
    name: discountTypes,
  }),
};
