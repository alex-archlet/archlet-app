const types = [
  {
    text: 'Contributing to Discount',
    type: 'contributing_to',
    is_default: false,
    example: 'Supplier Europe 1',
    input_by: 'supplier',
    is_unique: true,
  },
  {
    text: 'Discounted by Discount',
    type: 'discounted_by',
    is_default: false,
    example: 'Supplier Europe 2',
    input_by: 'supplier',
    is_unique: true,
  },
];

const names = types.map(type => type.text);

module.exports = {
  types,
  up: async (queryInterface) => {
    const typesRows = types.map(type => ({
      ...type,
      created_at: new Date(),
      updated_at: new Date(),
    }));

    await queryInterface.bulkInsert('column_types', typesRows);
  },
  down: async (queryInterface) => {
    queryInterface.bulkDelete('column_types', {
      text: names,
    });
  },
};
