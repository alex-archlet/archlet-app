const materialsItems = ['Raw Materials', 'Food Ingredients'];
const logisticsItems = ['Road Freight', 'Air Freight', 'Ocean Freight', 'Parcels'];
const manufacturedGoodsItems = ['Packaging', 'Engineering Products', 'Electronics', 'MRO'];
const servicesItems = ['Temporary Labour', 'Transcription', 'IT', 'Legal'];
const types = [
  {
    name: 'Materials',
    items: materialsItems,
  },
  {
    name: 'Logistics',
    items: logisticsItems,
  },
  {
    name: 'Manufactured Goods',
    items: manufacturedGoodsItems,
  },
  {
    name: 'Services',
    items: servicesItems,
  },
];

module.exports = {
  up: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      const inDB = await queryInterface.rawSelect(
        'project_categories',
        {
          where: {
            name: [
              ...materialsItems,
              ...logisticsItems,
              ...manufacturedGoodsItems,
              ...servicesItems,
            ],
          },
          transaction,
        },
        ['id']
      );

      if (!inDB) {
        await Promise.all(types.map(async (type) => {
          const typeId = await queryInterface.rawSelect(
            'project_types',
            {
              where: { name: type.name },
              transaction,
            },
            ['id']
          );

          if (typeId) {
            const typeRows = type.items.map(item => ({
              name: item,
              type_id: typeId,
              created_at: new Date(),
              updated_at: new Date(),
            }));

            await queryInterface.bulkInsert('project_categories', typeRows, { transaction });
          }
        }));
      }

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.bulkDelete(
        'project_categories',
        {
          name: [
            ...materialsItems,
            ...logisticsItems,
            ...manufacturedGoodsItems,
            ...servicesItems,
          ],
        },
        { transaction }
      );

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
};
