const types = [
  {
    text: 'Location',
    type: 'location',
    is_default: false,
    example: 'Zurich',
    input_by: 'supplier',
    is_unique: false,
  },
  {
    text: 'Item Type',
    type: 'item_type',
    is_default: false,
    example: 'Current Specs',
    input_by: 'supplier',
    is_unique: true,
  },
  {
    text: 'Capacity',
    type: 'capacity',
    is_default: false,
    example: '75',
    input_by: 'supplier',
    is_unique: false,
  },
];

const names = types.map(type => type.text);

const deleteBidColumnsType = ['price_historic', 'currency_historic', 'supplier_historic'];

module.exports = {
  types,
  up: async (queryInterface) => {
    await queryInterface.bulkDelete('column_types', {
      type: deleteBidColumnsType,
    });

    const typesRows = types.map(type => ({
      ...type,
      created_at: new Date(),
      updated_at: new Date(),
    }));

    await queryInterface.bulkInsert('column_types', typesRows);

    await queryInterface.sequelize.query("UPDATE column_types SET is_default = 'false' WHERE type = 'price_total'");
    await queryInterface.sequelize.query("UPDATE column_types SET is_default = 'true' WHERE type = 'supplier'");
    await queryInterface.sequelize.query("UPDATE column_types SET is_default = 'false' WHERE type = 'unit'");
  },
  down: async (queryInterface) => {
    queryInterface.bulkDelete('column_types', {
      text: names,
    });
    await queryInterface.bulkDelete('column_types', {
      type: deleteBidColumnsType,
    });
  },
};
