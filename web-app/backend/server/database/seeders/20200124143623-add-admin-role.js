const permissions = [
  'CAN_CREATE_SCENARIO',
  'CAN_READ_SCENARIO',
  'CAN_DELETE_SCENARIO',
  'CAN_READ_INSIGHTS',
  'CAN_READ_ITEM_DEEP_DIVE',
  'CAN_READ_SUPPLIER_DEEP_DIVE',
  'CAN_READ_SCENARIO_SETTINGS',
];

module.exports = {
  up: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      const inDB = await queryInterface.rawSelect(
        'roles',
        {
          where: { name: 'Admin' },
          transaction,
        },
        ['id']
      );

      if (!inDB) {
        await queryInterface.bulkInsert(
          'roles',
          [{
            permissions: `{ ${permissions.join(', ')} }`,
            name: 'Admin',
            created_at: new Date(),
            updated_at: new Date(),
          }],
          { transaction }
        );
      }

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.bulkDelete(
        'roles',
        {
          name: 'Admin',
        },
        { transaction }
      );

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
};
