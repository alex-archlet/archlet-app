const csv = `
Afghani,AFN,False
Algerian Dinar,DZD,False
US Dollar,USD,True
Euro,EUR,True
East Caribbean Dollar,XCD,False
Argentine Peso,ARS,False
Australian Dollar,AUD,False
Belarusian Ruble,BYN,False
CFA Franc BCEAO,XOF,False
Indian Rupee,INR,True
Boliviano,BOB,False
Norwegian Krone,NOK,False
Brazilian Real,BRL,True
Bulgarian Lev,BGN,False
Riel,KHR,False
CFA Franc BEAC,XAF,False
Canadian Dollar,CAD,True
Chilean Peso,CLP,False
Yuan Renminbi,CNY,True
Colombian Peso,COP,False
New Zealand Dollar,NZD,False
Cuban Peso,CUP,False
Netherlands Antillean Guilder,ANG,False
Czech Koruna,CZK,False
Danish Krone,DKK,False
Egyptian Pound,EGP,False
Ethiopian Birr,ETB,False
Fiji Dollar,FJD,False
CFP Franc,XPF,False
Dalasi,GMD,False
Lari,GEL,False
Ghana Cedi,GHS,False
Gibraltar Pound,GIP,False
Pound Sterling,GBP,True
Hong Kong Dollar,HKD,False
Forint,HUF,False
Rupiah,IDR,False
Iranian Rial,IRR,False
Iraqi Dinar,IQD,False
New Israeli Sheqel,ILS,False
Yen,JPY,True
Won,KRW,False
Rand,ZAR,False
Liberian Dollar,LRD,False
Swiss Franc,CHF,True
Denar,MKD,False
Mexican Peso,MXN,False
Moroccan Dirham,MAD,False
Namibia Dollar,NAD,False
Nepalese Rupee,NPR,False
Naira,NGN,False
Pakistan Rupee,PKR,False
Guarani,PYG,False
Sol,PEN,False
Philippine Peso,PHP,False
Zloty,PLN,False
Qatari Rial,QAR,False
Romanian Leu,RON,False
Russian Ruble,RUB,True
Rwanda Franc,RWF,False
Tala,WST,False
Dobra,STN,False
Saudi Riyal,SAR,False
Serbian Dinar,RSD,False
Seychelles Rupee,SCR,False
Leone,SLL,False
Singapore Dollar,SGD,False
Somali Shilling,SOS,False
South Sudanese Pound,SSP,False
Sri Lanka Rupee,LKR,False
Sudanese Pound,SDG,False
Surinam Dollar,SRD,False
Swedish Krona,SEK,False
Syrian Pound,SYP,False
New Taiwan Dollar,TWD,False
Baht,THB,False
Trinidad and Tobago Dollar,TTD,False
Tunisian Dinar,TND,False
Turkish Lira,TRY,False
Turkmenistan New Manat,TMT,False
Uganda Shilling,UGX,False
Hryvnia,UAH,False
UAE Dirham,AED,False
Peso Uruguayo,UYU,False
Bolivar Soberano,VES,False
Dong,VND,False
Yemeni Rial,YER,False
Zambian Kwacha,ZMW,False
Zimbabwe Dollar,ZWL,False
Gold,XAU,False`;

module.exports = {
  up: async (queryInterface) => {
    const currencies = [];
    const [, ...editedResult] = csv.split('\n');

    editedResult.forEach((row) => {
      const [currencyName, currencyIso, is_main] = row.split(',');

      currencies.push({
        currency_name: currencyName,
        currency_iso: currencyIso,
        is_main,
      });
    });

    return queryInterface.bulkInsert('currencies', currencies.filter(currency => Boolean(currency)));
  },
  down: Promise.resolve,
};
