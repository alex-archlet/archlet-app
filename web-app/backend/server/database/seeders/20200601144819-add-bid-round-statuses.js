const statuses = ['Draft', 'Active', 'Paused', 'Finished'];

module.exports = {
  up: async (queryInterface) => {
    const statusRows = statuses.map(name => ({
      name,
      created_at: new Date(),
      updated_at: new Date(),
    }));

    return queryInterface.bulkInsert('bid_round_statuses', statusRows);
  },
  down: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.bulkDelete(
        'bid_round_statuses',
        {
          name: statuses,
        },
        { transaction }
      );

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
};
