const types = ['expression_example', 'offer_processing'];

module.exports = {
  up: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      const typesRow = types.map(status => ({
        name: status,
        created_at: new Date(),
        updated_at: new Date(),
      }));

      await queryInterface.bulkInsert('job_types', typesRow, { transaction });

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.bulkDelete(
        'job_types',
        {
          name: types,
        },
        { transaction }
      );

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
};
