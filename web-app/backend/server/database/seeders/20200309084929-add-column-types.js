const types = [
  {
    text: 'Item',
    type: 'item',
    is_default: true,
    example: 'Item 0001',
    input_by: 'buyer',
    is_unique: true,
  },
  {
    text: 'Volume',
    type: 'volume',
    is_default: true,
    example: '100',
    input_by: 'buyer',
    is_unique: false,
  },
  {
    text: 'Unit of Measure',
    type: 'unit',
    is_default: true,
    example: 'Tons',
    input_by: 'buyer',
    is_unique: false,
  },
  {
    text: 'Price',
    type: 'price',
    is_default: true,
    example: '100',
    input_by: 'supplier',
    is_unique: false,
  },
  {
    text: 'Currency',
    type: 'currency',
    is_default: true,
    example: 'EUR',
    input_by: 'supplier',
    is_unique: false,
  },
  {
    text: 'Total Price',
    type: 'price_total',
    is_default: true,
    example: '100',
    input_by: 'supplier',
    is_unique: true,
  },
  {
    text: 'Number Input',
    type: 'number',
    is_default: false,
    example: '150',
    input_by: 'supplier',
    is_unique: false,
  },
  {
    text: 'Single Choice',
    type: 'single_choice',
    is_default: false,
    example: 'DDP',
    input_by: 'supplier',
    is_unique: false,
  },
  {
    text: 'Text Input',
    type: 'text',
    is_default: false,
    example: 'Aluminium',
    input_by: 'supplier',
    is_unique: false,
  },
  {
    text: 'Buyer Bundle',
    type: 'buyer_bundle',
    is_default: false,
    example: 'Plant 1',
    input_by: 'supplier',
    is_unique: false,
  },
  {
    text: 'Supplier Bundle',
    type: 'supplier_bundle',
    is_default: false,
    example: 'B1',
    input_by: 'supplier',
    is_unique: false,
  },
  {
    text: 'Historic Price',
    type: 'price_historic',
    is_default: false,
    example: '120',
    input_by: 'supplier',
    is_unique: true,
  },
  {
    text: 'Historic Currency',
    type: 'currency_historic',
    is_default: false,
    example: 'EUR',
    input_by: 'supplier',
    is_unique: true,
  },
  {
    text: 'Historic Supplier',
    type: 'supplier_historic',
    is_default: false,
    example: 'Supplier 1',
    input_by: 'supplier',
    is_unique: true,
  },
  {
    text: 'Supplier Name',
    type: 'supplier',
    is_default: false,
    example: 'Supplier 1',
    input_by: 'supplier',
    is_unique: true,
  },
];
const names = types.map(type => type.text);

module.exports = {
  types,
  up: async (queryInterface) => {
    const typesRows = types.map(type => ({
      ...type,
      created_at: new Date(),
      updated_at: new Date(),
    }));

    await queryInterface.bulkInsert('column_types', typesRows);
  },
  down: queryInterface => queryInterface.bulkDelete(
    'column_types',
    {
      text: names,
    }
  ),
};
