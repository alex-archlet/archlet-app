const currencies = ['USD', 'EUR', 'GBP', 'CHF', 'JPY', 'CNH', 'AUD', 'CAD', 'BRL'];

module.exports = {
  up: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      const inDB = await queryInterface.rawSelect(
        'project_currencies',
        {
          where: {
            code: currencies,
          },
          transaction,
        },
        ['id']
      );

      if (!inDB) {
        const unitRows = currencies.map(code => ({
          code,
          created_at: new Date(),
          updated_at: new Date(),
        }));

        await queryInterface.bulkInsert('project_currencies', unitRows, { transaction });
      }

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
  down: async (queryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.bulkDelete(
        'project_currencies',
        {
          code: currencies,
        },
        { transaction }
      );

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      throw err;
    }
  },
};
