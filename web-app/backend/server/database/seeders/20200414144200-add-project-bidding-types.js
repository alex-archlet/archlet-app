const projectBiddingTypes = [{
  code: 'manual',
  name: 'Manual Import',
  created_at: new Date(),
  updated_at: new Date(),
},
{
  code: 'bid_collector',
  name: 'Archlet Bid Collector',
  created_at: new Date(),
  updated_at: new Date(),
},
{
  code: 'bid_import',
  name: 'Archlet Bid Importer',
  created_at: new Date(),
  updated_at: new Date(),
},
{
  code: 'ariba',
  name: 'SAP Ariba',
  created_at: new Date(),
  updated_at: new Date(),
},
];

module.exports = {
  up: async (queryInterface) => {
    try {
      await queryInterface.bulkInsert(
        'project_bidding_types',
        projectBiddingTypes
      );
    } catch (err) {
      throw err;
    }
  },
  down: async (queryInterface) => {
    try {
      await queryInterface.bulkDelete(
        'project_bidding_types',
        projectBiddingTypes
      );
    } catch (err) {
      throw err;
    }
  },
};
