const types = ['volume', 'price', 'number', 'capacity', 'price_total'];

module.exports = {
  types,
  up: async (queryInterface, Sequelize) => {
    queryInterface.bulkUpdate('column_types', { is_numeric: true }, {
      type: { [Sequelize.Op.or]: types },
    });
  },
  down: Promise.resolve,
};
