import { Op } from 'sequelize';
import { ProjectKpiDefs } from '../../models/ProjectKpiDefs';

export class ProjectKpiDefsRepository {
  public static async getByProjectIdAndDefaults(
    projectId: string
  ): Promise<ProjectKpiDefs[]> {
    return ProjectKpiDefs
      .findAll({
        where: {
          [Op.or]: [{ projectId: null }, { projectId }],
        },
      });
  }
}
