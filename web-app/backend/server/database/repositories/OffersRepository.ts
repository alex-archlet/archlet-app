import { Offers } from '../../models/Offers';
import { Demands } from '../../models';

export class OffersRepository {
  public static async getAllOfferByDemandId(
    demandId: string
  ): Promise<Offers[]> {
    return Offers.findAll({
      where: {
        demandId,
        isHistoric: false,
      },
    });
  }

  public static async getAllOffersByIdsAndDemandIds(
    demandIds: string[],
    offerIds: string[]
  ): Promise<Offers[]> {
    return Offers.findAll({
      where: {
        id: offerIds,
      },
      include: [
        {
          model: Demands,
          as: 'demand',
          where: {
            id: demandIds,
          },
          required: true,
        },
      ],
    });
  }

  public static async getAllOffersAndDemandByDemandIds(
    demandIds: string[]
  ): Promise<Offers[]> {
    return Offers.findAll({
      where: {
        demandId: demandIds,
        isHistoric: false,
      },
      include: [
        {
          model: Demands,
          as: 'demand',
          required: true,
          attributes: ['attributes'],
        },
      ],
    });
  }

  public static async getAllNonHistoricOffersAndDemanddByRoundId(roundId: string): Promise<Offers[]> {
    return Offers.findAll({
      where: {
        roundId,
        is_historic: false,
      },
      include: [{
        model: Demands,
        as: 'demand',
      }],
    });
  }
}
