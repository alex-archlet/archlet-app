import { FeatureFlags } from '../../models/FeatureFlags';
import { FeatureFlagTypes } from '../../models/FeatureFlagTypes';

export class FeatureFlagsRepository {
  public static PIVOT_TABLE_ID = 1;

  public static async getAllByBranchId(
    branchId: string
  ): Promise<FeatureFlags[]> {
    return FeatureFlags
      .findAll({
        where: {
          branchId,
        },
      });
  }

  public static async getAllByIdAndBranchId(
    typeId: number,
    branchId: string
  ): Promise<FeatureFlags> {
    return FeatureFlags
      .findOne({
        where: {
          branchId,
        },
        include: [
          {
            model: FeatureFlagTypes,
            as: 'type',
            where: {
              id: typeId,
            },
            required: true,
          },
        ],
      });
  }
}
