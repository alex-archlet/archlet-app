import { Scenarios } from '../../models';

export class ScenariosRepository {
  public static async getScenarioWithAllocationsByIdAndBranchId(
    id: string,
    branchId: string
  ) {
    return Scenarios.scope('allocations').findOne({
      where: {
        id,
        branchId,
      },
    });
  }

  public static async getByIdAndBranchId(
    id: string,
    branchId: string
  ) {
    return Scenarios.findOne({
      where: {
        id,
        branchId,
      },
    });
  }

  public static async getAllByIdsAndProjectId(ids: string[], projectId: string) {
    return Scenarios.findAll({
      where: {
        projectId,
        id: ids,
      },
    });
  }
}
