import { BiddingRounds } from '../../models/BiddingRounds';

export class BiddingRoundsRepository {
  public static async getBiddingRoundByIdAndProjectIdAndBranchId(
    id: string,
    projectId: string,
    branchId: string
  ): Promise<BiddingRounds> {
    return BiddingRounds
      .scope('basic')
      .findOne({
        where: {
          id,
          projectId,
          branchId,
        },
      });
  }

  public static async getBiddingRoundByProjectIdAndBranchId(
    projectId: string,
    branchId: string
  ): Promise<BiddingRounds[]> {
    return BiddingRounds
      .scope('basic')
      .findAll({
        where: {
          projectId,
          branchId,
        },
      });
  }

  public static async getBiddingRoundByIdAndBranchId(
    roundId: string,
    branchId: string
  ): Promise<BiddingRounds> {
    return BiddingRounds
      .scope('basic')
      .findOne({
        where: {
          id: roundId,
          branchId,
        },
      });
  }
}
