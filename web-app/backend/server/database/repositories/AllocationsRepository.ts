import { Allocations } from '../../models/Allocations';
import { Scenarios } from '../../models/Scenarios';

export class AllocationsRepository {
  public static async getAllByScenario(scenario: Scenarios, raw: boolean = false) {
    return Allocations.findAll({
      where: {
        scenarioId: scenario.id,
      },
      raw,
    });
  }
}
