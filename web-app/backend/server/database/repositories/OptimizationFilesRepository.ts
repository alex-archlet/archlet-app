import { OptimizationFiles } from '../../models/OptimizationFiles';

export class OptimizationFilesRepository {
  public static async getOptimizationFileByIdAndBranchId(
    id: string,
    branchId: string
  ): Promise<OptimizationFiles> {
    return OptimizationFiles.findOne({
      where: {
        id,
        branchId,
      },
    });
  }

  public static async getAllOptimizationFilesByIdsAndBranchId(
    id: string[],
    branchId: string
  ): Promise<OptimizationFiles[]> {
    return OptimizationFiles.findAll({
      where: {
        id,
        branchId,
      },
    });
  }

  public static async getAllOptimizationFilesByIdsAndBranchIdAndBiddingRoundId(
    id: string[],
    branchId: string,
    roundId: string
  ): Promise<OptimizationFiles[]> {
    return OptimizationFiles.findAll({
      where: {
        id,
        branchId,
        roundId,
      },
    });
  }

  public static async getLastProjectBidTemplateFile(
    projectId: string,
    branchId: string
  ): Promise<OptimizationFiles> {
    return OptimizationFiles.findOne({
      where: {
        projectId,
        branchId,
        fileType: 'bid_template',
      },
      order: [['createdAt', 'DESC']],
    });
  }
}
