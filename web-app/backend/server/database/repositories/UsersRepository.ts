import { Op } from 'sequelize';
import { Users } from '../../models';

export class UsersRepository {
  public static async getByUserName(username: string) {
    return Users
      .scope('expandedUser')
      .findOne({
        where: { username: { [Op.iLike]: username } },
      });
  }

  public static async getById(id: string) {
    return Users
      .scope('expandedUser')
      .findOne({
        where: { id },
      });
  }
}
