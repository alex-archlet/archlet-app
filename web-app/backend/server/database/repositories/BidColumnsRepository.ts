import { BidColumns } from '../../models/BidColumns';

export class BidColumnsRepository {
  public static async getLastOrderNumberByProjectId(projectId: string): Promise<number> {
    return BidColumns.max('order', {
      raw: true,
      where: { projectId },
    });
  }

  public static async getAutomaticColumnsByProjectId(projectId: string): Promise<BidColumns[]> {
    return BidColumns.findAll({
      where: {
        projectId,
        isAutomatic: true,
      },
    });
  }

  public static async getAllColumnsByProjectId(projectId: string): Promise<BidColumns[]> {
    return BidColumns.findAll({
      where: {
        projectId,
      },
    });
  }

  public static async getAllColumnsScopedByProjectId(projectId: string): Promise<BidColumns[]> {
    return BidColumns.scope('column').findAll({
      where: {
        projectId,
      },
    });
  }
}
