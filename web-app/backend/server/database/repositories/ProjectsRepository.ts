import { Projects } from '../../models/Projects';

export class ProjectsRepository {
  public static async getByIdAndBranchId(
    id: string,
    branchId: string
  ): Promise<Projects> {
    return Projects
      .findOne({
        where: {
          id,
          branchId,
        },
      });
  }
}
