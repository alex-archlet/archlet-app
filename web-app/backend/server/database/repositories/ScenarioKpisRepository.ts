import { ScenarioKpis } from '../../models/ScenarioKpis';
import { Scenarios } from '../../models/Scenarios';

export class ScenarioKpisRepository {
  public static async getAllByScenario(scenario: Scenarios, raw: boolean = false) {
    return ScenarioKpis.findAll({
      where: {
        scenarioId: scenario.id,
      },
      raw,
    });
  }
}
