import {
  Model,
  DataTypes,
} from 'sequelize';

import { sequelize } from '../database/instance';

export class ProjectAccesses extends Model {
  public accessType!: string;

  public json?: object | null;

  public userAccessId?: string | null;

  public projectAccessId?: string | null;
}

ProjectAccesses.init({
  accessType: {
    field: 'access_type',
    allowNull: false,
    type: DataTypes.STRING,
  },
  json: {
    defaultValue: null,
    type: DataTypes.JSONB,
  },
  userAccessId: {
    field: 'user_id',
    type: DataTypes.UUID,
  },
  projectAccessId: {
    field: 'project_id',
    type: DataTypes.UUID,
  },
}, {
  indexes: [
    {
      fields: [
        'project_id',
        'user_id',
      ],
    },
  ],
  timestamps: true,
  underscored: true,
  sequelize,
});
