import {
  Model, DataTypes,
} from 'sequelize';
import {
  JobTypes,
} from './index';
import { Allocations } from './Allocations';
import { Jobs } from './Jobs';
import { JobTypeEnum } from '../enums';

import { sequelize } from '../database/instance';

export class Scenarios extends Model {
  public id!: string;

  public invalidate!: boolean;

  public isDefault!: boolean;

  public json?: object | null;

  public lastOptimization?: object;

  public name!: string;

  public status!: string;

  public projectId?: string;

  public roundId?: string;

  public branchId?: string;

  public output?: object;

  public allocations?: Allocations[];

  public optimizations?: Jobs[];

  public validations?: Jobs[];

  public static async getScenariosList(
    projectId: string,
    branchId: string,
    includeOptimizations: Boolean = false
  ): Promise<Scenarios[]> {
    let options = {};

    if (projectId) {
      options = {
        ...options,
        where: {
          projectId,
          branchId,
        },
      };
    }

    if (includeOptimizations) {
      const optimizationTypeId = await JobTypes.getJobTypeId(
        JobTypeEnum.OPTIMIZATION
      );

      options = {
        ...options,
        include: [
          {
            as: 'optimizations',
            limit: 1,
            model: Jobs,
            order: [['updated_at', 'DESC']],
            required: false,
            where: {
              typeId: optimizationTypeId,
            },
          },
        ],
      };
    }

    const result = await Scenarios.findAll(options);

    return result;
  }

  public static associate(models) {
    Scenarios.hasMany(models.Jobs, {
      as: 'optimizations',
      foreignKey: 'scenarioId',
    });

    Scenarios.hasMany(models.Jobs, {
      as: 'validations',
      foreignKey: 'scenarioId',
    });

    Scenarios.belongsTo(models.BiddingRounds, {
      as: 'bidRound',
      foreignKey: 'roundId',
    });

    Scenarios.belongsTo(models.Projects, {
      as: 'project',
      foreignKey: 'projectId',
    });

    Scenarios.belongsTo(models.Branches, {
      as: 'branch',
      foreignKey: 'branchId',
    });

    Scenarios.hasMany(models.Allocations, {
      as: 'allocations',
      foreignKey: 'scenarioId',
    });
  }
}

Scenarios.init(
  {
    id: {
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.UUID,
    },
    invalidate: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
    },
    isDefault: {
      field: 'is_default',
      defaultValue: false,
      type: DataTypes.BOOLEAN,
    },
    json: {
      defaultValue: null,
      type: DataTypes.JSONB,
    },
    lastOptimization: {
      field: 'last_optimization',
      defaultValue: null,
      type: DataTypes.DATE,
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    status: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    projectId: {
      field: 'project_id',
      type: DataTypes.UUID,
    },
    roundId: {
      field: 'round_id',
      type: DataTypes.UUID,
    },
    branchId: {
      field: 'branch_id',
      type: DataTypes.UUID,
    },
    output: {
      defaultValue: null,
      field: 'output',
      type: DataTypes.JSONB,
    },
  },
  {
    getterMethods: {
      initialJson() {
        return this.getDataValue('json');
      },
    },
    paranoid: true,
    deletedAt: 'deletedAt',
    timestamps: true,
    underscored: true,
    sequelize,
    scopes: {
      allocations: {
        include: [
          {
            model: Allocations,
            as: 'allocations',
            include: ['demand', 'offer', 'kpis'],
          },
        ],
      },
    },
  }
);
