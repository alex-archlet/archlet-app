import {
  Sequelize,
  Model,
  DataTypes,
  HasManyGetAssociationsMixin,
} from 'sequelize';

import { sequelize } from '../database/instance';
import { Branches } from './Branches';

export class Companies extends Model {
  public id!: string;

  public name!: string;

  // timestamps!
  public readonly createdAt!: Date;

  public readonly updatedAt!: Date;

  // associations
  public branches?: Branches[];

  public getBranches!: HasManyGetAssociationsMixin<Branches>; // Note the null assertions!

  public static associate(models) {
    Companies.hasMany(models.Branches, {
      as: 'branches',
      foreignKey: 'companyId',
    });
  }
}

Companies.init(
  {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: Sequelize.literal('uuid_generate_v4()'),
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true,
    },
  },
  {
    sequelize,
    tableName: 'companies',
    timestamps: true,
    underscored: true,
  }
);
