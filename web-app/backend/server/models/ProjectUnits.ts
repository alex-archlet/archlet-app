import {
  Model,
  DataTypes,
} from 'sequelize';
import { sequelize } from '../database/instance';

export class ProjectUnits extends Model {
  public id!: number;

  public code!: string;

  public name!: string
}

ProjectUnits.init({
  id: {
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
    type: DataTypes.INTEGER,
  },
  code: {
    allowNull: false,
    type: DataTypes.STRING,
    unique: true,
  },
  name: {
    allowNull: false,
    type: DataTypes.STRING,
  },
}, {
  timestamps: true,
  underscored: true,
  sequelize,
});
