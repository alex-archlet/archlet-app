import {
  Sequelize,
  Model,
  DataTypes,
  BelongsToGetAssociationMixin,
} from 'sequelize';

import { sequelize } from '../database/instance';
import { Offers } from './Offers';
import { Demands } from './Demands';
import { Scenarios } from './Scenarios';
import { BiddingRounds } from './BiddingRounds';
import { ScenarioKpis } from './ScenarioKpis';

export class Allocations extends Model {
  public id!: string;

  public volume!: number;

  public isManual: boolean;

  public totalPrice!: number;

  public offerId!: string;

  public demandId!: string;

  public scenarioId!: string;

  public roundId!: string;

  // timestamps!
  public readonly createdAt!: Date;

  public readonly updatedAt!: Date;

  // associations
  public readonly offer?: Offers; // Note this is optional since it's only populated when explicitly requested in code

  public readonly demand?: Demands; // Note this is optional since it's only populated when explicitly requested in code

  public readonly scenario?: Scenarios; // Note this is optional since it's only populated when explicitly requested in code

  public readonly biddingRound?: BiddingRounds; // Note this is optional since it's only populated when explicitly requested in code

  public readonly kpis?: ScenarioKpis[]; // Note this is optional since it's only populated when explicitly requested in code

  public getOffer!: BelongsToGetAssociationMixin<Offers>; // Note the null assertions!

  public getDemand!: BelongsToGetAssociationMixin<Demands>; // Note the null assertions!

  public getScenario!: BelongsToGetAssociationMixin<Scenarios>; // Note the null assertions!

  public getBiddingRound!: BelongsToGetAssociationMixin<BiddingRounds>; // Note the null assertions!

  public static associate(models) {
    Allocations.belongsTo(models.Offers, {
      as: 'offer',
      foreignKey: 'offerId',
    });

    Allocations.belongsTo(models.Demands, {
      as: 'demand',
      foreignKey: 'demandId',
    });

    Allocations.belongsTo(models.Scenarios, {
      as: 'scenario',
      foreignKey: 'scenarioId',
    });

    Allocations.belongsTo(models.BiddingRounds, {
      as: 'biddingRound',
      foreignKey: 'roundId',
    });

    Allocations.hasMany(models.ScenarioKpis, {
      as: 'kpis',
      foreignKey: 'allocationId',
    });
  }
}

Allocations.init(
  {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: Sequelize.literal('uuid_generate_v4()'),
    },
    volume: {
      field: 'volume',
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    totalPrice: {
      field: 'total_price',
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    isManual: {
      field: 'is_manual',
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    offerId: {
      field: 'offer_id',
      type: DataTypes.UUID,
    },
    demandId: {
      field: 'demand_id',
      type: DataTypes.UUID,
    },
    scenarioId: {
      field: 'scenario_id',
      type: DataTypes.UUID,
    },
    roundId: {
      field: 'round_id',
      type: DataTypes.UUID,
    },
  },
  {
    sequelize,
    tableName: 'allocations',
    timestamps: true,
    underscored: true,
  }
);
