import {
  Model,
  DataTypes,
} from 'sequelize';

import { sequelize } from '../database/instance';

export class ProjectKpiDefs extends Model {
  public id!: string;

  public name!: string;

  public description!: string;

  public calculation!: string;

  public isDefault!: boolean;

  public projectId!: string;

  public createdAt!: Date;

  public updatedAt!: Date;

  public static associate(models) {
    ProjectKpiDefs.belongsTo(models.Projects, {
      as: 'project',
      foreignKey: 'projectId',
    });

    ProjectKpiDefs.hasMany(models.ScenarioKpis, {
      as: 'scenarioKpis',
      foreignKey: 'projectKpiDefId',
    });
  }
}

ProjectKpiDefs.init({
  id: {
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
    type: DataTypes.UUID,
  },
  name: {
    allowNull: false,
    type: DataTypes.STRING,
    defaultValue: '',
  },
  description: {
    allowNull: false,
    type: DataTypes.TEXT,
    defaultValue: '',
  },
  calculation: {
    allowNull: false,
    type: DataTypes.TEXT,
    defaultValue: '',
  },
  isDefault: {
    field: 'is_default',
    type: DataTypes.BOOLEAN,
  },
  projectId: {
    allowNull: true,
    field: 'project_id',
    type: DataTypes.UUID,
  },
}, {
  tableName: 'project_kpi_defs',
  timestamps: true,
  underscored: true,
  sequelize,
});
