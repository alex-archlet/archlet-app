import {
  Sequelize,
  Model,
  DataTypes,
  BelongsToManyGetAssociationsMixin,
  BelongsToGetAssociationMixin,
} from 'sequelize';

import { sequelize } from '../database/instance';
import { BidRoundStatuses } from './BidRoundStatuses';
import { Projects } from './Projects';
import { Users } from './Users';
import { Branches } from './Branches';
import { OptimizationFiles } from './OptimizationFiles';
import { BidColumns } from './BidColumns';

interface biddingRoundSettings {
  historicTargetPrices: boolean
}

export class BiddingRounds extends Model {
  public id!: string;

  public name!: string;

  public description: string | null;

  public startAt: Date | null;

  public endAt: Date | null;

  public reminderAt: Date | null;

  public projectId: string;

  public statusId: number;

  public createdById: string;

  public updatedById: string;

  public branchId: string;

  public settings: biddingRoundSettings | null;

  // timestamps!
  public readonly createdAt!: Date;

  public readonly updatedAt!: Date;

  // associations
  public readonly status?: BidRoundStatuses; // Note this is optional since it's only populated when explicitly requested in code

  public readonly project?: Projects; // Note this is optional since it's only populated when explicitly requested in code

  public readonly createdByUser?: Users; // Note this is optional since it's only populated when explicitly requested in code

  public readonly updatedByUser?: Users; // Note this is optional since it's only populated when explicitly requested in code

  public readonly branch?: Branches; // Note this is optional since it's only populated when explicitly requested in code

  public readonly columns?: BidColumns[]; // Note this is optional since it's only populated when explicitly requested in code

  public readonly files?: OptimizationFiles[]; // Note this is optional since it's only populated when explicitly requested in code

  public getStatus!: BelongsToGetAssociationMixin<BidRoundStatuses>; // Note the null assertions!

  public getProject!: BelongsToGetAssociationMixin<Projects>; // Note the null assertions!

  public getCreatedByUser!: BelongsToGetAssociationMixin<Users>; // Note the null assertions!

  public getUpdatedByUser!: BelongsToGetAssociationMixin<Users>; // Note the null assertions!

  public getBranch!: BelongsToGetAssociationMixin<Branches>; // Note the null assertions!

  public getSuppliers!: BelongsToManyGetAssociationsMixin<Users>; // Note the null assertions!

  public static associate(models) {
    BiddingRounds.belongsTo(models.BidRoundStatuses, {
      as: 'status',
      foreignKey: 'statusId',
    });

    BiddingRounds.belongsTo(models.Projects, {
      as: 'project',
      foreignKey: 'projectId',
    });

    BiddingRounds.belongsTo(models.Users, {
      as: 'createdByUser',
      foreignKey: 'createdById',
    });

    BiddingRounds.belongsTo(models.Users, {
      as: 'updatedByUser',
      foreignKey: 'updatedById',
    });

    BiddingRounds.belongsTo(models.Branches, {
      as: 'branch',
      foreignKey: 'branchId',
    });

    BiddingRounds.hasMany(models.BidColumns, {
      as: 'columns',
      foreignKey: 'projectId',
    });

    BiddingRounds.belongsToMany(models.Users, {
      as: 'suppliers',
      constraints: false,
      foreignKey: 'roundAccessId',
      through: models.SupplierAccesses,
    });

    BiddingRounds.hasMany(models.OptimizationFiles, {
      as: 'files',
      foreignKey: 'roundId',
    });
  }

  public static async findLatestBiddingRoundByProjectId(projectId) {
    const biddingRounds = await BiddingRounds.findAll({
      where: {
        project_id: projectId,
        status_id: 4,
      },
      order: [
        ['created_at', 'DESC'],
      ],
    });

    return biddingRounds[0];
  }
}

BiddingRounds.init(
  {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: Sequelize.literal('uuid_generate_v4()'),
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    description: {
      type: DataTypes.TEXT,
    },
    startAt: {
      field: 'start_at',
      type: DataTypes.DATE,
    },
    endAt: {
      field: 'end_at',
      type: DataTypes.DATE,
    },
    reminderAt: {
      field: 'reminder_at',
      type: DataTypes.DATE,
    },
    projectId: {
      field: 'project_id',
      type: DataTypes.UUID,
    },
    statusId: {
      field: 'status_id',
      type: DataTypes.NUMBER,
      allowNull: false,
    },
    createdById: {
      field: 'created_by',
      type: DataTypes.UUID,
    },
    settings: {
      field: 'settings',
      type: DataTypes.JSONB,
    },
    updatedById: {
      field: 'updated_by',
      type: DataTypes.UUID,
    },
    branchId: {
      field: 'branch_id',
      type: DataTypes.UUID,
    },
  },
  {
    sequelize,
    paranoid: true,
    deletedAt: 'deletedAt',
    timestamps: true,
    underscored: true,
    scopes: {
      basic: {
        include: [
          'status',
          'createdByUser',
          'updatedByUser',
          'files',
          'columns',
        ],
      },
    },
  }
);

