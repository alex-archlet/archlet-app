import {
  Model,
  DataTypes,
} from 'sequelize';

import { sequelize } from '../database/instance';

export class FeatureFlagTypes extends Model {
  public id!: number;

  public name!: string;
}

FeatureFlagTypes.init(
  {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true,
    },
  },
  {
    sequelize,
    tableName: 'feature_flag_types',
    underscored: true,
  }
);

