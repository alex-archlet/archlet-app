import {
  Sequelize,
  Model,
  DataTypes,
} from 'sequelize';

import { sequelize } from '../database/instance';

export class ColumnTypes extends Model {
  public id!: number;

  public type!: string;

  public text!: string;

  public isDefault: boolean;

  public isUnique: boolean;

  public isNumeric: boolean;

  public inputBy: string | null;

  public example: string | null;

  // timestamps!
  public readonly createdAt!: Date;

  public readonly updatedAt!: Date;

  public static async getTypeId(type) {
    const result = await ColumnTypes.findOne(
      {
        attributes: ['id'],
        where: { type },
      }
    );

    return result.get('id');
  }
}

ColumnTypes.init(
  {
    id: {
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER,
    },
    type: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true,
    },
    text: {
      allowNull: false,
      type: DataTypes.TEXT,
    },
    isDefault: {
      field: 'is_default',
      defaultValue: false,
      type: DataTypes.BOOLEAN,
    },
    isNumeric: {
      field: 'is_numeric',
      defaultValue: false,
      type: DataTypes.BOOLEAN,
    },
    isUnique: {
      field: 'is_unique',
      defaultValue: false,
      type: DataTypes.BOOLEAN,
    },
    inputBy: {
      field: 'input_by',
      type: DataTypes.STRING,
    },
    example: {
      type: DataTypes.STRING,
    },
  }, {
    sequelize,
    tableName: 'column_types',
    timestamps: true,
    underscored: true,
  }
);
