import {
  Model,
  DataTypes,
} from 'sequelize';
import { sequelize } from '../database/instance';

export class Translators extends Model {
  public id!: string;

  public name!: string;

  public offerId?: string | null;

  public demandId?: string | null;

  public scenarioId?: string | null;

  public static associate(models) {
    Translators.belongsTo(models.Branches, {
      as: 'branch',
      foreignKey: 'branchId',
    });

    Translators.belongsTo(models.ProjectCategories, {
      as: 'category',
      foreignKey: 'categoryId',
    });

    Translators.belongsTo(models.OptimizationFiles, {
      as: 'file',
      foreignKey: 'optimizationFileId',
    });
  }
}

Translators.init({
  id: {
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
    type: DataTypes.UUID,
  },
  name: {
    allowNull: false,
    type: DataTypes.STRING,
    unique: true,
  },
  branchId: {
    field: 'branch_id',
    type: DataTypes.UUID,
  },
  categoryId: {
    field: 'category_id',
    type: DataTypes.INTEGER,
  },
  optimizationFileId: {
    field: 'optimization_file_id',
    type: DataTypes.UUID,
  },
}, {
  timestamps: true,
  underscored: true,
  sequelize,
});
