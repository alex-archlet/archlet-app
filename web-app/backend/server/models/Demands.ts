import {
  Sequelize,
  Model,
  DataTypes,
  BelongsToGetAssociationMixin,
} from 'sequelize';

import { sequelize } from '../database/instance';
import { BiddingRounds } from './BiddingRounds';
import { Branches } from './Branches';
import { Users } from './Users';

export class Demands extends Model {
  public id!: string;

  public item!: string;

  public volume!: number;

  public attributes: Object | null;

  public createdById: string | null;

  public updatedById: string | null;

  public roundId: string | null;

  public branchId: number | null;

  // timestamps!
  public readonly createdAt!: Date;

  public readonly updatedAt!: Date;

  // associations
  public createdByUser?: Users;

  public updatedByUser?: Users;

  public bidRound?: BiddingRounds;

  public branch?: Branches;

  public getCreatedByUser!: BelongsToGetAssociationMixin<Users>; // Note the null assertions!

  public getUpdatedByUser!: BelongsToGetAssociationMixin<Users>; // Note the null assertions!

  public getBidRound!: BelongsToGetAssociationMixin<BiddingRounds>; // Note the null assertions!

  public getBranch!: BelongsToGetAssociationMixin<Branches>; // Note the null assertions!

  public static associate(models) {
    Demands.belongsTo(models.Users, {
      as: 'createdByUser',
      foreignKey: 'createdById',
    });

    Demands.belongsTo(models.Users, {
      as: 'updatedByUser',
      foreignKey: 'updatedById',
    });

    Demands.belongsTo(models.BiddingRounds, {
      as: 'bidRound',
      foreignKey: 'roundId',
    });

    Demands.belongsTo(models.Branches, {
      as: 'branch',
      foreignKey: 'branchId',
    });
  }
}

Demands.init(
  {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: Sequelize.literal('uuid_generate_v4()'),
    },
    item: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    volume: {
      type: DataTypes.FLOAT,
    },
    attributes: {
      type: DataTypes.JSONB,
    },
    createdById: {
      field: 'created_by',
      type: DataTypes.UUID,
    },
    updatedById: {
      field: 'updated_by',
      type: DataTypes.UUID,
    },
    roundId: {
      field: 'round_id',
      type: DataTypes.UUID,
    },
    branchId: {
      field: 'branch_id',
      type: DataTypes.UUID,
    },
  },
  {
    tableName: 'demands',
    sequelize,
    timestamps: true,
    underscored: true,
  }
);
