import {
  Model,
  DataTypes,
  BelongsToGetAssociationMixin,
} from 'sequelize';

import { sequelize } from '../database/instance';
import { ProjectKpiDefs } from './ProjectKpiDefs';

export class ScenarioKpis extends Model {
  public id!: string;

  public value!: number;

  public projectKpiDefId!: string;

  public allocationId!: string | null;

  public scenarioId!: string;

  public readonly projectKpiDef?: ProjectKpiDefs;

  public getProjectKpiDefs!: BelongsToGetAssociationMixin<ProjectKpiDefs>; // Note the null assertions!

  public static associate(models) {
    ScenarioKpis.belongsTo(models.Scenarios, {
      as: 'scenario',
      foreignKey: 'scenarioId',
    });

    ScenarioKpis.belongsTo(models.Allocations, {
      as: 'allocation',
      foreignKey: 'allocationId',
    });

    ScenarioKpis.belongsTo(models.ProjectKpiDefs, {
      as: 'projectKpiDef',
      foreignKey: 'projectKpiDefId',
    });
  }
}

ScenarioKpis.init({
  id: {
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
    type: DataTypes.UUID,
  },
  value: {
    allowNull: false,
    type: DataTypes.FLOAT,
    defaultValue: 0,
  },
  scenarioId: {
    allowNull: false,
    field: 'scenario_id',
    type: DataTypes.UUID,
  },
  projectKpiDefId: {
    allowNull: false,
    field: 'project_kpi_def_id',
    type: DataTypes.UUID,
  },
  allocationId: {
    allowNull: true,
    field: 'allocation_id',
    type: DataTypes.UUID,
  },
}, {
  tableName: 'scenario_kpis',
  underscored: true,
  timestamps: false,
  sequelize,
});
