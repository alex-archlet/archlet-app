import {
  Model,
  DataTypes,
} from 'sequelize';

import { sequelize } from '../database/instance';

export class BiddingTypes extends Model {
  public id!: number;

  public code!: string;

  public name!: string;

  // timestamps!
  public readonly createdAt!: Date;

  public readonly updatedAt!: Date;
}

BiddingTypes.init(
  {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
    code: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true,
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true,
    },
  },
  {
    sequelize,
    tableName: 'project_bidding_types',
    timestamps: true,
    underscored: true,
  }
);
