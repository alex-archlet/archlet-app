import {
  Model,
  DataTypes,
} from 'sequelize';
import { sequelize } from '../database/instance';

export class ProjectCategories extends Model {
  public id!: number;

  public name!: string;

  public typeId?: string | null;

  public static associate(models) {
    ProjectCategories.belongsTo(models.ProjectTypes, {
      as: 'type',
      foreignKey: 'typeId',
    });
  }
}
ProjectCategories.init({
  id: {
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
    type: DataTypes.INTEGER,
  },
  name: {
    allowNull: false,
    type: DataTypes.STRING,
    unique: true,
  },
  typeId: {
    field: 'type_id',
    type: DataTypes.INTEGER,
  },
}, {
  timestamps: true,
  underscored: true,
  sequelize,
});
