import Sequelize from 'sequelize';

import { sequelize } from '../database/instance';

import { Allocations as AllocationsModel } from './Allocations';
import { BidColumns as BidColumnsModel } from './BidColumns';
import { BiddingRounds as BiddingRoundsModel } from './BiddingRounds';
import { BiddingTypes as BiddingTypesModel } from './BiddingTypes';
import { BidRoundStatuses as BidRoundStatusesModel } from './BidRoundStatuses';
import { Branches as BranchesModel } from './Branches';
import { ColumnTypes as ColumnTypesModel } from './ColumnTypes';
import { Companies as CompaniesModel } from './Companies';
import { Demands as DemandsModel } from './Demands';
import { FeatureFlags as FeatureFlagsModel } from './FeatureFlags';
import { FeatureFlagTypes as FeatureFlagTypesModel } from './FeatureFlagTypes';
import { Jobs as JobsModel } from './Jobs';
import { JobStatuses as JobStatusesModel } from './JobStatuses';
import { JobTypes as JobTypesModel } from './JobTypes';
import { Offers as OffersModel } from './Offers';
import { OptimizationFiles as OptimizationFilesModel } from './OptimizationFiles';
import { ProjectAccesses as ProjectAccessesModel } from './ProjectAccesses';
import { ProjectCategories as ProjectCategoriesModel } from './ProjectCategories';
import { ProjectCurrencies as ProjectCurrenciesModel } from './ProjectCurrencies';
import { ProjectKpiDefs as ProjectKpiDefsModel } from './ProjectKpiDefs';
import { Projects as ProjectsModel } from './Projects';
import { ProjectTypes as ProjectTypesModel } from './ProjectTypes';
import { ProjectUnits as ProjectUnitsModel } from './ProjectUnits';
import { Roles as RolesModel } from './Roles';
import { ScenarioKpis as ScenarioKpisModel } from './ScenarioKpis';
import { Scenarios as ScenariosModel } from './Scenarios';
import { Sessions as SessionsModel } from './Sessions';
import { SupplierAccesses as SupplierAccessesModel } from './SupplierAccesses';
import { Translators as TranslatorsModel } from './Translators';
import { Users as UsersModel } from './Users';

const db = {
  Allocations: AllocationsModel,
  BidColumns: BidColumnsModel,
  BiddingRounds: BiddingRoundsModel,
  BiddingTypes: BiddingTypesModel,
  BidRoundStatuses: BidRoundStatusesModel,
  Branches: BranchesModel,
  ColumnTypes: ColumnTypesModel,
  Companies: CompaniesModel,
  Demands: DemandsModel,
  FeatureFlags: FeatureFlagsModel,
  FeatureFlagTypes: FeatureFlagTypesModel,
  Jobs: JobsModel,
  JobStatuses: JobStatusesModel,
  JobTypes: JobTypesModel,
  Offers: OffersModel,
  OptimizationFiles: OptimizationFilesModel,
  ProjectAccesses: ProjectAccessesModel,
  ProjectCategories: ProjectCategoriesModel,
  ProjectCurrencies: ProjectCurrenciesModel,
  ProjectKpiDefs: ProjectKpiDefsModel,
  Projects: ProjectsModel,
  ProjectTypes: ProjectTypesModel,
  ProjectUnits: ProjectUnitsModel,
  Roles: RolesModel,
  ScenarioKpis: ScenarioKpisModel,
  Sessions: SessionsModel,
  Scenarios: ScenariosModel,
  SupplierAccesses: SupplierAccessesModel,
  Translators: TranslatorsModel,
  Users: UsersModel,
};

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

export {
  sequelize,
  Sequelize,
};

export const {
  Allocations,
  BidColumns,
  BiddingRounds,
  BiddingTypes,
  BidRoundStatuses,
  Branches,
  ColumnTypes,
  Companies,
  Demands,
  FeatureFlags,
  FeatureFlagTypes,
  Jobs,
  JobStatuses,
  JobTypes,
  Offers,
  OptimizationFiles,
  ProjectAccesses,
  ProjectCategories,
  ProjectCurrencies,
  ProjectKpiDefs,
  Projects,
  ProjectTypes,
  ProjectUnits,
  Roles,
  ScenarioKpis,
  Scenarios,
  Sessions,
  SupplierAccesses,
  Translators,
  Users,
} = db;
