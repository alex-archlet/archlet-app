import {
  Model,
  DataTypes,
} from 'sequelize';

import { sequelize } from '../database/instance';

export class Sessions extends Model {
  public id!: number;

  public userId!: string | null;

  public data!: string | null;

  public expires!: Date | null;

  // timestamps!
  public readonly createdAt!: Date;

  public readonly updatedAt!: Date;
}

Sessions.init(
  {
    sid: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.STRING,
    },
    userId: {
      field: 'user_id',
      allowNull: true,
      type: DataTypes.UUID,
    },
    expires: {
      allowNull: true,
      type: DataTypes.DATE,
    },
    data: {
      defaultValue: null,
      type: DataTypes.TEXT,
    },
  },
  {
    tableName: 'sessions',
    timestamps: false,
    underscored: true,
    sequelize,
  }
);
