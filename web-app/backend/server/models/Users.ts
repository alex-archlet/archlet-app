import {
  Model,
  DataTypes,
} from 'sequelize';

import {
  Companies,
  Projects,
} from './index';

import { sequelize } from '../database/instance';
import { Branches } from './Branches';
import { Projects as ProjectsModel } from './Projects';
import { BadRequestError } from '../errors/BadRequestError';
import { Roles } from './Roles';

export class Users extends Model {
  public id!: string;

  public accountType!: string;

  public email!: string; // for nullable fields

  public firstName!: string;

  public lastLogin?: Date | null ;

  public lastName!: string;

  public passwordHash!: string;

  public passwordSalt!: string;

  public totalLogin!: number;

  public username!: string;

  public createdAt: Date;

  public updatedAt: Date;

  public branchId: string;

  public roleId: number;

  // associations
  public readonly branch?: Branches;

  public userPermissions?: Roles;

  public static associate(models) {
    Users.belongsTo(models.Branches, {
      as: 'branch',
    });

    Users.belongsToMany(models.Projects, {
      as: 'projects',
      constraints: false,
      foreignKey: 'userAccessId',
      through: models.ProjectAccesses,
    });

    Users.belongsTo(models.Roles, {
      as: 'userPermissions',
      foreignKey: 'roleId',
    });

    Users.belongsToMany(models.BiddingRounds, {
      as: 'biddingRounds',
      constraints: false,
      foreignKey: 'userAccessId',
      through: models.SupplierAccesses,
    });
  }

  public getAllPermissionsOrThrow(): string[] {
    const allPermissions = new Set<string>();

    if (this.userPermissions === undefined) {
      throw new Error('User permissions not joined');
    }

    this.userPermissions.permissions.forEach((permission) => {
      allPermissions.add(permission);
    });

    if (this.branch?.branchPermissions === undefined) {
      throw new Error('Branch permissions not joined');
    }

    this.branch.branchPermissions.permissions.forEach((permission) => {
      allPermissions.add(permission);
    });

    return Array.from(allPermissions);
  }

  public static async getUserCompany(userId: string): Promise<Users> {
    const result = await Users.findOne({
      include: [
        {
          as: 'branch',
          include: [
            {
              as: 'company',
              model: Companies,
              required: true,
            },
          ],
          model: Branches,
          required: true,
        },
      ],
      where: { id: userId },
    });

    return result;
  }

  public static async getBranchSettings(userId: string): Promise<object> {
    let settings = {};
    const result = await Users.findOne({
      include: [
        {
          as: 'branch',
          model: Branches,
          required: true,
        },
      ],
      where: { id: userId },
    });

    if (result) {
      settings = result.branch.json;
    }

    if (!Object.keys(settings).length) {
      const message = 'Missing branch settings';

      throw new BadRequestError(message, message);
    }

    return settings;
  }

  public static async getUserProjects(userId: string): Promise<ProjectsModel[]> {
    const result = await Projects.findAll({
      include: [
        {
          as: 'users',
          model: Users,
          required: true,
          where: {
            id: userId,
          },
        },
      ],
    });

    return result;
  }
}

Users.init({
  id: {
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
    type: DataTypes.UUID,
  },
  accountType: {
    field: 'account_type',
    allowNull: false,
    type: DataTypes.STRING,
  },
  email: {
    allowNull: false,
    type: DataTypes.STRING,
  },
  firstName: {
    field: 'first_name',
    allowNull: false,
    type: DataTypes.STRING,
  },
  lastLogin: {
    field: 'last_login',
    defaultValue: null,
    type: DataTypes.DATE,
  },
  lastName: {
    field: 'last_name',
    allowNull: false,
    type: DataTypes.STRING,
  },
  passwordHash: {
    field: 'password_hash',
    allowNull: false,
    type: DataTypes.STRING,
  },
  passwordSalt: {
    field: 'password_salt',
    allowNull: false,
    type: DataTypes.STRING,
  },
  totalLogin: {
    field: 'total_login',
    allowNull: false,
    defaultValue: 0,
    type: DataTypes.INTEGER,
  },
  username: {
    allowNull: false,
    type: DataTypes.STRING,
    unique: true,
  },
  branchId: {
    field: 'branch_id',
    type: DataTypes.UUID,
  },
  roleId: {
    field: 'role_id',
    type: DataTypes.INTEGER,
  },
}, {
  tableName: 'users',
  timestamps: true,
  underscored: true,
  scopes: {
    basic: {
      attributes: {
        exclude: [
          'passwordHash',
          'passwordSalt',
        ],
      },
    },
    expandedUser: {
      include: [
        {
          association: 'userPermissions',
          required: true,
        },
        {
          association: 'branch',
          required: true,
          include: [
            {
              association: 'branchPermissions',
              required: true,
            },
          ],
        },
      ],
    },
  },
  sequelize,
});
