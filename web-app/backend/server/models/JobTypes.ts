import {
  Model,
  DataTypes,
} from 'sequelize';

import { sequelize } from '../database/instance';

export class JobTypes extends Model {
  public id!: number;

  public name!: string;

  public static async getJobTypeId(type: string): Promise<number> {
    const result = await JobTypes.findOrCreate(
      {
        defaults: { name: type },
        where: { name: type },
      }
    );

    return result[0].get('id');
  }
}

JobTypes.init(
  {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true,
    },
  },
  {
    sequelize,
    tableName: 'job_types',
    timestamps: true,
    underscored: true,
  }
);

