import {
  Sequelize,
  DataTypes,
  Model,
  Op,
} from 'sequelize';
import { sequelize } from '../database/instance';

export class OptimizationFiles extends Model {
  public id!: string;

  public contentHash!: string;

  public description?: string | null;

  public fileExtension!: string;

  public filePath!:string;

  public fileType!:string;

  public fileVersion!:string;

  public name!:string;

  public sheetName?: string | null;

  public rowNumber?: number | null;

  public createdAt: Date;

  public updatedAt: Date;

  public projectId?: string | null;

  public scenarioId?: string | null;

  public roundId?: string | null;

  public branchId?: string | null;

  public matchings?: object | null;

  public static associate(models) {
    OptimizationFiles.belongsTo(models.Projects, {
      foreignKey: 'projectId',
    });

    OptimizationFiles.belongsTo(models.Scenarios, {
      foreignKey: 'scenarioId',
    });

    OptimizationFiles.belongsTo(models.BiddingRounds, {
      foreignKey: 'roundId',
    });

    OptimizationFiles.belongsTo(models.Branches, {
      foreignKey: 'branchId',
    });
  }

  public static async getLastFile(type: string, projectId: string, branchId: string, scenarioId: string): Promise<OptimizationFiles> {
    const where = {
      fileType: type,
      projectId,
      branchId,
      scenarioId: null,
    };

    if (scenarioId) {
      where.scenarioId = scenarioId;
    }

    let result = await OptimizationFiles.findOne({
      order: [
        [
          'fileVersion',
          'DESC',
        ],
      ],
      where,
    });

    if (!result) {
      result = null;
    }

    return result;
  }

  public static async getLastVersion(type: string, projectId: string): Promise<OptimizationFiles> {
    const lastVersion = await OptimizationFiles.findOne({
      where: {
        fileType: type,
        projectId,
      },
      order: [
        [
          'fileVersion',
          'DESC',
        ],
      ],
    });

    return lastVersion;
  }

  public static async countDeletedFiles(filesId: string): Promise<number> {
    const deletedFiles = await OptimizationFiles.count({
      paranoid: false,
      where: {
        id: filesId,
        deletedAt: { [Op.not]: null },
      },
    });

    return deletedFiles;
  }
}

OptimizationFiles.init({
  id: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.UUID,
    defaultValue: Sequelize.literal('uuid_generate_v4()'),
  },
  contentHash: {
    field: 'content_hash',
    allowNull: false,
    type: DataTypes.STRING,
  },
  description: {
    type: DataTypes.TEXT,
  },
  fileExtension: {
    field: 'file_extension',
    allowNull: false,
    type: DataTypes.STRING,
  },
  filePath: {
    field: 'file_path',
    allowNull: false,
    type: DataTypes.TEXT,
  },
  fileType: {
    field: 'file_type',
    allowNull: false,
    type: DataTypes.STRING,
  },
  fileVersion: {
    field: 'file_version',
    defaultValue: null,
    type: DataTypes.INTEGER,
  },
  name: {
    allowNull: false,
    type: DataTypes.STRING,
  },
  sheetName: {
    field: 'sheet_name',
    defaultValue: false,
    type: DataTypes.STRING,
  },
  rowNumber: {
    field: 'row_number',
    defaultValue: null,
    type: DataTypes.INTEGER,
  },
  projectId: {
    field: 'project_id',
    type: DataTypes.UUID,
  },
  matchings: {
    field: 'matchings',
    type: DataTypes.JSONB,
  },
  scenarioId: {
    field: 'scenario_id',
    type: DataTypes.UUID,
  },
  roundId: {
    field: 'round_id',
    type: DataTypes.UUID,
  },
  branchId: {
    field: 'branch_id',
    type: DataTypes.UUID,
  },
}, {
  indexes: [
    {
      fields: ['file_version'],
    },
    {
      fields: ['project_id'],
    },
  ],
  paranoid: true,
  deletedAt: 'deletedAt',
  timestamps: true,
  underscored: true,
  sequelize,
});
