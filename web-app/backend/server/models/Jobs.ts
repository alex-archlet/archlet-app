import {
  Sequelize,
  Model,
  DataTypes,
  BelongsToGetAssociationMixin,
} from 'sequelize';

import { sequelize } from '../database/instance';
import { BiddingRounds } from './BiddingRounds';
import { Branches } from './Branches';
import { JobTypes } from './JobTypes';
import { Projects } from './Projects';
import { Scenarios } from './Scenarios';
import { JobStatuses } from './JobStatuses';
import { Users } from './Users';

export class Jobs extends Model {
  public id!: string;

  // TODO: Define a TS type for job input data?
  public input!: Object;

  // TODO: Define a TS type for job output data?
  public output!: Object;

  public scenarioId: string | null;

  public projectId: string;

  public typeId!: string;

  public statusId!: number;

  public progress: number;

  public roundId: string;

  public branchId!: string;

  public userId: string;

  // timestamps!
  public readonly createdAt!: Date;

  public readonly updatedAt!: Date;

  // associations
  public project?: Projects;

  public scenario?: Scenarios;

  public type?: JobTypes;

  public status?: JobStatuses;

  public biddingRound?: BiddingRounds;

  public branch?: Branches;

  public user?: Users;

  public getProject!: BelongsToGetAssociationMixin<Projects>; // Note the null assertions!

  public getScenario!: BelongsToGetAssociationMixin<Scenarios>; // Note the null assertions!

  public getType!: BelongsToGetAssociationMixin<JobTypes>; // Note the null assertions!

  public getStatus!: BelongsToGetAssociationMixin<JobStatuses>; // Note the null assertions!

  public getBiddingRound!: BelongsToGetAssociationMixin<BiddingRounds>; // Note the null assertions!

  public getBranch!: BelongsToGetAssociationMixin<Branches>; // Note the null assertions!

  public getUser!: BelongsToGetAssociationMixin<Users>; // Note the null assertions!

  public get initialOutput() {
    return this.output;
  }

  public static associate(models) {
    Jobs.belongsTo(models.Projects, {
      as: 'project',
      foreignKey: 'projectId',
    });

    Jobs.belongsTo(models.Scenarios, {
      as: 'scenario',
      foreignKey: 'scenarioId',
    });

    Jobs.belongsTo(models.JobTypes, {
      as: 'type',
      foreignKey: 'typeId',
    });

    Jobs.belongsTo(models.JobStatuses, {
      as: 'status',
      foreignKey: 'statusId',
    });

    Jobs.belongsTo(models.BiddingRounds, {
      as: 'biddingRound',
      foreignKey: 'roundId',
    });

    Jobs.belongsTo(models.Branches, {
      as: 'branch',
      foreignKey: 'branchId',
    });

    Jobs.belongsTo(models.Users, {
      as: 'user',
      foreignKey: 'userId',
    });
  }

  public static async getLastJobResult(
    type: string,
    projectId: string,
    branchId: string,
    scenarioId: string | undefined
  ): Promise<Jobs> {
    const typeId = await JobTypes.getJobTypeId(type);

    const where = {
      projectId,
      branchId,
      typeId,
      scenarioId: null,
    };

    if (scenarioId) {
      where.scenarioId = scenarioId;
    }

    const result = await Jobs.findOne({
      order: [
        ['updated_at', 'DESC'],
      ],
      where,
    });

    return result;
  }
}

Jobs.init(
  {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: Sequelize.literal('uuid_generate_v4()'),
    },
    input: {
      allowNull: false,
      type: DataTypes.JSONB,
    },
    output: {
      allowNull: false,
      type: DataTypes.JSONB,
    },
    scenarioId: {
      field: 'scenario_id',
      type: DataTypes.UUID,
    },
    projectId: {
      field: 'project_id',
      type: DataTypes.UUID,
    },
    typeId: {
      field: 'type_id',
      type: DataTypes.INTEGER,
    },
    statusId: {
      field: 'status_id',
      type: DataTypes.INTEGER,
    },
    progress: {
      allowNull: false,
      defaultValue: 0,
      type: DataTypes.INTEGER,
    },
    roundId: {
      field: 'round_id',
      type: DataTypes.UUID,
    },
    branchId: {
      field: 'branch_id',
      type: DataTypes.UUID,
    },
    userId: {
      field: 'user_id',
      type: DataTypes.UUID,
    },
  },
  {
    sequelize,
    tableName: 'jobs',
    timestamps: true,
    underscored: true,
  }
);
