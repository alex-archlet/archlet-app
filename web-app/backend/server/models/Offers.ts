import {
  Sequelize,
  Model,
  DataTypes,
} from 'sequelize';

import { sequelize } from '../database/instance';

export class Offers extends Model {
  public id!: string;

  public totalVolume?: number | null;

  public total_fixed_price?: number | null;

  public total_var_price?: number | null;

  public isProcessed!: boolean;

  public isHistoric!: boolean;

  public processing_messages?: object | null;

  public priceTotal?: number | null;

  public supplier?: string | null;

  public attributes?: object | null;

  public readonly createdById?: Date | null;

  public readonly updatedById?: Date | null;

  public supplierId?: string | null;

  public roundId?: string | null;

  public demandId?: string | null;

  public static associate(models) {
    Offers.belongsTo(models.Users, {
      as: 'createdByUser',
      foreignKey: 'createdById',
    });

    Offers.belongsTo(models.Users, {
      as: 'updatedByUser',
      foreignKey: 'updatedById',
    });

    Offers.belongsTo(models.Users, {
      as: 'suppliers',
      foreignKey: 'supplierId',
    });

    Offers.belongsTo(models.BiddingRounds, {
      as: 'bidRound',
      foreignKey: 'roundId',
    });

    Offers.belongsTo(models.Demands, {
      as: 'demand',
      foreignKey: 'demandId',
    });
  }
}

Offers.init({
  id: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.UUID,
    defaultValue: Sequelize.literal('uuid_generate_v4()'),
  },
  totalVolume: {
    field: 'total_volume',
    type: DataTypes.FLOAT,
    allowNull: true,
  },
  total_fixed_price: {
    type: DataTypes.FLOAT,
    allowNull: true,
  },
  total_var_price: {
    type: DataTypes.FLOAT,
    allowNull: true,
  },
  isProcessed: {
    field: 'is_processed',
    type: DataTypes.BOOLEAN,
    allowNull: false,
  },
  isHistoric: {
    field: 'is_historic',
    type: DataTypes.BOOLEAN,
    allowNull: false,
  },
  processing_messages: {
    type: DataTypes.JSONB,
    allowNull: false,
  },
  priceTotal: {
    field: 'price_total',
    type: DataTypes.FLOAT,
  },
  supplier: {
    type: DataTypes.STRING,
  },
  attributes: {
    type: DataTypes.JSONB,
  },
  createdById: {
    field: 'created_by',
    type: DataTypes.UUID,
  },
  updatedById: {
    field: 'updated_by',
    type: DataTypes.UUID,
  },
  supplierId: {
    field: 'supplier_id',
    type: DataTypes.UUID,
  },
  roundId: {
    field: 'round_id',
    type: DataTypes.UUID,
  },
  demandId: {
    field: 'demand_id',
    type: DataTypes.UUID,
  },
}, {
  timestamps: true,
  underscored: true,
  sequelize,
});
