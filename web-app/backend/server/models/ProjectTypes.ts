import {
  Model,
  DataTypes,
} from 'sequelize';
import { sequelize } from '../database/instance';

export class ProjectTypes extends Model {
  public id!: number;

  public name!: string;
}

ProjectTypes.init({
  id: {
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
    type: DataTypes.INTEGER,
  },
  name: {
    allowNull: false,
    type: DataTypes.STRING,
    unique: true,
  },
}, {
  timestamps: true,
  underscored: true,
  sequelize,
});

