import {
  Sequelize,
  Model,
  DataTypes,
  BelongsToGetAssociationMixin,
} from 'sequelize';

import { sequelize } from '../database/instance';
import { ColumnTypes } from './ColumnTypes';
import { Projects } from './Projects';

export class BidColumns extends Model {
  public id!: string;

  public name!: string;

  public description: string | null;

  public calculation: string | null;

  public isAutomatic: boolean;

  public isMandatory: boolean;

  public isVisible: boolean;

  public order: number;

  public limitMin: number;

  public limitMax: number;

  public singleChoice: string[];

  public inputBy: string;

  public projectId: string;

  public typeId: number;

  public initialTypeId: number | null;

  public matchingConfidence: number | null;

  public initialMatchingConfidence: number | null;

  // timestamps!
  public readonly createdAt!: Date;

  public readonly updatedAt!: Date;

  public readonly columnType?: ColumnTypes; // Note this is optional since it's only populated when explicitly requested in code

  public readonly project?: Projects; // Note this is optional since it's only populated when explicitly requested in code

  public getColumnType!: BelongsToGetAssociationMixin<ColumnTypes>; // Note the null assertions!

  public static associate(models) {
    BidColumns.belongsTo(models.ColumnTypes, {
      as: 'columnType',
      foreignKey: 'typeId',
    });

    BidColumns.belongsTo(models.Projects, {
      as: 'project',
      foreignKey: 'projectId',
    });
  }
}

BidColumns.init(
  {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: Sequelize.literal('uuid_generate_v4()'),
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    description: {
      type: DataTypes.TEXT,
    },
    calculation: {
      type: DataTypes.TEXT,
    },
    isAutomatic: {
      field: 'is_automatic',
      defaultValue: false,
      type: DataTypes.BOOLEAN,
    },
    isMandatory: {
      field: 'is_mandatory',
      defaultValue: false,
      type: DataTypes.BOOLEAN,
    },
    isVisible: {
      field: 'is_visible',
      defaultValue: true,
      type: DataTypes.BOOLEAN,
    },
    order: {
      defaultValue: 0,
      type: DataTypes.INTEGER,
    },
    limitMin: {
      field: 'limit_min',
      type: DataTypes.FLOAT,
    },
    limitMax: {
      field: 'limit_max',
      type: DataTypes.FLOAT,
    },
    singleChoice: {
      field: 'single_choice',
      type: DataTypes.ARRAY(DataTypes.TEXT),
    },
    inputBy: {
      field: 'input_by',
      type: DataTypes.STRING,
    },
    typeId: {
      field: 'type_id',
      type: DataTypes.INTEGER,
    },
    projectId: {
      field: 'project_id',
      type: DataTypes.UUID,
    },
    initialTypeId: {
      field: 'initial_type_id',
      type: DataTypes.INTEGER,
    },
    matchingConfidence: {
      allowNull: true,
      field: 'matching_confidence',
      type: DataTypes.FLOAT,
    },
    initialMatchingConfidence: {
      allowNull: true,
      field: 'initial_matching_confidence',
      type: DataTypes.FLOAT,
    },
  },
  {
    sequelize,
    tableName: 'bid_columns',
    timestamps: true,
    underscored: true,
    scopes: {
      column: {
        include: [
          'columnType',
        ],
      },
    },
  }
);
