import {
  Sequelize,
  Model,
  DataTypes,
  BelongsToGetAssociationMixin,
  HasManyGetAssociationsMixin,
} from 'sequelize';

import { sequelize } from '../database/instance';
import { Companies } from './Companies';
import { Users } from './Users';
import { Roles } from './Roles';

export class Branches extends Model {
  public id!: string;

  public accountType!: string;

  public json: Object | null;

  public location: string;

  public name: string;

  public solver: string;

  public companyId: string;

  public roleId: string;

  // timestamps!
  public readonly createdAt!: Date;

  public readonly updatedAt!: Date;

  // associations
  public readonly company?: Companies; // Note this is optional since it's only populated when explicitly requested in code

  public readonly users?: Users[]; // Note this is optional since it's only populated when explicitly requested in code

  public readonly branchPermissions?: Roles; // Note this is optional since it's only populated when explicitly requested in code

  public getCompany!: BelongsToGetAssociationMixin<Companies>; // Note the null assertions!

  public getUsers!: HasManyGetAssociationsMixin<Users>; // Note the null assertions!

  public getBranchPermissions!: BelongsToGetAssociationMixin<Roles>; // Note the null assertions!

  public static associate(models) {
    Branches.belongsTo(models.Companies, {
      as: 'company',
    });

    Branches.hasMany(models.Users, {
      as: 'users',
      foreignKey: 'branchId',
    });

    Branches.belongsTo(models.Roles, {
      as: 'branchPermissions',
      foreignKey: 'roleId',
    });
  }
}

Branches.init(
  {
    id: {
      allowNull: false,
      primaryKey: true,
      defaultValue: Sequelize.literal('uuid_generate_v4()'),
      type: DataTypes.UUID,
    },
    accountType: {
      field: 'account_type',
      allowNull: false,
      type: DataTypes.STRING,
    },
    json: {
      defaultValue: null,
      type: DataTypes.JSONB,
    },
    location: {
      defaultValue: '',
      allowNull: false,
      type: DataTypes.STRING,
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true,
    },
    solver: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    companyId: {
      field: 'company_id',
      type: DataTypes.UUID,
    },
    roleId: {
      field: 'role_id',
      type: DataTypes.INTEGER,
    },
  }, {
    sequelize,
    defaultScope: {
      include: [
        'company',
      ],
    },
    tableName: 'branches',
    timestamps: true,
    underscored: true,
  }
);
