import {
  Model,
  DataTypes,
} from 'sequelize';
import {
  AccountTypeEnum,
} from '../enums';

import { sequelize } from '../database/instance';

export class SupplierAccesses extends Model {
  public accessType!: string;

  public userAccessId? : string | null;

  public roundAccessId: string | null;
}

SupplierAccesses.init({
  accessType: {
    field: 'access_type',
    allowNull: false,
    defaultValue: AccountTypeEnum.SUPPLIER,
    type: DataTypes.STRING,
  },
  userAccessId: {
    field: 'user_id',
    type: DataTypes.UUID,
  },
  roundAccessId: {
    field: 'round_id',
    type: DataTypes.UUID,
  },
}, {
  indexes: [
    {
      fields: [
        'round_id',
        'user_id',
      ],
    },
  ],
  timestamps: true,
  underscored: true,
  sequelize,
});

