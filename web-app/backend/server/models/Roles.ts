import {
  Model,
  DataTypes,
} from 'sequelize';

import { sequelize } from '../database/instance';

export class Roles extends Model {
  public id!: number;

  public name!: string;

  public permissions?: string[] | null; // for nullable fields

  // timestamps!
  public readonly createdAt!: Date;

  public readonly updatedAt!: Date;
}

Roles.init(
  {
    id: {
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER,
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    permissions: {
      defaultValue: null,
      type: DataTypes.ARRAY(DataTypes.TEXT),
    },
  },
  {
    tableName: 'roles',
    timestamps: true,
    underscored: true,
    sequelize,
  }
);
