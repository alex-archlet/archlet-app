import {
  Model,
  DataTypes,
} from 'sequelize';

import { sequelize } from '../database/instance';

export class BidRoundStatuses extends Model {
  public id!: number;

  public name!: string;

  // timestamps!
  public readonly createdAt!: Date;

  public readonly updatedAt!: Date;

  public static associate(models) {
    BidRoundStatuses.hasMany(models.BiddingRounds, {
      as: 'biddingRounds',
      foreignKey: 'statusId',
    });
  }

  public static async getStatusId(status): Promise<number> {
    const result = await BidRoundStatuses.findOrCreate(
      {
        defaults: { name: status },
        where: { name: status },
      }
    );

    return result[0].get('id');
  }
}

BidRoundStatuses.init(
  {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.NUMBER,
      autoIncrement: true,
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING,
    },
  },
  {
    sequelize,
    tableName: 'bid_round_statuses',
    timestamps: true,
    underscored: true,
  }
);

