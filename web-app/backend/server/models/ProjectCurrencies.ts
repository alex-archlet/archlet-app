import {
  Model,
  DataTypes,
} from 'sequelize';
import { sequelize } from '../database/instance';

export class ProjectCurrencies extends Model {
  public id!: number;

  public code!: string;
}

ProjectCurrencies.init({
  id: {
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
    type: DataTypes.INTEGER,
  },
  code: {
    allowNull: false,
    type: DataTypes.STRING,
    unique: true,
  },
}, {
  timestamps: true,
  underscored: true,
  sequelize,
});

