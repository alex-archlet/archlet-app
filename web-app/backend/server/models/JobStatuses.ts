import {
  Model,
  DataTypes,
} from 'sequelize';

import { sequelize } from '../database/instance';

export class JobStatuses extends Model {
  public id!: number;

  public name!: string;

  public static async getJobStatusId(status: string): Promise<number> {
    const result = await JobStatuses.findOrCreate(
      {
        defaults: { name: status },
        where: { name: status },
      }
    );

    return result[0].get('id');
  }
}

JobStatuses.init(
  {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true,
    },
  },
  {
    sequelize,
    tableName: 'job_statuses',
    timestamps: true,
    underscored: true,
  }
);

