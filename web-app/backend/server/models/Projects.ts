import {
  Model,
  DataTypes,
  Op,
} from 'sequelize';

import {
  BiddingRounds,
  Users,
} from './index';
import { AccountTypeEnum } from '../enums';
import { sequelize } from '../database/instance';

export class Projects extends Model {
  public id!: string;

  public name!: string;

  public status!: string;

  public categoryId!: string;

  public currencyId!: string;

  public branchId!: string;

  public biddingTypeId!: string;

  public settings!: object;

  public static associate(models) {
    Projects.belongsTo(models.ProjectCategories, {
      as: 'category',
      foreignKey: 'categoryId',
    });

    Projects.belongsTo(models.ProjectUnits, {
      as: 'units',
      foreignKey: 'unitId',
    });

    Projects.belongsTo(models.ProjectCurrencies, {
      as: 'currencies',
      foreignKey: 'currencyId',
    });

    Projects.hasMany(models.Scenarios, {
      as: 'scenarios',
      foreignKey: 'projectId',
    });

    Projects.belongsToMany(models.Users, {
      as: 'users',
      constraints: false,
      foreignKey: 'projectAccessId',
      through: models.ProjectAccesses,
    });

    Projects.hasMany(models.BiddingRounds, {
      foreignKey: 'projectId',
    });

    Projects.belongsTo(models.Branches, {
      as: 'branches',
      foreignKey: 'branchId',
    });

    Projects.belongsTo(models.BiddingTypes, {
      as: 'biddingType',
      foreignKey: 'biddingTypeId',
    });
  }

  // TODO: Check if works correctly
  public static async getProjectMembers(projectId: string, userId: string, includeAdmins: Boolean = true): Promise<Projects> {
    let usersWhere: any = {
      id: userId,
    };

    if (!includeAdmins) {
      usersWhere = {
        id: userId,
        accountType: {
          [Op.not]: AccountTypeEnum.ADMIN,
        },
      };
    }

    const result = await Projects.findOne({
      include: [
        {
          as: 'users',
          attributes: [
            'accountType',
            'firstName',
            'id',
            'lastName',
            'username',
          ],
          model: Users,
          required: true,
          // TODO: Check if works correctly
          // through: {
          //   model: ProjectAccesses,
          //   where: { userAccessId: userId },
          // },
          where: usersWhere,
        },
      ],
      where: { id: projectId },
    });

    return result;
  }

  public static async getProjectsRounds(options: object = {}): Promise<Projects[]> {
    const rounds = await Projects.findAll({
      ...options,
      raw: true,
      include: [
        {
          attributes: [],
          model: BiddingRounds,
          required: true,
        },
      ],
    });

    return rounds;
  }
}

Projects.init({
  id: {
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
    type: DataTypes.UUID,
  },
  name: {
    allowNull: false,
    type: DataTypes.STRING,
  },
  status: {
    allowNull: false,
    type: DataTypes.STRING,
  },
  settings: {
    field: 'settings',
    type: DataTypes.JSONB,
  },
  categoryId: {
    field: 'category_id',
    type: DataTypes.INTEGER,
  },
  unitId: {
    field: 'unit_id',
    type: DataTypes.INTEGER,
  },
  currencyId: {
    field: 'currency_id',
    type: DataTypes.INTEGER,
  },
  branchId: {
    field: 'branch_id',
    type: DataTypes.UUID,
  },
  biddingTypeId: {
    field: 'bidding_type_id',
    type: DataTypes.INTEGER,
  },
}, {
  paranoid: true,
  deletedAt: 'deletedAt',
  timestamps: true,
  underscored: true,
  sequelize,
});
