import {
  Sequelize,
  Model,
  DataTypes,
} from 'sequelize';

import { sequelize } from '../database/instance';

export class FeatureFlags extends Model {
  public id!: string;

  public flagTypeId!: number;

  public branchId!: string;

  public createdAt!: Date;

  public updatedAt!: Date;

  public static associate(models) {
    FeatureFlags.belongsTo(models.FeatureFlagTypes, {
      as: 'type',
      foreignKey: 'flagTypeId',
    });

    FeatureFlags.belongsTo(models.Branches, {
      as: 'branch',
      foreignKey: 'branchId',
    });
  }
}

FeatureFlags.init(
  {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: Sequelize.literal('uuid_generate_v4()'),
    },
    flagTypeId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    branchId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
  },
  {
    sequelize,
    tableName: 'feature_flags',
    underscored: true,
    timestamps: false,
  }
);

