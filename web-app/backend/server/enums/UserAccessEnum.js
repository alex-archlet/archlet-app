export default {
  READ: 'read',
  READ_AND_WRITE: 'read and write',
  SUPPLIER: 'supplier',
};
