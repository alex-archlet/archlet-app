export default {
  ABORTED: 'aborted',
  CANCELED: 'canceled',
  PENDING: 'pending',
  RUNNING: 'running',
  SUCCEEDED: 'succeeded',
};
