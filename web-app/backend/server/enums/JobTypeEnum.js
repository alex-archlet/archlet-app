export default {
  CREATE_SCENARIO: 'create_scenario',
  FILE_PROCESSING: 'file_processing',
  OPTIMIZATION: 'optimization',
  POSTPROCESSING: 'postprocessing',
  PROJECT_INFO: 'project_info',
  COLUMN_MATCHING: 'column_matching',
  EXPRESSION_EXAMPLE: 'expression_example',
  OFFER_PROCESSING: 'offer_processing',
  EXPRESSION_EVALUATION: 'expression_evaluation',
};
