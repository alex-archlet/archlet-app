import { Router } from 'express';

import { RolesController } from '../../controllers';

const router = Router();

// GET - /api/v1/branches
router.get(
  '/',
  RolesController.getRolesList
);

export default router;
