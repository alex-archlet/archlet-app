import { Router } from 'express';

import { checkValidation } from '../../middlewares/ValidationMiddleware';
import { hasBranchFeatureFlag } from '../../middlewares/FeatureFlagMiddleware';
import { ProjectsController } from '../../controllers';
import { BidColumnsListController } from '../../controllers/projects/bidColumns/bidColumnsList/BidColumnsListController';
import { BidColumnsDeleteController } from '../../controllers/projects/bidColumns/bidColumnsDelete/BidColumnsDeleteController';
import { BidColumnsCreateController } from '../../controllers/projects/bidColumns/bidColumnsCreate/BidColumnsCreateController';
import { ProjectInfoController } from '../../controllers/projects/projectInfo/ProjectInfoController';
import { ProjectSettingsController } from '../../controllers/projects/projectSettings/ProjectSettingsController';
import { ProjectPivotTableController } from '../../controllers/projects/pivotTable/ProjectPivotTableController';
import { ProjectKpiDefsController } from '../../controllers/projects/projectKpiDefs/ProjectKpiDefsController';
import validate from '../../validations/ProjectsValidation';
import {
  ACM,
  AccessControl,
} from '../../middlewares/AccessControlMiddleware';
import { FeatureFlagsRepository } from '../../database/repositories/FeatureFlagsRepository';

const router = Router();

// GET - /api/v1/projects
router.get(
  '/',
  ProjectsController.getProjectsList
);

// GET - /api/v1/projects/types
router.get(
  '/types',
  ProjectsController.getProjectTypes
);

// GET - /api/v1/projects/categories
router.get(
  '/categories',
  ProjectsController.getProjectCategories
);

// GET - /api/v1/projects/currencies
router.get(
  '/currencies',
  ProjectsController.getProjectCurrencies
);

// GET - /api/v1/projects/biddingTypes
router.get(
  '/biddingTypes',
  ProjectsController.getProjectBiddingTypes
);

// GET - /api/v1/projects/units
router.get(
  '/units',
  ProjectsController.getProjectUnits
);

// GET - /api/v1/projects/:projectId
router.get(
  '/:projectId',
  AccessControl(
    ACM.hasAccessToProject
  ),
  ProjectsController.getProjectDetails
);

// GET - /api/v1/projects/:projectId/scenarios
router.get(
  '/:projectId/scenarios',
  AccessControl(
    ACM.hasAccessToProject
  ),
  ProjectsController.getProjectScenariosList
);

// GET - /api/v1/projects/:projectId/files
router.get(
  '/:projectId/files',
  AccessControl(
    ACM.hasAccessToProject
  ),
  ProjectsController.getProjectFiles
);

// GET - /api/v1/projects/:projectId/files_validation
router.get(
  '/:projectId/files_validation',
  AccessControl(
    ACM.hasAccessToProject
  ),
  ProjectsController.getProjectFilesValidation
);

// GET - /api/v1/projects/:projectId/members
router.get(
  '/:projectId/members',
  AccessControl(
    ACM.hasAccessToProject
  ),
  ProjectsController.getProjectMembers
);

// GET - /api/v1/projects/:projectId/info
router.get(
  '/:projectId/info',
  AccessControl(
    ACM.hasAccessToProject
  ),
  ProjectInfoController.getProjectInfo
);

// POST - /api/v1/projects/:projectId/pivot_table
router.post(
  '/:projectId/pivot_table',
  hasBranchFeatureFlag(FeatureFlagsRepository.PIVOT_TABLE_ID),
  AccessControl(
    ACM.hasAccessToProject
  ),
  validate.pivotTable,
  checkValidation,
  ProjectPivotTableController.getProjectPivotTable
);

// GET - /api/v1/projects/:projectId/file_preview/:type
router.get(
  '/:projectId/file_preview/:type',
  AccessControl(
    ACM.hasAccessToProject
  ),
  ProjectsController.getFilePreview
);

// DELETE - /api/v1/projects/:projectId
router.delete(
  '/:projectId',
  AccessControl(
    ACM.hasAccessToProject
  ),
  ProjectsController.deleteProject
);

// POST - /api/v1/projects
router.post(
  '/',
  validate.createProject,
  checkValidation,
  ProjectsController.createProject
);

// POST - /api/v1/projects/:projectId/access
router.put(
  '/:projectId/access',
  AccessControl(
    ACM.hasAccessToProject
  ),
  validate.addProjectAccess,
  checkValidation,
  ProjectsController.addProjectAccess
);

// PUT /api/v1/projects/:projectId
router.put(
  '/:projectId',
  AccessControl(
    ACM.hasAccessToProject
  ),
  validate.updateProject,
  checkValidation,
  ProjectsController.updateProject
);

// GET - /api/v1/projects/:projectId/table_design
router.get(
  '/:projectId/table_design',
  AccessControl(
    ACM.hasAccessToProject
  ),
  BidColumnsListController.getTableDesignList
);

// DELETE - /api/v1/projects/:projectId/table_design/:columnId
router.delete(
  '/:projectId/table_design/:columnId',
  AccessControl(
    ACM.hasAccessToProject,
    ACM.columnBelongsToProject
  ),
  BidColumnsDeleteController.deleteTableDesignColumn
);

// PUT - /api/v1/projects/:projectId/table_design
router.put(
  '/:projectId/table_design',
  AccessControl(
    ACM.hasAccessToProject
  ),
  validate.createOrUpdateTableDesign,
  checkValidation,
  BidColumnsCreateController.createOrUpdateTableDesign
);

// PUT - /api/v1/projects/:projectId/settings
router.put(
  '/:projectId/settings',
  AccessControl(
    ACM.hasAccessToProject
  ),
  ProjectSettingsController.updateProjectSettings
);

// GET - /api/v1/projects/:projectId/kpi_defs
router.get(
  '/:projectId/kpi_defs',
  AccessControl(
    ACM.hasAccessToProject
  ),
  ProjectKpiDefsController.getProjectKpiDefs
);

export default router;

