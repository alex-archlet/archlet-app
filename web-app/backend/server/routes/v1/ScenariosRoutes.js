import { Router } from 'express';

import { DemandsController } from '../../controllers/demands/DemandsController';
import { FinalScenarioReviewController } from '../../controllers/scenarios/finalScenarioReview/FinalScenarioReviewController';
import { ScenarioAllocationsController } from '../../controllers/scenarios/scenarioAllocations/ScenarioAllocationsController';
import { ScenarioCreateController } from '../../controllers/scenarios/scenarioCreate/ScenarioCreateController';
import { ScenarioDeleteController } from '../../controllers/scenarios/scenarioDelete/ScenarioDeleteController';
import { ScenarioDetailsController } from '../../controllers/scenarios/scenarioDetails/ScenarioDetailsController';
import { ScenarioDuplicateController } from '../../controllers/scenarios/scenarioDuplicate/ScenarioDuplicateController';
import { ScenarioKpiController } from '../../controllers/scenarios/scenarioKpi/ScenarioKpiController';
import { ScenarioManualAllocationsController } from '../../controllers/scenarios/scenarioAllocations/ScenarioManualAllocationsController';
import { ScenarioUpdateController } from '../../controllers/scenarios/scenarioUpdate/ScenarioUpdateController';

import validate from '../../validations/ScenariosValidation';
import { checkValidation } from '../../middlewares/ValidationMiddleware';
import {
  ACM,
  AccessControl,
} from '../../middlewares/AccessControlMiddleware';
import { PermissionsEnum } from '../../enums';

const router = Router();

// GET - /api/v1/scenarios/kpi
router.get(
  '/kpi',
  ScenarioKpiController.getScenarioKpi
);

// GET - /api/v1/scenarios/:scenarioId
router.get(
  '/:scenarioId',
  AccessControl(
    ACM.hasPermission(PermissionsEnum.CAN_READ_SCENARIO),
    ACM.hasAccessToScenario
  ),
  ScenarioDetailsController.getScenarioDetails
);

// GET - /api/v1/scenarios/:scenarioId/allocations
router.get(
  '/:scenarioId/allocations',
  AccessControl(
    ACM.hasPermission(PermissionsEnum.CAN_READ_SCENARIO),
    ACM.hasAccessToScenario
  ),
  ScenarioAllocationsController.getScenarioAllocations
);

// GET - /api/v1/scenarios/:scenarioId/manual-allocation
router.get(
  '/:scenarioId/manual-allocation',
  AccessControl(
    ACM.hasPermission(PermissionsEnum.CAN_READ_SCENARIO),
    ACM.hasAccessToScenario
  ),
  DemandsController.getAllOffersFromDemands
);

// PUT - /api/v1/scenarios/:scenarioId/manual-allocation
router.put(
  '/:scenarioId/manual-allocation',
  AccessControl(
    ACM.hasPermission(PermissionsEnum.CAN_READ_SCENARIO),
    ACM.hasAccessToScenario
  ),
  validate.createManualAllocations,
  checkValidation,
  ScenarioManualAllocationsController.saveManualAllocations
);

// DELETE - /api/v1/scenarios/:scenarioId
router.delete(
  '/:scenarioId',
  AccessControl(
    ACM.hasPermission(PermissionsEnum.CAN_DELETE_SCENARIO),
    ACM.hasAccessToScenario
  ),
  ScenarioDeleteController.deleteScenario
);

// POST - /api/v1/scenarios
router.post(
  '/',
  AccessControl(
    ACM.hasPermission(PermissionsEnum.CAN_CREATE_SCENARIO)
  ),
  validate.createScenario,
  checkValidation,
  ScenarioCreateController.createScenario
);

// POST - /api/v1/scenarios/:scenarioId/duplicate
router.post(
  '/:scenarioId/duplicate',
  AccessControl(
    ACM.hasPermission(PermissionsEnum.CAN_CREATE_SCENARIO),
    ACM.hasAccessToScenario
  ),
  ScenarioDuplicateController.duplicateScenario
);

// POST - /api/v1/scenarios/:scenarioId/request-review
router.post(
  '/:scenarioId/request-review',
  AccessControl(
    ACM.hasPermission(PermissionsEnum.CAN_READ_SCENARIO),
    ACM.hasAccessToScenario
  ),
  validate.reviewScenario,
  checkValidation,
  FinalScenarioReviewController.sendReviewEmail
);

// PUT - /api/v1/scenarios/:scenarioId
router.put(
  '/:scenarioId',
  AccessControl(ACM.hasAccessToScenario),
  validate.createScenario,
  checkValidation,
  ScenarioUpdateController.updateScenario
);

export default router;
