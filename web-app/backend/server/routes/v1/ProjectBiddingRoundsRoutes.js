import { Router } from 'express';

import { upload } from '../../middlewares/FileUploadMiddleware';
import { BiddingRoundsController } from '../../controllers';
import { BiddingRoundsDetailsController } from '../../controllers/projects/biddingRounds/biddingRoundDetails/BiddingRoundsDetailsController';
import { BiddingRoundsTemplateFileController } from '../../controllers/projects/biddingRounds/templateFileUpload/BiddingRoundsTemplateFileController';
import { BiddingRoundsBidsFileController } from '../../controllers/projects/biddingRounds/bidFileUpload/BiddingRoundsBidsFileController';
import { BiddingRoundsBidsDeleteController } from '../../controllers/projects/biddingRounds/bidFileDelete/BiddingRoundsBidsDeleteController';
import { BidFileDownloadController } from '../../controllers/projects/biddingRounds/bidFileDownload/BidFileDownloadController';
import { ColumnMatchingUpdateController } from '../../controllers/projects/biddingRounds/columnMatchingUpdate/ColumnMatchingUpdateController';
import { BidCorrectionsController } from '../../controllers/projects/biddingRounds/bidCorrections/BidCorrectionsController';
import { BidCorrectionsUpdateOffersController } from '../../controllers/projects/biddingRounds/bidCorrections/BidCorrectionsUpdateOffersController';
import { BiddingRoundSkuGetController } from '../../controllers/projects/biddingRounds/biddingRoundSku/BiddingRoundSkuGetController';
import { DeleteBiddingRoundsController } from '../../controllers/projects/biddingRounds/deleteRound/DeleteBiddingRoundsController';
import {
  BiddingRoundSkuCreateUpdateController,
} from '../../controllers/projects/biddingRounds/biddingRoundSku/BiddingRoundSkuCreateUpdateController';
import { BidSettingsCreateController } from '../../controllers/projects/biddingRounds/bidSettings/BidSettingsCreateController';
import { BidColumnsCreateController } from '../../controllers/projects/bidColumns/bidColumnsCreate/BidColumnsCreateController';

import { BidCalculationsController } from '../../controllers/projects/biddingRounds/bidCalculations/BidCalculationsController';
import { StatusUpdateController } from '../../controllers/projects/biddingRounds/statusUpdate/StatusUpdateController';
import validate from '../../validations/BiddingRoundsValidation';
import { checkValidation } from '../../middlewares/ValidationMiddleware';
import {
  ACM,
  AccessControl,
} from '../../middlewares/AccessControlMiddleware';

const router = Router({ mergeParams: true });

// GET - /api/v1/projects/:projectId/bidding_rounds
router.get(
  '/',
  AccessControl(
    ACM.hasAccessToProject
  ),
  BiddingRoundsController.getBiddingRoundsList
);

// GET - /api/v1/projects/:projectId/bidding_rounds/:roundId
router.get(
  '/:roundId',
  AccessControl(
    ACM.hasAccessToProject,
    ACM.roundBelongsToProject
  ),
  BiddingRoundsDetailsController.getBidRoundDetails
);

// GET - /api/v1/projects/:projectId/bidding_rounds/:roundId/table_content
router.get(
  '/:roundId/table_content',
  AccessControl(
    ACM.hasAccessToProject,
    ACM.roundBelongsToProject
  ),
  BiddingRoundsController.getTableContentList
);

// GET - /api/v1/projects/:projectId/bidding_rounds/:roundId/suppliers
router.get(
  '/:roundId/suppliers',
  AccessControl(
    ACM.hasAccessToProject,
    ACM.roundBelongsToProject
  ),
  BiddingRoundsController.getInvitedSuppliersList
);

// DELETE - /api/v1/projects/:projectId/bidding_rounds/:roundId
router.delete(
  '/:roundId',
  AccessControl(
    ACM.hasAccessToProject,
    ACM.roundBelongsToProject
  ),
  DeleteBiddingRoundsController.deleteBidRound
);

// DELETE - /api/v1/projects/:projectId/bidding_rounds/:roundId/supplier/:supplierId
router.delete(
  '/:roundId/supplier/:supplierId',
  AccessControl(
    ACM.hasAccessToProject,
    ACM.roundBelongsToProject
  ),
  BiddingRoundsController.removeSupplierFromRound
);

// POST - /api/v1/projects/:projectId/bidding_rounds
router.post(
  '/',
  AccessControl(ACM.hasAccessToProject),
  validate.createBidRound,
  checkValidation,
  BiddingRoundsController.createBidRound
);

// PUT - /api/v1/projects/:projectId/bidding_rounds/:roundId/calculation
router.put(
  '/:roundId/calculation',
  AccessControl(
    ACM.hasAccessToProject,
    ACM.roundBelongsToProject
  ),
  validate.createOrUpdateCalculation,
  checkValidation,
  BidCalculationsController.createOrUpdateCalculation
);

// POST - /api/v1/projects/:projectId/bidding_rounds/:roundId/invite
router.post(
  '/:roundId/invite',
  AccessControl(
    ACM.hasAccessToProject,
    ACM.roundBelongsToProject
  ),
  validate.inviteSupplier,
  checkValidation,
  BiddingRoundsController.inviteSupplier
);

// POST - /api/v1/projects/:projectId/bidding_rounds/:roundId/invite/template/:templateId
router.post(
  '/:roundId/invite/template/:templateId',
  AccessControl(
    ACM.hasAccessToProject,
    ACM.roundBelongsToProject
  ),
  BiddingRoundsController.inviteSuppliersByTemplate
);

// POST - /api/v1/projects/:projectId/bidding_rounds/:roundId/table_content
router.post(
  '/:roundId/table_content',
  AccessControl(
    ACM.hasAccessToProject,
    ACM.roundBelongsToProject
  ),
  validate.createTableContent,
  checkValidation,
  BiddingRoundsController.createTableContent
);

// POST - /api/v1/projects/:projectId/bidding_rounds/:roundId/save_demands_offers
router.post(
  '/:roundId/save_demands_offers',
  AccessControl(
    ACM.hasAccessToProject,
    ACM.roundBelongsToProject
  ),
  validate.createTableContent,
  checkValidation,
  BiddingRoundSkuCreateUpdateController.saveDemandsAndOffers
);

// GET - /api/v1/projects/:projectId/bidding_rounds/:roundId/get_demands_offers
router.get(
  '/:roundId/get_demands_offers',
  AccessControl(
    ACM.hasAccessToProject,
    ACM.roundBelongsToProject
  ),
  BiddingRoundSkuGetController.getDemandsAndOffers
);

// PUT - /api/v1/projects/:projectId/bidding_rounds/:roundId/table_design
router.put(
  '/:roundId/table_design',
  AccessControl(
    ACM.hasAccessToProject,
    ACM.roundBelongsToProject
  ),
  BidColumnsCreateController.createOrUpdateTableDesign
);

// PATCH - /api/v1/projects/:projectId/bidding_rounds/:roundId
router.patch(
  '/:roundId',
  AccessControl(
    ACM.hasAccessToProject,
    ACM.roundBelongsToProject
  ),
  validate.updateBidRound,
  checkValidation,
  BiddingRoundsController.updateBidRound
);

// PATCH - /api/v1/projects/:projectId/bidding_rounds/:roundId/bidding_settings
router.patch(
  '/:roundId/bidding_settings',
  AccessControl(
    ACM.hasAccessToProject,
    ACM.roundBelongsToProject
  ),
  validate.biddingSettings,
  checkValidation,
  BidSettingsCreateController.createSettings
);

// POST - /api/v1/projects/:projectId/bidding_rounds/:roundId/template
router.post(
  '/:roundId/template',
  AccessControl(
    ACM.hasAccessToProject,
    ACM.roundBelongsToProject
  ),
  upload.any(),
  BiddingRoundsTemplateFileController.uploadFiles
);

// POST - /api/v1/projects/:projectId/bidding_rounds/:roundId/bid_import
router.post(
  '/:roundId/bid_import',
  AccessControl(
    ACM.hasAccessToProject,
    ACM.roundBelongsToProject
  ),
  upload.any(),
  BiddingRoundsBidsFileController.uploadFiles
);

// DELETE - /api/v1/projects/:projectId/bidding_rounds/:roundId/bid_import
router.delete(
  '/:roundId/bid_import',
  AccessControl(
    ACM.hasAccessToProject,
    ACM.roundBelongsToProject
  ),
  validate.deleteBidFile,
  BiddingRoundsBidsDeleteController.deleteFiles
);

// GET - /api/v1/projects/:projectId/bidding_rounds/:roundId/bid_download
router.get(
  '/:roundId/bid_download',
  AccessControl(
    ACM.hasAccessToProject,
    ACM.roundBelongsToProject
  ),
  validate.downloadBidFile,
  BidFileDownloadController.downloadFiles
);

// PATCH - /api/v1/projects/:projectId/bidding_rounds/:roundId/column_matchings
router.patch(
  '/:roundId/column_matchings',
  AccessControl(
    ACM.hasAccessToProject,
    ACM.roundBelongsToProject
  ),
  validate.columnMatchingUpdate,
  ColumnMatchingUpdateController.updateColumnMatchings
);

// GET - /api/v1/projects/:projectId/bidding_rounds/:roundId/offer_processed
router.get(
  '/:roundId/offer_processed',
  AccessControl(
    ACM.hasAccessToProject,
    ACM.roundBelongsToProject
  ),
  BidCorrectionsController.getProcessedOffers
);

// PATCH - /api/v1/projects/:projectId/bidding_rounds/:roundId/update_offer_processed
router.patch(
  '/:roundId/update_offer_processed',
  AccessControl(
    ACM.hasAccessToProject,
    ACM.roundBelongsToProject
  ),
  validate.updateProcessedOffers,
  BidCorrectionsUpdateOffersController.updateProcessedOffers
);

// PATCH - /api/v1/projects/:projectId/bidding_rounds/:roundId/finished-status
router.patch(
  '/:roundId/finished-status',
  AccessControl(
    ACM.hasAccessToProject,
    ACM.roundBelongsToProject
  ),
  StatusUpdateController.updateBiddingRoundFinishedStatus
);

export default router;

