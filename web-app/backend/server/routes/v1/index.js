import { Router } from 'express';

import { checkToken } from '../../middlewares/AuthMiddleware';
import {
  ACM,
  AccessControl,
} from '../../middlewares/AccessControlMiddleware';
import BranchesRoutes from './BranchesRoutes';
import CompaniesRoutes from './CompaniesRoutes';
import ProjectsRoutes from './ProjectsRoutes';
import ScenariosRoutes from './ScenariosRoutes';
import UsersRoutes from './UsersRoutes';
import FilesRoutes from './FilesRoutes';
import JobsRoutes from './JobsRoutes';
import AuthRoutes from './AuthRoutes';
import MembersRoutes from './MembersRoutes';
import ProjectBiddingRoundsRoutes from './ProjectBiddingRoundsRoutes';
import RolesRoutes from './RolesRoutes';
import BiddingRoundsRoutes from './BiddingRoundsRoutes';

const router = Router();

router.use('/auth', AuthRoutes);
router.use('/users', checkToken, UsersRoutes);
router.use('/files', checkToken, FilesRoutes);
router.use('/companies', checkToken, AccessControl(ACM.isAdmin), CompaniesRoutes);
router.use('/branches', checkToken, AccessControl(ACM.isAdmin), BranchesRoutes);
router.use('/scenarios', checkToken, ScenariosRoutes);
router.use('/jobs', checkToken, JobsRoutes);
router.use('/members', checkToken, AccessControl(ACM.hasAccessToProject), MembersRoutes);
router.use('/projects', checkToken, ProjectsRoutes);
router.use('/projects/:projectId/bidding_rounds', checkToken, ProjectBiddingRoundsRoutes);
router.use('/roles', checkToken, AccessControl(ACM.isAdmin), RolesRoutes);
router.use('/bidding_rounds', checkToken, BiddingRoundsRoutes);

export default router;
