import { Router } from 'express';

import checkUploadFields from '../../middlewares/FilesMiddleware';
import { upload } from '../../middlewares/FileUploadMiddleware';
import { FilesController } from '../../controllers';
import { XlsxParserController } from '../../controllers/files/xlsxParser/XlsxParserController';
import validate from '../../validations/FilesValidation';
import { checkValidation } from '../../middlewares/ValidationMiddleware';
import {
  ACM,
  AccessControl,
} from '../../middlewares/AccessControlMiddleware';

const router = Router();

// GET - /api/v1/files/types
router.get(
  '/types',
  FilesController.getFileTypes
);

// GET - /api/v1/files/example/:type
router.get(
  '/example/:type',
  FilesController.getExampleFile
);

// GET - /api/v1/files/download/:fileId
router.get(
  '/download/:fileId',
  AccessControl(
    ACM.hasAccessToFile
  ),
  FilesController.downloadFile
);

// GET - /api/v1/files/xlsx_parse/:fileId
router.get(
  '/xlsx_parse/:fileId',
  AccessControl(
    ACM.hasAccessToFile
  ),
  XlsxParserController.parseXlsxFile
);

// GET - /api/v1/files/download/example/:type
router.get(
  '/download/example/:type',
  FilesController.downloadExampleFile
);

// DELETE - /api/v1/files/:fileId
router.delete(
  '/:fileId',
  AccessControl(
    ACM.hasAccessToFile
  ),
  FilesController.deleteFile
);

// POST - /api/v1/files
router.post(
  '/',
  upload.any(),
  checkUploadFields,
  validate.uploadFiles,
  checkValidation,
  FilesController.uploadFiles
);

export default router;
