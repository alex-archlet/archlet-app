import { Router } from 'express';

import { BiddingRoundsController } from '../../controllers';

const router = Router();

// GET - /api/v1/bidding_rounds/column_types
router.get(
  '/column_types',
  BiddingRoundsController.getColumnTypesList
);

// GET - /api/v1/bidding_rounds/suppliers
router.get(
  '/suppliers',
  BiddingRoundsController.getSuppliersList
);

// GET - /api/v1/bidding_rounds/templates
router.get(
  '/templates',
  BiddingRoundsController.getTemplatesList
);

export default router;

