import { Router } from 'express';

import { MembersController } from '../../controllers';
import validate from '../../validations/MembersValidation';
import { checkValidation } from '../../middlewares/ValidationMiddleware';

const router = Router();

// POST - /api/v1/members
router.post(
  '/',
  validate.inviteMembers,
  checkValidation,
  MembersController.inviteMembers
);

// DELETE - /api/v1/members
router.delete(
  '/',
  validate.deleteMember,
  checkValidation,
  MembersController.deleteMember
);

export default router;
