import { Router } from 'express';

import { BranchesController } from '../../controllers';
import { checkValidation } from '../../middlewares/ValidationMiddleware';
import validate from '../../validations/BranchesValidation';

const router = Router();

// GET - /api/v1/branches
router.get(
  '/',
  BranchesController.getBranchesList
);

// GET - /api/v1/branches/:id
router.get(
  '/:id',
  BranchesController.getBranchDetails
);

// DELETE - /api/v1/branches/:id
router.delete(
  '/:id',
  BranchesController.deleteBranch
);

// POST - /api/v1/branches
router.post(
  '/',
  validate.createBranch,
  checkValidation,
  BranchesController.createBranch
);

// PATCH - /api/v1/branches/:id
router.patch(
  '/:id',
  validate.updateBranch,
  checkValidation,
  BranchesController.updateBranch
);

export default router;

