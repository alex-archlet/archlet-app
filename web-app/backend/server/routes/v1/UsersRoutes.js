import { Router } from 'express';

import { UsersController } from '../../controllers';
import { checkValidation } from '../../middlewares/ValidationMiddleware';
import validate from '../../validations/UsersValidation';
import {
  ACM,
  AccessControl,
} from '../../middlewares/AccessControlMiddleware';

const router = Router();

// GET - /api/v1/users
router.get(
  '/',
  AccessControl(
    ACM.isAdmin
  ),
  UsersController.getUsersList
);

// GET - /api/v1/users/:userId
router.get(
  '/:userId',
  AccessControl(
    ACM.isAdmin
  ),
  UsersController.getUserDetails
);

// GET - /api/v1/users/:userId/projects
router.get(
  '/:userId/projects',
  AccessControl(
    ACM.isMeOrAdmin
  ),
  UsersController.getUserProjectsList
);

// DELETE - /api/v1/users/:userId
router.delete(
  '/:userId',
  AccessControl(
    ACM.isAdmin
  ),
  UsersController.deleteUser
);

// POST - /api/v1/users
router.post(
  '/',
  validate.createUser,
  checkValidation,
  UsersController.createUser
);

// PATCH - /api/v1/users/password
router.patch(
  '/password',
  validate.changePassword,
  checkValidation,
  UsersController.changePassword
);

// PATCH - /api/v1/users/:userId
router.patch(
  '/:userId',
  AccessControl(
    ACM.isMeOrAdmin
  ),
  validate.updateUser,
  checkValidation,
  UsersController.updateUser
);

export default router;
