import { Router } from 'express';

import { JobsController } from '../../controllers';
import { JobsCancelController } from '../../controllers/projects/jobs/jobsCancel/JobsCancelController';
import { ColumnMatchingJobController } from '../../controllers/jobs/columnMatchingJob/ColumnMatchingJobController';
import { OfferProcessingJobController } from '../../controllers/jobs/offerProcessingJob/OfferProcessingJobController';
import { ExpressionExampleJobController } from '../../controllers/jobs/expressionExampleJob/ExpressionExampleJobController';
import { ExpressionEvaluationJobController } from '../../controllers/jobs/expressionEvaluationJob/ExpressionEvaluationJobController';
import validate from '../../validations/JobsValidation';
import { checkValidation } from '../../middlewares/ValidationMiddleware';
import {
  ACM,
  AccessControl,
} from '../../middlewares/AccessControlMiddleware';

const router = Router();

// GET - /api/v1/jobs/project/:projectId/:jobId
router.get(
  '/project/:projectId/:jobId',
  validate.projectJob,
  checkValidation,
  JobsController.getProjectJobStatus
);

// DELETE - /api/v1/jobs/project/:projectId/:jobId
router.delete(
  '/project/:projectId/:jobId',
  validate.projectJob,
  checkValidation,
  JobsCancelController.cancelJob
);

// GET - /api/v1/jobs/:type/project/:projectId
router.get(
  '/:type/project/:projectId',
  AccessControl(
    ACM.hasAccessToProject
  ),
  JobsController.getLastJobResultRequest
);

// GET - /api/v1/jobs/:type/project/:projectId/scenario/:scenarioId
router.get(
  '/:type/project/:projectId/scenario/:scenarioId',
  AccessControl(
    ACM.hasAccessToProject,
    ACM.hasAccessToScenario
  ),
  JobsController.getLastJobResultRequest
);

// POST - /api/v1/jobs/files_validation
router.post(
  '/files_validation',
  validate.runFilesValidationJob,
  checkValidation,
  JobsController.runFilesValidationJob
);

// POST - /api/v1/jobs/scenario_postprocessing
router.post(
  '/scenario_postprocessing',
  validate.runPostprocessingJob,
  checkValidation,
  JobsController.runPostprocessingJob
);

// POST - /api/v1/jobs/scenario_optimization
router.post(
  '/scenario_optimization',
  validate.runScenarioOptimizationJob,
  checkValidation,
  JobsController.runScenarioOptimizationJob
);

// POST - /api/v1/jobs/column_matching
router.post(
  '/column_matching',
  validate.runColumnMatchingJob,
  checkValidation,
  ColumnMatchingJobController.runColumnMatchingJob
);

// POST - /api/v1/jobs/jobs/offer_processing
router.post(
  '/offer_processing',
  validate.runOfferProcessingJob,
  checkValidation,
  OfferProcessingJobController.runOfferProcessingJob
);

// POST - /api/v1/jobs/expression_example
router.post(
  '/expression_example',
  validate.runExpressionExampleJob,
  checkValidation,
  ExpressionExampleJobController.runExpressionExampleJob
);

// POST - /api/v1/jobs/expression_evaluation
router.post(
  '/expression_evaluation',
  validate.runExpressionEvaluationJob,
  checkValidation,
  ExpressionEvaluationJobController.runExpressionEvaluationJob
);

export default router;
