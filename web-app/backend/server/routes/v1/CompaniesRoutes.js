import { Router } from 'express';

import { CompaniesController } from '../../controllers';
import validate from '../../validations/CompaniesValidation';
import { checkValidation } from '../../middlewares/ValidationMiddleware';

const router = Router();

// GET - /api/v1/companies
router.get(
  '/',
  CompaniesController.getCompaniesList
);

// GET - /api/v1/companies/:id
router.get(
  '/:id',
  CompaniesController.getCompanyDetails
);

// GET - /api/v1/companies/:id/branches
router.get(
  '/:id/branches',
  CompaniesController.getCompanyBranchesList
);

// DELETE - /api/v1/companies/:id
router.delete(
  '/:id',
  CompaniesController.deleteCompany
);

// POST - /api/v1/companies
router.post(
  '/',
  validate.createCompany,
  checkValidation,
  CompaniesController.createCompany
);

// PATCH - /api/v1/companies/:id
router.patch(
  '/:id',
  validate.updateCompany,
  checkValidation,
  CompaniesController.updateCompany
);

export default router;

