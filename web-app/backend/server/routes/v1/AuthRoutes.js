import { Router } from 'express';
import passport from 'passport';

import { checkToken } from '../../middlewares/AuthMiddleware';
import { ForgotPasswordController } from '../../controllers/auth/forgotPassword/ForgotPasswordController';
import { SignOutController } from '../../controllers/auth/signOut/SignOutController';
import { SignInController } from '../../controllers/auth/signIn/SignInController';
import { GetMeController } from '../../controllers/auth/me/GetMeController';
import { Auth0CallbackController } from '../../controllers/auth/auth0Callback/Auth0CallbackController';
import { checkValidation } from '../../middlewares/ValidationMiddleware';
import validate from '../../validations/AuthValidation';

const router = Router();

// GET - /api/v1/auth/me
router.get(
  '/me',
  checkToken,
  GetMeController.getLoggedUser
);

// GET - /api/v1/auth/login - route to redirect the user to auth0 / to our app if logged in
router.get('/login',
  (req, _, next) => {
    const { query } = req;

    // set return to path into the session to be consumed later
    req.session.returnTo = query.returnTo || '';
    next();
  },
  passport.authenticate('auth0', {
    scope: 'openid email profile',
  }),
  (req, res) => {
    // when user was logged in already, this callback will be called
    // TODO: set path should be provided by the frontend so that we could send them back
    res.redirect('/');
  });

// GET - /api/v1/auth/callback - called when user is logged in via auth0
router.get('/callback', Auth0CallbackController.auth0Callback);

// POST - /api/v1/auth/signin
router.post(
  '/signin',
  validate.signIn,
  checkValidation,
  SignInController.signIn
);

// POST - /api/v1/auth/signout
router.post(
  '/signout',
  SignOutController.signOut
);

// POST - /api/v1/auth/forgotpassword
router.post(
  '/forgotpassword',
  validate.forgotPassword,
  checkValidation,
  ForgotPasswordController.forgotPassword
);

export default router;
