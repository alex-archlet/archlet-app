import {
  Response,
  Router,
} from 'express';
import { Branches } from '../../models';
import { getQueueConnection } from '../../services/queue';

const router = Router();

// just run a database query and check if queue connection is doable to validate service health
router.get('/', async (_, res: Response) => {
  await Branches.count();
  await getQueueConnection();
  res.sendStatus(200);
});

export default router;

