import { v4 as uuidv4 } from 'uuid';

interface ItemWithId {
  id: string;
}

type NewIdMap = Record<string, string>;

export const getNewIdsForArray = <T extends ItemWithId>(items: T[]): NewIdMap => items.reduce((acc, item) => {
  acc[item.id] = uuidv4();

  return acc;
}, {});
