export function sortByKeyDesc<T>(key: string) {
  return (a: T, b: T) => b[key] - a[key];
}

export function arrayToMap<T>(array: T[], key: string = 'id'): { [key: string]: T } {
  const result = {};

  if (!array) {
    return result;
  }

  array.forEach((item) => { result[item[key]] = item; });

  return result;
}
