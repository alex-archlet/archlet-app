import fs from 'fs';

export const shallowCompareTwoObjects = (obj1, obj2) => {
  const diffObj = {};

  if (!obj2) {
    return obj1;
  }
  Object.keys(obj1).forEach((obj1Keys) => {
    if (obj1[`${obj1Keys}`] !== obj2[`${obj1Keys}`]) {
      diffObj[`${obj1Keys}`] = obj1[`${obj1Keys}`];
    }
  });

  return diffObj;
};

const removeElements = (object, elements) => {
  const result = object;

  elements.forEach((element) => {
    result[element] = undefined;
  });

  return JSON.parse(JSON.stringify(result));
};

const getSplitedFileName = (filename, type, version, ext) => {
  const name = filename.replace('.xlsx', '');
  let extension = ext;

  if (!extension) {
    extension = `.${filename.substr(filename.lastIndexOf('.') + 1)}`;
  }

  return [
    name,
    `_v${version}`,
    extension,
  ];
};

const moveFile = (oldPath, newPath, callback) => {
  fs.rename(oldPath, newPath, (err) => {
    if (err) {
      console.error(err);

      if (err.code === 'EXDEV') {
        const readStream = fs.createReadStream(oldPath);
        const writeStream = fs.createWriteStream(newPath);

        readStream.on('error', callback);
        writeStream.on('error', callback);

        readStream.on('close', () => {
          fs.unlink(oldPath, callback);
        });

        readStream.pipe(writeStream);
      } else {
        callback(err);
      }

      return false;
    }

    if (callback) {
      callback();
    }

    return true;
  });
};

export {
  getSplitedFileName,
  moveFile,
  removeElements,
};
