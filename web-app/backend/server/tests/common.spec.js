import 'jest';
import { shallowCompareTwoObjects } from '../utils/common';

const obj1 = {
  hello: true,
  hello1: false,
};

const obj2 = {
  hello: true,
  hello1: true,
};

describe('Shallow Compare Functions', () => {
  it('if the object is the same then, it will return the empty object', () => {
    const emptyObj = shallowCompareTwoObjects(obj1, obj1);

    expect(
      emptyObj
    ).toStrictEqual({});
  });

  it('if the object is the different then', () => {
    const emptyObj = shallowCompareTwoObjects(obj1, obj2);

    expect(
      emptyObj
    ).toStrictEqual({ hello1: false });
  });
});
