import { body } from 'express-validator';

import {
  ErrorMessageEnum,
  AccountTypeEnum,
} from '../enums';

const validate = {
  createBranch: [
    body('name')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING),
    body('companyId')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
    body('accountType')
      .optional()
      .isIn([
        AccountTypeEnum.USER,
        AccountTypeEnum.ADMIN,
      ])
      .withMessage(`Please choose one of the available option: "${AccountTypeEnum.USER}" or "${AccountTypeEnum.ADMIN}"`),
    body('location')
      .optional()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING),
    body('json')
      .optional()
      .isJSON()
      .withMessage(ErrorMessageEnum.IS_JSON)
      .bail()
      .customSanitizer(value => JSON.parse(value)),
  ],
  updateBranch: [
    body('name')
      .optional()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING),
    body('accountType')
      .optional()
      .isIn([
        AccountTypeEnum.USER,
        AccountTypeEnum.ADMIN,
      ])
      .withMessage(`Please choose one of the available option: "${AccountTypeEnum.USER}" or "${AccountTypeEnum.ADMIN}"`),
    body('location')
      .optional()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING),
    body('json')
      .optional()
      .isJSON()
      .withMessage(ErrorMessageEnum.IS_JSON)
      .bail()
      .customSanitizer(value => JSON.parse(value)),
  ],
};

export default validate;
