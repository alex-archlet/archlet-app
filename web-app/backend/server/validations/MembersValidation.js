import { body } from 'express-validator';

import { ErrorMessageEnum } from '../enums';

const validate = {
  inviteMembers: [
    body('projectId')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
    body('emails')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
  ],
  deleteMember: [
    body('projectId')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
    body('ids')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
  ],
};

export default validate;
