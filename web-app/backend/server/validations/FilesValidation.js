import { body } from 'express-validator';

import { ErrorMessageEnum } from '../enums';

const validate = {
  uploadFiles: [
    body('projectId')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
  ],
};

export default validate;
