import Sequelize from 'sequelize';
import { body } from 'express-validator';

import {
  Projects,
  ProjectAccesses,
  Users,
} from '../models';
import { ErrorMessageEnum } from '../enums';

const projectNameTaken = async (name, { req }) => {
  const { projectId } = req.params;
  const { branchId } = req.decodedToken;
  let where = { name };

  if (projectId) {
    where = {
      ...where,
      id: { [Sequelize.Op.not]: projectId },
    };
  }

  const project = await Projects.findOne({
    raw: true,
    include: [
      {
        as: 'users',
        model: Users,
        required: true,
        where: {
          branch_id: branchId,
        },
        through: {
          model: ProjectAccesses,
        },
      },
    ],
    where,
  });

  if (project) {
    throw new Error(ErrorMessageEnum.NAME_TAKEN);
  }

  return true;
};

const createProject = [
  body('name')
    .exists()
    .withMessage(ErrorMessageEnum.IS_REQUIRED)
    .bail()
    .isString()
    .withMessage(ErrorMessageEnum.IS_STRING)
    .bail()
    .custom(projectNameTaken),
  body('status')
    .exists()
    .withMessage(ErrorMessageEnum.IS_REQUIRED)
    .bail()
    .isString()
    .withMessage(ErrorMessageEnum.IS_STRING),
  body('unitId')
    .exists()
    .withMessage(ErrorMessageEnum.IS_REQUIRED),
  body('currencyId')
    .exists()
    .withMessage(ErrorMessageEnum.IS_REQUIRED),
];

const createOrUpdateTableDesign = [
  body('columns')
    .exists()
    .withMessage(ErrorMessageEnum.IS_REQUIRED)
    .bail()
    .isArray()
    .withMessage(ErrorMessageEnum.IS_ARRAY),
  body('columns[*].name')
    .exists()
    .withMessage(ErrorMessageEnum.IS_REQUIRED),
  body('columns[*].typeId')
    .optional({ nullable: true })
    .isInt()
    .withMessage(ErrorMessageEnum.IS_NUMERIC),
  body('columns[*].inputBy')
    .exists()
    .withMessage(ErrorMessageEnum.IS_REQUIRED)
    .isIn([
      'buyer',
      'supplier',
    ])
    .withMessage('Please choose one of the available option: "buyer" or "supplier"'),
  body('columns[*].isMandatory')
    .exists()
    .withMessage(ErrorMessageEnum.IS_REQUIRED)
    .bail()
    .isBoolean()
    .withMessage(ErrorMessageEnum.IS_BOOLEAN),
  body('columns[*].isVisible')
    .optional()
    .isBoolean()
    .withMessage(ErrorMessageEnum.IS_BOOLEAN),
  body('columns[*].limitMin')
    .optional({ checkFalsy: true })
    .isFloat()
    .withMessage(ErrorMessageEnum.IS_NUMERIC),
  body('columns[*].limitMax')
    .optional({ checkFalsy: true })
    .isFloat()
    .withMessage(ErrorMessageEnum.IS_NUMERIC),
  body('columns[*].singleChoice')
    .optional({ checkFalsy: true })
    .isArray()
    .withMessage(ErrorMessageEnum.IS_ARRAY),
  body('columns[*].id')
    .optional()
    .isUUID('4')
    .withMessage(ErrorMessageEnum.IS_UUID4),
];

const validate = {
  createProject: [
    ...createProject,
    body('categoryId')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
  ],
  addProjectAccess: [
    body('categoryId')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
    body('userIds')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
  ],
  pivotTable: [
    body('groupBy.*')
      .isUUID('4')
      .withMessage(ErrorMessageEnum.IS_UUID4),
    body('results.*')
      .isUUID('4')
      .withMessage(ErrorMessageEnum.IS_UUID4),
  ],
  updateProject: createProject,
  createOrUpdateTableDesign,
};

export default validate;
