import {
  body,
  check,
  query,
} from 'express-validator';
import validator from 'validator';

import { ErrorMessageEnum } from '../enums';

const checkAdditionalColumn = async (value, { path }) => {
  const test = /(rows\[([0-9]+)].attributes.)/;
  const uuid = path.replace(test, '');

  if (!validator.isUUID(uuid, '4')) {
    throw new Error('Key must be an UUIDv4 format');
  }

  return true;
};

const validate = {
  createBidRound: [
    body('name')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING),
    body('description')
      .optional()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING),
    body('template')
      .optional()
      .isUUID('4')
      .withMessage(ErrorMessageEnum.IS_UUID4),
  ],
  updateBidRound: [
    body('name')
      .optional()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING),
    body('description')
      .optional()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING),
    body('startAt')
      .optional()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING)
      .bail()
      .toDate(),
    body('endAt')
      .optional()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING)
      .bail()
      .toDate(),
    body('reminderAt')
      .optional()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING),
    body('isCompleted')
      .optional()
      .isBoolean()
      .withMessage(ErrorMessageEnum.IS_BOOLEAN),
  ],
  createOrUpdateCalculation: [
    body('calculations.*.id')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .isString()
      .withMessage(ErrorMessageEnum.IS_UUID4),
    body('calculations.*.name')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING),

  ],
  inviteSupplier: [
    body('supplierName')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING),
    body('firstName')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING),
    body('lastName')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING),
    body('email')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING)
      .bail()
      .isEmail()
      .withMessage(ErrorMessageEnum.IS_EMAIL),
  ],
  createTableContent: [
    body('rows')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .isArray()
      .withMessage(ErrorMessageEnum.IS_ARRAY),
    check('rows[*].attributes[*]')
      .optional()
      .custom(checkAdditionalColumn),
  ],
  columnMatchingUpdate: [
    body('files')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .isArray()
      .withMessage(ErrorMessageEnum.IS_ARRAY),
    check('files[*].id')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .isUUID('4')
      .withMessage(ErrorMessageEnum.IS_UUID4),
    check('files[*].matchings[*].bid_column')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .isUUID('4')
      .withMessage(ErrorMessageEnum.IS_UUID4),
    check('files[*].matchings[*].excel_column')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .isString(),
    check('files[*].matchings[*].matching')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .isBoolean(),
    check('files[*].matchings[*].message')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .isString(),
  ],
  deleteBidFile: [
    body('fileIds')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .isArray()
      .withMessage(ErrorMessageEnum.IS_ARRAY),
    check('fileIds.*')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .isUUID('4')
      .withMessage(ErrorMessageEnum.IS_UUID4),
  ],
  downloadBidFile: [
    query('fileIds')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .isArray()
      .withMessage(ErrorMessageEnum.IS_ARRAY),
    check('fileIds.*')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .isUUID('4')
      .withMessage(ErrorMessageEnum.IS_UUID4),
  ],
  biddingSettings: [
    body('historicTargetPrices')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .isBoolean()
      .withMessage(ErrorMessageEnum.IS_BOOLEAN),
    body('historicTemplateData')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .isBoolean()
      .withMessage(ErrorMessageEnum.IS_BOOLEAN),
  ],
  updateProcessedOffers: [
    body('attributes')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .withMessage(ErrorMessageEnum.IS_JSON),
    body('processing_messages')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .withMessage(ErrorMessageEnum.IS_JSON),
    body('id')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .isUUID('4')
      .withMessage(ErrorMessageEnum.IS_UUID4),
  ],
};

export default validate;
