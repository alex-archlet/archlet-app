import Sequelize from 'sequelize';
import { body } from 'express-validator';

import { Companies } from '../models';
import { ErrorMessageEnum } from '../enums';

const companyNameTaken = async (name, { req }) => {
  const { id } = req.params;
  let where = { name };

  if (id) {
    where = {
      ...where,
      id: { [Sequelize.Op.not]: id },
    };
  }

  const company = await Companies.findOne({ where });

  if (company) {
    throw new Error(ErrorMessageEnum.NAME_TAKEN);
  }

  return true;
};

const validate = {
  createCompany: [
    body('name')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING)
      .bail()
      .custom(companyNameTaken),
  ],
  updateCompany: [
    body('name')
      .optional()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING)
      .bail()
      .custom(companyNameTaken),
  ],
};

export default validate;
