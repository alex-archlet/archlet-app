import { body } from 'express-validator';

import { ErrorMessageEnum } from '../enums';

const validate = {
  signIn: [
    body('username')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING),
    body('password')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING),
  ],
  forgotPassword: [
    body('username')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .isEmail()
      .withMessage(ErrorMessageEnum.IS_EMAIL),
  ],
};

export default validate;
