import {
  body,
  param,
} from 'express-validator';

import { ErrorMessageEnum } from '../enums';

const validate = {
  runFilesValidationJob: [
    body('projectId')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
    body('files')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
  ],
  runPostprocessingJob: [
    body('projectId')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
    body('scenarioId1')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
    body('scenarioId2')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
  ],
  runCreateScenarioJob: [
    body('projectId')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
    body('scenarioId')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
  ],
  runScenarioOptimizationJob: [
    body('scenarioId')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
  ],
  projectJob: [
    param('projectId')
      .isString()
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
    param('jobId')
      .isString()
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
  ],
  runColumnMatchingJob: [
    body('fileIds')
      .isLength({ min: 1 })
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
    body('sheetName')
      .isString()
      .isLength({ min: 1 })
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
    body('rowNumber')
      .isInt({ min: 1 })
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
    body('fileIds.*')
      .isString()
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
  ],
  runOfferProcessingJob: [
    body('projectId')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
    body('roundId')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
  ],
  runExpressionExampleJob: [
    body('projectId')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
    body('roundId')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
    body('expression')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
  ],
  runExpressionEvaluationJob: [
    body('projectId')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
    body('roundId')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
  ],
};

export default validate;
