import {
  body, param,
} from 'express-validator';

import { ErrorMessageEnum } from '../enums';

const validate = {
  createScenario: [
    body('name')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING),
    body('json')
      .optional()
      .isJSON()
      .withMessage(ErrorMessageEnum.IS_JSON)
      .bail()
      .customSanitizer(value => JSON.parse(value)),
    body('projectId')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
  ],
  createManualAllocations: [
    body('allocations')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .isArray()
      .withMessage(ErrorMessageEnum.IS_ARRAY),
    body('allocations[*].demandId')
      .exists()
      .isUUID('4')
      .withMessage(ErrorMessageEnum.IS_UUID4),
    body('allocations[*].offerId')
      .exists()
      .isUUID('4')
      .withMessage(ErrorMessageEnum.IS_UUID4),
    param('scenarioId')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
  ],
  reviewScenario: [
    body('comment')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING),
    body('email')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .isEmail()
      .withMessage(ErrorMessageEnum.IS_EMAIL),
  ],
};

export default validate;
