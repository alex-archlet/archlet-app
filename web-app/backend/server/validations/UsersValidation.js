import Sequelize from 'sequelize';
import Bcrypt from 'bcrypt';
import { body } from 'express-validator';

import { Users } from '../models';
import { ErrorMessageEnum } from '../enums';

const usernameTaken = async (username, { req }) => {
  const { id } = req.params;
  let where = { username };

  if (id) {
    where = {
      ...where,
      id: { [Sequelize.Op.not]: id },
    };
  }

  const user = await Users.findOne({
    raw: true,
    where,
  });

  if (user) {
    throw new Error(ErrorMessageEnum.NAME_TAKEN);
  }

  return true;
};

const password = (value) => {
  const regexp = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-._]).{8,}$/;

  if (!(value && regexp.test(value))) {
    throw new Error(`Password must be minimum eight characters long, contain at least one uppercase and one lowercase letter, 
    one number and one special character (#?!@$%^&*-._).`);
  }

  return true;
};

const oldPassword = async (oldPass, { req }) => {
  const { id } = req.decodedToken;

  const user = await Users.findByPk(id);
  const passwordHash = user.get('passwordHash');
  const isPasswordCorrect = await Bcrypt.compare(oldPass, passwordHash);

  if (!isPasswordCorrect) {
    throw new Error('The old password is wrong');
  }

  return true;
};

const validate = {
  createUser: [
    body('username')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING)
      .bail()
      .isEmail()
      .withMessage(ErrorMessageEnum.IS_EMAIL)
      .bail()
      .custom(usernameTaken),
    body('email')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING)
      .bail()
      .isEmail()
      .withMessage(ErrorMessageEnum.IS_EMAIL),
    body('firstName')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING),
    body('lastName')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING),
    body('password')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING)
      .bail()
      .custom(password),
    body('branchId')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED),
  ],
  updateUser: [
    body('username')
      .optional()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING)
      .bail()
      .isEmail()
      .withMessage(ErrorMessageEnum.IS_EMAIL)
      .bail()
      .custom(usernameTaken),
    body('email')
      .optional()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING)
      .bail()
      .isEmail()
      .withMessage(ErrorMessageEnum.IS_EMAIL),
    body('firstName')
      .optional()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING),
    body('lastName')
      .optional()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING),
  ],
  changePassword: [
    body('password')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING)
      .bail()
      .custom(password),
    body('oldPassword')
      .exists()
      .withMessage(ErrorMessageEnum.IS_REQUIRED)
      .bail()
      .isString()
      .withMessage(ErrorMessageEnum.IS_STRING)
      .bail()
      .custom(oldPassword),
  ],
};

export default validate;
