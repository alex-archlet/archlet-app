import Bcrypt from 'bcrypt';
import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import Auth0Strategy from 'passport-auth0';

import { UsersRepository } from '../database/repositories/UsersRepository';
import { Users } from '../models/Users';
import { NotificationService } from '../services/slack/NotificationService';
import { UnauthorizedError } from '../errors/UnauthorizedError';
import { BadRequestError } from '../errors/BadRequestError';

passport.use(new LocalStrategy({
  usernameField: 'username',
  passwordField: 'password',
}, async (username: string, password: string, done: Function) => {
  try {
    const userDocument = await UsersRepository.getByUserName(username);
    const passwordsMatch = userDocument ? await Bcrypt.compare(password, userDocument.passwordHash) : false;

    if (passwordsMatch) {
      return done(null, userDocument);
    }

    throw new BadRequestError('Username or password incorrect', 'Username or password incorrect');
  } catch (error) {
    return done(error);
  }
}));

const auth0Params = {
  domain: process.env.AUTH0_DOMAIN,
  clientID: process.env.AUTH0_CLIENT_ID,
  clientSecret: process.env.AUTH0_CLIENT_SECRET,
  callbackURL: process.env.AUTH0_CALLBACK_URL,
};

interface ProfileResponse {
  _json?: {
    email?: string;
    email_verified: boolean;
  }
}

const auth0Strategy = new Auth0Strategy(
  auth0Params,
  async (accessToken, refreshToken, extraParams, profile: ProfileResponse, done: Function) => {
    // accessToken is the token to call Auth0 API (not needed in the most cases)
    // extraParams.id_token has the JSON Web Token
    // profile has all the information from the user

    const error = new UnauthorizedError('SSO user does not have an account in the database');

    if (profile?._json.email && profile?._json.email_verified) {
      const email = profile?._json.email;

      const user = await UsersRepository.getByUserName(email);

      if (user) {
        return done(null, user);
      }

      // notify slack if auth0 provided user, but none exists in our DB
      const env = process.env.ENVIRONMENT_STRING || 'localhost';
      const payload = {
        channel: '#login-failures',
        text: `User tried to login, but no such account exists:\nE-mail: ${email}\nEnviroment: ${env}`,
      };

      NotificationService.sendSlackNotification(payload);

      error.redirectUrl = process.env.AUTH0_FAILED_URL;
    }

    console.error('SSO login failed', profile);

    return done(error, null);
  }
);

passport.use(auth0Strategy);

passport.serializeUser((user: Users, done: Function) => {
  const { id } = user;

  done(null, id);
});

passport.deserializeUser(async (userId: string, done: Function) => {
  const user = await UsersRepository.getById(userId);

  done(null, user);
});
