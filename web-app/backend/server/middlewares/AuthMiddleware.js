export const checkToken = (req, res, next) => {
  if (req.isAuthenticated()) {
    // TODO: replace decodedToken with req.user?
    req.decodedToken = req.user;

    return next();
  }

  return res.sendStatus(401);
};
