import {
  AccountTypeEnum,
} from '../enums';
import {
  BidColumns,
  BiddingRounds,
  ProjectAccesses,
  Projects,
  Scenarios,
  OptimizationFiles,
  Sequelize,
} from '../models';
import { ForbiddenError } from '../errors/ForbiddenError';

const isAdminChecks = (req) => {
  const { accountType } = req.user;

  if (accountType === AccountTypeEnum.ADMIN) {
    return true;
  }

  throw new ForbiddenError('User is not an admin');
};

const isMeOrAdminChecks = (req) => {
  const {
    accountType,
    id,
  } = req.user;
  const checkingUserId = req.params.userId;

  if ((`${id}` === checkingUserId) || (accountType === AccountTypeEnum.ADMIN)) {
    return true;
  }

  throw new ForbiddenError('User is not an admin');
};

const hasAccessToProjectChecks = async (projectAccessId, userAccessId) => {
  const result = await ProjectAccesses.findOne({
    where: {
      projectAccessId,
      userAccessId,
    },
  });

  if (result) {
    return true;
  }

  throw new ForbiddenError('User does not have access to project');
};

const hasAccessToProjectRequestChecks = (req) => {
  let { projectId } = req.params;

  if (!projectId) {
    ({ projectId } = req.body);
  }
  const userAccessId = req.user.id;

  return hasAccessToProjectChecks(projectId, userAccessId);
};

const hasAccessToProject = (argProjectId = null, argUserId = null) => {
  let projectAccessId;
  let userAccessId;

  if (argProjectId && argUserId) {
    projectAccessId = argProjectId;
    userAccessId = argUserId;
  }

  return hasAccessToProjectChecks(projectAccessId, userAccessId);
};

const hasAccessToScenarioCheck = async (projectAccessId, userAccessId) => {
  const result = await ProjectAccesses.findOne({
    where: {
      projectAccessId,
      userAccessId,
    },
  });

  if (result) {
    return true;
  }

  throw new ForbiddenError('User does not have access to scenario');
};

const hasAccessToScenarioRequestChecks = async (req) => {
  const { scenarioId } = req.params;
  const userAccessId = req.user.id;
  let projectAccessId = null;

  const scenario = await Scenarios.findByPk(scenarioId, {
    attributes: [
      [Sequelize.col('project.id'), 'projectId'],
    ],
    include: [{
      required: true,
      attributes: [],
      as: 'project',
      model: Projects,
    }],
  });

  if (scenario) {
    projectAccessId = scenario.get('projectId');
  }

  return hasAccessToScenarioCheck(projectAccessId, userAccessId);
};

const hasAccessToScenario = async (argScenarioId = null, argUserId = null) => {
  let scenarioId;
  let userAccessId;
  let projectAccessId = null;

  if (argScenarioId && argUserId) {
    scenarioId = argScenarioId;
    userAccessId = argUserId;
  }

  const scenario = await Scenarios.findByPk(scenarioId, {
    attributes: [
      [Sequelize.col('project.id'), 'projectId'],
    ],
    include: [{
      required: true,
      attributes: [],
      as: 'project',
      model: Projects,
    }],
  });

  if (scenario) {
    projectAccessId = scenario.get('projectId');
  }

  return hasAccessToScenarioCheck(projectAccessId, userAccessId);
};

const hasAccessToFile = async (req) => {
  const { fileId } = req.params;
  const userId = req.user.id;

  const file = await OptimizationFiles.findByPk(fileId);

  const projectId = file.get('projectId');
  const scenarioId = file.get('scenarioId');

  if (projectId && scenarioId) {
    await Promise.all([
      hasAccessToProject(projectId, userId),
      hasAccessToScenario(scenarioId, userId),
    ]);
  } else if (projectId) {
    await hasAccessToProject(projectId, userId);
  }

  return true;
};

const roundBelongsToProject = async (req) => {
  const {
    projectId,
    roundId,
  } = req.params;

  const bidRound = await BiddingRounds.findByPk(roundId);

  if (!bidRound) {
    throw new ForbiddenError('Round does not exist');
  }

  const projectIdToCheck = `${bidRound.get('projectId')}`;

  if (projectIdToCheck !== projectId) {
    throw new ForbiddenError('Round does not belong to project');
  }

  return true;
};

const columnBelongsToProject = async (req) => {
  const {
    projectId,
    columnId,
  } = req.params;

  const column = await BidColumns.findOne({
    where: {
      id: columnId,
      projectId,
    },
  });

  if (!column) {
    throw new ForbiddenError('Column does not belong to project');
  }

  return true;
};

const hasPermissionCheck = (existingPermissions, expectedPermission) => {
  if (existingPermissions && existingPermissions.includes(expectedPermission)) {
    return true;
  }

  throw new ForbiddenError('User is missing a required permission');
};

export const hasPermission = expectedPermission => req => hasPermissionCheck(req.user.getAllPermissionsOrThrow(), expectedPermission);

export const AccessControl = (...methods) => async (req, _, next) => {
  try {
    if (!methods.length) {
      throw new Error('No params have been pass to this function');
    }

    await Promise.all(
      methods.map(method => method(req))
    ).catch(async (error) => {
      console.info(`AccessControl: ${methods}`);

      next(error);

      throw error;
    });

    next();
  } catch (error) {
    next(error);
  }
};

// Access Control Methods
export const ACM = {
  isAdmin: isAdminChecks,
  isMeOrAdmin: isMeOrAdminChecks,
  hasAccessToProject: hasAccessToProjectRequestChecks,
  hasAccessToScenario: hasAccessToScenarioRequestChecks,
  hasAccessToFile,
  hasPermission,
  roundBelongsToProject,
  columnBelongsToProject,
};
