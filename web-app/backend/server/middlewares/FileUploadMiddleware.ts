import fs from 'fs';
import path from 'path';
import multer from 'multer';
import { v4 as uuidV4 } from 'uuid';

const uploadsDir = path.join(process.env.DATA_PATH);

if (!fs.existsSync(uploadsDir)) {
  fs.mkdirSync(uploadsDir);
}

const storage = multer.diskStorage({
  destination: (innerReq, file, cb) => {
    cb(null, uploadsDir);
  },
  filename: (innerReq, file, cb) => {
    cb(null, `${file.fieldname}-${uuidV4()}.xlsx`);
  },
});

export const upload = multer({ storage });
