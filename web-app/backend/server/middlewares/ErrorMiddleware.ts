import {
  NextFunction,
  Request,
  Response,
} from 'express';
import * as HttpStatus from 'http-status-codes';

import { BaseError } from '../errors/BaseError';
import { UnauthorizedError } from '../errors/UnauthorizedError';

// disable this rule since we need 4 params in the handler for the error handler to be called correctly
// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const handleErrors = (error: Error | BaseError, req: Request, res: Response, next: NextFunction) => {
  const internalMessage = error.message;
  let code = HttpStatus.INTERNAL_SERVER_ERROR;
  let responsePayload = null;

  if (error instanceof UnauthorizedError && error.redirectUrl) {
    return res.redirect(error.redirectUrl);
  }

  if (error instanceof BaseError) {
    code = error.options.responseCode;
    responsePayload = {
      message: error.options.userMessage,
      errors: error.options.errors,
    };
  }

  console.log(new Date(), internalMessage);
  console.log('Code: ', code);
  console.log('Response payload: ', responsePayload);
  console.log('Error: ', error);

  return res
    .status(code)
    .send(responsePayload);
};
