import {
  Request,
  Response,
  NextFunction,
} from 'express';

import { FeatureFlagsRepository } from '../database/repositories/FeatureFlagsRepository';
import { UnauthorizedError } from '../errors/UnauthorizedError';

export const hasBranchFeatureFlag = (featureFlagId: number) => async (req: Request, res: Response, next: NextFunction) => {
  // @ts-ignore implement request time for logged in user
  const { branchId } = req.decodedToken;
  const featureFlag = await FeatureFlagsRepository.getAllByIdAndBranchId(featureFlagId, branchId);

  if (featureFlag) {
    next();

    return;
  }

  throw new UnauthorizedError('Feature flag not enabled for company');
};
