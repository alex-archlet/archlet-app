const logRequestStart = (req, res, next) => {
  console.info(new Date(), `${req.method} ${req.originalUrl}`);

  next();
};

export default logRequestStart;
