import {
  Request,
  Response,
  NextFunction,
} from 'express';
import {
  ValidationError as ValidatorValidationError,
  validationResult,
} from 'express-validator';
import { ValidationErrors } from '../errors/BaseError';

import { ValidationError } from '../errors/ValidationError';

const mapValidationErrors = (errors: ValidatorValidationError[]) => errors.reduce((errorsResult, {
  param,
  msg,
}): ValidationErrors => {
  const paramExists = Object.keys(errorsResult).includes(param);
  const errorsAccumulated = paramExists ? errorsResult[param].messages.concat(msg) : [msg];

  return {
    ...errorsResult,
    [param]: errorsAccumulated,
  };
}, {});

export const checkValidation = (req: Request, _: Response, next: NextFunction) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    throw new ValidationError(mapValidationErrors(errors.array()));
  }

  next();
};
