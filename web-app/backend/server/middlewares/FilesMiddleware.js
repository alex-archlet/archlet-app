import { BadRequestError } from '../errors/BadRequestError';

import { Users } from '../models';

const checkUploadFields = async (req, res, next) => {
  const {
    files,
    decodedToken,
  } = req;
  const userId = decodedToken.id;
  const branchSettings = await Users.getBranchSettings(userId);
  const fileTypes = branchSettings.file_types;

  files.forEach((file) => {
    if (!fileTypes[file.fieldname]) {
      const message = `${file.fieldname} is an invalid file type`;

      throw new BadRequestError(message, message);
    }
  });

  return next();
};

export default checkUploadFields;
