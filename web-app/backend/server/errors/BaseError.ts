// Basic Responses
// The simplest way we handle errors is to respond with an appropriate status code.
// Some common response codes include:
// 400 BadRequestError — Client sent an invalid request — such as lacking required request body or parameter
// 401 UnauthorizedError — Client failed to authenticate with the server
// 403 ForbiddenError — Client authenticated but does not have permission to access the requested resource
// 404 NotFoundError — The requested resource does not exist
// 500 InternalServerError — A generic error occurred on the server
// 503 Service Unavailable — The requested service is not available

export interface ValidationErrors {
  [key: string]: string[];
}

interface ErrorOptions {
  userMessage: string;
  errors?: ValidationErrors;
  responseCode: number;
}

export class BaseError extends Error {
  public options: ErrorOptions;

  constructor(message: string, options: ErrorOptions) {
    super(message);
    this.options = options;
  }
}
