import * as HttpStatus from 'http-status-codes';

import { BaseError } from './BaseError';

export class BadRequestError extends BaseError {
  constructor(message: string, userMessage: string = 'default') {
    super(
      message,
      {
        responseCode: HttpStatus.BAD_REQUEST,
        userMessage,
      }
    );
  }
}
