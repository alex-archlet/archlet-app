import * as HttpStatus from 'http-status-codes';

import { BaseError } from './BaseError';

export class UnauthorizedError extends BaseError {
  public redirectUrl?: string;

  constructor(message: string) {
    super(
      message,
      {
        responseCode: HttpStatus.UNAUTHORIZED,
        userMessage: 'Unauthorized error',
      }
    );
  }
}
