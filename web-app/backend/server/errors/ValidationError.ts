import * as HttpStatus from 'http-status-codes';

import {
  BaseError,
  ValidationErrors,
} from './BaseError';

export class ValidationError extends BaseError {
  constructor(errors: ValidationErrors) {
    super(
      'Validation error occurred',
      {
        responseCode: HttpStatus.UNPROCESSABLE_ENTITY,
        userMessage: 'Validation error occurred',
        errors,
      }
    );
  }
}
