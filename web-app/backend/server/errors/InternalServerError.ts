import * as HttpStatus from 'http-status-codes';

import { BaseError } from './BaseError';

export class InternalServerError extends BaseError {
  constructor(message: string) {
    super(
      message,
      {
        responseCode: HttpStatus.INTERNAL_SERVER_ERROR,
        userMessage: 'Internal server error',
      }
    );
  }
}
