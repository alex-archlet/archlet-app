import * as HttpStatus from 'http-status-codes';

import { BaseError } from './BaseError';

export class ForbiddenError extends BaseError {
  constructor(message: string) {
    super(
      message,
      {
        responseCode: HttpStatus.FORBIDDEN,
        userMessage: 'Access forbidden',
      }
    );
  }
}
