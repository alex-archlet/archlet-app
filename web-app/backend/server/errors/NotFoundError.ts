import * as HttpStatus from 'http-status-codes';

import { BaseError } from './BaseError';

export class NotFoundError extends BaseError {
  constructor(message: string) {
    super(
      message,
      {
        responseCode: HttpStatus.NOT_FOUND,
        userMessage: 'Not found',
      }
    );
  }
}
