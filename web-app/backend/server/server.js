import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import * as Sentry from '@sentry/node';
import passport from 'passport';
import session from 'express-session';
import StoreFactory from 'connect-session-sequelize';

// patch express router to catch async errors
import 'express-async-errors';

import v1ApiRoutes from './routes/v1';
import healthRoutes from './routes/health';
import { handleErrors } from './middlewares/ErrorMiddleware';
import logRequestStart from './middlewares/RequestMiddleware';
import { captureSentryException } from './sentry';
import { sequelize } from './database/instance';
// import { isProdEnv } from './config';

// set up all passport logic
import './auth/passportSetup';

const SequelizeStore = StoreFactory(session.Store);

const PORT = process.env.REST_API_SERVER_PORT || 4201;
const SENTRY_DSN_KEY = process.env.SENTRY_DSN;
const environment = process.env.ENVIRONMENT_STRING;

const app = express();

Sentry.init({
  dsn: environment && SENTRY_DSN_KEY,
  environment,
});

// The request handler must be the first middleware on the app
app.use(Sentry.Handlers.requestHandler());

app.use(cors({
  exposedHeaders: ['Link'],
  credentials: true,
  origin: environment ? undefined : true, // TODO: Define archlet path here instead of the undefined - https://xx.archlet.io
}));

app.use(bodyParser.urlencoded({
  limit: '25mb',
  extended: true,
}));
app.use(bodyParser.json({
  limit: '25mb',
}));

app.use(session({
  secret: process.env.APP_SECRET_KEY,
  store: new SequelizeStore({
    db: sequelize,
    table: 'Sessions',
  }),
  resave: true,
  saveUninitialized: true,
  proxy: true,
  cookie: {
    maxAge: 24 * 60 * 60 * 1000,
    httpOnly: true,
    // TODO: find out why secure cookies don't work with a reverse proxy
    // secure: isProdEnv,
  },
}));
app.use(passport.initialize());
app.use(passport.session());
app.enable('trust proxy');

app.use(logRequestStart);

app.use('/api/v1', v1ApiRoutes);

app.use('/health', healthRoutes);

app.use(captureSentryException);

app.use(handleErrors);

app.listen(PORT, () => {
  console.log('REST API Server is running on port:', PORT);
});

export { app };
