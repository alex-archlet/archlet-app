export const isProdEnv = process.env.NODE_ENV === 'production';

export const publicDomainCn = process.env.PUBLIC_DOMAIN_CN;
