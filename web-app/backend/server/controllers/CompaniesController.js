import {
  Branches,
  Companies,
} from '../models';
import { NotFoundError } from '../errors/NotFoundError';

class CompaniesController {
  static async getCompaniesList(req, res) {
    const companies = await Companies.findAll();

    return res.json(companies);
  }

  static async getCompanyBranchesList(req, res) {
    const { id } = req.params;
    const company = await Companies.findByPk(id);

    if (!company) {
      const message = 'The company does not exist';

      throw new NotFoundError(message);
    }

    const companyBranches = await Branches.findAll({
      where: { companyId: id },
    });

    return res.json(companyBranches);
  }

  static async getCompanyDetails(req, res) {
    const { id } = req.params;
    const companyDetails = await Companies.findByPk(id);

    if (!companyDetails) {
      const message = 'The company does not exist';

      throw new NotFoundError(message);
    }

    return res.json(companyDetails);
  }

  static async createCompany(req, res) {
    const {
      name,
    } = req.body;

    const newCompany = await Companies.create({
      name,
    });

    return res.json(newCompany);
  }

  static async deleteCompany(req, res) {
    const { id } = req.params;

    const result = await Companies.destroy({
      where: { id },
    });

    if (!result) {
      const message = 'The company does not exist';

      throw new NotFoundError(message);
    }

    return res.json(null);
  }

  static async updateCompany(req, res) {
    const { id } = req.params;
    const {
      name,
    } = req.body;
    const company = await Companies.findByPk(id);

    if (!company) {
      const message = 'The company does not exist';

      throw new NotFoundError(message);
    }

    const companyData = company.dataValues;
    const entry = {
      name: name || companyData.name,
    };

    await Companies.update(
      entry,
      { where: { id } }
    );

    return res.json(entry);
  }
}

export default CompaniesController;
