import {
  Request,
  Response,
} from 'express';

import { OffersRepository } from '../../database/repositories/OffersRepository';

export class DemandsController {
  public static async getAllOffersFromDemands(req: Request, res: Response) {
    let demandIds = req.query.demand as string[] | string;

    if (!Array.isArray(demandIds)) {
      demandIds = [demandIds];
    }

    const offers = await OffersRepository.getAllOffersAndDemandByDemandIds(demandIds);

    const offerMap = demandIds.map(demandId => (
      offers.filter(offer => offer.demandId === demandId)
    ));

    res.json(offerMap);
  }
}
