import Bcrypt from 'bcrypt';

import {
  Projects,
  ProjectAccesses,
  Users,
} from '../models';
import {
  AccountTypeEnum,
} from '../enums';
import { removeElements } from '../utils/common';
import { NotFoundError } from '../errors/NotFoundError';

class UsersController {
  static async getUserDetails(req, res) {
    const { userId } = req.params;
    const userDetails = await Users.findByPk(userId);

    if (!userDetails) {
      const message = 'The user does not exist';

      throw new NotFoundError(message);
    }

    return res.json(userDetails);
  }

  static async getUserProjectsList(req, res) {
    const { userId } = req.params;

    const projects = await Users.getUserProjects(userId);
    const projectsId = projects.map(project => project.get('id'));
    const userProjects = await Projects.findAll({
      include: [
        {
          as: 'users',
          attributes: [
            'firstName',
            'lastName',
          ],
          model: Users,
          required: true,
          through: {
            model: ProjectAccesses,
            where: { projectAccessId: projectsId },
          },
        },
      ],
      order: [
        [
          'updated_at',
          'DESC',
        ],
      ],
    });

    return res.json(userProjects);
  }

  static async getUsersList(req, res) {
    const users = await Users.findAll();

    return res.json(users);
  }

  static async createUser(req, res) {
    const {
      username,
      password,
      email,
      firstName,
      lastName,
      branchId,
      roleId,
    } = req.body;

    const salt = await Bcrypt.genSalt(11);
    const hash = await Bcrypt.hash(password, salt);

    const entry = {
      accountType: AccountTypeEnum.USER,
      email,
      firstName,
      json: null,
      lastName,
      notes: null,
      passwordHash: hash,
      passwordSalt: salt,
      preferences: null,
      totalLogin: 0,
      username,
      branchId,
      roleId,
    };

    const result = await Users.create(entry);
    const newUser = removeElements(result.dataValues, [
      'passwordHash',
      'passwordSalt',
    ]);

    return res.json(newUser);
  }

  static async deleteUser(req, res) {
    const { userId } = req.params;

    const result = await Users.destroy({
      where: { id: userId },
    });

    if (!result) {
      const message = 'The user does not exist';

      throw new NotFoundError(message);
    }

    return res.json(null);
  }

  static async updateUser(req, res) {
    const { userId } = req.params;
    const {
      username,
      email,
      firstName,
      lastName,
      roleId,
    } = req.body;

    const user = await Users.findByPk(userId);

    if (!user) {
      const message = 'The user does not exist';

      throw new NotFoundError(message);
    }

    const userData = user.dataValues;
    const entry = {
      username: username || userData.username,
      email: email || userData.email,
      firstName: firstName || userData.firstName,
      lastName: lastName || userData.lastName,
      roleId: roleId || userData.roleId,
    };

    await Users.update(
      entry,
      { where: { id: userId } }
    );

    return res.json(entry);
  }

  static async changePassword(req, res) {
    const { id } = req.decodedToken;
    const { password } = req.body;

    const user = await Users.findByPk(id);
    const salt = await Bcrypt.genSalt(11);
    const hash = await Bcrypt.hash(password, salt);

    user.set('passwordHash', hash);
    user.set('passwordSalt', salt);
    await user.save();

    return res.json(null);
  }
}

export default UsersController;
