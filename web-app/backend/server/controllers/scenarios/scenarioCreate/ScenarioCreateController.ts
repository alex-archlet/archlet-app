import { Response } from 'express';

import { ScenarioCreateService } from '../../../services/projects/scenarios/ScenarioCreateService';
import { UserRequest } from '../../../types';

interface RequestBody {
  invalidate?: boolean;
  isDefault?: boolean;
  status?: string;
  json?: object;
  name: string;
  projectId: string;
}

interface ScenarioWithId {
  id: string;
}

export class ScenarioCreateController {
  /**
   * @api {post} scenarios Create a Scenario
   * @apiName Create a Scenario
   * @apiGroup Scenarios
   *
   * @apiInterfaceParam {RequestBody}
   *
   * @apiInterfaceSuccess {ScenarioWithId} the created scenario
   */
  public static async createScenario(req: UserRequest<any, any, RequestBody>, res: Response) {
    const { branchId } = req.user;

    const result = await ScenarioCreateService.createScenario({
      ...req.body,
      branchId,
    });

    return res.json(result);
  }
}
