import { Response } from 'express';
import { UserRequest } from '../../../types';
import { ScenarioDeleteService } from './ScenarioDeleteService';

export class ScenarioDeleteController {
  /**
   * @api {delete} scenarios/:scenarioId Delete a Scenario
   * @apiName Delete a Scenario
   * @apiGroup Scenarios
   *
   * @apiParam {string} scenarioId The scenario UUID
   */
  public static async deleteScenario(req: UserRequest, res: Response) {
    const {
      branchId,
    } = req.user;
    const { scenarioId } = req.params;

    await ScenarioDeleteService.deleteScenario(scenarioId, branchId);

    return res.json(null);
  }
}
