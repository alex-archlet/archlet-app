import { sequelize } from '../../../database/instance';
import { ScenariosRepository } from '../../../database/repositories/ScenariosRepository';
import { NotFoundError } from '../../../errors/NotFoundError';
import { Allocations } from '../../../models/Allocations';
import { ScenarioKpis } from '../../../models/ScenarioKpis';

export class ScenarioDeleteService {
  public static async deleteScenario(scenarioId: string, branchId: string) {
    return sequelize.transaction(async () => {
      const result = await ScenariosRepository.getByIdAndBranchId(
        scenarioId,
        branchId
      );

      if (!result) {
        const message = 'The scenario does not exist';

        throw new NotFoundError(message);
      }

      await ScenarioKpis.destroy({
        where: {
          scenarioId,
        },
      });

      await Allocations.destroy({
        where: {
          scenarioId,
        },
      });

      return result.destroy();
    });
  }
}
