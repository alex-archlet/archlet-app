import {
  Request, Response,
} from 'express';

import { ScenariosRepository } from '../../../database/repositories/ScenariosRepository';
import { ScenarioAllocationModel } from './ScenarioAllocationModel';
import { ForbiddenError } from '../../../errors/ForbiddenError';

export class ScenarioAllocationsController {
  public static async getScenarioAllocations(req: Request, res: Response) {
    const { scenarioId } = req.params;
    // @ts-ignore
    const { branchId } = req.decodedToken;
    const scenario = await ScenariosRepository.getScenarioWithAllocationsByIdAndBranchId(
      scenarioId,
      branchId
    );

    if (!scenario) {
      throw new ForbiddenError('Scenario is not accessible in branch');
    }

    const data = scenario.allocations.map(
      allocation => new ScenarioAllocationModel(allocation)
    );

    res.json(data);
  }
}
