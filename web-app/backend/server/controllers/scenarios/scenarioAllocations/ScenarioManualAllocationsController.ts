import {
  Request, Response,
} from 'express';

import {
  Allocations,
} from '../../../models';
import { OffersRepository } from '../../../database/repositories/OffersRepository';

const getOfferId = row => row.offerId;
const getDemandId = row => row.demandId;

const mapOffersDemandsToManualAllocations = (offersDemands, scenarioId) => offersDemands.map(offerDemand => ({
  demandId: offerDemand.demandId,
  offerId: offerDemand.id,
  totalPrice: -1,
  isManual: true,
  scenarioId,
  roundId: offerDemand.roundId,
  volume: offerDemand.totalVolume,
}));

export class ScenarioManualAllocationsController {
  public static async saveManualAllocations(req: Request, res: Response) {
    const { allocations } = req.body;

    const offerIds = allocations.map(getOfferId);
    const demandIds = allocations.map(getDemandId);

    const { scenarioId } = req.params;

    const offersDemands = await OffersRepository.getAllOffersByIdsAndDemandIds(demandIds, offerIds);

    if (offersDemands.length !== allocations.length) {
      res.json(null);
    }

    await ScenarioManualAllocationsController.deleteExistingManualAllocations(demandIds, scenarioId);

    const manualAllocations = mapOffersDemandsToManualAllocations(offersDemands, scenarioId);

    await Allocations.bulkCreate(manualAllocations);

    res.json(null);
  }

  private static async deleteExistingManualAllocations(demandIds: string[], scenarioId: string) {
    return Allocations.destroy({
      where: {
        demandId: demandIds,
        isManual: true,
        scenarioId,
      },
    });
  }
}
