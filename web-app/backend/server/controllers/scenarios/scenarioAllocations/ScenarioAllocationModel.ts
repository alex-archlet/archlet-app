import { Allocations } from '../../../models/Allocations';
import { ScenarioKpis } from '../../../models/ScenarioKpis';

const kpisToObject = (kpis?: ScenarioKpis[]) => kpis?.reduce((acc, kpi) => {
  acc[kpi.projectKpiDefId] = kpi.value;

  return acc;
}, {}) ?? {};

export class ScenarioAllocationModel {
  public id: string;

  public demandId: string;

  public offerId: string | null;

  public scenarioId: string;

  public isManual: boolean;

  public item: string;

  public supplier: string;

  public totalPrice: number;

  public volume: number;

  public offerAttributes: object;

  public demandAttributes: object;

  public kpiAttributes: object;

  constructor(allocation: Allocations) {
    this.id = allocation.id;
    this.demandId = allocation.demandId;
    this.offerId = allocation.offerId;
    this.scenarioId = allocation.scenarioId;
    this.totalPrice = allocation.totalPrice;
    this.volume = allocation.volume;
    this.isManual = allocation.isManual;

    this.item = allocation.demand.item;

    this.supplier = allocation.offer?.supplier ?? null;
    this.offerAttributes = allocation.offer?.attributes ?? {};
    this.demandAttributes = allocation.demand.attributes;
    this.kpiAttributes = kpisToObject(allocation.kpis);
  }
}
