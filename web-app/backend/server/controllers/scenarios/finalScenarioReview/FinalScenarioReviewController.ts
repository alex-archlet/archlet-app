import { Response } from 'express';
import { ProjectsRepository } from '../../../database/repositories/ProjectsRepository';
import { ScenariosRepository } from '../../../database/repositories/ScenariosRepository';
import { ScenarioReviewEmailService } from '../../../services/email/ScenarioReviewEmailService';
import { UserRequest } from '../../../types';

export class FinalScenarioReviewController {
  public static async sendReviewEmail(req: UserRequest, res: Response) {
    const { user } = req;
    const {
      comment,
      email,
    } = req.body;
    const {
      scenarioId,
    } = req.params;

    const scenario = await ScenariosRepository.getByIdAndBranchId(scenarioId, user.branchId);
    const project = await ProjectsRepository.getByIdAndBranchId(scenario.projectId, user.branchId);

    await ScenarioReviewEmailService.sendEmail(user, scenario, project, comment, email);

    res.sendStatus(200);
  }
}
