import { JobTypeEnum } from '../../../enums';

import { ScenarioFetchingService } from '../../../services/projects/scenarios/ScenarioFetchingService';
import { ScenarioDetailsModel } from '../../../services/projects/scenarios/ScenarioDetailsModel';
import { Jobs } from '../../../models/Jobs';
import { JobTypes } from '../../../models/JobTypes';
import { Scenarios } from '../../../models/Scenarios';

export class ScenarioDetailsService {
  public static async getScenarioDetails(
    scenarioId: string,
    branchId: string,
    includeLastOptimization: Boolean = false,
    includeLastValidation: Boolean = false
  ): Promise<ScenarioDetailsModel> {
    const commonOptions = {
      limit: 1,
      model: Jobs,
      order: [['updated_at', 'DESC']],
      required: false,
    };
    const options = {
      plain: true,
      include: [],
      where: {
        id: scenarioId,
        branchId,
      },
    };

    if (includeLastOptimization) {
      const optimizationTypeId = await JobTypes.getJobTypeId(
        JobTypeEnum.OPTIMIZATION
      );

      options.include.push({
        ...commonOptions,
        as: 'optimizations',
        where: {
          typeId: optimizationTypeId,
        },
      });
    }

    if (includeLastValidation) {
      const createScenarioTypeId = await JobTypes.getJobTypeId(
        JobTypeEnum.CREATE_SCENARIO
      );

      options.include.push({
        ...commonOptions,
        as: 'validations',
        where: {
          typeId: createScenarioTypeId,
        },
      });
    }

    const result = await Scenarios.findOne(options);
    const bidColumns = await ScenarioFetchingService.getScenarioBidColumns(scenarioId, branchId);

    return new ScenarioDetailsModel(result, bidColumns);
  }
}
