import { Response } from 'express';
import { NotFoundError } from '../../../errors/NotFoundError';
import { ScenarioDetailsInterface } from '../../../services/projects/scenarios/ScenarioDetailsModel';
import { UserRequest } from '../../../types';
import { ScenarioDetailsService } from './ScenarioDetailsService';

interface RequestParams {
  /**
   * The scenario UUID
   */
  scenarioId: string;
}

export class ScenarioDetailsController {
  /**
   * @api {get} scenarios/:scenarioId Get Scenario details
   * @apiName Get Scenario details
   * @apiGroup Scenarios
   *
   * @apiInterfaceParam {RequestParams}
   *
   * @apiInterfaceSuccess (../../../services/projects/scenarios/ScenarioDetailsModel.ts) {ScenarioDetailsInterface} _ the returned object
   */
  public static async getScenarioDetails(req: UserRequest<RequestParams, ScenarioDetailsInterface>, res: Response) {
    const {
      branchId,
    } = req.user;
    const { scenarioId } = req.params;
    const scenarioDetails = await ScenarioDetailsService.getScenarioDetails(scenarioId, branchId, true, true);

    if (!scenarioDetails) {
      const message = 'The scenario does not exist';

      throw new NotFoundError(message);
    }

    return res.json(scenarioDetails);
  }
}
