import { Response } from 'express';

import { UserRequest } from '../../../types';
import { ScenarioUpdateService } from './ScenarioUpdateService';

interface RequestParams {
  scenarioId: string;
}

interface RequestBody {
  /**
   * The name to set for the scenario
   */
  name: string;
  /**
   * The json object to set for the scenario
   */
  json: object;
}

interface ResponseBody extends RequestBody {}

export class ScenarioUpdateController {
  /**
   * @api {put} scenarios/:scenarioId Update a Scenario
   * @apiName Update a Scenario
   * @apiGroup Scenarios
   *
   * @apiParam {string} scenarioId The scenario UUID
   * @apiInterfaceParam {RequestBody}
   *
   * @apiInterfaceSuccess {ResponseBody}
   */
  public static async updateScenario(req: UserRequest<RequestParams, ResponseBody, RequestBody>, res: Response) {
    const {
      branchId,
    } = req.user;
    const { scenarioId } = req.params;
    const {
      name,
      json,
    } = req.body;

    const updatedScenario = await ScenarioUpdateService.updateScenario(scenarioId, branchId, name, json);

    return res.json({
      name: updatedScenario.name,
      json: updatedScenario.json,
    });
  }
}
