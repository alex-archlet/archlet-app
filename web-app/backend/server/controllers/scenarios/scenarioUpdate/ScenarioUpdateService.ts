import { ScenariosRepository } from '../../../database/repositories/ScenariosRepository';
import { NotFoundError } from '../../../errors/NotFoundError';

export class ScenarioUpdateService {
  public static async updateScenario(scenarioId: string, branchId: string, name: string, json: object) {
    const scenario = await ScenariosRepository.getByIdAndBranchId(
      scenarioId,
      branchId
    );

    if (!scenario) {
      const message = 'The scenario does not exist';

      throw new NotFoundError(message);
    }

    scenario.name = name;
    scenario.json = json;

    return scenario.save();
  }
}
