import {
  Request,
  Response,
} from 'express';

import { ScenarioKpiModel } from './ScenarioKpiModel';
import { runSelectQuery } from '../../../database/queries/utils';
import { SCENARIO_SUM_KPIS } from '../../../database/queries/scenario_sum_kpis';
import { ScenariosRepository } from '../../../database/repositories/ScenariosRepository';

export class ScenarioKpiController {
  public static async getScenarioKpi(req: Request, res: Response) {
    const projectId = req.query.projectId as string;
    let scenarioIds = req.query.scenarioIds as string[] | string;

    if (!Array.isArray(scenarioIds)) {
      scenarioIds = [scenarioIds];
    }

    if (!scenarioIds.length) {
      res.json({});
    } else {
      const scenarios = await ScenariosRepository.getAllByIdsAndProjectId(scenarioIds, projectId);
      const safeScenarioIds = scenarios.map(s => s.id);

      const scenariosKpis = await runSelectQuery(SCENARIO_SUM_KPIS, { scenarioIds: safeScenarioIds });
      const parsedScenarioKpi = scenariosKpis.map(
        scenarioKpi => new ScenarioKpiModel(scenarioKpi)
      );

      const scenarioKpiStructure = parsedScenarioKpi.reduce((accumulator: any, currentValue) => {
        const {
          scenarioId, value, kpiId,
        } = currentValue;

        return ({
          ...accumulator,
          [scenarioId]: {
            ...accumulator[scenarioId],
            [kpiId]: value,
          },
        });
      }, {});

      res.json(scenarioKpiStructure);
    }
  }
}
