import { ScenarioSumKpiResult } from '../../../database/queries/scenario_sum_kpis';

export class ScenarioKpiModel {
  public value: number;

  public scenarioId: string;

  public kpiId: string;

  constructor(scenarioKpi: ScenarioSumKpiResult) {
    this.value = scenarioKpi.value;
    this.scenarioId = scenarioKpi.scenario_id;
    this.kpiId = scenarioKpi.kpi_id;
  }
}
