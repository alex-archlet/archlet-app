import { Scenarios } from '../../../models/Scenarios';
import { Jobs } from '../../../models/Jobs';
import {
  JobTypeEnum,
} from '../../../enums';

export class ScenarioOptimizationsDuplicateService {
  public static async duplicateOptimizationJob(oldScenario: Scenarios, newScenario: Scenarios) {
    const lastOptimization = await Jobs.getLastJobResult(
      JobTypeEnum.OPTIMIZATION,
      oldScenario.projectId,
      oldScenario.branchId,
      oldScenario.id
    );

    if (lastOptimization && Object.keys(lastOptimization).length) {
      // @ts-ignore
      const optValues = { ...lastOptimization.dataValues };

      return Jobs.create({
        ...optValues,
        id: undefined,
        scenarioId: newScenario.id,
      });
    }

    return null;
  }
}
