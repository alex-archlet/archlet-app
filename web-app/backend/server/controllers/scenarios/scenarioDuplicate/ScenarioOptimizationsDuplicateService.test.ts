import { Jobs } from '../../../models/Jobs';
import { Scenarios } from '../../../models';
import { ScenarioOptimizationsDuplicateService } from './ScenarioOptimizationsDuplicateService';

describe('ScenarioOptimizationsDuplicateService', () => {
  beforeEach(jest.clearAllMocks);

  const scenarioId = 'skenaario';
  const newScenarioId = 'skenaario';
  const branchId = 'braants';
  const projectId = 'projekt';
  const roundId = 'raund';
  const oldScenarioValues = {
    id: scenarioId,
    name: 'LOL',
    isDefault: false,
    json: {},
    status: 'in_progress',
    projectId,
    roundId,
    branchId,
    output: {},
    lastOptimization: null,
  };
  const oldScenario = new Scenarios(oldScenarioValues);
  const newScenarioValues = {
    id: newScenarioId,
    name: `Copy of ${oldScenarioValues.name}`,
    isDefault: false,
    json: {},
    status: 'in_progress',
    projectId,
    roundId,
    branchId,
    output: {},
    lastOptimization: null,
  };
  const newScenario = new Scenarios(newScenarioValues);

  describe('duplicateOptimizationJob', () => {
    it('should not try to recreate if no previous optimization exists', async () => {
      Jobs.getLastJobResult = jest.fn().mockResolvedValue(null);
      Jobs.create = jest.fn();

      await ScenarioOptimizationsDuplicateService.duplicateOptimizationJob(oldScenario, newScenario);

      expect(Jobs.create).not.toHaveBeenCalled();
    });
    it('should recreate if previous optimization exists', async () => {
      const jobValues = {
        input: {},
        output: {},
        scenarioId,
        projectId,
        typeId: 1,
        statusId: 1,
        progress: 100,
        roundId,
        branchId,
      };
      const job = new Jobs({
        ...jobValues,
        id: 'job-uuid',
      });

      Jobs.getLastJobResult = jest.fn().mockResolvedValue(job);
      Jobs.create = jest.fn();

      await ScenarioOptimizationsDuplicateService.duplicateOptimizationJob(oldScenario, newScenario);

      expect(Jobs.create).toHaveBeenCalledTimes(1);
      expect(Jobs.create).toHaveBeenCalledWith({
        ...jobValues,
        scenarioId: newScenarioId,
        branchId,
        id: undefined,
      });
    });
  });
});
