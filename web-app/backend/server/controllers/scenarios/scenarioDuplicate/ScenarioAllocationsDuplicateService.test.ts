import { AllocationsRepository } from '../../../database/repositories/AllocationsRepository';
import { ScenarioKpisRepository } from '../../../database/repositories/ScenarioKpisRepository';
import { Allocations } from '../../../models';
import { ScenarioKpis } from '../../../models/ScenarioKpis';
import { Scenarios } from '../../../models/Scenarios';

import { ScenarioAllocationsDuplicateService } from './ScenarioAllocationsDuplicateService';

jest.mock('../../../utils/uuidUtils', () => ({
  __esModule: true, // this property makes it work
  getNewIdsForArray: () => ({
    'old-allocation-uuid': 'new-allocation-uuid',
  }),
}));

describe('ScenarioAllocationsDuplicateService', () => {
  beforeEach(jest.clearAllMocks);

  const scenarioId = 'skenaario';
  const newScenarioId = 'skenaario';
  const branchId = 'braants';
  const projectId = 'projekt';
  const roundId = 'raund';
  const oldScenarioValues = {
    id: scenarioId,
    name: 'LOL',
    isDefault: false,
    json: {},
    status: 'in_progress',
    projectId,
    roundId,
    branchId,
    output: {},
    lastOptimization: null,
  };
  const oldScenario = new Scenarios(oldScenarioValues);
  const newScenarioValues = {
    id: newScenarioId,
    name: `Copy of ${oldScenarioValues.name}`,
    isDefault: false,
    json: {},
    status: 'in_progress',
    projectId,
    roundId,
    branchId,
    output: {},
    lastOptimization: null,
  };
  const newScenario = new Scenarios(newScenarioValues);

  describe('duplicateAllocations', () => {
    it('should copy all allocations and kpis to new Allocations', async () => {
      const allocationValues = {
        id: 'old-allocation-uuid',
        volume: 1,
        isManual: false,
        totalPrice: 10,
        offerId: 'offer-uuid',
        demandId: 'demand-uuid',
        scenarioId,
        roundId,
      };

      AllocationsRepository.getAllByScenario = jest.fn().mockResolvedValue([
        new Allocations(allocationValues),
      ]);
      Allocations.bulkCreate = jest.fn();

      const kpiValues = {
        value: 1337,
        projectKpiDefId: 'kpi-def-uuid',
        allocationId: 'old-allocation-uuid',
        scenarioId,
      };

      ScenarioKpisRepository.getAllByScenario = jest.fn().mockReturnValue([
        new ScenarioKpis(kpiValues),
      ]);
      ScenarioKpis.bulkCreate = jest.fn();

      await ScenarioAllocationsDuplicateService.duplicateAllocations(oldScenario, newScenario);

      expect(Allocations.bulkCreate).toHaveBeenCalledWith([{
        ...allocationValues,
        scenarioId: newScenarioId,
        id: 'new-allocation-uuid',
      }]);
      expect(ScenarioKpis.bulkCreate).toHaveBeenCalledWith([{
        ...kpiValues,
        scenarioId: newScenarioId,
        allocationId: 'new-allocation-uuid',
      }]);
    });
  });
});
