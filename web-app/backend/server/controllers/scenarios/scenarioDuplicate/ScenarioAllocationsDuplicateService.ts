import { Scenarios } from '../../../models/Scenarios';
import { Allocations } from '../../../models/Allocations';
import { ScenarioKpis } from '../../../models/ScenarioKpis';

import { AllocationsRepository } from '../../../database/repositories/AllocationsRepository';
import { getNewIdsForArray } from '../../../utils/uuidUtils';
import { ScenarioKpisRepository } from '../../../database/repositories/ScenarioKpisRepository';

export class ScenarioAllocationsDuplicateService {
  public static async duplicateAllocations(oldScenario: Scenarios, newScenario: Scenarios) {
    const allAllocations = await AllocationsRepository.getAllByScenario(oldScenario, true);
    // we generate a new id map so that we can easily also use this for the KPIs later
    const newIdMap = getNewIdsForArray(allAllocations);

    const newAllocations = allAllocations.map(allocation => ({
      id: newIdMap[allocation.id],
      volume: allocation.volume,
      scenarioId: newScenario.id,
      isManual: allocation.isManual,
      totalPrice: allocation.totalPrice,
      offerId: allocation.offerId,
      demandId: allocation.demandId,
      roundId: allocation.roundId,
    }));

    await Allocations.bulkCreate(newAllocations);

    return ScenarioAllocationsDuplicateService.duplicateKpis(oldScenario, newScenario, newIdMap);
  }

  private static async duplicateKpis(oldScenario: Scenarios, newScenario: Scenarios, newIdMap: Record<string, string>) {
    const allKpis = await ScenarioKpisRepository.getAllByScenario(oldScenario, true);

    const newKpis = allKpis.map(kpi => ({
      allocationId: newIdMap[kpi.allocationId],
      scenarioId: newScenario.id,
      value: kpi.value,
      projectKpiDefId: kpi.projectKpiDefId,
    }));

    await ScenarioKpis.bulkCreate(newKpis);
  }
}
