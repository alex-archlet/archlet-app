import { ScenariosRepository } from '../../../database/repositories/ScenariosRepository';
import { Scenarios } from '../../../models';

import { ScenarioCreateService } from '../../../services/projects/scenarios/ScenarioCreateService';
import { ScenarioAllocationsDuplicateService } from './ScenarioAllocationsDuplicateService';
import { ScenarioDuplicateService } from './ScenarioDuplicateService';
import { ScenarioOptimizationsDuplicateService } from './ScenarioOptimizationsDuplicateService';

describe('ScenarioDuplicateService', () => {
  const scenarioId = 'skenaario';
  const newScenarioId = 'skenaario';
  const branchId = 'braants';
  const projectId = 'projekt';
  const roundId = 'raund';

  beforeEach(jest.clearAllMocks);

  describe('duplicateScenario', () => {
    it('should duplicate scenario and call child services', async () => {
      const oldScenarioValues = {
        id: scenarioId,
        name: 'LOL',
        isDefault: false,
        json: {},
        status: 'in_progress',
        projectId,
        roundId,
        branchId,
        output: {},
        lastOptimization: null,
      };
      const oldScenario = new Scenarios(oldScenarioValues);
      const newScenarioValues = {
        id: newScenarioId,
        name: `Copy of ${oldScenarioValues.name}`,
        isDefault: false,
        json: {},
        status: 'in_progress',
        projectId,
        roundId,
        branchId,
        output: {},
        lastOptimization: null,
      };
      const newScenario = new Scenarios(newScenarioValues);

      ScenariosRepository.getByIdAndBranchId = jest.fn().mockResolvedValue(oldScenario);
      ScenarioCreateService.createScenario = jest.fn().mockResolvedValue(newScenario);
      ScenarioOptimizationsDuplicateService.duplicateOptimizationJob = jest.fn();
      ScenarioAllocationsDuplicateService.duplicateAllocations = jest.fn();

      await ScenarioDuplicateService.duplicateScenario(scenarioId, branchId);

      expect(ScenarioCreateService.createScenario).toHaveBeenCalledWith({
        ...oldScenarioValues,
        name: newScenarioValues.name,
      });
      expect(ScenarioOptimizationsDuplicateService.duplicateOptimizationJob).toHaveBeenCalledWith(oldScenario, newScenario);
      expect(ScenarioAllocationsDuplicateService.duplicateAllocations).toHaveBeenCalledWith(oldScenario, newScenario);
    });
  });
});
