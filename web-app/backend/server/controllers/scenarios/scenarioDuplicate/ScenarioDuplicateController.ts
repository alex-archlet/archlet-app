import { Response } from 'express';
import { sequelize } from '../../../models';

import { UserRequest } from '../../../types';
import { ScenarioDuplicateService } from './ScenarioDuplicateService';

interface ScenarioWithId {
  id: string;
}

export class ScenarioDuplicateController {
  /**
   * @api {post} scenarios/:scenarioId/duplicate Duplicate a Scenario
   * @apiName Duplicate a Scenario and all of it's optimization data
   * @apiGroup Scenarios
   *
   * @apiParam {string} scenarioId The scenario UUID
   *
   * @apiInterfaceSuccess {ScenarioWithId} the duplicated scenario
   */
  public static async duplicateScenario(req: UserRequest, res: Response) {
    const {
      branchId,
    } = req.user;
    const { scenarioId } = req.params;

    return sequelize.transaction(async () => {
      const scenarioDuplicate = await ScenarioDuplicateService.duplicateScenario(scenarioId, branchId);

      return res.json(scenarioDuplicate);
    });
  }
}
