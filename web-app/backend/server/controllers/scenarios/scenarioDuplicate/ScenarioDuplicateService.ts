import { ScenariosRepository } from '../../../database/repositories/ScenariosRepository';

import { ScenarioCreateService } from '../../../services/projects/scenarios/ScenarioCreateService';
import { ScenarioAllocationsDuplicateService } from './ScenarioAllocationsDuplicateService';
import { ScenarioOptimizationsDuplicateService } from './ScenarioOptimizationsDuplicateService';

export class ScenarioDuplicateService {
  public static async duplicateScenario(scenarioId: string, branchId: string) {
    const scenario = await ScenariosRepository.getByIdAndBranchId(scenarioId, branchId);
    // @ts-ignore
    const scenarioValues = scenario.dataValues;

    const scenarioDuplicate = await ScenarioCreateService.createScenario({
      ...scenarioValues,
      name: `Copy of ${scenario.name}`,
    });

    await ScenarioOptimizationsDuplicateService.duplicateOptimizationJob(scenario, scenarioDuplicate);

    await ScenarioAllocationsDuplicateService.duplicateAllocations(scenario, scenarioDuplicate);

    return scenarioDuplicate;
  }
}
