import fs from 'fs';
import path from 'path';

import {
  OptimizationFiles,
  Users,
} from '../models';
import {
  getSplitedFileName,
  moveFile,
  removeElements,
} from '../utils/common';
import { NotFoundError } from '../errors/NotFoundError';
import { XlsxReadService } from '../services/xlsx/XlsxReadService';

class FilesController {
  static async getExampleFile(req, res) {
    const userId = req.decodedToken.id;
    const { type } = req.params;
    const branchSettings = await Users.getBranchSettings(userId);
    const fileTypes = branchSettings.file_types;
    const exampleFileId = fileTypes[type] && fileTypes[type].example;
    const file = await OptimizationFiles.findByPk(exampleFileId);

    if (!exampleFileId || !file) {
      const message = 'File type does not exist';

      throw new NotFoundError(message);
    }

    const {
      fileExtension,
      filePath,
      name,
    } = file.dataValues;

    const relativePath = path.join(filePath, name + fileExtension);
    const workbook = await XlsxReadService.parseHeaderNameToValueXlsxFile(relativePath);

    const firstSheetName = Object.keys(workbook)[0];

    const { headers } = workbook[firstSheetName];
    const rows = workbook[firstSheetName].data;

    return res.json({
      headers,
      rows,
    });
  }

  static getFileDir(companyId, branchId, projectId, fileType) {
    const relativePath = path.join('companies', `${companyId}`, 'branches', `${branchId}`, 'projects', `${projectId}`, 'user_uploads', fileType);
    const absolutePath = path.join(process.env.DATA_PATH, relativePath);

    if (!fs.existsSync(absolutePath)) {
      fs.mkdirSync(absolutePath, { recursive: true });
    }

    return absolutePath;
  }

  static async getFileTypes(req, res) {
    const userId = req.decodedToken.id;
    const branchSettings = await Users.getBranchSettings(userId);

    return res.json(branchSettings.file_types);
  }

  static async getFileVersion(projectId, type) {
    let version = 0;
    const lastVersion = await OptimizationFiles.getLastVersion(type, projectId);

    if (lastVersion) {
      version = lastVersion.fileVersion + 1;
    }

    return version;
  }

  static async addFile(data) {
    const {
      filePath,
      projectId,
      scenarioId,
      splitedFileName,
      type,
      version,
      branchId,
      roundId,
    } = data;
    const relativePath = filePath.replace(path.join(process.env.DATA_PATH, '/'), '');

    return OptimizationFiles.create({
      contentHash: 'test',
      fileExtension: splitedFileName[2],
      filePath: relativePath,
      fileType: type,
      fileVersion: version,
      name: splitedFileName[0],
      projectId,
      scenarioId,
      branchId,
      roundId,
    });
  }

  static async deleteFile(req, res) {
    const { fileId } = req.params;
    const result = await OptimizationFiles.destroy({
      where: { id: fileId },
    });

    if (!result) {
      const message = 'File does not exist';

      throw new NotFoundError(message);
    }

    return res.json(null);
  }

  static async downloadExampleFile(req, res) {
    const userId = req.decodedToken.id;
    const { type } = req.params;
    const branchSettings = await Users.getBranchSettings(userId);
    const fileTypes = branchSettings.file_types;
    const exampleFileId = fileTypes[type] && fileTypes[type].example;
    const file = await OptimizationFiles.findByPk(exampleFileId);

    if (!exampleFileId || !file) {
      const message = 'File does not exist';

      throw new NotFoundError(message);
    }

    const {
      fileExtension,
      filePath,
      name,
    } = file.dataValues;

    const absolutePath = path.join(process.env.DATA_PATH, filePath, name + fileExtension);

    return res.download(absolutePath);
  }

  static async downloadFile(req, res) {
    const { fileId } = req.params;
    const file = await OptimizationFiles.findByPk(fileId);

    if (!file) {
      const message = 'File does not exist';

      throw new NotFoundError(message);
    }

    const {
      fileExtension,
      filePath,
      fileType,
      fileVersion,
      name,
    } = file.dataValues;

    const splitedFileName = getSplitedFileName(name, fileType, fileVersion, fileExtension);
    const absolutePath = path.join(process.env.DATA_PATH, filePath, splitedFileName.join(''));

    return res.download(absolutePath);
  }

  static async uploadFiles(req, res) {
    const { user } = req;
    const userId = user.id;
    const {
      projectId,
      roundId,
    } = req.body;
    const branchSettings = await Users.getBranchSettings(userId);

    let response = {};

    const branch = user.get('branch');
    const company = branch.get('company');
    const fileTypes = branchSettings.file_types;

    await Promise.all(req.files.map(async (file) => {
      const type = file.fieldname;

      if (fileTypes[type]) {
        const newPath = FilesController.getFileDir(company.get('id'), branch.get('id'), projectId, type);
        const version = await FilesController.getFileVersion(projectId, type);
        const splitedFileName = getSplitedFileName(file.originalname, type, version);

        moveFile(file.path, path.join(newPath, splitedFileName.join('')));

        let fileResponse = await FilesController.addFile({
          filePath: newPath,
          projectId,
          splitedFileName,
          type,
          version,
          branchId: branch.id,
          roundId,
        });

        fileResponse = removeElements(fileResponse.dataValues, ['filePath']);

        response = {
          ...response,
          [type]: fileResponse,
        };
      }
    }));

    return res.json(response);
  }
}

export default FilesController;
