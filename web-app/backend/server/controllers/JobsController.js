import amqp from 'amqplib/callback_api';

import {
  BiddingRounds,
  Jobs,
  JobStatuses,
  JobTypes,
  Projects,
  ProjectAccesses,
  Scenarios,
  Translators,
  Users,
} from '../models';
import {
  JobTypeEnum,
  JobStatusEnum,
} from '../enums';

const JOB_QUEUE_SERVER_URI = process.env.QUEUE_SERVICE_URI || 'amqp://queue';

class JobsController {
  static async getProjectJobStatus(req, res) {
    const { branchId } = req.decodedToken;
    const {
      projectId,
      jobId,
    } = req.params;

    let jobResult = {};

    const job = await Jobs.findOne({
      where: {
        id: jobId,
        projectId,
        branchId,
      },
    });

    if (job) {
      jobResult = job;
    }

    return res.json(jobResult);
  }

  static async getLastJobResultRequest(req, res) {
    const { branchId } = req.decodedToken;
    const {
      type,
      projectId,
      scenarioId,
    } = req.params;

    const jobResult = await Jobs.getLastJobResult(type, projectId, branchId, scenarioId);

    return res.json(jobResult);
  }

  static async getProjectTranslator(projectId) {
    const [
      projectAccess,
      project,
    ] = await Promise.all([
      ProjectAccesses.findOne({
        where: { projectAccessId: projectId },
      }),
      Projects.findByPk(projectId),
    ]);

    const userAccessId = projectAccess.get('userAccessId');
    const user = await Users.findByPk(userAccessId);

    const branchId = user.get('branchId');
    const categoryId = project.get('categoryId');

    const translator = await Translators.findOne({
      where: {
        branchId,
        categoryId,
      },
    });

    if (!translator) {
      throw new Error('Translator does not exist');
    }

    return translator.get('optimizationFileId');
  }

  static async postJob(job_id) {
    const queue = 'task_queue';
    const msg = JSON.stringify({ job_id });

    await amqp.connect(JOB_QUEUE_SERVER_URI, (conErr, conn) => {
      if (conErr) {
        throw conErr;
      }

      conn.createChannel((err, channel) => {
        channel.assertQueue(queue, {
          durable: true,
        });
        channel.sendToQueue(queue, Buffer.from(msg), {
          persistent: true,
        });
      });
    });
  }

  static async prepareJobData(scenarioId, type) {
    const scenario = await Scenarios.findByPk(scenarioId);
    let resultType;
    let inputFiles;
    let outputFiles;

    if (!scenario) {
      throw new Error('Scenario does not exist');
    }

    const { projectId } = scenario.dataValues;

    if (JobTypeEnum.CREATE_SCENARIO === type || JobTypeEnum.OPTIMIZATION === type) {
      resultType = JobTypeEnum.FILE_PROCESSING;
    }

    const lastJobResult = await Jobs.getLastJobResult(resultType, projectId, scenario.branchId);

    // fetch last job result if they exist
    if (lastJobResult) {
      const {
        input,
        initialOutput,
      } = lastJobResult;

      if (!input.files || !initialOutput.files) {
        throw new Error('Input or output of last job result is empty');
      }

      inputFiles = input.files;
      outputFiles = initialOutput.files;
    } else {
      inputFiles = null;
      outputFiles = null;
    }

    return {
      inputFiles,
      outputFiles,
      projectId,
      scenario: scenario.dataValues,
    };
  }

  static async runFilesValidationJob(req, res) {
    const {
      branchId,
      id: userId,
    } = req.decodedToken;

    const jobResult = {};
    const {
      files,
      projectId,
    } = req.body;

    const [
      jobType,
      jobStatus,
    ] = await Promise.all([
      JobTypes.getJobTypeId(JobTypeEnum.FILE_PROCESSING),
      JobStatuses.getJobStatusId(JobStatusEnum.PENDING),
    ]);

    const translatorId = await JobsController.getProjectTranslator(projectId);

    const input = {
      files,
      params: {},
      translator: translatorId,
    };

    const newJob = await Jobs.create({
      input,
      output: {},
      projectId,
      statusId: jobStatus,
      typeId: jobType,
      branchId,
      userId,
    });

    if (newJob) {
      const { id } = newJob.dataValues;

      await JobsController.postJob(id);
      jobResult.id = newJob.id;
    }

    return res.json(jobResult);
  }

  static async runPostprocessingJob(req, res) {
    const {
      branchId,
      id: userId,
    } = req.decodedToken;
    const jobResult = {};
    const {
      projectId,
      scenarioId1,
      scenarioId2,
    } = req.body;

    const [
      jobType,
      jobStatus,
      lastOptimization1,
      lastOptimization2,
    ] = await Promise.all([
      JobTypes.getJobTypeId(JobTypeEnum.POSTPROCESSING),
      JobStatuses.getJobStatusId(JobStatusEnum.PENDING),
      Jobs.getLastJobResult(JobTypeEnum.OPTIMIZATION, projectId, branchId, scenarioId1),
      Jobs.getLastJobResult(JobTypeEnum.OPTIMIZATION, projectId, branchId, scenarioId2),
    ]);

    if (
      !Object.keys(lastOptimization1.initialOutput ||
        !Object.keys(lastOptimization2.initialOutput))
    ) {
      // TODO: what is this even?
      throw new Error('Optimizations do not have initialOutput');
    }

    const translatorId = await JobsController.getProjectTranslator(projectId);

    const input = {
      files: {
        allocations_table1: lastOptimization1.initialOutput.files.allocations_table,
        allocations_table2: lastOptimization2.initialOutput.files.allocations_table,
      },
      params: {},
      translator: translatorId,
    };

    const newJob = await Jobs.create({
      input,
      output: {},
      projectId,
      statusId: jobStatus,
      typeId: jobType,
      branchId,
      userId,
    });

    if (newJob) {
      const { id } = newJob.dataValues;

      await JobsController.postJob(id);
      jobResult.id = newJob.id;
    }

    return res.json(jobResult);
  }

  static async runScenarioOptimizationJob(req, res) {
    const {
      branchId,
      id: userId,
    } = req.decodedToken;
    const jobResult = {};
    const jobTypeKey = JobTypeEnum.OPTIMIZATION;
    const { scenarioId } = req.body;
    const [
      jobType,
      jobStatus,
      jobData,
    ] = await Promise.all([
      JobTypes.getJobTypeId(jobTypeKey),
      JobStatuses.getJobStatusId(JobStatusEnum.PENDING),
      JobsController.prepareJobData(scenarioId, jobTypeKey),
    ]);
    const {
      outputFiles,
      projectId,
      scenario,
    } = jobData;

    const translatorId = await JobsController.getProjectTranslator(projectId);

    // get last finished bidding round --> to be overwritten
    const biddingRound = await BiddingRounds.findLatestBiddingRoundByProjectId(projectId);
    let roundId;

    if (biddingRound) {
      roundId = biddingRound.id;
    }

    const input = {
      files: outputFiles,
      round_id: roundId,
      params: {
        scenario_settings: scenario.json,
      },
      translator: translatorId,
    };

    const newJob = await Jobs.create({
      input,
      output: {},
      projectId,
      scenarioId,
      statusId: jobStatus,
      typeId: jobType,
      branchId,
      userId,
    });

    if (newJob) {
      const {
        createdAt,
        id,
      } = newJob.dataValues;

      const [, updatedProject] = await Promise.all([
        Scenarios.update(
          {
            lastOptimization: createdAt,
            name: scenario.json.name,
          },
          { where: { id: scenario.id } }
        ),
        Projects.findOne({
          where: {
            id: projectId,
            branchId,
          },
        }),
      ]);

      await updatedProject.save();
      await JobsController.postJob(id);
      jobResult.id = newJob.id;
    }

    return res.json(jobResult);
  }
}

export default JobsController;
