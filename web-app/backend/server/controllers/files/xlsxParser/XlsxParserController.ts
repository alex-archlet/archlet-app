import {
  Request,
  Response,
} from 'express';

import { FileUtils } from '../../../services/files/FileUtils';
import { XlsxReadService } from '../../../services/xlsx/XlsxReadService';

interface Sheet {
  [sheetName: string]: object[][]
}

interface ResponseBody {
  /**
   * All of the sheet names
   */
  sheetNames: string[];
  /**
   * All of the data in a dictionary of sheets, where sheet names are the keys for the first level
   */
  sheets: {
    [sheetName: string]: Sheet;
  };
}

export class XlsxParserController {
  /**
   * @api {get} files/xlsx_parse/:fileId Xlsx Parsing
   * @apiName Parse an XLSX file to JSON given the file UUID
   * @apiGroup Files
   *
   * @apiParam {string} fileId File UUID string
   *
   * @apiInterfaceSuccess {ResponseBody}
   */
  public static async parseXlsxFile(req: Request, res: Response) {
    const {
      fileId,
    } = req.params;

    const {
      type, row,
    } = req.query;
    // @ts-ignore
    const { branchId } = req.decodedToken;

    const filePath = await FileUtils.getRelativeFilePath(fileId, branchId);

    const workbook = await XlsxReadService.parseXlsxFile(filePath, {
      returnType: type,
      row,
    });

    const sheetNames = Object.keys(workbook);

    res.json({
      sheetNames,
      sheets: workbook,
    });
  }
}
