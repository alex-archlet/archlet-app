import Bcrypt from 'bcrypt';

import {
  Branches,
  Users,
  SupplierAccesses,
  Projects,
  BidColumns,
  ColumnTypes,
  BiddingRounds,
  BidRoundStatuses,
  Demands,
  sequelize,
  Sequelize,
} from '../models';
import { BadRequestError } from '../errors/BadRequestError';
import { NotFoundError } from '../errors/NotFoundError';
import {
  BidRoundStatusEnum,
  AccountTypeEnum,
  UserAccessEnum,
} from '../enums';
import { BiddingRoundsFetchingService } from '../services/projects/biddingRounds/BiddingRoundsFetchingService';
import { ProjectsRepository } from '../database/repositories/ProjectsRepository';

const PROJECT_BID_COLLECTOR_ID = 2;

class BiddingRoundsController {
  static async getBiddingRoundsList(req, res) {
    const {
      branchId,
    } = req.decodedToken;
    const { projectId } = req.params;
    const biddingRounds = await BiddingRoundsFetchingService.getBiddingRoundDetailsForProject(
      projectId,
      branchId
    );

    return res.json(biddingRounds);
  }

  static async getTableContentList(req, res) {
    const { branchId } = req.decodedToken;
    const { roundId } = req.params;
    const demands = await Demands.findAll({
      where: {
        roundId,
        branchId,
      },
    });

    return res.json(demands);
  }

  static async getColumnTypesList(req, res) {
    const columnTypes = await ColumnTypes.findAll();

    return res.json(columnTypes);
  }

  static async getInvitedSuppliersList(req, res) {
    const { roundId } = req.params;
    const suppliers = await Users
      .scope('basic')
      .findAll({
        attributes: {
          include: [
            [Sequelize.col('branch.name'), 'supplierName'],
          ],
        },
        include: [
          {
            attributes: [],
            required: true,
            as: 'branch',
            model: Branches,
          },
          {
            attributes: [],
            as: 'biddingRounds',
            model: BiddingRounds,
            required: true,
            through: {
              model: SupplierAccesses,
              where: { roundAccessId: roundId },
            },
          },
        ],
      });

    return res.json(suppliers);
  }

  static async removeSupplierFromRound(req, res) {
    const {
      roundId,
      supplierId,
    } = req.params;

    const result = await SupplierAccesses.destroy({
      where: {
        userAccessId: supplierId,
        roundAccessId: roundId,
      },
    });

    if (!result) {
      const message = 'Cannot remove the supplier from this round';

      throw new BadRequestError(message, message);
    }

    return res.json(null);
  }

  static async getSuppliersList(req, res) {
    const { branchId } = req.decodedToken;

    const projects = await Projects.getProjectsRounds({
      attributes: [
        [Sequelize.col('BiddingRounds.id'), 'id'],
      ],
      where: { branchId },
    });

    const roundIds = projects.map(project => project.id);
    const rounds = await BiddingRounds.findAll({
      where: {
        id: roundIds,
        branchId,
      },
      attributes: [
        'id',
        'name',
        [Sequelize.col('suppliers->branch.name'), 'supplierName'],
      ],
      include: [
        {
          required: true,
          as: 'suppliers',
          attributes: [
            'firstName',
            'lastName',
            'email',
          ],
          model: Users,
          through: {
            attributes: [],
            model: SupplierAccesses,
          },
          include: [
            {
              attributes: [],
              as: 'branch',
              required: true,
              model: Branches,
            },
          ],
        },
      ],
    });

    return res.json(rounds);
  }

  static async getTemplatesList(req, res) {
    const { branchId } = req.decodedToken;

    const rounds = await Projects.getProjectsRounds({
      attributes: [
        [Sequelize.col('BiddingRounds.id'), 'id'],
        [Sequelize.col('BiddingRounds.name'), 'name'],
      ],
      where: { branchId },
    });

    return res.json(rounds);
  }

  static async createBidRound(req, res) {
    const {
      id: userId,
      branchId,
    } = req.decodedToken;
    const { projectId } = req.params;
    const {
      name,
      description,
      template,
    } = req.body;

    const project = await ProjectsRepository.getByIdAndBranchId(projectId, branchId);

    const isProjectBidCollector = project.biddingTypeId === PROJECT_BID_COLLECTOR_ID;
    const newBidRound = await sequelize.transaction(async () => {
      const statusId = await BidRoundStatuses.getStatusId(BidRoundStatusEnum.DRAFT);
      let result = await BiddingRounds.create(
        {
          name,
          description,
          statusId,
          projectId,
          createdById: userId,
          branchId,
        }
      );

      const newRoundId = result.id;

      if (template) {
        const bidColumns = await BidColumns.findAll(
          {
            raw: true,
            where: { roundId: template },
            order: [
              ['order', 'ASC'],
            ],
          }
        );

        const duplicatedColumns = bidColumns.map(round => ({
          name: round.name,
          typeId: round.typeId,
          initialTypeId: round.typeId,
          inputBy: round.inputBy,
          isMandatory: round.isMandatory,
          isVisible: round.isVisible,
          limitMin: round.limitMin,
          limitMax: round.limitMax,
          singleChoice: round.singleChoice,
          order: round.order,
          roundId: newRoundId,
        }));

        await BidColumns.bulkCreate(duplicatedColumns);
      } else if (isProjectBidCollector) {
        const columnTypes = await ColumnTypes.findAll({
          raw: true,
        });

        const defaultColumns = columnTypes.map((type, order) => ({
          name: type.text,
          typeId: type.id,
          initialTypeId: type.typeId,
          inputBy: type.inputBy,
          isMandatory: true,
          isVisible: true,
          roundId: newRoundId,
          order,
        }));

        await BidColumns.bulkCreate(defaultColumns);
      }
      result = await BiddingRoundsFetchingService.getBiddingRoundDetails(
        newRoundId,
        projectId,
        branchId
      );

      return result;
    });

    return res.json(newBidRound);
  }

  static async inviteSupplier(req, res) {
    const { branchId: userBranchId } = req.decodedToken;
    const {
      projectId,
      roundId,
    } = req.params;
    const {
      supplierName,
      firstName,
      lastName,
      email,
    } = req.body;

    const invitedSupplier = await sequelize.transaction(async () => {
      const bidRound = await BiddingRounds.findOne(
        {
          where: {
            id: roundId,
            projectId,
            branchId: userBranchId,
          },
        }
      );
      const [{ id: branchId }] = await Branches.findOrCreate(
        {
          /* TODO: Change when have another solver */
          raw: true,
          attributes: ['id'],
          defaults: {
            name: supplierName,
            accountType: AccountTypeEnum.SUPPLIER,
            solver: 'CBC',
          },
          where: { name: supplierName },
        }
      );

      const generatedPassword = Math.random()
        .toString(36)
        .slice(-8);
      const salt = await Bcrypt.genSalt(11);
      const hash = await Bcrypt.hash(generatedPassword, salt);

      const [supplier, isNew] = await Users
        .scope('basic')
        .findOrCreate(
          {
            attributes: ['id'],
            defaults: {
              email,
              username: email,
              firstName,
              lastName,
              accountType: AccountTypeEnum.SUPPLIER,
              passwordHash: hash,
              passwordSalt: salt,
              totalLogin: 0,
              branchId,
            },
            where: {
              email,
              username: email,
              firstName,
              lastName,
            },
          }
        );

      bidRound.addSupplier(
        supplier.id,
        { through: { accessType: UserAccessEnum.SUPPLIER } }
      );

      const invited = await Users
        .scope('basic')
        .findByPk(supplier.id, {
          attributes: {
            include: [
              [Sequelize.col('branch.name'), 'supplierName'],
            ],
          },
          include: [
            {
              attributes: [],
              required: true,
              as: 'branch',
              model: Branches,
            },
          ],
        });

      // TODO Send email to supplier
      if (isNew) {
        console.log('New supplier: ', email);
        console.log('Password: ', generatedPassword);
      }

      return invited;
    });

    return res.json(invitedSupplier);
  }

  static async inviteSuppliersByTemplate(req, res) {
    const {
      roundId,
      templateId,
    } = req.params;

    const suppliers = await SupplierAccesses.findAll({
      where: { roundAccessId: templateId },
    });

    const suppliersRows = suppliers.map(supplier => ({
      roundAccessId: roundId,
      userAccessId: supplier.userAccessId,
    }));

    const result = await SupplierAccesses.bulkCreate(suppliersRows, { ignoreDuplicates: true });

    return res.json(result);
  }

  static async createTableContent(req, res) {
    const {
      branchId,
      id: userId,
    } = req.decodedToken;
    const { roundId } = req.params;
    const { rows } = req.body;

    const contentRows = rows.map((row) => {
      const {
        item,
        volume,
        attributes,
      } = row;

      const parsedRow = {
        item,
        volume,
        attributes,
        roundId,
        branchId,
        createdById: userId,
      };

      return parsedRow;
    });

    await sequelize.transaction(async () => {
      await Demands.destroy(
        { where: { roundId } }
      );

      await Demands.bulkCreate(contentRows);

      return true;
    });

    const result = await Demands.findAll({
      where: {
        roundId,
        branchId,
      },
    });

    return res.json(result);
  }

  static async updateBidRound(req, res) {
    const {
      branchId,
      id: userId,
    } = req.decodedToken;
    const {
      roundId,
      projectId,
    } = req.params;
    const {
      name,
      description,
      startAt,
      endAt,
      reminderAt,
      isCompleted,
      settings,
    } = req.body;

    const bidRound = await BiddingRounds.findOne({
      where: {
        id: roundId,
        branchId,
      },
    });

    if (!bidRound) {
      const message = 'The bid round does not exist';

      throw new NotFoundError(message);
    }

    const bidRoundValues = bidRound.dataValues;
    const reminderAtDate = reminderAt ? new Date(reminderAt) : null;
    let reminderAtValue = bidRoundValues.reminderAt;

    if (reminderAtDate instanceof Date && !Number.isNaN(reminderAtDate)) {
      reminderAtValue = reminderAtDate;
    }

    if (reminderAt === 'reset') {
      reminderAtValue = null;
    }

    const entry = {
      name: name || bidRoundValues.name,
      description: description || bidRoundValues.description,
      startAt: startAt || bidRoundValues.startAt,
      endAt: endAt || bidRoundValues.endAt,
      reminderAt: reminderAtValue,
      updatedById: userId,
      settings,
    };

    if (isCompleted) {
      const statusId = await BidRoundStatuses.getStatusId(BidRoundStatusEnum.ACTIVE);

      entry.statusId = statusId;
    }

    await bidRound.update(
      entry,
      { where: { id: roundId } }
    );

    const updatedBidRound = await BiddingRoundsFetchingService.getBiddingRoundDetails(
      roundId,
      projectId,
      branchId
    );

    return res.json(updatedBidRound);
  }
}

export default BiddingRoundsController;
