import { Branches } from '../models';
import {
  AccountTypeEnum,
} from '../enums';
import { NotFoundError } from '../errors/NotFoundError';

class BranchesController {
  static async getBranchDetails(req, res) {
    const { id } = req.params;
    const branchDetails = await Branches.findByPk(id);

    if (!branchDetails) {
      const message = 'The branch does not exist';

      throw new NotFoundError(message);
    }

    return res.json(branchDetails);
  }

  static async getBranchesList(req, res) {
    const branches = await Branches.findAll();

    return res.json(branches);
  }

  static async createBranch(req, res) {
    const {
      name,
      location,
      companyId,
      json = {},
      accountType = AccountTypeEnum.USER,
      roleId,
    } = req.body;

    /* TODO change solver when we have other solvers */

    const newBranch = await Branches.create({
      accountType,
      companyId,
      json,
      location,
      name,
      roleId,
      solver: 'CBC',
    });

    return res.json(newBranch);
  }

  static async deleteBranch(req, res) {
    const { id } = req.params;

    const result = await Branches.destroy({
      where: { id },
    });

    if (!result) {
      const message = 'The branch does not exist';

      throw new NotFoundError(message);
    }

    return res.json(null);
  }

  static async updateBranch(req, res) {
    const { id } = req.params;
    const {
      name,
      location,
      accountType,
      json,
      companyId,
      roleId,
    } = req.body;

    const branch = await Branches.findByPk(id);

    if (!branch) {
      const message = 'The branch does not exist';

      throw new NotFoundError(message);
    }

    const branchData = branch.dataValues;
    const entry = {
      name: name || branchData.name,
      location: location || branchData.location,
      accountType: accountType || branchData.accountType,
      json: json || branchData.json,
      companyId: companyId || branchData.companyId,
      roleId: roleId || branchData.roleId,
    };

    await Branches.update(
      entry,
      { where: { id } }
    );

    return res.json(entry);
  }
}

export default BranchesController;
