import {
  Request,
  Response,
} from 'express';
import * as HttpStatus from 'http-status-codes';

import {
  Jobs,
  JobStatuses,
} from '../../../../models';
import {
  ResponseStatusEnum,
  JobStatusEnum,
} from '../../../../enums';

export class JobsCancelController {
  /**
   * @api {delete} jobs/project/:projectId/:jobId Cancel an engine job
   * @apiName Cancel an engine job
   * @apiGroup Engine Jobs
   * @apiParam {string} projectId The project UUID
   * @apiParam {string} jobId The job UUID
   */
  public static async cancelJob(req: Request, res: Response) {
    // @ts-ignore
    const { branchId } = req.decodedToken;
    const {
      projectId,
      jobId,
    } = req.params;

    let jobResult = {};

    const job = await Jobs.findOne({
      where: {
        id: jobId,
        projectId,
        branchId,
      },
    });

    const jobStatus = await JobStatuses.getJobStatusId(JobStatusEnum.CANCELED);

    if (job) {
      job.statusId = jobStatus;
      await job.save();
      jobResult = job;
    }

    return res.json(jobResult);
  }
}
