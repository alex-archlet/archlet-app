import {
  Request,
  Response,
} from 'express';

import { ProjectsRepository } from '../../../database/repositories/ProjectsRepository';
import { ProjectDataService } from '../../../services/projects/dataService/ProjectDataService';
import { NotFoundError } from '../../../errors/NotFoundError';

export class ProjectInfoController {
  public static async getProjectInfo(req: Request, res: Response) {
    // @ts-ignore
    const { branchId } = req.decodedToken;
    const { projectId } = req.params;
    const project = await ProjectsRepository.getByIdAndBranchId(projectId, branchId);

    if (!project) {
      const message = 'The project does not exist';

      throw new NotFoundError(message);
    }

    const stream = await ProjectDataService.getProjectInfo(project);

    return stream
      .on('error', (e) => {
        throw e;
      })
      .pipe(res);
  }
}
