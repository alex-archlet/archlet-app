import {
  Request, Response,
} from 'express';

import { ProjectKpiDefsRepository } from '../../../database/repositories/ProjectKpiDefsRepository';

export class ProjectKpiDefsController {
  public static async getProjectKpiDefs(req: Request, res: Response) {
    const { projectId } = req.params;
    const projectKpiDef = await ProjectKpiDefsRepository.getByProjectIdAndDefaults(projectId);

    res.json(projectKpiDef);
  }
}
