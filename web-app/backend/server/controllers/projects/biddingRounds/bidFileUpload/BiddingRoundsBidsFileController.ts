import {
  Request,
  Response,
  Express,
} from 'express';

import {
  JobTypeEnum,
  JobStatusEnum,
} from '../../../../enums';
import {
  JobTypes,
  JobStatuses,
  sequelize,
} from '../../../../models';

import { FileUploadService } from '../../../../services/files/FileUploadService';
import { Jobs } from '../../../../models/Jobs';
import { JobQueueService } from '../../../../services/jobs/JobQueueService';
import { OptimizationFilesRepository } from '../../../../database/repositories/OptimizationFilesRepository';
import { UserRequest } from '../../../../types';

export class BiddingRoundsBidsFileController {
  static async uploadFiles(req: UserRequest, res: Response) {
    const { user } = req;
    const userId = user.id;
    const {
      projectId,
      roundId,
    } = req.params;

    const { branch } = user;
    const { company } = branch;
    const files = req.files as Express.Multer.File[];

    const {
      rowNumber,
      sheetName,
    } = await OptimizationFilesRepository.getLastProjectBidTemplateFile(projectId, branch.id);

    const createdFiles = await sequelize.transaction(() => Promise.all(files.map(async file => FileUploadService.uploadFile(
      file,
      company,
      branch,
      projectId,
      'bid_import',
      null,
      roundId,
      rowNumber,
      sheetName
    ))));

    const [
      jobType,
      jobStatus,
    ] = await Promise.all([
      JobTypes.getJobTypeId(JobTypeEnum.COLUMN_MATCHING),
      JobStatuses.getJobStatusId(JobStatusEnum.PENDING),
    ]);

    const jobs = await sequelize.transaction(async () => {
      const jobsToCreate = createdFiles.map((file) => {
        const input = {
          file_id: file.id,
          round_id: file.roundId,
          params: {},
        };

        return {
          input,
          output: {},
          projectId: file.projectId,
          statusId: jobStatus,
          typeId: jobType,
          branchId: branch.id,
          userId,
        };
      });

      return Jobs.bulkCreate(jobsToCreate);
    });

    const jobIds = jobs.map(({ id }) => id);

    await JobQueueService.addJobsToQueue(jobIds);

    return res.json(jobIds);
  }
}
