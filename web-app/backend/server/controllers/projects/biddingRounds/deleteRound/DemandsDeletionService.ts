import { Demands } from '../../../../models';

export class DemandsDeletionService {
  public static async deleteDemandsByRoundIdAndBranchId(roundId: string, branchId: string) {
    return Demands.destroy({
      where: {
        roundId,
        branchId,
      },
    });
  }
}
