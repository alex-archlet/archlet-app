import { BiddingRoundsRepository } from '../../../../database/repositories/BiddingRoundsRepository';
import { NotFoundError } from '../../../../errors/NotFoundError';
import { Offers } from '../../../../models';
import { BidColumnsDeletionService } from './BidColumnsDeletionService';
import { DemandsDeletionService } from './DemandsDeletionService';

export class BiddingRoundDeletionService {
  public static async deleteBiddingRound(roundId: string, branchId: string) {
    const bidRound = await BiddingRoundsRepository.getBiddingRoundByIdAndBranchId(roundId, branchId);

    if (!bidRound) {
      const message = 'The bid round does not exist';

      throw new NotFoundError(message);
    }

    await Offers.destroy({
      where: {
        roundId,
      },
    });

    const allBiddingRounds = await BiddingRoundsRepository.getBiddingRoundByProjectIdAndBranchId(bidRound.projectId, branchId);
    const otherBiddingRounds = allBiddingRounds.filter(round => round.id !== bidRound.id);

    if (!otherBiddingRounds.length) {
      await BidColumnsDeletionService.deleteBidColumnsByProjectId(bidRound.projectId);
      await DemandsDeletionService.deleteDemandsByRoundIdAndBranchId(bidRound.id, branchId);
    }

    return bidRound.destroy();
  }
}
