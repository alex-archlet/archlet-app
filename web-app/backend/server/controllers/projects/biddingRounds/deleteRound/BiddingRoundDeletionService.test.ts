import { BiddingRoundsRepository } from '../../../../database/repositories/BiddingRoundsRepository';
import { NotFoundError } from '../../../../errors/NotFoundError';
import { BiddingRounds, Offers } from '../../../../models';
import { BidColumnsDeletionService } from './BidColumnsDeletionService';
import { BiddingRoundDeletionService } from './BiddingRoundDeletionService';
import { DemandsDeletionService } from './DemandsDeletionService';

describe('BiddingRoundDeletionService', () => {
  const roundId = 'round-uuid';
  const branchId = 'branch-uuid';
  const projectId = 'project-uuid';

  beforeEach(jest.clearAllMocks);

  const round = new BiddingRounds({
    id: roundId,
    branchId,
    projectId,
  });
  const otherRound = new BiddingRounds({
    id: 'other-round-uuid',
    branchId,
    projectId,
  });

  describe('deleteBiddingRound', () => {
    it('should throw if bidding round does not exist', async () => {
      BiddingRoundsRepository.getBiddingRoundByIdAndBranchId = jest.fn().mockResolvedValue(null);

      await expect(BiddingRoundDeletionService.deleteBiddingRound(roundId, branchId)).rejects.toThrow(NotFoundError);
    });
    it('should not delete bid columns nor demands if other rounds exist', async () => {
      BiddingRoundsRepository.getBiddingRoundByIdAndBranchId = jest.fn().mockResolvedValue(round);
      BiddingRoundsRepository.getBiddingRoundByProjectIdAndBranchId = jest.fn().mockResolvedValue([otherRound]);
      Offers.destroy = jest.fn();
      round.destroy = jest.fn();
      BidColumnsDeletionService.deleteBidColumnsByProjectId = jest.fn();
      DemandsDeletionService.deleteDemandsByRoundIdAndBranchId = jest.fn();

      await BiddingRoundDeletionService.deleteBiddingRound(roundId, branchId);

      expect(Offers.destroy).toHaveBeenCalledWith({
        where: {
          roundId,
        },
      });
      expect(round.destroy).toHaveBeenCalledTimes(1);
      expect(BidColumnsDeletionService.deleteBidColumnsByProjectId).not.toHaveBeenCalled();
      expect(DemandsDeletionService.deleteDemandsByRoundIdAndBranchId).not.toHaveBeenCalled();
    });
    it('should delete bid columns and demands if no other rounds exist', async () => {
      BiddingRoundsRepository.getBiddingRoundByIdAndBranchId = jest.fn().mockResolvedValue(round);
      BiddingRoundsRepository.getBiddingRoundByProjectIdAndBranchId = jest.fn().mockResolvedValue([]);
      Offers.destroy = jest.fn();
      round.destroy = jest.fn();
      BidColumnsDeletionService.deleteBidColumnsByProjectId = jest.fn();
      DemandsDeletionService.deleteDemandsByRoundIdAndBranchId = jest.fn();

      await BiddingRoundDeletionService.deleteBiddingRound(roundId, branchId);

      expect(Offers.destroy).toHaveBeenCalledWith({
        where: {
          roundId,
        },
      });
      expect(round.destroy).toHaveBeenCalledTimes(1);
      expect(BidColumnsDeletionService.deleteBidColumnsByProjectId).toHaveBeenCalledWith(round.projectId);
      expect(DemandsDeletionService.deleteDemandsByRoundIdAndBranchId).toHaveBeenCalledWith(round.id, branchId);
    });
  });
});
