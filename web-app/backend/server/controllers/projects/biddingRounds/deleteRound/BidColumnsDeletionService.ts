import { BidColumns } from '../../../../models';

export class BidColumnsDeletionService {
  public static async deleteBidColumnsByProjectId(projectId: string) {
    return BidColumns.destroy({
      where: {
        projectId,
      },
    });
  }
}
