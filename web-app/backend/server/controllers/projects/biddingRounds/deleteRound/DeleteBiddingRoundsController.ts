import { Response } from 'express';
import { sequelize } from '../../../../models';

import { UserRequest } from '../../../../types';
import { BiddingRoundDeletionService } from './BiddingRoundDeletionService';

export class DeleteBiddingRoundsController {
  /**
   * @api {delete} projects/:projectId/bidding_rounds/:roundId Delete a bidding round
   * @apiName Delete a bidding round (and all bid columns + demands if it is the only round)
   * @apiGroup Bidding Rounds
   * @apiParam {string} roundId The bidding round UUID
   */
  static async deleteBidRound(req: UserRequest, res: Response) {
    const { roundId } = req.params;
    const { branchId } = req.user;

    await sequelize.transaction(() => BiddingRoundDeletionService.deleteBiddingRound(roundId, branchId));

    return res.json(null);
  }
}
