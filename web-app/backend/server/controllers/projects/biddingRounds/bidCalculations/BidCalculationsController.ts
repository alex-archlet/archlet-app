import {
  Request,
  Response,
} from 'express';

import {
  BidColumns,
  ColumnTypes,
  sequelize,
} from '../../../../models';

import { BidColumnsRepository } from '../../../../database/repositories/BidColumnsRepository';
import { arrayToMap } from '../../../../utils/arrayUtils';

const PRICE_TOTAL = 'price_total';
const PRICE = 'price';

export class BidCalculationsController {
  static async createOrUpdateCalculation(req: Request, res: Response) {
    const {
      projectId,
    } = req.params;
    const { calculations } = req.body;

    // TODO: is this needed? Automatic columns could stay NULLs imo
    const lastOrderNumber = await BidColumnsRepository.getLastOrderNumberByProjectId(projectId);

    const totalPriceTypeId: number = await ColumnTypes.getTypeId(PRICE_TOTAL);
    const priceTypeId: number = await ColumnTypes.getTypeId(PRICE);
    const existingCalculations = await BidColumnsRepository.getAutomaticColumnsByProjectId(projectId);
    const existingCalculationsMap = arrayToMap(existingCalculations);

    const counter = lastOrderNumber || 0;

    await sequelize
      .transaction(() => Promise
        .all(calculations.map(({
          id,
          calculation,
          name,
          column,
        }, index) => {
          const existingCalculation = existingCalculationsMap[id];

          if (existingCalculation) {
            existingCalculation.calculation = calculation;
            existingCalculation.name = name;

            return existingCalculation.save();
          }

          // the first value should be the total price calculation
          const typeId = column === PRICE ? priceTypeId : totalPriceTypeId; 

          return BidColumns.create({
            name,
            calculation,
            typeId,
            initialTypeId: typeId,
            inputBy: 'supplier',
            isMandatory: true,
            isVisible: true,
            isAutomatic: true,
            order: counter + index,
            projectId,
          });
        })));

    const result = await BidColumnsRepository.getAllColumnsByProjectId(projectId);

    return res.json(result);
  }
}
