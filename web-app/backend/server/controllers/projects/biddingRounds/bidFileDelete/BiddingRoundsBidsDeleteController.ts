import {
  Request,
  Response,
} from 'express';

import {
  sequelize,
} from '../../../../models';
import { OptimizationFilesRepository } from '../../../../database/repositories/OptimizationFilesRepository';
import { BiddingRoundsDetailsModel } from '../../../../services/projects/biddingRounds/BiddingRoundsDetailsModel';
import { BiddingRoundsFetchingService } from '../../../../services/projects/biddingRounds/BiddingRoundsFetchingService';

type FileIdsBody = {
  fileIds: string[],
}

export class BiddingRoundsBidsDeleteController {
  public static async deleteFiles(req: Request, res: Response) {
    const {
      branchId,
      // @ts-ignore TODO: Implement Request type with token
    } = req.decodedToken;
    const {
      projectId,
      roundId,
    } = req.params;
    const { fileIds } = req.body as FileIdsBody;

    const files = await OptimizationFilesRepository.getAllOptimizationFilesByIdsAndBranchIdAndBiddingRoundId(
      fileIds,
      branchId,
      roundId
    );

    await sequelize.transaction(() => Promise.all(files.map(file => file.destroy())));

    // TODO: Delete all data connected to the file - offers, etc.

    const biddingRound: BiddingRoundsDetailsModel = await BiddingRoundsFetchingService.getBiddingRoundDetails(
      roundId,
      projectId,
      branchId
    );

    res.json(biddingRound);
  }
}
