import {
  Response,
} from 'express';

import {
  sequelize,
  BidColumns,
} from '../../../../models';
import { FileUploadService } from '../../../../services/files/FileUploadService';
import { UserRequest } from '../../../../types';

export class BiddingRoundsTemplateFileController {
  static async uploadFiles(req: UserRequest, res: Response) {
    const {
      projectId,
    } = req.params;
    const { user } = req;

    const { branch } = user;
    const { company } = branch;
    const files = req.files as Express.Multer.File[];

    // TODO: check if the round has any offers before going further, since a new template would not be allowed with existing bids
    await BidColumns.destroy({
      where: {
        projectId,
      },
    });

    const newFiles = await sequelize.transaction(async () => Promise.all(files.map(async file => FileUploadService.uploadFile(
      file,
      company,
      branch,
      projectId,
      'bid_template',
      null,
      null
    ))));

    return res.json(newFiles);
  }
}
