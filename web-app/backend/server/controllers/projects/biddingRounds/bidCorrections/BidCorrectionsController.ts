import {
  Request,
  Response,
} from 'express';
import { OffersRepository } from '../../../../database/repositories/OffersRepository';

export class BidCorrectionsController {
  public static async getProcessedOffers(req: Request, res: Response) {
    const {
      roundId,
    } = req.params;
    const offers = await OffersRepository.getAllNonHistoricOffersAndDemanddByRoundId(roundId);

    res.json(offers);
  }
}
