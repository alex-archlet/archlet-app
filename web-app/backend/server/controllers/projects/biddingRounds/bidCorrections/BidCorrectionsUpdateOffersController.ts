import {
  Request,
  Response,
} from 'express';

import {
  Offers, sequelize,
} from '../../../../models';

export class BidCorrectionsUpdateOffersController {
  public static async updateProcessedOffers(req: Request, res: Response) {
    const {
      rows,
    } = req.body.payload;

    await sequelize.transaction(async () => rows.forEach(async (row) => {
      await Offers.update({
        attributes: row.attributes,
        processing_messages: row.processing_messages,
      },
      { where: { id: row.id } });
    }));

    res.json(null);
  }
}
