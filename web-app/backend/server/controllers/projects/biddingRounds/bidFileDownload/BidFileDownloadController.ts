import {
  Response,
} from 'express';
import path from 'path';
import AdmZip from 'adm-zip';

import { OptimizationFilesRepository } from '../../../../database/repositories/OptimizationFilesRepository';
import { FileUtils } from '../../../../services/files/FileUtils';
import { UserRequest } from '../../../../types';

type FileIdsQuery = {
  fileIds: string[],
}

// eslint-disable-next-line prefer-destructuring
const DATA_PATH = process.env.DATA_PATH;

export class BidFileDownloadController {
  /**
   * @api {get} projects/:projectId/bidding_rounds/:roundId/bid_download?fileIds=:fileId1&fileIds=:fileId2 Download bid files
   * @apiName Given file UUIDs, compose a zip file of them for download
   * @apiGroup Bidding Rounds
   *
   * @apiParam {string} projectId Project UUID string
   * @apiParam {string} roundId Bidding round UUID string
   * @apiParam {string} fileIds File UUID strings to include in the zip file
   *
   * @apiSuccess {ResponseBody}
   */
  public static async downloadFiles(req: UserRequest, res: Response) {
    const {
      branchId,
    } = req.user;
    const {
      roundId,
    } = req.params;
    const { fileIds } = req.query as FileIdsQuery;

    const fileIdsClean = Array.isArray(fileIds) ? fileIds : [fileIds];

    const files = await OptimizationFilesRepository.getAllOptimizationFilesByIdsAndBranchIdAndBiddingRoundId(
      fileIdsClean,
      branchId,
      roundId
    );

    const zip = new AdmZip();

    await Promise.all(files.map(async (file) => {
      const relativePath = await FileUtils.getRelativeFilePath(file.id, branchId);
      const absolutePath = path.join(DATA_PATH, relativePath);

      return zip.addLocalFile(absolutePath, '', `${file.name}${file.fileExtension}`);
    }));

    const downloadBuffer = zip.toBuffer();

    res.writeHead(200, [
      ['Content-Type', 'application/zip'],
      ['Content-Disposition', 'attachment; filename=bid_files_export.zip'],
    ]);
    res.end(downloadBuffer);
  }
}
