import {
  Request,
  Response,
} from 'express';
import {
  BidRoundStatuses,
} from '../../../../models';
import {
  BidRoundStatusEnum,
} from '../../../../enums';
import { BiddingRoundsRepository } from '../../../../database/repositories/BiddingRoundsRepository';

export class StatusUpdateController {
  static async updateBiddingRoundFinishedStatus(req: Request, res: Response) {
    const { roundId } = req.params;
    const {
      id: userId,
      branchId,
      // @ts-ignore
    } = req.decodedToken;

    const biddingRound = await BiddingRoundsRepository.getBiddingRoundByIdAndBranchId(roundId, branchId);

    const statusId = await BidRoundStatuses.getStatusId(BidRoundStatusEnum.FINISHED);

    biddingRound.statusId = statusId;
    biddingRound.updatedById = userId;
    await biddingRound.save();

    return res.json(null);
  }
}
