import {
  Request,
  Response,
} from 'express';

import { BiddingRounds } from '../../../../models/BiddingRounds';
import { BiddingRoundsFetchingService } from '../../../../services/projects/biddingRounds/BiddingRoundsFetchingService';
import {
  BiddingRoundsDeleteDemandsHistoricOffersService,
} from '../../../../services/projects/biddingRounds/BiddingRoundsDeleteDemandsHistoricOffersService';
import { NotFoundError } from '../../../../errors/NotFoundError';

export class BidSettingsCreateController {
  static async createSettings(req: Request, res: Response) {
    const {
      roundId,
      projectId,
    } = req.params;

    const {
      branchId,
      // @ts-ignore
    } = req.decodedToken;

    const biddingRound = await BiddingRoundsFetchingService.getBiddingRoundDetails(
      roundId,
      projectId,
      branchId
    );

    if (!biddingRound) {
      const message = 'The bid round does not exist';

      throw new NotFoundError(message);
    }

    const currentSettings = req.body;
    const entry = {
      settings: {
        ...biddingRound.settings,
        ...currentSettings,
      },
    };

    // improve this by return the updated one
    await BiddingRounds.update(
      entry,
      { where: { id: roundId } }
    );

    const updatedBiddingRound = await BiddingRoundsFetchingService.getBiddingRoundDetails(
      roundId,
      projectId,
      branchId
    );

    await BiddingRoundsDeleteDemandsHistoricOffersService.deleteDemandsAndOffers(roundId);

    return res.json(updatedBiddingRound.settings);
  }
}
