import {
  Request,
  Response,
} from 'express';

import {
  Users,
  sequelize,
} from '../../../../models';
import { OptimizationFilesRepository } from '../../../../database/repositories/OptimizationFilesRepository';
import { arrayToMap } from '../../../../utils/arrayUtils';
import { UserRequest } from '../../../../types';

type MatchingType = {
  bid_column: string,
  excel_column: string,
  matching: boolean,
  message: string,
}

type BodyFileType = {
  id: string,
  matchings: MatchingType[],
}

export class ColumnMatchingUpdateController {
  public static async updateColumnMatchings(req: UserRequest, res: Response) {
    const { user } = req;
    const { files } = req.body;

    const { branch } = user;
    const fileIds = files.map(({ id }) => id);
    const branchFiles = await OptimizationFilesRepository.getAllOptimizationFilesByIdsAndBranchId(fileIds, branch.id);

    const bodyFileMap = arrayToMap(files, 'id');

    await sequelize.transaction(() => Promise.all(branchFiles.map((file) => {
      // assign file to new variable to bypass eslint
      const newFile = file;
      const bodyFile: BodyFileType = bodyFileMap[file.id] as BodyFileType;

      newFile.matchings = bodyFile.matchings;

      return file.save();
    })));

    return res.json(null);
  }
}
