import {
  Request,
  Response,
} from 'express';

import { BiddingRoundsFetchingService } from '../../../../services/projects/biddingRounds/BiddingRoundsFetchingService';
import { BiddingRoundsCreateDemandsService } from '../../../../services/projects/biddingRounds/BiddingRoundsCreateDemandsService';
import { BiddingRoundsCreateHistoricOffersService } from '../../../../services/projects/biddingRounds/BiddingRoundsCreateHistoricOffersService';
import {
  BiddingRoundsDeleteDemandsHistoricOffersService,
} from '../../../../services/projects/biddingRounds/BiddingRoundsDeleteDemandsHistoricOffersService';
import { BidColumnsRepository } from '../../../../database/repositories/BidColumnsRepository';
import { BidColumns } from '../../../../models/BidColumns';
import { sequelize } from '../../../../models';

// TODO: Write tests
export class BiddingRoundSkuCreateUpdateController {
  public static async saveDemandsAndOffers(req: Request, res: Response) {
    const {
      branchId,
      userId,
      // @ts-ignore
    } = req.decodedToken;
    const {
      roundId,
      projectId,
    } = req.params;

    const { rows } = req.body;

    const { settings } = await BiddingRoundsFetchingService.getBiddingRoundDetails(
      roundId,
      projectId,
      branchId
    );

    const bidColumns = await BidColumnsRepository.getAllColumnsScopedByProjectId(projectId);
    const buyerColumns = bidColumns.filter(col => col.inputBy === 'buyer');
    const supplierColumns = bidColumns.filter(col => col.inputBy === 'supplier');

    const volumeBidColumn = bidColumns.find(column => column.columnType?.type === 'volume');
    const itemBidColumn = bidColumns.find(column => column.columnType?.type === 'item');
    const supplierBidColumn = bidColumns.find(column => column.columnType?.type === 'supplier');

    return sequelize.transaction(async () => {
      if (settings.historicTargetPrices === false) {
        const preparedObjects = rows.map((row) => {
          const buyerAttributes = BiddingRoundSkuCreateUpdateController.getAttributeMap(row.attributes, buyerColumns);

          return {
            id: row.id,
            item: row.attributes[itemBidColumn.id],
            volume: row.attributes[volumeBidColumn.id],
            buyerAttributes,
          };
        });

        const result = await BiddingRoundsCreateDemandsService.createOrUpdateDemands(preparedObjects, roundId, branchId, userId);

        return res.json(result);
      }

      const preparedObjects = rows.map((row) => {
        const buyerAttributes = BiddingRoundSkuCreateUpdateController.getAttributeMap(row.attributes, buyerColumns);
        const supplierAttributes = BiddingRoundSkuCreateUpdateController.getAttributeMap(row.attributes, supplierColumns);

        return {
          id: row.id,
          demandId: row.demandId,
          item: row.attributes[itemBidColumn.id],
          volume: row.attributes[volumeBidColumn.id],
          supplier: row.attributes[supplierBidColumn.id],
          buyerAttributes,
          supplierAttributes,
        };
      });

      const result = await BiddingRoundsCreateHistoricOffersService.createOrUpdateDemandsAndOffers(preparedObjects, roundId, branchId, userId);

      return res.json(result);
    });
  }

  private static getAttributeMap(attributes: object, columns: BidColumns[]) {
    return columns.reduce((acc, col) => {
      acc[col.id] = attributes[col.id];

      return acc;
    }, {});
  }
}
