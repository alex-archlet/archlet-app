import {
  Request,
  Response,
} from 'express';

import { BiddingRoundsFetchingService } from '../../../../services/projects/biddingRounds/BiddingRoundsFetchingService';
import { Demands } from '../../../../models';
import { Offers } from '../../../../models/Offers';

export class BiddingRoundSkuGetController {
  public static async getDemandsAndOffers(req: Request, res: Response) {
    const {
      branchId,
      // @ts-ignore
    } = req.decodedToken;
    const {
      roundId,
      projectId,
    } = req.params;

    const { settings } = await BiddingRoundsFetchingService.getBiddingRoundDetails(
      roundId,
      projectId,
      branchId
    );

    if (settings && settings.historicTargetPrices === false) {
      const demands = await Demands.findAll({
        where: {
          roundId,
          branchId,
        },
      });

      return res.json(demands);
    }

    if (settings && settings.historicTargetPrices === true) {
      const offers = await Offers.findAll({
        where: {
          roundId,
          is_historic: true,
        },
        include: [
          {
            as: 'demand',
            model: Demands,
          },
        ],
      });

      return res.json(offers);
    }

    return res.json([]);
  }
}
