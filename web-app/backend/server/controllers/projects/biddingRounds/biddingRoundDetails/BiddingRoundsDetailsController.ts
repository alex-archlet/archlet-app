import {
  Request,
  Response,
} from 'express';

import { BiddingRoundsDetailsModel } from '../../../../services/projects/biddingRounds/BiddingRoundsDetailsModel';
import { BiddingRoundsFetchingService } from '../../../../services/projects/biddingRounds/BiddingRoundsFetchingService';

export class BiddingRoundsDetailsController {
  public static async getBidRoundDetails(req: Request, res: Response) {
    const {
      projectId,
      roundId,
    } = req.params;
    // @ts-ignore
    const { branchId } = req.decodedToken;
    const biddingRound: BiddingRoundsDetailsModel = await BiddingRoundsFetchingService.getBiddingRoundDetails(
      roundId,
      projectId,
      branchId
    );

    res.json(biddingRound);
  }
}
