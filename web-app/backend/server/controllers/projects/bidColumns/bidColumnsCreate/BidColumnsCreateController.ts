import {
  Request,
  Response,
} from 'express';

import { BidColumnListModel } from '../BidColumnListModel';
import { BidColumns } from '../../../../models/BidColumns';

interface Column {
  /**
   * UUID of the column if updating an existing column
   */
  id: string | undefined,
  /**
   * Name of the column
   */
  name: string,
  typeId: number,
  inputBy: string,
  isMandatory: boolean,
  isVisible: boolean,
  matchingConfidence: number,
  limitMin: number,
  limitMax: number,
  /**
   * Array of string options for the single choice column
   */
  singleChoice: string[],
}

interface CreateColumnBody {
  /**
   * Array of columns to be created/updated
   */
  columns: Column[],
}

export class BidColumnsCreateController {
  /**
   * @api {put} projects/:projectId/table_design Create/Update Bid Columns
   * @apiName Create project bid columns
   * @apiGroup Bid Columns
   *
   * @apiParam {string} projectId The project UUID
   * @apiInterfaceParam {CreateColumnBody} columns Columns to create/update
   *
   * @apiInterfaceSuccess (../BidColumnListModel.ts) {BidColumnListModelInterface[]} _ an array of objects containing the following keys:
   */
  public static async createOrUpdateTableDesign(req: Request, res: Response) {
    const { projectId } = req.params;
    const { columns } = req.body as CreateColumnBody;

    const columnsRows = columns.map((column, order) => {
      const {
        id,
        name,
        typeId,
        inputBy,
        isMandatory,
        isVisible = true,
        matchingConfidence,
        limitMin,
        limitMax,
        singleChoice,
      } = column;

      const parsedCol = {
        id: undefined,
        name,
        typeId,
        initialTypeId: typeId,
        inputBy,
        isMandatory,
        isVisible,
        matchingConfidence,
        limitMin,
        limitMax,
        singleChoice,
        order,
        projectId,
      };

      if (id) {
        parsedCol.id = id;
      }

      return parsedCol;
    });

    await BidColumns.bulkCreate(columnsRows, {
      updateOnDuplicate: [
        'name',
        'typeId',
        'inputBy',
        'isMandatory',
        'isVisible',
        'matchingConfidence',
        'limitMin',
        'limitMax',
        'singleChoice',
        'order',
        'updatedAt',
      ],
    });

    const bidColumns = await BidColumns.scope('column').findAll({
      where: {
        projectId,
      },
      order: [
        ['order', 'ASC'],
      ],
    });

    const data = bidColumns.map(col => new BidColumnListModel(col));

    return res.json(data);
  }
}
