import {
  Request,
  Response,
} from 'express';

import { BadRequestError } from '../../../../errors/BadRequestError';

import { BidColumns } from '../../../../models/BidColumns';

function isDefaultColumn(column: BidColumns) {
  return column.columnType?.isDefault ?? false;
}

export class BidColumnsDeleteController {
  public static async deleteTableDesignColumn(req: Request, res: Response) {
    const {
      columnId,
      projectId,
    } = req.params;

    // TODO: Move to a repository when Bid Import is merged
    const column = await BidColumns.scope('column').findOne({
      where: {
        id: columnId,
        projectId,
      },
    });

    // TODO: what is this for?
    if (column.columnType.type !== 'price') {
      if (isDefaultColumn(column)) {
        const message = 'Cannot delete this column - default or does not exist';

        throw new BadRequestError(message, message);
      }
    }
    await column.destroy();

    return res.json(null);
  }
}
