import {
  Request,
  Response,
} from 'express';

import { BidColumns } from '../../../../models/BidColumns';
import { BidColumnListModel } from '../BidColumnListModel';

export class BidColumnsListController {
  /**
   * @api {get} projects/:projectId/table_design Fetch Bid Columns
   * @apiName Fetch project bid columns
   * @apiGroup Bid Columns
   *
   * @apiParam {string} projectId The project UUID
   *
   * @apiInterfaceSuccess (../BidColumnListModel.ts) {BidColumnListModelInterface[]} _ an array of objects containing the following keys:
   */
  public static async getTableDesignList(req: Request, res: Response) {
    const { projectId } = req.params;
    // TODO: Move to a repository when Bid Import is merged
    const bidColumns = await BidColumns.scope('column').findAll({
      where: {
        projectId,
      },
      order: [
        ['order', 'ASC'],
      ],
    });

    const data = bidColumns.map(col => new BidColumnListModel(col));

    return res.json(data);
  }
}
