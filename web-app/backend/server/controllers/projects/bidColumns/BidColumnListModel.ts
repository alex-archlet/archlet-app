import { BidColumns } from '../../../models/BidColumns';

interface BidColumnListModelInterface {
  id: string;
  name: string;
  description: string | null;
  calculation: string | null;
  isAutomatic: boolean;
  isMandatory: boolean;
  isVisible: boolean;
  order: number;
  limitMin: number;
  limitMax: number;
  singleChoice: string[];
  inputBy: string;
  projectId: string;
  typeId: number;
  initialTypeId: number | null;
  matchingConfidence: number | null;
  initialMatchingConfidence: number | null;
  column: string | null;
  isNumeric: boolean;
  createdAt: Date;
  updatedAt: Date;
}

export class BidColumnListModel implements BidColumnListModelInterface {
  public id: string;

  public name: string;

  public description: string | null;

  public calculation: string | null;

  public isAutomatic: boolean;

  public isMandatory: boolean;

  public isVisible: boolean;

  public order: number;

  public limitMin: number;

  public limitMax: number;

  public singleChoice: string[];

  public inputBy: string;

  public projectId: string;

  public typeId: number;

  public initialTypeId: number | null;

  public matchingConfidence: number | null;

  public initialMatchingConfidence: number | null;

  public column: string | null;

  public isNumeric: boolean;

  public createdAt: Date;

  public updatedAt: Date;

  constructor(column: BidColumns) {
    this.id = column.id;
    this.name = column.name;
    this.description = column.description;
    this.calculation = column.calculation;
    this.isAutomatic = column.isAutomatic;
    this.isMandatory = column.isMandatory;
    this.isVisible = column.isVisible;
    this.order = column.order;
    this.limitMin = column.limitMin;
    this.limitMax = column.limitMax;
    this.singleChoice = column.singleChoice;
    this.inputBy = column.inputBy;
    this.projectId = column.projectId;
    this.typeId = column.typeId;
    this.initialTypeId = column.initialTypeId;
    this.matchingConfidence = column.matchingConfidence;
    this.initialMatchingConfidence = column.initialMatchingConfidence;
    this.column = column.columnType?.type ?? null;
    this.isNumeric = column.columnType?.isNumeric ?? null;
    this.createdAt = column.createdAt;
    this.updatedAt = column.updatedAt;
  }
}
