import {
  Request,
  Response,
} from 'express';

import { ProjectsRepository } from '../../../database/repositories/ProjectsRepository';
import { NotFoundError } from '../../../errors/NotFoundError';
import { ProjectDataService } from '../../../services/projects/dataService/ProjectDataService';

export class ProjectPivotTableController {
  public static async getProjectPivotTable(req: Request, res: Response) {
    // @ts-ignore
    const { branchId } = req.decodedToken;
    const { projectId } = req.params;
    const {
      groupBy,
      results,
    } = req.body;
    const project = await ProjectsRepository.getByIdAndBranchId(projectId, branchId);

    if (!project) {
      const message = 'The project does not exist';

      throw new NotFoundError(message);
    }

    const data = await ProjectDataService.getProjectPivotTable(
      project,
      groupBy,
      results
    );

    return res.json(data);
  }
}
