import {
  Request,
  Response,
} from 'express';

import { ProjectsRepository } from '../../../database/repositories/ProjectsRepository';
import { ProjectSettingUpdateService } from '../../../services/projects/projectSettings/ProjectSettingUpdateService';
import { NotFoundError } from '../../../errors/NotFoundError';

export class ProjectSettingsController {
  public static async updateProjectSettings(req: Request, res: Response) {
    // @ts-ignore
    const { branchId } = req.decodedToken;
    const { projectId } = req.params;
    const { settings } = req.body;
    const project = await ProjectsRepository.getByIdAndBranchId(projectId, branchId);

    if (!project) {
      const message = 'The project does not exist';

      throw new NotFoundError(message);
    }

    delete settings.name;
    delete settings.unitId;
    delete settings.currencyId;

    const newProjectSettings = await ProjectSettingUpdateService.updateSettings(project, settings);

    return res.json(newProjectSettings);
  }
}
