import {
  Projects,
  Users,
} from '../models';
import {
  UserAccessEnum,
} from '../enums';
import { NotFoundError } from '../errors/NotFoundError';
import { ForbiddenError } from '../errors/ForbiddenError';

class MembersController {
  static async inviteMembers(req, res) {
    const { branchId } = req.decodedToken;
    const response = {};
    const {
      projectId,
      emails,
    } = req.body;

    emails.forEach((email) => {
      response[email] = { message: `User "${email}" does not exist` };
    });

    const project = await Projects.findOne({
      where: {
        id: projectId,
        branchId,
      },
    });
    const users = await Users.findAll({
      attributes: [
        'accountType',
        'firstName',
        'id',
        'lastName',
        'username',
      ],
      raw: true,
      where: { username: emails },
    });

    if (!users.length) {
      const message = 'The user does not exist';

      throw new NotFoundError(message);
    }

    users.forEach((user) => {
      project.addUser(user.id, {
        through: {
          accessType: UserAccessEnum.READ_AND_WRITE,
        },
      });

      response[user.username] = {
        ...user,
        ProjectAccesses: {
          accessType: UserAccessEnum.READ_AND_WRITE,
        },
      };
    });

    return res.json(Object.values(response));
  }

  static async deleteMember(req, res) {
    const {
      projectId,
      ids,
    } = req.body;

    const project = await Projects.getProjectMembers(projectId, ids, false);
    const members = (project && project.get('users')) || [];

    if (!project || members.length !== ids.length) {
      throw new ForbiddenError('Project not accessible or not all members have access');
    }

    members.forEach((member) => {
      project.removeUser(member.get('id'));
    });

    return res.json(null);
  }
}

export default MembersController;
