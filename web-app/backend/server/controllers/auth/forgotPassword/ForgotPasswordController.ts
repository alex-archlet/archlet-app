import {
  Request,
  Response,
} from 'express';
import Bcrypt from 'bcrypt';

import {
  sequelize,
} from '../../../models';
import { ResetPasswordEmailService } from '../../../services/email/ResetPasswordEmailService';
import { UsersRepository } from '../../../database/repositories/UsersRepository';
import { InternalServerError } from '../../../errors/InternalServerError';

const SECURITY_RESPONSE_DELAY = 2000;

export class ForgotPasswordController {
  public static async forgotPassword(req: Request, res: Response) {
    const { username } = req.body;

    const password = Math.random()
      .toString(36)
      .slice(-8);

    const salt = await Bcrypt.genSalt(11);
    const hash = await Bcrypt.hash(password, salt);

    const user = await UsersRepository.getByUserName(username);

    if (!user) {
      // if there is no user like this, delay the reply to not indicate whether user exists
      setTimeout(() => res.sendStatus(200), SECURITY_RESPONSE_DELAY);

      return;
    }

    try {
      await sequelize.transaction(async () => {
        user.passwordHash = hash;
        user.passwordSalt = salt;

        await user.save();

        return ResetPasswordEmailService.sendEmail(user, password);
      });
      res.sendStatus(200);
    } catch (error) {
      console.log(error);

      throw new InternalServerError('Email sending failed');
    }
  }
}
