import {
  NextFunction,
  Response,
} from 'express';
import passport from 'passport';

import { UnauthorizedError } from '../../../errors/UnauthorizedError';
import { Users } from '../../../models/Users';
import { PassportRequest } from '../../../types';

export class Auth0CallbackController {
  static async auth0Callback(req: PassportRequest, res: Response, next: NextFunction) {
    passport.authenticate('auth0', async (error: Error | UnauthorizedError | null, user: Users) => {
      try {
        if (error) {
          return next(error);
        }

        return req.logIn(user, async (err) => {
          if (err) {
            return next(err);
          }

          return res.redirect(req.session.returnTo || '/');
        });
      } catch (err) {
        return next(err);
      }
    })(
      req,
      res,
      next
    );
  }
}
