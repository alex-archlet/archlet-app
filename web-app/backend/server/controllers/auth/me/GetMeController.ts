import {
  Response,
} from 'express';

import { AuthUserService } from '../../../services/auth/userResponse/AuthUserService';
import { UserRequest } from '../../../types';

export class GetMeController {
  public static async getLoggedUser(req: UserRequest, res: Response) {
    const { user } = req;
    const response = await AuthUserService.getUserResponse(user);

    return res.json(response);
  }
}
