import {
  Response,
} from 'express';
import util from 'util';
import url from 'url';
import querystring from 'querystring';
import { PassportRequest } from '../../../types';

export class SignOutController {
  public static async signOut(req: PassportRequest, res: Response) {
    const { body } = req;
    const returnTo: string = body.returnTo as string || '/';

    if (req?.session?.destroy) {
      req.session.destroy();
    }
    req.logout();

    const logoutURL = new url.URL(
      util.format('https://%s/v2/logout', process.env.AUTH0_DOMAIN)
    );
    const searchString = querystring.stringify({
      client_id: process.env.AUTH0_CLIENT_ID,
      returnTo,
    });

    logoutURL.search = searchString;

    const text = logoutURL.toString();

    res.send(text);
  }
}
