import passport from 'passport';

import { AuthUserService } from '../../../services/auth/userResponse/AuthUserService';

export class SignInController {
  public static async signIn(req, res, next) {
    passport.authenticate(
      'local',
      async (error, user) => {
        if (error) {
          return next(error);
        }

        return req.logIn(user, async (err) => {
          if (err) {
            return next(err);
          }

          const response = await AuthUserService.getUserResponse(user);

          return res.json(response);
        });
      }
    )(req, res);
  }
}
