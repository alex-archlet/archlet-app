import {
  Response,
} from 'express';

import {
  JobTypes,
  JobStatuses,
  Jobs,
} from '../../../models';
import {
  JobTypeEnum,
  JobStatusEnum,
} from '../../../enums';
import { JobQueueService } from '../../../services/jobs/JobQueueService';
import { UserRequest } from '../../../types';

export class ExpressionExampleJobController {
  public static async runExpressionExampleJob(req: UserRequest, res: Response) {
    const {
      roundId,
      projectId,
      expression,
    } = req.body;
    const {
      id: userId,
      branchId,
    } = req.user;

    const [
      jobType,
      jobStatus,
    ] = await Promise.all([
      JobTypes.getJobTypeId(JobTypeEnum.EXPRESSION_EXAMPLE),
      JobStatuses.getJobStatusId(JobStatusEnum.PENDING),
    ]);

    const newJob = {
      input: {
        round_id: roundId,
        params: {},
        expression,
      },
      output: {},
      projectId,
      statusId: jobStatus,
      typeId: jobType,
      branchId,
      userId,
    };

    const createdJob = await Jobs.create(newJob);

    await JobQueueService.addJobToQueue(createdJob.id);

    return res.json(createdJob);
  }
}
