import {
  Request,
  Response,
} from 'express';

import {
  JobTypes,
  JobStatuses,
  Jobs,
  OptimizationFiles,
  sequelize,
} from '../../../models';
import {
  JobTypeEnum,
  JobStatusEnum,
} from '../../../enums';
import { OptimizationFilesRepository } from '../../../database/repositories/OptimizationFilesRepository';
import { JobQueueService } from '../../../services/jobs/JobQueueService';
import { InternalServerError } from '../../../errors/InternalServerError';
import { UserRequest } from '../../../types';

export class ColumnMatchingJobController {
  public static async runColumnMatchingJob(req: UserRequest, res: Response) {
    const {
      id: userId,
      branchId,
      // @ts-ignore
    } = req.user;
    const {
      fileIds,
      rowNumber,
      sheetName,
    } = req.body;

    const [
      jobType,
      jobStatus,
    ] = await Promise.all([
      JobTypes.getJobTypeId(JobTypeEnum.COLUMN_MATCHING),
      JobStatuses.getJobStatusId(JobStatusEnum.PENDING),
    ]);

    const files = await OptimizationFilesRepository.getAllOptimizationFilesByIdsAndBranchId(fileIds, branchId);

    if (!files.length) {
      throw new InternalServerError('Not all files are accessible for this branch');
    }

    const jobs = await sequelize.transaction(async () => {
      const branchFileIds = files.map(({ id }) => id);

      await OptimizationFiles.update(
        {
          rowNumber,
          sheetName,
        },
        {
          where: {
            id: branchFileIds,
          },
        }
      );

      const jobsToCreate = files.map((file) => {
        const input = {
          file_id: file.id,
          round_id: file.roundId,
          params: {},
        };

        return {
          input,
          output: {},
          projectId: file.projectId,
          statusId: jobStatus,
          typeId: jobType,
          branchId,
          userId,
        };
      });

      return Jobs.bulkCreate(jobsToCreate);
    });

    const jobIds = jobs.map(({ id }) => id);

    await JobQueueService.addJobsToQueue(jobIds);

    return res.json(jobs);
  }
}
