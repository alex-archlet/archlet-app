import {
  Response,
} from 'express';

import {
  JobTypes,
  JobStatuses,
  Jobs,
} from '../../../models';
import {
  JobTypeEnum,
  JobStatusEnum,
} from '../../../enums';

import { JobQueueService } from '../../../services/jobs/JobQueueService';
import { UserRequest } from '../../../types';

export class OfferProcessingJobController {
  public static async runOfferProcessingJob(req: UserRequest, res: Response) {
    const {
      roundId,
      projectId,
    } = req.body;
    const {
      id: userId,
      branchId,
    } = req.user;

    const [
      jobType,
      jobStatus,
    ] = await Promise.all([
      JobTypes.getJobTypeId(JobTypeEnum.OFFER_PROCESSING),
      JobStatuses.getJobStatusId(JobStatusEnum.PENDING),
    ]);

    const newJob = {
      input: {
        round_id: roundId,
        params: {},
      },
      output: {},
      projectId,
      statusId: jobStatus,
      typeId: jobType,
      branchId,
      userId,
    };

    const createdJob = await Jobs.create(newJob);

    await JobQueueService.addJobToQueue(createdJob.id);

    return res.json(createdJob);
  }
}
