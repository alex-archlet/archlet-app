import path from 'path';
import Sequelize from 'sequelize';

import { XlsxReadService } from '../services/xlsx/XlsxReadService';
import {
  Jobs,
  OptimizationFiles,
  Projects,
  ProjectAccesses,
  ProjectCategories,
  ProjectUnits,
  ProjectCurrencies,
  BiddingTypes,
  ProjectTypes,
  Scenarios,
  Users,
  Translators,
} from '../models';
import {
  JobTypeEnum,
  UserAccessEnum,
} from '../enums';
import { NotFoundError } from '../errors/NotFoundError';
import { ScenarioCreateService } from '../services/projects/scenarios/ScenarioCreateService';
import defaultScenarios from '../static/default_scenarios.json';
import { getSplitedFileName } from '../utils/common';
import { ProjectsRepository } from '../database/repositories/ProjectsRepository';

class ProjectsController {
  static async getFilePreview(req, res) {
    const { branchId } = req.decodedToken;
    const {
      projectId,
      type,
    } = req.params;

    const file = await OptimizationFiles.getLastFile(type, projectId, branchId);

    if (!file) {
      const message = 'File does not exist';

      throw new NotFoundError(message);
    }

    const {
      fileExtension,
      filePath,
      fileType,
      fileVersion,
      name,
    } = file.dataValues;
    const splitedFileName = getSplitedFileName(name, fileType, fileVersion, fileExtension);
    const relativePath = path.join(filePath, splitedFileName.join(''));

    const workbook = await XlsxReadService.parseHeaderNameToValueXlsxFile(relativePath);

    const firstSheetName = Object.keys(workbook)[0];

    const { headers } = workbook[firstSheetName];
    const rows = workbook[firstSheetName].data;

    return res.json({
      headers,
      rows,
    });
  }

  static async getProjectCategories(req, res) {
    const { branchId } = req.decodedToken;
    const translators = await Translators.findAll({
      where: { branchId },
    });
    let projectCategories = await ProjectCategories.findAll({ raw: true });

    projectCategories = projectCategories.map((category) => {
      const isDisabled = !translators.find(translator => translator.categoryId === category.id);

      return {
        ...category,
        isDisabled,
      };
    });

    return res.json(projectCategories);
  }

  static async getProjectCurrencies(req, res) {
    const projectCurrencies = await ProjectCurrencies.findAll();

    return res.json(projectCurrencies);
  }

  static async getProjectBiddingTypes(req, res) {
    const biddingTypes = await BiddingTypes.findAll();

    return res.json(biddingTypes);
  }

  static async getProjectUnits(req, res) {
    const projectUnits = await ProjectUnits.findAll();

    return res.json(projectUnits);
  }

  static async getProjectDetails(req, res) {
    const { branchId } = req.decodedToken;
    const { projectId } = req.params;
    const projectDetails = await Projects.findOne({
      where: {
        id: projectId,
        branchId,
      },
      attributes: [
        'id',
        'name',
        'status',
        'settings',
        'categoryId',
        'unitId',
        'currencyId',
        'biddingTypeId',
        'created_at',
        'updated_at',
        [Sequelize.col('category.name'), 'categoryName'],
        [Sequelize.col('category->type.name'), 'typeName'],
      ],
      include: [
        {
          attributes: [],
          as: 'category',
          model: ProjectCategories,
          required: true,
          include: [
            {
              attributes: [],
              as: 'type',
              model: ProjectTypes,
              required: true,
            },
          ],
        },
      ],
    });

    if (!projectDetails) {
      const message = 'The project does not exist';

      throw new NotFoundError(message);
    }

    return res.json(projectDetails);
  }

  static async getProjectFiles(req, res) {
    const {
      branchId,
      id: userId,
    } = req.decodedToken;
    const { projectId } = req.params;
    let projectFiles = {};

    const branchSettings = await Users.getBranchSettings(userId);
    const fileTypes = branchSettings.file_types;

    await Promise.all(Object.keys(fileTypes).map(async (type) => {
      const fileResponse = await OptimizationFiles.getLastFile(type, projectId, branchId);

      if (fileResponse) {
        projectFiles = {
          ...projectFiles,
          [type]: fileResponse.dataValues,
        };
      }
    }));

    return res.json(projectFiles);
  }

  static async getProjectFilesValidation(req, res) {
    const { branchId } = req.decodedToken;
    const { projectId } = req.params;
    const response = await Jobs.getLastJobResult(JobTypeEnum.FILE_PROCESSING, projectId, branchId);
    let projectFilesValidation = response;

    if (!response || !Object.keys(response).length) {
      return res.json({});
    }

    const filesId = Object.values(response.input.files);
    const deletedFiles = await OptimizationFiles.countDeletedFiles(filesId);

    if (deletedFiles) {
      projectFilesValidation = {};
    }

    return res.json(projectFilesValidation);
  }

  static async getProjectScenariosList(req, res) {
    const { branchId } = req.decodedToken;
    const { projectId } = req.params;
    const projectScenarios = await Scenarios.getScenariosList(projectId, branchId, true);

    return res.json(projectScenarios);
  }

  static async getProjectTypes(req, res) {
    const projectTypes = await ProjectTypes.findAll();

    return res.json(projectTypes);
  }

  static async getProjectsList(req, res) {
    const { branchId } = req.decodedToken;
    const projects = await Projects.findAll({
      where: {
        branchId,
      },
    });

    return res.json(projects);
  }

  static async addProjectAccess(req, res) {
    const { branchId } = req.decodedToken;
    const { projectId } = req.params;
    const {
      categoryId,
      userIds,
    } = req.body;
    let project = await Projects.findOne({
      where: {
        id: projectId,
        branchId,
      },
    });

    if (!project) {
      const message = 'The project does not exist';

      throw new NotFoundError(message);
    }

    project.setCategory(categoryId);
    userIds.map(
      userId => project.addUser(
        userId,
        { through: { accessType: UserAccessEnum.READ_AND_WRITE } }
      )
    );
    project = await project.save();

    return res.json(project);
  }

  static async getProjectMembers(req, res) {
    const userId = req.decodedToken.id;
    const { projectId } = req.params;

    const project = await Projects.getProjectMembers(projectId, {
      [Sequelize.Op.not]: userId,
    });
    const members = (project && project.get('users')) || [];

    return res.json(members);
  }

  static async createProject(req, res) {
    const {
      branchId,
      id: userId,
    } = req.decodedToken;
    const {
      name,
      status,
      categoryId,
      unitId,
      currencyId,
      biddingTypeId,
    } = req.body;

    let project = await Projects.create({
      name,
      status,
      unitId,
      currencyId,
      branchId,
      biddingTypeId,
    });

    project.setCategory(categoryId);
    project.addUser(userId, {
      through: {
        accessType: UserAccessEnum.READ_AND_WRITE,
      },
    });
    project = await project.save();

    const projectId = project.dataValues.id;

    await Promise.all(defaultScenarios.scenarios.map(async (scenario) => {
      const data = {
        ...scenario,
        isDefault: false,
        projectId,
        branchId,
      };

      await ScenarioCreateService.createScenario(data);
    }));

    const newProject = await Projects.findOne({
      include: [
        {
          as: 'users',
          attributes: [
            'firstName',
            'lastName',
          ],
          model: Users,
          required: true,
          through: {
            model: ProjectAccesses,
            where: { userAccessId: userId },
          },
        },
      ],
      where: {
        id: projectId,
        branchId,
      },
    });

    return res.json(newProject);
  }

  static async deleteProject(req, res) {
    const {
      branchId,
    } = req.decodedToken;
    const { projectId } = req.params;
    const project = await Projects.destroy({
      where: {
        id: projectId,
        branchId,
      },
    });

    if (!project) {
      const message = 'The project does not exist';

      throw new NotFoundError(message);
    }

    return res.json(null);
  }

  static async updateProject(req, res) {
    const {
      branchId,
    } = req.decodedToken;
    const { projectId } = req.params;
    const {
      name,
      status,
      unitId,
      currencyId,
    } = req.body;
    const project = await Projects.findOne({
      id: projectId,
      branchId,
    });

    if (!project) {
      const message = 'The project does not exist';

      throw new NotFoundError(message);
    }

    await Projects.update(
      {
        name,
        status,
        unitId,
        currencyId,
      },
      { where: { id: projectId } }
    );

    // additional request to database
    const updatedProject = await ProjectsRepository.getByIdAndBranchId(projectId, branchId);

    return res.json(updatedProject);
  }
}

export default ProjectsController;
