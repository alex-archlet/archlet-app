import { Roles } from '../models';

class RolesController {
  static async getRolesList(req, res) {
    const roles = await Roles.findAll();

    return res.json(roles);
  }
}

export default RolesController;
