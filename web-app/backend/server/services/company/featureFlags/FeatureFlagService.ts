import { FeatureFlagsRepository } from '../../../database/repositories/FeatureFlagsRepository';

export class FeatureFlagService {
  public static async getFeatureFlagIdsByBranchId(branchId: string): Promise<number[]> {
    const companyFeatureFlags = await FeatureFlagsRepository.getAllByBranchId(branchId);

    return companyFeatureFlags.map(flag => flag.flagTypeId);
  }
}
