import Axios from 'axios';

const PARSER_SERVICE_URI = 'http://parser:5001/v1';
const API_INSTANCE = Axios.create({ baseURL: PARSER_SERVICE_URI });

export class XlsxReadService {
  /**
   * @param path - has to start with the '/'
   * @return sheetname : { header: [array], data: [[]]}
   */
  public static async parseXlsxFile(path, options) {
    const { data } = await API_INSTANCE.post('/excel-parser/', {
      path,
      ...options,
    });

    return data;
  }

  /**
 * @param path - has to start with the '/'
 * @return sheetname : { header: [array], data: [{header: value}]}
 */
  public static async parseHeaderNameToValueXlsxFile(path) {
    const { data } = await API_INSTANCE.post('/excel-parser/', {
      path,
      returnType: 'records',
      row: 1,
    });

    return data;
  }
}
