import { getQueueConnection } from '../queue';

const QUEUE_NAME = 'task_queue';

export class JobQueueService {
  public static async addJobToQueue(job_id: string) {
    const msg = JSON.stringify({ job_id });
    const connection = await getQueueConnection();
    const ch = await connection.createChannel();

    await ch.assertQueue(QUEUE_NAME);
    await ch.sendToQueue(QUEUE_NAME, Buffer.from(msg));

    return ch.close();
  }

  public static async addJobsToQueue(jobIds: string[]) {
    const connection = await getQueueConnection();
    const ch = await connection.createChannel();

    await ch.assertQueue(QUEUE_NAME);
    await Promise.all(jobIds.map((job_id: string) => {
      const msg = JSON.stringify({ job_id });

      return ch.sendToQueue(QUEUE_NAME, Buffer.from(msg));
    }));

    return ch.close();
  }
}
