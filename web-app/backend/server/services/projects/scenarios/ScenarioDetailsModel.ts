import { ScenarioBidColumnsModel } from './ScenarioBidColumnsModel';
import { Jobs } from '../../../models/Jobs';
import { Scenarios } from '../../../models/Scenarios';

interface JobInterface {
  id: string;
  input: object;
  output: object;
}

export interface ScenarioDetailsInterface {
  id: string;

  invalidate: boolean;

  isDefault: boolean;

  json?: object | null;

  lastOptimization?: object;

  name: string;

  status: string;

  projectId?: string;

  roundId?: string;

  branchId?: string;

  output?: object;

  optimizations?: JobInterface[];

  validations?: JobInterface[];

  bidColumns: ScenarioBidColumnsModel[];
}
export class ScenarioDetailsModel implements ScenarioDetailsInterface {
  public id!: string;

  public invalidate!: boolean;

  public isDefault!: boolean;

  public json?: object | null;

  public lastOptimization?: object;

  public name!: string;

  public status!: string;

  public projectId?: string;

  public roundId?: string;

  public branchId?: string;

  public output?: object;

  public optimizations?: Jobs[];

  public validations?: Jobs[];

  public bidColumns: ScenarioBidColumnsModel[];

  constructor(scenario: Scenarios, bidColumns: ScenarioBidColumnsModel[]) {
    this.id = scenario.id;
    this.invalidate = scenario.invalidate;
    this.isDefault = scenario.isDefault;
    this.json = scenario.json;
    this.lastOptimization = scenario.lastOptimization;
    this.name = scenario.name;
    this.status = scenario.status;
    this.projectId = scenario.projectId;
    this.roundId = scenario.roundId;
    this.branchId = scenario.branchId;
    this.output = scenario.output;
    this.optimizations = scenario.optimizations;
    this.validations = scenario.validations;

    this.bidColumns = bidColumns;
  }
}
