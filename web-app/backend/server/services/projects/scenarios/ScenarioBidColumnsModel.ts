import { BidColumns } from '../../../models/BidColumns';

export class ScenarioBidColumnsModel {
  public id: string;

  public label: string;

  public value: string;

  public type: string;

  constructor(bidColumn: BidColumns) {
    this.id = bidColumn.id;
    this.value = bidColumn.id;
    this.label = bidColumn.name;
    this.type = bidColumn.inputBy;
  }
}