import { BidColumns } from '../../../models';
import { ScenariosRepository } from '../../../database/repositories/ScenariosRepository';
import { ScenarioBidColumnsModel } from './ScenarioBidColumnsModel';

export class ScenarioFetchingService {
  public static async getScenarioBidColumns(
    scenarioId: string,
    branchId: string
  ): Promise<ScenarioBidColumnsModel[]> {
    const scenario = await ScenariosRepository.getScenarioWithAllocationsByIdAndBranchId(
      scenarioId,
      branchId
    );

    const attributeIds = scenario.allocations
      .map(({
        offer, demand,
      }) => [
        ...Object.keys(offer?.attributes ?? {}),
        ...Object.keys(demand?.attributes ?? {}),
      ])
      .flat();
    const uniqueAttributeIds = [...new Set(attributeIds)];

    const bidColumns = await BidColumns.findAll({
      attributes: ['id', 'name', ['input_by', 'inputBy']],
      where: {
        id: uniqueAttributeIds,
      },
      order: [['name', 'ASC']],
    });

    return bidColumns
      .map(bidColumn => new ScenarioBidColumnsModel(bidColumn))
      .map((bidColumn, _, arr) => {
        const isDuplicated = arr.some(
          ({
            label, type,
          }) => bidColumn.label === label && bidColumn.type !== type
        );

        if (!isDuplicated) return bidColumn;
        const suffix = bidColumn.type === 'supplier' ? 'Offers' : 'Demands';

        return {
          ...bidColumn,
          label: `${bidColumn.label} (${suffix})`,
        };
      });
  }
}
