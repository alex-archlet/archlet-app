import { Scenarios } from '../../../models/Scenarios';

interface CreateParams {
  invalidate?: boolean,
  isDefault?: boolean,
  status?: string,
  json?: object,
  name: string,
  projectId: string,
  branchId: string,
}

export class ScenarioCreateService {
  public static async createScenario(data: CreateParams) {
    const {
      invalidate = false,
      isDefault = false,
      status = 'in_progress',
      json = {},
      name,
      projectId,
      branchId,
    } = data;

    return Scenarios.create({
      invalidate,
      isDefault,
      json,
      name,
      projectId,
      status,
      branchId,
    });
  }
}
