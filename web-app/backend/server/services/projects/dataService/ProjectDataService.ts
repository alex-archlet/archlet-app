import axios from 'axios';

import { Projects } from '../../../models/Projects';

const PROJECT_DATA_SERVICE_URI = 'http://aggrevator:5000/v1';
const API_INSTANCE = axios.create({ baseURL: PROJECT_DATA_SERVICE_URI });

type GroupByColumns = string[];
type ResultColumns = string[];

export class ProjectDataService {
  public static async getProjectInfo(project: Projects): Promise<NodeJS.ReadableStream> {
    const { data } = await API_INSTANCE.get(
      `/project_info/${project.id}`,
      {
        responseType: 'stream',
      }
    );

    return data;
  }

  public static async getProjectPivotTable(
    project: Projects,
    groupBy: GroupByColumns,
    resultColumns: ResultColumns
  ) {
    const { data } = await API_INSTANCE.post(
      `/pivot_table/${project.id}`,
      {
        group_by: groupBy,
        result_columns: resultColumns,
      }
    );

    return data;
  }
}
