import { BiddingRoundsRepository } from '../../../database/repositories/BiddingRoundsRepository';
import { OptimizationFilesRepository } from '../../../database/repositories/OptimizationFilesRepository';
import { BiddingRoundsDetailsModel } from './BiddingRoundsDetailsModel';
import { BiddingRoundsListModel } from './BiddingRoundsListModel';
import {
  runSelectQuery,
} from '../../../database/queries/utils';
import { arrayToMap } from '../../../utils/arrayUtils';
import { BIDDING_ROUNDS_LIST_KPI_QUERY } from '../../../database/queries/bidding_rounds_list_kpis';

const DEFAULT_LIST_VALUES = {
  offers_count: null,
  suppliers_count: null,
};

export class BiddingRoundsFetchingService {
  public static async getBiddingRoundDetails(
    id: string,
    projectId: string,
    branchId: string
  ) {
    const biddingRound = await BiddingRoundsRepository.getBiddingRoundByIdAndProjectIdAndBranchId(
      id,
      projectId,
      branchId
    );

    const lastTemplateFile = await OptimizationFilesRepository.getLastProjectBidTemplateFile(projectId, branchId);

    return new BiddingRoundsDetailsModel(biddingRound, lastTemplateFile);
  }

  public static async getBiddingRoundDetailsForProject(
    projectId: string,
    branchId: string
  ) {
    const biddingRounds = await BiddingRoundsRepository.getBiddingRoundByProjectIdAndBranchId(
      projectId,
      branchId
    );

    const roundIds = biddingRounds.map(r => r.id);
    const kpiResults = roundIds.length ? await runSelectQuery(BIDDING_ROUNDS_LIST_KPI_QUERY, { roundIds }) : [];
    const lookupMap = arrayToMap(kpiResults, 'round_id');

    return biddingRounds.map((biddingRound) => {
      const roundValues = lookupMap[biddingRound.id] || DEFAULT_LIST_VALUES;

      return new BiddingRoundsListModel(
        biddingRound,
        roundValues.offers_count,
        roundValues.suppliers_count
      );
    });
  }
}
