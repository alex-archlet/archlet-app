import { BiddingRounds } from '../../../models/BiddingRounds';
import { OptimizationFiles } from '../../../models/OptimizationFiles';
import { FileModel } from '../../files/FileModel';
import { BiddingRoundsColumnModel } from './BiddingRoundsColumnModel';

interface biddingRoundSettings {
  historicTargetPrices: boolean
}

export class BiddingRoundsDetailsModel {
  public id: string;

  public name: string;

  public description: string;

  public startAt: Date;

  public endAt: Date;

  public reminderAt: Date;

  public statusName: string;

  public createdBy: string;

  public updatedBy: string;

  public createdAt: Date;

  public updatedAt: Date;

  public settings: biddingRoundSettings | null = null;

  public lastTemplateFile: FileModel | null = null;

  public bidImportFiles: FileModel[] = [];

  public columns: BiddingRoundsColumnModel[];

  constructor(biddingRound: BiddingRounds, lastTemplateFile: OptimizationFiles | null) {
    this.id = biddingRound.id;
    this.name = biddingRound.name;
    this.description = biddingRound.description;
    this.startAt = biddingRound.startAt;
    this.endAt = biddingRound.endAt;
    this.reminderAt = biddingRound.reminderAt;
    this.statusName = biddingRound.status.name;

    if (biddingRound.createdByUser) {
      this.createdBy = biddingRound.createdByUser.username;
    }

    if (biddingRound.updatedByUser) {
      this.updatedBy = biddingRound.updatedByUser.username;
    }
    this.createdAt = biddingRound.createdAt;
    this.updatedAt = biddingRound.updatedAt;

    if (lastTemplateFile) {
      this.lastTemplateFile = new FileModel(lastTemplateFile);
    }

    const bidImportFiles = biddingRound.files ? biddingRound.files.filter(({ fileType }) => fileType === 'bid_import') : [];

    this.bidImportFiles = bidImportFiles.map(f => new FileModel(f));

    this.settings = biddingRound.settings;

    this.columns = biddingRound.columns.map(column => new BiddingRoundsColumnModel(column));
  }
}
