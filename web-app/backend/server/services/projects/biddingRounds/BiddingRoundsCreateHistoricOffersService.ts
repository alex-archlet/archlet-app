import { v4 as uuidv4 } from 'uuid';
import {
  Demands, Offers,
} from '../../../models';

type HistoricOfferRows = {
  id: string;
  demandId: string;
  item: string;
  volume: number;
  supplier: string;
  buyerAttributes: {
    [key: string]: string | null | number | undefined;
  };
  supplierAttributes: {
    [key: string]: string | null | number | undefined;
  };
}

export class BiddingRoundsCreateHistoricOffersService {
  public static async createOrUpdateDemandsAndOffers(rows: HistoricOfferRows[], roundId: string, branchId: string, userId: string) {
    const demands = [];
    const offers = [];

    rows.map(async (row) => {
      const parseRow = {
        attributes: row.supplierAttributes,
        isHistoric: true,
        isProcessed: true,
        supplier: row.supplier,
        roundId,
        processing_messages: {},
        branchId,
        createdById: userId,
        demand: {
          item: row.item,
          volume: row.volume,
          attributes: row.buyerAttributes,
          roundId,
          branchId,
          createdById: userId,
          isProcessed: false,
        },
      };

      if (row.id && row.demandId) {
        await Offers.update(parseRow, {
          where: {
            id: row.id,
          },
        });
        await Demands.update(parseRow.demand, {
          where: {
            id: row.demandId,
          },
        });
      } else {
        const offerId = uuidv4();
        const demandId = uuidv4();

        const demand = {
          ...parseRow.demand,
          id: demandId,
        };

        demands.push(demand);

        const offer = {
          ...parseRow,
          id: offerId,
          demandId,
        };

        offers.push(offer);
      }
    });
    await BiddingRoundsCreateHistoricOffersService.bulkCreateDemands(demands);
    await BiddingRoundsCreateHistoricOffersService.bulkCreateOffers(offers);
  }

  public static async bulkCreateDemands(demands: typeof Demands[]) {
    return Demands.bulkCreate(demands);
  }

  public static async bulkCreateOffers(offers: typeof Offers[]) {
    return Offers.bulkCreate(offers);
  }
}
