import { BiddingRounds } from '../../../models/BiddingRounds';

export class BiddingRoundsListModel {
  public id: string;

  public name: string;

  public offersCount: number;

  public suppliersCount: number;

  public statusName: string;

  public createdAt: Date;

  public updatedAt: Date;

  public endAt: Date;

  public startAt: Date;

  public reminderAt: Date;

  constructor(
    round: BiddingRounds,
    offersCount: string | null,
    suppliersCount: string | null
  ) {
    this.id = round.id;
    this.name = round.name;
    this.offersCount = offersCount ? parseInt(offersCount, 10) : null;
    this.suppliersCount = suppliersCount ? parseInt(suppliersCount, 10) : null;
    this.statusName = round.status.name;
    this.createdAt = round.createdAt;
    this.updatedAt = round.updatedAt;
    this.endAt = round.endAt;
    this.startAt = round.startAt;
    this.reminderAt = round.reminderAt;
  }
}
