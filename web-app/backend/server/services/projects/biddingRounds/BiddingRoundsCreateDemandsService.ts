import {
  Demands,
} from '../../../models';

type DemandsRows = {
  id: string;
  item: string;
  volume: number;
  buyerAttributes: {
    [key: string]: string | null | number | undefined
  },
}

export class BiddingRoundsCreateDemandsService {
  public static async createOrUpdateDemands(rows: DemandsRows[], roundId: string, branchId: string, userId: string) {
    rows.map(async (row) => {
      const {
        id,
        item,
        volume,
        buyerAttributes,
      } = row;

      const parsedRow = {
        item,
        volume,
        attributes: buyerAttributes,
        roundId,
        branchId,
        createdById: userId,
        isProcessed: false,
      };

      if (id) {
        await Demands.update(
          parsedRow,
          { where: { id } }
        );
      } else {
        await Demands.create(parsedRow);
      }
    });
  }
}
