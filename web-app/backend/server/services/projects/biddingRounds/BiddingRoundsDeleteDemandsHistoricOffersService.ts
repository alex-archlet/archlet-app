import {
  Demands, Offers, sequelize,
} from '../../../models';

export class BiddingRoundsDeleteDemandsHistoricOffersService {
  public static async deleteDemandsAndOffers(roundId: string) {
    await sequelize.transaction(async () => {
      await Demands.destroy({ where: { roundId } });
      await Offers.destroy({
        where: {
          roundId,
          isHistoric: true,
        },
      });
    });
  }
}

