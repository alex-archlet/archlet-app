import { BidColumns } from '../../../models/BidColumns';

export class BiddingRoundsColumnModel {
  public id: string;

  public name: string;

  public isAutomatic: boolean;

  public isMandatory: boolean;

  public isVisible: boolean;

  public inputBy: string;

  public projectId: string;

  public typeId: number;

  public order: number;

  public matchConfidence: number | null;

  public initialMatchingConfidence: number | null;

  constructor(bidColumn: BidColumns) {
    this.id = bidColumn.id;
    this.name = bidColumn.name;
    this.isAutomatic = bidColumn.isAutomatic;
    this.isMandatory = bidColumn.isMandatory;
    this.isVisible = bidColumn.isVisible;
    this.inputBy = bidColumn.inputBy;
    this.projectId = bidColumn.projectId;
    this.typeId = bidColumn.typeId;
    this.order = bidColumn.order;
    this.matchConfidence = bidColumn.matchingConfidence;
    this.initialMatchingConfidence = bidColumn.initialMatchingConfidence;
  }
}
