import { Projects } from '../../../models/Projects';

type ProjectSettings = {
  name : string;
  unitId: number;
  currencyId: number;
}

export class ProjectSettingUpdateService {
  public static async updateSettings(project: Projects, settings: ProjectSettings) {
    const newSettings = {
      settings: {
        ...project.settings,
        ...settings,
      },
    };

    return project.update(newSettings);
  }
}
