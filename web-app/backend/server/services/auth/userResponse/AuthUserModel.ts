import { Users } from '../../../models/Users';

export class AuthUserModel {
  public accessToken: string;

  public accountType: string;

  public branchId: string;

  public branchName: string;

  public createdAt: Date;

  public email: string;

  public featureFlagIds: number[];

  public firstName: string;

  public id: string;

  public lastLogin: Date;

  public lastName: string;

  public roleId: number;

  public totalLogin: number;

  public updatedAt: Date;

  public username: string;

  public permissions: string[] = [];

  constructor(user: Users, featureFlagIds: number[]) {
    this.accountType = user.accountType;
    this.branchId = user.branchId;
    this.branchName = user.branch.name;
    this.createdAt = user.createdAt;
    this.email = user.email;
    this.featureFlagIds = featureFlagIds;
    this.firstName = user.firstName;
    this.id = user.id;
    this.lastLogin = user.lastLogin;
    this.lastName = user.lastName;
    this.roleId = user.roleId;
    this.totalLogin = user.totalLogin;
    this.updatedAt = user.updatedAt;
    this.username = user.username;

    this.permissions = user.getAllPermissionsOrThrow();
  }
}
