import { Users } from '../../../models/Users';
import { FeatureFlagService } from '../../company/featureFlags/FeatureFlagService';
import { AuthUserModel } from './AuthUserModel';

export class AuthUserService {
  public static async getUserResponse(user: Users) {
    const featureFlagIds = await FeatureFlagService.getFeatureFlagIdsByBranchId(user.branchId);

    return new AuthUserModel(user, featureFlagIds);
  }
}
