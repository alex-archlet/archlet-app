import { publicDomainCn } from '../../config';
import { Projects } from '../../models/Projects';
import { Scenarios } from '../../models/Scenarios';
import { Users } from '../../models/Users';
import { BaseEmailService } from './BaseEmailService';

const EMAIL_TYPE = 'REVIEW_FINAL_SCENARIO';

export class ScenarioReviewEmailService {
  static async sendEmail(user: Users, scenario: Scenarios, project: Projects, comment: string, email: string) {
    const url = `https://${publicDomainCn}/projects/${project.id}/scenariosDetails/${scenario.id}`;

    const msg = {
      type: EMAIL_TYPE,
      header: {
        to: email,
      },
      body: {
        user: `${user.firstName} ${user.lastName}`,
        scenario: scenario.name,
        project: project.name,
        comment,
        url,
      },
    };

    return BaseEmailService.sendEmail(msg);
  }
}
