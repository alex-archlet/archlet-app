import { getQueueConnection } from '../queue';

const QUEUE_NAME = process.env.EMAIL_QUEUE_NAME || 'email-queue';

export class BaseEmailService {
  static async sendEmail(payload: object) {
    const msg = JSON.stringify(payload);
    const connection = await getQueueConnection();
    const ch = await connection.createChannel();

    await ch.assertQueue(QUEUE_NAME);
    await ch.sendToQueue(QUEUE_NAME, Buffer.from(msg));

    return ch.close();
  }
}
