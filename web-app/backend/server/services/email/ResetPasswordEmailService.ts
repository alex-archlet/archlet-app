import { Users } from '../../models/Users';
import { BaseEmailService } from './BaseEmailService';

const EMAIL_TYPE = 'RESET_PASSWORD';

export class ResetPasswordEmailService {
  static async sendEmail(user: Users, password: string) {
    const msg = {
      type: EMAIL_TYPE,
      header: {
        to: user.email,
      },
      body: {
        name: user.firstName,
        email: user.username,
        password,
      },
    };

    return BaseEmailService.sendEmail(msg);
  }
}
