import { OptimizationFiles } from '../../models/OptimizationFiles';

export class FileModel {
  public id: string;

  public branchId: string;

  public roundId: string | null;

  public projectId: string;

  public scenarioId: string | null;

  public name: string;

  public fileExtension: string;

  public fullName: string;

  public fileType: string;

  public createdAt: Date;

  public updatedAt: Date;

  public matchings: object | null;

  public rowNumber: number;

  public sheetName: string;

  constructor(file: OptimizationFiles) {
    this.id = file.id;
    this.branchId = file.branchId;
    this.roundId = file.roundId;
    this.projectId = file.projectId;
    this.scenarioId = file.scenarioId;
    this.name = file.name;
    this.fileExtension = file.fileExtension;
    this.fullName = `${this.name}${this.fileExtension}`;
    this.fileType = file.fileType;
    this.createdAt = file.createdAt;
    this.updatedAt = file.updatedAt;
    this.matchings = file.matchings;
    this.rowNumber = file.rowNumber;
    this.sheetName = file.sheetName;
  }
}
