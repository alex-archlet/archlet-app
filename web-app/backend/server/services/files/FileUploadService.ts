import fs from 'fs';
import path from 'path';
import { Express } from 'express';

import { Companies } from '../../models/Companies';
import { Branches } from '../../models/Branches';
import {
  getSplitedFileName,
  moveFile,
} from '../../utils/common';
import { OptimizationFiles } from '../../models/OptimizationFiles';

export class FileUploadService {
  private static getFileDir(
    companyId,
    branchId,
    projectId,
    fileType
  ) {
    const relativePath = path.join('companies', `${companyId}`, 'branches', `${branchId}`, 'projects', `${projectId}`, 'user_uploads', fileType);
    const absolutePath = path.join(process.env.DATA_PATH, relativePath);

    if (!fs.existsSync(absolutePath)) {
      fs.mkdirSync(absolutePath, { recursive: true });
    }

    return absolutePath;
  }

  public static async uploadFile(
    file: Express.Multer.File,
    company: Companies,
    branch: Branches,
    projectId: string,
    type: string,
    scenarioId: string | null = null,
    roundId: string | null = null,
    rowNumber: number | null = null,
    sheetName: string | null = null
  ): Promise<OptimizationFiles> {
    const newPath = this.getFileDir(company.get('id'), branch.get('id'), projectId, type);
    // TODO: implement versioning logic for special files with versions
    // const version = await FilesController.getFileVersion(projectId, type);
    const version = 0;
    const splitedFileName = getSplitedFileName(file.originalname, type, version);
    const relativePath = newPath.replace(path.join(process.env.DATA_PATH, '/'), '');

    moveFile(file.path, path.join(newPath, splitedFileName.join('')));

    return OptimizationFiles.create({
      contentHash: 'test',
      fileExtension: splitedFileName[2],
      filePath: relativePath,
      fileType: type,
      fileVersion: version,
      name: splitedFileName[0],
      projectId,
      scenarioId,
      branchId: branch.id,
      roundId,
      rowNumber,
      sheetName,
    });
  }
}
