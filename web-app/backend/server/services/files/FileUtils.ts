import path from 'path';

import { getSplitedFileName } from '../../utils/common';
import { OptimizationFilesRepository } from '../../database/repositories/OptimizationFilesRepository';

export class FileUtils {
  public static async getAbsoluteFilePath(
    fileId: string,
    branchId: string
  ): Promise<string> {
    const file = await OptimizationFilesRepository.getOptimizationFileByIdAndBranchId(fileId, branchId);
    const {
      fileExtension,
      filePath,
      fileType,
      fileVersion,
      name,
    } = file;

    const splitFileName = getSplitedFileName(name, fileType, fileVersion, fileExtension);
    const absolutePath = path.join(process.env.DATA_PATH, filePath, splitFileName.join(''));

    return absolutePath;
  }

  public static async getRelativeFilePath(
    fileId: string,
    branchId: string
  ): Promise<string> {
    const file = await OptimizationFilesRepository.getOptimizationFileByIdAndBranchId(fileId, branchId);
    const {
      fileExtension,
      filePath,
      fileType,
      fileVersion,
      name,
    } = file;

    const splitFileName = getSplitedFileName(name, fileType, fileVersion, fileExtension);
    const absolutePath = path.join(filePath, splitFileName.join(''));

    return absolutePath;
  }
}
