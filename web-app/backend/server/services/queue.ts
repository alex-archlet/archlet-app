import amqp, { Connection } from 'amqplib';

const QUEUE_SERVICE_URI = process.env.QUEUE_SERVICE_URI || 'amqp://queue';

let queueConnection;

export const getQueueConnection = async (): Promise<Connection> => {
  if (!queueConnection) {
    queueConnection = await amqp.connect(QUEUE_SERVICE_URI);
  }

  return queueConnection;
};
