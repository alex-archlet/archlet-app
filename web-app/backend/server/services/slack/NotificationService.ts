import axios from 'axios';
import { isProdEnv } from '../../config';

type WebhookAttachment = {
  color: string;
  text: string;
  title: string;
};

type WebhookPayload = {
  channel: string; // #channel / @user in slack
  username?: string; // Override webhook name
  icon_emoji?: string; // Display image
  text: string; // text payload
  attachments?: WebhookAttachment[];
};

// eslint-disable-next-line prefer-destructuring
const SLACK_SIGNUP_URL = process.env.SLACK_SIGNUP_URL;

const isProd = isProdEnv;

export class NotificationService {
  static DEFAULTS = {
    CHANNEL: '#test-env',
    USERNAME: 'Archlet bot',
    ICON: ':rocket:',
  };

  public static async sendSlackNotification(payload: WebhookPayload) {
    if (!SLACK_SIGNUP_URL) {
      return null;
    }

    const {
      channel = this.DEFAULTS.CHANNEL,
      username = this.DEFAULTS.USERNAME,
      icon_emoji = this.DEFAULTS.ICON,
      text = '',
      attachments = [],
    } = payload;

    const message = {
      channel: isProd ? channel : '#test-env',
      username: isProd ? username : 'Archlet bot',
      icon_emoji: isProd ? icon_emoji : ':rocket:',
      text,
      attachments,
    };

    console.info('slack message', message);

    return axios.post(SLACK_SIGNUP_URL, message);
  }
}
