import {
  Request, Response, NextFunction,
} from 'express';

import * as Sentry from '@sentry/node';
import { UserRequest } from './types';

const getUserFromRequest = (req: Request | UserRequest) => ('user' in req ? req.user : null);

export const captureSentryException = async (
  error: Error,
  req: Request,
  _: Response,
  next: NextFunction
) => {
  Sentry.captureException(error, {
    user: getUserFromRequest(req),
  });
  next(error);
};
