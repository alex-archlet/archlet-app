import { Request } from 'express';

import { Users } from './models/Users';

type LogInCallback = (error?: Error) => void;

export interface PassportRequest extends Request {
  session: {
    destroy: Function;
    returnTo?: string;
  };
  logout: Function;
  logIn: (user: any, cb: LogInCallback) => void;
}

export interface UserRequest<TParams = any, TResBody = any, TReqBody = any> extends Request<TParams, TResBody, TReqBody> {
  user: Users;
}
