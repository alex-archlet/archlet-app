# Creating the application

All of the components in this section run in separate processes. The engine, backend and frontend (server) run in Docker containers, the PostgreSQL database
is hosted on Microsoft Azure and the served frontend React code runs in clients'
web browsers.

Communication between all the components is done using a combination of RESTful
requests and Websockets. Requests from the frontend to the backend and from the
backend to the engine are done using HTTP REST calls and full-duplex 
communication between the frontend and engine (with the backend acting as a 
sort of proxy or relay) is done using websockets (specifically the `socket.io`
libarary).

## Python Engine

The Python engine is responsible for executing computationally intensive 
optimization tasks. However, before we look at the computation part of the
engine, let us see how we implement a small RESTful API that processes requests
from the backend.

### RESTful API server

The Python server is implemented using mainly the `ayncio` and `aiohttp` 
libraries. The `ayncio` libraries allows us to run tasks concurrently while the
`aiohttp` library acts in a similar way to the JavaScript Express library in
helping us with receiving and handling HTTP requests.

Note that in the future we should implement an [`asyncio` task queue](https://docs.python.org/3/library/asyncio-queue.html). [Here](https://aiohttp.readthedocs.io/en/stable/) is StackOverflow post on how to make this work with the `aiohttp` library.

Upon receiving a request from the backend, the Python engine then runs the task 
in an executor thread  (`loop.run_in_executor()`) and uses the `socket.io` 
libary to send updates back to the backend.

References:
* [A smalll introduction to `asyncio`](https://realpython.com/async-io-python/)
* [The `aiohttp` library and a small web server example]()
* [The Python `socket.io` library](https://python-socketio.readthedocs.io/en/latest/client.html)
* [The event-loop and `run_in_executor` for CPU-bound tasks](https://docs.python.org/3/library/asyncio-eventloop.html#executing-code-in-thread-or-process-pools)
* [Spawning tasks with `aiojobs`](https://docs.aiohttp.org/en/stable/web_advanced.html)

### Optimization engine

This is the core of the Archlet O tool! All of the actual computation takes 
place here. Documentation coming soon to a `README.md` near you.

References:
* [`socket.io` for Python]()
* [`socket.io` CLI for testing server/client](https://www.npmjs.com/package/iocat)

## Node.js Backend

## ReactJS Frontend

## PostgreSQL Database

The PostgreSQL database runs as a hosted service on Microsoft Azure, see [here]((https://docs.microsoft.com/en-us/azure/postgresql/quickstart-create-server-database-azure-cli)) for more information. Note that 
any PostgreSQL database accessible from the outside internet is suitable but in
the current implementation we happen to have the database hosted on Azure. In
truth, the only thing needed are the database URL (and port), and valid login
 credentials.

# Deploying the application on Azure using Nginx and Docker

## Dependencies

The following tools are expected to be installed:
* Docker for Mac: [installation instructions](https://docs.docker.com/docker-for-mac/install/)
* node: `brew install node`
* npm: `brew install npm`
* yarn: `brew install yarn`
* Azure CLI (with a valid login): `brew install azure-cli`
* Nginx: `brew install nginx`
* PostgreSQL: `brew install postgresql`
* Python 3: `brew install python3` 
* pip: `brew install python-pip`

## Note on PostgreSQL Database

This file assumes the reader has access to a PostgreSQL database running on
`archlet.postgres.database.azure.com`. The user, database and password (as well
as this URL) are all defined accordingly in the `server.js` file. All the steps
except those in the last section (deploying to Azure) can be followed with a
PostgreSQL databse accessible on `localhost:5432`.

## Running the app for development

To start the web application locally in a way that the running applications get
updated continuously as the source code is edited, run the following commands.

Open a terminal window and navigate the to directory containing the source code.

    cd web-app/archlet-o

First, start the frontend:

    cd frontend
    npm start

In a new window, start the backend:

    cd backend
    nodemon server/server.js

You can now access the web application by going on your browser and entering
`localhost:3000`.

NOTE: For development we make use of the development React proxy as setup in
`setupProxy.js`. This stands in for the Nginx reverse-proxy used in production.
Info on React proxying can be found
[here](https://facebook.github.io/create-react-app/docs/proxying-api-requests-in-development).

References:
* [React proxy on development server](https://facebook.github.io/create-react-app/docs/proxying-api-requests-in-development)

## Serving the frontend using an Nginx server and reverse-proxy and in Docker containers locally

### Docker containers

Since we eventually want to run our code on Azure servers, we want to be able to
easily deploy it. One way to do this is to run our code inside Docker containers.

Containers are a sort of lightweight virtual machine, however instead of containing a full-blown OS they work by essentially partitioning resources of a
main Linux system (filesystems, networks, CPU time, etc.).

Since our application consists of several containers we will use `docker-compose` to streamline the deployment process.

### Nginx

Here we explain how the React frontend is served by an Nginx server that also
reverse-proxies all REST API requests back to the backend through `localhost`.

The React code is served to clients' web-browsers. Since we only want to expose
the front end to the public internet (and not the backend), we need a way to
handle RESTful API calls made to the Node backend server running separately. To
do so, we serve the frontend code with Nginx. More info in Nginx can be found 
[here](http://nginx.org/en/docs/beginners_guide.html).

Nginx allows us to run a server which serves compiled React code and creates a
reverse-proxy to the backend allowing us to communicated to the backend using a
local network (i.e. `localhost`). This is particularly useful to us since
eventually the code runs in Docker containers on Azure servers.

References:
* [Nginx as a webserver](https://docs.nginx.com/nginx/admin-guide/web-server/serving-static-content/), note the `try_files` command to work with React compiled code when the page is refreshed
* [Nginx as a reverse proxy (RESTful API)](https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/)
* [Nginx as a reverse proxy (socket.io)](https://www.nginx.com/blog/nginx-nodejs-websockets-socketio/)

### Setup

Let's now build and run all the containers forming our application. First, 
navigate to the source code directory and run `docker-compose build` to build
all the containers with the latest source code:

    cd /archlet-o/
    docker-compose build --parallel

Next, simply use `docker-compose up` to start all the containers:

    docker-compose up

You can now navigate in your web browser to `localhost` to see the web app.

Note that all of the magi here is located in two precious files:
* `docker-compose.yaml`: here is all the container-related setup
* `nginx.conf`: this file contains all of the nginx related setup including
seting up the reverse-proxy and serving the React code.

Finally, note that the React code being served by our Nginx server is compiled
and hence production code unlike the previous section which allowed us to host
a development server with `npm start`.

## Deploy the application to Microsoft Azure cloud services

Finally, we want to run the application on the cloud. We do so with Microsoft
Azure cloud services.

### Setup a multi-container application on Azure

Coming soon.

    az group create \
    --name archlet-group \
    --location "West Europe"

    az appservice plan create \
    --name archlet-plan \
    --resource-group archlet-group \
    --sku S1 \
    --is-linux

    az webapp create \
    --resource-group archlet-group \
    --plan archlet-plan \
    --name archlet-o \
    --multicontainer-config-type compose \
    --multicontainer-config-file docker-compose.yaml

References:
* [Deploying a multicontainer instance on Azure](https://docs.microsoft.com/en-us/azure/container-instances/container-instances-multi-container-yaml)
* [Creating an Azure Container Registry (ACR)](https://docs.microsoft.com/en-us/azure/container-instances/container-instances-tutorial-prepare-acr)

### Setup a PostgreSQL database on Azure

Coming soon.

Current credentials for database:
* Username: `admin`
* Password: `xxxxxx`

References:
* [Create an Azure PostgreSQL database](https://docs.microsoft.com/en-us/azure/postgresql/quickstart-create-server-database-azure-cli)

### Upload container images

Now, we upload out container images to a private Azure repository. You will
need a valid Azure login with access to the Archlet Azure Container Registry 
(ACR) for this step.

First, as usual, navigate to the source code directory and make sure the you
included the latest changes in the Docker container images:

    cd /archlet-o/
    docker-compose build --parallel

Next, login to the Azure CLI and Archler ACR:

    az login
    az acr login --name archlet

You are now ready to push the container images to the cloud:

    docker-compose push

### Deploy the web application

You are now almost done. The last step is to go to the Azure portal to the 
`Settings > Container settings` page for the multi-container application and copy the contents of `docker-compose.yaml` to the `Configuration` field.

## TODO

* Implement work queue in Python (`asyncio.Queue`)
* Define port numbers using environment variables in `docker-compose`