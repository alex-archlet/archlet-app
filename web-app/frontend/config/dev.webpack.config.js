const baseConfig = require('./webpack.config');

const env = 'development';

module.exports = function getDevConfig() {
  const baseConfigObject = baseConfig(env);

  return Object.assign({}, baseConfigObject, {
    devServer: {
      historyApiFallback: true,
      open: true,
    },
  });
};
