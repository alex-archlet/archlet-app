import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { Button } from '@common/components/Button/Button';
import { PrimaryButton } from '@common/components/Button/PrimaryButton';
import { SecondaryButton } from '@common/components/Button/SecondaryButton';
import { CancelButton as CancelBtn } from '@common/components/Button/CancelButton';
import { DeleteButton as DeleteBtn } from '@common/components/Button/DeleteButton';
import { ConfirmButton as ConfirmBtn } from '@common/components/Button/ConfirmButton';
import { ButtonProps } from '@material-ui/core';


export default {
  title: 'Example/Button',
  component: Button,
  argTypes: {
    color: {
      control: {
        type: 'select',
        options: ['inherit', 'primary', 'secondary', 'default'],
      },
    },
    variant: {
      control: {
        type: 'select',
        options: ['text', 'primary', 'outlined', 'contained'],
      },
    },
    disabled: {
      control: {
        type: 'boolean',
      },
    },
    disableElevation: {
      control: {
        type: 'boolean',
      },
    },
    fullWidth: {
      control: {
        type: 'boolean',
      },
    },
    disableFocusRipple: {
      control: {
        type: 'boolean',
      },
    },
    size: {
      control: {
        type: 'select',
        options: ['small', 'medium', 'large', undefined],
      },
    },
  },
} as Meta;

// base = default button
const TemplateBase: Story<ButtonProps> = args => (
  <Button {...args}>{args.children}</Button>
);

export const Default = TemplateBase.bind({});
Default.args = {
  children: 'Default Button',
  variant: 'contained',
  color: 'default'
};

// primary button
const TemplatePrimary: Story<ButtonProps> = args => (
  <PrimaryButton {...args}>{args.children}</PrimaryButton>
);

export const Primary = TemplatePrimary.bind({});
Primary.args = {
  children: 'Primary Button',
  disableElevation: true,
  color: 'primary'
};

// secondary button
const TemplateSecondary: Story<ButtonProps> = args => (
  <SecondaryButton {...args}>{args.children}</SecondaryButton>
);
export const Secondary = TemplateSecondary.bind({});
Secondary.args = {
  children: 'Secondary Button',
  color: 'secondary',
};

// cancel button
const TemplateCancel: Story<ButtonProps> = args => (
  <CancelBtn {...args}>{args.children}</CancelBtn>
);

export const CancelButton = TemplateCancel.bind({});
CancelButton.args = {
  children: 'Cancel Button',
};

// confirm button
const TemplateConfirm: Story<ButtonProps> = args => (
  <ConfirmBtn {...args}>{args.children}</ConfirmBtn>
);

export const ConfirmButton = TemplateConfirm.bind({});
ConfirmButton.args = {
  children: 'Confirm Button',
};

// delete button
const TemplateDelete: Story<ButtonProps> = args => (
  <DeleteBtn {...args}>{args.children}</DeleteBtn>
);

export const DeleteButton = TemplateDelete.bind({});
DeleteButton.args = {
  children: 'Delete Button',
};
