import React from 'react';
import styled from 'styled-components';

const RectangleShape = styled.div`
  height: 6px;
  width: 15px;
  border-radius: 2.5px;
  background-color: #3296ED;
  margin-right: 5px;
`;

const SquareShape = styled.div`
  height: 14px;
  width: 14px;
  border-radius: 2.5px;
  margin-right: 5px;
  background-color: ${props => props.color};
`;


const LargeRectangleShape = styled.div`
  box-sizing: border-box;
  height: 16px;
  width: 56px;
  border: 1px solid  ${props => props.color};
  border-radius: 7.5px;
  background-color:  ${props => props.color};
  margin-right: 84px;
`;

export type ShapeOption = 'rectangle' | 'square' | 'largeRectangleShape' | 'largeSquare';

interface Props {
  color: string,
  shape?: ShapeOption,
}

export const ColorShape: React.FC<Props> = ({ shape, color }) => {
  switch (shape) {
    case 'rectangle':
      return <RectangleShape />;
    case 'square':
      return <SquareShape color={color} />;
    case 'largeRectangleShape':
      return <LargeRectangleShape color={color} />;
    default:
      return null;
  }
};

ColorShape.defaultProps = {
  shape: 'square',
};

