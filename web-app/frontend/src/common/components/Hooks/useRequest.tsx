import { useEffect, useState } from 'react';
import request from '@utils/request';

export function usePostRequest<T>(url: string, payload: object) {
  const [response, setResponse] = useState<T | null>(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const { data } = await request.post<T>(url, payload);

        setResponse(data);
      } catch (e) {
        setError(e);
      }
    };

    fetchData();
  }, [url]);

  return {
    error,
    response,
  };
}

export function useGetRequest<T>(url: string, shouldRefresh?: boolean) {
  const [response, setResponse] = useState<T | null>(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const { data } = await request.get<T>(url);

        setResponse(data);
      } catch (e) {
        setError(e);
      }
    };

    fetchData();
  }, [url, shouldRefresh]);

  return {
    error,
    response,
  };
}
