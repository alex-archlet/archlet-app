import React, { ChangeEvent } from 'react';
import { StyledInput } from '@common/components/Input/Input';

export type EventType = ChangeEvent<HTMLInputElement | HTMLTextAreaElement>;
interface Props {
  value: string;
  label: string;
  placeholder?: string;
  helperText?: string;
  error?: boolean;
  disabled?: boolean;
  rows?: number;
  minRows?: number;
  maxRows?: number;
  onChange: (value: string, event: EventType) => void;
  onBlur?: (event: EventType) => void;
}

export const TextArea: React.FC<Props> = ({
  value,
  label,
  placeholder,
  helperText,
  error,
  disabled,
  rows,
  minRows,
  maxRows,
  onChange,
  onBlur,
}) => {
  const onInputChanged = (event: EventType) => {
    const inputValue = event?.target?.value ?? '';
    if (onChange) {
      onChange(inputValue, event);
    }
  };

  return (
    <StyledInput
      label={label}
      value={value}
      placeholder={placeholder}
      error={error}
      disabled={disabled}
      helperText={helperText}
      rows={rows}
      // @ts-ignore
      minRows={minRows}
      maxRows={maxRows}
      onChange={onInputChanged}
      onBlur={onBlur}
      variant="outlined"
      multiline
      fullWidth
    />
  );
};
