import styled, { css } from 'styled-components';

interface Props {
  align?: string;
  justify?: string;
  flex?: string;
  wrap?: string;
  direction?: string;
}

export const Flex = styled.div`
  display: flex;
  ${(props: Props) => (props.direction === 'column' ?
    css`
          flex-direction: column;
        ` :
    css`
          flex-direction: row;
        `)};
  ${(props: Props) => props.align &&
    css`
      align-items: ${props.align};
    `};
  ${(props: Props) => props.justify &&
    css`
      justify-content: ${props.justify};
    `};
  ${(props: Props) => props.flex &&
    css`
      flex: ${props.flex};
    `};
  ${(props: Props) => props.wrap &&
    css`
      flex-wrap: ${props.wrap};
    `};
  ${(props: Props) => props.direction &&
    css`
      flex-direction: ${props.direction};
    `};
`;
