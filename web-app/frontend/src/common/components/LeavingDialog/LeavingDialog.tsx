import React, { useEffect, useState } from 'react';
import { Prompt, useHistory } from 'react-router-dom';
import { useCustomTranslation } from '@utils/translate';
import { CancelButton } from '@common/components/Button/CancelButton';
import { ConfirmButton } from '@common/components/Button/ConfirmButton';

import { ConfirmationDialog } from '../ConfirmationDialog/ConfirmationDialog';

interface Props {
  when?: boolean;
  confirmedCallback: Function;
}

export const LeavingDialog: React.FC<Props> = ({ when, confirmedCallback }) => {
  const history = useHistory();
  const { t } = useCustomTranslation();

  const [modalVisible, setModalVisible] = useState(false);
  const [lastLocation, setLastLocation] = useState<Location | null>(null);
  const [confirmedNavigation, setConfirmedNavigation] = useState(false);
  const closeModal = () => {
    setModalVisible(false);
    setConfirmedNavigation(true);
  };
  const closeWithoutNavigateModal = () => {
    setModalVisible(false);
    setConfirmedNavigation(false);
  };
  const handleBlockedNavigation = (nextLocation: Location): boolean => {
    if (!confirmedNavigation && nextLocation.search === '') {
      setModalVisible(true);
      setLastLocation(nextLocation);
      return false;
    }
    return true;
  };
  const handleConfirmNavigationClick = () => {
    setModalVisible(false);
    setConfirmedNavigation(true);
    confirmedCallback();
  };

  useEffect(() => {
    if (confirmedNavigation && lastLocation) {
      history.push(lastLocation.pathname);
    }
  }, [confirmedNavigation, lastLocation]);

  return (
    <>
      <Prompt
        when={when}
        // @ts-ignore
        message={location => handleBlockedNavigation(location)}
      />
      <ConfirmationDialog
        isOpen={modalVisible}
        message={t(
          'Do you want to save your changes? Already created scenarios might be invalid and need to be updated.'
        )}
        handleClose={closeWithoutNavigateModal}
        ButtonActions={
          <>
            <ConfirmButton onClick={handleConfirmNavigationClick} autoFocus>
              {t('Save')}
            </ConfirmButton>
            <CancelButton onClick={closeModal}>{t('No')}</CancelButton>
          </>
        }
      />
    </>
  );
};
