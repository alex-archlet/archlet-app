import React, { useState, useEffect } from 'react';
import ReactCountryFlag from 'react-country-flag';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

import { colors } from '@utils/uiTheme';
import { Box, ClickAwayListener, Paper } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { setLanguageRequest } from '@store/actions/user';
import { Text } from '../Text/Text';

interface FlagItem {
  label: string;
  mapCode: string;
  translateCode: string;
  locale: string;
}
const flagItems: FlagItem[] = [
  {
    label: 'English',
    mapCode: 'GB',
    translateCode: 'en',
    locale: 'en-GB',
  },
  {
    label: 'Deutsch',
    mapCode: 'CH',
    translateCode: 'de',
    locale: 'de-CH',
  },
];

const getFlagItemFromTranslateCode = (translateCode) => {
  if (translateCode === null) {
    return flagItems[0];
  }
  const currentLanguage = flagItems.find(
    item => item.translateCode === translateCode
  );

  if (currentLanguage) {
    return currentLanguage;
  }

  return flagItems[0];
};

const StyledUserDropdown = styled.div`
  position: relative;
  font-size: 14px;
  text-align: right;
  margin-right: 5px;
  cursor: pointer;
`;

const StyledPaper = styled(Paper)`
  position: absolute;
  top: 43px;
  right: 0;
  z-index: 10;
  min-width: 100px;
  padding: 13px 15px;
  font-size: 14px;
  line-height: 25px;
  text-align: left;
`;

const FlagDropdownComponent = () => {
  const [, i18n] = useTranslation();

  const [currentLanguage, setCurrentLanguage] = useState(getFlagItemFromTranslateCode(i18n.language));
  const dispatch = useDispatch();

  const [anchorEl, setAnchorEl] = useState(null);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  useEffect(() => {
    dispatch(setLanguageRequest(currentLanguage));
  }, [currentLanguage]);

  const handleLanguageChange = (translateCode) => {
    i18n.changeLanguage(translateCode);
    const newLanguage = getFlagItemFromTranslateCode(translateCode)
    setCurrentLanguage(newLanguage);
    dispatch(setLanguageRequest(newLanguage));
  };

  return (
    <StyledUserDropdown>
      <ClickAwayListener onClickAway={handleClose}>
        <div>
          <Box
            display="flex"
            justifyContent="flex-end"
            alignItems="center"
            marginRight="10px"
            onClick={anchorEl ? handleClose : handleClick}
          >
            <ReactCountryFlag
              className="emojiFlag"
              countryCode={currentLanguage.mapCode}
              style={{
                lineHeight: '2em',
                marginRight: '5px',
              }}
            />
            <Text color={colors.white}>{currentLanguage.label}</Text>
          </Box>
          <Box>
            {anchorEl ? (
              <StyledPaper>
                {flagItems.map(country => (
                  <Box
                    display="flex"
                    justifyContent="flex-between"
                    alignItems="center"
                    onClick={() => handleLanguageChange(country.translateCode)}
                  >
                    <ReactCountryFlag
                      className="emojiFlag"
                      countryCode={country.mapCode}
                      style={{
                        lineHeight: '2em',
                        marginRight: '5px',
                      }}
                    />
                    <Text color={colors.black}>{country.label}</Text>
                  </Box>
                ))}
              </StyledPaper>
            ) : null}
          </Box>
        </div>
      </ClickAwayListener>
    </StyledUserDropdown>
  );
};

export const FlagDropdown = FlagDropdownComponent;
