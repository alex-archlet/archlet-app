/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Box } from '@material-ui/core';
import { Button } from '@common/components/Button/Button';

const StyledInput = styled.input`
  display: none;
`;

const StyledLabel = styled.p`
  margin-bottom: 0;
  font-weight: 500;
  font-size: 11px;
  line-height: 15px;
`;

const StyledCheckboxButton = styled(Button)`
  margin: 6px 7px 0 0 !important;
  border: 1px solid #ffcb30 !important;
  padding: 5px 15px !important;
  background-color: ${props =>
    props.selected ? '#ffcb30' : 'white'} !important;
  font-weight: 500 !important;
  font-size: 9px !important;
  transition: none !important;
`;

export const RadioButtonGroup = ({ form, field, options, title }) => (
  <Box mb={10}>
    <StyledLabel>{title}</StyledLabel>
    {options.map(option => (
      <StyledCheckboxButton
        onClick={() => {
          form.setFieldValue(field.name, option.value);
        }}
        key={option.label}
        selected={form.values[field.name] === option.value}
        name={field.name}
        value={option.value}
        margin="dense"
        variant="outlined"
      >
        <label>
          <StyledInput type="radio" name={option.label} />
          {option.label}
        </label>
      </StyledCheckboxButton>
    ))}
  </Box>
);

RadioButtonGroup.propTypes = {
  field: PropTypes.shape({
    setFieldValue: PropTypes.func.isRequired,
    value: PropTypes.string.isRequired,
  }).isRequired,
  form: PropTypes.shape({}).isRequired,
  input: PropTypes.shape({}).isRequired,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
    })
  ).isRequired,
  title: PropTypes.string.isRequired,
};
