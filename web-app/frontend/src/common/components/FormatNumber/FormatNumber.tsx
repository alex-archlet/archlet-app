import React from 'react';
import { userSelectors } from '@store/selectors/user';
import { useSelector } from 'react-redux';
import { formatNumber, FormattingConfig } from '@utils/formatNumber';

interface Props {
  value: number | string | []; // potentially we will get rid from array
  type?: FormattingConfig;
}

export const LocaleFormatNumber: React.FC<Props> = ({ 
  value, 
  type = {} 
}) => {
  const locale = useSelector(userSelectors.getLocaleLanguage);

  const formattingConfig: FormattingConfig = { ...type, locale };

  if (typeof value === 'string') {
  return <span>{value}</span>;
  }

  // It could happen that api for bid columns will return an array,
  // for example columns ContributingToRebate, DiscountByRebate 
  if (Array.isArray(value)) {
    return <span>-</span>;
  } 
  return <span>{formatNumber(value, formattingConfig)}</span>;  
};
