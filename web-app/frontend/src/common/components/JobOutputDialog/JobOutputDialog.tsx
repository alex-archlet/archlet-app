import React, { useEffect, useMemo, useState } from 'react';
import { jobsSelectors } from '@store/selectors/jobs';
import { useDispatch, useSelector } from 'react-redux';
import { useCustomTranslation } from '@utils/translate';
import { Box, Chip } from '@material-ui/core';
import styled from 'styled-components';
import { Error, Info, Warning } from '@material-ui/icons';
import { colors } from '@utils/uiTheme';
import clsx from 'clsx';
import { setJobOutput } from '@store/actions/jobs';
import { Table } from '../Table/Table';
import { VerticalSpacer } from '../VerticalSpacer/VerticalSpacer';
import { Flex } from '../Flex/Flex';
import { PrimaryButton } from '../Button/PrimaryButton';
import { Modal } from '../Modal/Modal';

const StyledPrimaryButton = styled(PrimaryButton)`
  padding: 0px;
`;
const StyledError = styled(Error)`
  font-size: 16px;
  color: ${colors.error} !important;
`;

const StyledWarning = styled(Warning)`
  font-size: 16px;
  color: ${colors.warning} !important;
`;

const StyledInfo = styled(Info)`
  font-size: 16px;
  color: ${colors.highlightBlue} !important;
`;

const StyledContainer = styled(Box)`
  width: 1000px;
  height: 60vh;
  padding: 30px;
`;

export const StyledChip = styled(Chip)`
  && {
    position: relative;
    width: 125px;
    height: 35px;
    box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.19);
    margin: 0 4px 0 0;
    border-radius: 8px;
    background-color: rgba(255, 255, 255, 1);
    font-size: 14px;
    line-height: 35px;
    text-align: center;
    user-select: none;
  }
  &.active {
    border: 2px ${(props: { borderColor: string }) => props.borderColor} solid;
  }

  &:hover {
    .MuiChip-deleteIcon {
      opacity: 1;
    }
  }
`;

const StyledTitle = styled.p`
  color: ${colors.textColor};
  font-size: 24px;
  letter-spacing: 0;
  margin: 0;
`;

const StyledDescription = styled.p`
  color: ${colors.descriptionColor};
  font-size: 14px;
  margin-bottom: 10px;
`;

const ERROR = 'error';
const WARNING = 'warning';
const INFO = 'info';

const getIcon = (value: string) => {
  switch (value) {
    case ERROR:
      return <StyledError />;
      break;

    case WARNING:
      return <StyledWarning />;
      break;

    default:
      return <StyledInfo />;
      break;
  }
};

type FilterOption = '' | 'warning' | 'error' | 'info';
const chipGenerator: FilterOption[] = [ERROR, WARNING, INFO];

export const JobOutputDialog: React.FC = () => {
  const dispatch = useDispatch();
  const [modal, setModal] = useState<boolean>(false);
  const [currentFilter, setCurrentFilter] = useState<FilterOption>('');
  const jobOutput = useSelector(jobsSelectors.getJobOutput);
  const { t } = useCustomTranslation();

  useEffect(() => {
    if (jobOutput) {
      setModal(true);
    }
  }, [jobOutput]);

  const translateMessageType = {
    error: { value: t('Error'), color: colors.error },
    warning: { value: t('Warning'), color: colors.warning },
    info: { value: t('Info'), color: colors.highlightBlue },
  };

  const columns = [
    {
      Cell: ({ cell: { value } }) => getIcon(value),
      Header: '',
      accessor: value => value.severity,
      id: 'messageIcon',
    },
    {
      Header: t('Message Type'),
      accessor: value => translateMessageType[value.severity].value,
      id: 'messageType',
    },
    {
      Header: t('Message (English)'),
      accessor: value => value.message,
      id: 'message',
    },
  ];

  const currentJobOutputInfo = useMemo(() => {
    let error = 0;
    let warning = 0;
    let info = 0;
    if (jobOutput && jobOutput.length) {
      jobOutput.forEach((message) => {
        if (message.severity === ERROR) {
          error += 1;
        } else if (message.severity === WARNING) {
          warning += 1;
        } else if (message.severity === INFO) {
          info += 1;
        }
      });
    }
    return { error, warning, info };
  }, [jobOutput]);

  const tableData = useMemo(() => {
    if (currentFilter === ERROR) {
      return jobOutput.filter(message => message.severity === ERROR);
    }
    if (currentFilter === WARNING) {
      return jobOutput.filter(message => message.severity === WARNING);
    }
    if (currentFilter === INFO) {
      return jobOutput.filter(message => message.severity === INFO);
    }
    return jobOutput;
  }, [currentFilter, jobOutput]);

  const dynamicTitleAndDescription = useMemo(() => {
    if (currentJobOutputInfo.error && currentJobOutputInfo.warning) {
      return {
        title: t('Errors and Warnings'),
        description: t(
          'During the data processing job, we found the following errors and warnings.'
        ),
      };
    }
    if (currentJobOutputInfo.error) {
      return {
        title: t('Errors'),
        description: t(
          'During the data processing job, we found the following errors.'
        ),
      };
    }
    return {
      title: t('Warnings'),
      description: t(
        'During the data processing job, we found the following warnings.'
      ),
    };
  }, [currentJobOutputInfo, t]);

  return (
    <Modal
      isOpen={modal}
      handleClose={async () => {
        await setModal(false);
        await setCurrentFilter('');
        await dispatch(setJobOutput(null));
      }}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      maxWidth="lg"
    >
      <StyledContainer>
        <StyledTitle>{dynamicTitleAndDescription.title}</StyledTitle>
        <StyledDescription>
          {dynamicTitleAndDescription.description}
        </StyledDescription>
        <Flex align="center">
          {chipGenerator.map(keyword =>
            currentJobOutputInfo[keyword] ? (
              <StyledChip
                label={`${currentJobOutputInfo[keyword]} ${translateMessageType[keyword].value}`}
                icon={getIcon(keyword)}
                clickable
                className={clsx({
                  active: currentFilter === keyword,
                })}
                borderColor={translateMessageType[keyword].color}
                onClick={() => setCurrentFilter(keyword)}
              />
            ) : null
          )}
          {currentFilter && (
            <StyledPrimaryButton onClick={() => setCurrentFilter('')}>
              {t('Clear Selection')}
            </StyledPrimaryButton>
          )}
        </Flex>
        <VerticalSpacer height={5} />
        {tableData && (
          <Table
            data={tableData}
            columns={columns}
            getUniqueKey={(_, key) => key.toString()}
            maxHeight="60vh"
            stickyHeader
          />
        )}
      </StyledContainer>
    </Modal>
  );
};
