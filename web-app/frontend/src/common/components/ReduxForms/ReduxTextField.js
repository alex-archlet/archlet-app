import React from 'react';
import PropTypes from 'prop-types';
import { StyledInput } from '@common/components/Input/Input';

export const ReduxTextField = ({
  input,
  variant,
  margin,
  meta: {
    error,
    invalid,
    touched,
  },
  ...custom
}) => (
  <StyledInput
    error={touched && invalid}
    variant={variant}
    helperText={touched && error}
    margin={margin}
    {...input}
    {...custom}
  />
);

ReduxTextField.propTypes = {
  custom: PropTypes.shape({}),
  input: PropTypes.shape({}).isRequired,
  margin: PropTypes.string,
  meta: PropTypes.shape({
    error: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.array,
    ]),
    invalid: PropTypes.bool,
    touched: PropTypes.bool,
  }).isRequired,
  variant: PropTypes.string,
};

ReduxTextField.defaultProps = {
  custom: {},
  margin: 'normal',
  variant: 'standard',
};
