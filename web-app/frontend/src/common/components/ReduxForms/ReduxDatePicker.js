import React from 'react';
import PropTypes from 'prop-types';

import { DatePicker } from '@material-ui/pickers';
import {
  InputAdornment,
  TextField,
} from '@material-ui/core';
import { CalendarTodayOutlined } from '@material-ui/icons';

export const ReduxDatePicker = ({
  input: {
    onChange,
    value,
    onBlur,
  },
  meta: {
    error,
    invalid,
    touched,
  },
  ...custom
}) => (
  <DatePicker
    disablePast
    disableToolbar
    format="MMMM DD, YYYY"
    variant="dialog"
    emptyLabel="Date"
    TextFieldComponent={props => (
      <TextField
        {...props}
        error={touched && invalid}
        helperText={touched && error}
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <CalendarTodayOutlined />
            </InputAdornment>
          ),
        }}
      />
    )}
    onChange={onChange}
    onClose={onBlur}
    value={value || null}
    {...custom}
  />
);

ReduxDatePicker.propTypes = {
  custom: PropTypes.shape({}),
  input: PropTypes.shape({}).isRequired,
  meta: PropTypes.shape({
    error: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.array,
    ]),
    invalid: PropTypes.bool,
    touched: PropTypes.bool,
  }).isRequired,
};

ReduxDatePicker.defaultProps = {
  custom: {},
};
