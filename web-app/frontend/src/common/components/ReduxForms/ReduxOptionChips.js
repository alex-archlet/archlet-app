import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { Add } from '@material-ui/icons';

import {
  Box,
  Chip,
  TextField,
} from '@material-ui/core';

const StyledChip = styled(Chip)`
  && {
    position: relative;
    height: 25px;
    margin: 0 7px 6px 0;
    border: 1px solid #ffc100;
    border-radius: 3px;
    padding: 4px 3px;
    background-color: white;
    font-weight: 500;
    font-size: 10px;
    line-height: 13px;
  }

  &&.MuiChip-clickable {
    :hover {
      background-color: #eee;
    }
  }

  .MuiChip-deleteIcon {
    position: absolute;
    top: -6px;
    right: -16px;
    opacity: 0;
    fill: rgba(0, 0, 0, 0.38);
    height: 14px;
  }

  &:hover {
    background-color: #eee;

    .MuiChip-deleteIcon {
      opacity: 1;
    }
  }

  svg {
    fill: #ffcb30;
    font-size: 13px;
  }
`;

const StyledTextField = styled(TextField)`
  width: 94px;
  height: 25px;
  padding: 4px 3px;
  background-color: white;

  &&.MuiFormControl-marginDense {
    margin: 0 7px 6px 0;
  }

  && .MuiOutlinedInput-notchedOutline {
    border: 1px solid #ffc100;
    border-radius: 3px;
  }

  && .Mui-focused {
    .MuiOutlinedInput-notchedOutline {
      border: 1px solid #ffc100;
    }
  }

  && .MuiOutlinedInput-root {
    &:hover {
      .MuiOutlinedInput-notchedOutline {
        border: 1px solid #ffc100;
      }
    }
  }

  && .MuiInputBase-inputMarginDense {
    font-weight: 500;
    font-size: 10px;
    line-height: 13px;
  }

  && .MuiInputLabel-marginDense {
    font-weight: 500;
    font-size: 10px;
    line-height: 13px;
    transform: translate(14px, 7px) scale(1);
  }

  && .MuiInputLabel-shrink {
    transform: translate(14px, -6px) scale(0.75);
  }

  .MuiOutlinedInput-inputMarginDense {
    padding-top: 7px;
    padding-bottom: 7px;
  }
`;

export const ReduxOptionChips = ({
  input: {
    onChange,
    value,
  },
}) => {
  const [isAddMode, setIsAddMode] = useState(false);
  const [addLabel, setAddLabel] = useState('');

  const leaveAddMode = () => {
    setAddLabel('');
    setIsAddMode(false);
  };

  const handleKeyUp = (event) => {
    if (event.key === 'Enter') {
      if (!value.includes(addLabel)) {
        onChange([...(value || []), addLabel]);
      }
      leaveAddMode();
    } else if (event.key === 'Escape') {
      leaveAddMode();
    }
  };

  return (
    <Box mt={8}>
      {!!value && value.map(choice => (
        <StyledChip
          autoFocus
          key={choice}
          label={choice}
          onDelete={() => onChange([...value].filter(storedChoice => storedChoice !== choice))}
        />
      ))}
      {isAddMode ? (
        <StyledTextField
          autoFocus
          label="Add option"
          variant="outlined"
          margin="dense"
          value={addLabel}
          onChange={event => setAddLabel(event.currentTarget.value)}
          onKeyUp={handleKeyUp}
          onKeyPress={(e) => {
            if (e.key === 'Enter') e.preventDefault();
          }}
          onBlur={leaveAddMode}
        />
      ) : (
        <StyledChip
          label="Add option"
          icon={<Add />}
          onClick={() => setIsAddMode(true)}
          onKeyUp={() => setIsAddMode(true)}
        />
      )}
    </Box>
  );
};

ReduxOptionChips.propTypes = {
  input: PropTypes.shape({
    onChange: PropTypes.func.isRequired,
    value: PropTypes.arrayOf(PropTypes.string),
  }).isRequired,
};
