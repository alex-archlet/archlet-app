/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Button } from '@common/components/Button/Button';
import { colors } from '@utils/uiTheme';

const StyledInput = styled.input`
  display: none; 
`;

const StyledCheckboxButton = styled(
  ({
    isSelected: _isSelected,
    ...props
  }) => {
    return (<Button {...props} />)
  }
)`
  && {
    cursor: pointer;
    margin: 16px 16px 0 0;
    font-weight: 300;
    background-color: ${props =>
      props.isSelected ? `${colors.iconGrey}` : `${colors.default}`};
    color: ${props =>
      props.isSelected  ? `${colors.white}` : `${colors.black}`};
  }

  &&.Mui-disabled {
    background-color: rgba(0, 0, 0, 0.05);
  }

  &&.MuiButton-contained:hover {
    color: ${colors.black};
  }
`;

export const ReduxRadioButton = ({
  input: { onChange, name, value },
  isDisabled,
  field,
  label,
  option,
  ...custom
}) => {
  const isSelected = () => (field ? option[field] === value : option === value);

  return (
    <StyledCheckboxButton
      onClick={() => onChange(field ? option[field] : option)}
      disabled={isDisabled}
      isSelected={isSelected()}
      {...custom}
    >
      <label>
        <StyledInput type="radio" name={name} />
        {label}
      </label>
    </StyledCheckboxButton>
  );
};

ReduxRadioButton.propTypes = {
  field: PropTypes.string,
  input: PropTypes.shape({}).isRequired,
  isDisabled: PropTypes.bool,
  label: PropTypes.string.isRequired,
  option: PropTypes.oneOfType([PropTypes.shape({}), PropTypes.bool]).isRequired,
};

ReduxRadioButton.defaultProps = {
  field: null,
  isDisabled: false,
};
