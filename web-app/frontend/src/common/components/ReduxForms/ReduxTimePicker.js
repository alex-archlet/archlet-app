import React from 'react';
import PropTypes from 'prop-types';

import { TimePicker } from '@material-ui/pickers';
import { TextField } from '@material-ui/core';

export const ReduxTimePicker = ({
  input: {
    onChange,
    value,
    onBlur,
  },
  meta: {
    invalid,
    touched,
  },
  ...custom
}) => (
  <TimePicker
    disablePast
    ampm={false}
    format="HH : mm"
    emptyLabel="00 : 00"
    TextFieldComponent={props => (
      <TextField
        {...props}
        error={touched && invalid}
      />
    )}
    onClose={onBlur}
    onChange={onChange}
    value={value || null}
    {...custom}
  />
);

ReduxTimePicker.propTypes = {
  custom: PropTypes.shape({}),
  input: PropTypes.shape({}).isRequired,
  meta: PropTypes.shape({
    error: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.array,
    ]),
    invalid: PropTypes.bool,
    touched: PropTypes.bool,
  }).isRequired,
};

ReduxTimePicker.defaultProps = {
  custom: {},
};
