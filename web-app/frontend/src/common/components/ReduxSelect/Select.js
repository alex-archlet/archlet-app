import React from 'react';
import PropTypes from 'prop-types';
import {
  Field,
  reduxForm,
} from 'redux-form';
import { Dropdown } from '@common/components/Dropdown/Dropdown';
import {
  Box,
  FormHelperText,
} from '@material-ui/core';

export const SelectDropdown = ({
  grey,
  items,
  input: {
    onBlur,
    onChange,
    value,
  },
  meta: {
    touched,
    error,
  },
  large,
  medium,
  label,
  multiple,
  allSelected,
  valueKey,
  labelKey,
}) => (
  <div>
    <Dropdown
      items={items}
      onClick={onChange}
      onBlur={onBlur}
      reduxFormValue={value}
      title={label}
      large={large}
      medium={medium}
      grey={grey}
      multiple={multiple}
      allSelected={allSelected}
      valueKey={valueKey}
      labelKey={labelKey}
    />
    {touched && !!error && (
      <FormHelperText error>
        {error}
      </FormHelperText>
    )}
  </div>
);

SelectDropdown.propTypes = {
  allSelected: PropTypes.bool,
  grey: PropTypes.bool,
  input: PropTypes.shape({}).isRequired,
  items: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  label: PropTypes.string,
  labelKey: PropTypes.string,
  large: PropTypes.bool,
  medium: PropTypes.bool,
  meta: PropTypes.shape({}).isRequired,
  multiple: PropTypes.bool,
  valueKey: PropTypes.string,
};

SelectDropdown.defaultProps = {
  allSelected: false,
  grey: false,
  label: 'Select items',
  labelKey: 'label',
  large: false,
  medium: false,
  multiple: false,
  valueKey: 'value',
};

const SelectComponent = ({
  allText,
  onChange,
  grey,
  items,
  large,
  medium,
  label,
  multiple,
  allSelected,
}) => {
  const itemsList = items.map(item => ({
    key: item.key || item,
    value: item === 'all' ? allText : (item.value || item),
  }));

  return (
    <Box
      ml={10}
      mr={10}
    >
      <Field
        name="item"
        component={SelectDropdown}
        onClick={onChange}
        items={itemsList}
        label={label}
        large={large}
        medium={medium}
        grey={grey}
        multiple={multiple}
        allSelected={allSelected}
      />
    </Box>
  );
};

SelectComponent.propTypes = {
  allSelected: PropTypes.bool,
  allText: PropTypes.string,
  grey: PropTypes.bool,
  items: PropTypes.arrayOf(PropTypes.string).isRequired,
  label: PropTypes.string,
  large: PropTypes.bool,
  medium: PropTypes.bool,
  multiple: PropTypes.bool,
  onChange: PropTypes.func,
};

SelectComponent.defaultProps = {
  allSelected: false,
  allText: 'All items',
  grey: false,
  label: 'Select item',
  large: false,
  medium: false,
  multiple: false,
  onChange: () => {},
};

export const Select = reduxForm()(SelectComponent);

