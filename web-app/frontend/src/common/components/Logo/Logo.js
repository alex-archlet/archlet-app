import * as React from 'react';
import styled from 'styled-components';
import logo from '../../../assets/logo_white.svg';

const StyledImg = styled.img`
  height: 24px;
`;

export const Logo = () => (
  <StyledImg
    src={logo}
    alt="logo"
  />
);
