import {
  ArrowBack,
} from '@material-ui/icons';
import styled from 'styled-components';

export const ArrowOutlineLeft = styled(ArrowBack)`
  opacity: 0.75;
  font-size: 15px;
  cursor: pointer;
  border: #57B4D3 2px solid;
  border-radius: 2px;
`;
