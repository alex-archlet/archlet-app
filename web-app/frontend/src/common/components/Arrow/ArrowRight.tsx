import {
  KeyboardArrowRight,
} from '@material-ui/icons';
import styled from 'styled-components';

export const ArrowRight = styled(KeyboardArrowRight)`
  opacity: 0.75;
  font-size: 15px;
  cursor: pointer;
`;
