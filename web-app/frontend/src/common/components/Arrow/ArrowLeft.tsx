import {
  KeyboardArrowLeft,
} from '@material-ui/icons';
import styled from 'styled-components';

export const ArrowLeft = styled(KeyboardArrowLeft)`
  opacity: 0.75;
  font-size: 15px;
  cursor: pointer;
`;
