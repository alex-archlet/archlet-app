import {
  ArrowForward,
} from '@material-ui/icons';
import styled from 'styled-components';

export const ArrowOutlineRight = styled(ArrowForward)`
  opacity: 0.75;
  font-size: 15px;
  cursor: pointer;
  border: #57B4D3 2px solid;
  border-radius: 2px;
`;
