import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import moment from 'moment';

const StyledDetails = styled.p`
  margin: 2px 0;
  color: #767676;
  font-size: 12px;
`;

export const LastEdit = ({ date }) => (
  <StyledDetails>{`Last edit ${moment(date).calendar()}`}</StyledDetails>
);

LastEdit.propTypes = {
  date: PropTypes.string.isRequired,
};
