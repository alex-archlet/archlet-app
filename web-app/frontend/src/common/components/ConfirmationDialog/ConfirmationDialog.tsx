import React from 'react';
import styled from 'styled-components';
import {
  Box,
  Dialog,
  DialogActions,
  IconButton,
} from '@material-ui/core';
import { Text } from '@common/components/Text/Text';
import { Close } from '@material-ui/icons';

const StyledIconButton = styled(IconButton)`
  &&.MuiIconButton-root {
    position: absolute;
    top: 0px;
    right: 0px;
  }
`;


const StyledDialog = styled(Dialog)`
  & .MuiPaper-root {
    width: 400px;
    padding: 30px 18px 15px 30px;
  }
`;

export const ConfirmationDialog = ({
  handleClose,
  isOpen,
  message,
  ButtonActions,
}) => {
  return (
    <StyledDialog
      open={isOpen}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <StyledIconButton onClick={handleClose}>
        <Close />
      </StyledIconButton>
      <Text fontSize={14} fontWeight={400}>
        {message}
      </Text>
      <Box mt={20}>
        <DialogActions>{ButtonActions}</DialogActions>
      </Box>
    </StyledDialog>
  );
};

ConfirmationDialog.defaultProps = {
  message: 'Do you really want to delete this project?',
};
