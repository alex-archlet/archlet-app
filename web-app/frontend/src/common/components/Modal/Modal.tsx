import React from 'react';
import styled from 'styled-components';

import { Close } from '@material-ui/icons';
import { Dialog, IconButton } from '@material-ui/core';

const StyledIconButton = styled(IconButton)`
  &&.MuiIconButton-root {
    position: absolute;
    top: 3px;
    right: 3px;
    flex: 0;
  }
`;
interface Modal {
  isOpen: boolean;
  handleClose: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  children: React.ReactNode;
  maxWidth?: 'xs' | 'sm' | 'md' | 'lg' | 'xl' | false;
}

export const Modal: React.FC<Modal> = ({
  isOpen,
  handleClose,
  children,
  maxWidth,
}) => (
  <Dialog
    open={isOpen}
    onClose={handleClose}
    aria-labelledby="form-dialog-title"
    maxWidth={maxWidth}
    disableEscapeKeyDown
  >
    <StyledIconButton onClick={handleClose}>
      <Close />
    </StyledIconButton>
    {children}
  </Dialog>
);
