import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Box } from '@material-ui/core';
import { colors } from '@utils/uiTheme';
import { Text } from '@common/components/Text/Text';
import { DataTable } from '@common/components/DataTable/DataTable';

const StyledTableContainer = styled(({
  hideBorder: _hideBorder,
  ...props
}) => <Box {...props} />)`
  width: 100%;
  margin-top: 35px;
  border: 1px solid ${props => (props.hideBorder ? 'transparent' : colors[props.type])};
  border-radius: 13px;
  padding: 20px;
`;

const StyledText = styled(Text)`
  margin-left: 7px;
`;

const headers = [
  {
    key: 'location',
    value: 'Location',
  },
  {
    key: 'message',
    value: 'Error / Warning',
  },
  {
    key: 'severity',
    value: 'Type',
  },
];

export const ErrorsList = ({
  cardStyle: {
    type,
    icon,
  },
  hideBorder,
  validation,
}) => (
  <StyledTableContainer
    type={type}
    hideBorder={hideBorder}
  >
    <Box
      display="flex"
      alignItems="center"
      mb={10}
    >
      { icon }
      <StyledText
        fontSize={18}
        color={colors[type]}
        fontWeight={500}
      >
        { `We found ${validation.errors} Errors and ${validation.warnings} Warnings.` }
      </StyledText>
    </Box>
    <DataTable
      data={{
        headers,
        rows: validation.data,
      }}
    />
  </StyledTableContainer>
);

ErrorsList.propTypes = {
  cardStyle: PropTypes.shape({}).isRequired,
  hideBorder: PropTypes.bool,
  validation: PropTypes.shape({}).isRequired,
};

ErrorsList.defaultProps = {
  hideBorder: false,
};
