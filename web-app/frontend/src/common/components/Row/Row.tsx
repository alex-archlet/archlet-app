import React from 'react';
import styled from 'styled-components';

const StyledContainer = styled.div`
  display: flex;
  flex-direction: row;

  && > *:not(:first-child) {
    margin-left: 0;
  }
`;

interface Props {
  children: React.ReactNode;
}

export const Row: React.FC<Props> = ({ children }) => (
  <StyledContainer>
    {children}
  </StyledContainer>
);

