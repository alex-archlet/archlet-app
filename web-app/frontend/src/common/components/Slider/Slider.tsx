import React from 'react';
import { Slider as MuiSlider, SliderProps } from '@material-ui/core';

type Props = SliderProps;

export const Slider = (props: Props) => (
  <MuiSlider
    {...props}
  />
);
