import React from 'react';
import styled from 'styled-components';
import { Button } from '@material-ui/core';

const StyledCheckboxButton = styled(Button)`
  && {
    margin: 16px 16px 0 0;
    font-weight: 300;
    text-transform: none;
  }

  &&.Mui-disabled {
    background-color: rgba(0, 0, 0, 0.05);
    color: rgba(0, 0, 0, 0.4);
  }

  &&.MuiButton-textPrimary {
    margin: 6px 7px 0 0;
    border: 1px solid #ffcb30;
    padding: 5px 15px;
    background-color: white;
    font-weight: 500;
    font-size: 9px;
    transition: none;
  }

  &&.MuiButton-containedPrimary {
    box-shadow: none;
    margin: 6px 7px 0 0;
    background-color: #ffcb30;
    color: black;
    font-weight: 500;
    font-size: 9px;
    transition: none;
  }
`;

interface Props {
  isDisabled?: boolean;
  onChange: Function;
  values: any;
  options: any[];
}

export const GroupedButtons: React.FC<Props> = ({
  onChange,
  values,
  isDisabled,
  options,
}) => (
<>
  {options.map(option => (
    <StyledCheckboxButton
      color="primary"
      onClick={() => onChange(option.value)}
      variant={values === option.value ? 'contained' : 'text'}
      disabled={isDisabled}
    >
      {option.label}
    </StyledCheckboxButton>
  ))}
</>
)

GroupedButtons.defaultProps = {
  isDisabled: false,
};
