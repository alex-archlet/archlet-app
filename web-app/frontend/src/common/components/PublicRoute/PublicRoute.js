import React from 'react';
import PropTypes from 'prop-types';
import {
  Redirect,
  Route,
} from 'react-router-dom';

/**
 * This route is to be used when a logged-in user is trying to access a page
 * that is not useful for them (login, password reset page, etc.)
 */
export const PublicRoute = ({
  component: Component,
  isAuthorized,
  ...rest
}) => (
  <Route
    {...rest}
    render={
      props => (
        isAuthorized ? (
          <Redirect
            to={{
              pathname: '/projects',
              state: { from: props.location },
            }}
          />
        ) : (
          <Component {...props} />
        )
      )
    }
  />
);

PublicRoute.propTypes = ({
  component: PropTypes.elementType.isRequired,
  isAuthorized: PropTypes.bool.isRequired,
  location: PropTypes.shape({}),
  redirect: PropTypes.string,
});

PublicRoute.defaultProps = {
  location: {},
  redirect: '/projects',
};
