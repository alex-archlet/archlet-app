import React from 'react';
import PropTypes from 'prop-types';
import {
  Field,
  reduxForm,
} from 'redux-form';
import { Dropdown } from '@common/components/Dropdown/Dropdown';
import { Box } from '@material-ui/core';

export const SelectRegionDropdown = ({
  grey,
  items,
  input: {
    onChange,
    value,
  },
  large,
  medium,
}) => (
  <Dropdown
    items={items}
    onClick={onChange}
    title="All regions"
    large={large}
    medium={medium}
    grey={grey}
    reduxFormValue={value}
  />
);

SelectRegionDropdown.propTypes = {
  grey: PropTypes.bool,
  input: PropTypes.shape({}).isRequired,
  items: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  large: PropTypes.bool,
  medium: PropTypes.bool,
};

SelectRegionDropdown.defaultProps = {
  grey: false,
  large: false,
  medium: false,
};

const SelectRegionComponent = ({
  grey,
  regions,
  large,
  medium,
}) => {
  const regionsList = Object.keys(regions).map(region => ({
    label: region === 'Global' ? 'All regions' : region,
    value: region,
  }));

  return (
    <Box
      ml={10}
      mr={10}
    >
      <Field
        name="item"
        component={SelectRegionDropdown}
        items={regionsList}
        label="Select region"
        large={large}
        medium={medium}
        grey={grey}
      />
    </Box>
  );
};

SelectRegionComponent.propTypes = {
  grey: PropTypes.bool,
  large: PropTypes.bool,
  medium: PropTypes.bool,
  regions: PropTypes.shape({}).isRequired,
  translator: PropTypes.shape({}).isRequired,
};

SelectRegionComponent.defaultProps = {
  grey: false,
  large: false,
  medium: false,
};

export const SelectRegion = reduxForm({
  enableReinitialize: true,
  form: 'SelectRegionForm',
  initialValues: {
    item: 'Global',
  },
})(SelectRegionComponent);
