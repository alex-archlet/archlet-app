import styled from 'styled-components';
import { drawer, animation } from '@utils/uiTheme';

interface Props {
  backgroundColor?: string,
  marginLeft?: number,
}

export const BackgroundContainer = styled.div`
  ${({ backgroundColor }: Props) => (backgroundColor ? `background-color: ${backgroundColor};` : '')}
  flex-grow: 1;
  transition: margin ${animation.duration.enteringScreen}ms ${animation.timingFunction.sharp} 0;
  margin-left: ${drawer.widthOnlyIcons}px;
  padding-bottom: 10px;
  min-height: calc(100vh - 80px);
  width: calc(100% - ${drawer.widthOnlyIcons}px);
  &.contentShift {
    width: calc(100% - ${({ marginLeft }: Props) => (marginLeft || 0)}px);
    transition: margin ${animation.duration.leavingScreen}ms ${animation.timingFunction.sharp} 0;
    ${({ marginLeft }: Props) => (marginLeft ? `margin-left: ${marginLeft}px;` : '')};
  }
`;
