import React from 'react';
import styled from 'styled-components';
import { Box, LinearProgress, Paper } from '@material-ui/core';
import { Button } from '@common/components/Button/Button';
import { Text } from '@common/components/Text/Text';

const StyledBox = styled(Box)`
  && {
    position: fixed;
    top: 0;
    left: 0;
    z-index: 1400;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.6);
  }
`;

const StyledPaper = styled(Paper)`
  width: 340px;
  height: 330px;
  padding: 30px 27px;
`;

const StyledLinearProgress = styled(LinearProgress)`
  margin: 27px 0;
`;

interface Props {
  progress: number;
  onCancel: () => void;
  title: string;
  iconHelperTextElement: React.ReactNode;
}

export const JobModalProgress: React.FC<Props> = ({
  progress,
  onCancel,
  title,
  iconHelperTextElement: IconHelperTextElement,
}) => (
  <StyledBox>
    <StyledPaper>
      <Text fontSize={18} fontWeight={700}>
        {title}
      </Text>
      <StyledLinearProgress
        variant="determinate"
        color="secondary"
        value={progress}
      />
      {IconHelperTextElement}
      <Box display="flex" justifyContent="flex-end">
        <Button onClick={onCancel}>Cancel</Button>
      </Box>
    </StyledPaper>
  </StyledBox>
);
