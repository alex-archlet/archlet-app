import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { jobsSelectors } from '@store/selectors/jobs';
import { blockingJobCloseAndCancel } from '@store/actions/jobs';

import { JobModalProgress } from './JobModalProgress';

interface Props {
  globalJobProgress: number;
  closeModalAction: () => void;
  progressVisible: boolean;
  title: string;
  iconHelperTextElement: React.ReactNode;
}

const JobModalProgressComponent: React.FC<Props> = ({
  globalJobProgress,
  progressVisible,
  closeModalAction,
  title,
  iconHelperTextElement,
}) => (
  progressVisible ?
    (
      <JobModalProgress
        progress={globalJobProgress}
        onCancel={closeModalAction}
        title={title}
        iconHelperTextElement={iconHelperTextElement}
      />
    ) :
    null
);

const mapStateToProps = state => ({
  globalJobProgress: jobsSelectors.getGlobalJobProgress(state),
  progressVisible: jobsSelectors.getIsGlobalJobRunning(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  closeModalAction: blockingJobCloseAndCancel,
}, dispatch);

export const JobModalProgressContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(JobModalProgressComponent);
