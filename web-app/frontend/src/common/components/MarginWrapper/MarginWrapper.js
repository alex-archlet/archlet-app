import styled from 'styled-components';
import { Box } from '@material-ui/core';

export const MarginWrapper = styled(Box)`
  width: 100%;
  padding: 0 ${props => (props.size === 'small' ? 100 : 250)}px;
`;
