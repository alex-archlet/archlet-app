import React, { ChangeEvent, FocusEvent } from 'react';
import { TextField } from '@material-ui/core';
import styled from 'styled-components';
import { colors } from '@utils/uiTheme';

export type EventType = ChangeEvent<HTMLInputElement | HTMLTextAreaElement>;

interface Props {
  value: string | number;
  label?: string;
  placeholder?: string;
  error?: boolean;
  disabled?: boolean;
  helperText?: string;
  onBlur?:
    | ((event: FocusEvent<HTMLInputElement | HTMLTextAreaElement>) => void)
    | undefined;
  type?: string;
  height?: string;
  fullWidth?: boolean;
  bidImporterStyle?: boolean;
  multiline?: boolean;
  rows?: number;
  onChange: (value: string, event: EventType) => void;
}

const StyledTextField = styled(TextField)`

  && .MuiOutlinedInput-input {
    padding: 10px 11px;
    background-color: white;
  }

  && .MuiOutlinedInput-notchedOutline {
    border: 1px solid #ffc100;
  }


  && .MuiOutlinedInput-root {
    background-color: rgba(109, 114, 120, 0.05);

      .MuiOutlinedInput-notchedOutline {
        border: 1px solid #ccc;
      }
    }
    
    && .Mui-focused {
      .MuiOutlinedInput-notchedOutline {
        border: 1px solid #ffc100;
      }
    }

    &.Mui-disabled,
    &.Mui-disabled:hover {
      .MuiOutlinedInput-notchedOutline {
        border: 5px solid #f9f9f9;
      }
    }
  }

  && .MuiOutlinedInput-multiline {
    padding: 0;
  } 
  && .MuiInputBase-inputMarginDense {
    font-size: 11px;
    line-height: 15px;
  }

  && .MuiInputLabel-marginDense {
    color: rgba(0, 0, 0, 0.34);
    font-size: 11px;
    line-height: 15px;
  }

  &&.MuiFormControl-marginNormal {
    width: 94px;
    margin-right: 7px;
  }

  &&.MuiFormControl-marginDense {
    && .MuiInputLabel-shrink {
      display: none;
    }

    legend {
      width: 0;
    }
  }

`;

export const StyledInput = styled(TextField)`
  &.MuiFormControl-root {
    border-radius: 4px;

    .MuiFormLabel-root {
      &.Mui-focused {
        color: ${colors.black};
      }
      &.Mui-error {
        color: ${colors.error};
      }
    }
  }

  && .Mui-focused {
    .MuiOutlinedInput-notchedOutline {
      border-color: ${colors.black}
      border: 2px solid ${colors.black};
      border-radius: 4px;
    }
  }
  
  && .Mui-error {
    .MuiOutlinedInput-notchedOutline {
      border-color: ${colors.error};
    }
    .Mui-focused {
      border-color: ${colors.error};
    }  
  }

  &.Mui-disabled,
  &.Mui-disabled:hover {
    .MuiOutlinedInput-notchedOutline {
      border: 5px solid #f9f9f9;
    }
  }
`;

export const Input: React.FC<Props> = ({
  value,
  label,
  placeholder,
  helperText,
  error,
  disabled,
  onChange,
  onBlur,
  rows,
  multiline,
  type,
  height = '20px',
  fullWidth = true,
  bidImporterStyle,
}) => {
  const onInputChanged = (event: EventType) => {
    const inputValue = event?.target?.value ?? '';
    if (onChange) {
      onChange(inputValue, event);
    }
  };
  return bidImporterStyle ? (
    <StyledTextField
      inputProps={{
        style: { height },
      }}
      label={label}
      value={value}
      placeholder={placeholder}
      error={error}
      disabled={disabled}
      helperText={helperText}
      onChange={onInputChanged}
      onBlur={onBlur}
      variant="outlined"
      multiline={multiline}
      type={type}
      fullWidth={fullWidth}
      rows={rows}
    />
  ) : (
    <StyledInput
      inputProps={{
        style: { height },
      }}
      label={label}
      value={value}
      placeholder={placeholder}
      error={error}
      disabled={disabled}
      helperText={helperText}
      onChange={onInputChanged}
      onBlur={onBlur}
      variant="outlined"
      multiline={multiline}
      type={type}
      fullWidth={fullWidth}
      rows={rows}
    />
  );
};
