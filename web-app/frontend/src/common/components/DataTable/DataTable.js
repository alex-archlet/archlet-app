import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from '@material-ui/core';
import { colors } from '@utils/uiTheme';

const StyledTableCell = styled(TableCell)`
  && {
    min-width: 100px;
    padding: 10px;
    font-size: 12px;

    &:last-child {
      padding: 12px 10px;
    }
  }
`;

const StyledHeaderCell = styled(TableCell)`
  && {
    min-width: 100px;
    padding: 20px 10px;
    color: ${colors.black};
    font-weight: 500;
    font-size: 14px;
  }
`;

const StyledBox = styled(({
  fullHeight: _fullHeight,
  ...props
}) => <Box {...props} />)`
  overflow: auto;
  height: ${props => (props.fullHeight ? 'calc(100vh - 400px)' : 'auto')};
  max-height: ${props => (props.fullHeight ? 'auto' : '300px')};
`;

export const DataTable = ({
  data: {
    headers,
    rows,
  },
  fullHeight,
  uniqueKey,
}) => (
  <StyledBox fullHeight={fullHeight}>
    <Table>
      <TableHead>
        <TableRow>
          { headers && headers.map(header => (
            <StyledHeaderCell
              size="small"
              key={header.key}
            >
              { header.value }
            </StyledHeaderCell>
          ))}
        </TableRow>
      </TableHead>
      <TableBody>
        { rows && rows.map(row => (
          <TableRow key={row[uniqueKey]}>
            { headers && headers.map(header => (
              <StyledTableCell
                key={header.key}
                size="small"
                padding="none"
              >
                { row[header.key] || '-' }
              </StyledTableCell>
            ))}
          </TableRow>
        )) }
      </TableBody>
    </Table>
  </StyledBox>
);

DataTable.propTypes = ({
  data: PropTypes.shape({
    headers: PropTypes.arrayOf(PropTypes.shape({})),
    rows: PropTypes.arrayOf(PropTypes.shape({})),
    url: PropTypes.string,
  }).isRequired,
  fullHeight: PropTypes.bool,
  uniqueKey: PropTypes.string,
});

DataTable.defaultProps = {
  fullHeight: false,
  uniqueKey: 'id',
};
