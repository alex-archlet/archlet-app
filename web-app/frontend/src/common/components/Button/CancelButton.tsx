import styled from 'styled-components';
import { Button } from '@common/components/Button/Button';
import { colors } from '@utils/uiTheme';

export const CancelButton = styled(Button)`
  && {
    min-width: 90px;
    background-color: ${colors.cancel}; 
    border: 1px solid #e1e5e9;
    color: ${colors.black};

    &:hover {
      color: ${colors.dark};
      background-color: ${colors.darkGrey};
    }
  }
`;
