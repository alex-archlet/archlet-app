import styled from 'styled-components';
import { Button } from '@common/components/Button/Button';
import { colors } from '@utils/uiTheme';

export const DeleteButton = styled(Button)`
&& {
  min-width: 90px;
  background-color: ${colors.error};
  color: ${colors.white};

  &:hover {
    background-color: #98142c;
  }
}
`;
