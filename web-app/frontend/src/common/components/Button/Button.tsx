import React from 'react';
import styled from 'styled-components';
import { ButtonProps, Button as MuiButton } from '@material-ui/core';

export const Button: React.FC<ButtonProps> = styled(
  ({ ...props }: ButtonProps) => (
    <MuiButton {...props} disableElevation variant="contained" />
  )
)`
  && {
    text-transform: none;
  }
`;
