import styled from 'styled-components';
import { Button } from '@common/components/Button/Button';
import { colors } from '@utils/uiTheme';

export const ConfirmButton = styled(Button)`
  && {
    min-width: 90px;
    background-color: ${colors.green};
    color: ${colors.white};
    cursor: pointer;

    &:hover {
      background-color: ${colors.lightGreen};
    }
  }
`;