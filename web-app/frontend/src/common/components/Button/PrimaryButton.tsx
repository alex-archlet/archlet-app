import React from 'react';
import styled from 'styled-components';
import { ButtonProps, Button as MuiButton } from '@material-ui/core';
import { colors } from '@utils/uiTheme';

export const PrimaryButton: React.FC<ButtonProps> = styled(
  ({ ...props }: ButtonProps) => (
    <MuiButton {...props} disableElevation variant="contained" color="primary" />
  )
)`
  && {
    text-transform: none;
    border: 1px solid ${colors.palette.primary.dark};
  }
`;
