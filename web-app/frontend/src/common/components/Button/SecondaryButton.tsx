import React from 'react';
import { Button } from '@common/components/Button/Button';
import { ButtonProps } from '@material-ui/core';

export const SecondaryButton: React.FC<ButtonProps> = ({
  ...props
}: ButtonProps) => <Button {...props} variant="text" color="secondary" />;
