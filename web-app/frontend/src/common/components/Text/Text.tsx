import styled from 'styled-components';

interface Props {
  color?: string,
  fontWeight?: number,
  fontSize?: number,
  lineHeight?: number,
  textAlign?: string,
}

export const Text = styled.p`
  margin: 0;
  color: ${(props: Props) => props.color || 'black'};
  font-weight: ${(props: Props) => props.fontWeight || 300};
  font-size: ${(props: Props) => props.fontSize || 14}px;
  line-height: ${(props: Props) => props.lineHeight || 24}px;
  text-align: ${(props: Props) => props.textAlign || 'left'};
`;
