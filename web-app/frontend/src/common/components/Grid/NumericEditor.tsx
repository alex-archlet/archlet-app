import React, { useRef, useEffect } from 'react';
import { EditorProps } from 'react-data-grid';

type Props = EditorProps<any, any>;

function isNumeric(str) {
  if (typeof str !== 'string') return false;
  return !Number.isNaN(str);
}

export const NumericEditor: React.FC<Props> = ({
  row,
  column,
  onRowChange,
  onClose,
}) => {
  const value = row[column.key];
  const inputElement = useRef<HTMLInputElement>(null);

  useEffect(() => {
    if (inputElement.current !== null) {
      inputElement.current.focus();
    }
  }, []);

  const setNewValueInRow = (newValue) => {
    if (isNumeric(newValue)) {
      return {
        ...row,
        [column.key]: Number(parseFloat(newValue)),
      };
    }
    return {
      ...row,
      [column.key]: newValue,
    };
  };

  return (
    <input
      type="number"
      step={0.01}
      className="rdg-text-editor"
      ref={inputElement}
      value={value}
      onChange={event => onRowChange(setNewValueInRow(event.target.value))}
      onBlur={() => onClose(true)}
      onKeyDown={(event) => {
        if (/^(Arrow(Left|Right)|Home|End)$/.test(event.key)) {
          event.stopPropagation();
        }
      }}
    />
  );
};
