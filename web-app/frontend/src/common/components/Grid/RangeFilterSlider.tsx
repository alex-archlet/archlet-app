import React from 'react';
import { FilterRendererProps } from 'react-data-grid';
import styled from 'styled-components';

import { formatNumber } from '@utils/formatNumber';
import { colors } from '@utils/uiTheme';

import { Slider } from '../Slider/Slider';

const StyledContainer = styled.div`
  height: 100%;
  width: 100%;
  padding: 10px 24px;
  display: flex;
  flex-direction: column;
  line-height: 16px;
`;
const StyledValueContainer = styled.span`
  width: 100%
  text-align: center;
  font-weight: 500;
  color: ${colors.palette.secondary.main};
`;

type Props = FilterRendererProps<any, any> & {
  min: number;
  max: number;
};

export const RangeFilterSlider: React.FC<Props> = ({
  onChange,
  value,
  min,
  max,
}) => {
  const currentValue = value || [min, max];

  return (
    <StyledContainer>
      <StyledValueContainer>
        {formatNumber(currentValue[0], { decimals: 2 })} - {formatNumber(currentValue[1], { decimals: 2 })}
      </StyledValueContainer>
      <Slider
        color="secondary"
        // valueLabelDisplay="auto"
        // @ts-ignore
        onChange={(_: React.ChangeEvent<{}>, newValue: number | number[]) =>
          onChange(newValue)
        }
        value={currentValue}
        valueLabelFormat={formatValue =>
          formatNumber(formatValue, { decimals: 0 })
        }
        min={min}
        max={max}
      />
    </StyledContainer>
  );
};
