import React, { useRef, useEffect } from 'react';
import { EditorProps } from 'react-data-grid';

type Props = EditorProps<any, any>;

export const TextEditor: React.FC<Props> = ({
  row,
  column,
  onRowChange,
  onClose,
}) => {
  const value = row[column.key];
  const inputElement = useRef<HTMLInputElement>(null);

  useEffect(() => {
    if (inputElement.current !== null) {
      inputElement.current.focus();
    }
  }, []);

  const setNewValueInRow = (newValue) => {
    return {
      ...row,
      [column.key]: newValue,
    };
  };

  return (
    <input
      className="rdg-text-editor"
      ref={inputElement}
      value={value}
      onChange={event => onRowChange(setNewValueInRow(event.target.value))}
      onBlur={() => onClose(true)}
      onKeyDown={(event) => {
        if (/^(Arrow(Left|Right)|Home|End)$/.test(event.key)) {
          event.stopPropagation();
        }
      }}
    />
  );
};
