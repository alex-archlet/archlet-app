import {
  stringFilter,
  rangeFilter,
  RangeFilter,
  getMinMaxForColumn,
} from './filterUtils';

describe('string filter utils', () => {
  describe('stringFilter', () => {
    const filterValueReturnsTrue = {
      id: 'g',
    };

    const filterValueReturnsFalse = {
      id: 'x',
    };

    const rowValue = {
      id: 'something',
    };
    const stringFilterWithId = stringFilter('id');
    const stringFilterWithFailedId = stringFilter('wrongId');

    it('should return true without finding the correct id', () => {
      expect(
        stringFilterWithFailedId(rowValue, filterValueReturnsTrue)
      ).toEqual(true);
    });

    it('should return true with finding the correct value in the object', () => {
      expect(stringFilterWithId(rowValue, filterValueReturnsTrue)).toEqual(
        true
      );
    });

    it('should return false because they dont match', () => {
      expect(stringFilterWithId(rowValue, filterValueReturnsFalse)).toEqual(
        false
      );
    });
  });
});

describe('range filter utils', () => {
  describe('rangeFilter', () => {
    const filterArrayReturnsTrue = {
      id: [4, 10] as RangeFilter,
    };

    const filterArrayReturnsFalse = {
      id: [1, 2] as RangeFilter,
    };

    const rangeRowValue = {
      id: 4,
    };
    const rangeFilterWithId = rangeFilter('id');
    const rangeFilterWithWrongId = rangeFilter('WrongId');

    it('should return true because the id isnt being filtered', () => {
      expect(
        rangeFilterWithWrongId(rangeRowValue, filterArrayReturnsTrue)
      ).toBe(true);
    });

    it('should return true because the id matchs and the value matchs', () => {
      expect(rangeFilterWithId(rangeRowValue, filterArrayReturnsTrue)).toBe(
        true
      );
    });

    it('should return false due to id matching and the number isnt in range', () => {
      expect(rangeFilterWithId(rangeRowValue, filterArrayReturnsFalse)).toBe(
        false
      );
    });
  });
});

describe('MinMaxForColumn utils', () => {
  describe('getMinMaxForColumn', () => {
    const rowsValue = [
      {
        id: 4,
      },
      {
        id: 5,
      },
    ];

    const wrongRowsValue = [
      {
        id: 'hello',
      },
      {
        id: 'hi',
      },
    ];

    it('should return min and max because they are numbers', () => {
      expect(getMinMaxForColumn(rowsValue, 'id')).toStrictEqual({
        min: 4,
        max: 5,
      });
    });

    it('should return zeros because they are found ', () => {
      expect(getMinMaxForColumn(wrongRowsValue, 'id')).toStrictEqual({
        min: 0,
        max: 0,
      });
    });
  });
});
