import React from 'react';
import { TableSortLabel } from '@material-ui/core';

const translate = {
  ASC: 'asc',
  DESC: 'desc',
};

export const HeaderCell = ({ sortColumn, sortDirection, onSort, name }) => (
  <TableSortLabel
    direction={translate[sortDirection ?? 'asc']}
    onClick={() =>
      sortColumn &&
      onSort &&
      onSort(sortColumn, sortDirection === 'ASC' ? 'DESC' : 'ASC')
    }
  >
    {name}
  </TableSortLabel>
);
