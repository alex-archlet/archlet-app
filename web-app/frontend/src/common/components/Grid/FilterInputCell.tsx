import React from 'react';
import { FilterRendererProps } from 'react-data-grid';

type Props = FilterRendererProps<any, any>;

export const FilterInputCell: React.FC<Props> = ({ value, onChange }) => (
  <div className="rdg-filter-container">
    <input
      className="rdg-filter"
      value={value}
      onChange={e => onChange(e.target.value)}
    />
  </div>
);
