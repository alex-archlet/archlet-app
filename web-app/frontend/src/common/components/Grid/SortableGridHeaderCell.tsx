import React from 'react';
import { HeaderRendererProps } from 'react-data-grid';
import { HeaderCell } from './HeaderCell';

type Props = HeaderRendererProps<any, any>;

export const SortableGridHeaderCell: React.FC<Props> = ({
  sortColumn,
  sortDirection,
  onSort,
  column,
}) => (
  <HeaderCell
    sortColumn={sortColumn}
    sortDirection={sortDirection}
    onSort={(_, direction) => onSort && onSort(column.key, direction)}
    name={column.name}
  />
);
