import React from 'react';
import { FormatterProps } from 'react-data-grid';
import styled from 'styled-components';

const StyledGridCell = styled.div`
  font-weight: 700;
  padding: 0px 8px;
`;

type Props = FormatterProps<any, any>;

export const ReadOnlyCell: React.FC<Props> = ({ column, row }) => {
  const value = row[column.key];

  return <StyledGridCell>{value}</StyledGridCell>;
};
