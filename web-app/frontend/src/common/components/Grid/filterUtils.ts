type StringFilter = string;
export type RangeFilter = [number, number];

interface FilterShape {
  [key: string]: StringFilter | RangeFilter;
}
export const stringFilter = <T>(id: string) => (
  row: T,
  filters: FilterShape
) => {
  const filterValue = filters[id];
  const rowValue = row[id];
  if (
    rowValue &&
    typeof rowValue === 'string' &&
    filterValue &&
    typeof filterValue === 'string'
  ) {
    return rowValue.toLowerCase().includes(filterValue.toLowerCase());
  }
  return true;
};

export const rangeFilter = <T>(id: string) => (
  row: T,
  filters: FilterShape
) => {
  const filterValue = filters[id];
  const rowValue = row[id];
  if (
    rowValue &&
    typeof rowValue === 'number' &&
    filterValue &&
    Array.isArray(filterValue)
  ) {
    return rowValue >= filterValue[0] && rowValue <= filterValue[1];
  }
  return true;
};

export const getMinMaxForColumn = <T>(rows: T[], columnId: string) => {
  const values = rows.map(row => row[columnId]).filter(Number);
  if (!values.length) {
    return {
      min: 0,
      max: 0,
    };
  }
  return {
    min: Math.min(...values),
    max: Math.max(...values),
  };
};
