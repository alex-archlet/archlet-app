import React from 'react';
import { FormatterProps } from 'react-data-grid';
import styled from 'styled-components';
import { LocaleFormatNumber } from '../FormatNumber/FormatNumber';

const StyledGridCell = styled.div`
  padding: 0px 8px;
`;

type Props = FormatterProps<any, any>;

export const FormatNumberCell: React.FC<Props> = ({ column, row }) => {
  const value = row[column.key];

  return (
    <StyledGridCell>
      <LocaleFormatNumber value={value} type={{ decimals: 2 }} />
    </StyledGridCell>
  );
};
