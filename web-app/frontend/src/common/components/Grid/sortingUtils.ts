function isEmpty(value: any) {
  return typeof value === 'undefined' || value === null || value === '';
}

function makeMeReadyForComparison(value: any) {
  return typeof value === 'string' ? value.toLocaleLowerCase() : value;
}

type ComparisonType<T> = T | number | Date;

export function compareBasic<T>(rawA: T, rawB: T, desc: boolean) {
  let a: ComparisonType<T> = rawA;
  let b: ComparisonType<T> = rawB;

  // This is for date column
  if (a instanceof Date) {
    a = a.getTime();
  }

  if (b instanceof Date) {
    b = b.getTime();
  }

  // Lowercase comparison only
  a = makeMeReadyForComparison(a);
  b = makeMeReadyForComparison(b);

  if (a === b) {
    return 0;
  }

  if (isEmpty(a)) {
    return desc ? -1 : 1;
  }

  if (isEmpty(b)) {
    return desc ? 1 : -1;
  }
  if (desc) {
    return a > b ? 1 : -1;
  }

  return a > b ? -1 : 1;
}

export function sorting<T>(rowA: T, rowB: T, columnId: string, desc = false) {
  const a = rowA[columnId];
  const b = rowB[columnId];

  return compareBasic(a, b, desc);
}
