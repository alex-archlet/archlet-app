import React, { useCallback, useMemo, useRef, useState } from 'react';
import { useCustomTranslation } from '@utils/translate';
import DataGrid, {
  DataGridHandle,
  Filters,
  Column,
  DataGridProps,
} from 'react-data-grid';

import 'react-data-grid/dist/react-data-grid.css';
import styled from 'styled-components';
import { Position } from 'react-data-grid/lib/types';
import { Button } from '../Button/Button';
import { Flex } from '../Flex/Flex';
import { VerticalSpacer } from '../VerticalSpacer/VerticalSpacer';
import { Tooltip } from '../Tooltip/Tooltip';
import { sorting } from './sortingUtils';

export type SortDirection = 'ASC' | 'DESC' | 'NONE';

export enum CellStyleClass {
  Warning = 'warning-cell',
  Error = 'error-cell',
  Changed = 'change-cell',
}
type StyledDataGridProps = DataGridProps<any, any> &
  React.RefAttributes<DataGridHandle>;

const StyledGrid = styled(DataGrid)<StyledDataGridProps>`
  height: 70vh !important;
  & .rdg-cell {
    padding: 0px;
  }
  & .rdg-filter-container {
    padding: 0 8px;
  }
  & .header {
    padding: 0 8px;
  }

  & .${CellStyleClass.Warning} {
    box-shadow: inset 0 0 0 3px yellow;
  }

  & .${CellStyleClass.Error} {
    box-shadow: inset 0 0 0 3px red;
  }

  & .${CellStyleClass.Changed} {
    box-shadow: inset 0 0 0 3px blue;
  }
`;
interface BidColumnFlatValues {
  [bidColumnId: string]: string | number;
}
type StructureEnum = {
  enum: string | null;
};
interface BidColumnIdAndProcessMessage {
  id: string;
  processing_messages: {
    [bidColumnId: string]: {
      change?: number | string;
      error?: StructureEnum;
      warning?: StructureEnum;
    };
  };
}
export type BidColumnFlatRow = BidColumnFlatValues &
  BidColumnIdAndProcessMessage;

export interface CustomColumn extends Column<BidColumnFlatRow, unknown> {
  filterFunction: Function;
}
type KeyPairValue = {
  [key: string]: string | number;
};
interface Props {
  columns: CustomColumn[];
  addRowEnabled?: boolean;
  grid: KeyPairValue[];
  setGrid: (rows: BidColumnFlatRow[]) => void;
  enabledFilters?: boolean;
  setCurrentCell?: (position: Position) => void;
}

const rowKeyGetter = (row: { id: string }): React.ReactText => {
  return row.id;
};

export const Grid: React.FC<Props> = ({
  columns,
  grid,
  setGrid,
  addRowEnabled,
  setCurrentCell,
  enabledFilters,
}) => {
  const { t } = useCustomTranslation();
  const [filters, setFilters] = useState<Filters>({});
  const [selectedRows, setSelectedRows] = useState(() => new Set<React.Key>());
  const [[sortColumn, sortDirection], setSort] = useState<
    [string, SortDirection]
  >(['id', 'NONE']);

  const gridRef = useRef<DataGridHandle>(null);

  const filteredRows = useMemo(() => {
    if (!filters) {
      return grid;
    }
    const filterFunctions = columns
      .filter(editableColumn => editableColumn.filterFunction)
      .map(editableColumn => editableColumn.filterFunction);
    return grid.filter(r => filterFunctions.every(func => func(r, filters)));
  }, [grid, filters]);

  const addRow = async () => {
    const objKey = {};
    const row = Object.keys(grid[0]);
    row.forEach((key) => {
      objKey[key] = null;
    });
    // @ts-ignore
    await setGrid([...grid, objKey]);
    if (gridRef?.current) {
      gridRef.current.scrollToRow(Number(grid.length + 1));
    }
  };

  const sortedRows = useMemo(() => {
    if (sortDirection === 'NONE') return filteredRows;
    let copySortedRows = [...filteredRows];
    if (sortColumn) {
      copySortedRows = copySortedRows.sort((a, b) =>
        sorting(a, b, sortColumn, sortDirection === 'DESC')
      );
    }
    return copySortedRows;
  }, [filteredRows, sortDirection, sortColumn]);

  const handleSort = useCallback(
    (columnKey: string, direction: SortDirection) => {
      setSort([columnKey, direction]);
    },
    []
  );

  return (
    <>
      {addRowEnabled && (
        <>
          <Flex justify="flex-end">
            <Tooltip title={t('Placeholder')}>
              <Button onClick={() => addRow()}>{t('Add Item')}</Button>
            </Tooltip>
          </Flex>
          <VerticalSpacer height={10} />
        </>
      )}
      {/* @ts-ignore */}
      <StyledGrid
        className="rdg-light"
        ref={gridRef}
        columns={columns}
        rows={sortedRows}
        onCheckCellIsEditable={() => true}
        onRowsChange={setGrid}
        selectedRows={selectedRows}
        onSelectedRowsChange={setSelectedRows}
        rowKeyGetter={rowKeyGetter}
        enableFilterRow={enabledFilters}
        enableCellCopyPaste
        filters={filters}
        onFiltersChange={setFilters}
        sortColumn={sortColumn}
        sortDirection={sortDirection}
        onSort={handleSort}
        onSelectedCellChange={setCurrentCell}
        headerFiltersHeight={50}
      />
    </>
  );
};
