import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import { Text } from '../Text/Text';

const StyledText = styled(Text)`
  padding-left: 25px;
  white-space: nowrap;
`;

const FlexContainer = styled.div`
  padding: 15px 10px 10px 10px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
`;

export const PageTitle = ({ title, titleColor, rightSide }) => (
  <FlexContainer>
    <StyledText fontSize={26} fontWeight={500} color={titleColor}>
      {title}
    </StyledText>
    {rightSide}
  </FlexContainer>
);

PageTitle.propTypes = {
  rightSide: PropTypes.element,
  title: PropTypes.string.isRequired,
  titleColor: PropTypes.string,
};

PageTitle.defaultProps = {
  rightSide: null,
  titleColor: '#000000',
};
