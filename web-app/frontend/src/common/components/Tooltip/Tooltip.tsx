
import React from 'react';
import styled from 'styled-components';
import {
  Tooltip as MuiTooltip,
} from '@material-ui/core';

export const Tooltip = styled(props => (
  <MuiTooltip
    classes={{
      popper: props.className,
      tooltip: 'tooltip',
    }}
    {...props}
  />
))`
  & .tooltip {
    border-radius: 5px;
    margin-bottom: 0px;
    background-color: #FFFFFF;
    color: #44566C;
    box-shadow: 0 2px 10px 0 rgba(10,22,70,0.1);
    font-size: 14px;
    padding: 10px;
    max-width: 200px;
  }
`;
