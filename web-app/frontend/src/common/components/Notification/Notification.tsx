import React from 'react';
import { useSelector } from 'react-redux';
import { default as DisplayNotifications } from 'react-notification-system-redux';
import { notificationsSelectors } from '@selectors/notifications';
import { colors } from '@utils/uiTheme';

type Level = 'success' | 'error';
type Position = 'tr' | 'tl' | 'tc' | 'br' | 'bl' | 'bc';

interface NotificationOptions {
  uid?: string;
  title?: string;
  level: Level;
  message: string;
  position: Position;
  autoDismiss: number;
  action?: {
    label: string;
    callback: () => void;
  };
}

interface NotificationProps {
  notifications: Notification[];
}

export const notificationOpts: NotificationOptions = {
  level: 'error',
  message: 'something went wrong',
  position: 'tr',
  autoDismiss: 5, // Delay in seconds for the notification go away.
  action: undefined,
};

const style = {
  NotificationItem: {
    // Override the notification item
    DefaultStyle: {
      // Applied to every notification, regardless of the notification level
      margin: '10px 5px 2px 1px',
    },

    // Applied only to the success notification item
    success: {
      color: colors.green,
      border: `2px solid ${colors.green}`,
    },

    // Applied only to the error notification item
    error: {
      color: colors.error,
      border: `2px solid ${colors.error}`,
    },
  },
};

export const Notification: React.FC<NotificationProps> = () => {
  const notifications = useSelector(notificationsSelectors.getNotifications);

  if (notifications.length > 0) {
    return <DisplayNotifications notifications={notifications} style={style} />;
  }
  return null;
};
