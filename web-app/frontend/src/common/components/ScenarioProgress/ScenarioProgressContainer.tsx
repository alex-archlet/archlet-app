import React from 'react';

import { jobsSelectors } from '@store/selectors/jobs';
import { blockingJobCloseAndCancel } from '@store/actions/jobs';
import { useDispatch, useSelector } from 'react-redux';

import { ScenarioProgress } from './ScenarioProgress';

interface Props {
  title: string;
}

export const ScenarioProgressContainer: React.FC<Props> = ({ title }) => {
  const dispatch = useDispatch();
  const closeModalAction = () => dispatch(blockingJobCloseAndCancel());

  const globalJobProgress = useSelector(jobsSelectors.getGlobalJobProgress);
  const progressVisible = useSelector(jobsSelectors.getIsGlobalJobRunning);

  return (
    progressVisible && (
      <ScenarioProgress
        value={globalJobProgress}
        onCancel={closeModalAction}
        title={title}
      />
    )
  );
};
