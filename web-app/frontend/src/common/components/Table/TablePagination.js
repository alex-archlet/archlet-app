import React from 'react';
import PropTypes from 'prop-types';
import {
  FirstPage,
  KeyboardArrowLeft,
  KeyboardArrowRight,
  LastPage,
} from '@material-ui/icons';

import styled from 'styled-components';
import {
  Box, IconButton, MenuItem, Select,
} from '@material-ui/core';

const StyledContainer = styled(Box)`
  display: flex;
  align-items: center;
`;

const StyledBox = styled(Box)`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const StyledElement = styled(Box)`
margin-left: 10px;
padding: 2px;
`;

const StyledText = styled.p`
  margin: 2px 0;
  color: #767676;
  font-size: 12px;
  white-space: pre;
`;

const StyledMenuItem = styled(MenuItem)`
  && .MuiSelect-select.MuiSelect-select {
    padding-left: 5px;
    padding-right: 5px;
  }
`;

const StyledStrongText = styled.strong`

`;

export const TablePagination = ({
  goToPage,
  canPreviousPage,
  previousPage,
  canNextPage,
  nextPage,
  pageCount,
  pageIndex,
  pageOptions,
  pageSize,
  setPageSize,
}) => (
  <StyledContainer>
    <IconButton
      onClick={() => goToPage(0)}
      disabled={pageIndex === 0}
      aria-label="first page"
    >
      <FirstPage />
    </IconButton>
    <IconButton
      onClick={() => previousPage()}
      disabled={!canPreviousPage}
      aria-label="previous page"
    >
      <KeyboardArrowLeft />
    </IconButton>
    <IconButton
      onClick={() => nextPage()}
      disabled={!canNextPage}
      aria-label="next page"
    >
      <KeyboardArrowRight />
    </IconButton>
    <IconButton
      onClick={() => goToPage(pageCount - 1)}
      disabled={!canNextPage}
      aria-label="last page"
    >
      <LastPage />
    </IconButton>
    <StyledBox>
      <StyledElement>
        <StyledText>
          Page
          <StyledStrongText>
            {` ${pageIndex + 1} `}
            of
            {` ${pageOptions.length} `}
          </StyledStrongText>
        </StyledText>
      </StyledElement>
      <StyledElement>
        <Select
          value={pageSize}
          labelId="demo-simple-select"
          id="demo-simple-select"
          label="Items per Page"
          inputField={StyledText}
          disableUnderline
          onChange={(e) => {
            setPageSize(Number(e.target.value));
          }}
        >
          {[20, 50, 100].map(pageSizeValue => (
            <StyledMenuItem
              key={pageSizeValue}
              value={pageSizeValue}
            >
              {`${pageSizeValue}`}
            </StyledMenuItem>
          ))}
        </Select>
      </StyledElement>
    </StyledBox>
  </StyledContainer>
);

TablePagination.propTypes = {
  canNextPage: PropTypes.bool.isRequired,
  canPreviousPage: PropTypes.bool.isRequired,
  goToPage: PropTypes.func.isRequired,
  nextPage: PropTypes.func.isRequired,
  pageCount: PropTypes.number.isRequired,
  pageIndex: PropTypes.number.isRequired,
  pageOptions: PropTypes.arrayOf(PropTypes.number.isRequired).isRequired,
  pageSize: PropTypes.number.isRequired,
  previousPage: PropTypes.func.isRequired,
  setPageSize: PropTypes.func.isRequired,
};
