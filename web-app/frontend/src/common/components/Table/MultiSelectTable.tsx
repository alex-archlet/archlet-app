import React, { useMemo } from 'react';
import {
  Checkbox,
} from '@material-ui/core';
import { useCustomTranslation } from '@utils/translate';
import { Table, Props as TableProps } from './Table';

interface Props extends TableProps {
  onChange: Function;
  selected: object;
};

export const MultiSelectTable: React.FC<Props> = ({
  columns, data, defaultSorting, selected, onChange, tableActions,
}) => {
  const { t } = useCustomTranslation();
  const checkBoxDeclaration = useMemo(() => {
    return {
     Cell: ({ cell: { row } }) => (
       <Checkbox
         checked={selected[row.id] ?? false}
         onChange={() => onChange(row)}
       />
     ),
     Header: t('Select'),
     id: 'Select',
     tooltip: false,
   };
  }, [selected, onChange]);

  const newColumns = useMemo(() => [checkBoxDeclaration, ...columns], [checkBoxDeclaration, columns]);

  return (
    <Table
      columns={newColumns}
      data={data}
      defaultSorting={defaultSorting}
      size="small"
      maxHeight={500}
      stickyHeader
      tableActions={tableActions}
    />
  );
};
