import React from 'react';
import { Radio } from '@material-ui/core';
import { Table, Props as TableProps } from './Table';

interface Props extends TableProps {
  onChange: Function;
  selected: object;
};

export const SingleSelectTable: React.FC<Props> = ({
  columns,
  data,
  selected,
  onChange,
}) => {
  const CheckBoxDeclaration = {
    Cell: ({ cell: { row } }) => (
      <Radio
        checked={selected[row.id] ?? false}
        onChange={() => onChange(row)}
      />
    ),
    Header: 'Select',
    id: 'Select',
    tooltip: false,
  };

  const newColumns = [CheckBoxDeclaration, ...columns];

  return (
    <Table
      columns={newColumns}
      data={data}
      size="small"
      maxHeight={500}
      stickyHeader
    />
  );
};
