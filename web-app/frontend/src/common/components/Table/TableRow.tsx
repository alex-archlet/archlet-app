import styled from 'styled-components';
import {
  TableRow as MuiTableRow,
} from '@material-ui/core';

export const TableRow = styled(MuiTableRow)`
  &:first-child .MuiTableCell-body {
    border-top: none !important;
  }
`;
