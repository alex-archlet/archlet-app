import React from 'react';
import { ColumnInstance, HeaderGroup } from 'react-table';
import styled from 'styled-components';
import { FormattingConfig } from '../../../utils/formatNumber';
import { CellCallback, CustomColumn, Row, Footer, Accessor } from './Table';
import { LocaleFormatNumber } from '../FormatNumber/FormatNumber';

export const formatCell: CellCallback = (
  { cell: { value } }: Row,
  formattingProps: FormattingConfig | undefined = undefined
) => {
  if (typeof value !== 'number') {
    return '-';
  }

  return <LocaleFormatNumber value={value} type={formattingProps} />
};

export const StyledLabel = styled.span`
  color: ${props => props.color};
`;

const getValueFromAccessor = (row: any, accessor: Accessor) => {
  if (typeof accessor === 'string') {
    return row[accessor];
  }
  return accessor(row);
};

export const calculateColumnSum = (
  footer: Footer,
  customAccessor?: Accessor
) => {
  const accessor = customAccessor ?? footer.column.accessor;

  if (!accessor) {
    return 0;
  }

  return footer.data.reduce((acc: number, row: any) => {
    const value = Number(getValueFromAccessor(row, accessor)) || 0;
    return acc + value;
  }, 0);
};

export const calculateColumnSumWithFormatting = (
  footer: Footer,
  customAccessor?: Accessor
) => {
  const value = calculateColumnSum(footer, customAccessor);
  return <LocaleFormatNumber value={value} />
};

export const hasHeaderSub = (column: CustomColumn) => Boolean(column.HeaderSub);
export const isFirstColumnOfParentGroup = (
  column: HeaderGroup | ColumnInstance
) => {
  const { parent, id } = column;

  if (!parent) {
    return false;
  }

  const { columns } = parent;

  if (!columns) {
    return false;
  }

  const indexOfColumnInParent = columns.findIndex(col => col.id === id);

  return indexOfColumnInParent === 0;
};
export const isParentColumn = ({ columns }: HeaderGroup) =>
  columns && columns.length > 0;
export const hasSubRows = (item: any) =>
  item.subRows && item.subRows.length > 0;
export const getChildColumns = ({ columns }: CustomColumn) => columns || [];

export const getCellColor = (
  value: number,
  oneThreshold = 0,
  secondThreshold = 0
) => {
  if (value > oneThreshold) {
    return 'green';
  }
  if (value < secondThreshold) {
    return 'red';
  }
  return 'black';
};
export const hasChildColumns = (col: CustomColumn) =>
  getChildColumns(col).length > 0;
export const hasFooterRows = ({ Footer: footer }: CustomColumn) =>
  footer !== undefined;
