import React, { useMemo } from 'react';
import {
  Column,
  useExpanded,
  usePagination,
  useSortBy,
  useTable,
  ColumnInstance,
  HeaderGroup,
} from 'react-table';
import {
  IconButton,
  Table as MuiTable,
  TableRow as MuiTableRow,
  Size,
  TableBody,
  TableContainer,
  TableHead,
  TableSortLabel,
} from '@material-ui/core';
import { ExpandLess, ExpandMore } from '@material-ui/icons';

import { arrayToMap } from '@utils/array';

import { PrimaryButton } from '@common/components/Button/PrimaryButton';
import { TablePagination as CustomTablePagination } from './TablePagination';
import { Flex } from '../Flex/Flex';
import {
  getChildColumns,
  hasChildColumns,
  hasHeaderSub,
  hasSubRows,
  hasFooterRows,
  isFirstColumnOfParentGroup,
  isParentColumn,
} from './utils';
import { TooltipFormatter } from './TooltipFormatter';
import {
  FirstChildStyledTableCell,
  FirstStyledTableCell,
  HeaderSubContainer,
  StyledChildTableCell,
  StyledColumn,
  StyledTableCell,
  StyledTableHeaderCell,
  StyledTableParentHeaderCell,
  StyledTableHeaderRowCell,
  StyledFixedTableCell,
  StyledFixedTableHeaderCell,
  StyledFixedTableHeaderRowCell,
  StyledFixedChildTableCell,
} from './Cell';
import { numberSort } from './sortingUtils';
import { TableRow } from './TableRow';

type UniqueKeyFunction = (row: any, reactTableDefaultIndex: number) => string;
export type RowSelectFunction = (row: any, rowKey: string) => void;

export interface Row {
  row: {
    original: any;
    isExpanded: boolean;
    canExpand: boolean;
    getToggleRowExpandedProps: Function;
  };
  cell: {
    value: any;
    row: any;
  };
}
export interface Footer {
  data: any[];
  rows: any[];
  column: CustomColumn;
  columns: CustomColumn[];
}
export type SimpleCellValue = any;
export type Header = any;
export type CellCallback = (row: Row) => SimpleCellValue;
export type FooterCallback = (footer: Footer) => SimpleCellValue;
export type TooltipType = boolean | CellCallback;
export type AccessorFunction = (row: any) => any;
export type Accessor = AccessorFunction | string;
type SortType = 'DATETIME' | 'NUMBER' | 'STRING' | Function;

export type CustomHeaderGroups = HeaderGroup & {
  fixed?: boolean;
  HeaderSub?: string;
};
export type CustomColumnInstance = ColumnInstance & {
  fixed?: boolean;
};

export type CustomColumn = Column & {
  Header?: Header;
  Footer?: SimpleCellValue | FooterCallback;
  id: string;
  Cell?: SimpleCellValue | CellCallback;
  columns?: CustomColumn[];
  fixed?: boolean;
  HeaderSub?: string;
  accessor?: Accessor;
  sortType?: SortType;
  sortable?: boolean;
  tooltip?: TooltipType;
};

export type TableAction = {
  label: string;
  onClick: Function;
  disabled?: boolean;
};

interface DefaultSorting {
  desc: boolean;
  id: string;
}

export interface Props {
  columns: CustomColumn[];
  data: any[];
  defaultSorting?: DefaultSorting[];
  expandable?: boolean;
  getUniqueKey?: UniqueKeyFunction;
  onSelectRow?: RowSelectFunction;
  selectedRowId?: string;
  maxColumnWidth?: number;
  onRowHoverEnter?: (rowKey: string) => void;
  onRowHoverLeave?: (rowKey: string) => void;
  size?: Size;
  stickyHeader?: boolean;
  maxHeight?: number | string;
  tableActions?: TableAction[];
}

const SORTING_DIRECTION = {
  ASC: 'asc',
  DESC: 'desc',
};

export const SORTING_TYPE = {
  DATETIME: 'datetime',
  NUMBER: numberSort,
  STRING: 'alphanumeric',
};

const EXPANDER_ID = 'expander';
const expandColumn = {
  // Build our expander column
  // Use the row.canExpand and row.getToggleRowExpandedProps prop getter
  // to build the toggle for expanding a row
  Cell: ({ row }: Row) =>
    row.canExpand ? (
      <IconButton {...row.getToggleRowExpandedProps()} size="small">
        {row.isExpanded ? <ExpandLess /> : <ExpandMore />}
      </IconButton>
    ) : null,
  Header: '',
  id: EXPANDER_ID, // Make sure it has an ID
};
const defaultGetUniqueKey = (obj: any) => obj.id;
const defaultOnSelectRow = () => {};

export const Table: React.FC<Props> = ({
  columns,
  data,
  size,
  expandable,
  getUniqueKey,
  selectedRowId,
  onSelectRow = defaultOnSelectRow,
  defaultSorting,
  maxColumnWidth,
  onRowHoverEnter,
  onRowHoverLeave,
  stickyHeader,
  maxHeight,
  tableActions,
}) => {  
  const memoColumns = useMemo(() => {
    // check whether any rows have child rows
    const hasAnyRowSubRows = data.some(hasSubRows);

    return hasAnyRowSubRows && expandable
      ? [expandColumn, ...columns]
      : columns;
  }, [columns, expandable, data]);

  const hasAnyColumnSubColumns = useMemo(
    () => memoColumns.filter(hasChildColumns).length > 0,
    [memoColumns]
  );

  const columnOriginalIdMap = useMemo(() => {
    const flatColumnsArray: CustomColumn[] = memoColumns
      .map(column => [column, ...getChildColumns(column)])
      .flat();

    return arrayToMap(flatColumnsArray);
  }, [memoColumns]);

  const hasAnyColumnFooter = useMemo(
    () => Object.values(columnOriginalIdMap).find(hasFooterRows),
    [columnOriginalIdMap]
  );

  const dataOptions = {
    columns: memoColumns,
    data,
    initialState: {
      pageIndex: 0,
      pageSize: 20,
      sortBy: defaultSorting || [],
    },
    paginateExpandedRows: false,
    getRowId: getUniqueKey,
  };

  const {
    canPreviousPage,
    canNextPage,
    getTableProps,
    getTableBodyProps,
    headerGroups,
    footerGroups,
    prepareRow,
    page,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: { pageIndex, pageSize },
  } = useTable(dataOptions, useSortBy, useExpanded, usePagination);

  const anyColumnHasHeaderSub = useMemo(
    () => columns.filter(hasHeaderSub).length > 0,
    [columns]
  );

  return (
    <>
      <TableContainer
        style={{
          maxHeight,
        }}
      >
        <MuiTable {...getTableProps()} size={size} stickyHeader={stickyHeader}>
          <TableHead>
            {headerGroups.map(headerGroup => (
              <MuiTableRow
                {...headerGroup.getHeaderGroupProps()}
                key={headerGroup.Header as string}
              >
                {headerGroup.headers.map((column) => {
                  const { sortable = true } =
                    columnOriginalIdMap[column.id] || {};
                  const {
                    canSort,
                    isSorted,
                    isSortedDesc,
                    columns: subColumns,
                    placeholderOf,
                    fixed,
                    HeaderSub = ' ',
                  } = column as CustomHeaderGroups;

                  // @ts-ignore TODO: fix me so that the onclick is found
                  const { onClick } = column.getSortByToggleProps();
                  let cellContents = column.render('Header');
                  const isFirstColumnInGroup = isFirstColumnOfParentGroup(
                    column
                  );

                  if (subColumns || placeholderOf) {
                    return (
                      <StyledTableParentHeaderCell
                        {...column.getHeaderProps()}
                        key={column.id}
                        alignCenter={subColumns && subColumns.length > 1}
                        isFirstColumnInGroup={isParentColumn(column)}
                      >
                        {cellContents}
                      </StyledTableParentHeaderCell>
                    );
                  }

                  if (canSort && sortable) {
                    let direction;

                    if (isSorted) {
                      direction = isSortedDesc
                        ? SORTING_DIRECTION.DESC
                        : SORTING_DIRECTION.ASC;
                    }

                    cellContents = (
                      <TableSortLabel
                        active={isSorted}
                        direction={direction}
                        onClick={onClick}
                      >
                        {cellContents}
                      </TableSortLabel>
                    );
                  }

                  if (anyColumnHasHeaderSub) {
                    cellContents = (
                      <>
                        {cellContents}
                        <HeaderSubContainer>{HeaderSub}</HeaderSubContainer>
                      </>
                    );
                  }

                  if (fixed) {
                    return (
                      <StyledFixedTableHeaderCell
                        {...column.getHeaderProps()}
                        key={column.id}
                        isFirstColumnInGroup={isFirstColumnInGroup}
                        topPosition={stickyHeader && hasAnyColumnSubColumns}
                      >
                        {cellContents}
                      </StyledFixedTableHeaderCell>
                    );
                  }

                  return (
                    <StyledTableHeaderCell
                      {...column.getHeaderProps()}
                      key={column.id}
                      isFirstColumnInGroup={isFirstColumnInGroup}
                      topPosition={stickyHeader && hasAnyColumnSubColumns}
                    >
                      {cellContents}
                    </StyledTableHeaderCell>
                  );
                })}
              </MuiTableRow>
            ))}
          </TableHead>
          <TableBody {...getTableBodyProps()}>
            {/* just get the lowest level headers to only have one row here [slice(0, 1)] */}
            {hasAnyColumnFooter &&
              footerGroups.slice(0, 1).map(group => (
                <TableRow {...group.getFooterGroupProps()}>
                  {group.headers.map((column: CustomHeaderGroups) => {
                    if (column.fixed) {
                      return (
                        <StyledFixedTableHeaderRowCell
                          {...column.getFooterProps()}
                          isFirstColumnInGroup={isFirstColumnOfParentGroup(
                            column
                          )}
                        >
                          {column.render('Footer') || null}
                        </StyledFixedTableHeaderRowCell>
                      );
                    }
                    return (
                      <StyledTableHeaderRowCell
                        {...column.getFooterProps()}
                        isFirstColumnInGroup={isFirstColumnOfParentGroup(
                          column
                        )}
                      >
                        {column.render('Footer') || null}
                      </StyledTableHeaderRowCell>
                    );
                  })}
                </TableRow>
              ))}
            {page.map((row) => {
              prepareRow(row);
              const rowKey = row.id;
              const isChild = row.depth && row.depth > 0;

              return (
                <TableRow
                  {...row.getRowProps()}
                  key={rowKey}
                  selected={
                    selectedRowId !== undefined && selectedRowId === rowKey
                  }
                  onClick={() => onSelectRow(row.original, rowKey)}
                  onMouseEnter={() =>
                    onRowHoverEnter && onRowHoverEnter(rowKey)
                  }
                  onMouseLeave={() =>
                    onRowHoverLeave && onRowHoverLeave(rowKey)
                  }
                >
                  {row.cells.map((cell, index) => {
                    const currentCellColumn = cell.column as CustomColumnInstance;
                    const cellId = currentCellColumn.id;

                    const { tooltip = false } =
                      columnOriginalIdMap[cellId] || {};
                    const isFirstDataColumn = index === 1;
                    const isExpander = cellId === EXPANDER_ID;
                    let CellComponent = isChild
                      ? StyledChildTableCell
                      : StyledTableCell;
                    const isFirstColumnInGroup = isFirstColumnOfParentGroup(
                      currentCellColumn
                    );

                    if (isExpander) {
                      if (isChild) {
                        CellComponent = FirstChildStyledTableCell;
                      } else {
                        CellComponent = FirstStyledTableCell;
                      }
                    }
                    if (
                      isChild &&
                      columnOriginalIdMap[cellId] &&
                      columnOriginalIdMap[cellId].fixed
                    ) {
                      CellComponent = StyledFixedChildTableCell;
                    }
                    if (currentCellColumn.fixed && !isChild) {
                      CellComponent = StyledFixedTableCell;
                    }

                    const cellContent = (
                      <StyledColumn maxColumnWidth={maxColumnWidth}>
                        {cell.render('Cell')}
                      </StyledColumn>
                    );

                    return (
                      <CellComponent
                        {...cell.getCellProps()}
                        key={cellId}
                        depth={isFirstDataColumn ? row.depth : 0}
                        isFirstColumnInGroup={isFirstColumnInGroup}
                      >
                        <TooltipFormatter
                          tooltip={tooltip}
                          cellContent={cellContent}
                          cell={cell}
                        />
                      </CellComponent>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </MuiTable>
      </TableContainer>
      <Flex justify="space-between" align="center">
        <Flex>
          {tableActions?.map(({ disabled, onClick, label }) => (
            <PrimaryButton onClick={() => onClick()} disabled={disabled}>
              {label}
            </PrimaryButton>
          ))}
        </Flex>
        {data.length >= 20 && (
          <CustomTablePagination
            goToPage={gotoPage}
            canPreviousPage={canPreviousPage}
            canNextPage={canNextPage}
            previousPage={previousPage}
            nextPage={nextPage}
            pageCount={pageCount}
            pageIndex={pageIndex}
            pageSize={pageSize}
            pageOptions={pageOptions}
            setPageSize={setPageSize}
          />
        )}
      </Flex>
    </>
  );
};

Table.defaultProps = {
  defaultSorting: undefined,
  expandable: false,
  getUniqueKey: defaultGetUniqueKey,
  maxHeight: undefined,
  onSelectRow: defaultOnSelectRow,
  selectedRowId: undefined,
  stickyHeader: false,
  tableActions: [],
};
