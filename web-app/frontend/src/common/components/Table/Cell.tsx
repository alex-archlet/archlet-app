import styled, { css } from 'styled-components';
import {
  TableCell,
} from '@material-ui/core';

export const HeaderSubContainer = styled.div`
  font-size: 11px;
  font-weight: normal;
  line-height: 20px;
  height: 20px;
`;

interface HeaderProps {
  isFirstColumnInGroup: boolean,
  width?: number,
  topPosition?: boolean,
  isChild?: boolean,
  depth?: number,
}

export const StyledTableHeaderCell = styled(TableCell)`
  background-color: #fff !important;
  padding: 8 16px !important;
  ${(props: HeaderProps) => (props.width ? `width: ${props.width}px;` : '')}
  ${(props: HeaderProps) => (props.topPosition ? 'top: 23px !important;' : '')}
  ${(props: HeaderProps) => (props.isFirstColumnInGroup ? 'border-left: 2px solid rgba(224,224,224,1);' : '')}
`;

export const StyledFixedTableHeaderCell = styled(StyledTableHeaderCell)`
  position: sticky !important;
  left: 0;
  top: 0;
  z-index: 5 !important;
  background-color: white;
`;

interface ParentHeaderProps {
  alignCenter: boolean,
}

export const StyledTableParentHeaderCell = styled(StyledTableHeaderCell)`
  padding: 0 16px !important;
  border-bottom: none !important;
  ${(props: ParentHeaderProps) => (props.alignCenter ? 'text-align: center !important;' : '')}
`;

export const StyledFixedTableParentHeaderCell = styled(StyledTableParentHeaderCell)`
  position: sticky !important;
  left: 0;
  top: 0;
  z-index: 5 !important;
  background-color: white;
`;



export const StyledTableCell = styled(TableCell)`
  border-bottom: none !important;
  border-top: 1px solid rgba(224, 224, 224, 1);
  ${(props: HeaderProps) => (props.width ? `width: ${props.width}px;` : '')}
  ${(props: HeaderProps) => (props.isFirstColumnInGroup ? 'border-left: 2px solid rgba(224,224,224,1);' : '')}
`;

export const StyledFixedTableCell = styled(StyledTableCell)`
  position: sticky !important;
  left: 0;
  top: 0;
  z-index: 1;
  background-color: white;
`

export const StyledTableHeaderRowCell = styled(StyledTableCell)`
  font-weight: bold !important;
  font-style: italic;
  background-color: rgba(224, 224, 224, 1);
`;

export const StyledFixedTableHeaderRowCell = styled(StyledTableHeaderRowCell)`
  position: sticky !important;
  left: 0;
  top: 0;
  z-index: 1;
`

interface StyledColumnProps {
  maxColumnWidth?: number,
}

export const StyledColumn = styled.div`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  ${({ maxColumnWidth }: StyledColumnProps) => (maxColumnWidth ? `max-width: ${maxColumnWidth}px;` : '')}
`;

const CHILD_ROW_PADDING = 16;

const calcLeftPadding = (depth: number) => depth * CHILD_ROW_PADDING + 16;

const getPaddingString = (depth: number) => css`
  padding: 8px 16px 8px ${calcLeftPadding(depth)}px !important;
`;

interface StyledDepthProps {
  depth: number,
}

export const StyledChildTableCell = styled(StyledTableCell)`
  border-bottom: none !important;
  border-top: none !important;
  color: #888 !important;
  font-size: 0.75rem !important;
  ${({ depth }: StyledDepthProps) => getPaddingString(depth)}}
`;

export const FirstStyledTableCell = styled(StyledTableCell)`
  width: 50px;
`;

export const FirstChildStyledTableCell = styled(StyledChildTableCell)`
  width: 50px;
`;
export const StyledFixedChildTableCell = styled(StyledChildTableCell)`
  position: sticky !important;
  left: 0;
  top: 0;
  z-index: 1;
  background-color: white;
`;
