import React from 'react';
import styled from 'styled-components';
import { Cell } from 'react-table';
import {
  Tooltip,
} from '@material-ui/core';

import { TooltipType } from './Table';

const StyledTooltip = styled(props => (
  <Tooltip
    classes={{
      popper: props.className,
      tooltip: 'tooltip',
    }}
    {...props}
  />
))`
  & .tooltip {
    font-size: 16px;
  }
`;
interface Props {
  cell: Cell,
  tooltip: TooltipType | undefined,
  cellContent: any
}

export const TooltipFormatter: React.FC<Props> = ({
  tooltip,
  cellContent,
  cell,
}) => {
  if (!tooltip) {
    return cellContent;
  }

  if (typeof tooltip === 'function') {
    return (
      <StyledTooltip title={cell.render('tooltip') || ''}>
        {cellContent}
      </StyledTooltip>
    );
  }

  return (
    <StyledTooltip title={cell.value || ''}>
      {cellContent}
    </StyledTooltip>
  );
};
