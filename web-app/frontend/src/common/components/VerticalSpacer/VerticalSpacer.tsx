import styled from 'styled-components';
import { Box } from '@material-ui/core';

interface Props {
  height: number,
}

export const VerticalSpacer = styled(Box)`
  width: 100%;
  height: ${(props: Props) => props.height}px;
`;
