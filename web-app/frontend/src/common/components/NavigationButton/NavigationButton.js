import React from 'react';
import { useHistory } from 'react-router';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import {
  ArrowBack,
  Close,
} from '@material-ui/icons';

const StyledNavigationButton = styled.div`
  position: relative;
  top: 3px;
  width: 24px;
  margin-right: 30px;
  cursor: ${props => props.cursorType || 'default'};
`;

export const NavigationButton = ({
  iconType,
  destinationUrl,
}) => {
  const history = useHistory();

  const getButtonType = (type) => {
    if (type === 'arrow') return <ArrowBack />;

    if (type === 'close') return <Close />;

    return null;
  };

  const onClick = (type) => {
    if (!type) {
      return null;
    }
    
    if (typeof destinationUrl === 'function') {
      return destinationUrl();
    }

    if (destinationUrl) {
      return history.push(destinationUrl);
    }

    if (type === 'arrow') {
      return history.push('/');
    }

    return history.goBack();
  };

  return (
    <StyledNavigationButton
      onClick={() => onClick(iconType)}
      cursorType={iconType ? 'pointer' : 'default'}
    >
      { getButtonType(iconType) }
    </StyledNavigationButton>
  );
};

NavigationButton.propTypes = ({
  destinationUrl: PropTypes.string.isRequired,
  history: PropTypes.shape({}),
  iconType: PropTypes.string.isRequired,
});

NavigationButton.defaultProps = {
  history: {},
};
