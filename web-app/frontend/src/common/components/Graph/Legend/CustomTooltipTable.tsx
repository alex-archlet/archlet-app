import React from 'react';
import styled from 'styled-components';
import { Box } from '@material-ui/core';
import { useCustomTranslation } from '@utils/translate';
import {
  ColorShape,
  ShapeOption,
} from '@common/components/ColorShape/ColorShape';

const StyledTable = styled.table`
  border-spacing: 0px;
  margin-top: 4px;
`;

const ValueCell = styled.div`
  color: #44566c;
  font-size: 9px;
  line-height: 16px;
  font-weight: 500;
  white-space: nowrap;
  text-align: left;
  font-size: 14px;
`;

const LabelCell = styled(ValueCell)`
  margin-right: 4px;
  color: #44566c;
  opacity: 0.75;
  font-size: 12px;
  line-height: 11px;
  white-space: pre-line;
`;

export interface TableItem {
  label: string;
  value: string | number | React.ReactNode;
  shape?: ShapeOption;
  color?: string;
}

interface Props {
  items: TableItem[];
}

export const CustomTooltipTable: React.FC<Props> = ({ items }) => {
  const { t } = useCustomTranslation();
  
  return (
    <StyledTable>
      <tbody>
        {items.map(({ label, value, shape, color }) => (
          <tr key={label}>
            <td>
              <Box display="flex" alignItems="center" mb={10}>
                {color && <ColorShape shape={shape} color={color} />}
                <LabelCell>{t(label)}:</LabelCell>
              </Box>
            </td>
            <td>
              <Box display="flex" alignItems="center" mb={10}>
                <ValueCell>{value}</ValueCell>
              </Box>
            </td>
          </tr>
        ))}
      </tbody>
    </StyledTable>
  );
};
