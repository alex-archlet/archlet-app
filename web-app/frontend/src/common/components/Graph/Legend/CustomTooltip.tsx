import React from 'react';
import styled from 'styled-components';
import { Box } from '@material-ui/core';
import { Text } from '@common/components/Text/Text';
import { CustomTooltipTable, TableItem } from './CustomTooltipTable';

const StyledContainer = styled.div`
  padding: 8px 16px;
  background-color: #ffffff;
  border-radius: 5px;
  opacity: 1;
  box-shadow: 0 2px 10px 0 rgba(10, 22, 70, 0.1);
  max-width: 400px;
  font-size: 20px;
`;

interface Props {
  items: TableItem[];
  title?: string;
}

export const CustomTooltip: React.FC<Props> = ({ title, items }) => (
  <StyledContainer>
    <Box display="flex" alignItems="center" mb={10}>
      {title && (
        <Text color="#44566C" fontSize={16} lineHeight={13} fontWeight={500}>
          {title}
        </Text>
      )}
    </Box>
    {items?.length > 0 ? <CustomTooltipTable items={items} /> : null}
  </StyledContainer>
);

CustomTooltip.defaultProps = {
  items: [],
};
