import React from 'react';

const checkCurrentDotFillColour = (isSelected: boolean, scale: number) => {
  if (isSelected) {
    return [`rgba(50,150,237, ${scale})`, '#3296ED'];
  }

  return ['rgba(109,114,120,0.1)', '#D7DADD'];
};

interface Props {
  isSelected: boolean;
  shapeItem: {
    scale: number;
    cx: number;
    cy: number;
    normalizationNumberOfSuppliers: number;
  };
}

export const CustomDot: React.FC<Props> = ({
  shapeItem,
  isSelected,
}) => {
  const [fillColor, strokeColor] =
  checkCurrentDotFillColour(isSelected, shapeItem.scale);

  return (
    <circle
      cx={shapeItem.cx}
      cy={shapeItem.cy}
      r={shapeItem.normalizationNumberOfSuppliers * 20}
      stroke={strokeColor}
      strokeWidth={1}
      fill={fillColor}
    />
  );
};
