import React from 'react';
import styled from 'styled-components';

import { truncate } from '@utils/stringFormat';

type StyledProps = {
  alignment?: string
}

const StyledUnorderedList = styled.ul<StyledProps>`
  padding: 0px;
  margin: 0px;
  text-align: ${props => (props.alignment === 'center' ? 'center' : 'right')};
  width: ${props => (props.alignment === 'center' ? '110%' : null)};
  display: ${props => (props.alignment === 'center' ? 'flex' : null)};
  flex-direction: ${props => (props.alignment === 'center' ? 'row' : null)};
  flex-wrap: ${props => (props.alignment === 'center' ? 'wrap' : null)};
`;

const StyledListItem = styled.li`
  display: inline-block;
  margin-right: 10px;
`;

const StyledSvg = styled.svg`
  display: inline-block;
  vertical-align: middle;
  margin-right: 4px;
`;

const StyledSpan = styled.span`
  margin-left: 6px;
  margin-right: 12px;
`;

interface Payload {
  value: string;
  color: string;
  type: string;
}

interface Props {
  alignment: string;
  payload: Payload[];
}

export const CustomLegend: React.FC<Props> = ({ payload, alignment }) => (
  <StyledUnorderedList
    className="recharts-default-legend"
    alignment={alignment}
  >
    {payload.map(item => (
      <StyledListItem key={item.value} className="recharts-legend-item">
        <StyledSvg
          className="recharts-surface"
          width="12"
          height="12"
          viewBox="0 0 32 32"
          version="1.1"
        >
          <path
            fill={item.color}
            cx="16"
            cy="16"
            type={item.type}
            className="recharts-symbols"
            transform="translate(16, 16)"
            d="M16,0A16,16,0,1,1,-16,0A16,16,0,1,1,16,0"
          />
        </StyledSvg>
        <StyledSpan className="recharts-legend-item-text">
          {truncate(item.value)}
        </StyledSpan>
      </StyledListItem>
    ))}
  </StyledUnorderedList>
);
