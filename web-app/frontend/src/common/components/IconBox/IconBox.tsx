import React from 'react';
import styled from 'styled-components';
import {
  Avatar,
  Box,
} from '@material-ui/core';
import { Text } from '@common/components/Text/Text';

const StyledAvatar = styled(Avatar)`
  && {
    margin-right: 15px;
    background-color: #e3e3e3;

    .MuiSvgIcon-root {
      fill: #686868;
    }
  }
`;
interface Props {
  text?: string;
  children: React.ReactNode;
}

export const IconBox: React.FC<Props> = ({
  children,
  text,
}) => (
  <Box
    display="flex"
    alignItems="center"
    mb={15}
  >
    <StyledAvatar>
      { children }
    </StyledAvatar>
    {text && <Text
      fontSize={16}
      fontWeight={400}
    >
      { text }
    </Text>
    }
  </Box>
);

