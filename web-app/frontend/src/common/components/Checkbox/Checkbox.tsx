import React from 'react';
import {
  FormControlLabel,
  Checkbox as MuiCheckbox,
  FormControl,
} from '@material-ui/core';

interface Props {
  onChange: (
    event: React.ChangeEvent<HTMLInputElement>,
    checked: boolean
  ) => void;
  value: boolean;
  formComponent?: boolean;
  label?: string;
}

export const Checkbox: React.FC<Props> = ({
  onChange,
  value,
  label,
  formComponent = true,
  ...rest
}) => (
  <>
    {formComponent ? (
      <FormControl component="fieldset">
        <FormControlLabel
          control={
            <MuiCheckbox checked={!!value} onChange={onChange} {...rest} />
          }
          label={label}
        />
      </FormControl>
    ) : (
      <MuiCheckbox checked={!!value} onChange={onChange} {...rest} />
    )}
  </>
);
