import { TextField } from '@material-ui/core';
import { Autocomplete as MuiAutocomplete, AutocompleteChangeDetails, AutocompleteChangeReason } from '@material-ui/lab';
import { StyledChip } from '@routes/Bid/common/CostCalculation/InputChip';
import React, { ChangeEvent } from 'react';
import { MultiFilterValue, SingleFilterValue } from '../Filter/types';

export interface Props {
  options: SingleFilterValue[];
  value?: MultiFilterValue;
  placeholder: string;
  onChange: (event: ChangeEvent<{}>, value: (SingleFilterValue)[], reason: AutocompleteChangeReason, 
  details?: AutocompleteChangeDetails<SingleFilterValue> | undefined)
   => void
}

export const Autocomplete: React.FC<Props> = ({
  options,
  value,
  placeholder,
  onChange,
}) => {
  return (
    <MuiAutocomplete
      autoSelect
      multiple
      freeSolo
      disableClearable
      options={options}
      getOptionSelected={() => false}
      getOptionLabel={(option: SingleFilterValue) => option.label as string}
      renderTags={(val, getTagProps) =>
        val.map((option, index) => {
          return (
            <StyledChip
              // eslint-disable-next-line react/no-array-index-key
              {...getTagProps({ index })}
              label={option.label}
            />
          );
        })
      }
      disablePortal
      renderInput={params => (
        <TextField {...params} variant="outlined" placeholder={placeholder} />
      )}
      value={value || []}
      // @ts-ignore
      onChange={onChange}
    />
  );
};
