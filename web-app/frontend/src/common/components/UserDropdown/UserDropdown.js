import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import {
  Box,
  ClickAwayListener,
  Paper,
} from '@material-ui/core';
import { SecondaryButton } from '@common/components/Button/SecondaryButton';
import { connect } from 'react-redux';
import { userSelectors } from '@selectors/user';
import {
  KeyboardArrowDown,
  KeyboardArrowUp,
} from '@material-ui/icons';
import { useCustomTranslation } from '@utils/translate';
import { colors } from '@utils/uiTheme';
import { Text } from '../Text/Text';

const StyledUserDropdown = styled.div`
  position: relative;
  text-align: right;
  cursor: pointer;
`;

const StyledPaper = styled(Paper)`
  position: absolute;
  top: 43px;
  right: 0;
  z-index: 10;
  min-width: 200px;
  padding: 13px 15px;
  font-size: 14px;
  line-height: 25px;
  text-align: left;
`;

const StyledArrow = styled.div`
  margin: 5px 6px 0 0;
`;

const ButtonContainer = styled.div`
  margin-top: 16px;
`;

const StyledLink = styled(Link)`
  display: block;
  color: ${colors.palette.secondary.main};
  font-size: 12.5px;
  text-decoration: none;
`;

const UserDropdownComponent = ({
  onSignOut,
  isAdmin,
  user,
  currentLocation,
}) => {
  const [
    anchorEl,
    setAnchorEl,
  ] = React.useState(null);
  const { t } = useCustomTranslation();

  function handleClick(event) {
    setAnchorEl(event.currentTarget);
  }

  function handleClose() {
    setAnchorEl(null);
  }

  return (
    <StyledUserDropdown>
      <ClickAwayListener onClickAway={handleClose}>
        <div>
          <Box
            display="flex"
            justifyContent="flex-end"
            alignItems="center"
            onClick={anchorEl ? handleClose : handleClick}
          >
            <StyledArrow>{ anchorEl ? <KeyboardArrowUp /> : <KeyboardArrowDown /> }</StyledArrow>
            <Text
              color={colors.white}
              fontWeight="500"
            >
              {`${user.firstName} ${user.lastName}`}
            </Text>
          </Box>
          <Box>
            { anchorEl ?
              (
                <StyledPaper>
                  <div>{ `${user.firstName} ${user.lastName}` }</div>
                  <Text>{ user.email }</Text>
                  <StyledLink
                    to={{
                      pathname: '/user-settings',
                      state: { referrer: currentLocation },
                    }}
                    onClick={handleClose}
                  >
                    {t('User Settings')}
                  </StyledLink>
                  {isAdmin && (
                  <StyledLink
                    to={{
                      pathname: '/admin',
                      state: { referrer: currentLocation },
                    }}
                    onClick={handleClose}
                  >
                    Admin
                  </StyledLink>
                  )}
                  <ButtonContainer>
                    <SecondaryButton
                      onClick={onSignOut}
                    >
                      {t('Sign out')}
                    </SecondaryButton>
                  </ButtonContainer>
                </StyledPaper>
              ) : null}
          </Box>
        </div>
      </ClickAwayListener>
    </StyledUserDropdown>
  );
};

UserDropdownComponent.propTypes = {
  currentLocation: PropTypes.string.isRequired,
  isAdmin: PropTypes.bool.isRequired,
  onSignOut: PropTypes.func.isRequired,
  user: PropTypes.shape({
    email: PropTypes.string,
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    role: PropTypes.string,
  }).isRequired,
};

const mapStateToProps = state => ({
  isAdmin: userSelectors.isAdmin(state),
});

export const UserDropdown = connect(mapStateToProps)(UserDropdownComponent);
