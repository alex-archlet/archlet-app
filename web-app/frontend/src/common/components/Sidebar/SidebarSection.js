import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import {
  Box,
  Typography,
} from '@material-ui/core';

const StyledBox = styled(Box)`
  width: 100%;
  margin: 0 0 41px;
`;

const StyledTitle = styled(Typography)`
  && {
    margin: 0 0 16px 50px;
    color: #2C2C2C;
    font-weight: 400;
    font-size: 14px;
    line-height: 21px;
    text-transform: uppercase;
  }
`;

export const SidebarSection = ({
  children,
  title,
}) => (
  <StyledBox>
    <StyledTitle variant="h2">{title}</StyledTitle>
    {children}
  </StyledBox>
);

SidebarSection.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]).isRequired,
  title: PropTypes.string.isRequired,
};
