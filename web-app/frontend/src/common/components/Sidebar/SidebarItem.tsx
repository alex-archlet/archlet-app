import React from 'react';
import styled from 'styled-components';
import { Scrollbars } from 'react-custom-scrollbars';
import { Link, matchPath, useLocation, LinkProps } from 'react-router-dom';
import { Box, Paper } from '@material-ui/core';
import { Tooltip } from '@common/components/Tooltip/Tooltip';

import { colors } from '@utils/uiTheme';

const StyledPaper = styled(Paper)`
  position: absolute;
  top: 0;
  right: 0;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 285px;
  height: calc(100vh - 64px);
  border-left: 1px solid rgba(0, 0, 0, 0.12);
`;

interface CustomLinkProps extends LinkProps {
  isActive?: boolean;
}
// @ts-ignore
const StyledLink: React.FC<CustomLinkProps> = styled(
  ({ isActive: _isActive, ...props }) => (
    // @ts-ignore
    <Link {...props} />
  )
)`
  display: flex;
  align-items: center;
  width: 100%;
  margin: 8px 0;
  border-left: ${(props: CustomLinkProps) =>
    props.isActive ? '9px solid #6bcae5' : 'none'};
  padding: ${(props: CustomLinkProps) =>
    props.isActive ? '11px 25px 11px 16px' : '11px 25px'};
  color: ${(props: CustomLinkProps) =>
    props.isActive ? colors.black : colors.darkGrey};
  text-decoration: none;

  svg {
    margin-right: 20px;
  }

  &:hover {
    color: ${colors.black};
  }
`;
// @ts-ignore
const StyledSecondaryLink: React.FC<CustomLinkProps> = styled(
  ({ isActive: _isActive, ...props }) => (
    // @ts-ignore
    <Link {...props} />
  )
)`
  display: flex;
  align-items: center;
  width: 241px;
  margin: 8px 0;
  border-top-right-radius: 30px;
  border-bottom-right-radius: 30px;
  padding: 11px 25px 11px 49px;
  background: ${(props: CustomLinkProps) =>
    props.isActive ? '#6bcae5' : 'none'};
  color: ${(props: CustomLinkProps) =>
    props.isActive ? colors.white : colors.black};
  text-decoration: none;

  svg {
    margin-right: 20px;
  }

  path {
    fill-opacity: 1;
  }

  &:hover {
    color: ${props =>
      props.isActive ? 'rgba(255, 255, 255, 0.6)' : 'rgba(0, 0, 0, 0.6)'};
  }
`;

const StyledScrollbars = styled(Scrollbars)`
  width: 280px;
  height: 100%;

  .scrollbar-view {
    padding: 131px 0 55px 0;
  }
`;

interface Props {
  icon: JSX.Element;
  name: string;
  to: string;
  onClick?: (e: any) => void;
  isTooltip?: boolean;
  isSecondary?: boolean;
  isActive?: boolean;
  sideIcon?: JSX.Element;
  sideIconTooltip?: string;
}

export const SidebarItem: React.FC<Props> = ({
  icon,
  name,
  to,
  children,
  onClick = undefined,
  isSecondary = false,
  isTooltip = false,
  sideIcon,
  sideIconTooltip,
}) => {

  const location = useLocation();

  const isActive = Boolean(matchPath(location.pathname, to));

  return (
    <>
      {isSecondary ? (
        <StyledSecondaryLink
          to={{ pathname: to, state: { prevPath: location.pathname } }}
          isActive={isActive}
          onClick={onClick}
        >
          {icon}
          <span>{name}</span>
        </StyledSecondaryLink>
      ) : (
        <StyledLink
          to={{ pathname: to, state: { prevPath: location.pathname } }}
          isActive={isActive}
          onClick={onClick}
        >
          <Tooltip
            title={name}
            placement="left-start"
            arrow
            disableHoverListener={!isTooltip}
          >
            {icon}
          </Tooltip>
          <span>{name}</span>
          {sideIcon && sideIconTooltip && (
            <Tooltip title={sideIconTooltip}>
              <Box ml={5} mb={2}>
                {sideIcon}
              </Box>
            </Tooltip>
          )}
        </StyledLink>
      )}
      {!!children && isActive && (
        <StyledPaper elevation={0} variant="outlined" square>
          <StyledScrollbars
            autoHide
            renderView={({ ...props }) => (
              <div {...props} className="scrollbar-view" />
            )}
          >
            {children}
          </StyledScrollbars>
        </StyledPaper>
      )}
    </>
  );
};
