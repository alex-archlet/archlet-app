import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';
import { Button } from '@common/components/Button/Button';

const StyledButton = styled(Button)`
  &&.MuiButton-root {
    display: flex;
    justify-content: flex-start;
    align-items: center;
    width: 138px;
    margin: 0 0 10px 50px;
    color: rgba(44, 44, 44, 0.7);
    font-weight: 500;
    font-size: 13px;
    line-height: 1;
    cursor: pointer;
  }

  &&.MuiButton-outlined {
    border: 2px solid #6bcae5;
    border-radius: 9px;
    padding: 7px 10px;
  }

  svg {
    margin-right: 8px;
    font-size: 17px;
  }

  &:hover {
    color: rgba(44, 44, 44, 1);
  }
`;

const SidebarActionButtonComponent = ({ icon, name, onClick }) => (
  <StyledButton
    onClick={onClick}
    variant="outlined"
    color="secondary"
    component="button"
  >
    {icon}
    <span>{name}</span>
  </StyledButton>
);

SidebarActionButtonComponent.propTypes = {
  icon: PropTypes.shape({}).isRequired,
  name: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

export const SidebarActionButton = withRouter(SidebarActionButtonComponent);
