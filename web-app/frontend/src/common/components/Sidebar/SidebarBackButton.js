import React from 'react';
import { useHistory } from 'react-router';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Box } from '@material-ui/core';
import { ArrowBack } from '@material-ui/icons';

const StyledSidebarBackButton = styled.div`
  position: absolute;
  top: 62px;
  left: 47px;
  display: flex;
  align-items: center;
  cursor: pointer;
`;

const StyledBox = styled(Box)`
  width: 29px;
  height: 29px;
  margin-right: 11px;
  border: 2px solid #6bcae5;
  border-radius: 7px;
  font-size: 18px;
  line-height: 30px;
  text-align: center;
`;

export const SidebarBackButton = ({
  toPath,
  toName,
}) => {
  const history = useHistory();

  const onClick = (path) => {
    if (path) {
      return history.push(path);
    }

    return history.goBack();
  };

  return (
    <StyledSidebarBackButton
      onClick={() => onClick(toPath)}
    >
      <StyledBox>
        <ArrowBack fontSize="inherit" />
      </StyledBox>
      Back
      {!!toName && (
        <span>
          &nbsp;to&nbsp;
          <strong>{toName}</strong>
        </span>
      )}
    </StyledSidebarBackButton>
  );
};

SidebarBackButton.propTypes = ({
  history: PropTypes.shape({}),
  toName: PropTypes.string,
  toPath: PropTypes.string,
});

SidebarBackButton.defaultProps = {
  history: {},
  toName: '',
  toPath: '',
};
