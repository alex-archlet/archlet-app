import React from 'react';
import styled from 'styled-components';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import { colors, drawer, animation, dimensions } from '@utils/uiTheme';
import { useDispatch, useSelector } from 'react-redux';
import { sidebarSelectors } from '@store/selectors/sidebar';
import { toggleSidebar, openSidebar } from '@store/actions/sidebar';
import clsx from 'clsx';

const StyledDrawer = styled(({ isSecondary: _isSecondary, ...props }) => (
  <Drawer {...props} />
))`
  & > .MuiPaper-root {
    top: ${dimensions.headerHeight}px;
    overflow-y: inherit;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    height: calc(100vh - ${dimensions.headerHeight}px);
    text-align: left;
  }
  & .MuiDrawer-paper {
    overflow-y: inherit;
  }

  &.drawer {
    flex-shrink: 0;
    white-space: nowrap;
  }

  &.drawerOpen {
    & .MuiDrawer-paper {
      width: ${props => props.isSecondary ? `${drawer.widthSecondLevel}px` : `${drawer.width}px`};
      transition: width ${animation.duration.enteringScreen}ms ${animation.timingFunction.easeInOut} 0s;
    }
  }

  &.drawerClose {
    & .MuiDrawer-paper {
      width: ${drawer.widthOnlyIcons}px;
      transition: width ${animation.duration.leavingScreen}ms ${animation.timingFunction.easeOut} 0s;
    }
  }

`;

const Wrapper = styled.div`
  display: flex;
  height: calc(100vh - ${dimensions.headerHeight}px);
  flex-direction: column;
  transition: overflow ${animation.duration.enteringScreen}ms ${animation.timingFunction.sharp} 0.1s;
  overflow-x: inherit;
  &.closed {
    transition: overflow ${animation.duration.leavingScreen}ms ${animation.timingFunction.easeOut} 0s;
    overflow-x: hidden;
  }
`;

const StyledToolbar = styled.div`
  width: 100%;
  position: absolute;
`;

const StyledButton = styled(IconButton)`
  &&.MuiIconButton-root {
    width: auto;
    right: -13px;
    position: absolute;
    border: 1px solid ${colors.grey};
    padding: 0;
    top: 15px;
    background: ${colors.white}

    &:hover {
      background-color: ${colors.palette.secondary.main};
      color: ${colors.light};
      transition: 
        background-color ${animation.duration.enteringScreen}ms ${animation.timingFunction.sharp}, 
        color ${animation.duration.enteringScreen}ms ${animation.timingFunction.sharp};
    }

    &.close {
      transform: rotate(180deg);
      transition: transform ${animation.duration.enteringScreen}ms ${animation.timingFunction.sharp} 0.1s;
    }
  }
`

interface SidebarProps {
  children: React.ReactNode;
  isSecondary?: boolean;
}

export const Sidebar: React.FC<SidebarProps> = ({
  isSecondary = false, 
  children,
}) => {
  const dispatch = useDispatch();
  const isSidebarOpen = useSelector(sidebarSelectors.getSidebarSettings);

  // always keep opened sidebar, if second level from menu exists
  if (isSecondary) {
    dispatch(openSidebar());
  }

  return (
    <StyledDrawer
      variant="permanent"
      isSecondary={isSecondary}
      anchor="left"
      className={`drawer ${isSidebarOpen ? 'drawerOpen': 'drawerClose'}`}
    >
      {!isSecondary ? <StyledToolbar>
        <StyledButton
          aria-label="open drawer"
          edge="start"
          onClick={() => dispatch(toggleSidebar())}
          className={isSidebarOpen ? 'open': 'close'}
        >
          <ChevronLeftIcon />
        </StyledButton>
      </StyledToolbar> : null}

      <Wrapper 
        className={clsx({
          'closed': !isSidebarOpen,
        })}
      >  
        {children}
      </Wrapper>    
      
    </StyledDrawer>
  );
};
