import React from 'react';
import PropTypes from 'prop-types';
import { useDropzone } from 'react-dropzone';
import { CloudUpload, InsertDriveFile } from '@material-ui/icons';
import { Box } from '@material-ui/core';
import styled from 'styled-components';

import { useCustomTranslation } from '@utils/translate';

const StyledUploadField = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 115px;
  margin: 0 0 30px;
  border: 3px dashed #e4e4e4;
  border-radius: 13px;
  font-size: 15px;
  cursor: pointer;
`;

const StyledCloudUpload = styled(CloudUpload)`
  && {
    width: 36px;
    height: 36px;
    margin-right: 15px;
  }
`;

const StyledInsertDriveFile = styled(InsertDriveFile)`
  && {
    width: 36px;
    height: 36px;
    margin-right: 15px;
    fill: #3CBA54;
  }
`;

export const UploadField = ({
  onDrop,
  filename,
}) => {
  const { t } = useCustomTranslation();

  const onUpload = (acceptedFiles) => {
    const formData = new FormData();

    acceptedFiles.forEach((file) => {
      formData.append('files', file);
    });

    onDrop(formData);
  };

  const {
    getRootProps,
    getInputProps,
    isDragActive,
  } = useDropzone({ onDrop: onUpload });

  const fileUploadLabel = isDragActive ? t('Drop the files here ...') : t('Drag and drop files here or click');
  const successLabel = `${filename} - ${t('Select a different file?')}`;

  return (
    <StyledUploadField {...getRootProps()}>
      <input {...getInputProps()} />
      <Box
        display="flex"
        alignItems="center"
      >
        {!filename ? (
          <>
            <StyledCloudUpload />
            <p>
              {fileUploadLabel}
            </p>
          </>
        ) : (
          <>
            <StyledInsertDriveFile />
            <p>
              {successLabel}
            </p>
          </>
        )}
      </Box>
    </StyledUploadField>
  );
};

UploadField.propTypes = {
  filename: PropTypes.string, // is a file already uploaded
  onDrop: PropTypes.func.isRequired,
};

UploadField.defaultProps = {
  filename: null,
};
