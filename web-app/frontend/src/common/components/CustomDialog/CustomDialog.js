import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Box, Dialog, DialogActions } from '@material-ui/core';
import { Button } from '@common/components/Button/Button';
import { Text } from '@common/components/Text/Text';
import { useCustomTranslation } from '@utils/translate';

const StyledButton = styled(Button)`
  && {
    padding: 6px 21px;
    font-size: 13px;
  }
`;

const StyledDialog = styled(Dialog)`
  & .MuiPaper-root {
    width: 400px;
    padding: 30px 18px 15px 30px;
  }
`;

export const CustomDialog = ({ handleClose, isOpen, message, buttonText }) => {
  const { t } = useCustomTranslation();

  return (
    <StyledDialog
      open={isOpen}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <Text fontSize={14} fontWeight={400}>
        {message}
      </Text>
      <Box mt={10}>
        <DialogActions>
          <StyledButton onClick={handleClose} color="secondary">
            {t(buttonText)}
          </StyledButton>
        </DialogActions>
      </Box>
    </StyledDialog>
  );
};
CustomDialog.propTypes = {
  buttonText: PropTypes.string,
  handleClose: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  message: PropTypes.string.isRequired,
};

CustomDialog.defaultProps = {
  buttonText: 'Close',
};
