import React, { useMemo } from 'react';
import styled from 'styled-components';

import { useCustomTranslation } from '@utils/translate';

import { FilterDropdown, TYPE } from './FilterDropdown/FilterDropdown';
import { FilterButton } from './FilterButton/FilterButton';
import { checkValueIsTruthy } from './filterUtils';
import { FilterText } from './FilterText/FilterText';
import { FilterState, FilterDeclaration, SingleFilterValue, MultiFilterValue } from './types';

const StyledContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  flex-wrap: wrap;

  /* any direct child to have left margin */
  && > * {
    margin-left: 12px;
  }
  /* first direct child NOT to have left margin */
  && > *:first-child {
    margin-left: 0;
  }
`;

const DropDownContainer = styled.div`
  display: flex;
  align-items: center;
`;

const isClearable = (filter: FilterDeclaration) => filter.clearable === undefined || filter.clearable === true;

const isDefined = (val: any) => val !== undefined;

const hasClearableFilterValues = (values: FilterState, clearableFilters: FilterDeclaration[]) => {
  if (!values || Object.keys(values).length === 0) {
    return false;
  }

  const clearableFilterValues = clearableFilters
    .map(({ id }) => values[id])
    .filter(isDefined)
    .filter(checkValueIsTruthy);

  return clearableFilterValues.length > 0;
};

const isVisibleFilter = ({ options, type }: FilterDeclaration): boolean => (options && options.length > 1) || type === TYPE.MULTI;

interface Props {
  filters: FilterDeclaration[],
  onChange: (state: FilterState, filterId?: string, deleteValue?: boolean) => void,
  values: FilterState,
  alwaysShowFilters?: boolean,
  enableClearButton?: boolean,
  label?: string,
}

const EMPTY_ARRAY = [];

export const Filter: React.FC<Props> = ({
  filters,
  enableClearButton,
  onChange,
  label,
  values,
  alwaysShowFilters,
}) => {
  const { t } = useCustomTranslation();

  const clearableFilters = useMemo(() => filters.filter(isClearable), [filters]);
  const showClearButton = hasClearableFilterValues(values, clearableFilters) && enableClearButton;
  const visibleFilters = useMemo(() => (alwaysShowFilters ? filters : filters.filter(isVisibleFilter)), [filters, alwaysShowFilters]);

  const hasVisibleFilters = visibleFilters.length > 0;

  const onFilterChange = (filterId: string, newValue: SingleFilterValue | MultiFilterValue) => {
    const newValues = {
      ...values,
      [filterId]: newValue,
    };


    onChange(
      newValues,
      filterId
    );
  };
  const onFilterClear = () => {
    const newValues = { ...values };

    clearableFilters.forEach((filter) => {
      delete newValues[filter.id];
    });

    onChange(newValues);
  };

  if (!hasVisibleFilters) {
    return null;
  }

  return (
    <StyledContainer>
      {showClearButton && (
        <FilterButton onClick={onFilterClear}>
          {t('Clear Selection')}
        </FilterButton>
      )}

      {label && (
        <DropDownContainer>
          <FilterText text={label} />
        </DropDownContainer>
      )}

      {visibleFilters.map((filter) => {
        const identifer = filter.alias ? filter.alias : filter.id;

        return (
        <DropDownContainer>
          <FilterDropdown
            key={identifer}
            label={filter.label || ''}
            options={filter.options || EMPTY_ARRAY}
            text={filter.text}
            type={filter.type}
            values={values[identifer]}
            clearable={isClearable(filter)}
            onChange={newValue => onFilterChange(identifer, newValue)}
            />
        </DropDownContainer>
        )
          }
      )}
    </StyledContainer>
  );
};

Filter.defaultProps = {
  alwaysShowFilters: false,
  enableClearButton: false,
  label: undefined,
};
