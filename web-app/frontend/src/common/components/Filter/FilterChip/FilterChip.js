import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Chip from '@material-ui/core/Chip';
import CloseIcon from '@material-ui/icons/Close';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';

const StyledChip = styled(Chip)`
  background-color: ${props => (props.hasValue ? '#E7EDF3 !important' : 'rgba(255,255,255,0.75) !important;')}; 
  border-color: ${props => (props.hasValue ? '#E1E5E9 !important' : '#E1E5E9 !important')};
  color: ${props => (props.hasValue ? '#44566C !important' : '#2c2c2c !important')};
  font-size: 12px !important;
  border-radius: 5px !important;

  && .MuiChip-label {
    min-width: 100px;
  }
`;

const StyledCloseIcon = styled(CloseIcon)`
  color: #44566C !important;
`;

const StyledArrowDropDownIcon = styled(ArrowDropDownIcon)`
  color: ${props => (props.hasValue ? '#44566C !important' : 'inherit')};
`;

export const FilterChip = ({
  clearable,
  label,
  hasValue,
  onClick,
  onDelete,
}) => {
  const buttonRef = useRef(null);
  const handleDelete = (evt) => {
    evt.stopPropagation();
    evt.preventDefault();

    onDelete(evt, buttonRef);
  };

  const handleClick = (evt) => {
    evt.stopPropagation();
    evt.preventDefault();

    onClick(evt, buttonRef);
  };

  const isClearable = clearable && hasValue;
  const iconComponent = isClearable ? (
    <StyledCloseIcon
      onClick={handleDelete}
    />
  ) : (
    <StyledArrowDropDownIcon
      onClick={handleClick}
      hasValue={hasValue}
    />
  );

  return (
    <StyledChip
      ref={buttonRef}
      label={label}
      onClick={handleClick}
      onDelete={isClearable ? handleDelete : handleClick}
      variant="outlined"
      deleteIcon={iconComponent}
      hasValue={hasValue}
    />
  );
};

FilterChip.propTypes = {
  clearable: PropTypes.bool.isRequired,
  hasValue: PropTypes.bool.isRequired,
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
};
