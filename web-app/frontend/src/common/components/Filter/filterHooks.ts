import { useEffect, useMemo, useState } from 'react';
import { DataFrame, IDataFrame } from 'data-forge';
import { useSelector } from 'react-redux';

import { projectsSelectors } from '@selectors/projects';

import {
  checkValueIsTruthy,
  filterHelper,
  getInitialValues,
  getValueFromOption,
  isEmptyArray,
  stringToDropdownOption,
} from './filterUtils';
import { TYPE } from './FilterDropdown/FilterDropdown';
import {
  FilterDeclaration,
  FilterState,
  InitialValuesMap,
  SetFilterState,
  SingleFilterValue,
} from './types';
import { useReduxFilterState } from './useReduxFilterState';

const PROJECT_COST_COMPONENT = 'projectCostComponent';
const PROJECT_ITEM_TYPE = 'projectItemType';
const PROJECT_ITEMS = 'projectItems';
const PROJECT_VOLUME = 'projectVolume';
const PROJECT_SUPPLIERS = 'projectSuppliers';
const PROJECT_SUPPLIER_SINGLE = 'projectSupplier';

const FILTERS_SUPPLIER_KEY = 'supplier';

export const RESERVED_FILTER_IDS = {
  PROJECT_COST_COMPONENT,
  PROJECT_ITEM_TYPE,
  PROJECT_ITEMS,
  PROJECT_SUPPLIERS,
  PROJECT_SUPPLIER_SINGLE,
  PROJECT_VOLUME,
};

export const useInitialValues = (
  filterDeclaration: FilterDeclaration[],
  filterState: FilterState,
  setFilterState: SetFilterState,
  optionsMap: InitialValuesMap
) =>
  useEffect(() => {
    const { changed, newValue } = getInitialValues(
      filterDeclaration,
      filterState,
      optionsMap
    );

    if (changed) {
      setFilterState({
        ...filterState,
        ...newValue,
      });
    }
  }, [optionsMap, filterState, setFilterState, filterDeclaration]);

const INITIAL_STATE = {};
const INITIAL_VALUES_MAP = {};
const FILTER_COMBINATIONS = [];

interface UseFilterParams {
  filterDeclaration: FilterDeclaration[];
  initialState?: FilterState;
  initialValuesMap?: InitialValuesMap;
  reduxFilterId?: string;
}

export const useFilter = ({
  initialState = INITIAL_STATE,
  initialValuesMap = INITIAL_VALUES_MAP,
  filterDeclaration,
  reduxFilterId = '',
}: UseFilterParams) => {
  const [localFilterState, setLocalFilterState] = useState(initialState);
  const [reduxFilterState, setReduxFilterState] = useReduxFilterState({
    initialState,
    uniqueId: reduxFilterId,
  });

  const filterState = reduxFilterId ? reduxFilterState : localFilterState;
  const setFilterState = reduxFilterId
    ? setReduxFilterState
    : setLocalFilterState;

  useInitialValues(
    filterDeclaration,
    filterState,
    setFilterState,
    initialValuesMap
  );

  const { filterFunction, filterProps } = useMemo(
    () => filterHelper(filterDeclaration, filterState),
    [filterDeclaration, filterState]
  );

  return {
    filterFunction,
    filterProps,
    filterState,
    setFilterState,
  };
};

type QueryCallback = (row) => boolean;

const getQueryObjectsForFilter = (
  filterState: FilterState,
  filterId: string,
  filtersWithoutOptions: FilterDeclaration[]
) => {
  const queryObjects: QueryCallback[] = [];
  // we dont want to query the column itself, so we exclude it here
  const filtersToQuery = filtersWithoutOptions.filter(
    ({ id }) => id !== filterId
  );

  filtersToQuery.forEach(({ id }) => {
    const value = filterState[id];

    if (checkValueIsTruthy(value)) {
      let values: any[] = [];
      if (Array.isArray(value)) {
        values = value.map(getValueFromOption);
      } else {
        values = getValueFromOption(value);
      }
      if (values.length) {
        queryObjects.push(row => values.includes(row[id]));
      }
    }
  });

  // TODO: see if there is a better way to do supplier filtering and not have it so deep
  if (
    checkValueIsTruthy(filterState[PROJECT_SUPPLIERS]) &&
    filterId !== FILTERS_SUPPLIER_KEY
  ) {
    const value = filterState[PROJECT_SUPPLIERS];

    if (Array.isArray(value)) {
      const values = value.map(getValueFromOption);

      queryObjects.push(row => values.includes(row[FILTERS_SUPPLIER_KEY]));
    }
  }

  // TODO: see if there is a better way to do supplier filtering and not have it so deep
  if (
    checkValueIsTruthy(filterState[PROJECT_SUPPLIER_SINGLE]) &&
    filterId !== FILTERS_SUPPLIER_KEY
  ) {
    const value = filterState[PROJECT_SUPPLIER_SINGLE];

    if (value) {
      const rawValue = getValueFromOption(
        value as SingleFilterValue
      );
      queryObjects.push(row => row[FILTERS_SUPPLIER_KEY] === rawValue);
    }
  }

  return queryObjects;
};

const runAllQueriesOnDf = (df: IDataFrame, queryCallbacks: QueryCallback[]): IDataFrame => {
  return queryCallbacks.reduce((newDf, cb) => {
    return newDf.where(cb);
  }, df);
}

const getFilterDeclarationsWithSmartOptions = (
  oldFilterDeclaration: FilterDeclaration[],
  filterState: FilterState,
  df
) => {
  const filtersWithoutOptions = oldFilterDeclaration.filter(({ options }) =>
    isEmptyArray(options)
  );

  return oldFilterDeclaration.map((filter) => {
    const { options, id } = filter;

    if (isEmptyArray(options)) {
      const queryObjects = getQueryObjectsForFilter(
        filterState,
        id,
        filtersWithoutOptions
      );
      const filteredDf = runAllQueriesOnDf(df, queryObjects);
      const rawOptions = filteredDf.getSeries(id).distinct().toArray();

      return {
        ...filter,
        options: rawOptions.map(stringToDropdownOption),
      };
    }

    // TODO: see if there is a better way to do supplier filtering and not have it so deep
    if (id === PROJECT_SUPPLIERS) {
      const queryObjects = getQueryObjectsForFilter(
        filterState,
        FILTERS_SUPPLIER_KEY,
        filtersWithoutOptions
      );
      const filteredDf = runAllQueriesOnDf(df, queryObjects);
      const rawOptions = filteredDf.getSeries(FILTERS_SUPPLIER_KEY).distinct().toArray();

      return {
        ...filter,
        options: rawOptions.map(stringToDropdownOption),
      };
    }

    // TODO: see if there is a better way to do supplier filtering and not have it so deep
    if (id === PROJECT_SUPPLIER_SINGLE) {
      const queryObjects = getQueryObjectsForFilter(
        filterState,
        FILTERS_SUPPLIER_KEY,
        filtersWithoutOptions
      );
      const filteredDf = runAllQueriesOnDf(df, queryObjects);
      const rawOptions = filteredDf.getSeries(FILTERS_SUPPLIER_KEY).distinct().toArray();

      return {
        ...filter,
        options: rawOptions.map(stringToDropdownOption),
      };
    }

    return filter;
  });
};

interface UseSmartFilterParams extends UseFilterParams {
  filterCombinations: any[];
}

export const useSmartFilter = ({
  initialState = INITIAL_STATE,
  initialValuesMap = INITIAL_VALUES_MAP,
  filterDeclaration,
  filterCombinations = FILTER_COMBINATIONS,
  reduxFilterId,
}: UseSmartFilterParams) => {
  const {
    filterFunction,
    filterProps: {
      // filters, // not needed, since we replace it with our "smart" logic
      values,
    },
    filterState,
    setFilterState,
  } = useFilter({
    filterDeclaration,
    initialState,
    initialValuesMap,
    reduxFilterId,
  });

  const smartFilterDeclaration = useMemo(() => {
    if (!filterCombinations.length) {
      return filterDeclaration;
    }

    const df = new DataFrame(filterCombinations);

    return getFilterDeclarationsWithSmartOptions(
      filterDeclaration,
      filterState,
      df
    );
  }, [filterDeclaration, filterState, filterCombinations]);

  return {
    filterFunction,
    filterProps: {
      // we override the filter declaration so that we can have our options there
      filters: smartFilterDeclaration,
      values,
    },
    filterState,
    setFilterState,
  };
};

const EMPTY_ARRAY = [];

const getMultiFilterDefinition = ({ name }) => ({
  accessor: row => row.filters[name],
  id: name,
  label: name,
  type: TYPE.MULTI,
});

const getOptionsFromArray = (array: string[]) =>
  array.map(stringToDropdownOption);

interface UseProjectFilterParams {
  additionalFilters: FilterDeclaration[];
  initialState?: FilterState;
  reduxFilterId?: string;
}

export const useProjectFilter = ({
  initialState = INITIAL_STATE,
  additionalFilters = EMPTY_ARRAY,
  reduxFilterId = undefined,
}: UseProjectFilterParams) => {
  const project = useSelector(projectsSelectors.getProjectDetails);
  const projectId = project?.id;

  const filterCombinations = useSelector(
    projectsSelectors.getProjectFilterCombinations
  );
  const filters = useSelector(projectsSelectors.getProjectFilters);
  const itemTypes = useSelector(projectsSelectors.getProjectItemTypes);
  const items = useSelector(projectsSelectors.getProjectItems);
  const priceComponents = useSelector(
    projectsSelectors.getProjectPriceComponents
  );
  const volumes = useSelector(projectsSelectors.getProjectVolumes);
  const suppliers = useSelector(projectsSelectors.getProjectSuppliers);

  const initialValuesMap = {
    [PROJECT_COST_COMPONENT]: priceComponents,
    [PROJECT_VOLUME]: volumes,
  };

  const additionalFiltersWithValues = additionalFilters.map((filter) => {
    const { id } = filter;

    switch (id) {
      case PROJECT_ITEMS:
        return {
          ...filter,
          options: getOptionsFromArray(items.sort()),
        };
      case PROJECT_ITEM_TYPE:
        return {
          ...filter,
          options: getOptionsFromArray(itemTypes.sort()),
        };
      case PROJECT_VOLUME:
        return {
          ...filter,
          options: getOptionsFromArray(volumes.sort()),
        };
      case PROJECT_COST_COMPONENT:
        return {
          ...filter,
          options: getOptionsFromArray(priceComponents),
        };
      case PROJECT_SUPPLIERS:
      case PROJECT_SUPPLIER_SINGLE:
        return {
          ...filter,
          options: getOptionsFromArray(suppliers.sort()),
        };
      default:
        return filter;
    }
  });

  const filterDeclaration = [
    ...additionalFiltersWithValues,
    ...filters.map(getMultiFilterDefinition),
  ];

  const projectReduxFilterId =
    projectId && reduxFilterId ? `${projectId}_${reduxFilterId}` : undefined;

  return useSmartFilter({
    filterCombinations,
    filterDeclaration,
    initialState,
    initialValuesMap,
    reduxFilterId: projectReduxFilterId,
  });
};
