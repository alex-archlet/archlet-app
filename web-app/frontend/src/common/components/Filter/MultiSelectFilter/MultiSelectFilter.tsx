import React from 'react';
import styled from 'styled-components';
import { Checkbox } from '@material-ui/core';
import { SelectAllToggle } from './SelectAllToggle';
import {
  FilterList,
  FilterListItem,
  FilterListItemLabel,
} from '../FilterList/FilterList';
import { OnMultiFilterChange, FilterOption, MultiFilterValue, SingleFilterValue } from '../types';

const StyledCheckBox = styled(Checkbox)`
  color: ${props => (props.checked ? '#4790A5 !important' : 'initial')};
  padding: 0 10px 0 0 !important;
`;

const isSameOption = (option: FilterOption, value: SingleFilterValue) => option.value === value.value;

const isChecked = (options: FilterOption[], value: SingleFilterValue) =>
  options.some(option => isSameOption(option, value));

interface Props {
  onChange: OnMultiFilterChange;
  options: FilterOption[];
  values: MultiFilterValue;
}

export const MultiSelectFilter: React.FC<Props> = ({ onChange, options, values }) => {
  const handleOnChange = (option, checked) => {
    if (checked) {
      onChange(values.filter(value => !isSameOption(option, value)));
    } else {
      onChange([...values, option]);
    }
  };

  const allChecked = values.length === options.length;

  const handleToggle = () => {
    if (allChecked) {
      onChange([]);
    } else {
      onChange(options);
    }
  };

  return (
    <FilterList>
      <SelectAllToggle onToggle={handleToggle} allSelected={allChecked} />
      {options.map((option) => {
        const checked = isChecked(values, option);

        return (
          <FilterListItem
            key={option.value}
            onClick={() => handleOnChange(option, checked)}
            button
          >
            <StyledCheckBox checked={checked} />
            <FilterListItemLabel>{option.label}</FilterListItemLabel>
          </FilterListItem>
        );
      })}
    </FilterList>
  );
};

MultiSelectFilter.defaultProps = {
  values: [],
};
