import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Switch } from '@material-ui/core';
import { FilterListItemLabel } from '../FilterList/FilterList';

const StyledContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  margin-bottom: 8px;
  padding: 0 16px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
`;

const StyledSwitch = styled(Switch)`
  && .MuiSwitch-track {
    background-color: ${props => (props.checked ? '#4790a5 !important' : 'default')};
  }

  && .MuiSwitch-thumb {
    color: ${props => (props.checked ? '#4790a5 !important' : 'default')};
  }
`;

const LABEL = 'Select all';

export const SelectAllToggle = ({
  allSelected,
  onToggle,
}) => (
  <StyledContainer>
    <FilterListItemLabel>
      {LABEL} 
    </FilterListItemLabel>
    <StyledSwitch
      checked={allSelected}
      onChange={onToggle}
      value="checkedB"
      inputProps={{ 'aria-label': 'primary checkbox' }}
    />
  </StyledContainer>
);

SelectAllToggle.propTypes = {
  allSelected: PropTypes.bool.isRequired,
  onToggle: PropTypes.func.isRequired,
};
