import styled from 'styled-components';
import { List, ListItem } from '@material-ui/core';

export const FilterList = styled(List)`
  padding-top: 0 !important;
  padding-bottom: 0px !important;
`;

export const FilterListItem = styled(ListItem)`
  font-size: 13px;
  padding-bottom: 4px !important;
  padding-top: 4px !important;
`;

export const FilterListItemLabel = styled.span`
  line-height: 16px;
`;

