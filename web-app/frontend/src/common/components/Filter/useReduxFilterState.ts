import { useSelector, useDispatch } from 'react-redux';
import { filtersSelectors } from '@store/selectors/filters';
import { setFiltersState } from '@store/actions/filters';
import { useEffect } from 'react';
import { FilterState, SetFilterState } from './types';

interface UseReduxFilterStateParams {
  uniqueId: string;
  initialState?: any;
}

const EMPTY_OBJECT: FilterState = {};

export const useReduxFilterState = ({
  initialState,
  uniqueId,
}: UseReduxFilterStateParams): [FilterState, SetFilterState] => {
  const filtersMap = useSelector(filtersSelectors.getFiltersMap);
  const dispatch = useDispatch();

  const filterState: FilterState | undefined = filtersMap[uniqueId];

  const setFilterState = (state: FilterState) => {
    dispatch(setFiltersState(uniqueId, state));
  };

  useEffect(() => {
    if (!filterState) {
      setFilterState(initialState);
    }
  }, [initialState, filterState]);

  return [filterState || EMPTY_OBJECT, setFilterState];
};
