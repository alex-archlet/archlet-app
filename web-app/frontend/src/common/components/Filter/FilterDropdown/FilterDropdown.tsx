import React, { useCallback, useMemo, useState } from 'react';
import styled from 'styled-components';
import Popover, {PopoverOrigin} from '@material-ui/core/Popover';
import { truncate } from '../../../../utils/stringFormat';

import { FilterChip } from '../FilterChip/FilterChip';
import { FilterPopoverContainer } from '../FilterPopoverContainer/FilterPopoverContainer';
import { MultiSelectFilter } from '../MultiSelectFilter/MultiSelectFilter';
import { SingleSelectFilter } from '../SingleSelectFilter/SingleSelectFilter';
import { checkValueIsTruthy, TYPE as FILTER_TYPE } from '../filterUtils';
import { FilterText } from '../FilterText/FilterText';

import {
  FilterOption,
  SingleFilterValue,
  MultiFilterValue,
  FilterType,
  OnSingleFilterChange,
  OnMultiFilterChange,
} from '../types';

const FIXED_PROPS = {
  anchorOrigin: {
    horizontal: 'left',
    vertical: 'bottom',
  },
  transformOrigin: {
    horizontal: 'left',
    vertical: 'top',
  },
};



const POPOVER_STYLE = {
  marginTop: 8,
};

const getTitleForArray = (values: MultiFilterValue) => {
  if (values.length > 1) {
    const otherItemCount = values.length - 1;

    return `${truncate(values[0].label, 17)} + ${otherItemCount}`;
  }

  return truncate(values[0].label);
};

const getTitleFromValues = (
  values: MultiFilterValue | SingleFilterValue | undefined,
  title: string
) => {
  if (!checkValueIsTruthy(values)) {
    return truncate(title);
  }

  if (Array.isArray(values)) {
    return getTitleForArray(values);
  }

  return truncate((values as SingleFilterValue).label);
};

export const TYPE = FILTER_TYPE;

const DropdownFlexContainer = styled.div`
  margin-top: 10px;
  display: flex;
  align-items: center;
`;

const FilterTextContainer = styled.div`
  margin-right: 12px;
`;

interface Props {
  clearable: boolean;
  label: string;
  onChange: OnSingleFilterChange | OnMultiFilterChange;
  options: FilterOption[];
  text?: string;
  type: FilterType;
  values?: SingleFilterValue | MultiFilterValue | undefined;
}

export const FilterDropdown: React.FC<Props> = ({
  clearable,
  onChange,
  options,
  label,
  text,
  type,
  values,
}) => {
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = useCallback(
    (event, ref) => {
      setAnchorEl(ref.current);
    },
    [setAnchorEl]
  );

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleClear = () => {
    if (type === TYPE.MULTI) {
      const value: MultiFilterValue = [];
      const method = onChange as OnMultiFilterChange
      method(value);
    } else {
      const value = null;
      onChange(value);
    }
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;
  const hasValue = useMemo(() => checkValueIsTruthy(values), [values]);
  const chipTitle = useMemo(() => getTitleFromValues(values, label), [
    values,
    label,
  ]);

  return (
    <DropdownFlexContainer>
      {text && (
        <FilterTextContainer>
          <FilterText text={text} />
        </FilterTextContainer>
      )}
      <FilterChip
        hasValue={hasValue}
        label={chipTitle}
        clearable={clearable}
        onClick={handleClick}
        onDelete={handleClear}
      />
      {/* @ts-ignore */}
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={FIXED_PROPS.anchorOrigin as PopoverOrigin}
        transformOrigin={FIXED_PROPS.transformOrigin as PopoverOrigin}
        style={POPOVER_STYLE}
      >
        <FilterPopoverContainer onClose={handleClose} title={label}>
          {type === FILTER_TYPE.MULTI && (
            <MultiSelectFilter
              options={options}
              values={values as MultiFilterValue}
              onChange={onChange as OnMultiFilterChange}
            />
          )}
          {type === FILTER_TYPE.SINGLE && (
            <SingleSelectFilter
              options={options}
              value={values as SingleFilterValue}
              onChange={onChange as OnSingleFilterChange}
            />
          )}
        </FilterPopoverContainer>
      </Popover>
    </DropdownFlexContainer>
  );
};

FilterDropdown.defaultProps = {
  text: undefined,
  values: undefined,
};
