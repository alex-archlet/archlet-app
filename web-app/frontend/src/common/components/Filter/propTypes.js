import PropTypes from 'prop-types';

const OPTION = PropTypes.shape({
  label: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
});

export const OPTIONS = PropTypes.arrayOf(OPTION);

export const VALUE = OPTION;
export const VALUES = PropTypes.arrayOf(VALUE);
