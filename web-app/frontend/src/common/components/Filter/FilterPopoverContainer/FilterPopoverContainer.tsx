import React from 'react';
import styled from 'styled-components';
import CloseIcon from '@material-ui/icons/Close';
import { IconButton } from '@material-ui/core';

const StyledContainer = styled.div`
  width: 220px;
  padding: 16px 0;
  position: relative;
`;

const StyledCloseIconButton = styled(IconButton)`
  position: absolute !important;
  top: 8px;
  right: 8px;
`;

const StyledTitle = styled.div`
  font-size: 16px;
  line-height: 21px;
  margin-bottom: 8px;
  padding: 0 16px;
`;

interface Props {
  children: React.ReactNode,
  onClose: () => void,
  title: string,
}

export const FilterPopoverContainer: React.FC<Props> = ({
  children,
  onClose,
  title,
}) => (
  <StyledContainer>
    <StyledTitle>
      {title}
    </StyledTitle>
    {children}
    <StyledCloseIconButton
      onClick={onClose}
      size="small"
    >
      <CloseIcon />
    </StyledCloseIconButton>
  </StyledContainer>
);

