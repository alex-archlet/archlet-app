import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const StyledText = styled.span`
  margin-top: 10px;
  font-size: 12px;
  line-height: 16px;
  color: #000000;
  white-space: nowrap;
`;

export const FilterText = ({ text }) => (
  <StyledText>
    {text}
  </StyledText>
);

FilterText.propTypes = {
  text: PropTypes.string.isRequired,
};
