export type AccessorFunction = (item: any) => any;
export type SingleFilterValue = {
  label: string | number;
  value: string | number;
};
export type MultiFilterValue = SingleFilterValue[];
export type FilterOption = SingleFilterValue;
export type InitialValuesMap = {
  [id: string]: FilterOption[] | string[] | number[],
};
export type SetFilterState = (state: FilterState) => void;
export type FilterType = 'SINGLE' | 'MULTI';
export interface FilterDeclaration {
  id: string;
  type: FilterType;
  accessor?: string | AccessorFunction;
  clearable?: boolean;
  customFilter?: (item: any, state: FilterState) => boolean;
  index?: number,
  initialValuesMap?: InitialValuesMap;
  label?: string;
  options?: FilterOption[];
  text?: string,
  alias?: string,
}

export interface FilterState {
  [id: string]: SingleFilterValue | MultiFilterValue;
}

export type OnSingleFilterChange = (newValue: SingleFilterValue | undefined | null) => void;
export type OnMultiFilterChange = (newValues: MultiFilterValue | undefined | null) => void;
