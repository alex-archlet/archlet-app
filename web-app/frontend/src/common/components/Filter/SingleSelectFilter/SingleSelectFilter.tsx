import React from 'react';
import styled from 'styled-components';
import { Radio } from '@material-ui/core';
import {
  FilterList,
  FilterListItem,
  FilterListItemLabel,
} from '../FilterList/FilterList';
import {
  OnSingleFilterChange,
  FilterOption,
  SingleFilterValue,
} from '../types';

const StyledRadio = styled(Radio)`
  color: ${props => (props.checked ? '#4790A5 !important' : 'initial')};
  padding: 0 10px 0 0 !important;
`;

const isSameOption = (option: FilterOption, value: SingleFilterValue | undefined) => {
  if (!option || !option.value || !value || !value.value) {
    return false;
  }

  return option.value === value.value;
};

interface Props {
  onChange: OnSingleFilterChange;
  options: FilterOption[];
  value?: SingleFilterValue;
}

export const SingleSelectFilter: React.FC<Props> = ({
  onChange,
  options,
  value,
}) => (
  <FilterList>
    {options.map(option => (
      <FilterListItem
        key={option.value}
        onClick={() => onChange(option)}
        button
      >
        <StyledRadio checked={isSameOption(option, value)} />
        <FilterListItemLabel>{option.label}</FilterListItemLabel>
      </FilterListItem>
    ))}
  </FilterList>
);

SingleSelectFilter.defaultProps = {
  value: undefined,
};
