import {
  FilterDeclaration,
  FilterState,
  AccessorFunction,
  InitialValuesMap,
  SingleFilterValue,
  FilterType,
} from './types';

interface TypeMap {
  [key: string]: FilterType;
}

export const TYPE: TypeMap = {
  MULTI: 'MULTI',
  SINGLE: 'SINGLE',
};

export const isEmptyArray = (array: Array<any> | any) =>
  !Array.isArray(array) || array.length === 0;

export const checkValueIsTruthy = (value: any) => {
  if (Array.isArray(value)) {
    return !isEmptyArray(value);
  }

  return Boolean(value);
};
export const getColumnsWithValues = (
  filterDeclaration: FilterDeclaration[],
  filterState: FilterState
) => {
  const filterStateValues = Object.values(filterState);

  if (filterStateValues.length === 0) {
    return [];
  }

  return filterDeclaration.filter(({ id }) =>
    checkValueIsTruthy(filterState[id])
  );
};

export const getColumnFilterFunction = (
  column: FilterDeclaration,
  filterState: FilterState
) => {
  const columnFilterState = filterState[column.id];

  if (column.accessor && columnFilterState) {
    const getValue: AccessorFunction =
      typeof column.accessor === 'string'
        ? row => row[column.accessor as string]
        : column.accessor;

    if (column.type === TYPE.SINGLE && !Array.isArray(columnFilterState)) {
      return row => columnFilterState.value === getValue(row);
    }

    if (
      column.type === TYPE.MULTI &&
      Array.isArray(columnFilterState) &&
      !isEmptyArray(columnFilterState)
    ) {
      return row =>
        columnFilterState.some(({ value }) => value === getValue(row));
    }
  }

  if (column.customFilter && typeof column.customFilter === 'function') {
    // @ts-ignore somehow the above checks are not made
    return row => column.customFilter(row, filterState);
  }

  return () => true;
};

const getFilterFunction = (
  filterDeclaration: FilterDeclaration[],
  filterState: FilterState
) => {
  if (!filterState) {
    return data => data;
  }
  const columnsWithValues = getColumnsWithValues(
    filterDeclaration,
    filterState
  );

  if (columnsWithValues.length === 0) {
    return data => data;
  }

  const filterFunctions = columnsWithValues.map(column =>
    getColumnFilterFunction(column, filterState)
  );

  return data => data.filter(row => filterFunctions.every(func => func(row)));
};

export const filterHelper = (
  filterDeclaration: FilterDeclaration[],
  filterState: FilterState
) => ({
  filterFunction: getFilterFunction(filterDeclaration, filterState),
  filterProps: {
    filters: filterDeclaration,
    values: filterState,
  },
});

export const stringToDropdownOption = (value: string | number) => ({
  label: value,
  value,
});

type Element = {
  name: string,
  id: string,
}

export const nameIdDropdownOption = (element: Element) => ({
  label: element.name,
  value: element.id,
});

type InitialValueOptions =
  | SingleFilterValue[]
  | string[]
  | number[];

type InitialValueOptionsOptional =
  | SingleFilterValue[]
  | string[]
  | number[]
  | undefined;

export const getFirstValue = (array: any) => {
  if (!Array.isArray(array) || !array.length) {
    return null;
  }
  return array[0];
};

const getOptionFromValue = (firstItem: any) => {
  if (typeof firstItem === 'string' || typeof firstItem === 'number') {
    return stringToDropdownOption(firstItem);
  }
  return firstItem;
}

export const getInitialValue = (array: InitialValueOptionsOptional) => {
  const firstItem = getFirstValue(array);
  return getOptionFromValue(firstItem);
};

export const getInitialValueMulti = (array: InitialValueOptions) => {
  // @ts-ignore they are all arrays anyway - so ignore
  return array.map(getOptionFromValue);
};

export const getInitialValueSingle = (array) => {
  const firstItem = getFirstValue(array);
  if (!firstItem) {
    return null;
  }

  return getOptionFromValue(firstItem);
};

export const getValueFromOption = ({ value }) => value;

export const getInitialValues = (
  filterDeclaration: FilterDeclaration[],
  filterState: FilterState,
  optionsMap: InitialValuesMap
) => {
  let changed = false;
  const newValue = {};

  const filterIdMap = {};

  filterDeclaration.forEach((declaration) => {
    filterIdMap[declaration.id] = declaration;
  });

  Object.keys(optionsMap).forEach((key) => {
    const optionsForKey = optionsMap[key] || [];
    const stateForKey = filterState[key];
    const columnFilterDeclaration = filterIdMap[key];

    if (
      columnFilterDeclaration &&
      optionsForKey.length > 0 &&
      stateForKey === undefined
    ) {
      if (columnFilterDeclaration.type === TYPE.SINGLE) {
        newValue[key] = getInitialValueSingle(optionsForKey);
        changed = true;
      } else if (columnFilterDeclaration.type === TYPE.MULTI) {
        newValue[key] = getInitialValueMulti(optionsForKey);
        changed = true;
      }
    }
  });

  return {
    changed,
    newValue,
  };
};
