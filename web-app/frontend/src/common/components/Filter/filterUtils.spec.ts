import {
  checkValueIsTruthy,
  getColumnFilterFunction,
  getColumnsWithValues,
  getFirstValue,
  getInitialValues,
  getValueFromOption,
  isEmptyArray,
} from './filterUtils';
import { TYPE } from './FilterDropdown/FilterDropdown';

const OPTION_LABEL = 'NorthAmerica';
const OPTION_VALUE = 'NorthAmerica';
const FILTER_ID = 'regions';

const dataRow1 = {
  region: 'NorthAmerica',
  totalCost: 400,
};

const dataRow2 = {
  region: 'SouthAmerica',
  totalCost: 200,
};

const dataRow3 = {
  region: 'Asia',
  totalCost: 200,
};

const data = [dataRow1, dataRow2, dataRow3];

const filterDeclaration = {
  accessor: 'region',
  id: FILTER_ID,
  label: 'Regions',
  options: [{
    label: OPTION_LABEL,
    value: OPTION_VALUE,
  }],
  type: TYPE.MULTI,
};

const filterDeclarations = [
  filterDeclaration,
];

const filterDeclarationSingle =
  {
    accessor: 'region',
    id: FILTER_ID,
    label: 'Regions',
    options: [{
      label: OPTION_LABEL,
      value: OPTION_VALUE,
    }],
    type: TYPE.SINGLE,
  };

const filterDeclarationCustom =
  {
    accessor: 'region',
    type: TYPE.MULTI,
    customFilter: row => row.filter(rowData => rowData.region === 'SouthAmerica'),
    id: FILTER_ID,
    label: 'Regions',
    options: [{
      label: OPTION_LABEL,
      value: OPTION_VALUE,
    }],
  };

const filterState = {
  [FILTER_ID]: {
    label: 'NorthAmerica',
    value: 'NorthAmerica',
  },
};

const filterMultiState = {
  [FILTER_ID]: [
    {
      label: 'NorthAmerica',
      value: 'NorthAmerica',
    },
    {
      label: 'SouthAmerica',
      value: 'SouthAmerica',
    },
  ],
};

describe('filter util', () => {
  describe('isEmptyArray', () => {
    it('should return false', () => {
      expect(isEmptyArray([])).toBeTruthy();
      expect(isEmptyArray(undefined)).toBeTruthy();
      expect(isEmptyArray(null)).toBeTruthy();
    });
    it('should return true', () => {
      expect(isEmptyArray([1])).toBeFalsy();
    });
  });

  describe('checkIfValues is Truthy or Falsey', () => {
    it('should return true if the function is given array with values', () => {
      expect(checkValueIsTruthy(['test'])).toBeTruthy();
    });
    it('should return false if the function is given null', () => {
      expect(checkValueIsTruthy(null)).toBeFalsy();
    });
  });

  describe('getColumnsWithValues expected returns', () => {
    it('should return empty array if the function is given empty object', () => {
      expect(getColumnsWithValues([], {})).toEqual([]);
    });
    it('should find first result if the function is given correct parameters', () => {
      const result = getColumnsWithValues(filterDeclarations, filterState);

      expect(result[0]).toEqual(filterDeclaration);
    });
  });

  describe('getColumnFilterFunction expected returns', () => {
    it('should return a function if given empty params', () => {
      const result = getColumnFilterFunction({ id: 'lol', type: TYPE.SINGLE }, {});

      expect(result({})).toEqual(true);
    });
    it('should match the data row and return true and testing the single declaration', () => {
      const filterFunction = getColumnFilterFunction(filterDeclarationSingle, filterState);

      expect(filterFunction(dataRow1)).toEqual(true);
    });
    it('should not match the data row and return false and testing the single declaration', () => {
      const filterFunction = getColumnFilterFunction(filterDeclarationSingle, filterState);

      expect(filterFunction(dataRow2)).toEqual(false);
    });
    it('should match the data row and return true and testing the multi declaration', () => {
      const filterFunctions = getColumnFilterFunction(filterDeclaration, filterMultiState);

      expect(filterFunctions(dataRow1)).toEqual(true);
      expect(filterFunctions(dataRow2)).toEqual(true);
    });
    it('should not match the data row and return false and testing the multi declaration', () => {
      const filterFunctions = getColumnFilterFunction(filterDeclaration, filterMultiState);

      expect(filterFunctions(dataRow3)).toEqual(false);
    });
    it('should match the data row and return true and testing a custom function', () => {
      const filterFunction = getColumnFilterFunction(filterDeclarationCustom, filterState);

      expect(filterFunction(data)).toEqual([dataRow2]);
    });
  });

  describe('getFirstValue', () => {
    it('should get first value if not empty', () => {
      expect(getFirstValue([1])).toBe(1);
      expect(getFirstValue([1, 2])).toBe(1);
    });
    it('should get null if empty', () => {
      expect(getFirstValue([])).toBe(null);
      expect(getFirstValue(null)).toBe(null);
    });
  });

  describe('getValueFromOption', () => {
    it('should get value if exists', () => {
      expect(getValueFromOption({ value: 1 })).toBe(1);
      expect(getValueFromOption({ value: '1' })).toBe('1');
    });
    it('should get undefined if does not exist', () => {
      expect(getValueFromOption({ value: undefined })).toBe(undefined);
    });
  });

  describe('getInitialValues', () => {
    it('should return no changes if state empty and no options', () => {
      expect(getInitialValues(filterDeclarations, {}, {})).toStrictEqual({
        changed: false,
        newValue: {},
      });
    });
    it('should return no changes if state exists and no options', () => {
      expect(getInitialValues(filterDeclarations, {
        x: {
          label: '1',
          value: 1,
        },
        y: {
          label: '2',
          value: 2,
        },
      }, { })).toStrictEqual({
        changed: false,
        newValue: {},
      });
    });
    it('should return no changes if state exists and options exist', () => {
      expect(getInitialValues(filterDeclarations, {
        x: {
          label: '1',
          value: 1,
        },
        y: {
          label: '2',
          value: 2,
        },
      }, { x: [1] })).toStrictEqual({
        changed: false,
        newValue: {},
      });
    });
    it('should return changes if state does not exist and options exist', () => {
      expect(getInitialValues(filterDeclarations, {
        y: {
          label: '2',
          value: 2,
        },
      }, { x: [1, 2] })).toStrictEqual({
        changed: false,
        newValue: {},
      });
    });
  });
});
