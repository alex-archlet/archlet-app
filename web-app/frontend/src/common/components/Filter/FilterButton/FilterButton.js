import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Button } from '@common/components/Button/Button';

export const StyledButton = styled(Button)`
  && {
    border: 1px solid #e1e5e9;
    border-radius: 5px;
    background-color: #eef2f4;
    color: #44566c;
    font-size: 12px;
    font-weight: normal;
    text-transform: none;
    line-height: 18px;
    height: 32px;
    margin-top: 10px;
  }
`;

export const FilterButton = ({ children, onClick }) => (
  <StyledButton onClick={onClick}>{children}</StyledButton>
);

FilterButton.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]).isRequired,
  onClick: PropTypes.func.isRequired,
};
