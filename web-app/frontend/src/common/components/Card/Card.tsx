import React from 'react';
import styled from 'styled-components';
import { colors } from '@utils/uiTheme';
import { Box } from '@material-ui/core';
import { Text } from '@common/components/Text/Text';
import { CardTitle } from './CardTitle';

const StyledContainer = styled.div`
  margin: 10px;
  margin-top: 0px;
  border: 1px solid ${colors.default};
  border-radius: 5px;
  background-color: ${colors.light};
  width: calc(100% - 20px);
  padding: 10px 20px;
  flex: 1;
  overflow-x: auto;
`;

const StyledDescription = styled(Text)`
  color: #2c2c2c;
  line-height: 20px;
  opacity: 0.4;
`;

interface Props {
  children: React.ReactNode;
  title?: string;
  tooltipInfoMessage?: string;
  description?: string;
  rightSide?: React.ReactNode;
  style?: object;
}

export const Card: React.FC<Props> = ({
  children,
  style,
  title,
  tooltipInfoMessage,
  description,
  rightSide,
}: Props) => {

  if (!title) {
    return (
      <StyledContainer style={style}>
        <Box ml={10}>
          <StyledDescription fontSize={14} fontWeight={400} color="#44566c">
            {description}
          </StyledDescription>
        </Box>
        {children}
      </StyledContainer>
    );
  }

  return (
    <StyledContainer style={style}>
      {title && (
        <CardTitle
          title={title}
          tooltipInfoMessage={tooltipInfoMessage}
          rightSide={rightSide}
        />
      )}
      <Box ml={10}>
        <StyledDescription fontSize={14} fontWeight={400} color="#44566c">
          {description}
        </StyledDescription>
      </Box>
      {children}
    </StyledContainer>
  );
};
Card.defaultProps = {
  rightSide: null,
  style: {},
  title: undefined,
  description: undefined,
  tooltipInfoMessage: undefined,
};
