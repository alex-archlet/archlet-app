import React from 'react';
import styled from 'styled-components';
import { Box, Tooltip } from '@material-ui/core';
import { colors } from '@utils/uiTheme';
import { Info } from '@material-ui/icons';

import { Text } from '@common/components/Text/Text';

const StyledInfoTitle = styled(Info)`
  font-size: 20px !important;
  margin-left: 2px;
  color: ${colors.textColor};
`;

const FlexContainer = styled.div`
  width: 100%;
  margin: 10px 10px 10px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
`;

const StyledTooltip = styled(props => (
  <Tooltip
    classes={{
      popper: props.className,
      tooltip: 'tooltip',
    }}
    {...props}
  />
))`
  & .tooltip {
    border-radius: 5px;
    margin-bottom: 0px;
    background-color: #ffffff;
    color: #44566c;
    box-shadow: 0 2px 10px 0 rgba(10, 22, 70, 0.1);
    font-size: 14px;
    padding: 10px;
    max-width: 200px;
  }
`;

interface Props {
  title: string;
  tooltipInfoMessage?: string;
  rightSide?: React.ReactNode;
}

export const CardTitle: React.FC<Props> = ({
  title,
  tooltipInfoMessage,
  rightSide,
}) => (
  <FlexContainer>
    <Box display="flex" alignItems="center">
      <Text fontSize={18} fontWeight={400} color="#44566c">
        {title}
      </Text>
      {tooltipInfoMessage && (
        <StyledTooltip title={tooltipInfoMessage} placement="right-end">
          <StyledInfoTitle />
        </StyledTooltip>
      )}
    </Box>
    {rightSide}
  </FlexContainer>
);

CardTitle.defaultProps = {
  rightSide: null,
  tooltipInfoMessage: '',
};
