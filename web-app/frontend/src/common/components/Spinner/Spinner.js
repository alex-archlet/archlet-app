import React from 'react';
import styled from 'styled-components';
import { CircularProgress } from '@material-ui/core';

const LoadingSpinner = styled.div`
  position: fixed;
  z-index: 1300;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
  background-color: rgba(255, 255, 255, 0.7);
`;

export const Spinner = () => (
  <LoadingSpinner>
    <CircularProgress />
  </LoadingSpinner>
);
