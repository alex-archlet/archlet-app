import { useQueryParams, StringParam, withDefault } from 'use-query-params';

interface Props {
  options: string[];
  initialValue: string;
}

export const useTabParams = ({ options, initialValue }: Props) => {
  const [query, setQuery] = useQueryParams({
    tab: withDefault(StringParam, initialValue),
  });

  const onChange = (id) => {
    setQuery({ tab: id }, 'push');
  };
  const { tab } = query;

  const currentTab = options.includes(tab) ? tab : initialValue;

  return {
    tab: currentTab,
    onChange,
  };
};
