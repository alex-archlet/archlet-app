import React, { useEffect, useMemo, useState } from 'react';
import styled from 'styled-components';
import { AppBar, Tab, Tabs } from '@material-ui/core';
import { colors } from '@utils/uiTheme';
import { useCustomTranslation } from '@utils/translate';

type StyledProps = {
  paddingTop?: number;
};
const StyledAppBar = styled(AppBar)<StyledProps>`
  && {
    box-shadow: none;
    border-bottom: 1px solid ${colors.grey};
    padding: 0 33px;
    background-color: #fff;
    color: #000;
    padding-top: ${props => props.paddingTop || 6}px;
  }
`;

const StyledTab = styled(({ active: _active, ...props }) => <Tab {...props} />)`
  && {
    min-width: 90px;
    margin: 0 10px;
    color: #000000;
    letter-spacing: 1.1px;
    font-weight: bold;
    opacity: ${props => (props.active ? '1' : '0.5')};
  }
`;

const RightSideContainer = styled.div`
  position: absolute;
  bottom: 10px;
  right: 0px;
`;

interface Tab {
  id: string;
  label: string;
  render: Function;
  disabled?: boolean;
}

interface Props {
  tabs: Tab[];
  paddingTop?: number;
  initialValue?: string;
  rightSide?: JSX.Element | null | undefined | string;
  onChange?: Function;
}

const TabsComponent: React.FC<Props> = ({
  initialValue,
  rightSide,
  tabs,
  paddingTop,
  onChange,
}) => {
  const [value, setValue] = useState<string | null>();
  const { t } = useCustomTranslation();

  useEffect(() => {
    if (initialValue && initialValue !== value) {
      setValue(initialValue);
    } else if (!value) {
      const newValue = tabs[0] ? tabs[0].id : null;

      setValue(newValue);
    }
  }, [initialValue, tabs, value]);

  const handleChange = (index) => {
    if (onChange) {
      onChange(tabs[index].id);
    }
    setValue(tabs[index].id);
  };

  const activeTab = useMemo(() => tabs.find(tab => tab.id === value) || null, [
    tabs,
    value,
  ]);
  const numberValue = Math.max(activeTab ? tabs.indexOf(activeTab) : 0, 0);

  return (
    <div>
      <StyledAppBar position="static" paddingTop={paddingTop}>
        <Tabs
          value={numberValue}
          onChange={(_, index) => handleChange(index)}
          indicatorColor="secondary"
          textColor="inherit"
        >
          {tabs.map((item, index) => (
            <StyledTab
              value={index}
              key={item.id}
              label={t(item.label)}
              disabled={item.disabled}
              active={item === activeTab}
              disableRipple
            />
          ))}
          {rightSide && <RightSideContainer>{rightSide}</RightSideContainer>}
        </Tabs>
      </StyledAppBar>
      {activeTab && activeTab.render()}
    </div>
  );
};

TabsComponent.defaultProps = {
  initialValue: undefined,
};

export { TabsComponent as Tabs };
