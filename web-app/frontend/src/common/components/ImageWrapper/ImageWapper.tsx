import React from 'react';
import styled from 'styled-components';
import ButtonNew from '../../../assets/ButtonNew.svg';
import ButtonBeta from '../../../assets/ButtonBeta.svg';

const StyledImg = styled.img`
  height: 24px;
`;

export const IconImageNew = () => (
  <StyledImg
    src={ButtonNew}
    alt="Button New"
  />
);


export const IconImageBeta = () => (
  <StyledImg
    src={ButtonBeta}
    alt="Button Beta"
  />
);
