import React from 'react';
import styled from 'styled-components';
import { Text } from '@common/components/Text/Text';

interface Props {
  text: string;
}

const StyledContainer = styled.div`
  min-height: 300px;
  display: flex;
  align-items: center;
  justify-content: center;
  opacity: 0.55;
`;

export const PlaceholderText: React.FC<Props> = ({ text }) => (
  <StyledContainer>
    <Text fontSize={14} color="#2C2C2C">
      {text}
    </Text>
  </StyledContainer>
);
