import React from 'react';
import PropTypes from 'prop-types';
import { StyledButton } from '../Filter/FilterButton/FilterButton';

export const DownloadBox = ({ children, onClick }) => (
  <StyledButton onClick={onClick}>
    {children}
  </StyledButton>
);

DownloadBox.propTypes = {
  children: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};
