import React from 'react';
import styled from 'styled-components';
import { useCustomTranslation } from '@utils/translate';
import Select from 'react-select';
import { colors } from '@utils/uiTheme';
import { MultiFilterValue, SingleFilterValue } from '../Filter/types';

const ReactSelectElement = styled(Select)`
  & .react-select__control {
    border-bottom-left-radius: 5;
    border-bottom-right-radius: 5;
    border-top-left-radius: 5;
    border-top-right=radius: 5;
    box-shadow: none;
      &:hover {
        border-color: ${props =>
          props?.style?.controlBorderColor ??
          colors.palette.secondary.main} !important;
      }
  }
 
  & .react-select__control--menu-is-open {
    border-color: ${props =>
      props?.style?.controlBorderColor ?? colors.palette.secondary.main};
      color: ${props => props?.style?.textColor ?? colors.white};
      &:hover {
        border-color: ${props =>
          props?.style?.controlBorderColor ??
          colors.palette.secondary.main} !important;
      }
  } 
  & .react-select__control--is-focused {
    border-color: ${props =>
      props?.style?.controlBorderColor ?? colors.palette.secondary.main};
      color: ${props => props?.style?.textColor ?? colors.white};
  }
  & .react-select__option--is-focused {
    background-color: ${props =>
      props?.style?.optionBorderColor ?? colors.palette.secondary.light}
      color: ${props => props?.style?.textColor ?? colors.white};
    }

  & .react-select__option--is-selected {
    background-color: ${props =>
      props?.style?.optionSelectColor ?? colors.palette.secondary.light}
    color: ${props => props?.style?.textColor ?? colors.white};
  }
`;

type SelectElementType = {
  controlBorderColor?: string;
  optionBorderColor?: string;
  optionSelectColor?: string;
  textColor?: string;
};
interface Props {
  selectedColumnValue: SingleFilterValue | [];
  options: MultiFilterValue;
  style?: SelectElementType;
  onChange: (value) => void;
}

export const SelectDropdown: React.FC<Props> = ({
  selectedColumnValue,
  options,
  style,
  onChange,
}) => {
  const { t } = useCustomTranslation();

  return (
    <ReactSelectElement
      classNamePrefix="react-select"
      options={options}
      isMenuOpen
      value={selectedColumnValue}
      placeholder={t('Choose...')}
      components={{
        IndicatorSeparator: () => null,
      }}
      style={style}
      onChange={({ value }) => onChange(value)}
    />
  );
};
