import React, { useEffect } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import {
  Box,
  Checkbox,
  ClickAwayListener,
  List,
  ListItem,
  Paper,
  Switch,
} from '@material-ui/core';
import { Button } from '@common/components/Button/Button';
import { KeyboardArrowDown, KeyboardArrowUp } from '@material-ui/icons';
import { usePrevious } from '@utils/usePrevious';
import { colors } from '@utils/uiTheme';

const getButtonHeight = (props) => {
  if (props.large) return 50;
  if (props.medium) return 40;
  return 30;
};

const StyledPaper = styled(Paper)`
  overflow: auto;
  min-width: 140px;
  max-height: 300px;
`;

const StyledButton = styled(
  ({ ...props }) => {
    return (<Button {...props} />)
  })
`
  && {
    justify-content: flex-start;
    height: ${props =>
      !props.dynamicHeight ? `${getButtonHeight(props)}px` : 'auto'};
    padding: ${props =>
      props.large ? '3px 20px 3px 10px' : '3px 13px 3px 3px'};
    font-weight: ${props => (props.large ? 500 : 400)};
    font-size: ${props => (props.large ? 18 : 14)}px;
    text-transform: none;
    max-width: ${props => (props.dynamicHeight ? '150px' : 'auto')};
  }

  &&.MuiButton-contained {
    background-color: ${colors.light};
    border-width: 1px;
    border-style: solid;
    border-color: ${props => props.grey ? colors.grey : 'rgba(0, 0, 0, 0.5)'};
  }
`;

const StyledBox = styled(Box)`
  white-space: pre;
`;

export const Dropdown = ({
  dynamicHeight,
  grey,
  items,
  large,
  onClick,
  title,
  labelKey,
  valueKey,
  medium,
  multiple,
  allSelected,
  reduxFormValue,
  onBlur,
}) => {
  const getInitialState = () => {
    if (multiple) {
      return allSelected ? items.map(item => item[valueKey]) : [];
    }

    if (reduxFormValue) {
      const matchingItem = items.find(
        item => item[valueKey] === reduxFormValue
      );

      return matchingItem ? matchingItem[labelKey] : '';
    }

    return '';
  };

  const [state, setState] = React.useState({
    isOpen: false,
    selected: getInitialState(),
  });

  useEffect(() => {
    if (multiple && allSelected) {
      onClick(items.map(item => item[valueKey]));

      setState({
        ...state,
        selected: getInitialState(),
      });
    }
  }, [items]);

  useEffect(() => {
    if (!multiple && reduxFormValue) {
      setState({
        ...state,
        selected: getInitialState(),
      });
    }
  }, [items, reduxFormValue]);

  const prevIsOpen = usePrevious(state.isOpen);

  useEffect(() => {
    if (prevIsOpen && !state.isOpen) {
      onBlur();
    }
  }, [state]);

  const handleButtonClick = () => {
    setState({
      ...state,
      isOpen: !state.isOpen,
    });
  };

  const handleElementClick = (item) => {
    let newSelectedItems;
    let isOpen;

    setState((prevValue) => {
      if (multiple) {
        isOpen = !!state.isOpen;
        const itemIndex = prevValue.selected.findIndex(
          val => val === item[valueKey]
        );

        newSelectedItems =
          itemIndex >= 0
            ? [
                ...prevValue.selected.slice(0, itemIndex),
                ...prevValue.selected.slice(
                  itemIndex + 1,
                  prevValue.selected.length
                ),
              ].sort()
            : [...prevValue.selected, item[valueKey]].sort();
      } else {
        isOpen = !state.isOpen;
        newSelectedItems = item[labelKey];
      }

      return {
        ...prevValue,
        isOpen,
        selected: newSelectedItems,
      };
    });

    onClick(multiple ? newSelectedItems : item[valueKey]);
  };

  const handleClickAway = () => {
    setState({
      ...state,
      isOpen: false,
    });
  };

  const areAllSelected = state.selected.length === items.length;

  const toggleAll = () => {
    const newSelected = areAllSelected ? [] : items.map(item => item[valueKey]);

    setState({
      ...state,
      selected: newSelected,
    });

    onClick(newSelected);
  };

  return (
    <ClickAwayListener onClickAway={state.isOpen ? handleClickAway : () => {}}>
      <Box>
        <StyledButton
          onClick={handleButtonClick}
          variant="outlined"
          large={large}
          medium={medium}
          grey={grey}
          dynamicHeight={dynamicHeight}
        >
          {state.isOpen ? <KeyboardArrowUp /> : <KeyboardArrowDown />}
          {multiple || !state.selected ? title : state.selected}
        </StyledButton>
        {state.isOpen ? (
          <Box position="absolute" zIndex={100} pt={7}>
            <StyledPaper>
              <List>
                {multiple && (
                  <ListItem>
                    <StyledBox ml={12} mr={3}>
                      Select all
                    </StyledBox>
                    <Switch
                      size="small"
                      checked={areAllSelected}
                      onChange={toggleAll}
                      value="checkedB"
                      inputProps={{ 'aria-label': 'primary checkbox' }}
                    />
                  </ListItem>
                )}
                {items.map(item => (
                  <ListItem
                    key={item[valueKey]}
                    button
                    onClick={() => handleElementClick(item)}
                  >
                    {multiple && (
                      <Checkbox
                        checked={state.selected.indexOf(item[valueKey]) > -1}
                      />
                    )}
                    {item[labelKey] || item[valueKey]}
                  </ListItem>
                ))}
              </List>
            </StyledPaper>
          </Box>
        ) : null}
      </Box>
    </ClickAwayListener>
  );
};

Dropdown.propTypes = {
  allSelected: PropTypes.bool,
  dynamicHeight: PropTypes.bool,
  grey: PropTypes.bool,
  items: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  labelKey: PropTypes.string,
  large: PropTypes.bool,
  medium: PropTypes.bool,
  multiple: PropTypes.bool,
  onBlur: PropTypes.func,
  onClick: PropTypes.func.isRequired,
  reduxFormValue: PropTypes.oneOfType([
    // TODO: Fix this to be "value"
    PropTypes.string,
    PropTypes.number,
    PropTypes.arrayOf(PropTypes.string),
    PropTypes.arrayOf(PropTypes.number),
  ]),
  title: PropTypes.string,
  valueKey: PropTypes.string,
};

Dropdown.defaultProps = {
  allSelected: false,
  dynamicHeight: false,
  grey: false,
  labelKey: 'label',
  large: false,
  medium: false,
  multiple: false,
  onBlur: () => {},
  reduxFormValue: '',
  title: '',
  valueKey: 'value',
};
