import React from 'react';
import PropTypes from 'prop-types';
import {
  Redirect,
  Route,
} from 'react-router-dom';

export const PrivateRoute = ({
  component: Component,
  isAuthorized,
  ...rest
}) => (
  <Route
    {...rest}
    render={
      props => (
        isAuthorized ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/sign-in',
              state: { from: props.location },
            }}
          />
        )
      )
    }
  />
);

PrivateRoute.propTypes = ({
  component: PropTypes.elementType.isRequired,
  isAuthorized: PropTypes.bool.isRequired,
  location: PropTypes.shape({}),
});

PrivateRoute.defaultProps = {
  location: {},
};
