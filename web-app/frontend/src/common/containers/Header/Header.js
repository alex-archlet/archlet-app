import React from 'react';
import { Grid, Toolbar } from '@material-ui/core';
import { Button } from '@common/components/Button/Button';
import { People } from '@material-ui/icons';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Link, useLocation } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { userSelectors } from '@selectors/user';
import { interfaceSelectors } from '@selectors/interface';
import { signOut } from '@actions/user';
import { colors } from '@utils/uiTheme';
import { useCustomTranslation } from '@utils/translate';
import { NavigationButton } from '@common/components/NavigationButton/NavigationButton';
import { JobOutputDialog } from '@common/components/JobOutputDialog/JobOutputDialog';
import { Logo } from '../../components/Logo/Logo';
import { UserDropdown } from '../../components/UserDropdown/UserDropdown';
import { FlagDropdown } from '../../components/FlagDropdown/FlagDropdown';

const StyledHeader = styled.header`
  position: fixed;
  z-index: 20;
  width: 100%;
  padding: 0 30px;
  background-color: black;
  color: white;
  font-weight: 500;
  font-size: 18px;
  text-align: center;
`;

const StyledRightSection = styled(Grid)`
  display: flex;
  justify-content: flex-end;
  align-items: center;
`;

const StyledButton = styled(Button)`
  && {
    height: 37px;
    margin-right: 20px;
    padding: 0 20px 0 17px;
    background-color: #555;
    color: white;

    &: hover {
      background-color: ${colors.iconGrey};
    }
  }
`;

const StyledPeopleIcon = styled(People)`
  margin-right: 10px;
`;

const StyledLink = styled(Link)`
  text-decoration: none;
`;

const StyledLogoLink = styled(Link)`
  height: 24px;
`;

const StyledLogoGrid = styled(Grid)`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const HeaderComponent = ({
  backButtonDestination,
  backButtonType,
  isAuthenticated,
  pageTitle,
  signOutAction,
  user,
}) => {
  const location = useLocation();
  const { t } = useCustomTranslation();

  const returnTo = window.location.origin;

  const pathArray = location.pathname.split('/');
  const id = pathArray[1] === 'projects' && pathArray[2];

  const displayShareButton = id === 0 || !!id;

  return (
    <>
      <StyledHeader>
        <Toolbar>
          <Grid container spacing={3} alignItems="center">
            <Grid alignItems="center" container item xs={4}>
              <NavigationButton
                iconType={backButtonType}
                destinationUrl={backButtonDestination}
              />
              <div>{pageTitle}</div>
            </Grid>
            <StyledLogoGrid item xs={4}>
              <StyledLogoLink to="/">
                <Logo />
              </StyledLogoLink>
            </StyledLogoGrid>
            <StyledRightSection item xs={4}>
              <FlagDropdown />
              {displayShareButton && (
                <StyledLink to={`/projects/${id}/share`}>
                  <StyledButton>
                    <StyledPeopleIcon />
                    {t('Share')}
                  </StyledButton>
                </StyledLink>
              )}
              {isAuthenticated && (
                <UserDropdown
                  onSignOut={() => signOutAction(returnTo)}
                  user={user}
                  currentLocation={location.pathname}
                />
              )}
            </StyledRightSection>
          </Grid>
        </Toolbar>
        <JobOutputDialog />
      </StyledHeader>
    </>
  );
};

const mapStateToProps = state => ({
  backButtonDestination: interfaceSelectors.getBackButtonDestination(state),
  backButtonType: interfaceSelectors.getBackButtonType(state),
  isAuthenticated: userSelectors.isAuthenticated(state),
  pageTitle: interfaceSelectors.getPageTitle(state),
  user: userSelectors.getUser(state),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      signOutAction: signOut,
    },
    dispatch
  );

HeaderComponent.propTypes = {
  backButtonDestination: PropTypes.string.isRequired,
  backButtonType: PropTypes.string.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  pageTitle: PropTypes.string.isRequired,
  signOutAction: PropTypes.func.isRequired,
  user: PropTypes.PropTypes.shape({
    email: PropTypes.string,
    firstName: PropTypes.string,
    lastName: PropTypes.string,
  }).isRequired,
};

export const Header = connect(
  mapStateToProps,
  mapDispatchToProps
)(HeaderComponent);
