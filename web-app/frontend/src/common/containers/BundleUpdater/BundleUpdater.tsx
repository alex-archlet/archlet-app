import React, { useState } from 'react';
import styled from 'styled-components';
import axios from 'axios';
import { Snackbar } from '@material-ui/core';
import { Alert } from '@material-ui/lab';

import { useCustomTranslation } from '@utils/translate';
import { useInterval } from '@common/hooks/useInterval';
import { isProdEnv } from 'src/config/config';

import { version } from '../../../../package.json';

const ClickableAlert = styled(Alert)`
  cursor: pointer;
`;

const BUNDLE_URL = '/release.txt';
// null interval duration means that the provided callback is not called
const POLL_FREQ = isProdEnv ? 5000 : null;

const onReload = () => {
  window.location.reload();
};

export const BundleUpdater = () => {
  const { t } = useCustomTranslation();

  const [showMessage, setShowMessage] = useState(false);

  useInterval(() => {
    const getCurrentVersion = async () => {
      try {
        const nonCachedUrl = `${BUNDLE_URL}?x=${Date.now()}`;
        const { data: backendVersion } = await axios.get(nonCachedUrl);

        if (backendVersion !== version) {
          setShowMessage(true);
        }
      } catch (error) {
        console.error(error);
      }
    };

    getCurrentVersion();
  }, POLL_FREQ);

  if (!showMessage) {
    return null;
  }

  return (
    <Snackbar
      open={showMessage}
      anchorOrigin={{
        horizontal: 'right',
        vertical: 'bottom',
      }}
    >
      <ClickableAlert
        onClick={onReload}
        severity="info"
      >
        <span>
          {`${t('The Archlet application has been updated.')} `}
        </span>
        <b>
          {t('Click here to get the newest version.')}
        </b>
      </ClickableAlert>
    </Snackbar>
  );
};
