import React from 'react';
import styled from 'styled-components';
import { i18n, useCustomTranslation } from '@utils/translate';
import { PrimaryButton } from '@common/components/Button/PrimaryButton';
import { Button } from '@common/components/Button/Button';

import logo from '../../../assets/logo_black.svg';

const StyledContainer = styled.div`
  position: relative;
  height: 100vh;
`;

const StyledErrorSection = styled.div`
  position: absolute;
  left: 50%;
  top: 50%;
  -webkit-transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
  max-width: 660px;
  width: 100%;
  padding-left: 220px;
  line-height: 1.1;
`;

const StyledIcon = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  display: inline-block;
  width: 140px;
  height: 140px;
  background-image: url(${logo});
  background-size: contain;
  background-repeat: no-repeat;
  background-position: center;

  &:before {
    content: '';
    position: absolute;
    width: 100%;
    height: 100%;
    -webkit-transform: scale(1.8);
    -ms-transform: scale(1.8);
    transform: scale(1.8);
    border-radius: 50%;
    background-color: #f2f5f8;
    z-index: -1;
  }
`;

const StyledButton = styled(PrimaryButton)`
  &.MuiButton-contained {
    margin-right: 8px;
`;

const StyledParagraph = styled.p`
  line-height: 1.5;
  font-size: 1.1em;
`;

interface Props {
  isResetPossible?: boolean;
  title?: string;
  message?: string;
  error?: Error;
  onGoToHome?: () => void;
  onReset?: () => void;
}

export const ErrorFallback: React.FC<Props> = ({
  title = i18n.t('An unexpected error occurred'),
  // @ts-ignore max-len
  message = i18n.t(
    'The app has ran into an unexpected error and Archlet team has been notified. Please try again and if the issue persists write us at '
  ),
  isResetPossible = true,
  onReset,
  onGoToHome,
}) => {
  const { t } = useCustomTranslation();

  return (
    <StyledContainer>
      <StyledErrorSection>
        <StyledIcon />
        <h1>{title}</h1>
        <StyledParagraph>
          {message}
          <a href="mailto:support@archlet.ch">support@archlet.ch</a>
        </StyledParagraph>
        {isResetPossible && (
          <>
            <StyledButton onClick={onReset}>{t('Try again')}</StyledButton>
            <Button onClick={onGoToHome}>{t('Back to home')}</Button>
          </>
        )}
      </StyledErrorSection>
    </StyledContainer>
  );
};
