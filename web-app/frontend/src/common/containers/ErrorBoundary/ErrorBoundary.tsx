import React from 'react';
import { ErrorBoundary as ErrorBoundaryWrapper } from 'react-error-boundary';
import { captureException } from '@sentry/react';
import { useHistory, useLocation } from 'react-router';

import { ErrorFallback } from './ErrorFallback';

const myErrorHandler = (error: Error, info: { componentStack: string }) => {
  captureException(error, {
    contexts: {
      // @ts-ignore
      componentStack: info.componentStack,
    },
  });
};

interface Props {
  children: React.ReactNode;
}

export const ErrorBoundary: React.FC<Props> = ({ children }) => {
  const location = useLocation();
  const history = useHistory();

  const onGoToHome = () => {
    history.push('/');
  };

  return (
    <ErrorBoundaryWrapper
      FallbackComponent={({ resetErrorBoundary }) => (
        <ErrorFallback
          onReset={resetErrorBoundary}
          onGoToHome={onGoToHome}
        />
      )}
      onError={myErrorHandler}
      resetKeys={[location.pathname]}
    >
      {children}
    </ErrorBoundaryWrapper>
  );
};
