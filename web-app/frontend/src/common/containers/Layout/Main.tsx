import React from 'react';
import styled from 'styled-components';
import { dimensions } from '@utils/uiTheme';

const StyledMain = styled.main`
  padding-top: ${dimensions.headerHeight}px;
  min-height: 100vh;
`;

interface Props {
  children: React.ReactNode,
}

export const MainLayout: React.FC<Props> = ({ children }) => (
  <StyledMain>{children}</StyledMain>
);
