import React from 'react';
import styled from 'styled-components';
import { colors } from '@utils/uiTheme';

const StyledBaseLayout = styled.div`
  background-color: ${colors.white};
`;

interface Props {
  children: React.ReactNode,
}

export const BaseLayout: React.FC<Props> = ({ children }) => (
  <StyledBaseLayout>{children}</StyledBaseLayout>
);
