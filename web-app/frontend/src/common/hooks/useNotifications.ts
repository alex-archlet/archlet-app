import { ErrorNotificationParams, NotificationParams, showNotificationError, showNotificationSuccess } from "@store/actions/notifications";
import { useDispatch } from "react-redux";

export const useNotifications = () => {
  const dispatch = useDispatch();  

  const showError = (params: ErrorNotificationParams) => {
    dispatch(showNotificationError(params));
  };
  const showSuccess = (params: NotificationParams) => {
    dispatch(showNotificationSuccess(params));
  };

  return {
    showError,
    showSuccess,
  };
};
