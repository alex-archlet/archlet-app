import { useEffect, useRef } from 'react';

export const useInterval = (callback: Function, delay: number | null) => {
  const savedCallback = useRef<Function>(callback);

  useEffect(
    () => {
      const handler = (...args: any) => savedCallback.current(...args);

      if (delay !== null) {
        const id = setInterval(handler, delay);

        return () => clearInterval(id);
      }

      return () => {};
    },
    [delay]
  );
};

