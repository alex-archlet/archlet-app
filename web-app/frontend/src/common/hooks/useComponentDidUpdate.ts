import { useRef, useEffect } from 'react';

/**
 * This hook is to be used as the componentDidUpdate lifecycle method on old class-based components.
 * Use case - don't call the callback with the first render, but only with subsequent ones.
 * @param callback 
 * @param deps 
 */
export const useComponentDidUpdate = (callback: Function, deps: any[]) => {
  const initialRender = useRef<boolean>(true);

  return useEffect(() => {
    if (initialRender.current) {
      initialRender.current = false;
      return () => {};
    }
    return callback();
  }, deps);
};
