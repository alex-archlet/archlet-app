import { interfaceSelectors } from "@store/selectors/interface";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import {
  useLocation,
} from 'react-router-dom';

export const useOnRouteChange = (action) => {
  const location = useLocation();
  const shouldHeaderRouteListener = useSelector(interfaceSelectors.shouldHeaderRouteListener);
  useEffect(() => {
    if (location.search === "" && shouldHeaderRouteListener) {
      action();
    }
  }, [location]);

}
