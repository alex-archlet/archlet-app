type WindowWithEnv = Window & typeof globalThis & {
  ARCHLET_ENV: object;
}

export const getEnvVariable = (key: string, fallback: any = undefined) => {
  return (window as WindowWithEnv)?.ARCHLET_ENV?.[key] ?? fallback;
};
