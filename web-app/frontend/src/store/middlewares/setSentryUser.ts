import { GET_ME_SUCCESS, SIGN_IN_SUCCESS, SIGN_OUT } from '@store/actions/user';
import { setUser } from '@sentry/react';

export const setSentryUser = (_: any) => (next: Function) => (action: any) => {
  switch (action.type) {
    case GET_ME_SUCCESS:
    case SIGN_IN_SUCCESS:
      setUser(action.payload);
      break;
    case SIGN_OUT:
      setUser(null);
      break;
    default:
  }

  return next(action);
};
