import { calculateGraphData } from '@utils/calculateGraphData';
import {
  ANALYTICS_ACTION_FORM_NAME,
  ANALYTICS_SUPPLIERS_FORM_NAME,
  SCENARIO_ITEM_DEEP_DIVE_OPPORTUNITY_FORM_NAME,
} from '@utils/formNames';
import { createSelector } from 'reselect';
import get from 'lodash.get';
import { arrayToMap } from '@utils/array';

const getScenarioAllocationsMap = state => state.projectScenarios.scenarioAllocationsMap;

const makeGetScenarioAllocations = scenarioId => createSelector(
  getScenarioAllocationsMap,
  map => scenarioId && (map[scenarioId] || null)
);

const getScenarios = state => state.projectScenarios.list;
const getScenariosMap = createSelector(
  getScenarios,
  list => arrayToMap(list)
);

const getScenarioDetails = state => state.projectScenarios.scenarioDetails;

const getScenarioKpiList = state => state.projectScenarios.scenarioKpis;

const getScenarioBidColumns = state => get(getScenarioDetails(state), ['bidColumns']);

const getScenarioDetailOutput = state => get(getScenarioDetails(state), [
  'output',
], []);

const getScenarioDetailOutputCapacities = state => get(getScenarioDetailOutput(state), [
  'capacities',
], []);

const getScenarioDetailOutputHasCapacities = createSelector(
  getScenarioDetailOutput,
  output => output && Boolean(output.capacities)
);

const getScenarioDetailOutputDiscounts = state => get(getScenarioDetailOutput(state), [
  'discounts',
], []);

const getScenarioDetailOutputHasDiscounts = createSelector(
  getScenarioDetailOutput,
  output => output && output.discounts && output.discounts.length > 0
);

export const projectScenariosSelectors = {
  getPostProcessing: state => state.projectScenarios.postProcessing,
  getScenarioAllocations: state => state.projectScenarios.scenarioAllocationsMap,
  getScenarioBidColumns,
  getScenarioDetailOutput,
  getScenarioDetailOutputCapacities,
  getScenarioDetailOutputDiscounts,
  getScenarioDetailOutputHasCapacities,
  getScenarioDetailOutputHasDiscounts,
  getScenarioDetails,
  getScenarioKpiList,
  getScenarioFormValues: state => get(state, [
    'form',
    'CreateScenarioForm',
    'values',
  ], {}),
  getScenarios,
  getScenariosMap,
  getScenariosWithGraphData: (state, form) => {
    const selectedRegion = get(state, [
      'form',
      form,
      'values',
      'item',
    ], 'all');

    return calculateGraphData(state.projectScenarios.list, selectedRegion);
  },
  getSelectedAction: state => get(state, [
    'form',
    ANALYTICS_ACTION_FORM_NAME,
    'values',
    'item',
  ], []),
  getSelectedOpportunity: state => get(state, [
    'form',
    SCENARIO_ITEM_DEEP_DIVE_OPPORTUNITY_FORM_NAME,
    'values',
    'item',
  ], []),
  getSelectedRegion: (state, form) => get(state, [
    'form',
    form,
    'values',
    'item',
  ], []),
  getSelectedSuppliers: state => get(state, [
    'form',
    ANALYTICS_SUPPLIERS_FORM_NAME,
    'values',
    'item',
  ], []),
  makeGetScenarioAllocations,
};
