import { PivotTableReducer } from '@store/reducers/pivotTable';

// TODO: Move to Root reducer once all files are converted
interface RootReducer {
  pivotTable: PivotTableReducer;
}

const getPivotTableMap = (state: RootReducer) => state.pivotTable.pivotTableMap;

export const pivotTableSelectors = {
  getPivotTableMap,
};
