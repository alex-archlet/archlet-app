export const userSelectors = {
  error: state => state.user.error,
  getUser: state => state.user.me,
  getUserPermissions: state => (state.user.me ? state.user.me.permissions : []),
  getUserFeatureFlagIds: state => (state.user.featureFlagIds ? state.user.featureFlagIds : []),
  isAdmin: state => state.user.me.accountType === 'admin',
  isAuthenticated: state => !!state.user.me.id,
  isFetching: state => state.user.isFetching,
  isReady: state => state.user.isReady,
  getLanguage: state => state.user.language,
  getLocaleLanguage: state => state.user.language.locale
};
