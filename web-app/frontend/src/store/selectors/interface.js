export const interfaceSelectors = {
  getBackButtonDestination: state => state.interface.backButtonDestination,
  getBackButtonType: state => state.interface.backButtonType,
  getPageTitle: state => state.interface.pageTitle,
  isSpinnerVisible: state => !!state.interface.showSpinner.length,
};
