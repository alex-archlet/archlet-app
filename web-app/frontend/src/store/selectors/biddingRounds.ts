import { createSelector } from 'reselect';
import { BiddingRoundsReducer } from '@store/reducers/biddingRounds';
import { nameIdDropdownOption } from '@common/components/Filter/filterUtils';

// TODO: Remove this definition when the reducer index file is TS
interface IRootReducer {
  biddingRounds: BiddingRoundsReducer;
}

// TODO: Define types in reducer and here when using/touching them

const getColumnTypes = state => state.biddingRounds.columnTypes;

const arrayToMap = (array) => {
  const result = {};

  array.forEach((item) => {
    result[item.id] = item.example;
  });

  return result;
};

const getColumnExamplesMap = createSelector(getColumnTypes, arrayToMap);
const getSupplierColumnType = createSelector(getColumnTypes, columnTypes =>
  columnTypes.find(({ type }) => type === 'supplier')
);

const getBidColumnNumbers = ['price', 'volume', 'currency', 'price_total'];

const getCalculatableColumnTypes = createSelector(getColumnTypes, columnTypes =>
  columnTypes.filter(col => getBidColumnNumbers.includes(col.type))
);

const getBiddingRoundColumns = state => state.biddingRounds.tableDesign;
const getRoundSupplierColumn = createSelector(
  [getBiddingRoundColumns, getSupplierColumnType],
  (columns, supplierColumnType) =>
    supplierColumnType &&
    columns.find(column => column.typeId === supplierColumnType.id)
);
const getCalculationColumns = createSelector(getBiddingRoundColumns, values =>
  values.filter(col => col.isAutomatic && col.inputBy === 'supplier')
);

const getRoundBidImportFilesMap = state =>
  state.biddingRounds.bidImportFilesMap;

const getRoundBidImportFiles = createSelector(getRoundBidImportFilesMap, map =>
  Object.values(map)
);

const getNonAutomaticColumns = createSelector(getBiddingRoundColumns, columns =>
  columns.filter(col => !col.isAutomatic)
);
const getColumnsForTableContent = createSelector(
  getNonAutomaticColumns,
  columns => columns.filter(column => column.inputBy === 'buyer')
);

const getColumnsForBuyerOptions = createSelector(
  getColumnsForTableContent,
  bidColumnsOption => bidColumnsOption.map(nameIdDropdownOption)
);

const hasNoBidColumns = createSelector(
  getNonAutomaticColumns,
  columns => columns.length
);

const getColumnsForTableDesign = createSelector(
  getNonAutomaticColumns,
  columns =>
    columns.filter(
      column => column.inputBy === 'buyer' || column.inputBy === 'supplier'
    )
);

const getColumnsForSupplier = createSelector(getNonAutomaticColumns, columns =>
  columns.filter(column => column.inputBy === 'supplier')
);

const getColumnsForSupplierOptions = createSelector(
  getColumnsForSupplier,
  bidColumnsOption => bidColumnsOption.map(nameIdDropdownOption)
);
const getNumericSupplierBidColumns = createSelector(
  getColumnsForSupplier,
  columns => columns.filter(column => column.isNumeric)
);

const getNumericColumnsForSupplierOptions = createSelector(
  getNumericSupplierBidColumns,
  bidColumnsOption => bidColumnsOption.map(nameIdDropdownOption)
);

const getColumnsForSupplierWithItem = createSelector(
  [getColumnsForSupplier, getColumnsForTableContent],
  (supplier, tableContent) => [
    ...tableContent.filter(content => content.column === 'item'),
    ...supplier,
  ]
);

const getBidColumnsOptions = createSelector(
  getColumnsForTableDesign,
  bidColumnsOption => bidColumnsOption.map(nameIdDropdownOption)
);

// TODO: we might want to remove this selector in future in case user wants to calc. diff between baseline and new price
// for some reason
const getColumnsForCostCalculation = createSelector(
  getBiddingRoundColumns,
  values =>
    values.filter(
      col =>
        col.inputBy === 'supplier' ||
        (!col.isAutomatic && col.inputBy === 'buyer')
    )
);
// get calculatable columns (being of a numbers type and not buyer-internal) to define cost
const getCalculableColumns = createSelector(
  [getColumnsForCostCalculation, getCalculatableColumnTypes],
  (columns, calcColumnTypes) =>
    columns.filter(column =>
      calcColumnTypes.map(a => a.id).includes(column.typeId)
    )
);
// TODO: verify that this is the correct way to check if there are offers
const getRoundHasOffers = createSelector(
  getRoundBidImportFiles,
  files => files.length > 0
);

const getRoundsList = (state: IRootReducer) => state.biddingRounds.list;

const getBiddingRoundListOrderByCreatedAt = createSelector(
  getRoundsList,
  rounds =>
    rounds
      .filter(round => round.statusName === 'Finished')
      .sort((aRound, bRound) => {
        const aDate = new Date(aRound.createdAt);
        const bData = new Date(bRound.createdAt);

        return aDate.getDate() - bData.getDate();
      })
);

export const biddingRoundsSelectors = {
  getBidRoundChangedStatus: state => state.biddingRounds.changed,
  getCalculableColumns,
  getCalculationColumns,
  getColumnExamplesMap,
  getColumnTypes,
  getColumnsForSupplier,
  getColumnsForSupplierWithItem,
  getColumnsForTableContent,
  getColumnsForTableDesign,
  getBiddingRoundListOrderByCreatedAt,
  getInvitedSuppliers: state => state.biddingRounds.invitedSuppliers,
  getRoundBidImportFiles,
  getRoundDetails: state => state.biddingRounds.details,
  getRoundDetailsName: state => state.biddingRounds.details.name,
  getRoundHasOffers,
  getRoundSettings: state => state.biddingRounds?.details?.settings,
  getRoundSupplierColumn,
  getRoundTableContent: state => state.biddingRounds.tableContent,
  getRoundTableDesign: state => state.biddingRounds.tableDesign,
  getRoundTemplates: state => state.biddingRounds.roundTemplates,
  getRoundsList,
  hasNoBidColumns,
  getNumericSupplierBidColumns,
  getColumnsForSupplierOptions,
  getNumericColumnsForSupplierOptions,
  getColumnsForBuyerOptions,
  getBidColumnsOptions,
  getTemplatesSuppliers: state => state.biddingRounds.templatesSuppliers,
};
