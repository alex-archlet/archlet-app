import { createSelector } from 'reselect';
import { userSelectors } from './user';

const PIVOT_TABLE_ID = 1;

const makeHasUserFeatureFlag = (flagId: number) =>
  createSelector(userSelectors.getUserFeatureFlagIds, featureFlagIds =>
    featureFlagIds.includes(flagId)
  );

const hasPivotTableFeatureFlag = makeHasUserFeatureFlag(PIVOT_TABLE_ID);

export const featureFlagSelectors = {
  hasPivotTableFeatureFlag,
};
