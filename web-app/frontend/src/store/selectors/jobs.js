export const jobsSelectors = {
  getGlobalJobId: state => state.jobs.globalJobId,
  getGlobalJobProgress: state => state.jobs.globalJobProgress,
  getIsGlobalJobRunning: state => state.jobs.isGlobalJobRunning,
  getJobOutput: state => state.jobs.jobOutput,
};
