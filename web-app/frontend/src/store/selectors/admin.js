export const adminSelectors = {
  branches: state => state.admin.branches,
  companies: state => state.admin.companies,
  roles: state => state.admin.roles,
  users: state => state.admin.users,
};
