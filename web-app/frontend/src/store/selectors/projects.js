import { toCamelCase } from 'case-converter';
import { createSelector } from 'reselect';
import get from 'lodash.get';
import { nameIdDropdownOption } from '@common/components/Filter/filterUtils';
import { biddingRoundsSelectors } from './biddingRounds';

const categoriesSelector = state => state.projects.categories;
const selectedTypeFilter = state =>
  get(state, ['form', 'CreateProjectForm', 'values', 'typeId']);
const filteredCategoriesSelector = createSelector(
  [selectedTypeFilter, categoriesSelector],
  (selectedType, categories) =>
    categories.filter(category => category.typeId === selectedType)
);

const getProjectSecondAxisKeys = state =>
  state.projects.projectInfo?.project_info?.second_axis || [];
const getCurrencies = state => state.projects.currencies;
const getBiddingTypes = state => state.projects.biddingTypes || [];
const getProjectDetails = state => state.projects.projectDetails;

const getProjectKpiDefs = state => state.projects.kpiDefs || [];
const getCustomProjectKpiDefs = createSelector(getProjectKpiDefs, defs =>
  defs.filter(def => !def.isDefault)
);

const getCurrentProjectCurrency = createSelector(
  getProjectDetails,
  getCurrencies,
  (project, currencies) => {
    if (!project || !currencies.length) {
      return null;
    }

    return (
      currencies.find(currency => currency.id === project.currencyId) || null
    );
  }
);

const getCurrentProjectCurrencyCode = createSelector(
  getCurrentProjectCurrency,
  currency => (currency ? currency.code : null)
);

const getUnits = state => state.projects.units;

const getCurrentProjectUnit = createSelector(
  getProjectDetails,
  getUnits,
  (project, units) => {
    if (!project || !units.length) {
      return null;
    }

    return units.find(unit => unit.id === project.unitId) || null;
  }
);

const getCurrentProjectSettings = createSelector(
  getProjectDetails,
  project => project?.settings ?? {}
);

const getCurrentProjectFilterColumnIds = createSelector(
  getCurrentProjectSettings,
  settings => settings?.filter?.map(({ id }) => id) ?? []
);

const getCurrentProjectId = createSelector(
  getProjectDetails,
  project => (project ? project.id : null)
);

const getCurrentProjectUnitName = createSelector(getCurrentProjectUnit, unit =>
  unit ? unit.name : null
);

const getProjectInfo = state => state.projects.projectInfo;
const getProjectInfoOutput = state =>
  get(getProjectInfo(state), ['project_info'], {});

const getProjectRegions = state =>
  get(getProjectInfoOutput(state), ['regions'], []);

const getProjectSuppliersPerRegions = state =>
  get(getProjectInfoOutput(state), ['suppliers_per_regions'], []);

const getProjectSuppliersPerItem = state =>
  get(getProjectInfoOutput(state), ['suppliers_per_item'], []);

const getProjectItems = state =>
  get(getProjectInfoOutput(state), ['items'], []);

const getProjectSuppliersPerItemAlternative = state =>
  get(getProjectInfoOutput(state), ['suppliers_per_item_alternative'], []);

const getProjectCriteria = state =>
  get(getProjectInfoOutput(state), ['criteria'], []);

const getProjectItemCategories = state => [
  ...new Set(
    get(getProjectInfoOutput(state), ['item_categories'], []).map(
      category => category.itemCategory
    )
  ),
];

const getProjectPriceComponents = state =>
  get(getProjectInfoOutput(state), ['price_components'], []);

const getProjectCapacities = state =>
  get(getProjectInfoOutput(state), ['capacities'], []);

const getProjectPriceComponentsStructure = state =>
  get(getProjectInfoOutput(state), ['price_components_structure'], []);

const arrayToMap = (array) => {
  const result = {};

  array.forEach((item) => {
    result[item.name] = item;
  });

  return result;
};

const flattenArray = (items) => {
  const result = [];

  items.forEach((item) => {
    result.push(item);

    if (item.children) {
      result.push(...flattenArray(item.children));
    }
  });

  return result;
};

const getProjectPriceComponentsStructureMap = createSelector(
  getProjectPriceComponentsStructure,
  structure => arrayToMap(flattenArray(structure))
);

const getProjectPriceComponentsRoot = createSelector(
  getProjectPriceComponentsStructure,
  structure => structure[0]
);

const getProjectSuppliers = state =>
  get(getProjectInfoOutput(state), ['suppliers'], []);

const getProjectCustomColumns = state =>
  get(getProjectInfoOutput(state), ['custom_columns'], []);

const getProjectHeatmap = state =>
  get(getProjectInfo(state), ['price_heatmap'], []);

const getProjectFilters = state =>
  get(getProjectInfoOutput(state), ['filters'], []);

const getProjectItemTypes = state =>
  get(getProjectInfoOutput(state), ['item_types'], []);

const getProjectVolumes = state =>
  get(getProjectInfoOutput(state), ['volumes'], []);

const getProjectFilterCombinations = state =>
  get(getProjectInfoOutput(state), ['filters_map'], {});

const getProjectHasHistoricPrices = state =>
  get(getProjectInfoOutput(state), ['has_historic_prices'], false);

const getProjectHasIncumbentPrices = state =>
  get(getProjectInfoOutput(state), ['has_incumbent_prices'], false);

const getSettings = state => state.projects.projectDetails.settings;

const getSettingsDemandFilters = createSelector(
  getSettings,
  settings => settings?.filters || []
);

const getSecondAxisSettings = createSelector(
  getSettings,
  settings => settings?.second_axis || []
);

const getSecondAxisBidColumns = createSelector(
  getSecondAxisSettings,
  biddingRoundsSelectors.getColumnsForTableDesign,
  (settingsFilter, bidColumns) =>
    settingsFilter
      .map(settingFilter =>
        bidColumns.find(bidColumn => bidColumn.id === settingFilter)
      )
      .filter(Boolean)
);

const getCustomColumnSettings = createSelector(
  getSettings,
  settings => settings?.custom_columns || []
);

const getCustomColumnsBidColumn = createSelector(
  getCustomColumnSettings,
  biddingRoundsSelectors.getColumnsForTableDesign,
  (settingsFilter, bidColumns) =>
    settingsFilter
      .map(settingFilter =>
        bidColumns.find(bidColumn => bidColumn.id === settingFilter)
      )
      .filter(Boolean)
);

const getSoftConstraintsSettings = createSelector(
  getSettings,
  settings => settings?.soft_constraints || []
);

const getSoftConstraintsBidColumn = createSelector(
  getSoftConstraintsSettings,
  biddingRoundsSelectors.getColumnsForTableDesign,
  (settingsFilter, bidColumns) =>
    settingsFilter
      .map(settingFilter =>
        bidColumns.find(bidColumn => bidColumn.id === settingFilter)
      )
      .filter(Boolean)
);

const getSettingOfferFilters = createSelector(
  getSettings,
  settings => settings?.offers_filters || []
);

const getSecondAxisOptions = createSelector(
  biddingRoundsSelectors.getColumnsForTableDesign,
  bidColumns => bidColumns.filter(bidColumn => bidColumn)
);

const getSettingFilters = createSelector(
  getSettingsDemandFilters,
  getSettingOfferFilters,
  (demandFilters, offerFilters) => [...offerFilters, ...demandFilters]
);

const getFilterDemandBidColumns = createSelector(
  getSettingsDemandFilters,
  biddingRoundsSelectors.getColumnsForTableDesign,
  (settingsFilter, bidColumns) =>
    settingsFilter
      .map(settingFilter =>
        bidColumns.find(bidColumn => bidColumn.id === settingFilter)
      )
      .filter(Boolean)
);

const getBidColumnsOptions = createSelector(
  biddingRoundsSelectors.getColumnsForTableDesign,
  bidColumnsOption => bidColumnsOption.map(nameIdDropdownOption)
);

const getFilterBidColumns = createSelector(
  getSettingFilters,
  biddingRoundsSelectors.getColumnsForTableDesign,
  (settingsFilter, bidColumns) =>
    settingsFilter
      .map(settingFilter =>
        bidColumns.find(bidColumn => bidColumn.id === settingFilter)
      )
      .filter(Boolean)
);

const getProjectHasItemTypes = createSelector(
  getProjectItemTypes,
  itemTypes => itemTypes.length > 1
);

export const projectsSelectors = {
  getCategories: filteredCategoriesSelector,
  getCurrencies,
  getProjectBiddingTypes: getBiddingTypes,
  getProjectCapacities,
  getProjectCriteria,
  getProjectCurrency: getCurrentProjectCurrency,
  getProjectCurrencyCode: getCurrentProjectCurrencyCode,
  getProjectCustomColumns,
  getProjectDetails: state => state.projects.projectDetails,
  getProjectFilterCombinations,
  getProjectFilters,
  getProjectFilterColumnIds: getCurrentProjectFilterColumnIds,
  getProjectKpiDefs,
  getCustomProjectKpiDefs,
  getProjectHasHistoricPrices,
  getProjectHasIncumbentPrices,
  getProjectHasItemTypes,
  getProjectHeatmap,
  getProjectId: getCurrentProjectId,
  getProjectInfo,
  getProjectItemCategories,
  getProjectItemTypes,
  getProjectItems,
  getProjectPriceComponents,
  getProjectPriceComponentsRoot,
  getProjectPriceComponentsStructureMap,
  getProjectRegions,
  getProjectSecondAxisKeys,
  getCurrentProjectUnit,
  getProjectSuppliers,
  getProjectSuppliersPerItem,
  getProjectSuppliersPerItemAlternative,
  getProjectSuppliersPerRegions,
  getProjectUnitName: getCurrentProjectUnitName,
  getProjectVolumes,
  getProjects: state => state.projects.list,
  getRegionTranslator: (state) => {
    const regions = [
      'Global',
      ...get(getProjectInfoOutput(state), ['regions'], []),
    ];
    const translator = {};

    regions.forEach((el) => {
      translator[el] = el;
    });

    return toCamelCase(translator);
  },
  getSettings,
  getSettingsDemandFilters,
  getFilterDemandBidColumns,
  getCustomColumnsBidColumn,
  getFilterBidColumns,
  getSecondAxisSettings,
  getSecondAxisBidColumns,
  getSecondAxisOptions,
  getSoftConstraintsBidColumn,
  getBidColumnsOptions,
  getTypes: state => state.projects.types,
  getUnits: state => state.projects.units,
};
