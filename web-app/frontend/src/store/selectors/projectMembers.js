export const projectMembersSelectors = {
  error: state => state.projectMembers.error,
  getProjectMembers: state => state.projectMembers.list,
};
