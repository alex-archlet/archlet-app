export const projectDataSelectors = {
  getData: state => state.projectData.data,
  getExampleData: state => state.projectData.exampleData,
  getFiles: state => state.projectData.files,
  getFilesTypes: state => state.projectData.filesTypes,
  getValidation: state => state.projectData.validation,
  getNewFilesUpload: state => state.projectData.isNewFilesUpload,
  hasProjectsFiles: state => Object.keys(state.projectData.files).length !== 0,
};
