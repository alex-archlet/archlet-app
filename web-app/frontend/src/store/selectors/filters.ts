import { FiltersReducer } from '@store/reducers/filters';

// TODO: Move to Root reducer once all files are converted
interface RootReducer {
  filters: FiltersReducer;
}

const getFiltersMap = (state: RootReducer) => state.filters.filterState;

export const filtersSelectors = {
  getFiltersMap,
};
