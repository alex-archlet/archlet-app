import { FilterState } from "@common/components/Filter/types";

const prefix = '[FILTERS]';

export const SET_FILTERS_STATE = `${prefix} SET_FILTERS_STATE`;

export const setFiltersState = (key: string, state: FilterState) => ({
  payload: {
    key,
    state,
  },
  type: SET_FILTERS_STATE,
})
