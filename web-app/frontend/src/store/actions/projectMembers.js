import request from '@utils/request';
import { showNotificationError } from '@store/actions/notifications';
import { i18n } from '@utils/translate';

const prefix = '[MEMBERS]';

export const GET_PROJECT_MEMBERS_REQUEST = `${prefix} GET_PROJECT_MEMBERS_REQUEST`;
export const GET_PROJECT_MEMBERS_SUCCESS = `${prefix} GET_PROJECT_MEMBERS_SUCCESS`;
export const GET_PROJECT_MEMBERS_FAILURE = `${prefix} GET_PROJECT_MEMBERS_FAILURE`;

const getProjectMembersRequest = () => ({ type: GET_PROJECT_MEMBERS_REQUEST });
const getProjectMembersSuccess = payload => ({
  payload,
  type: GET_PROJECT_MEMBERS_SUCCESS,
});
const getProjectMembersFailure = payload => ({
  payload,
  type: GET_PROJECT_MEMBERS_FAILURE,
});

export const getProjectMembers = projectId => (dispatch) => {
  dispatch(getProjectMembersRequest());

  return request
    .get(`projects/${projectId}/members`)
    .then(({ data }) => {
      dispatch(getProjectMembersSuccess(data));

      return data;
    })
    .catch((error) => {
      dispatch(getProjectMembersFailure(error.response.data.message));
      dispatch(
        showNotificationError({
          title: i18n.t('Project members loading failed'),
          error,
        })
      );

      throw error;
    });
};

export const DELETE_PROJECT_MEMBER_REQUEST = `${prefix} DELETE_PROJECT_MEMBER_REQUEST`;
export const DELETE_PROJECT_MEMBER_SUCCESS = `${prefix} DELETE_PROJECT_MEMBER_SUCCESS`;
export const DELETE_PROJECT_MEMBER_FAILURE = `${prefix} DELETE_PROJECT_MEMBER_FAILURE`;

const deleteProjectMemberRequest = () => ({
  type: DELETE_PROJECT_MEMBER_REQUEST,
});
const deleteProjectMemberSuccess = payload => ({
  payload,
  type: DELETE_PROJECT_MEMBER_SUCCESS,
});
const deleteProjectMemberFailure = payload => ({
  payload,
  type: DELETE_PROJECT_MEMBER_FAILURE,
});

export const deleteProjectMember = payload => (dispatch) => {
  dispatch(deleteProjectMemberRequest());

  return request
    .delete('members', { data: payload })
    .then(({ data }) => {
      dispatch(deleteProjectMemberSuccess(payload.ids));

      return data;
    })
    .catch((error) => {
      dispatch(deleteProjectMemberFailure(error.response.data.message));
      dispatch(
        showNotificationError({
          title: i18n.t('It is not possible to remove member'),
          error,
        })
      );

      throw error;
    });
};

export const UPDATE_PROJECT_MEMBER_REQUEST = `${prefix} UPDATE_PROJECT_MEMBER_REQUEST`;
export const UPDATE_PROJECT_MEMBER_SUCCESS = `${prefix} UPDATE_PROJECT_MEMBER_SUCCESS`;
export const UPDATE_PROJECT_MEMBER_FAILURE = `${prefix} UPDATE_PROJECT_MEMBER_FAILURE`;

const updateProjectMemberRequest = () => ({
  type: UPDATE_PROJECT_MEMBER_REQUEST,
});
const updateProjectMemberSuccess = payload => ({
  payload,
  type: UPDATE_PROJECT_MEMBER_SUCCESS,
});

export const updateProjectMember = payload => (dispatch) => {
  dispatch(updateProjectMemberRequest());

  const parsedPayload = Object.keys(payload).map(key => ({
    accessType: payload[key],
    id: key,
  }));

  dispatch(updateProjectMemberSuccess(parsedPayload));
};

export const ADD_PROJECT_MEMBERS_REQUEST = `${prefix} ADD_PROJECT_MEMBERS_REQUEST`;
export const ADD_PROJECT_MEMBERS_SUCCESS = `${prefix} ADD_PROJECT_MEMBERS_SUCCESS`;
export const ADD_PROJECT_MEMBERS_FAILURE = `${prefix} ADD_PROJECT_MEMBERS_FAILURE`;

const addProjectMembersRequest = () => ({ type: ADD_PROJECT_MEMBERS_REQUEST });
const addProjectMembersSuccess = payload => ({
  payload,
  type: ADD_PROJECT_MEMBERS_SUCCESS,
});
const addProjectMembersFailure = payload => ({
  payload,
  type: ADD_PROJECT_MEMBERS_FAILURE,
});

export const addProjectMembers = payload => (dispatch) => {
  dispatch(addProjectMembersRequest());

  return request
    .post('members', payload)
    .then(({ data }) => {
      const invitedMembers = data.filter(member => !member.message);

      dispatch(addProjectMembersSuccess(invitedMembers));

      return data;
    })
    .catch((error) => {
      dispatch(addProjectMembersFailure(error.response.data.message));
      dispatch(
        showNotificationError({
          title: i18n.t('It is not possible to add member'),
          error,
        })
      );

      throw error;
    });
};
