import {
  toggleSidebar, openSidebar,
  TOGGLE, OPEN
} from '../sidebar';

describe('settings action', () => {
  it('should have a type of TOGGLE', () => {
    expect(toggleSidebar().type).toEqual(TOGGLE);
  });
  
  it('should have a type of OPEN', () => {
    expect(openSidebar().type).toEqual(OPEN);
  });
});