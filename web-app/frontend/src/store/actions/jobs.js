import request from '@utils/request';
import { CustomError } from '@utils/error';

import { jobsSelectors } from '../selectors/jobs';
import { projectsSelectors } from '../selectors/projects';

const prefix = '[JOBS]';

export const SET_JOB_OUTPUT = `${prefix} SET_JOB_OUTPUT`;

export const BLOCKING_JOB_REQUEST = `${prefix} BLOCKING_JOB_REQUEST`;
export const BLOCKING_JOB_SUCCESS = `${prefix} BLOCKING_JOB_SUCCESS`;
export const BLOCKING_JOB_ERROR = `${prefix} BLOCKING_JOB_ERROR`;
export const BLOCKING_JOB_PROGRESS = `${prefix} BLOCKING_JOB_PROGRESS`;
export const BLOCKING_JOB_CLOSE = `${prefix} BLOCKING_JOB_CLOSE`;
export const BLOCKING_JOB_SET_ID = `${prefix} BLOCKING_JOB_SET_ID`;

export const blockingJobRequest = () => ({ type: BLOCKING_JOB_REQUEST });
export const blockingJobSuccess = () => ({ type: BLOCKING_JOB_SUCCESS });
export const blockingJobFailure = () => ({ type: BLOCKING_JOB_ERROR });
const blockingJobClose = () => ({ type: BLOCKING_JOB_CLOSE });

export const blockingJobProgress = payload => ({
  payload,
  type: BLOCKING_JOB_PROGRESS,
});

export const blockingJobSetId = payload => ({
  payload,
  type: BLOCKING_JOB_SET_ID,
});

export const setJobOutput = payload => ({
  payload,
  type: SET_JOB_OUTPUT,
});

const cancelCurrentJob = async (projectId, jobId) => {
  const result = await request.delete(`jobs/project/${projectId}/${jobId}`, {
    loader: false,
  });

  return result;
};

export const blockingJobCloseAndCancel = () => async (dispatch, getState) => {
  try {
    const jobId = jobsSelectors.getGlobalJobId(getState());
    const projectId = projectsSelectors.getProjectId(getState());

    await cancelCurrentJob(projectId, jobId);
  } catch (err) {
    dispatch(blockingJobClose());
  } finally {
    dispatch(blockingJobClose());
  }
};

const POLL_TIMEOUT = 1000;
const ABORTED_STATUS_ID = 4;
const SUCCESS_STATUS_ID = 5;

const fetchJob = async (projectId, jobId) => {
  const result = await request.get(`jobs/project/${projectId}/${jobId}`, {
    loader: false,
  });

  return result;
};

const asyncSleep = time => new Promise(resolve => setTimeout(resolve, time));

const emptyFunction = () => {};

const isGlobalJobRunning = () => {
  const { store } = window;

  if (store) {
    return jobsSelectors.getIsGlobalJobRunning(store.getState());
  }

  return true;
};

export class CancelledJobError extends CustomError {}

export const pollJobUntilFinished = async (
  projectId,
  jobId,
  onUpdateProgress = emptyFunction
) => {
  // eslint-disable-next-line no-constant-condition
  while (true) {
    // check to break if modal closed
    if (!isGlobalJobRunning()) {
      throw new CancelledJobError('The job has been cancelled');
    }

    // eslint-disable-next-line no-await-in-loop
    await asyncSleep(POLL_TIMEOUT);
    // eslint-disable-next-line no-await-in-loop
    const jobResultRequest = await fetchJob(projectId, jobId);
    const jobResult = jobResultRequest.data;

    onUpdateProgress(jobResult.progress);
    const isFinished = jobResult.statusId === SUCCESS_STATUS_ID;

    if (isFinished) {
      return jobResultRequest;
    }

    const isAborted = jobResult.statusId === ABORTED_STATUS_ID;

    if (isAborted) {
      throw new CustomError(
        'The job has been aborted',
        jobResult.output.messages.filter(
          message =>
            message.severity === 'error' || message.severity === 'warning'
        )
      );
    }
  }
};
