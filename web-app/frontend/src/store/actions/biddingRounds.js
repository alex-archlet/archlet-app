import { SubmissionError } from 'redux-form';
import request from '@utils/request';
import { getFieldErrors } from '@utils/helpers';
import { getErrorMessage } from '@utils/error';
import { i18n } from '@utils/translate';

import {
  showNotificationError,
  showNotificationSuccess,
} from './notifications';
import {
  blockingJobFailure,
  blockingJobProgress,
  blockingJobRequest,
  blockingJobSuccess,
  blockingJobSetId,
  pollJobUntilFinished,
  setJobOutput,
} from './jobs';

const prefix = '[BIDDING]';

export const GET_ROUNDS_LIST_REQUEST = `${prefix} GET_ROUNDS_LIST_REQUEST`;
export const GET_ROUNDS_LIST_SUCCESS = `${prefix} GET_ROUNDS_LIST_SUCCESS`;
export const GET_ROUNDS_LIST_FAILURE = `${prefix} GET_ROUNDS_LIST_FAILURE`;

const getRoundsListRequest = () => ({ type: GET_ROUNDS_LIST_REQUEST });
const getRoundsListSuccess = payload => ({
  payload,
  type: GET_ROUNDS_LIST_SUCCESS,
});
const getRoundsListFailure = payload => ({
  payload,
  type: GET_ROUNDS_LIST_FAILURE,
});

export const getRoundsList = projectId => (dispatch) => {
  dispatch(getRoundsListRequest());

  return request
    .get(`projects/${projectId}/bidding_rounds`)
    .then(({ data }) => {
      dispatch(getRoundsListSuccess(data));

      return data;
    })
    .catch(({ response: { data } }) => {
      dispatch(getRoundsListFailure(data.message));

      throw data;
    });
};

export const GET_ROUND_DETAILS_REQUEST = `${prefix} GET_ROUND_DETAILS_REQUEST`;
export const GET_ROUND_DETAILS_SUCCESS = `${prefix} GET_ROUND_DETAILS_SUCCESS`;
export const GET_ROUND_DETAILS_FAILURE = `${prefix} GET_ROUND_DETAILS_FAILURE`;

const getRoundDetailsRequest = () => ({ type: GET_ROUND_DETAILS_REQUEST });
const getRoundDetailsSuccess = payload => ({
  payload,
  type: GET_ROUND_DETAILS_SUCCESS,
});
const getRoundDetailsFailure = payload => ({
  payload,
  type: GET_ROUND_DETAILS_FAILURE,
});

export const getRoundDetails = (projectId, roundId) => (dispatch) => {
  dispatch(getRoundDetailsRequest());

  return request
    .get(`projects/${projectId}/bidding_rounds/${roundId}`)
    .then(({ data }) => {
      dispatch(getRoundDetailsSuccess(data));

      return data;
    })
    .catch(({ response: { data } }) => {
      dispatch(getRoundDetailsFailure(data.message));

      throw data;
    });
};

export const CREATE_ROUND_REQUEST = `${prefix} CREATE_ROUND_REQUEST`;
export const CREATE_ROUND_SUCCESS = `${prefix} CREATE_ROUND_SUCCESS`;
export const CREATE_ROUND_FAILURE = `${prefix} CREATE_ROUND_FAILURE`;

const createRoundRequest = () => ({ type: CREATE_ROUND_REQUEST });
const createRoundSuccess = payload => ({
  payload,
  type: CREATE_ROUND_SUCCESS,
});
const createRoundFailure = payload => ({
  payload,
  type: CREATE_ROUND_FAILURE,
});

export const createRound = (projectId, payload) => (dispatch) => {
  dispatch(createRoundRequest());

  return request
    .post(`projects/${projectId}/bidding_rounds`, payload)
    .then(({ data }) => {
      dispatch(createRoundSuccess(data));

      return data;
    })
    .catch((error) => {
      dispatch(createRoundFailure(error.response.data.message));

      dispatch(
        showNotificationError({
          title: i18n.t('Unable to create new bidding round'),
          error,
        })
      );

      throw error;
    });
};

export const UPDATE_BIDDING_SETTINGS_REQUEST = `${prefix} UPDATE_BIDDING_SETTINGS_REQUEST`;
export const UPDATE_BIDDING_SETTINGS_SUCCESS = `${prefix} UPDATE_BIDDING_SETTINGS_SUCCESS`;
export const UPDATE_BIDDING_SETTINGS_FAILURE = `${prefix} UPDATE_BIDDING_SETTINGS_FAILURE`;

const updateBiddingSettingsRequest = () => ({
  type: UPDATE_BIDDING_SETTINGS_REQUEST,
});
const updateBiddingSettingsSuccess = payload => ({
  payload,
  type: UPDATE_BIDDING_SETTINGS_SUCCESS,
});
const updateBiddingSettingsFailure = payload => ({
  payload,
  type: UPDATE_BIDDING_SETTINGS_FAILURE,
});

export const updateBiddingSettings = (
  projectId,
  roundId,
  payload
) => async (dispatch) => {
  dispatch(updateBiddingSettingsRequest());

  return request
    .patch(
      `projects/${projectId}/bidding_rounds/${roundId}/bidding_settings`,
      payload
    )
    .then(({ data }) => {
      dispatch(updateBiddingSettingsSuccess(data));

      return data;
    })
    .catch(({ response: { data } }) => {
      dispatch(updateBiddingSettingsFailure(data));

      throw new SubmissionError({
        ...getFieldErrors(data),
        _error: data.message || getErrorMessage(),
      });
    });
};

export const UPDATE_ROUND_REQUEST = `${prefix} UPDATE_ROUND_REQUEST`;
export const UPDATE_ROUND_SUCCESS = `${prefix} UPDATE_ROUND_SUCCESS`;
export const UPDATE_ROUND_FAILURE = `${prefix} UPDATE_ROUND_FAILURE`;

const updateRoundRequest = () => ({ type: UPDATE_ROUND_REQUEST });
const updateRoundSuccess = payload => ({
  payload,
  type: UPDATE_ROUND_SUCCESS,
});
const updateRoundFailure = payload => ({
  payload,
  type: UPDATE_ROUND_FAILURE,
});

export const updateRound = (projectId, roundId, payload) => (dispatch) => {
  dispatch(updateRoundRequest());

  return request
    .patch(`projects/${projectId}/bidding_rounds/${roundId}`, payload)
    .then(({ data }) => {
      dispatch(updateRoundSuccess(data));

      return data;
    })
    .catch(({ response: { data } }) => {
      dispatch(updateRoundFailure(data));

      throw new SubmissionError({
        ...getFieldErrors(data),
        _error: data.message || getErrorMessage(),
      });
    });
};

export const DELETE_ROUND_REQUEST = `${prefix} DELETE_ROUND_REQUEST`;
export const DELETE_ROUND_SUCCESS = `${prefix} DELETE_ROUND_SUCCESS`;
export const DELETE_ROUND_FAILURE = `${prefix} DELETE_ROUND_FAILURE`;

const deleteRoundRequest = () => ({ type: DELETE_ROUND_REQUEST });
const deleteRoundSuccess = payload => ({
  payload,
  type: DELETE_ROUND_SUCCESS,
});
const deleteRoundFailure = payload => ({
  payload,
  type: DELETE_ROUND_FAILURE,
});

export const deleteRound = (projectId, roundId) => (dispatch) => {
  dispatch(deleteRoundRequest());

  return request
    .delete(`projects/${projectId}/bidding_rounds/${roundId}`)
    .then(({ data }) => {
      dispatch(deleteRoundSuccess(data));
      dispatch(
        showNotificationSuccess({
          title: i18n.t('Bidding round deleted'),
        })
      );
      return data;
    })
    .catch((error) => {
      dispatch(deleteRoundFailure(error.response.data.message));
      dispatch(
        showNotificationError({
          title: i18n.t('Bidding round deleting failed'),
          error,
        })
      );
      throw error;
    });
};

export const GET_ROUND_TEMPLATES_REQUEST = `${prefix} GET_ROUND_TEMPLATES_REQUEST`;
export const GET_ROUND_TEMPLATES_SUCCESS = `${prefix} GET_ROUND_TEMPLATES_SUCCESS`;
export const GET_ROUND_TEMPLATES_FAILURE = `${prefix} GET_ROUND_TEMPLATES_FAILURE`;

const getRoundTemplatesRequest = () => ({ type: GET_ROUND_TEMPLATES_REQUEST });
const getRoundTemplatesSuccess = payload => ({
  payload,
  type: GET_ROUND_TEMPLATES_SUCCESS,
});
const getRoundTemplatesFailure = payload => ({
  payload,
  type: GET_ROUND_TEMPLATES_FAILURE,
});

export const getRoundTemplates = () => (dispatch) => {
  dispatch(getRoundTemplatesRequest());

  return request
    .get('bidding_rounds/templates')
    .then(({ data }) => {
      dispatch(getRoundTemplatesSuccess(data));

      return data;
    })
    .catch(({ response: { data } }) => {
      dispatch(getRoundTemplatesFailure(data.message));

      throw data;
    });
};

export const GET_TABLE_DESIGN_REQUEST = `${prefix} GET_TABLE_DESIGN_REQUEST`;
export const GET_TABLE_DESIGN_SUCCESS = `${prefix} GET_TABLE_DESIGN_SUCCESS`;
export const GET_TABLE_DESIGN_FAILURE = `${prefix} GET_TABLE_DESIGN_FAILURE`;

const getRoundTableDesignRequest = () => ({ type: GET_TABLE_DESIGN_REQUEST });
const getRoundTableDesignSuccess = payload => ({
  payload,
  type: GET_TABLE_DESIGN_SUCCESS,
});
const getRoundTableDesignFailure = payload => ({
  payload,
  type: GET_TABLE_DESIGN_FAILURE,
});

export const getRoundTableDesign = projectId => (dispatch) => {
  dispatch(getRoundTableDesignRequest());

  return request
    .get(`projects/${projectId}/table_design`)
    .then(({ data }) => {
      dispatch(getRoundTableDesignSuccess(data));

      return data;
    })
    .catch(({ response: { data } }) => {
      dispatch(getRoundTableDesignFailure(data.message));

      throw data;
    });
};

export const GET_COLUMN_TYPES_REQUEST = `${prefix} GET_COLUMN_TYPES_REQUEST`;
export const GET_COLUMN_TYPES_SUCCESS = `${prefix} GET_COLUMN_TYPES_SUCCESS`;
export const GET_COLUMN_TYPES_FAILURE = `${prefix} GET_COLUMN_TYPES_FAILURE`;

const getColumnTypesRequest = () => ({ type: GET_COLUMN_TYPES_REQUEST });
const getColumnTypesSuccess = payload => ({
  payload,
  type: GET_COLUMN_TYPES_SUCCESS,
});
const getColumnTypesFailure = payload => ({
  payload,
  type: GET_COLUMN_TYPES_FAILURE,
});

export const getColumnTypes = () => (dispatch) => {
  dispatch(getColumnTypesRequest());

  return request
    .get('bidding_rounds/column_types')
    .then(({ data }) => {
      dispatch(getColumnTypesSuccess(data));

      return data;
    })
    .catch(({ response: { data } }) => {
      dispatch(getColumnTypesFailure(data.message));

      throw data;
    });
};

export const SET_COLUMNS = `${prefix} SET_COLUMNS`;

export const setColumns = payload => ({
  payload,
  type: SET_COLUMNS,
});

export const UPDATE_TABLE_DESIGN_REQUEST = `${prefix} UPDATE_TABLE_DESIGN_REQUEST`;
export const UPDATE_TABLE_DESIGN_SUCCESS = `${prefix} UPDATE_TABLE_DESIGN_SUCCESS`;
export const UPDATE_TABLE_DESIGN_FAILURE = `${prefix} UPDATE_TABLE_DESIGN_FAILURE`;

const updateRoundTableDesignRequest = () => ({
  type: UPDATE_TABLE_DESIGN_REQUEST,
});

const updateRoundTableDesignSuccess = payload => ({
  payload,
  type: UPDATE_TABLE_DESIGN_SUCCESS,
});
const updateRoundTableDesignFailure = payload => ({
  payload,
  type: UPDATE_TABLE_DESIGN_FAILURE,
});

export const updateRoundTableDesign = (projectId, payload) => (dispatch) => {
  dispatch(updateRoundTableDesignRequest());

  return request
    .put(`projects/${projectId}/table_design`, payload)
    .then(({ data }) => {
      dispatch(updateRoundTableDesignSuccess(data));
      dispatch(
        showNotificationSuccess({
          title: i18n.t('Bidding round template updated'),
        })
      );

      return data;
    })
    .catch((error) => {
      dispatch(
        showNotificationError({
          title: i18n.t('Bidding round template updating failed'),
          error,
        })
      );
      dispatch(updateRoundTableDesignFailure(error.response.data.message));

      throw error;
    });
};

export const DELETE_COLUMN_REQUEST = `${prefix} DELETE_COLUMN_REQUEST`;
export const DELETE_COLUMN_SUCCESS = `${prefix} DELETE_COLUMN_SUCCESS`;
export const DELETE_COLUMN_FAILURE = `${prefix} DELETE_COLUMN_FAILURE`;

const deleteColumnRequest = () => ({ type: DELETE_COLUMN_REQUEST });
const deleteColumnSuccess = payload => ({
  payload,
  type: DELETE_COLUMN_SUCCESS,
});
const deleteColumnFailure = payload => ({
  payload,
  type: DELETE_COLUMN_FAILURE,
});

export const deleteColumn = (projectId, columnId) => (dispatch) => {
  dispatch(deleteColumnRequest());

  return request
    .delete(`projects/${projectId}/table_design/${columnId}`)
    .then(({ data }) => {
      dispatch(deleteColumnSuccess(columnId));

      return data;
    })
    .catch(({ response: { data } }) => {
      dispatch(deleteColumnFailure(data));

      throw data;
    });
};

export const SET_GRID = `${prefix} SET_GRID`;

export const setGrid = payload => ({
  payload,
  type: SET_GRID,
});

export const SET_BID_SETTINGS = `${prefix} SET_BID_SETTINGS`;

export const setBidSettings = payload => ({
  payload,
  type: SET_BID_SETTINGS,
});

export const GET_TABLE_CONTENT_REQUEST = `${prefix} GET_TABLE_CONTENT_REQUEST`;
export const GET_TABLE_CONTENT_SUCCESS = `${prefix} GET_TABLE_CONTENT_SUCCESS`;
export const GET_TABLE_CONTENT_FAILURE = `${prefix} GET_TABLE_CONTENT_FAILURE`;

const getRoundTableContentRequest = () => ({ type: GET_TABLE_CONTENT_REQUEST });
const getRoundTableContentSuccess = payload => ({
  payload,
  type: GET_TABLE_CONTENT_SUCCESS,
});
const getRoundTableContentFailure = payload => ({
  payload,
  type: GET_TABLE_CONTENT_FAILURE,
});

export const getRoundTableContent = (
  projectId,
  roundId,
  projectUnits,
  excelData
) => (dispatch) => {
  dispatch(getRoundTableContentRequest());

  return request
    .get(`projects/${projectId}/bidding_rounds/${roundId}/table_content`)
    .then(({ data }) => {
      dispatch(
        getRoundTableContentSuccess({
          data,
          excelData,
          projectUnits,
        })
      );

      return data;
    })
    .catch(({ response: { data } }) => {
      dispatch(getRoundTableContentFailure(data.message));

      throw data;
    });
};

export const CREATE_TABLE_CONTENT_REQUEST = `${prefix} CREATE_TABLE_CONTENT_REQUEST`;
export const CREATE_TABLE_CONTENT_SUCCESS = `${prefix} CREATE_TABLE_CONTENT_SUCCESS`;
export const CREATE_TABLE_CONTENT_FAILURE = `${prefix} CREATE_TABLE_CONTENT_FAILURE`;

const createRoundTableContentRequest = () => ({
  type: CREATE_TABLE_CONTENT_REQUEST,
});

const createRoundTableContentSuccess = payload => ({
  payload,
  type: CREATE_TABLE_CONTENT_SUCCESS,
});
const createRoundTableContentFailure = payload => ({
  payload,
  type: CREATE_TABLE_CONTENT_FAILURE,
});

export const createRoundTableContent = (
  projectId,
  roundId,
  payload = []
) => (dispatch) => {
  dispatch(createRoundTableContentRequest());

  return request
    .post(
      `projects/${projectId}/bidding_rounds/${roundId}/table_content`,
      payload,
      { noSnakeCase: true }
    )
    .then(({ data }) => {
      dispatch(createRoundTableContentSuccess(data));

      return data;
    })
    .catch(({ response: { data } }) => {
      dispatch(createRoundTableContentFailure(data));

      throw data;
    });
};

export const CREATE_DEMANDS_OFFERS_REQUEST = `${prefix} CREATE_DEMANDS_OFFERS_REQUEST`;
export const CREATE_DEMANDS_OFFERS_SUCCESS = `${prefix} CREATE_DEMANDS_OFFERS_SUCCESS`;
export const CREATE_DEMANDS_OFFERS_FAILURE = `${prefix} CREATE_DEMANDS_OFFERS_FAILURE`;

const createDemandsOffersRequest = () => ({
  type: CREATE_DEMANDS_OFFERS_REQUEST,
});

const createDemandsOffersSuccess = payload => ({
  payload,
  type: CREATE_DEMANDS_OFFERS_SUCCESS,
});
const createDemandsOffersFailure = payload => ({
  payload,
  type: CREATE_DEMANDS_OFFERS_FAILURE,
});

export const createDemandsOffers = (projectId, roundId, grid) => (dispatch) => {
  dispatch(createDemandsOffersRequest());

  return request
    .post(
      `projects/${projectId}/bidding_rounds/${roundId}/save_demands_offers`,
      grid,
      { noSnakeCase: true }
    )
    .then(({ data }) => {
      dispatch(
        createDemandsOffersSuccess({
          data,
        })
      );
      dispatch(
        showNotificationSuccess({
          title: i18n.t('Demands created successfully'),
        })
      );

      return data;
    })
    .catch((error) => {
      dispatch(
        showNotificationError({
          title: i18n.t('Demands creation failed'),
          error,
        })
      );
      dispatch(createDemandsOffersFailure(error.response.data.message));

      throw error;
    });
};

export const GET_DEMANDS_OFFERS_REQUEST = `${prefix} GET_DEMANDS_OFFERS_REQUEST`;
export const GET_DEMANDS_OFFERS_SUCCESS = `${prefix} GET_DEMANDS_OFFERS_SUCCESS`;
export const GET_DEMANDS_OFFERS_FAILURE = `${prefix} GET_DEMANDS_OFFERS_FAILURE`;

const getDemandsOffersRequest = () => ({ type: GET_DEMANDS_OFFERS_REQUEST });

const getDemandsOffersSuccess = payload => ({
  payload,
  type: GET_DEMANDS_OFFERS_SUCCESS,
});
const getDemandsOffersFailure = payload => ({
  payload,
  type: GET_DEMANDS_OFFERS_FAILURE,
});

export const getDemandsOffers = (
  projectId,
  roundId,
  headerData,
  excelData,
  projectUnits,
  projectCurrencies,
  payload
) => (dispatch) => {
  dispatch(getDemandsOffersRequest());

  return request
    .get(
      `projects/${projectId}/bidding_rounds/${roundId}/get_demands_offers`,
      payload,
      { noSnakeCase: true }
    )
    .then(({ data }) => {
      dispatch(
        getDemandsOffersSuccess({
          data,
          excelData,
          headerData,
          projectCurrencies,
          projectUnits,
        })
      );

      return data;
    })
    .catch((error) => {
      dispatch(getDemandsOffersFailure(error));

      throw error;
    });
};

export const CREATE_OR_UPDATE_COST_CALCULATION_REQUEST = `${prefix} CREATE_OR_UPDATE_COST_CALCULATION_REQUEST`;
export const CREATE_OR_UPDATE_COST_CALCULATION_SUCCESS = `${prefix} CREATE_OR_UPDATE_COST_CALCULATION_SUCCESS`;
export const CREATE_OR_UPDATE_COST_CALCULATION_FAILURE = `${prefix} CREATE_OR_UPDATE_COST_CALCULATION_FAILURE`;

const createOrUpdateCostCalculationRequest = () => ({
  type: CREATE_OR_UPDATE_COST_CALCULATION_REQUEST,
});

const createOrUpdateCostCalculationSuccess = payload => ({
  payload,
  type: CREATE_OR_UPDATE_COST_CALCULATION_SUCCESS,
});
const createOrUpdateCostCalculationFailure = payload => ({
  payload,
  type: CREATE_OR_UPDATE_COST_CALCULATION_FAILURE,
});

export const createOrUpdateCostCalculation = (
  projectId,
  roundId,
  calculations
) => (dispatch) => {
  dispatch(createOrUpdateCostCalculationRequest());

  return request
    .put(`projects/${projectId}/bidding_rounds/${roundId}/calculation`, {
      calculations,
    })
    .then(({ data }) => {
      dispatch(createOrUpdateCostCalculationSuccess(data));
      dispatch(
        showNotificationSuccess({
          title: i18n.t('Cost calculation updated'),
        })
      );

      return data;
    })
    .catch((error) => {
      dispatch(
        showNotificationError({
          title: i18n.t('Cost calculation updating failed'),
          error,
        })
      );

      dispatch(createOrUpdateCostCalculationFailure(error));

      throw error;
    });
};

export const GET_INVITED_SUPPLIERS_LIST_REQUEST = `${prefix} GET_INVITED_SUPPLIERS_LIST_REQUEST`;
export const GET_INVITED_SUPPLIERS_LIST_SUCCESS = `${prefix} GET_INVITED_SUPPLIERS_LIST_SUCCESS`;
export const GET_INVITED_SUPPLIERS_LIST_FAILURE = `${prefix} GET_INVITED_SUPPLIERS_LIST_FAILURE`;

const getInvitedSuppliersListRequest = () => ({
  type: GET_INVITED_SUPPLIERS_LIST_REQUEST,
});
const getInvitedSuppliersListSuccess = payload => ({
  payload,
  type: GET_INVITED_SUPPLIERS_LIST_SUCCESS,
});
const getInvitedSuppliersListFailure = payload => ({
  payload,
  type: GET_INVITED_SUPPLIERS_LIST_FAILURE,
});

export const getInvitedSuppliersList = (projectId, roundId) => (dispatch) => {
  dispatch(getInvitedSuppliersListRequest());

  return request
    .get(`projects/${projectId}/bidding_rounds/${roundId}/suppliers`)
    .then(({ data }) => {
      dispatch(getInvitedSuppliersListSuccess(data));

      return data;
    })
    .catch(({ response: { data } }) => {
      dispatch(getInvitedSuppliersListFailure(data.message));

      throw data;
    });
};

export const GET_TEMPLATES_SUPPLIERS_LIST_REQUEST = `${prefix} GET_TEMPLATES_SUPPLIERS_LIST_REQUEST`;
export const GET_TEMPLATES_SUPPLIERS_LIST_SUCCESS = `${prefix} GET_TEMPLATES_SUPPLIERS_LIST_SUCCESS`;
export const GET_TEMPLATES_SUPPLIERS_LIST_FAILURE = `${prefix} GET_TEMPLATES_SUPPLIERS_LIST_FAILURE`;

const getTemplatesSuppliersListRequest = () => ({
  type: GET_TEMPLATES_SUPPLIERS_LIST_REQUEST,
});
const getTemplatesSuppliersListSuccess = payload => ({
  payload,
  type: GET_TEMPLATES_SUPPLIERS_LIST_SUCCESS,
});
const getTemplatesSuppliersListFailure = payload => ({
  payload,
  type: GET_TEMPLATES_SUPPLIERS_LIST_FAILURE,
});

export const getTemplatesSuppliersList = () => (dispatch) => {
  dispatch(getTemplatesSuppliersListRequest());

  return request
    .get('bidding_rounds/suppliers')
    .then(({ data }) => {
      dispatch(getTemplatesSuppliersListSuccess(data));

      return data;
    })
    .catch(({ response: { data } }) => {
      dispatch(getTemplatesSuppliersListFailure(data.message));

      throw data;
    });
};

export const INVITE_SUPPLIER_REQUEST = `${prefix} INVITE_SUPPLIER_REQUEST`;
export const INVITE_SUPPLIER_SUCCESS = `${prefix} INVITE_SUPPLIER_SUCCESS`;
export const INVITE_SUPPLIER_FAILURE = `${prefix} INVITE_SUPPLIER_FAILURE`;

const inviteSupplierRequest = () => ({ type: INVITE_SUPPLIER_REQUEST });

const inviteSupplierSuccess = payload => ({
  payload,
  type: INVITE_SUPPLIER_SUCCESS,
});
const inviteSupplierFailure = payload => ({
  payload,
  type: INVITE_SUPPLIER_FAILURE,
});

export const inviteSupplier = (projectId, roundId, supplier) => (dispatch) => {
  dispatch(inviteSupplierRequest());

  return request
    .post(`projects/${projectId}/bidding_rounds/${roundId}/invite`, supplier)
    .then(({ data }) => {
      dispatch(inviteSupplierSuccess(data));

      return data;
    })
    .catch((error) => {
      dispatch(inviteSupplierFailure(error));

      throw error;
    });
};

export const INVITE_SUPPLIERS_BY_TEMPLATE_REQUEST = `${prefix} INVITE_SUPPLIERS_BY_TEMPLATE_REQUEST`;
export const INVITE_SUPPLIERS_BY_TEMPLATE_SUCCESS = `${prefix} INVITE_SUPPLIERS_BY_TEMPLATE_SUCCESS`;
export const INVITE_SUPPLIERS_BY_TEMPLATE_FAILURE = `${prefix} INVITE_SUPPLIERS_BY_TEMPLATE_FAILURE`;

const inviteSuppliersByTemplateRequest = () => ({
  type: INVITE_SUPPLIERS_BY_TEMPLATE_REQUEST,
});

const inviteSuppliersByTemplateSuccess = payload => ({
  payload,
  type: INVITE_SUPPLIERS_BY_TEMPLATE_SUCCESS,
});
const inviteSuppliersByTemplateFailure = payload => ({
  payload,
  type: INVITE_SUPPLIERS_BY_TEMPLATE_FAILURE,
});

export const inviteSuppliersByTemplate = (
  projectId,
  roundId,
  templateId
) => (dispatch) => {
  dispatch(inviteSuppliersByTemplateRequest());

  return request
    .post(
      `projects/${projectId}/bidding_rounds/${roundId}/invite/template/${templateId}`
    )
    .then(({ data }) => {
      dispatch(inviteSuppliersByTemplateSuccess(data));

      return data;
    })
    .catch((error) => {
      dispatch(inviteSuppliersByTemplateFailure(error));

      throw error;
    });
};

export const DELETE_SUPPLIER_REQUEST = `${prefix} DELETE_SUPPLIER_REQUEST`;
export const DELETE_SUPPLIER_SUCCESS = `${prefix} DELETE_SUPPLIER_SUCCESS`;
export const DELETE_SUPPLIER_FAILURE = `${prefix} DELETE_SUPPLIER_FAILURE`;

const deleteSupplierRequest = () => ({ type: DELETE_SUPPLIER_REQUEST });

const deleteSupplierSuccess = payload => ({
  payload,
  type: DELETE_SUPPLIER_SUCCESS,
});
const deleteSupplierFailure = payload => ({
  payload,
  type: DELETE_SUPPLIER_FAILURE,
});

export const deleteSupplier = (projectId, roundId, supplierId) => (dispatch) => {
  dispatch(deleteSupplierRequest());

  return request
    .delete(
      `projects/${projectId}/bidding_rounds/${roundId}/supplier/${supplierId}`
    )
    .then(() => {
      dispatch(deleteSupplierSuccess(supplierId));

      return supplierId;
    })
    .catch((error) => {
      dispatch(deleteSupplierFailure(error));

      throw error;
    });
};

export const UPLOAD_TEMPLATE_REQUEST = `${prefix} UPLOAD_TEMPLATE_REQUEST`;
export const UPLOAD_TEMPLATE_SUCCESS = `${prefix} UPLOAD_TEMPLATE_SUCCESS`;
export const UPLOAD_TEMPLATE_FAILURE = `${prefix} UPLOAD_TEMPLATE_FAILURE`;

const uploadTemplateRequest = () => ({ type: UPLOAD_TEMPLATE_REQUEST });
const uploadTemplateSuccess = payload => ({
  payload,
  type: UPLOAD_TEMPLATE_SUCCESS,
});
const uploadTemplateFailure = payload => ({
  payload,
  type: UPLOAD_TEMPLATE_FAILURE,
});

export const uploadTemplate = (projectId, roundId, files) => (dispatch) => {
  dispatch(uploadTemplateRequest());

  return request
    .post(`projects/${projectId}/bidding_rounds/${roundId}/template`, files)
    .then(({ data }) => {
      dispatch(uploadTemplateSuccess(data));

      return data;
    })
    .catch((error) => {
      dispatch(uploadTemplateFailure(error.response.data.message));

      return error;
    });
};

export const UPLOAD_BID_REQUEST = `${prefix} UPLOAD_BID_REQUEST`;
export const UPLOAD_BID_SUCCESS = `${prefix} UPLOAD_BID_SUCCESS`;
export const UPLOAD_BID_FAILURE = `${prefix} UPLOAD_BID_FAILURE`;

const uploadBidRequest = () => ({ type: UPLOAD_BID_REQUEST });
const uploadBidSuccess = payload => ({
  payload,
  type: UPLOAD_BID_SUCCESS,
});
const uploadBidFailure = payload => ({
  payload,
  type: UPLOAD_BID_FAILURE,
});

const createUploadBid = async (projectId, roundId, files) => {
  const response = await request.post(
    `projects/${projectId}/bidding_rounds/${roundId}/bid_import`,
    files
  );

  return response.data;
};

export const uploadBid = (projectId, roundId, files) => async (dispatch) => {
  dispatch(uploadBidRequest());

  const uploadBidJobId = await createUploadBid(projectId, roundId, files);
  const jobId = uploadBidJobId[uploadBidJobId.length - 1];

  dispatch(getRoundDetails(projectId, roundId));
  dispatch(blockingJobSetId(jobId));
  dispatch(blockingJobRequest());

  const updateProgress = (progress) => {
    dispatch(blockingJobProgress(progress));
  };

  dispatch(uploadBidSuccess());

  return pollJobUntilFinished(projectId, jobId, updateProgress)
    .then(({ data }) => {
      dispatch(uploadBidSuccess(data));
      dispatch(blockingJobSuccess());

      return data;
    })
    .catch((error) => {
      dispatch(setJobOutput(error.errorMessage));
      dispatch(uploadBidFailure(error.response.data.message));
      dispatch(blockingJobFailure());

      return error;
    });
};

export const DELETE_BID_REQUEST = `${prefix} DELETE_BID_REQUEST`;
export const DELETE_BID_SUCCESS = `${prefix} DELETE_BID_SUCCESS`;
export const DELETE_BID_FAILURE = `${prefix} DELETE_BID_FAILURE`;

const deleteBidRequest = () => ({ type: DELETE_BID_REQUEST });
const deleteBidSuccess = payload => ({
  payload,
  type: DELETE_BID_SUCCESS,
});
const deleteBidFailure = payload => ({
  payload,
  type: DELETE_BID_FAILURE,
});

export const deleteBid = (projectId, roundId, fileIds) => (dispatch) => {
  dispatch(deleteBidRequest());

  return request
    .delete(`projects/${projectId}/bidding_rounds/${roundId}/bid_import`, {
      data: { fileIds },
    })
    .then(({ data }) => {
      dispatch(deleteBidSuccess(data));

      return data;
    })
    .catch((error) => {
      dispatch(deleteBidFailure(error.response.data.message));

      return error;
    });
};

export const COLUMN_MATCHING_REQUEST = `${prefix} COLUMN_MATCHING_REQUEST`;
export const COLUMN_MATCHING_SUCCESS = `${prefix} COLUMN_MATCHING_SUCCESS`;
export const COLUMN_MATCHING_FAILURE = `${prefix} COLUMN_MATCHING_FAILURE`;

const runColumnMatchingJobRequest = () => ({ type: COLUMN_MATCHING_REQUEST });
const runColumnMatchingJobSuccess = payload => ({
  payload,
  type: COLUMN_MATCHING_SUCCESS,
});
const runColumnMatchingJobFailure = payload => ({
  payload,
  type: COLUMN_MATCHING_FAILURE,
});

const createColumnMatchingJob = async (fileIds, sheetName, rowNumber) => {
  const response = await request.post('jobs/column_matching', {
    fileIds,
    rowNumber,
    sheetName,
  });

  return response.data;
};

export const runColumnMatchingJob = (
  projectId,
  fileIds,
  sheetName,
  rowNumber
) => async (dispatch) => {
  dispatch(runColumnMatchingJobRequest());

  const columnMatchingJob = await createColumnMatchingJob(
    fileIds,
    sheetName,
    rowNumber
  );
  const { id: jobId } = columnMatchingJob[0];

  dispatch(blockingJobSetId(jobId));
  dispatch(blockingJobRequest());

  const updateProgress = (progress) => {
    dispatch(blockingJobProgress(progress));
  };

  return pollJobUntilFinished(projectId, jobId, updateProgress)
    .then(({ data }) => {
      dispatch(runColumnMatchingJobSuccess(data));
      dispatch(blockingJobSuccess());

      return data;
    })
    .catch((error) => {
      dispatch(setJobOutput(error.errorMessage));
      dispatch(runColumnMatchingJobFailure(error.response.data.message));
      dispatch(blockingJobFailure());

      return error;
    });
};

export const OFFERS_PROCESSING_REQUEST = `${prefix} OFFERS_PROCESSING_REQUEST`;
export const OFFERS_PROCESSING_SUCCESS = `${prefix} OFFERS_PROCESSING_SUCCESS`;
export const OFFERS_PROCESSING_FAILURE = `${prefix} OFFERS_PROCESSING_FAILURE`;

const runOfferProcessingJobRequest = () => ({
  type: OFFERS_PROCESSING_REQUEST,
});
const runOfferProcessingJobSuccess = payload => ({
  payload,
  type: OFFERS_PROCESSING_SUCCESS,
});
const runOfferProcessingJobFailure = payload => ({
  payload,
  type: OFFERS_PROCESSING_FAILURE,
});

const createOfferProcessingJob = async (projectId, roundId) => {
  const response = await request.post('jobs/offer_processing', {
    projectId,
    roundId,
  });

  return response.data;
};

export const runOfferProcessingJob = (projectId, roundId) => async (dispatch) => {
  dispatch(runOfferProcessingJobRequest());

  const offerProcessingJob = await createOfferProcessingJob(projectId, roundId);

  const { id: jobId } = offerProcessingJob;

  dispatch(blockingJobSetId(jobId));
  dispatch(blockingJobRequest());

  const updateProgress = (progress) => {
    dispatch(blockingJobProgress(progress));
  };

  return pollJobUntilFinished(projectId, jobId, updateProgress)
    .then(({ data }) => {
      dispatch(runOfferProcessingJobSuccess(data));
      dispatch(blockingJobSuccess());

      return data;
    })
    .catch((error) => {
      dispatch(setJobOutput(error.errorMessage));
      dispatch(runOfferProcessingJobFailure(error.response.data.message));
      dispatch(blockingJobFailure());

      return error;
    });
};

export const EXPRESSION_EXAMPLE_REQUEST = `${prefix} EXPRESSION_EXAMPLE_REQUEST`;
export const EXPRESSION_EXAMPLE_SUCCESS = `${prefix} EXPRESSION_EXAMPLE_SUCCESS`;
export const EXPRESSION_EXAMPLE_FAILURE = `${prefix} EXPRESSION_EXAMPLE_FAILURE`;

const runExpressionExampleJobRequest = () => ({
  type: EXPRESSION_EXAMPLE_REQUEST,
});
const runExpressionExampleJobSuccess = payload => ({
  payload,
  type: EXPRESSION_EXAMPLE_SUCCESS,
});
const runExpressionExampleJobFailure = payload => ({
  payload,
  type: EXPRESSION_EXAMPLE_FAILURE,
});

const createExpressionExampleJob = async (expression, projectId, roundId) => {
  const response = await request.post('jobs/expression_example', {
    expression,
    projectId,
    roundId,
  });

  return response.data;
};

export const runExpressionExampleJob = (
  expression,
  projectId,
  roundId
) => async (dispatch) => {
  dispatch(runExpressionExampleJobRequest());

  const expressionExampleJob = await createExpressionExampleJob(
    expression,
    projectId,
    roundId
  );
  const { id: jobId } = expressionExampleJob;

  dispatch(blockingJobSetId(jobId));
  dispatch(blockingJobRequest());

  const updateProgress = (progress) => {
    dispatch(blockingJobProgress(progress));
  };

  return pollJobUntilFinished(projectId, jobId, updateProgress)
    .then(({ data }) => {
      dispatch(runExpressionExampleJobSuccess(data));
      dispatch(blockingJobSuccess());

      return data;
    })
    .catch((error) => {
      dispatch(setJobOutput(error.errorMessage));
      dispatch(runExpressionExampleJobFailure(error));
      dispatch(blockingJobFailure());

      return error;
    });
};

export const EXPRESSION_EVALUATION_REQUEST = `${prefix} EXPRESSION_EVALUATION_REQUEST`;
export const EXPRESSION_EVALUATION_SUCCESS = `${prefix} EXPRESSION_EVALUATION_SUCCESS`;
export const EXPRESSION_EVALUATION_FAILURE = `${prefix} EXPRESSION_EVALUATION_FAILURE`;

const runExpressionEvaluationJobRequest = () => ({
  type: EXPRESSION_EVALUATION_REQUEST,
});
const runExpressionEvaluationJobSuccess = payload => ({
  payload,
  type: EXPRESSION_EVALUATION_SUCCESS,
});
const runExpressionEvaluationJobFailure = payload => ({
  payload,
  type: EXPRESSION_EVALUATION_FAILURE,
});

const createExpressionEvaluationJob = async (projectId, roundId) => {
  const response = await request.post('jobs/expression_evaluation', {
    projectId,
    roundId,
  });

  return response.data;
};

export const runExpressionEvaluationJob = (
  projectId,
  roundId
) => async (dispatch) => {
  dispatch(runExpressionEvaluationJobRequest());

  const expressionExampleJob = await createExpressionEvaluationJob(
    projectId,
    roundId
  );
  const { id: jobId } = expressionExampleJob;

  dispatch(blockingJobSetId(jobId));
  dispatch(blockingJobRequest());

  const updateProgress = (progress) => {
    dispatch(blockingJobProgress(progress));
  };

  return pollJobUntilFinished(projectId, jobId, updateProgress)
    .then(({ data }) => {
      dispatch(runExpressionEvaluationJobSuccess(data));

      dispatch(blockingJobSuccess());

      return data;
    })
    .catch((error) => {
      dispatch(setJobOutput(error.errorMessage));
      dispatch(runExpressionEvaluationJobFailure(error));
      dispatch(blockingJobFailure());

      throw error;
    });
};

export const UPDATE_BID_IMPORT_FILE = `${prefix} UPDATE_BID_IMPORT_FILE`;
export const updateBidImportFile = payload => ({
  payload,
  type: UPDATE_BID_IMPORT_FILE,
});

export const UPDATE_COLUMN_MATCHING_REQUEST = `${prefix} UPDATE_COLUMN_MATCHING_REQUEST`;
export const UPDATE_COLUMN_MATCHING_SUCCESS = `${prefix} UPDATE_COLUMN_MATCHING_SUCCESS`;
export const UPDATE_COLUMN_MATCHING_FAILURE = `${prefix} UPDATE_COLUMN_MATCHING_FAILURE`;

const updateColumnMatchingRequest = () => ({
  type: UPDATE_COLUMN_MATCHING_REQUEST,
});
const updateColumnMatchingSuccess = payload => ({
  payload,
  type: UPDATE_COLUMN_MATCHING_SUCCESS,
});
const updateColumnMatchingFailure = payload => ({
  payload,
  type: UPDATE_COLUMN_MATCHING_FAILURE,
});

export const updateColumnMatching = (projectId, roundId, files) => (dispatch) => {
  dispatch(updateColumnMatchingRequest());

  return request
    .patch(`projects/${projectId}/bidding_rounds/${roundId}/column_matchings`, {
      files,
    })
    .then(({ data }) => {
      dispatch(updateColumnMatchingSuccess(data));

      return data;
    })
    .catch((error) => {
      dispatch(updateColumnMatchingFailure(error.response.data.message));

      return error;
    });
};

export const GET_OFFER_PROCESSED_REQUEST = `${prefix} GET_OFFER_PROCESSED_REQUEST`;
export const GET_OFFER_PROCESSED_SUCCESS = `${prefix} GET_OFFER_PROCESSED_SUCCESS`;
export const GET_OFFER_PROCESSED_FAILURE = `${prefix} GET_OFFER_PROCESSED_FAILURE`;

const getOfferProcessedRequest = () => ({ type: GET_OFFER_PROCESSED_REQUEST });
const getOfferProcessedSuccess = payload => ({
  payload,
  type: GET_OFFER_PROCESSED_SUCCESS,
});
const getOfferProcessedFailure = payload => ({
  payload,
  type: GET_OFFER_PROCESSED_FAILURE,
});

export const getOfferProcessed = (
  projectId,
  roundId,
  headerData,
  projectUnits,
  projectCurrencies
) => (dispatch) => {
  dispatch(getOfferProcessedRequest());

  return request
    .get(`projects/${projectId}/bidding_rounds/${roundId}/offer_processed`, {})
    .then(({ data }) => {
      dispatch(
        getOfferProcessedSuccess({
          data,
          headerData,
          projectCurrencies,
          projectUnits,
        })
      );

      return data;
    })
    .catch((error) => {
      dispatch(getOfferProcessedFailure(error.response.data.message));

      return error;
    });
};

export const UPDATE_OFFER_PROCESSED_REQUEST = `${prefix} UPDATE_OFFER_PROCESSED_REQUEST`;
export const UPDATE_OFFER_PROCESSED_SUCCESS = `${prefix} UPDATE_OFFER_PROCESSED_SUCCESS`;
export const UPDATE_OFFER_PROCESSED_FAILURE = `${prefix} UPDATE_OFFER_PROCESSED_FAILURE`;

const updateOfferProcessedRequest = () => ({
  type: UPDATE_OFFER_PROCESSED_REQUEST,
});
const updateOfferProcessedSuccess = payload => ({
  payload,
  type: UPDATE_OFFER_PROCESSED_SUCCESS,
});
const updateOfferProcessedFailure = payload => ({
  payload,
  type: UPDATE_OFFER_PROCESSED_FAILURE,
});

export const updateOfferProcessed = (
  projectId,
  roundId,
  payload
) => (dispatch) => {
  dispatch(updateOfferProcessedRequest());

  return request
    .patch(
      `projects/${projectId}/bidding_rounds/${roundId}/update_offer_processed`,
      { payload }
    )
    .then(({ data }) => {
      dispatch(
        updateOfferProcessedSuccess({
          data,
        })
      );
      dispatch(
        showNotificationSuccess({
          title: i18n.t('Offers updated successfully'),
        })
      );

      return data;
    })
    .catch((error) => {
      dispatch(
        showNotificationError({
          title: i18n.t('Offers updating failed failed'),
          error,
        })
      );
      dispatch(updateOfferProcessedFailure(error.response.data.message));

      return error;
    });
};

export const UPDATE_FINISHED_STATUS_REQUEST = `${prefix} UPDATE_FINISHED_STATUS_REQUEST`;
export const UPDATE_FINISHED_STATUS_SUCCESS = `${prefix} UPDATE_FINISHED_STATUS_SUCCESS`;
export const UPDATE_FINISHED_STATUS_FAILURE = `${prefix} UPDATE_FINISHED_STATUS_FAILURE`;

const updateFinishedStatusRequest = () => ({
  type: UPDATE_FINISHED_STATUS_REQUEST,
});
const updateFinishedStatusSuccess = payload => ({
  payload,
  type: UPDATE_FINISHED_STATUS_SUCCESS,
});
const updateFinishedStatusFailure = payload => ({
  payload,
  type: UPDATE_FINISHED_STATUS_FAILURE,
});

export const updateFinishedStatus = (projectId, roundId) => (dispatch) => {
  dispatch(updateFinishedStatusRequest());

  return request
    .patch(`projects/${projectId}/bidding_rounds/${roundId}/finished-status`)
    .then(() => {
      dispatch(updateFinishedStatusSuccess());
    })
    .catch((error) => {
      dispatch(updateFinishedStatusFailure(error.response.data.message));

      return error;
    });
};
