import request from '@utils/request';
import { setRememberMe } from '@utils/jwt';
import { showNotificationError } from '@store/actions/notifications';
import { i18n } from '@utils/translate';

const prefix = '[USER]';

export const GET_ME_REQUEST = `${prefix} GET_ME_REQUEST`;
export const GET_ME_SUCCESS = `${prefix} GET_ME_SUCCESS`;
export const GET_ME_FAILURE = `${prefix} GET_ME_FAILURE`;
export const SIGN_IN_REQUEST = `${prefix} SIGN_IN_REQUEST`;
export const SIGN_IN_SUCCESS = `${prefix} SIGN_IN_SUCCESS`;
export const SIGN_IN_FAILURE = `${prefix} SIGN_IN_FAILURE`;
export const SIGN_OUT = `${prefix} SIGN_OUT`;
export const SET_READY = `${prefix} SET_READY`;
export const UPDATE_PASSWORD_REQUEST = `${prefix} UPDATE_PASSWORD_REQUEST`;
export const UPDATE_PASSWORD_SUCCESS = `${prefix} UPDATE_PASSWORD_SUCCESS`;
export const UPDATE_PASSWORD_FAILURE = `${prefix} UPDATE_PASSWORD_FAILURE`;
export const UPDATE_USER_DETAILS_REQUEST = `${prefix} UPDATE_USER_DETAILS_REQUEST`;
export const UPDATE_USER_DETAILS_SUCCESS = `${prefix} UPDATE_USER_DETAILS_SUCCESS`;
export const UPDATE_USER_DETAILS_FAILURE = `${prefix} UPDATE_USER_DETAILS_FAILURE`;
export const FORGOT_PASSWORD_REQUEST = `${prefix} FORGOT_PASSWORD_REQUEST`;
export const FORGOT_PASSWORD_SUCCESS = `${prefix} FORGOT_PASSWORD_SUCCESS`;
export const FORGOT_PASSWORD_FAILURE = `${prefix} FORGOT_PASSWORD_FAILURE`;
export const SET_LANGUAGE_REQUEST = `${prefix} SET_LANGUAGE_REQUEST`;

const setReadyAction = () => ({ type: SET_READY });

export const setReady = () => (dispatch) => {
  dispatch(setReadyAction());
};

export const setLanguageRequest = payload => ({
  payload,
  type: SET_LANGUAGE_REQUEST,
});

const getMeRequest = () => ({ type: GET_ME_REQUEST });
const getMeSuccess = payload => ({
  payload,
  type: GET_ME_SUCCESS,
});
const getMeFailure = () => ({
  type: GET_ME_FAILURE,
});

export const getMe = () => (dispatch) => {
  dispatch(getMeRequest());

  return request
    .get('auth/me/')
    .then(({ data }) => {
      dispatch(getMeSuccess(data));
      dispatch(setReadyAction());

      return data;
    })
    .catch((error) => {
      dispatch(getMeFailure());
      throw error;
    });
};

const signInRequest = () => ({ type: SIGN_IN_REQUEST });
const signInSuccess = payload => ({
  payload,
  type: SIGN_IN_SUCCESS,
});
const signInFailure = payload => ({
  payload,
  type: SIGN_IN_FAILURE,
});

export const signIn = ({ password, email, rememberMe }) => (dispatch) => {
  dispatch(signInRequest());

  return request
    .post('auth/signin', {
      password,
      username: email,
    })
    .then(({ data }) => {
      if (rememberMe) {
        setRememberMe();
      }

      dispatch(signInSuccess(data));

      return data;
    })
    .catch((error) => {
      dispatch(signInFailure(error.response.data.message));

      throw error;
    });
};

const signOutAction = () => ({ type: SIGN_OUT });

export const signOut = returnTo => (dispatch) => {
  return request.post('auth/signout', { returnTo }).then(({ data }) => {
    dispatch(signOutAction());
    window.location.href = data;
  });
};

const updateUserPasswordRequest = () => ({ type: UPDATE_PASSWORD_REQUEST });
const updateUserPasswordSuccess = payload => ({
  payload,
  type: UPDATE_PASSWORD_SUCCESS,
});
const updateUserPasswordFailure = payload => ({
  payload,
  type: UPDATE_PASSWORD_FAILURE,
});

export const updateUserPassword = ({
  oldPassword,
  newPassword,
}) => (dispatch) => {
  dispatch(updateUserPasswordRequest());

  return request
    .patch('users/password', {
      oldPassword,
      password: newPassword,
    })
    .then(({ data }) => {
      dispatch(updateUserPasswordSuccess(data));

      return data;
    })
    .catch((error) => {
      dispatch(
        updateUserPasswordFailure(error.response && error.response.data.message)
      );
      dispatch(
        showNotificationError({
          title: i18n.t('The old password is not correct'),
          error,
        })
      );
    });
};

const updateUserDetailsRequest = () => ({ type: UPDATE_USER_DETAILS_REQUEST });
const updateUserDetailsSuccess = payload => ({
  payload,
  type: UPDATE_USER_DETAILS_SUCCESS,
});
const updateUserDetailsFailure = payload => ({
  payload,
  type: UPDATE_USER_DETAILS_FAILURE,
});

export const updateUserDetails = ({ firstName, id, lastName }) => (dispatch) => {
  dispatch(updateUserDetailsRequest());

  return request
    .patch(`users/${id}`, {
      firstName,
      lastName,
    })
    .then(({ data }) => {
      dispatch(updateUserDetailsSuccess(data));

      return data;
    })
    .catch(({ response: { data } }) => {
      dispatch(updateUserDetailsFailure(data.message));

      throw data;
    });
};

const forgotPasswordRequest = () => ({ type: FORGOT_PASSWORD_REQUEST });
const forgotPasswordSuccess = () => ({
  type: FORGOT_PASSWORD_SUCCESS,
});
const forgotPasswordFailure = payload => ({
  payload,
  type: FORGOT_PASSWORD_FAILURE,
});

export const forgotPassword = ({ email }) => (dispatch) => {
  dispatch(forgotPasswordRequest());

  return request
    .post('auth/forgotpassword', {
      username: email,
    })
    .then(() => {
      dispatch(forgotPasswordSuccess());
    })
    .catch((error) => {
      dispatch(forgotPasswordFailure(error.response.data.message));

      throw error;
    });
};
