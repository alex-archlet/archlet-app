import request from '@utils/request';
import { i18n } from '@utils/translate';
import { showNotificationError } from '@store/actions/notifications';
import {
  blockingJobFailure,
  blockingJobProgress,
  blockingJobRequest,
  blockingJobSetId,
  blockingJobSuccess,
  pollJobUntilFinished,
  setJobOutput,
} from './jobs';
import { getProjectInfo } from './projects';

const prefix = '[DATA]';

export const GET_PROJECT_FILES_REQUEST = `${prefix} GET_PROJECT_FILES_REQUEST`;
export const GET_PROJECT_FILES_SUCCESS = `${prefix} GET_PROJECT_FILES_SUCCESS`;
export const GET_PROJECT_FILES_FAILURE = `${prefix} GET_PROJECT_FILES_FAILURE`;

const getProjectFilesRequest = () => ({ type: GET_PROJECT_FILES_REQUEST });
const getProjectFilesSuccess = payload => ({
  payload,
  type: GET_PROJECT_FILES_SUCCESS,
});
const getProjectFilesFailure = payload => ({
  payload,
  type: GET_PROJECT_FILES_FAILURE,
});

export const getProjectFiles = projectId => (dispatch) => {
  dispatch(getProjectFilesRequest());

  return request
    .get(`projects/${projectId}/files`)
    .then(({ data }) => {
      dispatch(getProjectFilesSuccess(data));

      return data;
    })
    .catch((error) => {
      dispatch(getProjectFilesFailure(error.response.data.message));
      dispatch(
        showNotificationError({
          title: i18n.t('Project files loading failed'),
          error,
        })
      );

      throw error;
    });
};

export const UPLOAD_FILES_REQUEST = `${prefix} UPLOAD_FILES_REQUEST`;
export const UPLOAD_FILES_SUCCESS = `${prefix} UPLOAD_FILES_SUCCESS`;
export const UPLOAD_FILES_FAILURE = `${prefix} UPLOAD_FILES_FAILURE`;

const uploadFilesRequest = () => ({ type: UPLOAD_FILES_REQUEST });
const uploadFilesSuccess = payload => ({
  payload,
  type: UPLOAD_FILES_SUCCESS,
});
const uploadFilesFailure = payload => ({
  payload,
  type: UPLOAD_FILES_FAILURE,
});

export const uploadFiles = files => (dispatch) => {
  dispatch(uploadFilesRequest());

  return request
    .post('files', files)
    .then(({ data }) => {
      dispatch(uploadFilesSuccess(data));
   
      return data;
    })
    .catch((error) => {
      dispatch(uploadFilesFailure(error.response.data.message));
      dispatch(
        showNotificationError({
          title: i18n.t('Upload project data failed'),
          error,
        })
      );

      return error;
    });
};

const parseData = data => ({
  headers: data.headers.map(header => ({
    key: header,
    value: header,
  })),
  rows: data.rows,
});

export const GET_EXAMPLE_DATA_REQUEST = `${prefix} GET_EXAMPLE_DATA_REQUEST`;
export const GET_EXAMPLE_DATA_SUCCESS = `${prefix} GET_EXAMPLE_DATA_SUCCESS`;
export const GET_EXAMPLE_DATA_FAILURE = `${prefix} GET_EXAMPLE_DATA_FAILURE`;

const getExampleDataRequest = () => ({ type: GET_EXAMPLE_DATA_REQUEST });
const getExampleDataSuccess = payload => ({
  payload,
  type: GET_EXAMPLE_DATA_SUCCESS,
});
const getExampleDataFailure = payload => ({
  payload,
  type: GET_EXAMPLE_DATA_FAILURE,
});

export const getExampleData = type => (dispatch) => {
  dispatch(getExampleDataRequest());

  return request
    .get(`files/example/${type}`)
    .then(({ data }) => {
      const parsedData = parseData(data);

      dispatch(getExampleDataSuccess(parsedData));

      return parsedData;
    })
    .catch((error) => {
      dispatch(getExampleDataFailure(error.response.data.message));

      return error;
    });
};

export const DELETE_DATA_REQUEST = `${prefix} DELETE_DATA_REQUEST`;
export const DELETE_DATA_SUCCESS = `${prefix} DELETE_DATA_SUCCESS`;
export const DELETE_DATA_FAILURE = `${prefix} DELETE_DATA_FAILURE`;

const deleteDataRequest = () => ({ type: DELETE_DATA_REQUEST });
const deleteDataSuccess = payload => ({
  payload,
  type: DELETE_DATA_SUCCESS,
});
const deleteDataFailure = payload => ({
  payload,
  type: DELETE_DATA_FAILURE,
});

export const deleteData = file => (dispatch) => {
  dispatch(deleteDataRequest());

  return request
    .delete(`files/${file.id}`)
    .then(({ data }) => {
      dispatch(deleteDataSuccess(file.fileType));

      return data;
    })
    .catch((error) => {
      dispatch(deleteDataFailure(error.response.data.message));

      return error;
    });
};

export const GET_DATA_REQUEST = `${prefix} GET_DATA_REQUEST`;
export const GET_DATA_SUCCESS = `${prefix} GET_DATA_SUCCESS`;
export const GET_DATA_FAILURE = `${prefix} GET_DATA_FAILURE`;

const getDataRequest = () => ({ type: GET_DATA_REQUEST });
const getDataSuccess = payload => ({
  payload,
  type: GET_DATA_SUCCESS,
});
const getDataFailure = payload => ({
  payload,
  type: GET_DATA_FAILURE,
});

export const getData = (type, projectId) => (dispatch) => {
  dispatch(getDataRequest());

  return request
    .get(`projects/${projectId}/file_preview/${type}`)
    .then(({ data }) => {
      const parsedData = parseData(data);

      dispatch(getDataSuccess(parsedData));

      return parsedData;
    })
    .catch((error) => {
      dispatch(getDataFailure(error.response.data.message));

      return error;
    });
};

export const VALIDATE_DATA_REQUEST = `${prefix} VALIDATE_DATA_REQUEST`;
export const VALIDATE_DATA_SUCCESS = `${prefix} VALIDATE_DATA_SUCCESS`;
export const VALIDATE_DATA_FAILURE = `${prefix} VALIDATE_DATA_FAILURE`;

const initialData = {
  data: [],
  errors: 0,
  warnings: 0,
};

const categorizeValidationData = (output, filesTypes) => {
  if (output.messages) {
    const initialValidation = Object.keys(filesTypes).reduce(
      (prev, curr) => ({
        ...prev,
        [curr]: { ...initialData },
      }),
      {}
    );

    return output.messages.reduce(
      (prev, curr, index) => {
        if (curr.category === 'default') {
          return {
            ...prev,
            default: {
              data: [
                ...prev.default.data,
                {
                  ...curr,
                  id: index,
                },
              ],
            },
          };
        }

        const fileName = curr.filetype;
        const previous = prev[fileName] || initialData;

        return {
          ...prev,
          [fileName]: {
            data: [
              ...previous.data,
              {
                ...curr,
                id: index,
              },
            ],
            errors:
              curr.severity === 'error' ? previous.errors + 1 : previous.errors,
            warnings:
              curr.severity === 'warning'
                ? previous.warnings + 1
                : previous.warnings,
          },
        };
      },
      {
        ...initialValidation,
        default: { ...initialData },
      }
    );
  }

  return {};
};

const validateDataRequest = () => ({ type: VALIDATE_DATA_REQUEST });
const validateDataSuccess = payload => ({
  payload,
  type: VALIDATE_DATA_SUCCESS,
});
const validateDataFailure = payload => ({
  payload,
  type: VALIDATE_DATA_FAILURE,
});

const createValidateDataJob = async (payload) => {
  const response = await request.post('jobs/files_validation', payload);

  return response.data;
};

export const validateData = (
  projectId,
  files,
  filesTypes,
  onUpdateProgress = () => {}
) => async (dispatch) => {
  dispatch(validateDataRequest());

  const filesKeys = Object.keys(files);

  const filesPayload = filesKeys.reduce(
    (prev, curr) => ({
      ...prev,
      [`${curr}_table`]: files[curr].id,
    }),
    {}
  );

  const payload = {
    files: filesPayload,
    projectId,
  };

  const createdJob = await createValidateDataJob(payload);
  const { id: jobId } = createdJob;

  dispatch(blockingJobSetId(jobId));

  return pollJobUntilFinished(projectId, jobId, onUpdateProgress)
    .then(({ data }) => {
      const categorizedData = categorizeValidationData(data.output, filesTypes);

      dispatch(validateDataSuccess(categorizedData));

      return categorizedData;
    })
    .catch((error) => {
      // dispatch(blockingJobSetId(null));
      dispatch(setJobOutput(error.errorMessage));
      dispatch(validateDataFailure(error.response.data.message));

      return error;
    });
};

export const createValidationJobAndGetProjectInfo = (
  projectId,
  files,
  filesTypes
) => async (dispatch) => {
  const updateProgress = (index, progress) => {
    const base = index * 100;

    dispatch(blockingJobProgress(base + progress));
  };

  dispatch(blockingJobRequest());

  try {
    await dispatch(
      validateData(projectId, files, filesTypes, progress =>
        updateProgress(0, progress)
      )
    );

    await dispatch(getProjectInfo(projectId));

    dispatch(blockingJobSuccess());
  } catch (error) {
    dispatch(blockingJobFailure());

    throw error;
  }
};

export const GET_DATA_VALIDITY_REQUEST = `${prefix} GET_DATA_VALIDITY_REQUEST`;
export const GET_DATA_VALIDITY_SUCCESS = `${prefix} GET_DATA_VALIDITY_SUCCESS`;
export const GET_DATA_VALIDITY_FAILURE = `${prefix} GET_DATA_VALIDITY_FAILURE`;

const getDataValidityRequest = () => ({ type: GET_DATA_VALIDITY_REQUEST });
const getDataValiditySuccess = payload => ({
  payload,
  type: GET_DATA_VALIDITY_SUCCESS,
});
const getDataValidityFailure = payload => ({
  payload,
  type: GET_DATA_VALIDITY_FAILURE,
});

export const getDataValidity = (projectId, filesTypes) => (dispatch) => {
  dispatch(getDataValidityRequest());

  return request
    .get(`projects/${projectId}/files_validation`)
    .then(({ data }) => {
      const categorizedData =
        data && data.output
          ? categorizeValidationData(data.output, filesTypes)
          : {};

      dispatch(getDataValiditySuccess(categorizedData));

      return categorizedData;
    })
    .catch((error) => {
      dispatch(getDataValidityFailure(error.response.data.message));
      dispatch(
        showNotificationError({
          title: i18n.t('Files are not valid'),
          error,
        })
      );

      return error;
    });
};

export const GET_FILES_TYPES_REQUEST = `${prefix} GET_FILES_TYPES_REQUEST`;
export const GET_FILES_TYPES_SUCCESS = `${prefix} GET_FILES_TYPES_SUCCESS`;
export const GET_FILES_TYPES_FAILURE = `${prefix} GET_FILES_TYPES_FAILURE`;

const getFileTypesRequest = () => ({ type: GET_FILES_TYPES_REQUEST });
const getFileTypesSuccess = payload => ({
  payload,
  type: GET_FILES_TYPES_SUCCESS,
});
const getFileTypesFailure = payload => ({
  payload,
  type: GET_FILES_TYPES_FAILURE,
});

export const getFileTypes = () => (dispatch) => {
  dispatch(getFileTypesRequest());

  return request
    .get('files/types')
    .then(({ data }) => {
      dispatch(getFileTypesSuccess(data));

      return data;
    })
    .catch((error) => {
      dispatch(getFileTypesFailure(error.response.data.message));

      return error;
    });
};

export const RESET_NEW_FILES_UPLOAD = `${prefix} RESET_NEW_FILES_UPLOAD`;

export const resetNewUploadData = () => (
  { 
    type: RESET_NEW_FILES_UPLOAD,
    isNewFilesUpload: false
  }
);
