const prefix = '[SIDEBAR]';

export const TOGGLE = `${prefix} TOGGLE`;
export const OPEN = `${prefix} OPEN`;

export const toggleSidebar = () => ({
  type: TOGGLE,
});

export const openSidebar = () => ({
  type: OPEN,
});