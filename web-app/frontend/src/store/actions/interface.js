const prefix = '[INTERFACE]';

export const REMOVE_SPINNER = `${prefix} REMOVE_SPINNER`;
export const SET_PAGE_TITLE = `${prefix} SET_PAGE_TITLE`;
export const SET_BACK_BUTTON = `${prefix} SET_BACK_BUTTON`;
export const SET_SPINNER = `${prefix} SET_SPINNER`;

export const setPageTitle = title => (dispatch) => {
  dispatch({
    payload: title,
    type: SET_PAGE_TITLE,
  });
};

export const setBackButton = (type, destinationUrl = '') => (dispatch) => {
  dispatch({
    payload: {
      destinationUrl,
      type,
    },
    type: SET_BACK_BUTTON,
  });
};

export const setSpinner = () => dispatch => dispatch({ type: SET_SPINNER });

export const removeSpinner = () => dispatch =>
  dispatch({ type: REMOVE_SPINNER });
