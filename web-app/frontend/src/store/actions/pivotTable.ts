import { ProjectPivotTableRow } from '@store/reducers/pivotTable';

const prefix = '[PIVOT_TABLE]';

export const SET_PROJECT_PIVOT_TABLE_STATE = `${prefix} SET_PROJECT_PIVOT_TABLE_STATE`;

export const setProjectPivotTableState = (
  key: string,
  state: ProjectPivotTableRow[]
) => ({
  payload: {
    key,
    state,
  },
  type: SET_PROJECT_PIVOT_TABLE_STATE,
});
