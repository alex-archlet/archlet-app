import request from '@utils/request';
import { getFieldErrors } from '@utils/helpers';
import { SubmissionError } from 'redux-form';
import { getErrorMessage } from '@utils/error';
import {
  showNotificationError,
  showNotificationSuccess,
} from '@store/actions/notifications';
import { i18n } from '@utils/translate';

const prefix = '[PROJECTS]';

export const GET_PROJECTS_REQUEST = `${prefix} GET_PROJECTS_REQUEST`;
export const GET_PROJECTS_SUCCESS = `${prefix} GET_PROJECTS_SUCCESS`;
export const GET_PROJECTS_FAILURE = `${prefix} GET_PROJECTS_FAILURE`;

const getProjectsRequest = () => ({ type: GET_PROJECTS_REQUEST });
const getProjectsSuccess = payload => ({
  payload,
  type: GET_PROJECTS_SUCCESS,
});
const getProjectsFailure = payload => ({
  payload,
  type: GET_PROJECTS_FAILURE,
});

export const getProjects = userId => (dispatch) => {
  dispatch(getProjectsRequest());

  return request
    .get(`users/${userId}/projects`)
    .then(({ data }) => {
      dispatch(getProjectsSuccess(data));

      return data;
    })
    .catch((error) => {
      dispatch(getProjectsFailure(error.response.data.message));
      dispatch(
        showNotificationError({
          title: i18n.t('Project list loading failed'),
          body: i18n.t('User does not have permissions to view any projects'),
          error,
        })
      );

      throw error;
    });
};

export const DELETE_PROJECT_REQUEST = `${prefix} DELETE_PROJECT_REQUEST`;
export const DELETE_PROJECT_SUCCESS = `${prefix} DELETE_PROJECT_SUCCESS`;
export const DELETE_PROJECT_FAILURE = `${prefix} DELETE_PROJECT_FAILURE`;

const deleteProjectRequest = () => ({ type: DELETE_PROJECT_REQUEST });
const deleteProjectSuccess = payload => ({
  payload,
  type: DELETE_PROJECT_SUCCESS,
});
const deleteProjectFailure = payload => ({
  payload,
  type: DELETE_PROJECT_FAILURE,
});

export const deleteProject = projectId => (dispatch) => {
  dispatch(deleteProjectRequest());

  return request
    .delete(`projects/${projectId}`)
    .then(() => {
      dispatch(deleteProjectSuccess(projectId));
    })
    .catch((error) => {
      dispatch(deleteProjectFailure(error.response.data.message));

      throw error;
    });
};

export const GET_PROJECT_DETAILS_REQUEST = `${prefix} GET_PROJECT_DETAILS_REQUEST`;
export const GET_PROJECT_DETAILS_SUCCESS = `${prefix} GET_PROJECT_DETAILS_SUCCESS`;
export const GET_PROJECT_DETAILS_FAILURE = `${prefix} GET_PROJECT_DETAILS_FAILURE`;

const getProjectDetailsRequest = () => ({ type: GET_PROJECT_DETAILS_REQUEST });
const getProjectDetailsSuccess = payload => ({
  payload,
  type: GET_PROJECT_DETAILS_SUCCESS,
});
const getProjectDetailsFailure = payload => ({
  payload,
  type: GET_PROJECT_DETAILS_FAILURE,
});

export const getProjectDetails = projectId => (dispatch) => {
  dispatch(getProjectDetailsRequest());

  return request
    .get(`projects/${projectId}`)
    .then(({ data }) => {
      dispatch(getProjectDetailsSuccess(data));

      return data;
    })
    .catch((error) => {
      dispatch(getProjectDetailsFailure(error.response.data.message));
      dispatch(
        showNotificationError({
          title: i18n.t('Project details loading failed'),
          error,
        })
      );

      throw error;
    });
};

export const GET_PROJECT_BIDDING_TYPES_REQUEST = `${prefix} GET_PROJECT_BIDDING_TYPES_REQUEST`;
export const GET_PROJECT_BIDDING_TYPES_SUCCESS = `${prefix} GET_PROJECT_BIDDING_TYPES_SUCCESS`;
export const GET_PROJECT_BIDDING_TYPES_FAILURE = `${prefix} GET_PROJECT_BIDDING_TYPES_FAILURE`;

const getProjectBiddingTypesRequest = () => ({
  type: GET_PROJECT_BIDDING_TYPES_REQUEST,
});
const getProjectBiddingTypesSuccess = payload => ({
  payload,
  type: GET_PROJECT_BIDDING_TYPES_SUCCESS,
});
const getProjectBiddingTypesFailure = payload => ({
  payload,
  type: GET_PROJECT_BIDDING_TYPES_FAILURE,
});

export const getProjectBiddingTypes = () => (dispatch) => {
  dispatch(getProjectBiddingTypesRequest());

  return request
    .get('projects/biddingTypes')
    .then(({ data }) => {
      dispatch(getProjectBiddingTypesSuccess(data));

      return data;
    })
    .catch((error) => {
      dispatch(getProjectBiddingTypesFailure(error.response.data.message));

      throw error;
    });
};

export const GET_PROJECT_TYPES_REQUEST = `${prefix} GET_PROJECT_TYPES_REQUEST`;
export const GET_PROJECT_TYPES_SUCCESS = `${prefix} GET_PROJECT_TYPES_SUCCESS`;
export const GET_PROJECT_TYPES_FAILURE = `${prefix} GET_PROJECT_TYPES_FAILURE`;

const getProjectTypesRequest = () => ({ type: GET_PROJECT_TYPES_REQUEST });
const getProjectTypesSuccess = payload => ({
  payload,
  type: GET_PROJECT_TYPES_SUCCESS,
});
const getProjectTypesFailure = payload => ({
  payload,
  type: GET_PROJECT_TYPES_FAILURE,
});

export const getProjectTypes = () => (dispatch) => {
  dispatch(getProjectTypesRequest());

  return request
    .get('projects/types')
    .then(({ data }) => {
      dispatch(getProjectTypesSuccess(data));

      return data;
    })
    .catch((error) => {
      dispatch(getProjectTypesFailure(error.response.data.message));
      dispatch(
        showNotificationError({
          title: i18n.t('Project types loading failed'),
          error,
        })
      );

      throw error;
    });
};

export const GET_PROJECT_CATEGORIES_REQUEST = `${prefix} GET_PROJECT_CATEGORIES_REQUEST`;
export const GET_PROJECT_CATEGORIES_SUCCESS = `${prefix} GET_PROJECT_CATEGORIES_SUCCESS`;
export const GET_PROJECT_CATEGORIES_FAILURE = `${prefix} GET_PROJECT_CATEGORIES_FAILURE`;

const getProjectCategoriesRequest = () => ({
  type: GET_PROJECT_CATEGORIES_REQUEST,
});
const getProjectCategoriesSuccess = payload => ({
  payload,
  type: GET_PROJECT_CATEGORIES_SUCCESS,
});
const getProjectCategoriesFailure = payload => ({
  payload,
  type: GET_PROJECT_CATEGORIES_FAILURE,
});

export const getProjectCategories = () => (dispatch) => {
  dispatch(getProjectCategoriesRequest());

  return request
    .get('projects/categories')
    .then(({ data }) => {
      dispatch(getProjectCategoriesSuccess(data));

      return data;
    })
    .catch((error) => {
      dispatch(getProjectCategoriesFailure(error.response.data.message));
      dispatch(
        showNotificationError({
          title: i18n.t('Project categories loading failed'),
          error,
        })
      );

      throw error;
    });
};

export const GET_PROJECT_CURRENCIES_REQUEST = `${prefix} GET_PROJECT_CURRENCIES_REQUEST`;
export const GET_PROJECT_CURRENCIES_SUCCESS = `${prefix} GET_PROJECT_CURRENCIES_SUCCESS`;
export const GET_PROJECT_CURRENCIES_FAILURE = `${prefix} GET_PROJECT_CURRENCIES_FAILURE`;

const getProjectCurrenciesRequest = () => ({
  type: GET_PROJECT_CURRENCIES_REQUEST,
});
const getProjectCurrenciesSuccess = payload => ({
  payload,
  type: GET_PROJECT_CURRENCIES_SUCCESS,
});
const getProjectCurrenciesFailure = payload => ({
  payload,
  type: GET_PROJECT_CURRENCIES_FAILURE,
});

export const getProjectCurrencies = () => (dispatch) => {
  dispatch(getProjectCurrenciesRequest());

  return request
    .get('projects/currencies')
    .then(({ data }) => {
      dispatch(getProjectCurrenciesSuccess(data));

      return data;
    })
    .catch((error) => {
      dispatch(getProjectCurrenciesFailure(error.response.data.message));
      dispatch(
        showNotificationError({
          title: i18n.t('Project currencies loading failed'),
          error,
        })
      );

      throw error;
    });
};

export const GET_PROJECT_UNITS_REQUEST = `${prefix} GET_PROJECT_UNITS_REQUEST`;
export const GET_PROJECT_UNITS_SUCCESS = `${prefix} GET_PROJECT_UNITS_SUCCESS`;
export const GET_PROJECT_UNITS_FAILURE = `${prefix} GET_PROJECT_UNITS_FAILURE`;

const getProjectUnitsRequest = () => ({ type: GET_PROJECT_UNITS_REQUEST });
const getProjectUnitsSuccess = payload => ({
  payload,
  type: GET_PROJECT_UNITS_SUCCESS,
});
const getProjectUnitsFailure = payload => ({
  payload,
  type: GET_PROJECT_UNITS_FAILURE,
});

export const getProjectUnits = () => (dispatch) => {
  dispatch(getProjectUnitsRequest());

  return request
    .get('projects/units')
    .then(({ data }) => {
      dispatch(getProjectUnitsSuccess(data));

      return data;
    })
    .catch((error) => {
      dispatch(getProjectUnitsFailure(error.response.data.message));
      dispatch(
        showNotificationError({
          title: i18n.t('Project units loading failed'),
          error,
        })
      );

      throw error;
    });
};

export const CREATE_PROJECT_REQUEST = `${prefix} CREATE_PROJECT_REQUEST`;
export const CREATE_PROJECT_SUCCESS = `${prefix} CREATE_PROJECT_SUCCESS`;
export const CREATE_PROJECT_FAILURE = `${prefix} CREATE_PROJECT_FAILURE`;

const createProjectRequest = () => ({ type: CREATE_PROJECT_REQUEST });
const createProjectSuccess = payload => ({
  payload,
  type: CREATE_PROJECT_SUCCESS,
});
const createProjectFailure = payload => ({
  payload,
  type: CREATE_PROJECT_FAILURE,
});

export const createProject = project => (dispatch) => {
  dispatch(createProjectRequest());

  return request
    .post('projects', project)
    .then(({ data }) => {
      dispatch(createProjectSuccess(data));

      return data;
    })
    .catch(({ response: { data } }) => {
      dispatch(createProjectFailure(data));

      throw new SubmissionError({
        ...getFieldErrors(data),
        _error: data.message || getErrorMessage(),
      });
    });
};

export const UPDATE_PROJECT_REQUEST = `${prefix} UPDATE_PROJECT_REQUEST`;
export const UPDATE_PROJECT_SUCCESS = `${prefix} UPDATE_PROJECT_SUCCESS`;
export const UPDATE_PROJECT_FAILURE = `${prefix} UPDATE_PROJECT_FAILURE`;

const updateProjectRequest = () => ({ type: UPDATE_PROJECT_REQUEST });
const updateProjectSuccess = payload => ({
  payload,
  type: UPDATE_PROJECT_SUCCESS,
});
const updateProjectFailure = payload => ({
  payload,
  type: UPDATE_PROJECT_FAILURE,
});

export const updateProject = project => (dispatch) => {
  dispatch(updateProjectRequest());
  return request
    .put(`projects/${project.id}`, project)
    .then(({ data }) => {
      dispatch(updateProjectSuccess(data));
      dispatch(
        showNotificationSuccess({
          title: i18n.t('Project updated'),
        })
      );

      return data;
    })
    .catch(({ response: { data } }) => {
      dispatch(updateProjectFailure(data));
      dispatch(
        showNotificationError({
          title: i18n.t('Project update failed'),
          error: data,
        })
      );

      throw new SubmissionError({
        ...getFieldErrors(data),
        _error: data.message || getErrorMessage(),
      });
    });
};

export const GET_PROJECT_INFO_REQUEST = `${prefix} GET_PROJECT_INFO_REQUEST`;
export const GET_PROJECT_INFO_SUCCESS = `${prefix} GET_PROJECT_INFO_SUCCESS`;
export const GET_PROJECT_INFO_FAILURE = `${prefix} GET_PROJECT_INFO_FAILURE`;

const getProjectInfoRequest = () => ({ type: GET_PROJECT_INFO_REQUEST });
const getProjectInfoSuccess = payload => ({
  payload,
  type: GET_PROJECT_INFO_SUCCESS,
});
const getProjectInfoFailure = payload => ({
  payload,
  type: GET_PROJECT_INFO_FAILURE,
});

export const getProjectInfo = projectId => (dispatch) => {
  dispatch(getProjectInfoRequest());

  return request
    .get(`projects/${projectId}/info`)
    .then(({ data }) => {
      dispatch(getProjectInfoSuccess(data));

      return data;
    })
    .catch((error) => {
      dispatch(getProjectInfoFailure(error.response.data.message));
      dispatch(
        showNotificationError({
          title: i18n.t('Project information loading failed'),
          body: i18n.t('Project id is not correct'),
          error,
        })
      );

      return error;
    });
};

export const UPDATE_PROJECT_SETTINGS_REQUEST = `${prefix} UPDATE_PROJECT_SETTINGS_REQUEST`;
export const UPDATE_PROJECT_SETTINGS_SUCCESS = `${prefix} UPDATE_PROJECT_SETTINGS_SUCCESS`;
export const UPDATE_PROJECT_SETTINGS_FAILURE = `${prefix} UPDATE_PROJECT_SETTINGS_FAILURE`;

const updateProjectSettingsRequest = () => ({
  type: UPDATE_PROJECT_SETTINGS_REQUEST,
});
const updateProjectSettingsSuccess = payload => ({
  payload,
  type: UPDATE_PROJECT_SETTINGS_SUCCESS,
});
const updateProjectSettingsFailure = payload => ({
  payload,
  type: UPDATE_PROJECT_SETTINGS_FAILURE,
});

export const updateProjectSettings = (projectId, settings) => (dispatch) => {
  dispatch(updateProjectSettingsRequest());

  return request
    .put(`projects/${projectId}/settings`, { settings })
    .then(({ data }) => {
      dispatch(updateProjectSettingsSuccess(data));
      dispatch(
        showNotificationSuccess({
          title: i18n.t('Project settings updated'),
        })
      );

      return data;
    })
    .catch((error) => {
      dispatch(updateProjectSettingsFailure(error.response.data.message));
      dispatch(
        showNotificationError({
          title: i18n.t('Project settings update failed'),
          body: i18n.t('Project settings were not applied'),
          error,
        })
      );

      return error;
    });
};

export const GET_PROJECT_KPI_DEFS_REQUEST = `${prefix} GET_PROJECT_KPI_DEFS_REQUEST`;
export const GET_PROJECT_KPI_DEFS_SUCCESS = `${prefix} GET_PROJECT_KPI_DEFS_SUCCESS`;
export const GET_PROJECT_KPI_DEFS_FAILURE = `${prefix} GET_PROJECT_KPI_DEFS_FAILURE`;

const getProjectKpiDefsRequest = () => ({ type: GET_PROJECT_KPI_DEFS_REQUEST });
const getProjectKpiDefsSuccess = payload => ({
  payload,
  type: GET_PROJECT_KPI_DEFS_SUCCESS,
});
const getProjectKpiDefsFailure = payload => ({
  payload,
  type: GET_PROJECT_KPI_DEFS_FAILURE,
});

export const getProjectKpiDefs = projectId => (dispatch) => {
  dispatch(getProjectKpiDefsRequest());

  return request
    .get(`projects/${projectId}/kpi_defs`)
    .then(({ data }) => {
      dispatch(getProjectKpiDefsSuccess(data));

      return data;
    })
    .catch((error) => {
      dispatch(getProjectKpiDefsFailure(error.response.data.message));

      return error;
    });
};

export const getProjectStatics = () => (dispatch) => {
  dispatch(getProjectCategories());
  dispatch(getProjectCurrencies());
  dispatch(getProjectUnits());
  dispatch(getProjectTypes());
};
