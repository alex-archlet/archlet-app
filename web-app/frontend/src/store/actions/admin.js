import request from '@utils/request';
import { getFieldErrors } from '@utils/helpers';
import { getErrorMessage } from '@utils/error';
import { SubmissionError } from 'redux-form';
import { showNotificationError } from '@store/actions/notifications';
import { i18n } from '@utils/translate';

const prefix = '[ADMIN]';

export const GET_COMPANIES_REQUEST = `${prefix} GET_COMPANIES_REQUEST`;
export const GET_COMPANIES_SUCCESS = `${prefix} GET_COMPANIES_SUCCESS`;
export const GET_COMPANIES_FAILURE = `${prefix} GET_COMPANIES_FAILURE`;

const getCompaniesRequest = () => ({ type: GET_COMPANIES_REQUEST });
const getCompaniesSuccess = payload => ({
  payload,
  type: GET_COMPANIES_SUCCESS,
});
const getCompaniesFailure = payload => ({
  payload,
  type: GET_COMPANIES_FAILURE,
});

export const getCompanies = () => (dispatch) => {
  dispatch(getCompaniesRequest());

  return request.get('companies')
    .then(({ data }) => {
      dispatch(getCompaniesSuccess(data));

      return data;
    })
    .catch((error) => {
      dispatch(getCompaniesFailure(error.response.data.message));
      dispatch(
        showNotificationError({
          title: i18n.t('Companies loading failed'),
          error,
        })
      );

      throw error;
    });
};

export const CREATE_COMPANY_REQUEST = `${prefix} CREATE_COMPANY_REQUEST`;
export const CREATE_COMPANY_SUCCESS = `${prefix} CREATE_COMPANY_SUCCESS`;
export const CREATE_COMPANY_FAILURE = `${prefix} CREATE_COMPANY_FAILURE`;

const createCompanyRequest = () => ({ type: CREATE_COMPANY_REQUEST });
const createCompanySuccess = payload => ({
  payload,
  type: CREATE_COMPANY_SUCCESS,
});
const createCompanyFailure = payload => ({
  payload,
  type: CREATE_COMPANY_FAILURE,
});

export const createCompany = payload => (dispatch) => {
  dispatch(createCompanyRequest());

  return request.post('companies', payload)
    .then(({ data }) => {
      dispatch(createCompanySuccess(data));

      return data;
    })
    .catch(({ response: { data } }) => {
      dispatch(createCompanyFailure(data));

      throw new SubmissionError({
        ...getFieldErrors(data),
        _error: data.message || getErrorMessage(),
      });
    });
};

export const UPDATE_COMPANY_REQUEST = `${prefix} UPDATE_COMPANY_REQUEST`;
export const UPDATE_COMPANY_SUCCESS = `${prefix} UPDATE_COMPANY_SUCCESS`;
export const UPDATE_COMPANY_FAILURE = `${prefix} UPDATE_COMPANY_FAILURE`;

const updateCompanyRequest = () => ({ type: UPDATE_COMPANY_REQUEST });
const updateCompanySuccess = payload => ({
  payload,
  type: UPDATE_COMPANY_SUCCESS,
});
const updateCompanyFailure = payload => ({
  payload,
  type: UPDATE_COMPANY_FAILURE,
});

export const updateCompany = ({
  id,
  ...payload
}) => (dispatch) => {
  dispatch(updateCompanyRequest());

  return request.patch(`companies/${id}`, payload)
    .then(({ data }) => {
      dispatch(updateCompanySuccess({
        data,
        id,
      }));

      return data;
    })
    .catch(({ response: { data } }) => {
      dispatch(updateCompanyFailure(data));

      throw new SubmissionError({
        ...getFieldErrors(data),
        _error: data.message || getErrorMessage(),
      });
    });
};

export const DELETE_COMPANY_REQUEST = `${prefix} DELETE_COMPANY_REQUEST`;
export const DELETE_COMPANY_SUCCESS = `${prefix} DELETE_COMPANY_SUCCESS`;
export const DELETE_COMPANY_FAILURE = `${prefix} DELETE_COMPANY_FAILURE`;

const deleteCompanyRequest = () => ({ type: DELETE_COMPANY_REQUEST });
const deleteCompanySuccess = payload => ({
  payload,
  type: DELETE_COMPANY_SUCCESS,
});
const deleteCompanyFailure = payload => ({
  payload,
  type: DELETE_COMPANY_FAILURE,
});

export const deleteCompany = id => (dispatch) => {
  dispatch(deleteCompanyRequest());

  return request.delete(`companies/${id}`)
    .then(({ data }) => {
      dispatch(deleteCompanySuccess(id));

      return data;
    })
    .catch((error) => {
      dispatch(deleteCompanyFailure(error.response.data.message));
      dispatch(
        showNotificationError({
          title: i18n.t('It is not possible to remove a company'),
          error,
        })
      );

      throw error;
    });
};

export const GET_BRANCHES_REQUEST = `${prefix} GET_BRANCHES_REQUEST`;
export const GET_BRANCHES_SUCCESS = `${prefix} GET_BRANCHES_SUCCESS`;
export const GET_BRANCHES_FAILURE = `${prefix} GET_BRANCHES_FAILURE`;

const getBranchesRequest = () => ({ type: GET_BRANCHES_REQUEST });
const getBranchesSuccess = payload => ({
  payload,
  type: GET_BRANCHES_SUCCESS,
});
const getBranchesFailure = payload => ({
  payload,
  type: GET_BRANCHES_FAILURE,
});

export const getBranches = () => (dispatch) => {
  dispatch(getBranchesRequest());

  return request.get('branches')
    .then(({ data }) => {
      dispatch(getBranchesSuccess(data));

      return data;
    })
    .catch((error) => {
      dispatch(getBranchesFailure(error.response.data.message));
      dispatch(
        showNotificationError({
          title: i18n.t('Branches loading failed'),
          error,
        })
      );

      throw error;
    });
};

export const CREATE_BRANCH_REQUEST = `${prefix} CREATE_BRANCH_REQUEST`;
export const CREATE_BRANCH_SUCCESS = `${prefix} CREATE_BRANCH_SUCCESS`;
export const CREATE_BRANCH_FAILURE = `${prefix} CREATE_BRANCH_FAILURE`;

const createBranchRequest = () => ({ type: CREATE_BRANCH_REQUEST });
const createBranchSuccess = payload => ({
  payload,
  type: CREATE_BRANCH_SUCCESS,
});
const createBranchFailure = payload => ({
  payload,
  type: CREATE_BRANCH_FAILURE,
});

export const createBranch = payload => (dispatch) => {
  dispatch(createBranchRequest());

  return request.post('branches', payload)
    .then(({ data }) => {
      dispatch(createBranchSuccess(data));

      return data;
    })
    .catch(({ response: { data } }) => {
      dispatch(createBranchFailure(data));

      throw new SubmissionError({
        ...getFieldErrors(data),
        _error: data.message || getErrorMessage(),
      });
    });
};

export const UPDATE_BRANCH_REQUEST = `${prefix} UPDATE_BRANCH_REQUEST`;
export const UPDATE_BRANCH_SUCCESS = `${prefix} UPDATE_BRANCH_SUCCESS`;
export const UPDATE_BRANCH_FAILURE = `${prefix} UPDATE_BRANCH_FAILURE`;

const updateBranchRequest = () => ({ type: UPDATE_BRANCH_REQUEST });
const updateBranchSuccess = payload => ({
  payload,
  type: UPDATE_BRANCH_SUCCESS,
});
const updateBranchFailure = payload => ({
  payload,
  type: UPDATE_BRANCH_FAILURE,
});

export const updateBranch = ({
  id,
  ...payload
}) => (dispatch) => {
  dispatch(updateBranchRequest());

  return request.patch(`branches/${id}`, payload)
    .then(({ data }) => {
      dispatch(updateBranchSuccess({
        data,
        id,
      }));

      return data;
    })
    .catch(({ response: { data } }) => {
      dispatch(updateBranchFailure(data));

      throw new SubmissionError({
        ...getFieldErrors(data),
        _error: data.message || getErrorMessage(),
      });
    });
};

export const DELETE_BRANCH_REQUEST = `${prefix} DELETE_BRANCH_REQUEST`;
export const DELETE_BRANCH_SUCCESS = `${prefix} DELETE_BRANCH_SUCCESS`;
export const DELETE_BRANCH_FAILURE = `${prefix} DELETE_BRANCH_FAILURE`;

const deleteBranchRequest = () => ({ type: DELETE_BRANCH_REQUEST });
const deleteBranchSuccess = payload => ({
  payload,
  type: DELETE_BRANCH_SUCCESS,
});
const deleteBranchFailure = payload => ({
  payload,
  type: DELETE_BRANCH_FAILURE,
});

export const deleteBranch = id => (dispatch) => {
  dispatch(deleteBranchRequest());

  return request.delete(`branches/${id}`)
    .then(({ data }) => {
      dispatch(deleteBranchSuccess(id));

      return data;
    })
    .catch((error) => {
      dispatch(deleteBranchFailure(error.response.data.message));

      throw error;
    });
};

export const GET_ROLES_REQUEST = `${prefix} GET_ROLES_REQUEST`;
export const GET_ROLES_SUCCESS = `${prefix} GET_ROLES_SUCCESS`;
export const GET_ROLES_FAILURE = `${prefix} GET_ROLES_FAILURE`;

const getRolesRequest = () => ({ type: GET_ROLES_REQUEST });
const getRolesSuccess = payload => ({
  payload,
  type: GET_ROLES_SUCCESS,
});
const getRolesFailure = payload => ({
  payload,
  type: GET_ROLES_FAILURE,
});

export const getRoles = () => (dispatch) => {
  dispatch(getRolesRequest());

  return request.get('roles')
    .then(({ data }) => {
      dispatch(getRolesSuccess(data));

      return data;
    })
    .catch((error) => {
      dispatch(getRolesFailure(error.response.data.message));
      dispatch(
        showNotificationError({
          title: i18n.t('Roles loading failed'),
          error,
        })
      );

      throw error;
    });
};

export const GET_USERS_REQUEST = `${prefix} GET_USERS_REQUEST`;
export const GET_USERS_SUCCESS = `${prefix} GET_USERS_SUCCESS`;
export const GET_USERS_FAILURE = `${prefix} GET_USERS_FAILURE`;

const getUsersRequest = () => ({ type: GET_USERS_REQUEST });
const getUsersSuccess = payload => ({
  payload,
  type: GET_USERS_SUCCESS,
});
const getUsersFailure = payload => ({
  payload,
  type: GET_USERS_FAILURE,
});

export const getUsers = () => (dispatch) => {
  dispatch(getUsersRequest());

  return request.get('users')
    .then(({ data }) => {
      dispatch(getUsersSuccess(data));

      return data;
    })
    .catch((error) => {
      dispatch(getUsersFailure(error.response.data.message));
      dispatch(
        showNotificationError({
          title: i18n.t('Users loading failed'),
          error,
        })
      );

      throw error;
    });
};

export const CREATE_USER_REQUEST = `${prefix} CREATE_USER_REQUEST`;
export const CREATE_USER_SUCCESS = `${prefix} CREATE_USER_SUCCESS`;
export const CREATE_USER_FAILURE = `${prefix} CREATE_USER_FAILURE`;

const createUserRequest = () => ({ type: CREATE_USER_REQUEST });
const createUserSuccess = payload => ({
  payload,
  type: CREATE_USER_SUCCESS,
});
const createUserFailure = payload => ({
  payload,
  type: CREATE_USER_FAILURE,
});

export const createUser = payload => (dispatch) => {
  dispatch(createUserRequest());

  return request.post('users', {
    ...payload,
    email: payload.username,
    role: 'user',
  })
    .then(({ data }) => {
      dispatch(createUserSuccess(data));

      return data;
    })
    .catch(({ response: { data } }) => {
      dispatch(createUserFailure(data));

      throw new SubmissionError({
        ...getFieldErrors(data),
        _error: data.message || getErrorMessage(),
      });
    });
};

export const UPDATE_USER_REQUEST = `${prefix} UPDATE_USER_REQUEST`;
export const UPDATE_USER_SUCCESS = `${prefix} UPDATE_USER_SUCCESS`;
export const UPDATE_USER_FAILURE = `${prefix} UPDATE_USER_FAILURE`;

const updateUserRequest = () => ({ type: UPDATE_USER_REQUEST });
const updateUserSuccess = payload => ({
  payload,
  type: UPDATE_USER_SUCCESS,
});
const updateUserFailure = payload => ({
  payload,
  type: UPDATE_USER_FAILURE,
});

export const updateUser = ({
  id,
  ...payload
}) => (dispatch) => {
  dispatch(updateUserRequest());

  return request.patch(`users/${id}`, {
    ...payload,
    role: 'user',
    username: payload.email,
  })
    .then(({ data }) => {
      dispatch(updateUserSuccess({
        data,
        id,
      }));

      return data;
    })
    .catch(({ response: { data } }) => {
      dispatch(updateUserFailure(data));

      throw new SubmissionError({
        ...getFieldErrors(data),
        _error: data.message || getErrorMessage(),
      });
    });
};

export const DELETE_USER_REQUEST = `${prefix} DELETE_USER_REQUEST`;
export const DELETE_USER_SUCCESS = `${prefix} DELETE_USER_SUCCESS`;
export const DELETE_USER_FAILURE = `${prefix} DELETE_USER_FAILURE`;

const deleteUserRequest = () => ({ type: DELETE_USER_REQUEST });
const deleteUserSuccess = payload => ({
  payload,
  type: DELETE_USER_SUCCESS,
});
const deleteUserFailure = payload => ({
  payload,
  type: DELETE_USER_FAILURE,
});

export const deleteUser = id => (dispatch) => {
  dispatch(deleteUserRequest());

  return request.delete(`users/${id}`)
    .then(({ data }) => {
      dispatch(deleteUserSuccess(id));

      return data;
    })
    .catch((error) => {
      dispatch(deleteUserFailure(error.response.data.message));

      throw error;
    });
};
