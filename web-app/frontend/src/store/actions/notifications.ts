import { notificationOpts } from '@common/components/Notification/Notification';
import { AxiosError, AxiosResponse } from 'axios';
import Notifications from 'react-notification-system-redux';

export interface NotificationParams {
  title: string;
  body?: string;
}

type NotificationError = Error | AxiosError;

export interface ErrorNotificationParams extends NotificationParams {
  error?: NotificationError;
}

interface ErrorResponse {
  message: string;
}

const prefix = '[NOTIFICATION]';
export const ERROR_MESSAGE = `${prefix} ERROR`;
export const SUCCESS_MESSAGE = `${prefix} SUCCESS`;

export const getNotificationError = payload => ({
  payload,
  type: ERROR_MESSAGE,
});

export const getNotificationSuccess = payload => ({
  payload,
  type: SUCCESS_MESSAGE,
});

const getMessageFromError = (error: NotificationError | undefined): string | undefined => {
  if (error && 'response' in error) {
    
    const errorResponse: AxiosResponse<ErrorResponse> | undefined = error.response;
    if (typeof errorResponse?.data === 'string') {
      return errorResponse.data;
    }
    return errorResponse?.data?.message;
  } 
  return undefined;
};

// an action creator
export const showNotificationError = ({ title, body, error }: ErrorNotificationParams) => { 
  return Notifications.error({
    ...notificationOpts,
    level: 'error',
    title, 
    message: getMessageFromError(error) || body,
  });
};

export const showNotificationSuccess = ({ title, body }: NotificationParams) => {
  return Notifications.success({
    ...notificationOpts,
    level: 'success',
    title, 
    message: body,
  });
};
