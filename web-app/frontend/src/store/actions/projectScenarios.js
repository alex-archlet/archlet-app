/* eslint-disable guard-for-in */
import request from '@utils/request';
import { getFieldErrors } from '@utils/helpers';
import { SubmissionError } from 'redux-form';
import { getErrorMessage } from '@utils/error';
import queryString from 'query-string';
import { showNotificationError } from '@store/actions/notifications';
import { i18n } from '@utils/translate';
import {
  blockingJobFailure,
  blockingJobProgress,
  blockingJobRequest,
  blockingJobSetId,
  blockingJobSuccess,
  pollJobUntilFinished,
  CancelledJobError,
  setJobOutput,
} from './jobs';
import { jobsSelectors } from '../selectors/jobs';

const prefix = '[SCENARIOS]';

const categorizeErrors = (data) => {
  const initialValidation = {
    error: [],
    info: [],
    warning: [],
  };

  const validation = data.output.messages.reduce((prev, curr) => {
    const currentType = curr.severity;

    return {
      ...prev,
      [currentType]: [...prev[currentType], curr],
    };
  }, initialValidation);

  return {
    ...data,
    data: [...validation.error, ...validation.warning],
    errors: validation.error.length,
    warnings: validation.warning.length,
  };
};

export const GET_SCENARIOS_REQUEST = `${prefix} GET_SCENARIOS_REQUEST`;
export const GET_SCENARIOS_SUCCESS = `${prefix} GET_SCENARIOS_SUCCESS`;
export const GET_SCENARIOS_FAILURE = `${prefix} GET_SCENARIOS_FAILURE`;

const getScenariosRequest = () => ({ type: GET_SCENARIOS_REQUEST });
const getScenariosSuccess = payload => ({
  payload,
  type: GET_SCENARIOS_SUCCESS,
});
const getScenariosFailure = payload => ({
  payload,
  type: GET_SCENARIOS_FAILURE,
});

export const getScenarios = projectId => (dispatch) => {
  dispatch(getScenariosRequest());

  return request
    .get(`projects/${projectId}/scenarios`)
    .then(({ data }) => {
      const parsedScenarios = data.map(scenario => ({
        ...scenario,
        optimization: scenario.optimizations[0] || {},
      }));

      dispatch(getScenariosSuccess(parsedScenarios));

      return parsedScenarios;
    })
    .catch((error) => {
      dispatch(getScenariosFailure(error.response.data.message));
      dispatch(
        showNotificationError({
          title: i18n.t('Project scenarios loading failed'),
          error,
        })
      );

      throw error;
    });
};

export const CREATE_SCENARIO_REQUEST = `${prefix} CREATE_SCENARIO_REQUEST`;
export const CREATE_SCENARIO_SUCCESS = `${prefix} CREATE_SCENARIO_SUCCESS`;
export const CREATE_SCENARIO_FAILURE = `${prefix} CREATE_SCENARIO_FAILURE`;

const createScenarioRequest = () => ({ type: CREATE_SCENARIO_REQUEST });
const createScenarioSuccess = payload => ({
  payload,
  type: CREATE_SCENARIO_SUCCESS,
});
const createScenarioFailure = payload => ({
  payload,
  type: CREATE_SCENARIO_FAILURE,
});

export const createScenario = ({ name, projectId, ...rest }) => (dispatch) => {
  dispatch(createScenarioRequest());

  const json = {
    ...rest,
    scenario_type: 'custom',
  };

  const payload = {
    invalidate: true,
    json: JSON.stringify(json),
    name,
    optimization_algorithm: 't',
    projectId,
    status: 'in_progress',
  };

  return request
    .post('scenarios', payload)
    .then(({ data }) => {
      dispatch(createScenarioSuccess(data));

      return data;
    })
    .catch(({ response: { data } }) => {
      dispatch(createScenarioFailure(data));

      throw new SubmissionError({
        ...getFieldErrors(data),
        _error: data.message || getErrorMessage(),
      });
    });
};

export const UPDATE_SCENARIO_REQUEST = `${prefix} UPDATE_SCENARIO_REQUEST`;
export const UPDATE_SCENARIO_SUCCESS = `${prefix} UPDATE_SCENARIO_SUCCESS`;
export const UPDATE_SCENARIO_FAILURE = `${prefix} UPDATE_SCENARIO_FAILURE`;

const updateScenarioRequest = () => ({ type: UPDATE_SCENARIO_REQUEST });
const updateScenarioSuccess = payload => ({
  payload,
  type: UPDATE_SCENARIO_SUCCESS,
});
const updateScenarioFailure = payload => ({
  payload,
  type: UPDATE_SCENARIO_FAILURE,
});

export const updateScenario = (
  scenarioId,
  details,
  name,
  projectId
) => (dispatch) => {
  dispatch(updateScenarioRequest());

  const json = {
    ...details,
    scenario_type: 'custom',
  };

  const payload = {
    json: JSON.stringify(json),
    name,
    projectId,
  };

  return request
    .put(`scenarios/${scenarioId}`, payload)
    .then(({ data }) => {
      dispatch(updateScenarioSuccess(data));

      return data;
    })
    .catch(({ response: { data } }) => {
      dispatch(updateScenarioFailure(data));

      throw new SubmissionError({
        ...getFieldErrors(data),
        _error: data.message || getErrorMessage(),
      });
    });
};

export const DELETE_SCENARIO_REQUEST = `${prefix} DELETE_SCENARIO_REQUEST`;
export const DELETE_SCENARIO_SUCCESS = `${prefix} DELETE_SCENARIO_SUCCESS`;
export const DELETE_SCENARIO_FAILURE = `${prefix} DELETE_SCENARIO_FAILURE`;

const deleteScenarioRequest = () => ({ type: DELETE_SCENARIO_REQUEST });
const deleteScenarioSuccess = payload => ({
  payload,
  type: DELETE_SCENARIO_SUCCESS,
});
const deleteScenarioFailure = payload => ({
  payload,
  type: DELETE_SCENARIO_FAILURE,
});

export const deleteScenario = id => (dispatch) => {
  dispatch(deleteScenarioRequest());

  return request
    .delete(`scenarios/${id}`)
    .then(() => {
      dispatch(deleteScenarioSuccess());

      return {};
    })
    .catch((error) => {
      dispatch(deleteScenarioFailure(error.response.data.message));

      throw error;
    });
};

export const DUPLICATE_SCENARIO_REQUEST = `${prefix} DUPLICATE_SCENARIO_REQUEST`;
export const DUPLICATE_SCENARIO_SUCCESS = `${prefix} DUPLICATE_SCENARIO_SUCCESS`;
export const DUPLICATE_SCENARIO_FAILURE = `${prefix} DUPLICATE_SCENARIO_FAILURE`;

const duplicateScenarioRequest = () => ({ type: DUPLICATE_SCENARIO_REQUEST });
const duplicateScenarioSuccess = payload => ({
  payload,
  type: DUPLICATE_SCENARIO_SUCCESS,
});
const duplicateScenarioFailure = payload => ({
  payload,
  type: DUPLICATE_SCENARIO_FAILURE,
});

export const duplicateScenario = id => (dispatch) => {
  dispatch(duplicateScenarioRequest());

  return request
    .post(`scenarios/${id}/duplicate`)
    .then(({ data }) => {
      dispatch(duplicateScenarioSuccess(data));

      return data;
    })
    .catch((error) => {
      dispatch(duplicateScenarioFailure(error.response.data.message));

      throw error;
    });
};

export const GET_SCENARIO_DETAILS_REQUEST = `${prefix} GET_SCENARIO_DETAILS_REQUEST`;
export const GET_SCENARIO_DETAILS_SUCCESS = `${prefix} GET_SCENARIO_DETAILS_SUCCESS`;
export const GET_SCENARIO_DETAILS_FAILURE = `${prefix} GET_SCENARIO_DETAILS_FAILURE`;

const getScenarioDetailsRequest = () => ({
  type: GET_SCENARIO_DETAILS_REQUEST,
});
const getScenarioDetailsSuccess = payload => ({
  payload,
  type: GET_SCENARIO_DETAILS_SUCCESS,
});
const getScenarioDetailsFailure = payload => ({
  payload,
  type: GET_SCENARIO_DETAILS_FAILURE,
});

export const getScenarioDetails = id => (dispatch) => {
  dispatch(getScenarioDetailsRequest());

  return request
    .get(`scenarios/${id}`)
    .then(({ data }) => {
      const { optimizations, validations } = data;
      let validation = {};

      if (
        validations[0] &&
        validations[0].output &&
        validations[0].output.messages
      ) {
        validation = categorizeErrors(validations[0]);
      }

      const parsedData = {
        ...data,
        optimization: optimizations[0] || {},
        validation,
      };

      dispatch(getScenarioDetailsSuccess(parsedData));

      return parsedData;
    })
    .catch((error) => {
      dispatch(getScenarioDetailsFailure(error.response?.data.message));

      throw error;
    });
};

export const RESET_SCENARIO_DETAILS = `${prefix} RESET_SCENARIO_DETAILS`;

export const resetScenarioDetails = () => dispatch =>
  dispatch({ type: RESET_SCENARIO_DETAILS });

export const RUN_OPTIMIZATION_REQUEST = `${prefix} RUN_OPTIMIZATION_REQUEST`;
export const RUN_OPTIMIZATION_SUCCESS = `${prefix} RUN_OPTIMIZATION_SUCCESS`;
export const RUN_OPTIMIZATION_FAILURE = `${prefix} RUN_OPTIMIZATION_FAILURE`;

const runOptimizationRequest = () => ({ type: RUN_OPTIMIZATION_REQUEST });
const runOptimizationSuccess = payload => ({
  payload,
  type: RUN_OPTIMIZATION_SUCCESS,
});
const runOptimizationFailure = payload => ({
  payload,
  type: RUN_OPTIMIZATION_FAILURE,
});

const createOptimizationJob = async (scenarioId) => {
  const response = await request.post(
    'jobs/scenario_optimization',
    { scenarioId },
    { loader: false }
  );

  return response.data;
};

const runSingleOptimizationJob = (
  projectId,
  scenarioId,
  updateProgress
) => async (dispatch) => {
  dispatch(runOptimizationRequest());
  const createdJob = await createOptimizationJob(scenarioId);
  const { id: jobId } = createdJob;

  dispatch(blockingJobSetId(jobId));

  return pollJobUntilFinished(projectId, jobId, updateProgress)
    .then(({ data }) => {
      dispatch(runOptimizationSuccess(data));

      return data;
    })
    .catch((error) => {
      dispatch(setJobOutput(error.errorMessage));
      dispatch(runOptimizationFailure({ scenarioId }));

      throw error;
    });
};

export const runOptimization = (projectId, scenarioId) => async (dispatch) => {
  dispatch(blockingJobRequest());

  const updateProgress = (progress) => {
    dispatch(blockingJobProgress(progress));
  };

  try {
    const result = await dispatch(
      runSingleOptimizationJob(projectId, scenarioId, updateProgress)
    );

    dispatch(blockingJobSuccess());

    return result;
  } catch (error) {
    dispatch(blockingJobFailure());

    throw error;
  }
};

export const runOptimizationForScenarios = (projectId, scenarioIds) => async (
  dispatch,
  getState
) => {
  const numOfScenario = scenarioIds.length;
  const ratio = 1 / numOfScenario;

  dispatch(blockingJobRequest());

  const updateProgress = (index, progress) => {
    const base = index * 100 * ratio;

    dispatch(blockingJobProgress(base + progress * ratio));
  };

  const isGlobalJobRunning = jobsSelectors.getIsGlobalJobRunning(getState());

  try {
    // eslint-disable-next-line no-restricted-syntax
    for (const scenarioId of scenarioIds) {
      if (!isGlobalJobRunning) {
        break;
      }
      const index = scenarioIds.indexOf(scenarioId);

      try {
        // eslint-disable-next-line no-await-in-loop
        await dispatch(
          runSingleOptimizationJob(projectId, scenarioId, progress =>
            updateProgress(index, progress)
          )
        );
      } catch (error) {
        // if job has been explicitly cancelled, just break the loop
        // for backend/network errors, continue as normal
        if (error instanceof CancelledJobError) {
          break;
        }
      }
    }
    dispatch(blockingJobSuccess());
  } catch (error) {
    dispatch(blockingJobFailure());
  }
};

export const RUN_POSTPROCESSING_REQUEST = `${prefix} RUN_POSTPROCESSING_REQUEST`;
export const RUN_POSTPROCESSING_SUCCESS = `${prefix} RUN_POSTPROCESSING_SUCCESS`;
export const RUN_POSTPROCESSING_FAILURE = `${prefix} RUN_POSTPROCESSING_FAILURE`;

const runPostprocessingRequest = () => ({ type: RUN_POSTPROCESSING_REQUEST });
const runPostprocessingSuccess = payload => ({
  payload,
  type: RUN_POSTPROCESSING_SUCCESS,
});
const runPostprocessingFailure = payload => ({
  payload,
  type: RUN_POSTPROCESSING_FAILURE,
});

const createPostprocessingJob = async (payload) => {
  const result = await request.post('jobs/scenario_postprocessing', payload);

  return result.data;
};

const runPostprocessing = (
  projectId,
  payload,
  updateProgress
) => async (dispatch) => {
  dispatch(runPostprocessingRequest());

  const createdJob = await createPostprocessingJob(payload);

  const { id: jobId } = createdJob;

  dispatch(blockingJobSetId(jobId));

  return pollJobUntilFinished(projectId, jobId, updateProgress)
    .then(({ data }) => {
      dispatch(runPostprocessingSuccess(data));

      return data;
    })
    .catch((error) => {
      dispatch(setJobOutput(error.errorMessage));
      dispatch(runPostprocessingFailure(error.response.data.message));
    });
};

export const runSinglePostprocessing = (
  projectId,
  payload
) => async (dispatch) => {
  dispatch(blockingJobRequest());

  try {
    const updateProgress = (progress) => {
      dispatch(blockingJobProgress(progress));
    };

    await dispatch(runPostprocessing(projectId, payload, updateProgress));

    dispatch(blockingJobSuccess());
  } catch (error) {
    dispatch(blockingJobFailure());

    throw error;
  }
};

export const GET_OPTIMIZATION_REQUEST = `${prefix} GET_OPTIMIZATION_REQUEST`;
export const GET_OPTIMIZATION_SUCCESS = `${prefix} GET_OPTIMIZATION_SUCCESS`;
export const GET_OPTIMIZATION_FAILURE = `${prefix} GET_OPTIMIZATION_FAILURE`;

const getOptimizationRequest = () => ({ type: GET_OPTIMIZATION_REQUEST });
const getOptimizationSuccess = payload => ({
  payload,
  type: GET_OPTIMIZATION_SUCCESS,
});
const getOptimizationFailure = payload => ({
  payload,
  type: GET_OPTIMIZATION_FAILURE,
});

export const getOptimization = (projectId, scenarios) => (dispatch) => {
  dispatch(getOptimizationRequest());

  Promise.all(
    scenarios.map(scenario =>
      request.get(
        `jobs/optimization/project/${projectId}/scenario/${scenario.id}`
      )
    )
  )
    .then((response) => {
      const data = response.map(item => item.data);

      dispatch(getOptimizationSuccess(data));

      return data;
    })
    .catch((error) => {
      dispatch(
        getOptimizationFailure({
          error,
        })
      );

      throw error;
    });
};

export const GET_SCENARIO_ALLOCATIONS_REQUEST = `${prefix} GET_SCENARIO_ALLOCATIONS_REQUEST`;
export const GET_SCENARIO_ALLOCATIONS_SUCCESS = `${prefix} GET_SCENARIO_ALLOCATIONS_SUCCESS`;
export const GET_SCENARIO_ALLOCATIONS_FAILURE = `${prefix} GET_SCENARIO_ALLOCATIONS_FAILURE`;

const getScenarioAllocationsRequest = () => ({
  type: GET_SCENARIO_ALLOCATIONS_REQUEST,
});
const getScenarioAllocationsSuccess = payload => ({
  payload,
  type: GET_SCENARIO_ALLOCATIONS_SUCCESS,
});
const getScenarioAllocationsFailure = payload => ({
  payload,
  type: GET_SCENARIO_ALLOCATIONS_FAILURE,
});

export const getScenarioAllocations = scenarioId => (dispatch) => {
  dispatch(getScenarioAllocationsRequest());

  return request
    .get(`scenarios/${scenarioId}/allocations`)
    .then(({ data }) => {
      dispatch(
        getScenarioAllocationsSuccess({
          data,
          scenarioId,
        })
      );

      return data;
    })
    .catch((error) => {
      dispatch(getScenarioAllocationsFailure({ error }));
      dispatch(
        showNotificationError({
          title: i18n.t('Scenario allocations loading failed'),
          error,
        })
      );

      throw error;
    });
};

export const getParsedURLString = (object) => {
  return queryString.stringify(object);
};

export const GET_SCENARIO_KPIS_REQUEST = `${prefix} GET_SCENARIO_KPIS_REQUEST`;
export const GET_SCENARIO_KPIS_SUCCESS = `${prefix} GET_SCENARIO_KPIS_SUCCESS`;
export const GET_SCENARIO_KPIS_FAILURE = `${prefix} GET_SCENARIO_KPIS_FAILURE`;

const getScenarioKpiRequest = () => ({ type: GET_SCENARIO_KPIS_REQUEST });
const getScenarioKpiSuccess = payload => ({
  payload,
  type: GET_SCENARIO_KPIS_SUCCESS,
});
const getScenarioKpiFailure = payload => ({
  payload,
  type: GET_SCENARIO_KPIS_FAILURE,
});

export const getScenarioKpi = (projectId, scenarioIds) => (dispatch) => {
  dispatch(getScenarioKpiRequest());

  const urlParams = getParsedURLString({ scenarioIds, projectId });

  return request
    .get(`scenarios/kpi?${urlParams}`)
    .then(({ data }) => {
      dispatch(getScenarioKpiSuccess(data));

      return data;
    })
    .catch((error) => {
      dispatch(
        getScenarioKpiFailure({
          error,
        })
      );
      dispatch(
        showNotificationError({
          title: i18n.t('Scenario kpi loading failed'),
          error,
        })
      );

      throw error;
    });
};

export const CREATE_MANUAL_ALLOCATIONS_REQUEST = `${prefix} CREATE_MANUAL_ALLOCATIONS_REQUEST`;
export const CREATE_MANUAL_ALLOCATIONS_SUCCESS = `${prefix} CREATE_MANUAL_ALLOCATIONS_SUCCESS`;
export const CREATE_MANUAL_ALLOCATIONS_FAILURE = `${prefix} CREATE_MANUAL_ALLOCATIONS_FAILURE`;

const createManualAllocationsRequest = () => ({
  type: CREATE_MANUAL_ALLOCATIONS_REQUEST,
});
const createManualAllocationsSuccess = payload => ({
  payload,
  type: CREATE_MANUAL_ALLOCATIONS_SUCCESS,
});
const createManualAllocationsFailure = payload => ({
  payload,
  type: CREATE_MANUAL_ALLOCATIONS_FAILURE,
});

export const createManualAllocations = (scenarioId, payload) => (dispatch) => {
  dispatch(createManualAllocationsRequest());

  return request
    .put(`scenarios/${scenarioId}/manual-allocation`, payload)
    .then(({ data }) => {
      dispatch(
        createManualAllocationsSuccess({
          data,
        })
      );

      return data;
    })
    .catch((error) => {
      dispatch(
        createManualAllocationsFailure({
          error,
        })
      );
      dispatch(
        showNotificationError({
          title: i18n.t('It is not possible to create allocation'),
          error,
        })
      );

      throw error;
    });
};
