import {
  FORGOT_PASSWORD_FAILURE,
  FORGOT_PASSWORD_REQUEST,
  FORGOT_PASSWORD_SUCCESS,
  GET_ME_FAILURE,
  GET_ME_REQUEST,
  GET_ME_SUCCESS,
  SET_READY,
  SIGN_IN_FAILURE,
  SIGN_IN_REQUEST,
  SIGN_IN_SUCCESS,
  SIGN_OUT,
  UPDATE_PASSWORD_FAILURE,
  UPDATE_PASSWORD_REQUEST,
  UPDATE_PASSWORD_SUCCESS,
  UPDATE_USER_DETAILS_FAILURE,
  UPDATE_USER_DETAILS_REQUEST,
  UPDATE_USER_DETAILS_SUCCESS,
  SET_LANGUAGE_REQUEST,
} from '@actions/user';

const initialState = {
  error: '',
  isFetching: false,
  isReady: false,
  me: {},
  featureFlagIds: [],
  language: {
    label: 'English',
    mapCode: 'GB',
    translateCode: 'en',
    locale: 'en-GB',
  },
};

const getRequest = state => ({
  ...state,
  isFetching: true,
});

const getSuccess = (state, payload) => ({
  ...state,
  error: '',
  isFetching: false,
  me: payload || state.me,
  featureFlagIds: payload.featureFlagIds || [],
});

const getBasicSuccess = state => ({
  ...state,
  isFetching: false,
});

const getFailure = (state, payload) => ({
  ...state,
  error: payload,
  isFetching: false,
  isReady: true,
});

const signOut = () => ({
  ...initialState,
  isReady: true,
});

const setLanguage = (state, payload) => ({
  ...state,
  language: payload,
});

const setReady = state => ({
  ...state,
  isReady: true,
});

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_ME_FAILURE:
    case SIGN_IN_FAILURE:
      return getFailure(initialState, action.payload);
    case UPDATE_PASSWORD_FAILURE:
    case UPDATE_USER_DETAILS_FAILURE:
      return getFailure(state, action.payload);
    case GET_ME_REQUEST:
    case SIGN_IN_REQUEST:
    case UPDATE_PASSWORD_REQUEST:
    case UPDATE_USER_DETAILS_REQUEST:
      return getRequest(state);

    case SET_LANGUAGE_REQUEST:
      return setLanguage(state, action.payload);
    case GET_ME_SUCCESS:
    case SIGN_IN_SUCCESS:
      return getSuccess(state, action.payload);
    case UPDATE_PASSWORD_SUCCESS:
      return getBasicSuccess(state);
    case UPDATE_USER_DETAILS_SUCCESS:
      return getSuccess(state, {
        ...state.me,
        ...action.payload.data,
      });

    case SET_READY:
      return setReady(state);

    case SIGN_OUT:
      return signOut();

    case FORGOT_PASSWORD_REQUEST:
      return getRequest(state);
    case FORGOT_PASSWORD_SUCCESS:
      return getSuccess(state);
    case FORGOT_PASSWORD_FAILURE:
      return getFailure(state, action.payload);
    default:
      return state;
  }
};
