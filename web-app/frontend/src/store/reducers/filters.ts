import { FilterState } from '@common/components/Filter/types';
import { SET_FILTERS_STATE } from '@store/actions/filters';

export interface FilterStateMap {
  [filterKey: string]: FilterState;
}

export interface FiltersReducer {
  filterState: FilterStateMap;
}

const initialState: FiltersReducer = {
  filterState: {},
};

const getFilterState = (state: FiltersReducer, action: any) => {
  return {
    ...state,
    filterState: {
      ...state.filterState,
      [action.payload.key]: action.payload.state,
    },
  };
};

export default (state: FiltersReducer = initialState, action: any) => {
  switch (action.type) {
    case SET_FILTERS_STATE:
      return getFilterState(state, action);

    default:
      return state;
  }
};
