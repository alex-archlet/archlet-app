import {
  CREATE_BRANCH_FAILURE,
  CREATE_BRANCH_REQUEST,
  CREATE_BRANCH_SUCCESS,
  CREATE_COMPANY_FAILURE,
  CREATE_COMPANY_REQUEST,
  CREATE_COMPANY_SUCCESS,
  CREATE_USER_FAILURE,
  CREATE_USER_REQUEST,
  CREATE_USER_SUCCESS,
  DELETE_BRANCH_FAILURE,
  DELETE_BRANCH_REQUEST,
  DELETE_BRANCH_SUCCESS,
  DELETE_COMPANY_FAILURE,
  DELETE_COMPANY_REQUEST,
  DELETE_COMPANY_SUCCESS,
  DELETE_USER_FAILURE,
  DELETE_USER_REQUEST,
  DELETE_USER_SUCCESS,
  GET_BRANCHES_FAILURE,
  GET_BRANCHES_REQUEST,
  GET_BRANCHES_SUCCESS,
  GET_COMPANIES_FAILURE,
  GET_COMPANIES_REQUEST,
  GET_COMPANIES_SUCCESS,
  GET_ROLES_FAILURE,
  GET_ROLES_REQUEST,
  GET_ROLES_SUCCESS,
  GET_USERS_FAILURE,
  GET_USERS_REQUEST,
  GET_USERS_SUCCESS,
  UPDATE_BRANCH_FAILURE,
  UPDATE_BRANCH_REQUEST,
  UPDATE_BRANCH_SUCCESS,
  UPDATE_COMPANY_FAILURE,
  UPDATE_COMPANY_REQUEST,
  UPDATE_COMPANY_SUCCESS,
  UPDATE_USER_FAILURE,
  UPDATE_USER_REQUEST,
  UPDATE_USER_SUCCESS,
} from '@actions/admin';

const initialState = {
  branches: [],
  companies: [],
  error: '',
  isFetching: false,
  roles: [],
  users: [],
};

const getRequest = state => ({
  ...state,
  isFetching: true,
});

const getSuccess = (state, payload, field) => ({
  ...state,
  [field]: payload,
  isFetching: false,
});

const getFailure = (state, payload, field) => ({
  ...state,
  error: payload,
  [field]: initialState[field],
});

const createRequest = state => ({
  ...state,
  isFetching: true,
});

const createSuccess = (state, payload, field) => ({
  ...state,
  [field]: [
    ...state[field],
    payload,
  ],
  isFetching: false,
});

const createFailure = (state, payload) => ({
  ...state,
  error: payload,
});

const updateRequest = state => ({
  ...state,
  isFetching: true,
});

const updateSuccess = (state, payload, field) => ({
  ...state,
  [field]: state[field].map(entry => (
    entry.id === payload.id ?
      ({
        ...entry,
        ...payload.data,
      }) :
      entry
  )),
  isFetching: false,
});

const updateFailure = (state, payload) => ({
  ...state,
  error: payload,
});

const deleteRequest = state => ({
  ...state,
  isFetching: true,
});

const deleteSuccess = (state, payload, field) => ({
  ...state,
  error: '',
  [field]: state[field].filter(item => item.id !== payload),
  isFetching: false,
});

const deleteFailure = (state, payload) => ({
  ...state,
  error: payload,
});

export default (state = initialState, {
  payload,
  type,
}) => {
  switch (type) {
    case GET_COMPANIES_REQUEST:
    case GET_BRANCHES_REQUEST:
    case GET_ROLES_REQUEST:
    case GET_USERS_REQUEST:
      return getRequest(state);

    case GET_COMPANIES_FAILURE:
      return getFailure(state, payload, 'companies');
    case GET_BRANCHES_FAILURE:
      return getFailure(state, payload, 'branches');
    case GET_ROLES_FAILURE:
      return getFailure(state, payload, 'roles');
    case GET_USERS_FAILURE:
      return getFailure(state, payload, 'users');

    case GET_COMPANIES_SUCCESS:
      return getSuccess(state, payload, 'companies');
    case GET_BRANCHES_SUCCESS:
      return getSuccess(state, payload, 'branches');
    case GET_ROLES_SUCCESS:
      return getSuccess(state, payload, 'roles');
    case GET_USERS_SUCCESS:
      return getSuccess(state, payload, 'users');

    case CREATE_COMPANY_REQUEST:
    case CREATE_BRANCH_REQUEST:
    case CREATE_USER_REQUEST:
      return createRequest(state);

    case CREATE_COMPANY_FAILURE:
    case CREATE_BRANCH_FAILURE:
    case CREATE_USER_FAILURE:
      return createFailure(state, payload);

    case CREATE_COMPANY_SUCCESS:
      return createSuccess(state, payload, 'companies');
    case CREATE_BRANCH_SUCCESS:
      return createSuccess(state, payload, 'branches');
    case CREATE_USER_SUCCESS:
      return createSuccess(state, payload, 'users');

    case UPDATE_COMPANY_REQUEST:
    case UPDATE_BRANCH_REQUEST:
    case UPDATE_USER_REQUEST:
      return updateRequest(state);

    case UPDATE_COMPANY_FAILURE:
    case UPDATE_BRANCH_FAILURE:
    case UPDATE_USER_FAILURE:
      return updateFailure(state, payload);

    case UPDATE_COMPANY_SUCCESS:
      return updateSuccess(state, payload, 'companies');
    case UPDATE_BRANCH_SUCCESS:
      return updateSuccess(state, payload, 'branches');
    case UPDATE_USER_SUCCESS:
      return updateSuccess(state, payload, 'users');

    case DELETE_COMPANY_REQUEST:
    case DELETE_BRANCH_REQUEST:
    case DELETE_USER_REQUEST:
      return deleteRequest(state);

    case DELETE_COMPANY_FAILURE:
    case DELETE_BRANCH_FAILURE:
    case DELETE_USER_FAILURE:
      return deleteFailure(state, payload);

    case DELETE_COMPANY_SUCCESS:
      return deleteSuccess(state, payload, 'companies');
    case DELETE_BRANCH_SUCCESS:
      return deleteSuccess(state, payload, 'branches');
    case DELETE_USER_SUCCESS:
      return deleteSuccess(state, payload, 'users');

    default:
      return state;
  }
};
