import { TOGGLE, OPEN } from '@store/actions/sidebar';

export interface SidebarReducer {
  isSidebarOpen: boolean;
}

const defaultSidebarBehavior = true;

const initialState: SidebarReducer = {
  isSidebarOpen: defaultSidebarBehavior
};

export default (state: SidebarReducer = initialState, action) => {
  switch (action.type) {
    case TOGGLE:
      return {
        ...state,
        isSidebarOpen: !state.isSidebarOpen
      };

    case OPEN:
      return {
        ...state,
        isSidebarOpen: true
      };
    
    default:
      return state;
  }
};
