import {
  CREATE_PROJECT_FAILURE,
  CREATE_PROJECT_REQUEST,
  CREATE_PROJECT_SUCCESS,
  DELETE_PROJECT_FAILURE,
  DELETE_PROJECT_REQUEST,
  DELETE_PROJECT_SUCCESS,
  GET_PROJECT_BIDDING_TYPES_FAILURE,
  GET_PROJECT_BIDDING_TYPES_REQUEST,
  GET_PROJECT_BIDDING_TYPES_SUCCESS,
  GET_PROJECT_CATEGORIES_FAILURE,
  GET_PROJECT_CATEGORIES_REQUEST,
  GET_PROJECT_CATEGORIES_SUCCESS,
  GET_PROJECT_CURRENCIES_FAILURE,
  GET_PROJECT_CURRENCIES_REQUEST,
  GET_PROJECT_CURRENCIES_SUCCESS,
  GET_PROJECT_DETAILS_FAILURE,
  GET_PROJECT_DETAILS_REQUEST,
  GET_PROJECT_DETAILS_SUCCESS,
  GET_PROJECT_INFO_FAILURE,
  GET_PROJECT_INFO_REQUEST,
  GET_PROJECT_INFO_SUCCESS,
  GET_PROJECT_TYPES_FAILURE,
  GET_PROJECT_TYPES_REQUEST,
  GET_PROJECT_TYPES_SUCCESS,
  GET_PROJECT_UNITS_FAILURE,
  GET_PROJECT_UNITS_REQUEST,
  GET_PROJECT_UNITS_SUCCESS,
  GET_PROJECTS_FAILURE,
  GET_PROJECTS_REQUEST,
  GET_PROJECTS_SUCCESS,
  UPDATE_PROJECT_FAILURE,
  UPDATE_PROJECT_REQUEST,
  UPDATE_PROJECT_SUCCESS,
  GET_PROJECT_KPI_DEFS_REQUEST,
  GET_PROJECT_KPI_DEFS_SUCCESS,
  GET_PROJECT_KPI_DEFS_FAILURE,
  UPDATE_PROJECT_SETTINGS_SUCCESS,
} from '@actions/projects';

const initialState = {
  categories: [],
  currencies: [],
  error: '',
  isFetching: false,
  list: [],
  projectDetails: {},
  projectInfo: {},
  types: [],
  units: [],
  kpiDefs: [],
};

const getRequest = state => ({
  ...state,
  isFetching: true,
});

 
const getSuccess = (state, payload, field) => ({
  ...state,
  [field]: payload,
  isFetching: false,
});

const getMergeAndSuccess = (state, payload, field) => ({
  ...state,
  [field]: Object.assign(state[field], payload),
  isFetching: false,
});

const getFailure = (state, payload, field) => ({
  ...state,
  error: payload,
  [field]: initialState[field],
});

const updateFailure = (state, payload) => ({
  ...state,
  error: payload,
});

const deleteRequest = state => ({
  ...state,
  isFetching: true,
});

const deleteSuccess = (state, payload) => ({
  ...state,
  error: '',
  isFetching: false,
  list: state.list.filter(item => item.id !== payload),
});

const deleteFailure = (state, payload) => ({
  ...state,
  error: payload,
});

const createProjectSuccess = (state, payload) => ({
  ...state,
  error: '',
  isFetching: false,
  list: [payload, ...state.list],
});

const createProjectFailure = (state, payload) => ({
  ...state,
  error: payload,
  isFetching: false,
});

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case GET_PROJECTS_REQUEST:
    case GET_PROJECT_DETAILS_FAILURE:
    case GET_PROJECT_DETAILS_REQUEST:
    case GET_PROJECT_CATEGORIES_REQUEST:
    case GET_PROJECT_TYPES_REQUEST:
    case CREATE_PROJECT_REQUEST:
    case UPDATE_PROJECT_REQUEST:
    case GET_PROJECT_INFO_REQUEST:
    case GET_PROJECT_CURRENCIES_REQUEST:
    case GET_PROJECT_BIDDING_TYPES_REQUEST:
    case GET_PROJECT_UNITS_REQUEST:
    case GET_PROJECT_KPI_DEFS_REQUEST:
      return getRequest(state);

    case GET_PROJECT_INFO_FAILURE:
      return getFailure(state, payload, 'projectInfo');

    case GET_PROJECT_INFO_SUCCESS:
      return getSuccess(state, payload, 'projectInfo');

    case GET_PROJECT_KPI_DEFS_FAILURE:
      return getFailure(state, payload, 'kpiDefs');

    case GET_PROJECT_KPI_DEFS_SUCCESS:
      return getSuccess(state, payload, 'kpiDefs');

    case GET_PROJECTS_FAILURE:
      return getFailure(state, payload, 'list');

    case GET_PROJECTS_SUCCESS:
      return getSuccess(state, payload, 'list');

    case DELETE_PROJECT_FAILURE:
      return deleteFailure(state, payload);

    case DELETE_PROJECT_REQUEST:
      return deleteRequest(state);

    case DELETE_PROJECT_SUCCESS:
      return deleteSuccess(state, payload);

    case GET_PROJECT_DETAILS_SUCCESS:
      return getSuccess(state, payload, 'projectDetails');

    case GET_PROJECT_CATEGORIES_FAILURE:
      return getFailure(state, payload, 'categories');

    case GET_PROJECT_CATEGORIES_SUCCESS:
      return getSuccess(state, payload, 'categories');

    case GET_PROJECT_CURRENCIES_FAILURE:
      return getFailure(state, payload, 'currencies');

    case GET_PROJECT_CURRENCIES_SUCCESS:
      return getSuccess(state, payload, 'currencies');

    case GET_PROJECT_BIDDING_TYPES_FAILURE:
      return getFailure(state, payload, 'biddingTypes');

    case GET_PROJECT_BIDDING_TYPES_SUCCESS:
      return getSuccess(state, payload, 'biddingTypes');

    case GET_PROJECT_UNITS_FAILURE:
      return getFailure(state, payload, 'units');

    case GET_PROJECT_UNITS_SUCCESS:
      return getSuccess(state, payload, 'units');

    case GET_PROJECT_TYPES_FAILURE:
      return getFailure(state, payload, 'types');

    case GET_PROJECT_TYPES_SUCCESS:
      return getSuccess(state, payload, 'types');

    case CREATE_PROJECT_FAILURE:
      return createProjectFailure(state, payload);

    case CREATE_PROJECT_SUCCESS:
      return createProjectSuccess(state, payload);

    case UPDATE_PROJECT_SUCCESS:
      return getSuccess(state, payload, 'projectDetails');

    case UPDATE_PROJECT_FAILURE:
      return updateFailure(state, payload);

    case UPDATE_PROJECT_SETTINGS_SUCCESS: {
      return getMergeAndSuccess(state, payload, 'projectDetails');
    }

    default:
      return state;
  }
};
