import {
  REMOVE_SPINNER,
  SET_BACK_BUTTON,
  SET_PAGE_TITLE,
  SET_SPINNER,
} from '@actions/interface';

const initialState = {
  backButtonDestination: '',
  backButtonType: '',
  pageTitle: '',
  showSpinner: [],
  headerRouteListener: false,
};

const setPageTitle = (state, payload) => ({
  ...state,
  pageTitle: payload,
});

const setBackButton = (state, payload) => ({
  ...state,
  backButtonDestination: payload.destinationUrl,
  backButtonType: payload.type,
});

const setSpinner = state => ({
  ...state,
  showSpinner: [...state.showSpinner, true],
});

const removeSpinner = state => ({
  ...state,
  showSpinner: state.showSpinner.slice(0, state.showSpinner.length - 1),
});

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_PAGE_TITLE:
      return setPageTitle(state, action.payload);

    case SET_BACK_BUTTON:
      return setBackButton(state, action.payload);

    case SET_SPINNER:
      return setSpinner(state);

    case REMOVE_SPINNER:
      return removeSpinner(state);

    default:
      return state;
  }
};
