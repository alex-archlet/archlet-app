import { SET_PROJECT_PIVOT_TABLE_STATE } from '@store/actions/pivotTable';

type ProjectPivotTableResultColumn = {
  mean: number;
  min: number;
  max: number;
  sum: number;
};

type ProjectPivotTableColumn = string | number | ProjectPivotTableResultColumn;

export interface ProjectPivotTableRow {
  [columnId: string]: ProjectPivotTableColumn;
}

export interface ProjectPivotTableMap {
  [projectId: string]: ProjectPivotTableRow[];
}

export interface PivotTableReducer {
  pivotTableMap: ProjectPivotTableMap;
}

const initialState: PivotTableReducer = {
  pivotTableMap: {},
};

const getPivotTableState = (state: PivotTableReducer, action: any) => {
  return {
    ...state,
    pivotTableMap: {
      ...state.pivotTableMap,
      [action.payload.key]: action.payload.state,
    },
  };
};

export default (state: PivotTableReducer = initialState, action: any) => {
  switch (action.type) {
    case SET_PROJECT_PIVOT_TABLE_STATE:
      return getPivotTableState(state, action);

    default:
      return state;
  }
};
