import {
  CREATE_SCENARIO_FAILURE,
  CREATE_SCENARIO_REQUEST,
  CREATE_SCENARIO_SUCCESS,
  DELETE_SCENARIO_FAILURE,
  DELETE_SCENARIO_REQUEST,
  DUPLICATE_SCENARIO_FAILURE,
  DUPLICATE_SCENARIO_REQUEST,
  DUPLICATE_SCENARIO_SUCCESS,
  GET_OPTIMIZATION_FAILURE,
  GET_OPTIMIZATION_REQUEST,
  GET_OPTIMIZATION_SUCCESS,
  GET_SCENARIO_ALLOCATIONS_FAILURE,
  GET_SCENARIO_ALLOCATIONS_REQUEST,
  GET_SCENARIO_ALLOCATIONS_SUCCESS,
  GET_SCENARIO_KPIS_FAILURE,
  GET_SCENARIO_KPIS_REQUEST,
  GET_SCENARIO_KPIS_SUCCESS,
  GET_SCENARIO_DETAILS_FAILURE,
  GET_SCENARIO_DETAILS_REQUEST,
  GET_SCENARIO_DETAILS_SUCCESS,
  GET_SCENARIOS_FAILURE,
  GET_SCENARIOS_REQUEST,
  GET_SCENARIOS_SUCCESS,
  RESET_SCENARIO_DETAILS,
  RUN_OPTIMIZATION_FAILURE,
  RUN_OPTIMIZATION_REQUEST,
  RUN_OPTIMIZATION_SUCCESS,
  RUN_POSTPROCESSING_FAILURE,
  RUN_POSTPROCESSING_REQUEST,
  RUN_POSTPROCESSING_SUCCESS,
  UPDATE_SCENARIO_FAILURE,
  UPDATE_SCENARIO_REQUEST,
  UPDATE_SCENARIO_SUCCESS,
} from '@actions/projectScenarios';
import findIndex from 'lodash.findindex';
import find from 'lodash.find';

const initialState = {
  error: '',
  isFetching: false,
  list: [],
  postProcessing: {},
  scenarioAllocationsMap: {},
  scenarioKpis: {
    kpi: {},
    kpiDef: [],
  },
  scenarioDetails: {},
};

const getRequest = state => ({
  ...state,
  isFetching: true,
});

const getSuccess = (state, payload, field) => ({
  ...state,
  [field]: payload,
  isFetching: false,
});

const getFailure = (state, payload, field) => ({
  ...state,
  error: payload,
  [field]: initialState[field],
});

const getNewScenariosList = (scenariosList, optimization, scenarioId) => {
  const index = findIndex(
    scenariosList,
    scenario => scenario.id === scenarioId
  );

  if (index > -1) {
    const newScenario = {
      ...scenariosList[index],
      lastOptimization: new Date().toISOString(),
      optimization,
    };

    return [
      ...scenariosList.slice(0, index),
      newScenario,
      ...scenariosList.slice(index + 1),
    ];
  }

  return scenariosList;
};

const runOptimizationSuccess = (state, payload) => ({
  ...state,
  list: getNewScenariosList(state.list, payload, payload.scenarioId),
});

const runOptimizationFailure = (state, { scenarioId }) => ({
  ...state,
  list: getNewScenariosList(state.list, {}, scenarioId),
});

const getOptimizationSuccess = (state, payload) => ({
  ...state,
  list: state.list.map(scenario => ({
    ...scenario,
    optimization: find(payload, opt => opt.scenarioId === scenario.id) || {},
  })),
});

const updateScenarioSuccess = (state, payload) => ({
  ...state,
  scenarioDetails: {
    ...state.scenarioDetails,
    json: {
      ...state.scenarioDetails.json,
      ...payload.json,
    },
    name: payload.name || state.scenarioDetails.name,
  },
});

const resetScenarioDetails = state => ({
  ...state,
  scenarioDetails: initialState.scenarioDetails,
});

const getScenarioAllocationsSuccess = (state, { data, scenarioId }) => ({
  ...state,
  scenarioAllocationsMap: {
    ...state.scenarioAllocationsMap,
    [scenarioId]: data,
  },
});

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case GET_SCENARIOS_FAILURE:
      return getFailure(state, payload, 'list');

    case CREATE_SCENARIO_FAILURE:
    case GET_SCENARIO_DETAILS_FAILURE:
    case UPDATE_SCENARIO_FAILURE:
      return getFailure(state, payload, 'scenarioDetails');

    case GET_SCENARIOS_REQUEST:
    case CREATE_SCENARIO_REQUEST:
    case GET_SCENARIO_DETAILS_REQUEST:
    case GET_SCENARIO_ALLOCATIONS_REQUEST:
    case RUN_POSTPROCESSING_REQUEST:
    case GET_OPTIMIZATION_REQUEST:
    case RUN_OPTIMIZATION_REQUEST:
    case UPDATE_SCENARIO_REQUEST:
    case DELETE_SCENARIO_REQUEST:
    case DUPLICATE_SCENARIO_REQUEST:
    case GET_SCENARIO_KPIS_REQUEST:
      return getRequest(state);

    case GET_SCENARIOS_SUCCESS:
      return getSuccess(state, payload, 'list');

    case CREATE_SCENARIO_SUCCESS:
    case GET_SCENARIO_DETAILS_SUCCESS:
    case DUPLICATE_SCENARIO_SUCCESS:
      return getSuccess(state, payload, 'scenarioDetails');

    case GET_SCENARIO_KPIS_SUCCESS:
      return getSuccess(state, payload, 'scenarioKpis');
    case GET_SCENARIO_KPIS_FAILURE:
      return getFailure(state, payload, 'scenarioKpis');

    case RUN_POSTPROCESSING_FAILURE:
      return getFailure(state, payload, 'postProcessing');

    case RUN_POSTPROCESSING_SUCCESS:
      return getSuccess(state, payload, 'postProcessing');

    case RUN_OPTIMIZATION_SUCCESS:
      return runOptimizationSuccess(state, payload);

    case GET_OPTIMIZATION_SUCCESS:
      return getOptimizationSuccess(state, payload);

    case RUN_OPTIMIZATION_FAILURE:
    case GET_OPTIMIZATION_FAILURE:
    case GET_SCENARIO_ALLOCATIONS_FAILURE:
    case DELETE_SCENARIO_FAILURE:
    case DUPLICATE_SCENARIO_FAILURE:
      return runOptimizationFailure(state, payload);

    case UPDATE_SCENARIO_SUCCESS:
      return updateScenarioSuccess(state, payload);

    case RESET_SCENARIO_DETAILS:
      return resetScenarioDetails(state);

    case GET_SCENARIO_ALLOCATIONS_SUCCESS:
      return getScenarioAllocationsSuccess(state, payload);

    default:
      return state;
  }
};
