import includes from 'lodash.includes';
import find from 'lodash.find';

import {
  ADD_PROJECT_MEMBERS_FAILURE,
  ADD_PROJECT_MEMBERS_REQUEST,
  ADD_PROJECT_MEMBERS_SUCCESS,
  DELETE_PROJECT_MEMBER_FAILURE,
  DELETE_PROJECT_MEMBER_REQUEST,
  DELETE_PROJECT_MEMBER_SUCCESS,
  GET_PROJECT_MEMBERS_FAILURE,
  GET_PROJECT_MEMBERS_REQUEST,
  GET_PROJECT_MEMBERS_SUCCESS,
  UPDATE_PROJECT_MEMBER_FAILURE,
  UPDATE_PROJECT_MEMBER_REQUEST,
  UPDATE_PROJECT_MEMBER_SUCCESS,
} from '@actions/projectMembers';

const initialState = {
  list: [],
};

const getRequest = (state, field) => ({
  ...state,
  [field]: initialState[field],
  isFetching: true,
});

const getSuccess = (state, payload, field) => ({
  ...state,
  [field]: payload,
  isFetching: false,
});

const getFailure = (state, payload) => ({
  ...state,
  error: payload,
  isFetching: false,
});

const request = state => ({
  ...state,
  isFetching: true,
});

const deleteSuccess = (state, payload) => ({
  ...state,
  isFetching: false,
  list: state.list.filter(item => !includes(payload, item.id)),
});

const updateSuccess = (state, payload) => {
  const responseFromBackend =
    payload.map((item) => {
      const newItem = find(state.list, member => member.id === item.id);

      return {
        ...newItem,
        projectAccess: {
          ...newItem.projectAccess,
          accessType: item.accessType,
        },
      };
    });

  return {
    ...state,
    isFetching: false,
    list: state.list.reduce((prev, curr) => [
      ...prev,
      find(responseFromBackend, item => item.id === curr.id) || curr,
    ], []),
  };
};

const addSuccess = (state, payload) => ({
  ...state,
  error: '',
  isFetching: false,
  list: [
    ...state.list,
    ...payload,
  ],
});

export default (state = initialState, {
  payload,
  type,
}) => {
  switch (type) {
    case GET_PROJECT_MEMBERS_REQUEST:
      return getRequest(state, 'list');

    case GET_PROJECT_MEMBERS_FAILURE:
    case DELETE_PROJECT_MEMBER_FAILURE:
    case UPDATE_PROJECT_MEMBER_FAILURE:
    case ADD_PROJECT_MEMBERS_FAILURE:
      return getFailure(state, payload);

    case GET_PROJECT_MEMBERS_SUCCESS:
      return getSuccess(state, payload, 'list');

    case DELETE_PROJECT_MEMBER_REQUEST:
    case UPDATE_PROJECT_MEMBER_REQUEST:
    case ADD_PROJECT_MEMBERS_REQUEST:
      return request(state);

    case DELETE_PROJECT_MEMBER_SUCCESS:
      return deleteSuccess(state, payload);

    case UPDATE_PROJECT_MEMBER_SUCCESS:
      return updateSuccess(state, payload);

    case ADD_PROJECT_MEMBERS_SUCCESS:
      return addSuccess(state, payload);

    default:
      return state;
  }
};
