import {
  DELETE_DATA_FAILURE,
  DELETE_DATA_REQUEST,
  DELETE_DATA_SUCCESS,
  GET_DATA_FAILURE,
  GET_DATA_REQUEST,
  GET_DATA_SUCCESS,
  GET_DATA_VALIDITY_FAILURE,
  GET_DATA_VALIDITY_REQUEST,
  GET_DATA_VALIDITY_SUCCESS,
  GET_EXAMPLE_DATA_FAILURE,
  GET_EXAMPLE_DATA_REQUEST,
  GET_EXAMPLE_DATA_SUCCESS,
  GET_FILES_TYPES_FAILURE,
  GET_FILES_TYPES_REQUEST,
  GET_FILES_TYPES_SUCCESS,
  GET_PROJECT_FILES_FAILURE,
  GET_PROJECT_FILES_REQUEST,
  GET_PROJECT_FILES_SUCCESS,
  UPLOAD_FILES_FAILURE,
  UPLOAD_FILES_REQUEST,
  UPLOAD_FILES_SUCCESS,
  VALIDATE_DATA_FAILURE,
  VALIDATE_DATA_REQUEST,
  VALIDATE_DATA_SUCCESS,
  RESET_NEW_FILES_UPLOAD
} from '@actions/projectData';

const initialState = {
  data: {},
  error: '',
  exampleData: {},
  files: {},
  filesTypes: {},
  isFetching: false,
  validation: {},
  isNewFilesUpload: false,
};

const getRequest = (state, field) => ({
  ...state,
  [field]: initialState[field],
  isFetching: true,
});

const getSuccess = (state, payload, field) => ({
  ...state,
  [field]: payload,
  isFetching: false,
});

const getFailure = (state, payload) => ({
  ...state,
  error: payload,
  isFetching: false,
});

const uploadFilesRequest = state => ({
  ...state,
  isFetching: true,
});

const getFilesSuccess = (state, payload, field) => ({
  ...state,
  [field]: {
    ...state[field],
    ...payload,
  },
  isFetching: false,
});

const uploadFilesSuccess = (state, payload, field) => ({
  ...state,
  [field]: {
    ...state[field],
    ...payload,
  },
  isFetching: false,
  isNewFilesUpload: true
});

const uploadFilesFailure = (state, payload) => ({
  ...state,
  error: payload,
  isFetching: false,
});

const deleteDataRequest = state => ({
  ...state,
  isFetching: true,
});

const deleteDataSuccess = (state, payload) => {
  const files = { ...state.files };
  const validation = { ...state.validation };

  delete files[payload];
  delete validation[payload];

  return {
    ...state,
    files,
    validation,
  };
};

const deleteDataFailure = (state, payload) => ({
  ...state,
  error: payload,
  isFetching: false,
});

export default (state = initialState, {
  payload,
  type,
}) => {
  switch (type) {
    case GET_PROJECT_FILES_REQUEST:
      return getRequest(state, 'files');

    case UPLOAD_FILES_REQUEST:
      return uploadFilesRequest(state);

    case GET_PROJECT_FILES_SUCCESS:
      return getFilesSuccess(state, payload, 'files');
    
    case UPLOAD_FILES_SUCCESS:
      return uploadFilesSuccess(state, payload, 'files');
    
    case RESET_NEW_FILES_UPLOAD: {
      return {
        ...state,
        isNewFilesUpload: false
      }
    }

    case GET_PROJECT_FILES_FAILURE:
    case UPLOAD_FILES_FAILURE:
    case VALIDATE_DATA_FAILURE:
    case GET_DATA_VALIDITY_FAILURE:
      return uploadFilesFailure(state, payload);

    case GET_DATA_REQUEST:
      return getRequest(state, 'data');

    case GET_DATA_SUCCESS:
      return getSuccess(state, payload, 'data');

    case GET_DATA_FAILURE:
    case GET_EXAMPLE_DATA_FAILURE:
    case GET_FILES_TYPES_FAILURE:
      return getFailure(state, payload);

    case GET_EXAMPLE_DATA_REQUEST:
      return getRequest(state, 'exampleData');

    case GET_EXAMPLE_DATA_SUCCESS:
      return getSuccess(state, payload, 'exampleData');

    case VALIDATE_DATA_SUCCESS:
    case GET_DATA_VALIDITY_SUCCESS:
      return uploadFilesSuccess(state, payload, 'validation');

    case VALIDATE_DATA_REQUEST:
    case GET_DATA_VALIDITY_REQUEST:
      return getRequest(state, 'validation');

    case DELETE_DATA_REQUEST:
      return deleteDataRequest(state);

    case DELETE_DATA_SUCCESS:
      return deleteDataSuccess(state, payload);

    case DELETE_DATA_FAILURE:
      return deleteDataFailure(state, payload);

    case GET_FILES_TYPES_REQUEST:
      return getRequest(state, 'filesTypes');

    case GET_FILES_TYPES_SUCCESS:
      return getSuccess(state, payload, 'filesTypes');

    default:
      return state;
  }
};
