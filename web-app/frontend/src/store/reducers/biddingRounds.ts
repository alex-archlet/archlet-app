import {
  CREATE_DEMANDS_OFFERS_FAILURE,
  CREATE_OR_UPDATE_COST_CALCULATION_FAILURE,
  CREATE_OR_UPDATE_COST_CALCULATION_REQUEST,
  CREATE_OR_UPDATE_COST_CALCULATION_SUCCESS,
  CREATE_ROUND_FAILURE,
  CREATE_ROUND_REQUEST,
  CREATE_ROUND_SUCCESS,
  DELETE_BID_FAILURE,
  DELETE_BID_REQUEST,
  DELETE_BID_SUCCESS,
  DELETE_COLUMN_FAILURE,
  DELETE_COLUMN_REQUEST,
  DELETE_COLUMN_SUCCESS,
  DELETE_ROUND_FAILURE,
  DELETE_ROUND_REQUEST,
  DELETE_ROUND_SUCCESS,
  DELETE_SUPPLIER_FAILURE,
  DELETE_SUPPLIER_REQUEST,
  DELETE_SUPPLIER_SUCCESS,
  EXPRESSION_EXAMPLE_SUCCESS,
  GET_COLUMN_TYPES_FAILURE,
  GET_COLUMN_TYPES_REQUEST,
  GET_COLUMN_TYPES_SUCCESS,
  GET_DEMANDS_OFFERS_FAILURE,
  GET_DEMANDS_OFFERS_REQUEST,
  GET_INVITED_SUPPLIERS_LIST_FAILURE,
  GET_INVITED_SUPPLIERS_LIST_REQUEST,
  GET_INVITED_SUPPLIERS_LIST_SUCCESS,
  GET_OFFER_PROCESSED_FAILURE,
  GET_OFFER_PROCESSED_REQUEST,
  GET_ROUND_DETAILS_FAILURE,
  GET_ROUND_DETAILS_REQUEST,
  GET_ROUND_DETAILS_SUCCESS,
  GET_ROUND_TEMPLATES_FAILURE,
  GET_ROUND_TEMPLATES_REQUEST,
  GET_ROUND_TEMPLATES_SUCCESS,
  GET_ROUNDS_LIST_FAILURE,
  GET_ROUNDS_LIST_REQUEST,
  GET_ROUNDS_LIST_SUCCESS,
  GET_TABLE_CONTENT_FAILURE,
  GET_TABLE_CONTENT_REQUEST,
  GET_TABLE_CONTENT_SUCCESS,
  GET_TABLE_DESIGN_FAILURE,
  GET_TABLE_DESIGN_REQUEST,
  GET_TABLE_DESIGN_SUCCESS,
  GET_TEMPLATES_SUPPLIERS_LIST_FAILURE,
  GET_TEMPLATES_SUPPLIERS_LIST_REQUEST,
  GET_TEMPLATES_SUPPLIERS_LIST_SUCCESS,
  INVITE_SUPPLIER_FAILURE,
  INVITE_SUPPLIER_REQUEST,
  INVITE_SUPPLIER_SUCCESS,
  INVITE_SUPPLIERS_BY_TEMPLATE_FAILURE,
  INVITE_SUPPLIERS_BY_TEMPLATE_REQUEST,
  OFFERS_PROCESSING_SUCCESS,
  SET_COLUMNS,
  SET_GRID,
  UPDATE_BID_IMPORT_FILE,
  UPDATE_BIDDING_SETTINGS_FAILURE,
  UPDATE_BIDDING_SETTINGS_REQUEST,
  UPDATE_BIDDING_SETTINGS_SUCCESS,
  UPDATE_ROUND_FAILURE,
  UPDATE_ROUND_REQUEST,
  UPDATE_ROUND_SUCCESS,
  UPDATE_TABLE_DESIGN_FAILURE,
  UPDATE_TABLE_DESIGN_REQUEST,
  UPDATE_TABLE_DESIGN_SUCCESS,
  UPLOAD_BID_SUCCESS,
} from '@actions/biddingRounds';
import { arrayToMap } from '../../utils/array';

export interface BiddingRoundListModel {
  id: string;
  name: string;
  offersCount: number;
  suppliersCount: number;
  statusName: string;
  createdAt: string;
  updatedAt: string;
  endAt: string;
  startAt: string;
  reminderAt: string;
}
// TODO: Define other keys when you touch them
export interface BiddingRoundsReducer {
  list: BiddingRoundListModel[];
}

const initialState = {
  bidImportFilesMap: {},
  changed: false,
  columnTypes: [],
  details: {},
  error: '',
  invitedSuppliers: [],
  isFetching: false,
  jobOutput: {},
  list: [],
  roundTemplates: [],
  tableContent: [],
  tableDesign: [
    {
      id: '0',
      inputBy: 'supplier',
      isMandatory: true,
      isVisible: true,
      name: 'Item',
      typeId: 0,
    },
  ],
  templatesSuppliers: [],
};

const request = state => ({
  ...state,
  isFetching: true,
});

const getSuccess = (state, payload, field) => ({
  ...state,
  [field]: payload,
  isFetching: false,
});

const getDetailsSuccess = (state, details) => ({
  ...state,
  bidImportFilesMap: arrayToMap(details.bidImportFiles),
  details,
  isFetching: false,
});

const getDetailsTrackChangedSuccess = (state, details) => ({
  ...state,
  bidImportFilesMap: arrayToMap(details.bidImportFiles),
  changed: true,
  details,
  isFetching: false,
});

const getFailure = (state, payload, field) => ({
  ...state,
  error: payload,
  [field]: initialState[field],
});

const createSuccess = (state, payload, field) => ({
  ...state,
  [field]: [...state[field], payload],
  isFetching: false,
});

const createFailure = (state, payload) => ({
  ...state,
  error: payload,
});

const updateSuccess = (state, payload, field) => ({
  ...state,
  [field]: state[field].map(item =>
    item.id === payload.id
      ? {
          ...item,
          ...payload,
        }
      : item
  ),
  isFetching: false,
});

const updateChangedSuccess = state => ({
  ...state,
  changed: true,
});
const updateNotChangedSuccess = state => ({
  ...state,
  changed: false,
});

const createOrUpdateSuccess = (state, payload, field) => {
  if (state[field].find(item => item.id === payload.id)) {
    return updateSuccess(state, payload, field);
  }

  return createSuccess(state, payload, field);
};

const deleteSuccess = (state, id, field) => ({
  ...state,
  [field]: state[field].filter(item => item.id !== id),
  isFetching: false,
});

const updateFailure = (state, payload) => ({
  ...state,
  error: payload,
});

// Changes a value in the map based on the provided key
// map[< payload.id >] = payload
const getUpdateMapItem = (state, payload, field, key = 'id') => ({
  ...state,
  [field]: {
    ...state[field],
    [payload[key]]: payload,
  },
});

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case GET_ROUNDS_LIST_REQUEST:
    case GET_ROUND_DETAILS_REQUEST:
    case GET_ROUND_TEMPLATES_REQUEST:
    case CREATE_ROUND_REQUEST:
    case UPDATE_ROUND_REQUEST:
    case GET_TABLE_DESIGN_REQUEST:
    case GET_COLUMN_TYPES_REQUEST:
    case UPDATE_TABLE_DESIGN_REQUEST:
    case DELETE_COLUMN_REQUEST:
    case GET_TABLE_CONTENT_REQUEST:
    case GET_OFFER_PROCESSED_REQUEST:
    case DELETE_ROUND_REQUEST:
    case CREATE_OR_UPDATE_COST_CALCULATION_REQUEST:
    case DELETE_SUPPLIER_REQUEST:
    case GET_INVITED_SUPPLIERS_LIST_REQUEST:
    case GET_TEMPLATES_SUPPLIERS_LIST_REQUEST:
    case INVITE_SUPPLIER_REQUEST:
    case INVITE_SUPPLIERS_BY_TEMPLATE_REQUEST:
    case DELETE_BID_REQUEST:
    case UPDATE_BIDDING_SETTINGS_REQUEST:
    case GET_DEMANDS_OFFERS_REQUEST:
      return request(state);

    case CREATE_DEMANDS_OFFERS_FAILURE:
      return createFailure(state, payload);

    case UPDATE_BIDDING_SETTINGS_FAILURE:
      return getFailure(state, payload, 'settings');

    case GET_ROUNDS_LIST_FAILURE:
      return getFailure(state, payload, 'list');

    case GET_TABLE_DESIGN_FAILURE:
      return getFailure(state, payload, 'tableDesign');

    case GET_OFFER_PROCESSED_FAILURE:
    case GET_TABLE_CONTENT_FAILURE:
      return getFailure(state, payload, 'tableContent');

    case GET_DEMANDS_OFFERS_FAILURE:
      return getFailure(state, payload, 'tableContent');

    case GET_COLUMN_TYPES_FAILURE:
      return getFailure(state, payload, 'columnTypes');
    case GET_ROUND_DETAILS_FAILURE:
      return getFailure(state, payload, 'details');

    case GET_ROUND_TEMPLATES_FAILURE:
      return getFailure(state, payload, 'roundTemplates');

    case GET_INVITED_SUPPLIERS_LIST_FAILURE:
      return getFailure(state, payload, 'invitedSuppliers');

    case GET_TEMPLATES_SUPPLIERS_LIST_FAILURE:
      return getFailure(state, payload, 'templatesSuppliers');

    case GET_ROUNDS_LIST_SUCCESS:
      return getSuccess(state, payload, 'list');

    case GET_TABLE_DESIGN_SUCCESS:
      return getSuccess(state, payload, 'tableDesign');

    case UPDATE_BIDDING_SETTINGS_SUCCESS:
      return getSuccess(state, payload, 'settings');

    case GET_INVITED_SUPPLIERS_LIST_SUCCESS:
      return getSuccess(state, payload, 'invitedSuppliers');

    case GET_TEMPLATES_SUPPLIERS_LIST_SUCCESS:
      return getSuccess(state, payload, 'templatesSuppliers');

    case INVITE_SUPPLIER_SUCCESS:
      return createSuccess(state, payload, 'invitedSuppliers');
    case INVITE_SUPPLIER_FAILURE:
    case INVITE_SUPPLIERS_BY_TEMPLATE_FAILURE:
      return createFailure(state, payload);

    case DELETE_SUPPLIER_SUCCESS:
      return deleteSuccess(state, payload, 'invitedSuppliers');
    case DELETE_SUPPLIER_FAILURE:
      return createFailure(state, payload);

    case GET_TABLE_CONTENT_SUCCESS:
      return createSuccess(state, payload, 'tableContent');

    case GET_COLUMN_TYPES_SUCCESS:
      return getSuccess(state, payload, 'columnTypes');
    case EXPRESSION_EXAMPLE_SUCCESS:
      return getSuccess(state, payload, 'jobOutput');

    case GET_ROUND_DETAILS_SUCCESS:
      return getDetailsSuccess(state, payload);
    case DELETE_BID_SUCCESS:
      return getDetailsTrackChangedSuccess(state, payload);

    case GET_ROUND_TEMPLATES_SUCCESS:
      return getSuccess(state, payload, 'roundTemplates');

    case CREATE_ROUND_SUCCESS:
      return createSuccess(state, payload, 'list');
    case CREATE_ROUND_FAILURE:
      return createFailure(state, payload);

    case CREATE_OR_UPDATE_COST_CALCULATION_SUCCESS:
      return createOrUpdateSuccess(state, payload, 'tableDesign');
    case CREATE_OR_UPDATE_COST_CALCULATION_FAILURE:
      return createFailure(state, payload);

    case UPDATE_ROUND_SUCCESS:
      return updateSuccess(state, payload, 'list');

    case UPDATE_TABLE_DESIGN_SUCCESS:
    case SET_COLUMNS:
      return getSuccess(state, payload, 'tableDesign');

    case SET_GRID:
      return getSuccess(state, payload, 'tableContent');

    case DELETE_COLUMN_SUCCESS:
      return deleteSuccess(state, payload, 'tableDesign');

    case DELETE_ROUND_SUCCESS:
      return deleteSuccess(state, payload, 'list');

    case UPDATE_ROUND_FAILURE:
    case UPDATE_TABLE_DESIGN_FAILURE:
    case DELETE_COLUMN_FAILURE:
    case DELETE_ROUND_FAILURE:
    case DELETE_BID_FAILURE:
      return updateFailure(state, payload);

    case UPDATE_BID_IMPORT_FILE:
      return getUpdateMapItem(state, payload, 'bidImportFilesMap', 'id');

    case UPLOAD_BID_SUCCESS:
      return updateChangedSuccess(state);

    case OFFERS_PROCESSING_SUCCESS:
      return updateNotChangedSuccess(state);

    default:
      return state;
  }
};
