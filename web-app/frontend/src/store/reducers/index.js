import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { reducer as notifications } from 'react-notification-system-redux';
 
import adminReducer from '@reducers/admin';
import biddingRoundsReducer from '@reducers/biddingRounds';
import filtersReducer from '@reducers/filters';
import interfaceReducer from '@reducers/interface';
import jobsReducer from '@reducers/jobs';
import pivotTableReducer from '@reducers/pivotTable';
import projectDataReducer from '@reducers/projectData';
import projectMembersReducer from '@reducers/projectMembers';
import projectScenariosReducer from '@reducers/projectScenarios';
import projectsReducer from '@reducers/projects';
import userReducer from '@reducers/user';
import sidebarReducer from '@reducers/sidebar';

export default combineReducers({
  admin: adminReducer,
  biddingRounds: biddingRoundsReducer,
  filters: filtersReducer,
  form: formReducer,
  interface: interfaceReducer,
  jobs: jobsReducer,
  pivotTable: pivotTableReducer,
  projectData: projectDataReducer,
  projectMembers: projectMembersReducer,
  projects: projectsReducer,
  projectScenarios: projectScenariosReducer,
  user: userReducer,
  notifications,
  sidebar: sidebarReducer,
});
