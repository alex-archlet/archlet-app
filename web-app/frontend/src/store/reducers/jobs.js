import {
  BLOCKING_JOB_CLOSE,
  BLOCKING_JOB_ERROR,
  BLOCKING_JOB_PROGRESS,
  BLOCKING_JOB_REQUEST,
  BLOCKING_JOB_SET_ID,
  BLOCKING_JOB_SUCCESS,
  SET_JOB_OUTPUT,
} from '@actions/jobs';

const initialState = {
  globalJobId: null,
  globalJobProgress: 0,
  isGlobalJobRunning: false,
  jobOutput: null,
};

const setGlobalJobProgress = (state, payload) => ({
  ...state,
  globalJobProgress: payload,
});

const setGlobalJobId = (state, payload) => ({
  ...state,
  globalJobId: payload,
});

const startGlobalJob = state => ({
  ...state,
  globalJobProgress: 0,
  isGlobalJobRunning: true,
});

const endGlobalJob = state => ({
  ...state,
  globalJobId: null,
  isGlobalJobRunning: false,
});

const setJobOutput = (state, payload) => ({
  ...state,
  jobOutput: payload,
});

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_JOB_OUTPUT:
      return setJobOutput(state, action.payload);

    case BLOCKING_JOB_REQUEST:
      return startGlobalJob(state);

    case BLOCKING_JOB_SUCCESS:
    case BLOCKING_JOB_ERROR:
    case BLOCKING_JOB_CLOSE:
      return endGlobalJob(state);

    case BLOCKING_JOB_SET_ID:
      return setGlobalJobId(state, action.payload);

    case BLOCKING_JOB_PROGRESS:
      return setGlobalJobProgress(state, action.payload);

    default:
      return state;
  }
};
