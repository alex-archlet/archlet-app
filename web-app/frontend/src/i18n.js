/* eslint-disable quote-props */
import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import { initReactI18next } from 'react-i18next';
import en from './locales/en/en.json';
import de from './locales/de/de.json';

export default i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    debug: false,
    defaultNS: 'translation',

    fallbackLng: 'en',

    interpolation: {
      escapeValue: false,
    },

    keySeparator: false,

    ns: ['translation'],

    resources: {
      de: {
        translation: {
          ...de,
        },
      },
      en: {
        translation: {
          ...en,
        },
      },
    },
  });
