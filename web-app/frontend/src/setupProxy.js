// eslint-disable-next-line @typescript-eslint/no-var-requires
const proxy = require('http-proxy-middleware');

module.exports = function func(app) {
  // Express (REST API) server
  app.use(proxy('/api', {
    logLevel: 'debug',
    target: `http://backend:${4201 || process.env.BACKEND_REST_API_SERVER_PORT}/`,
  }));
};
