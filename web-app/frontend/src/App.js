import React, { useEffect } from 'react';
import {
  Redirect,
  Route,
  BrowserRouter as Router,
  Switch,
} from 'react-router-dom';
import { connect, useStore } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { getMe, setReady } from '@actions/user';
import { userSelectors } from '@selectors/user';
import { DndProvider } from 'react-dnd';
import Backend from 'react-dnd-html5-backend';
import { QueryParamProvider } from 'use-query-params';

import { getFileTypes } from '@actions/projectData';
import { interfaceSelectors } from '@store/selectors/interface';
import { Spinner } from '@common/components/Spinner/Spinner';
import { ScrollToTopOnMount } from '@common/components/ScrollToTop/ScrollToTop';
import { Header } from '@common/containers/Header/Header';
import { MainLayout } from '@common/containers/Layout/Main';
import { PrivateRoute } from '@common/components/PrivateRoute/PrivateRoute';
import { PublicRoute } from '@common/components/PublicRoute/PublicRoute';
import { BundleUpdater } from '@common/containers/BundleUpdater/BundleUpdater';
import { ErrorBoundary } from '@common/containers/ErrorBoundary/ErrorBoundary';
import { initRequestInterceptors } from '@utils/requestInterceptor';

import { Notification } from '@common/components/Notification/Notification';
import { SignIn } from './routes/SignIn/containers/SignIn';
import { SsoError } from './routes/SsoError/containers/SsoError';
import { UserSettingsContainer } from './routes/UserSettings/containers/UserSettings';
import { AdminLayout } from './routes/Admin/components/AdminLayout/AdminLayout';
import { ProjectsRouter } from './routes/Projects/ProjectsRouter';
import { ForgotPassword } from './routes/ForgotPassword/containers/ForgotPassword';
import { getProjectStatics } from './store/actions/projects';
import { version } from '../package.json';
import 'core-js/stable/object/is';
// support flat method for Edge (16-18)
import 'core-js/features/array/flat';

global.appVersion = version;

function App({
  getFileTypesAction,
  getMeAction,
  getProjectStaticsAction,
  isAuthenticated,
  isAdmin,
  isReady,
  isSpinnerVisible,
  setReadyAction,
}) {
  useEffect(() => {
    if (isAuthenticated) {
      getFileTypesAction();
    }
    if (!isAuthenticated) {
      getMeAction().then(() => {
        getProjectStaticsAction();
        setReadyAction();
      });
    }
  }, [isAuthenticated]);

  const store = useStore();
  useEffect(() => {
    initRequestInterceptors(store);
  }, []);

  return (
    <div className="App">
      {isReady && (
        <Router>
          <ErrorBoundary>
            <QueryParamProvider ReactRouterRoute={Route}>
              <DndProvider backend={Backend}>
                <ScrollToTopOnMount />
                {isSpinnerVisible && <Spinner />}
                <Header />
                <MainLayout>
                  <Switch>
                    <PrivateRoute
                      path="/projects"
                      component={ProjectsRouter}
                      isAuthorized={isAuthenticated}
                    />
                    <PrivateRoute
                      path="/admin"
                      component={AdminLayout}
                      isAuthorized={isAuthenticated && isAdmin}
                    />
                    <PublicRoute
                      path="/sign-in"
                      component={SignIn}
                      isAuthorized={isAuthenticated}
                    />
                    <Route path="/sso-error" component={SsoError} />
                    <Route path="/forgot-password" component={ForgotPassword} />
                    <PrivateRoute
                      path="/user-settings"
                      component={UserSettingsContainer}
                      isAuthorized={isAuthenticated}
                    />
                    <Route
                      path="/"
                      exact
                      render={() => (
                        <Redirect
                          to={isAuthenticated ? '/projects' : '/sign-in'}
                        />
                      )}
                    />
                  </Switch>
                </MainLayout>
              </DndProvider>
            </QueryParamProvider>
          </ErrorBoundary>
        </Router>
      )}
      <BundleUpdater />
      <Notification />
    </div>
  );
}

App.propTypes = {
  getFileTypesAction: PropTypes.func.isRequired,
  getMeAction: PropTypes.func.isRequired,
  getProjectStaticsAction: PropTypes.func.isRequired,
  isAdmin: PropTypes.bool.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  isReady: PropTypes.bool.isRequired,
  isSpinnerVisible: PropTypes.bool.isRequired,
  setReadyAction: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  isAdmin: userSelectors.isAdmin(state),
  isAuthenticated: userSelectors.isAuthenticated(state),
  isReady: userSelectors.isReady(state),
  isSpinnerVisible: interfaceSelectors.isSpinnerVisible(state),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getFileTypesAction: getFileTypes,
      getMeAction: getMe,
      getProjectStaticsAction: getProjectStatics,
      setReadyAction: setReady,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(App);
