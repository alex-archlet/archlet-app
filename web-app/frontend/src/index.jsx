import 'react-app-polyfill/ie11';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ThemeProvider } from '@material-ui/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ErrorFallback } from '@common/containers/ErrorBoundary/ErrorFallback';
import { i18n } from '@utils/translate';

import configureStore from '@store/store';
import { theme } from '@utils/uiTheme';

import { isIE } from 'react-device-detect';
import App from './App';
import * as serviceWorker from './serviceWorker';

import './utils/polyfills';
import './i18n';
import './sentry';
import './index.css'; 

export const store = configureStore();

const renderIE = () => {
  ReactDOM.render(
    <ErrorFallback
      title={i18n.t('Hi there. You’re using an outdated browser')}
      message={i18n.t(
        'For a safer and faster user experience use the latest version of a modern browser like Chrome, Firefox, Safari, Opera, or Edge.'
      )}
      isResetPossible={false}
    />,
    document.getElementById('root')
  );
};

const renderApp = () => {
  ReactDOM.render(
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <CssBaseline>
          <App />
        </CssBaseline>
      </ThemeProvider>
    </Provider>,
    document.getElementById('root')
  );
};

const renderContent = () => {
  if (isIE) {
    renderIE();
  } else {
    renderApp();
  }
};

renderContent();

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
