import React from 'react';
import styled from 'styled-components';

import { Box, Card } from '@material-ui/core';
import { useCustomTranslation } from '@utils/translate';
import { Button } from '@common/components/Button/Button';
import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';

const StyledOuterContainer = styled(Box)`
  height: calc(100vh - 64px);
  background-color: #fbfbfb;
`;

const StyledCard = styled(Card)`
  display: flex;
  flex-direction: column;
  width: 410px;
  padding: 40px 50px 60px;
  position: relative;
`;

export const ErrorCard = ({ onGoToHome }) => {
  const { t } = useCustomTranslation();

  return (
    <StyledOuterContainer
      display="flex"
      justifyContent="center"
      flexDirection="column"
      alignItems="center"
    >
      <StyledCard elevation={0}>
        <h1>{t('SSO login failed')}</h1>
        <p>
          {/* eslint-disable-next-line max-len */}
          {t( 'The user does not exist or is not allowed to log in with the used login method. If this is a mistake and you should have access, please contact the Archlet support at: ')}
          <a href="mailto:support@archlet.ch">support@archlet.ch</a>
        </p>
        <VerticalSpacer height={16} />
        <Button onClick={onGoToHome}>{t('Back to home')}</Button>
      </StyledCard>
    </StyledOuterContainer>
  );
};
