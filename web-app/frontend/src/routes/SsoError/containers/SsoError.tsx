import React from 'react';
import { useHistory } from 'react-router-dom';

import { ErrorCard } from '../components/ErrorCard/ErrorCard';

export const SsoError: React.FC<void> = () => {
  const history = useHistory();

  const onGoToHome = () => {
    history.push('/');
  };

  return (
    <ErrorCard
      onGoToHome={onGoToHome}
    />
  );
};
