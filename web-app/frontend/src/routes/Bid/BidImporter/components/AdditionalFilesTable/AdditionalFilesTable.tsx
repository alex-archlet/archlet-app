import React, { useState } from 'react';
import styled from 'styled-components';
import { CheckCircle, Close, Error } from '@material-ui/icons';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Box, IconButton } from '@material-ui/core';

import { useCustomTranslation } from '@utils/translate';
import { arrayToMap } from '@utils/array';
import { DeleteButton } from '@common/components/Button/DeleteButton';
import { SecondaryButton } from '@common/components/Button/SecondaryButton';
import { Flex } from '@common/components/Flex/Flex';
import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';
import { Text } from '@common/components/Text/Text';
import { Checkbox } from '@common/components/Checkbox/Checkbox';
import { RowSelectFunction, Table } from '@common/components/Table/Table';
import { colors } from '@utils/uiTheme';
import {
  getTitleFromValues,
  tooltipArrayCSVFormatted,
} from '@utils/stringFormat';
import { BidImportFile } from '../../containers/AdditionalFiles/AdditionalFiles';

const StyledTableWrapper = styled.div`
  border: 1px solid ${colors.greyOutline};
  border-radius: 5px;
  min-height: 250px;
  max-height: 350px;
  overflow: auto;
`;

const StyledInfoBarBorder = styled.div`
  border: 1px solid ${colors.greyOutline};
  border-radius: 5px;
  padding: 5px;
`;

const StyledCheckCircle = styled(CheckCircle)`
  fill: ${colors.lightGreen} !important;
`;

const StyledError = styled(Error)`
  fill: ${colors.error} !important;x
`;

const StyledIconContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const StyledEmptyContainer = styled.div`
  height: 24px;
  width: 24px;
`;

const StyledCheckBox = styled(Checkbox)`
  padding: 0 !important;
`;

const StyledButtonSpacer = styled.div`
  width: 16px;
  display: inline-block;
`;

const StyledStatusMessage = styled.span<{ error: boolean }>`
  ${props => (props.error ? 'color: {colors.lightGreen} !important;' : '')}
`;

const CIRCULAR_PROGRESS_SIZE = 20;
const DEFAULT_SORTING = [
  {
    desc: false,
    id: 'fullName',
  },
];

const isMatchingInProgress = matchings => matchings === null;
const getUniqueSupplierNames = (matchings, supplierColumn) => {
  if (!matchings || !supplierColumn) {
    return '-';
  }
  const bidColumnMatching = matchings.matchings.find(
    ({ bid_column }) => bid_column === supplierColumn.id
  );

  if (!bidColumnMatching) {
    return '-';
  }
  const excelColumnValues =
    matchings.excel_values[bidColumnMatching.excel_column];

  if (!excelColumnValues) {
    return '-';
  }

  return [...new Set(excelColumnValues)];
};
interface Props {
  data: BidImportFile[];
  selectedRowId: string | undefined;
  supplierColumn: any[];
  fileErrors: object;
  onSelectRow: RowSelectFunction;
  onDelete: (keys: string[]) => void;
  onDownload: (keys: string[]) => void;
}

export const AdditionalFilesTable: React.FC<Props> = ({
  data,
  selectedRowId,
  supplierColumn,
  fileErrors,
  onSelectRow = undefined,
  onDelete,
  onDownload,
}) => {
  const [selected, setSelected] = useState<{ [key: string]: boolean }>({});
  const [hover, setHover] = useState<String>('');

  const { t } = useCustomTranslation();

  const onChange = (row) => {
    const existingRow = selected[row.id];

    if (existingRow) {
      const { [row.id]: _, ...newObject } = selected;
      return setSelected(newObject);
    }
    return setSelected(prev => ({
      ...prev,
      [row.id]: true,
    }));
  };

  const onSelectAllChange = (rows, isChecked) => {
    if (isChecked) {
      return setSelected({});
    }

    return setSelected(arrayToMap(rows));
  };

  const selectedRows = Object.keys(selected);
  const hasSelectedRows = selectedRows.length > 0;
  return (
    <>
      <StyledInfoBarBorder>
        <Flex justify="space-between" align="center">
          <Flex justify="center" align="center">
            <IconButton>
              {selectedRows.length !== 0 ? (
                <Close onClick={() => setSelected({})} />
              ) : (
                <StyledEmptyContainer />
              )}
            </IconButton>
            <Text fontSize={16} fontWeight={600}>
              {selectedRows.length} {t('Files selected')}
            </Text>
          </Flex>
          <Box mr={32}>
            <SecondaryButton
              onClick={() => onDownload(selectedRows)}
              disabled={!hasSelectedRows}
            >
              {t('Download')}
            </SecondaryButton>
            <StyledButtonSpacer />
            <DeleteButton
              onClick={() => {
                onDelete(selectedRows);
                setSelected({});
              }}
              disabled={!hasSelectedRows}
            >
              {t('Delete')}
            </DeleteButton>
          </Box>
        </Flex>
      </StyledInfoBarBorder>
      <VerticalSpacer height={8} />
      <StyledTableWrapper>
        <Table
          data={data}
          defaultSorting={DEFAULT_SORTING}
          onSelectRow={onSelectRow}
          selectedRowId={selectedRowId}
          stickyHeader
          maxHeight="350px"
          size="medium"
          onRowHoverEnter={rowKey => setHover(rowKey)}
          onRowHoverLeave={() => setHover('')}
          columns={[
            {
              Cell: ({ cell: { row } }) =>
                hover === row.id || selected[row.id] ? (
                  <StyledCheckBox
                    value={!!selected[row.id] ?? false}
                    onChange={(event) => {
                      event.stopPropagation();
                      onChange(row);
                    }}
                    formComponent={false}
                  />
                ) : null,
              Header: ({ data: rows }) => {
                const isChecked = rows.length === Object.keys(selected).length;
                return (
                  <StyledCheckBox
                    value={isChecked ?? false}
                    onChange={(event) => {
                      event.stopPropagation();
                      onSelectAllChange(rows, isChecked);
                    }}
                    formComponent={false}
                  />
                );
              },
              id: 'Select',
              tooltip: false,
            },
            {
              // eslint-disable-next-line react/prop-types
              Cell: ({ cell: { value, row } }) => (
                <StyledIconContainer>
                  {isMatchingInProgress(value) && (
                    <CircularProgress size={CIRCULAR_PROGRESS_SIZE} />
                  )}
                  {!isMatchingInProgress(value) &&
                    (Object.keys(fileErrors[row.original.id]).length === 0 ? (
                      <StyledCheckCircle />
                    ) : (
                      <StyledError />
                    ))}
                </StyledIconContainer>
              ),
              Header: '',
              accessor: 'matchings',
              id: 'status',
            },
            {
              Header: t('File Name'),
              accessor: 'fullName',
              id: 'fullName',
            },
            {
              // eslint-disable-next-line react/prop-types
              Cell: ({ cell: { value, row } }) => {
                const complete =
                  !isMatchingInProgress(value) &&
                  Object.keys(fileErrors[row.original.id]).length === 0;

                return (
                  <StyledStatusMessage error={!complete}>
                    {complete ? t('Complete') : t('Not complete')}
                  </StyledStatusMessage>
                );
              },
              Header: t('Column Matching'),
              accessor: 'matchings',
              id: 'matchings',
            },
            {
              // eslint-disable-next-line react/prop-types
              Cell: ({ cell: { row } }) => {
                const count = Object.keys(fileErrors[row.original.id]).length;

                return (
                  <StyledStatusMessage error={count !== 0}>
                    {count}
                  </StyledStatusMessage>
                );
              },
              Header: t('Unmatched Columns'),
              accessor: 'matchings',
              id: 'columns',
            },
            {
              // eslint-disable-next-line react/prop-types
              Cell: ({ cell: { value } }) => (
                <div>
                  {getTitleFromValues(
                    getUniqueSupplierNames(value, supplierColumn)
                  )}
                </div>
              ),
              tooltip: ({ cell: { value } }) => (
                <div>
                  {tooltipArrayCSVFormatted(
                    getUniqueSupplierNames(value, supplierColumn)
                  )}
                </div>
              ),
              Header: t('Supplier'),
              accessor: 'matchings',
              id: 'supplier',
            },
          ]}
        />
      </StyledTableWrapper>
    </>
  );
};
