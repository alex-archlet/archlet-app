import React, { useState } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { useParams } from 'react-router-dom';
import styled from 'styled-components';
import { bindActionCreators } from 'redux';
import { COLUMN_MODAL_FORM_NAME } from '@utils/formNames';
import { deleteColumn, setColumns } from '@actions/biddingRounds';
import { Add } from '@material-ui/icons';
import { useCustomTranslation } from '@utils/translate';
import { Button } from '@common/components/Button/Button';
import { biddingRoundsSelectors } from '@selectors/biddingRounds';
import { getFormValues } from 'redux-form';
import { ConfirmationDialog } from '@common/components/ConfirmationDialog/ConfirmationDialog';
import { Column } from '../../../common/Column/Column';
import { ScrollBars } from '../../../common/ScrollBars/ScrollBars';
import { BidColumnModalForm } from '../../../common/TableDesign/BidColumnModalForm';

const StyledColumnsContainer = styled.div`
  height: 470px;
`;

const StyledButton = styled(Button)`
  &&.MuiButton-root {
    display: flex;
    justify-content: flex-start;
    align-items: center;
    width: 150px;
    margin-bottom: 11px;
    background-color: #eef2f4;
    font-weight: 500;
    font-size: 12px;
    line-height: 13px;
    text-transform: none;
    cursor: pointer;
    border: none;
  }

  &&.MuiButton-outlined {
    border-radius: 5px;
    padding: 6px 10px;
  }

  svg {
    fill: rgba(0, 0, 0, 0.5);
    margin-right: 15px;
    font-size: 25px;
  }

  &:hover {
    color: rgba(44, 44, 44, 1);
  }
`;

const newColumnIdPrefix = 'new-column-';

const highMatchedStyled = {
  headerBackgroundColor: '#edf8ee',
  headerStrokeColor: '#3CBA54',
};

const lowMatchedStyled = {
  headerBackgroundColor: '#fef8e9',
  headerStrokeColor: '#F7B500',
};

const noMatchedStyled = {
  headerBackgroundColor: '#fff',
};

const noInputByStyled = {
  headerBackgroundColor: '#FCF0F0',
  headerStrokeColor: '#E64D4D',
};

const getStyleBaseOnConfidenceScore = (score, typeId, inputBy) => {
  if (['supplier', 'buyer'].includes(inputBy)) {
    if (typeId) {
      if (score < 0.8 && score > 0.4) {
        return lowMatchedStyled;
      }

      if (score > 0.8) {
        return highMatchedStyled;
      }
    }

    return noMatchedStyled;
  }

  return noInputByStyled;
};

const getHeaderColor = typeId => (typeId ? undefined : '#bfbfbf');

export const TableHeaderPreviewComponent = ({
  columns,
  columnTypes,
  deleteColumnAction,
  hasRoundMatching,
  setColumnsAction,
}) => {
  const [deleteColumnId, setDeleteColumnId] = useState(null);
  const [editColumn, setEditColumn] = useState();
  const [isColumnForm, setIsColumnForm] = useState();
  const { t } = useCustomTranslation();
  const { id: projectId } = useParams();

  const handleEdit = (id) => {
    setIsColumnForm(true);
    const columnToEdit = columns.find(column => column.id === id);

    setEditColumn(columnToEdit);
  };

  const moveColumn = (dragIndex, hoverIndex) => {
    const dragColumn = columns[dragIndex];
    const newColumns = [...columns];

    newColumns.splice(dragIndex, 1);
    newColumns.splice(hoverIndex, 0, dragColumn);
    setColumnsAction(newColumns);
  };

  const removeColumn = () => {
    const isNew = deleteColumnId.includes(newColumnIdPrefix);

    setColumnsAction(columns.filter(column => column.id !== deleteColumnId));

    if (!isNew) {
      deleteColumnAction(projectId, deleteColumnId);
    }
    setDeleteColumnId(null);
  };

  const updateColumn = (values) => {
    setColumnsAction(
      [...columns].map(column =>
        column.id === editColumn.id ? values : column
      )
    );
  };

  const addColumn = (values) => {
    setColumnsAction([
      ...columns,
      {
        id: `${newColumnIdPrefix}${[columns.length - 1]}`,
        isMandatory: false, // TODO: fix the isMandatory check
        ...values,
      },
    ]);
  };

  const handleSubmit = (value) => {
    const newColumnFormValue = { ...value };

    newColumnFormValue.matchingConfidence = 1;

    if (editColumn) {
      updateColumn(newColumnFormValue);
    } else {
      addColumn(newColumnFormValue);
    }

    setIsColumnForm(false);
  };

  return (
    <>
      {isColumnForm && (
        <BidColumnModalForm
          isOpen={isColumnForm}
          handleClose={() => setIsColumnForm(false)}
          onSubmit={value => handleSubmit(value)}
          initialValues={editColumn}
          columns={columns}
          columnTypes={columnTypes}
        />
      )}
      <ConfirmationDialog
        isOpen={!!deleteColumnId}
        handleClose={() => setDeleteColumnId(null)}
        handleConfirm={removeColumn}
        message={t('Do you really want to delete this column?')}
      />
      <StyledColumnsContainer>
        <ScrollBars
          autoHide
          renderView={({ ...props }) => (
            <div {...props} className="scrollbar-view" />
          )}
        >
          {columns.map(
            (
              { id, name, example, inputBy, matchingConfidence, typeId },
              index
            ) =>
              hasRoundMatching ? (
                <Column
                  key={id}
                  index={index}
                  id={id}
                  label={name}
                  headerColor={getHeaderColor(typeId)}
                  onMove={moveColumn}
                  setDeleteColumnId={setDeleteColumnId}
                  onEdit={handleEdit}
                  isDefault={false}
                  columnType={{
                    example,
                  }}
                  {...getStyleBaseOnConfidenceScore(
                    matchingConfidence,
                    typeId,
                    inputBy
                  )}
                />
              ) : (
                <Column
                  key={id}
                  index={index}
                  id={id}
                  label={name}
                  headerColor={getHeaderColor(typeId)}
                  isDefault={false}
                  columnType={{
                    example,
                  }}
                  {...getStyleBaseOnConfidenceScore(
                    matchingConfidence,
                    typeId,
                    inputBy
                  )}
                />
              )
          )}
        </ScrollBars>
      </StyledColumnsContainer>
      {hasRoundMatching && (
        <StyledButton
          onClick={() => handleEdit(null)}
          variant="outlined"
          component="button"
        >
          <Add />
          <span>{t('Add Column')}</span>
        </StyledButton>
      )}
    </>
  );
};

const mapStateToProps = state => ({
  columnTypes: biddingRoundsSelectors.getColumnTypes(state),
  formValues: getFormValues(COLUMN_MODAL_FORM_NAME)(state),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      deleteColumnAction: deleteColumn,
      setColumnsAction: setColumns,
    },
    dispatch
  );

TableHeaderPreviewComponent.propTypes = {
  columnTypes: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  columns: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  deleteColumnAction: PropTypes.func.isRequired,
  formValues: PropTypes.shape({}).isRequired,
  hasRoundMatching: PropTypes.bool.isRequired,
  setColumnsAction: PropTypes.func.isRequired,
};

export const TableHeaderPreview = connect(
  mapStateToProps,
  mapDispatchToProps
)(TableHeaderPreviewComponent);
