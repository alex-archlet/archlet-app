import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { TextField } from '@material-ui/core';
import {
  FilterDropdown,
  TYPE,
} from '@common/components/Filter/FilterDropdown/FilterDropdown';
import { stringToDropdownOption } from '@common/components/Filter/filterUtils';
import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';
import { useCustomTranslation } from '@utils/translate';
import { Text } from '@common/components/Text/Text';

const StyledTextFieldContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
`;

const StyledLabelContainer = styled.div`
  padding-right: 12px;
`;

export const DataSettings = ({ onChange, values, sheets }) => {
  const { t } = useCustomTranslation();

  const onValueChange = (key, value) => {
    onChange({
      ...values,
      [key]: value,
    });
  };

  return (
    <div>
      <Text fontSize={18} fontWeight={500}>
        {t('Data settings')}
      </Text>
      <VerticalSpacer height={10} />
      <StyledTextFieldContainer>
        <StyledLabelContainer>
          <Text fontSize={15} fontWeight={400}>
            {t('Choose your sheet')}
          </Text>
        </StyledLabelContainer>
        <FilterDropdown
          onChange={value => onValueChange('sheet', value.value)}
          values={values.sheet ? stringToDropdownOption(values.sheet) : null}
          options={sheets.map(stringToDropdownOption)}
          type={TYPE.SINGLE}
          label={t('Choose...')}
        />
      </StyledTextFieldContainer>
      <VerticalSpacer height={5} />
      <StyledTextFieldContainer>
        <StyledLabelContainer>
          <Text fontSize={15} fontWeight={400}>
            {t('Column titles are at table row number')}
          </Text>
        </StyledLabelContainer>
        <TextField
          type="number"
          variant="outlined"
          margin="dense"
          inputProps={{
            min: 1,
          }}
          size="small"
          onChange={evt => onValueChange('startIndex', evt.target.value)}
          value={values.startIndex}
        />
      </StyledTextFieldContainer>
    </div>
  );
};

DataSettings.propTypes = {
  onChange: PropTypes.func.isRequired,
  sheets: PropTypes.arrayOf(PropTypes.string).isRequired,
  values: PropTypes.shape({
    sheet: PropTypes.string,
    startIndex: PropTypes.number.isRequired,
  }).isRequired,
};
