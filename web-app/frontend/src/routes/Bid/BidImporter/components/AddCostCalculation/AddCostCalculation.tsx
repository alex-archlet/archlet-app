import React, { useState } from 'react';
import { Flex } from '@common/components/Flex/Flex';
import { Button } from '@common/components/Button/Button';
import { useDispatch } from 'react-redux';
import {
  createOrUpdateCostCalculation,
  getRoundTableDesign,
} from '@store/actions/biddingRounds';
import { useCustomTranslation } from '@utils/translate';
import { Box } from '@material-ui/core';
import { useParams } from 'react-router';
import { Modal } from '@routes/Bid/common/Modal/Modal';
import { SecondaryButton } from '@common/components/Button/SecondaryButton';
import { CardTitle } from '@common/components/Card/CardTitle';
import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';
import { BidColumn } from '@routes/Bid/common/utils';
import { ExpressionEvaluation } from '../ExpressionEvaluation/ExpressionEvaluation';

const newColumnIdPrefix = 'new-column-';
export const INITIAL_PRICE_COLUMN: BidColumn = {
  column: 'price',
  id: `${newColumnIdPrefix}1`,
  inputBy: 'supplier',
  isAutomatic: true,
  isMandatory: false,
  isVisible: true,
  name: '',
  calculation: null,
};

export const AddCostCalculation: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useCustomTranslation();
  const { id: projectId, roundId } = useParams<{
    id: string;
    roundId: string;
  }>();

  const [showModal, setShowModal] = useState(false);
  const [newPriceComponent, setNewPriceComponent] = useState(
    INITIAL_PRICE_COLUMN
  );

  const handleCreateOrUpdateCostCalculation = () => {
    return createOrUpdateCostCalculation(projectId, roundId, [
      newPriceComponent,
    ]);
  };
  const onSave = async () => {
    await dispatch(handleCreateOrUpdateCostCalculation());
    await dispatch(getRoundTableDesign(projectId));
    setNewPriceComponent(INITIAL_PRICE_COLUMN);
    setShowModal(false);
  };
  return (
    <>
      <Flex justify="flex-end">
        <Button
          onClick={async () => {
            setShowModal(true);
          }}
        >
          Add Calculation
        </Button>
      </Flex>
      <Modal
        isOpen={showModal}
        handleClose={() => {
          setShowModal(false);
        }}
        maxWidth="lg"
        fullWidth
      >
        <Box padding="60px 80px">
          <CardTitle title={t('Add Cost Calculation')} />
          <Box ml={10}>
            <ExpressionEvaluation
              currentBidCostComponent={newPriceComponent}
              handleUpdateAndCreateBidColumn={setNewPriceComponent}
            />
            <VerticalSpacer height={10} />
            <SecondaryButton onClick={onSave}>{t('Save')}</SecondaryButton>
          </Box>
        </Box>
      </Modal>
    </>
  );
};
