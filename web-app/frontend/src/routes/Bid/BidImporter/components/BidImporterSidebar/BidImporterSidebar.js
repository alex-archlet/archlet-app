import React from 'react';
import { Box, Typography } from '@material-ui/core';
import PropTypes from 'prop-types';
import {
  AttachMoney,
  BorderColor,
  NoteAdd,
  OpenInBrowser,
  Subject,
  ViewList,
} from '@material-ui/icons';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { useCustomTranslation } from '@utils/translate';
import { PrimaryButton } from '@common/components/Button/PrimaryButton';
import { SidebarSection } from '@common/components/Sidebar/SidebarSection';
import { SidebarItem } from '@common/components/Sidebar/SidebarItem';
import { biddingRoundsSelectors } from '@store/selectors/biddingRounds';

const StyledTitleContainer = styled.div`
  position: absolute;
  top: 52px;
  left: 47px;
  display: flex;
  align-items: center;
`;

const StyledTitle = styled(Typography)`
  && {
    color: rgba(44, 44, 44, 0.35);
    font-weight: 400;
    font-size: 16px;
    line-height: 21px;
    text-transform: uppercase;
  }
`;

const StyledColumnDirection = styled(Box)`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
`;

const StyledButton = styled(PrimaryButton)`
  && {
    min-width: 130px;
  }
`;

const BidImporterSidebar = ({
  biddingRoundName,
  handleDeleteRoundButtonClick,
  higherLevelPath,
  id,
  saveProgressAction,
}) => {
  const { t } = useCustomTranslation();

  return (
    <Box>
      <StyledTitleContainer>
        <StyledTitle variant="h2">
          {biddingRoundName ?? t('new bidding round')}
        </StyledTitle>
      </StyledTitleContainer>
      <Box>
        <SidebarSection title={t('Settings')}>
          <SidebarItem
            icon={<Subject />}
            isSecondary
            name={t('Description')}
            to={`${higherLevelPath}/${id}/description`}
          />
          <SidebarItem
            icon={<OpenInBrowser />}
            isSecondary
            name={t('Bid Sheet')}
            to={`${higherLevelPath}/${id}/upload-match`}
          />
          <SidebarItem
            icon={<ViewList />}
            isSecondary
            name={t('Items / SKUs')}
            to={`${higherLevelPath}/${id}/items`}
          />
          <SidebarItem
            icon={<NoteAdd />}
            isSecondary
            name={t('Bid Upload')}
            to={`${higherLevelPath}/${id}/additional-files`}
          />
          <SidebarItem
            icon={<BorderColor />}
            isSecondary
            name={t('Bid Corrections')}
            to={`${higherLevelPath}/${id}/bid-corrections`}
          />
          <SidebarItem
            icon={<AttachMoney />}
            isSecondary
            name={t('Cost Calculation')}
            to={`${higherLevelPath}/${id}/cost-calculation`}
          />
        </SidebarSection>
      </Box>
      <StyledColumnDirection mt={103}>
        <Box mb={10}>
          <StyledButton onClick={saveProgressAction}>
            {t('Save Draft')}
          </StyledButton>
        </Box>
        <StyledButton onClick={handleDeleteRoundButtonClick}>
          {t('Delete')}
        </StyledButton>
      </StyledColumnDirection>
    </Box>
  );
};

BidImporterSidebar.propTypes = {
  biddingRoundName: PropTypes.string,
  higherLevelPath: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  saveProgressAction: PropTypes.func.isRequired,
};

BidImporterSidebar.defaultProps = {
  biddingRoundName: null,
};

const mapStateToProps = state => ({
  biddingRoundName: biddingRoundsSelectors.getRoundDetailsName(state),
});

export const BidImporterSidebarContainer = connect(mapStateToProps)(
  BidImporterSidebar
);
