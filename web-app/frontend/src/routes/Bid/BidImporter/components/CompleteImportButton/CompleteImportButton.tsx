import React from 'react';
import {
  runExpressionEvaluationJob,
  updateFinishedStatus,
} from '@store/actions/biddingRounds';
import { getProjectInfo } from '@store/actions/projects';
import { useDispatch } from 'react-redux';
import { useHistory, useParams } from 'react-router';
import { useCustomTranslation } from '@utils/translate';
import { Button } from '@common/components/Button/Button';
import { BidColumnsCostComponentParsed } from '../../containers/CostCalculation/CostCalculation';

interface Props {
  setValidationError: React.SetStateAction<string>;
  bidColumnsCostComponentParsed: BidColumnsCostComponentParsed[];
  handleCreateOrUpdateCostCalculation: () => void;
}

export const CompleteImportButton = ({
  handleCreateOrUpdateCostCalculation,
  bidColumnsCostComponentParsed,
  setValidationError,
}) => {
  const { t } = useCustomTranslation();
  const dispatch = useDispatch();
  const history = useHistory();

  const { id: projectId, roundId } = useParams<{
    id: string;
    roundId: string;
  }>();

  const handleNextStep = async () => {
    const hasInvalid = bidColumnsCostComponentParsed.find(
      bidColumnCostComponentParsed => !bidColumnCostComponentParsed.isValid
    );

    if (!hasInvalid) {
      try {
        handleCreateOrUpdateCostCalculation();
        await dispatch(runExpressionEvaluationJob(projectId, roundId));
        await dispatch(updateFinishedStatus(projectId, roundId));
        await dispatch(getProjectInfo(projectId));
        history.push(`/projects/${projectId}/bid-importer`);
        // eslint-disable-next-line no-empty
      } catch {}
    } else {
      setValidationError(
        t(
          'Please make sure all cost calculation rows are valid before completing the import'
        )
      );
    }
  };

  return (
    <Button color="secondary" size="large" onClick={handleNextStep}>
      {t('Complete Import')}
    </Button>
  );
};
