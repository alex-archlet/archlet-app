import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Clear, Done } from '@material-ui/icons';

import { useCustomTranslation } from '@utils/translate';
import { Text } from '@common/components/Text/Text';
import { columnMatchingErrors } from '../../../common/enum/errors';

const StyledDone = styled(Done)`
  fill: #3CBA54 !important;
`;

const StyledClear = styled(Clear)`
  fill: #E64D4D !important;
`;

const StyledContainer = styled.div`
  padding: 12px 22px;
  border: 2px dashed #E4E4E4;
  border-radius: 5px;
  display: flex;
  flex-direction: row;
`;

const StyledTextContainer = styled.div`
  padding-left: 16px;
`;

export const MatchingMessage = ({
  error,
}) => {
  const { t } = useCustomTranslation();
  const IconComponent = !error ? StyledDone : StyledClear;

  const errorMessages = {
    [columnMatchingErrors.DUPLICATE_MATCH]: t('Column selected multiple times'),
    [columnMatchingErrors.NO_MATCH]: t('Unable to match, please adjust'),
  };
  const defaultError = t('Unable to match, please adjust');

  return (
    <StyledContainer>
      <IconComponent />
      <StyledTextContainer>
        <Text
          fontSize={14}
          fontWeight={500}
        >
          {!error ? t('Matching successful') : (errorMessages[error] || defaultError)}
        </Text>
      </StyledTextContainer>
    </StyledContainer>
  );
};

MatchingMessage.propTypes = {
  error: PropTypes.string,
};

MatchingMessage.defaultProps = {
  error: undefined,
};
