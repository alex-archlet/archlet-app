import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { MatchingEdit } from './MatchingEdit';
import { MatchingMessage } from './MatchingMessage';

const StyledRow = styled.div`
  display: flex;
  flex-direction: row;
  margin-bottom: 11px;
  justify-content: space-between;
`;

const StyledColumn = styled.div`
  width: calc(50% - 6px);
`;

export const MatchingTable = ({
  columns,
  selectedFileErrors,
  matchings,
  onUpdateMatchings,
}) => {
  if (!matchings) {
    return null;
  }

  const { matchings: excelMatchings, excel_columns: excelColumns } = matchings;

  const onUpdateSingleMatching = (index, excel_column) => {
    const newValue = {
      ...excelMatchings[index],
      excel_column,
      matching: true,
    };
    const newMatchings = [...excelMatchings];

    newMatchings[index] = newValue;
    onUpdateMatchings(newMatchings);
  };

  return (
    <div>
      {excelMatchings.map((matching, index) => (
        <StyledRow key={matching.bid_column}>
          <StyledColumn>
            <MatchingEdit
              excelColumns={excelColumns}
              matching={matching}
              columns={columns}
              onChange={value => onUpdateSingleMatching(index, value)}
              error={Boolean(selectedFileErrors[matching.bid_column])}
            />
          </StyledColumn>
          <StyledColumn>
            <MatchingMessage
              error={selectedFileErrors[matching.bid_column]}
            />
          </StyledColumn>
        </StyledRow>
      ))}
    </div>
  );
};

MatchingTable.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  matchings: PropTypes.shape({
    excel_columns: PropTypes.arrayOf(PropTypes.string).isRequired,
    matchings: PropTypes.arrayOf(PropTypes.shape({
      bid_column: PropTypes.string,
      excel_column: PropTypes.string.isRequired,
      matching: PropTypes.bool.isRequired,
      message: PropTypes.string,
    })),
  }),
  onUpdateMatchings: PropTypes.func.isRequired,
  selectedFileErrors: PropTypes.shape({}).isRequired,
};

MatchingTable.defaultProps = {
  matchings: null,
};
