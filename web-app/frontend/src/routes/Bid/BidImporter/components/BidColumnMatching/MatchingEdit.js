import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Select from 'react-select';

import { Text } from '@common/components/Text/Text';
import { useCustomTranslation } from '@utils/translate';

const StyledContainer = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row;
`;

const StyledTextContainer = styled.div`
  border: 1px solid #dddddd;
  border-radius: 5px;
  border-top-right-radius: 0px;
  border-bottom-right-radius: 0px;
  border-right-style: none;
  background-color: #eef2f4;
  display: flex;
  align-items: center;
  flex: 2;
  padding: 14px 22px;
`;

const StyledDropdownContainer = styled.div`
  flex: 3;
`;

const createOptionFromColumn = col => ({
  label: col,
  value: col,
});

const ERROR_COLOR = '#E64D4D';
const SUCCESS_COLOR = '#3CBA54';

const getCustomStyles = isError => ({
  control: provided => ({
    ...provided,
    '&:hover': {}, // border style on hover
    border: `1px solid ${isError ? ERROR_COLOR : SUCCESS_COLOR}`, // default border color
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 5,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 5,
    boxShadow: 'none', // no box-shadow
    minHeight: 53,
  }),
});

export const MatchingEdit = ({
  columns,
  excelColumns,
  error,
  matching,
  onChange,
}) => {
  const { t } = useCustomTranslation();

  const bidColumn = columns.find(column => column.id === matching.bid_column);
  const selectedColumnValue =
    matching.excel_column && createOptionFromColumn(matching.excel_column);
  const options = useMemo(() => excelColumns.map(createOptionFromColumn), [
    excelColumns,
  ]);

  return (
    <StyledContainer>
      <StyledTextContainer>
        <Text fontSize={14} fontWeight={500}>
          {bidColumn.name}
        </Text>
      </StyledTextContainer>
      <StyledDropdownContainer>
        <Select
          options={options}
          value={selectedColumnValue}
          menuPortalTarget={document.body}
          styles={getCustomStyles(error)}
          placeholder={t('Choose...')}
          components={{
            IndicatorSeparator: () => null,
          }}
          onChange={({ value }) => onChange(value)}
        />
      </StyledDropdownContainer>
    </StyledContainer>
  );
};

MatchingEdit.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  error: PropTypes.bool.isRequired,
  excelColumns: PropTypes.arrayOf(PropTypes.string).isRequired,
  matching: PropTypes.shape({
    bid_column: PropTypes.string,
    excel_column: PropTypes.string.isRequired,
    matching: PropTypes.bool.isRequired,
    message: PropTypes.string,
  }).isRequired,
  onChange: PropTypes.func.isRequired,
};
