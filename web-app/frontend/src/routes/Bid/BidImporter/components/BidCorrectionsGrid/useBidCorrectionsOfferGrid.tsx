import { BidColumnFlatRow } from '@common/components/Grid/Grid';
import { useGetRequest } from '@common/components/Hooks/useRequest';
import { calculateParsedOffersPayload } from '@routes/Bid/common/utils';
import { useEffect, useState } from 'react';

export const useBidCorrectionOfferGrid = (
  projectId: string,
  roundId: string,
  shouldRefresh: boolean
) => {
  const [grid, setGrid] = useState<BidColumnFlatRow[]>([]);

  const { response } = useGetRequest<object[]>(
    `projects/${projectId}/bidding_rounds/${roundId}/offer_processed`,
    shouldRefresh
  );
  useEffect(() => {
    if (response?.length) {
      const parsedGrid = calculateParsedOffersPayload(response);
      setGrid(parsedGrid);
    } else {
      setGrid([]);
    }
  }, [response, shouldRefresh]);

  return {
    offerDemandsData: grid,
    setGrid,
  };
};
