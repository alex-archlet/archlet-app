import React, { useState } from 'react';
import clsx from 'clsx';
import {
  Grid,
  CustomColumn,
  CellStyleClass,
  BidColumnFlatRow,
} from '@common/components/Grid/Grid';
import { TextEditor } from '@common/components/Grid/TextEditor';
import { Cell } from '@common/components/Grid/Cell';
import { FormatNumberCell } from '@common/components/Grid/FormatNumberCell';
import { ReadOnlyCell } from '@common/components/Grid/ReadOnlyCell';
import { FilterInputCell } from '@common/components/Grid/FilterInputCell';
import { useBlockingJobEnd } from '@routes/Scenarios/components/useBlockingJobEnd';
import { Position } from 'react-data-grid/lib/types';
import { BidColumn, checkCellForErrors } from '@routes/Bid/common/utils';
import { GridCellInfo } from '@routes/Bid/common/GridCellInfo/GridCellInfo';
import { SortableGridHeaderCell } from '@common/components/Grid/SortableGridHeaderCell';
import {
  getMinMaxForColumn,
  rangeFilter,
  stringFilter,
} from '@common/components/Grid/filterUtils';
import { RangeFilterSlider } from '@common/components/Grid/RangeFilterSlider';
import { NumericEditor } from '@common/components/Grid/NumericEditor';

const makeCellClassname = (columnId: string) => (row: BidColumnFlatRow) => {
  const hasError = row.processing_messages[columnId]?.error?.enum ?? '';
  const hasWarning = row.processing_messages[columnId]?.warning?.enum ?? '';

  return clsx({
    [CellStyleClass.Error]: hasError,
    [CellStyleClass.Warning]: hasWarning,
  });
};

const getEditableColumns = (columns: BidColumn[], data: BidColumnFlatRow[]) =>
  columns.map(
    ({ id, column, name, isNumeric }): CustomColumn => {
      if (column === 'item' || column === 'supplier') {
        return {
          name,
          key: id,
          width: 200,
          resizable: true,
          frozen: true,
          sortable: true,
          formatter: column === 'item' ? ReadOnlyCell : Cell,
          headerCellClass: 'header',
          filterFunction: stringFilter(id),
          filterRenderer: FilterInputCell,
          headerRenderer: SortableGridHeaderCell,
        };
      }
      if (isNumeric) {
        const { min, max } = getMinMaxForColumn(data, id);

        return {
          name,
          key: id,
          width: 200,
          resizable: true,
          sortable: true,
          editor: NumericEditor,
          filterFunction: rangeFilter(id),
          formatter: FormatNumberCell,
          cellClass: makeCellClassname(id),
          headerCellClass: 'header',
          filterRenderer: props => (
            <RangeFilterSlider {...props} min={min} max={max} />
          ),
          headerRenderer: SortableGridHeaderCell,
        };
      }
      return {
        name,
        key: id,
        width: 200,
        resizable: true,
        sortable: true,
        filterFunction: stringFilter(id),
        editor: TextEditor,
        formatter: Cell,
        headerCellClass: 'header',
        cellClass: makeCellClassname(id),
        filterRenderer: FilterInputCell,
        headerRenderer: SortableGridHeaderCell,
      };
    }
  );

interface Props {
  columns: BidColumn[];
  grid: BidColumnFlatRow[];
  setGrid: (grid: BidColumnFlatRow[]) => void;
}

export const BidCorrectionsGrid = ({ columns, grid, setGrid }) => {
  const [shouldRefresh, setShouldRefresh] = useState<boolean>(false);
  const [currentCell, setCurrentCell] = useState<Position>({
    idx: 0,
    rowIdx: 0,
  });

  const dataGridColumns: CustomColumn[] = getEditableColumns(columns, grid);

  const onChange = (rows: unknown[]) => {
    const { idx, rowIdx } = currentCell;
    const currentRow: BidColumnFlatRow = rows[rowIdx] as BidColumnFlatRow;
    const currentColumn = columns[idx];

    if (currentRow && currentColumn) {
      const value = currentRow[currentColumn.id];

      const row: BidColumnFlatRow = checkCellForErrors(
        value,
        currentColumn,
        currentRow
      );
      row[currentColumn.id] = value;
      const newRows = [...rows];
      newRows[rowIdx] = row;
      setGrid(newRows);
    }
  };
  useBlockingJobEnd(() => setShouldRefresh(!shouldRefresh));

  return (
    <>
      {currentCell && (
        <GridCellInfo
          currentCellInfo={currentCell}
          grid={grid}
          columns={columns}
        />
      )}
      <Grid
        columns={dataGridColumns}
        grid={grid}
        setGrid={onChange}
        setCurrentCell={setCurrentCell}
        enabledFilters
      />
    </>
  );
};
