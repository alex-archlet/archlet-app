import React from 'react';
import { useCustomTranslation } from '@utils/translate';
import { Text } from '@common/components/Text/Text';
import styled from 'styled-components';
import { Box } from '@material-ui/core';
import { colors } from '@utils/uiTheme';

const StyledInfoText = styled(Text)`
  height: 41px;
  padding: 11px 0 0;
  font-size: 16px;
  line-height: 10px;
  color: ${props => (props.color)};
`;

export const CellAdditionalInfo = ({error, warning, change, value}) => {
  const { t } = useCustomTranslation();

  return (
    <Box>
    { error && (
    <StyledInfoText color={colors.red}>
    {t(error)}
  </StyledInfoText>
    )}
    { warning && (
      <StyledInfoText color={colors.warning}>
        {t(warning)}
      </StyledInfoText>
    )}
    { change && (
      <StyledInfoText color={colors.highlightBlue}>
        {`${change} => ${value}`}
      </StyledInfoText>
    )}
  </Box>
  )
}
