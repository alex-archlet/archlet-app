import React, { useState } from 'react';
import { Button } from '@common/components/Button/Button';
import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';
import {
  getRoundTableDesign,
  runExpressionExampleJob,
} from '@store/actions/biddingRounds';
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router';
import { BidColumn } from '@routes/Bid/common/utils';
import { useCustomTranslation } from '@utils/translate';
import { ExpressionEvaluationTable } from '../ExpressionEvaluationTable/ExpressionEvaluationTable';
import { BidColumnsCostComponentParsed } from '../../containers/CostCalculation/CostCalculation';

export type ExpressionEvaluationJobResult = {
  calculated_examples: object[];
  header_calculated_examples: string[];
};

interface Props {
  currentBidCostComponent: BidColumn;
  handleCreateOrUpdateCostCalculation: () => void;
  bidColumnsCostComponentParsed: BidColumnsCostComponentParsed[];
  setValidationError: React.SetStateAction<any>;
}

export const ExpressionExampleButtonAndTable: React.FC<Props> = ({
  currentBidCostComponent,
  handleCreateOrUpdateCostCalculation,
  bidColumnsCostComponentParsed,
  setValidationError,
}) => {
  const { t } = useCustomTranslation();
  const dispatch = useDispatch();
  const [
    expressionExampleJobTable,
    setExpressionExampleJobTable,
  ] = useState<ExpressionEvaluationJobResult | null>(null);

  const { id: projectId, roundId } = useParams<{
    id: string;
    roundId: string;
  }>();

  const onStartExpressionExample = async () => {
    if (currentBidCostComponent) {
      const currentBidColumnCostComponentParsed = bidColumnsCostComponentParsed.find(
        bidColumnCostComponentParsed =>
          bidColumnCostComponentParsed.column.id === currentBidCostComponent.id
      );
      if (currentBidColumnCostComponentParsed?.isValid) {
        const expressionExampleJobResult = await dispatch(
          runExpressionExampleJob(
            currentBidCostComponent.calculation,
            projectId,
            roundId
          )
        );
        // @ts-ignore dispatch should be typed
        setExpressionExampleJobTable(expressionExampleJobResult.output);
      } else {
        setValidationError(
          t(
            'Please make sure all cost calculation rows are valid before completing the import'
          ) as string
        );
      }
    }
  };

  const handleStartExpressionExample = async () => {
    await handleCreateOrUpdateCostCalculation();
    await onStartExpressionExample();
    await dispatch(getRoundTableDesign(projectId));
  };

  return (
    <>
      <VerticalSpacer height={10} />
      <Button
        color="secondary"
        size="large"
        onClick={handleStartExpressionExample}
      >
        Run Example
      </Button>

      <VerticalSpacer height={5} />
      <ExpressionEvaluationTable data={expressionExampleJobTable} />
    </>
  );
};
