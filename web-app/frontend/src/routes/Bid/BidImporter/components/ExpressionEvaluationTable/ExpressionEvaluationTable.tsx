import React from 'react';
import { Table } from '@common/components/Table/Table';
import { LocaleFormatNumber } from '@common/components/FormatNumber/FormatNumber';
import styled from 'styled-components';
import { colors } from '@utils/uiTheme';
import { ExpressionEvaluationJobResult } from '../ExpressionExampleButtonAndTable/ExpressionExampleButtonAndTable';

const StyledTableWrapper = styled.div`
  border: 1px solid ${colors.greyOutline};
  border-radius: 5px;
  min-height: 250px;
  max-height: 350px;
  overflow: auto;
`;

interface Props {
  data: ExpressionEvaluationJobResult | null;
}
const calculateHeaders = header =>
  header.map((columnName, index) => ({
    Cell: ({ cell: { value } }) => <LocaleFormatNumber value={value} />,
    Header: columnName,
    accessor: row => row[columnName],
    id: index,
  }));

export const ExpressionEvaluationTable: React.FC<Props> = ({ data }) => {
  const tableData = data?.calculated_examples ?? [];
  const tableHeader = data?.header_calculated_examples ?? [];

  const columns = calculateHeaders(tableHeader);
  return (
    <>
      {data ? (
        <StyledTableWrapper>
          <Table
            getUniqueKey={(_, key) => key.toString()}
            data={tableData}
            columns={columns}
          />
        </StyledTableWrapper>
      ) : null}
    </>
  );
};
