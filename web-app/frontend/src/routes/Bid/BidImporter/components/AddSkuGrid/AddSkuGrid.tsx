import React from 'react';
import {
  BidColumnFlatRow,
  CustomColumn,
  Grid,
} from '@common/components/Grid/Grid';
import { TextEditor } from '@common/components/Grid/TextEditor';
import { Cell } from '@common/components/Grid/Cell';
import { FormatNumberCell } from '@common/components/Grid/FormatNumberCell';
import { SortableGridHeaderCell } from '@common/components/Grid/SortableGridHeaderCell';
import { NumericEditor } from '@common/components/Grid/NumericEditor';
import { RangeFilterSlider } from '@common/components/Grid/RangeFilterSlider';
import { FilterInputCell } from '@common/components/Grid/FilterInputCell';
import {
  getMinMaxForColumn,
  rangeFilter,
  stringFilter,
} from '@common/components/Grid/filterUtils';
import { BidColumn } from '@routes/Bid/common/utils';

const getEditableColumns = (columns: BidColumn[], data: BidColumnFlatRow[]) =>
  columns.map(({ column, name, id, isNumeric }) => {
    if (column === 'item' || column === 'supplier') {
      return {
        name,
        key: id,
        width: 200,
        resizable: true,
        frozen: true,
        sortable: true,
        formatter: Cell,
        editor: TextEditor,
        filterFunction: stringFilter(id),
        headerCellClass: 'header',
        filterRenderer: FilterInputCell,
        headerRenderer: SortableGridHeaderCell,
      };
    }
    if (isNumeric) {
      const { min, max } = getMinMaxForColumn(data, id);

      return {
        name,
        key: id,
        width: 200,
        resizable: true,
        sortable: true,
        filterFunction: rangeFilter(id),
        editor: NumericEditor,
        formatter: FormatNumberCell,
        headerCellClass: 'header',
        filterRenderer: props => (
          <RangeFilterSlider {...props} min={min} max={max} />
        ),
        headerRenderer: SortableGridHeaderCell,
      };
    }
    return {
      name,
      key: id,
      width: 200,
      resizable: true,
      sortable: true,
      editor: TextEditor,
      formatter: Cell,
      filterFunction: stringFilter(id),
      headerCellClass: 'header',
      filterRenderer: FilterInputCell,
      headerRenderer: SortableGridHeaderCell,
    };
  });

export const AddSkuGrid = ({ columns, grid, setGrid }) => {
  const dataGridColumns: CustomColumn[] = getEditableColumns(columns, grid);
  return (
    <Grid
      columns={dataGridColumns}
      grid={grid}
      setGrid={setGrid}
      enabledFilters
      addRowEnabled
    />
  );
};
