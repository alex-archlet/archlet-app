import { useGetRequest } from '@common/components/Hooks/useRequest';
import {
  BidColumn,
  calculateParsedOffersPayload,
} from '@routes/Bid/common/utils';
import { biddingRoundsSelectors } from '@store/selectors/biddingRounds';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

const calculateParsedExcelPayload = (data: any[], columns: BidColumn[]) => {
  if (!data) {
    return [];
  }

  return data.map((row) => {
    return columns.reduce((acc, col) => {
      acc[col.id] = row[col.name];
      return acc;
    }, {});
  });
};

export type Grid = {
  [details: string]: string;
};

const getBidColumnsToUse = (
  bidSettings: BiddingSettings,
  columns: BidColumn[]
) => {
  if (bidSettings && bidSettings.historicTargetPrices) {
    return columns;
  }

  if (bidSettings && !bidSettings.historicTargetPrices) {
    const res = columns.filter(column => column.inputBy === 'buyer');

    return res;
  }

  return [];
};

export const useBidImporterExcelGrid = ({
  data,
  isActive = true,
}: {
  data: Grid[];
  isActive: boolean;
}) => {
  const [grid, setGrid] = useState<Grid[]>([]);

  const allBidColumns = useSelector(
    biddingRoundsSelectors.getColumnsForTableDesign
  );
  const bidSettings = useSelector(biddingRoundsSelectors.getRoundSettings);
  const columnsToUse = getBidColumnsToUse(bidSettings, allBidColumns);

  useEffect(() => {
    if (isActive && bidSettings?.historicTemplateData) {
      setGrid(calculateParsedExcelPayload(data, columnsToUse));
    } else {
      setGrid([]);
    }
  }, [data, bidSettings]);

  return {
    excelGrid: grid,
    setGrid,
    columnsToUse,
  };
};

type BiddingSettings = {
  historicTargetPrices: boolean;
};

export const useOfferDemandsGrid = (
  projectId: string,
  roundId: string,
  bidSettings: BiddingSettings
) => {
  const [grid, setGrid] = useState<Grid[]>([]);

  const { response } = useGetRequest<object[]>(
    `projects/${projectId}/bidding_rounds/${roundId}/get_demands_offers`
  );
  const allBidColumns = useSelector(
    biddingRoundsSelectors.getColumnsForTableDesign
  );

  const columnsToUse = getBidColumnsToUse(bidSettings, allBidColumns);

  useEffect(() => {
    if (response?.length) {
      const parsedGrid = calculateParsedOffersPayload(response);
      setGrid(parsedGrid);
    } else {
      setGrid([]);
    }
  }, [response, bidSettings]);

  return {
    offerDemandsData: grid,
    setGrid,
    columnsToUse,
  };
};

export const useSkuGrid = (
  projectId: string,
  roundId: string,
  excelData: any,
  bidSettings: BiddingSettings,
  isColumnForm: boolean
) => {
  const {
    offerDemandsData,
    setGrid: offerDemandSetGrid,
    columnsToUse: offerDemandBidColumns,
  } = useOfferDemandsGrid(projectId, roundId, bidSettings);

  const { excelGrid, setGrid: setExcelGrid } = useBidImporterExcelGrid({
    data: excelData,
    isActive: true,
  });
  if (offerDemandsData?.length && !isColumnForm) {
    return {
      grid: offerDemandsData,
      setGrid: offerDemandSetGrid,
      bidColumnsToUse: offerDemandBidColumns,
    };
  }

  return {
    grid: excelGrid,
    setGrid: setExcelGrid,
    bidColumnsToUse: offerDemandBidColumns,
  };
};
