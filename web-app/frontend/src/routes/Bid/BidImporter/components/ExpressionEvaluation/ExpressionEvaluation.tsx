import React, { useMemo } from 'react';
import {
  ALLOWED_OPERATIONS,
  AvailableElements,
} from '@routes/Bid/common/CostCalculation/AvailableElements';
import {
  componentsToExpression,
  parseStringToColumnIds,
  parseValueArray,
  validateCalculation,
} from '@routes/Bid/common/CostCalculation/utils';
import { biddingRoundsSelectors } from '@store/selectors/biddingRounds';
import { useSelector } from 'react-redux';
import { Input } from '@common/components/Input/Input';
import { useCustomTranslation } from '@utils/translate';
import { Flex } from '@common/components/Flex/Flex';
import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';
import styled from 'styled-components';
import { CostCalculationExpressionBar } from '@routes/Bid/common/CostCalculation/CostCalculationExpressionBar';
import { BidColumn } from '@routes/Bid/common/utils';
import { BidColumnsCostComponentParsed } from '../../containers/CostCalculation/CostCalculation';

const StyledIconContainer = styled.div`
  margin-left: 5px;
`;
interface Props {
  currentBidCostComponent: BidColumn;
  handleUpdateAndCreateBidColumn: (bidColumn: BidColumn) => void;
}

export const ExpressionEvaluation: React.FC<Props> = ({
  currentBidCostComponent,
  handleUpdateAndCreateBidColumn,
}) => {
  const columns = useSelector(biddingRoundsSelectors.getCalculableColumns);

  const { t } = useCustomTranslation();

  const columnObjects = useMemo(
    () => [
      ...(columns || []).map(column => ({
        column,
        id: column.id,
        name: column.name,
        type: 'column',
      })),
    ],
    [columns]
  );

  const validOptions = useMemo(
    () => [...ALLOWED_OPERATIONS, ...columnObjects],
    [columnObjects]
  );

  const columnWithCalculation: BidColumnsCostComponentParsed = useMemo(() => {
    if (currentBidCostComponent?.calculation) {
      const splitExpression = parseStringToColumnIds(
        currentBidCostComponent.calculation
      );
      const expression = parseValueArray(splitExpression, validOptions);

      return {
        column: currentBidCostComponent,
        expression,
        isValid: validateCalculation(expression),
      };
    }
    return {
      column: currentBidCostComponent,
      expression: null,
      isValid: false,
    };
  }, [currentBidCostComponent, validOptions]);

  const onChange = (column, components) => {
    const newColumn = {
      ...column,
      calculation: componentsToExpression(components),
    };
    handleUpdateAndCreateBidColumn(newColumn);
  };

  const onValueClick = (value) => {
    const {
      column: currentPriceComponentColumn,
      expression: currentPriceComponentExpression,
    } = columnWithCalculation;

    const addedHeaderOrOperator = value.column ? value.column : value;

    const newColumn = {
      ...currentPriceComponentColumn,
      calculation: componentsToExpression([
        ...(currentPriceComponentExpression || []),
        addedHeaderOrOperator,
      ]),
    };
    handleUpdateAndCreateBidColumn(newColumn);
  };

  const handleInputChange = (value) => {
    const columnWithName = {
      ...columnWithCalculation.column,
      ...{ name: value },
    };
    handleUpdateAndCreateBidColumn(columnWithName);
  };

  return (
    <>
      {columns && (
        <AvailableElements columns={columns} onClick={onValueClick} />
      )}
      {columnWithCalculation?.column?.column === 'price_total' ? (
        <h3>{`${t('Total Cost')} =`}</h3>
      ) : (
        <div>
          <Flex align="center">
            <Input
              value={currentBidCostComponent.name}
              onChange={value => handleInputChange(value)}
              fullWidth={false}
              height="10px"
            />{' '}
            <StyledIconContainer>=</StyledIconContainer>
          </Flex>
        </div>
      )}
      <VerticalSpacer height={10} />
      {columnWithCalculation && (
        <CostCalculationExpressionBar
          value={columnWithCalculation.expression}
          onChange={(_, value) => onChange(columnWithCalculation.column, value)}
          isValid={columnWithCalculation.isValid}
          elements={validOptions}
        />
      )}
    </>
  );
};
