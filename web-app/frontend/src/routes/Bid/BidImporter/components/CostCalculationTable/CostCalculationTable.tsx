import React, { useEffect } from 'react';
import { Cancel, CheckCircle, Close } from '@material-ui/icons';
import styled from 'styled-components';
import { Table } from '@common/components/Table/Table';
import { useCustomTranslation } from '@utils/translate';
import { colors } from '@utils/uiTheme';
import { useParams } from 'react-router';
import { biddingRoundsSelectors } from '@store/selectors/biddingRounds';
import { useDispatch, useSelector } from 'react-redux';
import { setColumns, deleteColumn } from '@store/actions/biddingRounds';
import { BidColumn } from '@routes/Bid/common/utils';
import { BidColumnsCostComponentParsed } from '../../containers/CostCalculation/CostCalculation';

const StyledCheckCircle = styled(CheckCircle)`
  fill: ${colors.valid} !important;
`;

const StyledErrorCircle = styled(Cancel)`
  fill: ${colors.error} !important;
`;

const StyledCloseIcon = styled(Close)`
  width: 25px;
  fill: ${colors.iconGrey};
  cursor: pointer;
`;

const StyledIconContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

type Params = {
  id: string;
};

interface Props {
  selectedId: string;
  setSelectedId: (val: string) => void;
  bidColumnsCostComponentParsed: BidColumnsCostComponentParsed[];
}

export const CostCalculationTable: React.FC<Props> = ({
  selectedId,
  setSelectedId,
  bidColumnsCostComponentParsed,
}) => {
  const { t } = useCustomTranslation();
  const bidColumnsCostComponent: BidColumn[] = useSelector(
    biddingRoundsSelectors.getCalculationColumns
  );

  useEffect(() => {
    const hasTotalPriceColumn = bidColumnsCostComponent.find(
      ({ column }) => column === 'price_total'
    );
    if (hasTotalPriceColumn && !selectedId) {
      setSelectedId(hasTotalPriceColumn.id);
    }
  }, [bidColumnsCostComponent]);

  const { id: projectId } = useParams<Params>();
  const allColumns = useSelector(biddingRoundsSelectors.getRoundTableDesign);
  const dispatch = useDispatch();

  const handleDelete = (
    event: React.MouseEvent<SVGSVGElement, MouseEvent>,
    currentDeleteColumn
  ) => {
    event.stopPropagation();

    dispatch(
      setColumns(
        allColumns.filter(column => column.id !== currentDeleteColumn.Id)
      )
    );

    dispatch(deleteColumn(projectId, currentDeleteColumn.id));
  };

  const translateColumnTypes = {
    price_total: t('Total Price'),
    price: t('Price Component'),
  };

  return (
    <Table
      maxHeight="60vh"
      data={bidColumnsCostComponentParsed}
      onSelectRow={row => setSelectedId(row.column.id)}
      selectedRowId={selectedId}
      getUniqueKey={row => row.column.id}
      columns={[
        {
          Cell: ({ cell: { value } }) => (
            <StyledIconContainer>
              {value ? <StyledCheckCircle /> : <StyledErrorCircle />}
            </StyledIconContainer>
          ),
          Header: '',
          accessor: row => row.isValid,
          id: 'status',
          width: 80,
        },
        {
          Header: t('Name'),
          accessor: row => row.column.name,
          id: 'name',
        },
        {
          Header: t('Type'),
          accessor: row => translateColumnTypes[row.column.column],
          id: 'type',
        },
        {
          width: 150,
          minWidth: 150,
          Header: t('Expression'),
          accessor: row =>
            row.expression
              ? row.expression.map(expression => `${expression.name} `)
              : null,
          id: 'expression',
          tooltip: ({ cell }) =>
            cell.row.original.expression
              ? cell.row.original.expression.map(
                  expression => `${expression.name} `
                )
              : null,
        },
        {
          Header: '',
          Cell: ({ cell: { value } }) =>
            value.column !== 'price_total' ? (
              <StyledCloseIcon onClick={event => handleDelete(event, value)} />
            ) : null,
          accessor: row => row.column,
          id: 'delete',
        },
      ]}
    />
  );
};
