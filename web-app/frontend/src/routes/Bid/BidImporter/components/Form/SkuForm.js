import React from 'react';
import PropTypes from 'prop-types';
import { Field, Form, Formik } from 'formik';
import { RadioButtonGroup } from '@common/components/Form/RadioButtonGroup';
import { Box } from '@material-ui/core';
import { Button } from '@common/components/Button/Button';

import { useCustomTranslation } from '@utils/translate';
import styled from 'styled-components';
import { colors } from '@utils/uiTheme';

const StyledDescription = styled.p`
  height: 27px;
  color: ${colors.warning};
  font-size: 14px;
  line-height: 19px;
  margin-top: 20px;
`;

const StyledSaveButton = styled(Button)`
  &&.MuiButton-root {
    width: 110px;
    margin-top: 42px;
  }
`;

export const SkuForm = ({ onModalSubmit, initialValues, bidSettings }) => {
  const { t } = useCustomTranslation();

  const options = [
    {
      label: t('Yes'),
      value: true,
    },
    {
      label: t('No'),
      value: false,
    },
  ];

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={(values, actions) => {
        onModalSubmit(values);
        actions.setSubmitting(false);
      }}
    >
      {() => (
        <Form>
          <Field
            name="historicTemplateData"
            component={RadioButtonGroup}
            title={t('Do you want to use the data from your template?')}
            options={options}
          />
          <Field
            name="historicTargetPrices"
            component={RadioButtonGroup}
            title={t('Do you have historic or target prices?')}
            options={options}
          />
          <Box>
            {bidSettings && (
              <StyledDescription>
                {t(
                  'Please be aware that you are overriding pervious bidding settings'
                )}
              </StyledDescription>
            )}
            <StyledSaveButton type="submit" color="secondary">
              {t('Save')}
            </StyledSaveButton>
          </Box>
        </Form>
      )}
    </Formik>
  );
};

SkuForm.propTypes = {
  bidSettings: PropTypes.shape({}).isRequired,
  initialValues: PropTypes.shape({}).isRequired,
  onModalSubmit: PropTypes.func.isRequired,
};
