import React, { useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Redirect, Route, Switch, useParams } from 'react-router-dom';

import {
  getColumnTypes,
  getRoundDetails,
  getRoundTableDesign,
} from '@store/actions/biddingRounds';
import { biddingRoundsSelectors } from '@store/selectors/biddingRounds';
import { setBackButton } from '@store/actions/interface';

import { DescriptionContainer } from './containers/Description/Description';
import { DataUploadContainer } from './containers/DataUpload/DataUpload';
import { AddSkuContainer } from './containers/AddSku/AddSku';
import { AdditionalFilesContainer } from './containers/AdditionalFiles/AdditionalFiles';
import { CostCalculationContainer } from './containers/CostCalculation/CostCalculation';
import { BidCorrectionsContainer } from './containers/BidCorrections/BidCorrections';

const BidImporterRouterComponent = ({
  match,
  getRoundDetailsAction,
  getRoundTableDesignAction,
  getColumnTypesAction,
  setBackButtonAction,
}) => {
  const { id: projectId, roundId } = useParams();

  useEffect(() => {
    setBackButtonAction('arrow', `/projects/${projectId}/bid-importer`);

    return () => setBackButtonAction('');
  }, []);

  useEffect(() => {
    getRoundDetailsAction(projectId, roundId);
    getRoundTableDesignAction(projectId);
    getColumnTypesAction();
  }, [roundId, projectId]);

  return (
    <Switch>
      <Redirect
        from={`${match.path}/`}
        to={`${match.path}/description`}
        exact
      />
      <Route
        path={`${match.path}/description`}
        exact
        component={DescriptionContainer}
      />
      <Route
        path={`${match.path}/upload-match`}
        exact
        component={DataUploadContainer}
      />
      <Route path={`${match.path}/items`} exact component={AddSkuContainer} />
      <Route
        path={`${match.path}/additional-files`}
        exact
        component={AdditionalFilesContainer}
      />
      <Route
        path={`${match.path}/cost-calculation`}
        exact
        component={CostCalculationContainer}
      />
      <Route
        path={`${match.path}/bid-corrections`}
        exact
        component={BidCorrectionsContainer}
      />
    </Switch>
  );
};

BidImporterRouterComponent.propTypes = {
  getColumnTypesAction: PropTypes.func.isRequired,
  getRoundDetailsAction: PropTypes.func.isRequired,
  getRoundTableDesignAction: PropTypes.func.isRequired,
  match: PropTypes.shape({}).isRequired,
  setBackButtonAction: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  columnExamplesMap: biddingRoundsSelectors.getColumnExamplesMap(state),
  details: biddingRoundsSelectors.getRoundDetails(state),
  roundTableDesign: biddingRoundsSelectors.getRoundTableDesign(state),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getColumnTypesAction: getColumnTypes,
      getRoundDetailsAction: getRoundDetails,
      getRoundTableDesignAction: getRoundTableDesign,
      setBackButtonAction: setBackButton,
    },
    dispatch
  );

export const BidImporterRouter = connect(
  mapStateToProps,
  mapDispatchToProps
)(BidImporterRouterComponent);
