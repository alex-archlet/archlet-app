import React, { useEffect, useMemo, useState } from 'react';
import styled from 'styled-components';
import { useParams } from 'react-router-dom';
import { useHistory } from 'react-router';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  createDemandsOffers,
  createRoundTableContent,
  getDemandsOffers,
  getRoundDetails,
  getRoundTableDesign,
  updateBiddingSettings,
} from '@actions/biddingRounds';
import { getProjectUnits } from '@actions/projects';
import { biddingRoundsSelectors } from '@selectors/biddingRounds';

import { Button } from '@common/components/Button/Button';
import { useCustomTranslation } from '@utils/translate';
import { colors } from '@utils/uiTheme';
import { BidColumn } from '@routes/Bid/common/utils';
import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';
import { Box } from '@material-ui/core';
import { Modal } from '@common/components/Modal/Modal';
import { RoundHeader } from '../../../common/RoundHeader/RoundHeader';
import {
  Match,
  ProjectsLayout,
} from '../../../../ProjectDetails/components/ProjectsLayout/ProjectsLayout';
import { SkuForm } from '../../components/Form/SkuForm';
import { useExcelPreview } from '../../../common/hooks/useExcelPreview';

import { AddSkuGrid } from '../../components/AddSkuGrid/AddSkuGrid';
import { useSkuGrid } from '../../components/AddSkuGrid/useSkuGrid';

const StyledContainer = styled.div`
  padding: 0 65px;
`;

const StyledHeading = styled.h2`
  font-size: 22px;
`;

const StyledDescription = styled.p`
  height: 27px;
  opacity: 0.55;
  color: ${colors.error};
  font-size: 14px;
  line-height: 19px;
  margin-left: 30px;
  margin-top: 20px;
`;

const StyledButton = styled(Button)`
  &&.MuiButton-root {
    display: flex;
    justify-content: flex-start;
    align-items: center;
    margin-bottom: 11px;
    background-color: #eef2f4;
    font-weight: 500;
    font-size: 16px;
    line-height: 13px;
    text-transform: none;
    cursor: pointer;
    border: none;
    color: #44566c;
  }

  &&.MuiButton-outlined {
    border-radius: 5px;
    padding: 8px 10px;
  }
`;

const INITIAL_FORM_STATE = {
  historicTargetPrices: true,
  historicTemplateData: true,
};

interface SkuGridRow {
  attributes: {
    [bidColumnId: string]: string | number | null | undefined;
  };
}

type BiddingSettings = {
  historicTemplateData: boolean;
  historicTargetPrices: boolean;
};

interface RoundDetails {
  lastTemplateFile: {
    sheetName: string;
    id: string;
    rowNumber: number;
  };
}
interface Props {
  bidSettings: BiddingSettings;
  createDemandsOffersAction: (
    projectId: string,
    roundId: string,
    payload: {
      rows: SkuGridRow[];
    }
  ) => void;
  details: RoundDetails;
  getRoundDetailsAction: (projectId: string, roundId: string) => void;
  match: Match;
  updateBiddingSettingsAction: (
    projectId: string,
    roundId: string,
    { ...values }: BiddingSettings
  ) => void;
}

const accumulateGridColumns = (
  grid: Record<string, string | number>[],
  bidColumns: BidColumn[]
) => {
  return grid.map((row) => {
    const attributes = bidColumns.reduce((acc, bidColumn) => {
      acc[bidColumn.id] = row[bidColumn.id];
      return acc;
    }, {});

    return {
      attributes,
      id: row.id,
      demandId: row.demandId,
    };
  });
};

export const AddSku: React.FC<Props> = ({
  details,
  createDemandsOffersAction,
  match,
  bidSettings,
  updateBiddingSettingsAction,
  getRoundDetailsAction,
}) => {
  const { t } = useCustomTranslation();

  const { id: projectId, roundId } = useParams<{
    id: string;
    roundId: string;
  }>();

  const history = useHistory();

  const [isColumnForm, setIsColumnForm] = useState(false);

  const [excelDataMode, setExcelDataMode] = useState(false);

  useEffect(() => {
    if (bidSettings === null) {
      setIsColumnForm(true);
    }
  }, [bidSettings]);

  const onReset = () => {
    setIsColumnForm(false);
    setExcelDataMode(true);
  };

  const onModalSubmit = async (values) => {
    await updateBiddingSettingsAction(projectId, roundId, { ...values });
    getRoundDetailsAction(projectId, roundId);
    onReset();
  };

  const { lastTemplateFile } = details;

  const { tableData } = useExcelPreview(
    lastTemplateFile ? lastTemplateFile.id : null,
    true,
    {
      type: 'records',
      row: lastTemplateFile ? lastTemplateFile.rowNumber - 1 : undefined,
    }
  );

  const excelData = useMemo(() => {
    if (!lastTemplateFile || !tableData) {
      return [];
    }

    const data = tableData?.sheets[lastTemplateFile.sheetName]?.data ?? [];

    return data;
  }, [lastTemplateFile, tableData]);

  const { grid, setGrid, bidColumnsToUse } = useSkuGrid(
    projectId,
    roundId,
    excelData,
    bidSettings,
    excelDataMode
  );

  const handleNextStep = async () => {
    await createDemandsOffersAction(projectId, roundId, {
      rows: accumulateGridColumns(grid, bidColumnsToUse),
    });

    getRoundDetailsAction(projectId, roundId);
    history.push(
      `/projects/${projectId}/bid-importer/${roundId}/additional-files`
    );
  };

  const saveProgressAction = async () => {
    await createDemandsOffersAction(projectId, roundId, {
      rows: accumulateGridColumns(grid, bidColumnsToUse),
    });
  };

  const formValues = bidSettings || INITIAL_FORM_STATE;

  return (
    <ProjectsLayout
      // @ts-ignore
      match={match}
      sidebarLevel={2}
      saveProgressAction={saveProgressAction}
    >
      <StyledContainer>
        {isColumnForm && (
          <Modal
            isOpen={isColumnForm}
            handleClose={() => setIsColumnForm(false)}
            maxWidth="md"
          >
            <Box width="662px" padding="60px 178px">
              <StyledHeading>{t('Specify your baseline')}</StyledHeading>
              <SkuForm
                onModalSubmit={onModalSubmit}
                initialValues={formValues}
                bidSettings={bidSettings}
              />
            </Box>
          </Modal>
        )}
        <RoundHeader
          title={t('Add your Items / SKUs')}
          // eslint-disable-next-line max-len
          textUnderTitle={t(
            'Add the information on your items / SKUs in the bid sheet. If you have historic or target prices, add them to the table as well.'
          )}
          nextButtonLabel={t('Next Step')}
          // @ts-ignores
          nextButtonAction={handleNextStep}
        />
        <VerticalSpacer height={5} />
        <StyledButton onClick={() => setIsColumnForm(true)} variant="outlined">
          {t('Baseline Settings')}
        </StyledButton>
        <VerticalSpacer height={30} />
        {!bidSettings && (
          <StyledDescription>
            {t(
              'Please click on the baseline settings and choose from the options provided before continuing.'
            )}
          </StyledDescription>
        )}
        <VerticalSpacer height={5} />
        <AddSkuGrid columns={bidColumnsToUse} grid={grid} setGrid={setGrid} />
        <VerticalSpacer height={5} />
      </StyledContainer>
    </ProjectsLayout>
  );
};

const mapStateToProps = state => ({
  bidSettings: biddingRoundsSelectors.getRoundSettings(state),
  columnTypes: biddingRoundsSelectors.getColumnTypes(state),
  details: biddingRoundsSelectors.getRoundDetails(state),
  grid: biddingRoundsSelectors.getRoundTableContent(state),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      createDemandsOffersAction: createDemandsOffers,
      createRoundTableContentAction: createRoundTableContent,
      getDemandsOffersAction: getDemandsOffers,
      getProjectUnitsAction: getProjectUnits,
      getRoundDetailsAction: getRoundDetails,
      getRoundTableDesignAction: getRoundTableDesign,
      updateBiddingSettingsAction: updateBiddingSettings,
    },
    dispatch
  );

export const AddSkuContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(AddSku);
