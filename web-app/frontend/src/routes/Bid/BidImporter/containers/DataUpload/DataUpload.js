import React, { useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { useParams } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import styled from 'styled-components';
import { useHistory } from 'react-router';
import { Button } from '@common/components/Button/Button';
import { AssessmentOutlined, Check, Search } from '@material-ui/icons';

import { useCustomTranslation } from '@utils/translate';
import { projectsSelectors } from '@selectors/projects';
import {
  getRoundDetails,
  getRoundTableDesign,
  runColumnMatchingJob,
  updateRoundTableDesign,
  uploadTemplate,
} from '@store/actions/biddingRounds';
import { biddingRoundsSelectors } from '@store/selectors/biddingRounds';

import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';
import { UploadField } from '@common/components/UploadField/UploadField';
import { Text } from '@common/components/Text/Text';
import { JobModalProgressContainer } from '@common/components/JobModalProgress/JobModalProgressContainer';
import { IconBox } from '@common/components/IconBox/IconBox';

import { RoundHeader } from '../../../common/RoundHeader/RoundHeader';
import { ProjectsLayout } from '../../../../ProjectDetails/components/ProjectsLayout/ProjectsLayout';
import { TableHeaderPreview } from '../../components/TableHeaderPreview/TableHeaderPreview';
import { DataSettings } from '../../components/DataSettings/DataSettings';
import { ErrorSentence } from '../../../common/ErrorSentence/ErrorSentence';
import { useExcelPreview } from '../../../common/hooks/useExcelPreview';

const StyledLayoutContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const StyledContainer = styled.div`
  padding: 0 65px;
`;

const ROWS_TO_SHOW = 9;

const transpose = (m) => {
  if (!m.length) {
    return m;
  }

  return m[0].map((x, i) => m.map(y => y[i]));
};

const hasMatching = details =>
  details && details.lastTemplateFile && details.lastTemplateFile.matchings;

const isUnmatched = ({ typeId }) => typeId === null;

const getUnmatchedColumns = roundTableDesign =>
  roundTableDesign ? roundTableDesign.filter(isUnmatched) : [];

const newColumnIdPrefix = 'new-column-';

const DataUpload = ({
  columnTypes,
  columnExamplesMap,
  details,
  roundTableDesign,
  roundHasOffers,
  runColumnMatchingJobAction,
  uploadTemplateAction,
  getRoundDetailsAction,
  getRoundTableDesignAction,
  updateRoundTableDesignAction,
}) => {
  const history = useHistory();
  const { t } = useCustomTranslation();
  const [errorList, setErrorList] = useState(null);
  const { id: projectId, roundId } = useParams();

  const { lastTemplateFile } = details;
  const { tableData, previewState, setPreviewState } = useExcelPreview(
    lastTemplateFile ? lastTemplateFile.id : null,
    lastTemplateFile && !lastTemplateFile.matchings
  );

  const onDrop = async (formData) => {
    await uploadTemplateAction(projectId, roundId, formData);

    getRoundDetailsAction(projectId, roundId);
  };

  const { sheetNames, sheets } = tableData;

  const selectedSheetNameData =
    previewState.sheet &&
    sheets[previewState.sheet] &&
    sheets[previewState.sheet].data;

  const { startIndex } = previewState;

  const rawPreviewData = useMemo(() => {
    if (!selectedSheetNameData) {
      return [];
    }

    const safeIndex =
      Math.min(Math.max(startIndex, 1), selectedSheetNameData.length) - 1;
    const slicedData = selectedSheetNameData.slice(safeIndex);
    const rows = slicedData.slice(1, ROWS_TO_SHOW);
    const transposed = transpose(rows);
    const headers = slicedData[0] || [];

    return headers.map((column, index) => ({
      example: transposed[index],
      name: column,
    }));
  }, [selectedSheetNameData, startIndex]);

  const parsedPreviewData = useMemo(() => {
    if (
      roundTableDesign.length === 0 ||
      !details.lastTemplateFile ||
      !details.lastTemplateFile.matchings
    ) {
      return [];
    }

    const {
      excel_values: excelExamplesMap,
    } = details.lastTemplateFile.matchings;

    const getExamplesForExcelColumn = ({ name, typeId }) =>
      excelExamplesMap[name] || columnExamplesMap[typeId];

    // TODO: add matching score when ready.
    return roundTableDesign.map(column => ({
      ...column,
      example: getExamplesForExcelColumn(column),
    }));
  }, [roundTableDesign, details.lastTemplateFile, columnExamplesMap]);

  const hasRoundMatching = hasMatching(details);

  const previewData = hasRoundMatching ? parsedPreviewData : rawPreviewData;

  const onStartColumnMatching = async () => {
    try {
      await runColumnMatchingJobAction(
        projectId,
        [details.lastTemplateFile.id],
        previewState.sheet,
        previewState.startIndex
      );
      getRoundDetailsAction(projectId, roundId);
      getRoundTableDesignAction(projectId);
    } catch (err) {
      getRoundDetailsAction(projectId, roundId);
    }
  };

  const validateColumnsToCheckRequiredColumns = (columnsToSubmit) => {
    const requiredColumns = columnTypes.filter(column => column.isDefault);
    const array = [];

    requiredColumns.forEach((requiredColumn) => {
      if (
        !columnsToSubmit.some(
          columnToSubmit => requiredColumn.id === columnToSubmit.typeId
        )
      ) {
        array.push(requiredColumn.text);
      }
    });

    return array;
  };

  const handleNextStep = () => {
    const columnsToSubmit = previewData.map(
      ({
        id,
        inputBy,
        isMandatory,
        isVisible,
        matchingConfidence,
        name,
        typeId,
        singleChoice,
        limitMax,
        limitMin,
      }) => {
        const isNew = id.includes(newColumnIdPrefix);

        return {
          ...(!isNew ? { id } : {}),
          inputBy,
          isMandatory,
          isVisible,
          matchingConfidence,
          name,
          typeId,
          singleChoice,
          limitMax,
          limitMin,
        };
      }
    );

    const errorArray = validateColumnsToCheckRequiredColumns(columnsToSubmit);

    if (errorArray.length === 0) {
      updateRoundTableDesignAction(projectId, {
        columns: columnsToSubmit,
      }).then(() => {
        history.push(`/projects/${projectId}/bid-importer/${roundId}/items`);
      });
    } else {
      setErrorList(errorArray);
    }
  };

  const saveProgressAction = async () => {
    const columnsToSubmit = previewData.map(
      ({
        id,
        inputBy,
        isMandatory,
        isVisible,
        matchingConfidence,
        name,
        typeId,
      }) => {
        const isNew = id.includes(newColumnIdPrefix);

        return {
          ...(!isNew ? { id } : {}),
          inputBy,
          isMandatory,
          isVisible,
          matchingConfidence,
          name,
          typeId,
        };
      }
    );

    await updateRoundTableDesignAction(projectId, {
      columns: columnsToSubmit,
    });
  };

  const unmatchedColumns = getUnmatchedColumns(roundTableDesign);
  const columnMatchingMessage = unmatchedColumns.length
    ? t('You have {{counter}} unmatched columns', {
        counter: unmatchedColumns.length,
      })
    : null;

  return (
    <ProjectsLayout sidebarLevel={2} saveProgressAction={saveProgressAction}>
      <StyledContainer>
        {errorList && <ErrorSentence errorFields={errorList} />}
        <RoundHeader
          title={t('Upload & Matching')}
          textUnderTitle={t(
            'Upload your bid sheet and assign the relevant columns.'
          )}
          nextButtonLabel={t('Next Step')}
          nextButtonAction={handleNextStep}
        />
        {!roundHasOffers && (
          <>
            <VerticalSpacer height={20} />
            <UploadField
              onDrop={onDrop}
              filename={
                details.lastTemplateFile && details.lastTemplateFile.fullName
              }
            />
          </>
        )}
        {details.lastTemplateFile && (
          <>
            {previewData.length > 0 && (
              <>
                {!hasRoundMatching && (
                  <DataSettings
                    values={previewState}
                    onChange={setPreviewState}
                    sheets={sheetNames}
                  />
                )}
                <VerticalSpacer height={30} />
                <StyledLayoutContainer>
                  <Text fontSize={14} fontWeight={400}>
                    {columnMatchingMessage}
                  </Text>
                  <Button
                    color="secondary"
                    size="large"
                    onClick={onStartColumnMatching}
                    disabled={hasRoundMatching}
                  >
                    {t('Start Matching')}
                  </Button>
                </StyledLayoutContainer>
                <VerticalSpacer height={15} />
                <TableHeaderPreview
                  key={startIndex} // re-render the whole component since keys might be duplicated
                  columns={previewData}
                  hasRoundMatching={hasRoundMatching}
                />
              </>
            )}
          </>
        )}
        <JobModalProgressContainer
          title={t('Matching columns...')}
          iconHelperTextElement={
            <>
              <IconBox text={t('Processing Data')}>
                <AssessmentOutlined />
              </IconBox>
              <IconBox text={t('Matching Columns')}>
                <Check />
              </IconBox>
              <IconBox text={t('Identifying Errors')}>
                <Search />
              </IconBox>
            </>
          }
        />
      </StyledContainer>
    </ProjectsLayout>
  );
};

DataUpload.propTypes = {
  columnExamplesMap: PropTypes.shape({}).isRequired,
  columnTypes: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  details: PropTypes.shape({}).isRequired,
  getRoundDetailsAction: PropTypes.func.isRequired,
  getRoundTableDesignAction: PropTypes.func.isRequired,
  roundDetails: PropTypes.shape({}).isRequired,
  roundHasOffers: PropTypes.bool.isRequired,
  roundTableDesign: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  runColumnMatchingJobAction: PropTypes.func.isRequired,
  updateRoundTableDesignAction: PropTypes.func.isRequired,
  uploadTemplateAction: PropTypes.func.isRequired,
  validation: PropTypes.shape({}).isRequired,
};

const mapStateToProps = state => ({
  columnExamplesMap: biddingRoundsSelectors.getColumnExamplesMap(state),
  columnTypes: biddingRoundsSelectors.getColumnTypes(state),
  details: biddingRoundsSelectors.getRoundDetails(state),
  projectDetails: projectsSelectors.getProjectDetails(state),
  roundHasOffers: biddingRoundsSelectors.getRoundHasOffers(state),
  roundTableDesign: biddingRoundsSelectors.getRoundTableDesign(state),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getRoundDetailsAction: getRoundDetails,
      getRoundTableDesignAction: getRoundTableDesign,
      runColumnMatchingJobAction: runColumnMatchingJob,
      updateRoundTableDesignAction: updateRoundTableDesign,
      uploadTemplateAction: uploadTemplate,
    },
    dispatch
  );

export const DataUploadContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(DataUpload);
