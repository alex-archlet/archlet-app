import React, { useEffect, useMemo, useState } from 'react';
import { connect } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import styled from 'styled-components';
import queryString from 'query-string';

import { projectsSelectors } from '@selectors/projects';
import {
  deleteBid,
  getRoundDetails,
  updateBidImportFile,
  updateColumnMatching,
  uploadBid,
} from '@store/actions/biddingRounds';
import { biddingRoundsSelectors } from '@store/selectors/biddingRounds';
import { useCustomTranslation } from '@utils/translate';

import { ConfirmationDialog } from '@common/components/ConfirmationDialog/ConfirmationDialog';
import { CancelButton } from '@common/components/Button/CancelButton';
import { DeleteButton } from '@common/components/Button/DeleteButton';

import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';
import { UploadField } from '@common/components/UploadField/UploadField';

import { Card } from '@common/components/Card/Card';
import { Text } from '@common/components/Text/Text';
import { ProjectsLayout } from '@routes/ProjectDetails/components/ProjectsLayout/ProjectsLayout';
import { getErrorsForAllFiles } from '@routes/Bid/common/utils';
import { colors } from '@utils/uiTheme';
import { openInNewTab } from '@utils/windowUtils';
import { BACKEND_REST_REQUEST_PREFIX } from '@utils/request';
import { NextButton } from '@routes/Bid/common/RoundHeader/NextButton';
import { PageTitle } from '@common/components/PageTitle/PageTitle';
import { Box } from '@material-ui/core';
import { Row } from '@common/components/Row/Row';
import { AdditionalFilesTable } from '../../components/AdditionalFilesTable/AdditionalFilesTable';
import { MatchingTable } from '../../components/BidColumnMatching/MatchingTable';

const StyledContainer = styled.div`
  padding: 0;
`;

const StyledTitleContainer = styled.div``;

type BidColumnToExcelColumn = {
  bid_column: string;
  excel_column: string;
  matching: boolean;
  message: string;
};

type Matching = {
  excel_columns: string[];
  excel_values: { [key: string]: string[] };
  matchings: BidColumnToExcelColumn[];
};

export type BidImportFile = {
  branchId: string;
  createdAt: string;
  fileExtension: string;
  fileType: string;
  fullName: string;
  id: string;
  matchings: Matching;
  name: string;
  projectId: string;
  roundId: string;
  rowNumber: number;
  scenarioId: string | null;
  sheetName: string;
  updatedAt: string;
};

interface Props {
  bidImportFiles: BidImportFile[];
  deleteBidAction: (
    projectId: string,
    roundId: string,
    deleteFilesId: string[]
  ) => void;
  supplierColumn: any[];
  uploadBidsAction: (
    projectId: string,
    roundId: string,
    form: HTMLFormElement
  ) => void;
  getRoundDetailsAction: (projectId: string, roundId: string) => void;
  roundTableDesign: any[];
  updateBidImportFileAction: (file: BidImportFile) => void;
  updateColumnMatchingAction: (
    projectId: string,
    roundId: string,
    bidImportFiles: BidImportFile[]
  ) => Promise<void>;
}

const AdditionalFiles: React.FC<Props> = ({
  bidImportFiles,
  deleteBidAction,
  supplierColumn,
  uploadBidsAction,
  getRoundDetailsAction,
  roundTableDesign,
  updateBidImportFileAction,
  updateColumnMatchingAction,
}) => {
  const [fileToDelete, setFileToDelete] = useState<string[] | null>(null);
  const [selectedRowId, setSelectedRowId] = useState(undefined);
  const { id: projectId, roundId } = useParams<{
    id: string;
    roundId: string;
  }>();
  const { t } = useCustomTranslation();
  const history = useHistory();

  useEffect(() => {
    getRoundDetailsAction(projectId, roundId);
  }, [projectId, roundId]);

  const onDrop = async (formData: HTMLFormElement) => {
    await uploadBidsAction(projectId, roundId, formData);

    getRoundDetailsAction(projectId, roundId);
  };

  const onSelectRow = (_, rowId) => {
    setSelectedRowId(rowId);
  };

  const selectedRow = selectedRowId
    ? bidImportFiles.find(({ id }) => id === selectedRowId)
    : null;

  const onUpdateMatchings = (matchings: BidColumnToExcelColumn[]) => {
    if (selectedRow?.matchings) {
      const newFile = {
        ...selectedRow,
        matchings: {
          ...selectedRow.matchings,
          matchings,
        },
      };

      updateBidImportFileAction(newFile);
    }
  };

  const onNextPressed = () => {
    updateColumnMatchingAction(projectId, roundId, bidImportFiles).then(() => {
      history.push(
        `/projects/${projectId}/bid-importer/${roundId}/bid-corrections`
      );
    });
  };

  const saveProgressAction = () => {
    updateColumnMatchingAction(projectId, roundId, bidImportFiles);
  };

  const onDeleteConfirm = async () => {
    if (fileToDelete) {
      await deleteBidAction(projectId, roundId, fileToDelete);
      setFileToDelete(null);
    }
  };

  const onDeleteRequest = (file: string[]) => {
    setFileToDelete(file);
  };

  const onDeleteCancel = () => {
    setFileToDelete(null);
  };

  const onDownloadFiles = (fileIds: string[]) => {
    const query = queryString.stringify({ fileIds });
    return openInNewTab(
      `${BACKEND_REST_REQUEST_PREFIX}/projects/${projectId}/bidding_rounds/${roundId}/bid_download?${query}`
    );
  };

  const fileErrors = useMemo(() => getErrorsForAllFiles(bidImportFiles), [
    bidImportFiles,
  ]);

  return (
    // @ts-ignore
    <ProjectsLayout
      sidebarLevel={2}
      saveProgressAction={saveProgressAction}
      backgroundColor={colors.darkBackground}
    >
      <StyledContainer>
        <StyledTitleContainer>
          <PageTitle
            title={t('Bid Upload')}
            titleColor={colors.textColor}
            rightSide={
              // TODO: Fix when we have all in Card styles
              <Box pr={25}>
                <NextButton label={t('Next Step')} action={onNextPressed} />
              </Box>
            }
          />
        </StyledTitleContainer>
        <Row>
          <Card
            title={t('Offers Upload')}
            description={t(
              'Upload your supplier offers with the previously defined data structure.'
            )}
          >
            <VerticalSpacer height={5} />
            <UploadField onDrop={onDrop} />
            <Text color="#999" fontWeight={400}>
              {t(
                'Select one file to see details in card below. Select multiple files if you want to delete or download them.'
              )}
            </Text>
            <VerticalSpacer height={10} />
            {bidImportFiles && bidImportFiles.length > 0 && (
              <AdditionalFilesTable
                data={bidImportFiles}
                selectedRowId={selectedRowId}
                fileErrors={fileErrors}
                onSelectRow={onSelectRow}
                onDelete={onDeleteRequest}
                onDownload={onDownloadFiles}
                supplierColumn={supplierColumn}
              />
            )}
          </Card>
        </Row>
        {selectedRow && (
          <Row>
            <Card
              title={t('Offers Matching')}
              description={t(
                'Verify the matching of the columns of the supplier offers sheet with the previously defined data structure.'
              )}
            >
              <VerticalSpacer height={5} />
              <MatchingTable
                matchings={selectedRow.matchings}
                columns={roundTableDesign}
                selectedFileErrors={fileErrors[selectedRow.id]}
                onUpdateMatchings={onUpdateMatchings}
              />
            </Card>
          </Row>
        )}
        {fileToDelete && (
          <ConfirmationDialog
            message={t('Do you really want to delete')}
            handleClose={onDeleteCancel}
            // TODO: Fix this
            // @ts-ignore
            handleConfirm={onDeleteConfirm}
            isOpen
            ButtonActions={
              <>
                <DeleteButton onClick={onDeleteConfirm} autoFocus>
                  {t('Delete')}
                </DeleteButton>
                <CancelButton onClick={onDeleteCancel}>{t('No')}</CancelButton>
              </>
            }
          />
        )}
      </StyledContainer>
    </ProjectsLayout>
  );
};

const mapStateToProps = state => ({
  bidImportFiles: biddingRoundsSelectors.getRoundBidImportFiles(state),
  projectDetails: projectsSelectors.getProjectDetails(state),
  roundTableDesign: biddingRoundsSelectors.getRoundTableDesign(state),
  supplierColumn: biddingRoundsSelectors.getRoundSupplierColumn(state),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      deleteBidAction: deleteBid,
      getRoundDetailsAction: getRoundDetails,
      updateBidImportFileAction: updateBidImportFile,
      updateColumnMatchingAction: updateColumnMatching,
      uploadBidsAction: uploadBid,
    },
    dispatch
  );

export const AdditionalFilesContainer = connect(
  mapStateToProps,
  mapDispatchToProps
  // @ts-ignore
)(AdditionalFiles);
