import React, { useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { useParams } from 'react-router-dom';
import { AssessmentOutlined, Check, Search } from '@material-ui/icons';
import { IconBox } from '@common/components/IconBox/IconBox';

import {
  createOrUpdateCostCalculation,
  setColumns,
} from '@actions/biddingRounds';

import { biddingRoundsSelectors } from '@selectors/biddingRounds';
import { useCustomTranslation } from '@utils/translate';

import { JobModalProgressContainer } from '@common/components/JobModalProgress/JobModalProgressContainer';
import { ProjectsLayout } from '@routes/ProjectDetails/components/ProjectsLayout/ProjectsLayout';
import { colors } from '@utils/uiTheme';
import { Box } from '@material-ui/core';
import { BidColumn } from '@routes/Bid/common/utils';
import { ValidElement } from '@routes/Bid/common/CostCalculation/CostCalculationExpressionBar';
import { Card } from '@common/components/Card/Card';
import { PageTitle } from '@common/components/PageTitle/PageTitle';
import { Row } from '@common/components/Row/Row';
import { ALLOWED_OPERATIONS } from '@routes/Bid/common/CostCalculation/AvailableElements';
import {
  parseStringToColumnIds,
  parseValueArray,
  validateCalculation,
} from '@routes/Bid/common/CostCalculation/utils';
import { Text } from '@common/components/Text/Text';
import { AddCostCalculation } from '../../components/AddCostCalculation/AddCostCalculation';
import { CostCalculationTable } from '../../components/CostCalculationTable/CostCalculationTable';
import { ExpressionEvaluation } from '../../components/ExpressionEvaluation/ExpressionEvaluation';
import { ExpressionExampleButtonAndTable } from '../../components/ExpressionExampleButtonAndTable/ExpressionExampleButtonAndTable';
import { CompleteImportButton } from '../../components/CompleteImportButton/CompleteImportButton';

const StyledContainer = styled.div`
  padding: 0px;
`;

const StyledContentContainer = styled.div`
  margin-left: 10px;
`;

const newColumnIdPrefix = 'new-column-';
const totalPriceColumnType = 'price_total';
const INITIAL_TOTAL_COLUMN = {
  column: totalPriceColumnType,
  id: `${newColumnIdPrefix}0`,
  inputBy: 'supplier',
  isAutomatic: true,
  isMandatory: false,
  isVisible: true,
  name: 'Total Cost',
  calculation: null,
};
export interface BidColumnsCostComponentParsed {
  expression: ValidElement[];
  isValid: boolean | null;
  column: BidColumn;
}

export const CostCalculationContainer = () => {
  const dispatch = useDispatch();
  const [validationError, setValidationError] = useState<null | string>();

  const bidColumnsCostComponent: BidColumn[] = useSelector(
    biddingRoundsSelectors.getCalculationColumns
  );

  const allColumns: BidColumn[] = useSelector(
    biddingRoundsSelectors.getRoundTableDesign
  );

  const { id: projectId, roundId } = useParams<{
    id: string;
    roundId: string;
  }>();

  const [selectedId, setSelectedId] = useState<string>('');

  const { t } = useCustomTranslation();

  // set the total price column if there isn't any column
  useEffect(() => {
    if (!bidColumnsCostComponent.length) {
      dispatch(setColumns([...allColumns, INITIAL_TOTAL_COLUMN]));
    }
  }, [bidColumnsCostComponent]);

  useEffect(() => {
    if (validationError) {
      setValidationError('');
    }
  }, [allColumns]);

  const columnObjects = useMemo(
    () => [
      ...(allColumns || []).map(column => ({
        column,
        id: column.id,
        name: column.name,
        type: 'column',
      })),
    ],
    [allColumns]
  );

  const validOptions = useMemo(
    () => [...ALLOWED_OPERATIONS, ...columnObjects],
    [columnObjects]
  );

  const bidColumnsCostComponentParsed: BidColumnsCostComponentParsed[] = useMemo(() => {
    return bidColumnsCostComponent.map((column) => {
      if (column?.calculation) {
        const splitExpression = parseStringToColumnIds(column.calculation);
        const expression = parseValueArray(splitExpression, validOptions);

        return {
          column,
          expression,
          isValid: validateCalculation(expression),
        };
      }
      return {
        column,
        expression: '',
        isValid: false,
      };
    });
  }, [bidColumnsCostComponent, validOptions]);

  const editBidColumnCostComponent = (newColumn) => {
    const index = allColumns.findIndex(col => col.id === newColumn.id);
    const newColumns = [...allColumns];

    if (index === -1) {
      newColumns.push(newColumn);
    } else {
      newColumns[index] = newColumn;
    }
    dispatch(setColumns(newColumns));
  };

  const currentBidCostComponent = useMemo(() => {
    if (selectedId) {
      return bidColumnsCostComponent.find(
        bidColumnCostComponent => bidColumnCostComponent?.id === selectedId
      );
    }
    return null;
  }, [selectedId, bidColumnsCostComponent]);

  const handleCreateOrUpdateCostCalculation = () => {
    if (currentBidCostComponent) {
      setSelectedId(currentBidCostComponent.id);
    }
    dispatch(
      createOrUpdateCostCalculation(projectId, roundId, bidColumnsCostComponent)
    );
  };

  return (
    <ProjectsLayout
      sidebarLevel={2}
      saveProgressAction={() => handleCreateOrUpdateCostCalculation()}
      backgroundColor={colors.darkBackground}
    >
      <StyledContainer>
        <PageTitle
          title={t('Cost Calculation')}
          titleColor={colors.textColor}
          rightSide={
            // TODO: Fix when we have all in Card styles
            <Box pr={10}>
              <CompleteImportButton
                handleCreateOrUpdateCostCalculation={
                  handleCreateOrUpdateCostCalculation
                }
                bidColumnsCostComponentParsed={bidColumnsCostComponentParsed}
                setValidationError={setValidationError}
              />
            </Box>
          }
        />
        <Row>
          <Card
            title={t('Cost Component')}
            description={t(
              'Click on a cost component to see it in detail or add a new calculation'
            )}
            rightSide={<AddCostCalculation />}
          >
            <StyledContentContainer>
              {validationError ? (
                <Text color={colors.error} fontWeight={400}>
                  {validationError}
                </Text>
              ) : (
                <Box />
              )}
              <CostCalculationTable
                selectedId={selectedId}
                setSelectedId={setSelectedId}
                bidColumnsCostComponentParsed={bidColumnsCostComponentParsed}
              />
            </StyledContentContainer>
          </Card>
        </Row>
        <Row>
          {currentBidCostComponent && (
            <Card
              title={t('Cost Formula')}
              description={t(
                'By clicking on the different buttons you can define here your cost formula'
              )}
            >
              <StyledContentContainer>
                <ExpressionEvaluation
                  currentBidCostComponent={currentBidCostComponent}
                  handleUpdateAndCreateBidColumn={editBidColumnCostComponent}
                />
                <ExpressionExampleButtonAndTable
                  currentBidCostComponent={currentBidCostComponent}
                  handleCreateOrUpdateCostCalculation={
                    handleCreateOrUpdateCostCalculation
                  }
                  bidColumnsCostComponentParsed={bidColumnsCostComponentParsed}
                  setValidationError={setValidationError}
                />
              </StyledContentContainer>
            </Card>
          )}
        </Row>

        <JobModalProgressContainer
          title={t('Matching columns...')}
          iconHelperTextElement={
            <>
              <IconBox text={t('Evaluating Calculation')}>
                <AssessmentOutlined />
              </IconBox>
              <IconBox text={t('Validating Prices')}>
                <Check />
              </IconBox>
              <IconBox text={t('Identifying Opportunities')}>
                <Search />
              </IconBox>
            </>
          }
        />
      </StyledContainer>
    </ProjectsLayout>
  );
};
