import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { useParams } from 'react-router-dom';
import { useHistory } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button } from '@common/components/Button/Button';
import { AssessmentOutlined, Check, Search } from '@material-ui/icons';
import { useCustomTranslation } from '@utils/translate';

import {
  getOfferProcessed,
  runOfferProcessingJob,
  updateOfferProcessed,
  getRoundDetails,
} from '@actions/biddingRounds';
import { projectsSelectors } from '@selectors/projects';
import { biddingRoundsSelectors } from '@selectors/biddingRounds';

import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';
import { RoundHeader } from '@routes/Bid/common/RoundHeader/RoundHeader';
import { JobModalProgressContainer } from '@common/components/JobModalProgress/JobModalProgressContainer';
import { IconBox } from '@common/components/IconBox/IconBox';
import { useBlockingJobEnd } from '@routes/Scenarios/components/useBlockingJobEnd';
import { BidColumn } from '@routes/Bid/common/utils';
import { BidColumnFlatRow } from '@common/components/Grid/Grid';
import {
  Match,
  ProjectsLayout,
} from '../../../../ProjectDetails/components/ProjectsLayout/ProjectsLayout';

import { BidCorrectionsGrid } from '../../components/BidCorrectionsGrid/BidCorrectionsGrid';
import { useBidCorrectionOfferGrid } from '../../components/BidCorrectionsGrid/useBidCorrectionsOfferGrid';

const StyledContainer = styled.div`
  padding: 0 65px;
`;
const accumulateGridColumns = (
  grid: BidColumnFlatRow[],
  bidColumns: BidColumn[]
) => {
  return grid.map((row) => {
    const attributes = bidColumns.reduce((acc, bidColumn) => {
      acc[bidColumn.id] = row[bidColumn.id];
      return acc;
    }, {});

    return {
      attributes,
      id: row.id,
      processing_messages: row.processing_messages,
    };
  });
};

// find better place
export type ColumnType = {
  example: string;
  id: string;
  inputBy: string;
  isDefault: boolean;
  text: string;
  createdAt: Date | null;
  updatedAt: Date | null;
  type: string;
};

export type ProjectUnit = {
  id: number;
  code: string;
  name: string;
  createdAt: string;
  updatedAt: string;
};

export type ProjectCurrency = {
  id: number;
  code: string;
  createdAt: string;
  updatedAt: string;
};

interface Props {
  hasBidUploadChanged: boolean;
  match: Match;
  supplierColumns: BidColumn[];
  columnTypes: ColumnType[];
  projectUnits: ProjectUnit[];
  projectCurrencies: ProjectCurrency[];
  getOfferProcessedAction: (
    projectId: string,
    roundId: string,
    columns: any,
    projectUnits: ProjectUnit[],
    projectCurrencies: ProjectCurrency[]
  ) => (dispatch: any) => void;
  runOfferProcessingJobAction: (
    projectId: string,
    roundId: string
  ) => (dispatch: any) => Promise<any>;
  updateOfferProcessedAction: Function;
  getRoundDetailsAction: Function;
}

const BidCorrections: React.FC<Props> = ({
  hasBidUploadChanged,
  match,
  supplierColumns,
  getOfferProcessedAction,
  projectCurrencies,
  projectUnits,
  updateOfferProcessedAction,
  runOfferProcessingJobAction,
  getRoundDetailsAction,
}) => {
  // @ts-ignore
  const { id: projectId, roundId } = useParams();
  const [shouldRefresh, setShouldRefresh] = useState<boolean>(false);

  const { offerDemandsData: grid, setGrid } = useBidCorrectionOfferGrid(
    projectId,
    roundId,
    shouldRefresh
  );

  const { t } = useCustomTranslation();

  const history = useHistory();

  useEffect(() => {
    getRoundDetailsAction(projectId, roundId);
  }, [projectId, roundId]);

  useBlockingJobEnd(() => setShouldRefresh(!shouldRefresh));

  const onStartOfferJob = async () => {
    try {
      await runOfferProcessingJobAction(projectId, roundId);
      getOfferProcessedAction(
        projectId,
        roundId,
        supplierColumns,
        projectUnits,
        projectCurrencies
      );
    } catch (err) {
      console.error(err);
    }
  };

  const handleNextStep = async () => {
    const gridToSubmit = accumulateGridColumns(grid, supplierColumns);

    await updateOfferProcessedAction(projectId, roundId, {
      rows: gridToSubmit,
    });
    history.push(
      `/projects/${projectId}/bid-importer/${roundId}/cost-calculation`
    );
  };

  const saveProgressAction = async () => {
    const gridToSubmit = accumulateGridColumns(grid, supplierColumns);
    await updateOfferProcessedAction(projectId, roundId, {
      rows: gridToSubmit,
    });
  };

  const textUnderTitle = !hasBidUploadChanged
    ? t(
        'The following bid sheet combines and matches all your previously uploaded offers. Adjust values if needed.'
      )
    : t('Validate offers to correct them if necessary.');

  const hasCurrentOffers = !hasBidUploadChanged;

  return (
    <ProjectsLayout
      // @ts-ignore
      match={match}
      sidebarLevel={2}
      saveProgressAction={saveProgressAction}
    >
      <StyledContainer>
        <RoundHeader
          title={t('Bid Corrections')}
          textUnderTitle={textUnderTitle}
          nextButtonLabel={t('Next Step')}
          // @ts-ignore
          nextButtonAction={handleNextStep}
        />
        <VerticalSpacer height={10} />
        {hasBidUploadChanged && (
          <Button color="secondary" size="large" onClick={onStartOfferJob}>
            Validate Offers
          </Button>
        )}
        <VerticalSpacer height={10} />
        {hasCurrentOffers ? (
          <BidCorrectionsGrid
            columns={supplierColumns}
            grid={grid}
            setGrid={setGrid}
          />
        ) : null}

        <JobModalProgressContainer
          title={t('Offer processing…')}
          iconHelperTextElement={
            <>
              <IconBox text={t('Processing Data')}>
                <AssessmentOutlined />
              </IconBox>
              <IconBox text={t('Validating Data')}>
                <Check />
              </IconBox>
              <IconBox text={t('Identifying Changes')}>
                <Search />
              </IconBox>
            </>
          }
        />
      </StyledContainer>
    </ProjectsLayout>
  );
};

const mapStateToProps = state => ({
  columnTypes: biddingRoundsSelectors.getColumnTypes(state),
  grid: biddingRoundsSelectors.getRoundTableContent(state),
  hasBidUploadChanged: biddingRoundsSelectors.getBidRoundChangedStatus(state),
  projectCurrencies: projectsSelectors.getCurrencies(state),
  projectUnits: projectsSelectors.getUnits(state),
  supplierColumns: biddingRoundsSelectors.getColumnsForSupplierWithItem(state),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getOfferProcessedAction: getOfferProcessed,
      runOfferProcessingJobAction: runOfferProcessingJob,
      updateOfferProcessedAction: updateOfferProcessed,
      getRoundDetailsAction: getRoundDetails,
    },
    dispatch
  );

export const BidCorrectionsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(BidCorrections);
