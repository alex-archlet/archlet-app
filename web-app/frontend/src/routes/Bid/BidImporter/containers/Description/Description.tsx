import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { useHistory } from 'react-router';
import { useParams } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import * as Yup from 'yup';

import { projectsSelectors } from '@selectors/projects';
import { projectDataSelectors } from '@selectors/projectData';
import { biddingRoundsSelectors } from '@selectors/biddingRounds';
import { useCustomTranslation } from '@utils/translate';

import { getProjectDetails } from '@actions/projects';
import {
  createRound,
  getRoundDetails,
  getRoundTemplates,
  updateRound,
} from '@actions/biddingRounds';

import { setPageTitle } from '@actions/interface';
import styled from 'styled-components';

import { OpenDirOptions } from 'fs';
import { useFormik } from 'formik';
import { ProjectsLayout } from '@routes/ProjectDetails/components/ProjectsLayout/ProjectsLayout';
import { RoundHeader } from '@routes/Bid/common/RoundHeader/RoundHeader';
import { DescriptionForm } from '@routes/Bid/common/DescriptionForm/DescriptionForm';

const StyledContainer = styled.div`
  padding: 0 65px;
`;
type RoundDetails = {
  createdAt: string;
  createdBy: string;
  description: string;
  endAt: string | null;
  id: string;
  lastTemplateFile: string | null;
  name: string;
  reminderAt: string | null;
  settings: object;
  startAt: string | null;
  statusName: string;
  updatedAt: string;
  updatedBy: string;
};

type ProjectDetails = {
  id: string;
  name: string;
  settings: object;
  status: OpenDirOptions;
  typeName: string;
  unitId: number;
  updated_at: string;
  biddingTypeId: number;
  categoryId: number;
  categoryName: string;
  created_at: string;
  currencyId: number;
};

interface Props {
  getProjectDetailsAction: (projectId: string) => Promise<ProjectDetails>;
  getRoundDetailsAction: (
    projectId: string,
    roundId: string
  ) => Promise<RoundDetails>;
  getRoundTemplatesAction: () => void;
  roundDetails: RoundDetails;
  roundTemplates: { id: string; name: string }[];
  setBackButtonAction: (icon: string) => void;
  setPageTitleAction: (title: string) => void;
  updateRoundAction: (
    projectId: string,
    roundId: string,
    values: object
  ) => Promise<RoundDetails>;
}

type FormValues = {
  name: string;
  description: string;
};

const Description: React.FC<Props> = ({
  getProjectDetailsAction,
  getRoundDetailsAction,
  getRoundTemplatesAction,
  roundDetails,
  setPageTitleAction,
  updateRoundAction,
}) => {
  const { t } = useCustomTranslation();

  const { id: projectId, roundId } = useParams<{
    id: string;
    roundId: string;
  }>();

  const ReviewSchema = Yup.object().shape({
    name: Yup.string().required(t('A Name is required')),
  });

  const { name, description } = roundDetails;
  const {
    values,
    errors,
    setFieldValue,
    setFieldTouched,
  } = useFormik<FormValues>({
    initialValues: {
      name,
      description,
    },
    onSubmit: console.info,
    validationSchema: ReviewSchema,
    validateOnBlur: true,
    validateOnChange: true,
    enableReinitialize: true,
  });

  const onChange = (field: string, value: string | number | null) => {
    setFieldTouched(field);
    setFieldValue(field, value);
  };

  const history = useHistory();

  const [nextPageRoundId, setNextPageRoundId] = useState<string | null>(null);
  const [isRedirectToOverview, setIsRedirectToOverview] = useState(false);

  useEffect(() => {
    getProjectDetailsAction(projectId).then((project) => {
      return setPageTitleAction(project.name);
    });
    getRoundTemplatesAction();

    getRoundDetailsAction(projectId, roundId);
  }, [projectId, roundId]);

  const onSubmit = (formValues) => {
    const createOrUpdateRoundAction: Promise<RoundDetails> = updateRoundAction(
      projectId,
      roundId,
      formValues
    );

    return createOrUpdateRoundAction.then((data) => {
      if (data) {
        setNextPageRoundId(data.id);
      }
    });
  };

  const nextButtonAction = () => {
    if (Object.keys(errors).length === 0) {
      onSubmit(values);
    }
  };

  const saveProgressAction = () => {
    onSubmit(values);
    setIsRedirectToOverview(true);
  };

  useEffect(() => {
    if (nextPageRoundId) {
      history.push(
        isRedirectToOverview
          ? `/projects/${projectId}/bid-importer/${nextPageRoundId}/description`
          : `/projects/${projectId}/bid-importer/${nextPageRoundId}/upload-match`
      );
    }
  }, [history, isRedirectToOverview, nextPageRoundId, projectId]);

  return (
    // @ts-ignore
    <ProjectsLayout sidebarLevel={2} saveProgressAction={saveProgressAction}>
      <StyledContainer>
        <RoundHeader
          title={t('Bidding Round Description')}
          textUnderTitle={t(
            'Define the bidding round to inform your colleagues'
          )}
          nextButtonLabel={t('Next Step')}
          nextButtonAction={nextButtonAction}
        />
        <DescriptionForm
          values={values}
          onChange={onChange}
          errors={errors}
          setFieldTouched={setFieldTouched}
        />
      </StyledContainer>
    </ProjectsLayout>
  );
};

const mapStateToProps = state => ({
  projectDetails: projectsSelectors.getProjectDetails(state),
  roundDetails: biddingRoundsSelectors.getRoundDetails(state),
  roundTemplates: biddingRoundsSelectors.getRoundTemplates(state),
  validation: projectDataSelectors.getValidation(state),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      createRoundAction: createRound,
      getProjectDetailsAction: getProjectDetails,
      getRoundDetailsAction: getRoundDetails,
      getRoundTemplatesAction: getRoundTemplates,
      setPageTitleAction: setPageTitle,
      updateRoundAction: updateRound,
    },
    dispatch
  );

export const DescriptionContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(
  // @ts-ignore
  Description
);
