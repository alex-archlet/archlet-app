import React from 'react';
import PropTypes from 'prop-types';
import {
  Route,
  Switch,
} from 'react-router-dom';
import { DescriptionContainer } from './containers/Description';
import { TableDesignContainer } from './containers/TableDesign';
import { TableContentContainer } from './containers/TableContent';
import { CostCalculationContainer } from './containers/CostCalculation';
import { InviteSuppliersContainer } from './containers/InviteSuppliers';
import { ScheduleContainer } from './containers/Schedule';

export const BidCollectorRouter = ({ match }) => (
  <Switch>
    <Route
      path={`${match.path}/description`}
      exact
      component={DescriptionContainer}
    />
    <Route
      path={`${match.path}/table-design`}
      exact
      component={TableDesignContainer}
    />
    <Route
      path={`${match.path}/table-content`}
      exact
      component={TableContentContainer}
    />
    <Route
      path={`${match.path}/cost-calculation`}
      exact
      component={CostCalculationContainer}
    />
    <Route
      path={`${match.path}/invite-suppliers`}
      exact
      component={InviteSuppliersContainer}
    />
    <Route
      path={`${match.path}/schedule`}
      exact
      component={ScheduleContainer}
    />
  </Switch>
);

BidCollectorRouter.propTypes = ({
  match: PropTypes.shape({}).isRequired,
});
