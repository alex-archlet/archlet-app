import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Field, Form, reduxForm } from 'redux-form';

import { validators } from '@utils/validators';

import { Close } from '@material-ui/icons';
import {
  Box,
  Dialog,
  FormHelperText,
  IconButton,
  MenuItem,
} from '@material-ui/core';
import { Button } from '@common/components/Button/Button';
import { useCustomTranslation } from '@utils/translate';

import { ReduxTextField } from '@common/components/ReduxForms/ReduxTextField';
import { ReduxRadioButton } from '@common/components/ReduxForms/ReduxRadioButton';
import { ReduxOptionChips } from '@common/components/ReduxForms/ReduxOptionChips';
import { COLUMN_MODAL_FORM_NAME } from '@utils/formNames';

const onlyDecimal = (value) => {
  const newValue = value.replace(/[^0-9.]/g, '');

  const sections = newValue.split('.');

  if (sections[0] !== '0' && sections[0] !== '00') {
    sections[0] = sections[0].replace(/^0+/, '');
  } else {
    sections[0] = '0';
  }

  if (sections[1]) {
    return `${sections[0]}.${sections[1].slice(0, 2)}`;
  }

  if (newValue.indexOf('.') !== -1) {
    return `${sections[0]}.`;
  }

  return sections[0];
};

const StyledReduxRadioButton = styled(ReduxRadioButton)`
  &&.MuiButton-contained {
    box-shadow: none;
  }

  &&.MuiButton-textPrimary {
    margin: 6px 7px 0 0;
    border: 1px solid #ffcb30;
    padding: 5px 15px;
    background-color: white;
    font-weight: 500;
    font-size: 9px;
    transition: none;
  }

  &&.MuiButton-containedPrimary {
    box-shadow: none;
    margin: 6px 7px 0 0;
    background-color: #ffcb30;
    color: black;
    font-weight: 500;
    font-size: 9px;
    transition: none;
  }
`;

const StyledSaveButton = styled(Button)`
  &&.MuiButton-root {
    width: 110px;
    margin-top: 42px;
  }
`;

const StyledIconButton = styled(IconButton)`
  &&.MuiIconButton-root {
    position: absolute;
    top: 3px;
    right: 3px;
    flex: 0;
  }
`;

const StyledHeading = styled.h2`
  font-size: 22px;
`;

const StyledLabel = styled.p`
  margin-bottom: 0;
  font-weight: 500;
  font-size: 11px;
  line-height: 15px;
`;

const StyledField = styled(Field)`
  && .MuiOutlinedInput-notchedOutline {
    border: 2px solid #efefef;
  }

  && .Mui-focused {
    .MuiOutlinedInput-notchedOutline {
      border: 2px solid #ffc100;
    }
  }

  && .MuiOutlinedInput-root {
    background-color: rgba(109, 114, 120, 0.05);

    &:hover {
      .MuiOutlinedInput-notchedOutline {
        border: 2px solid #ccc;
      }
    }

    &.Mui-disabled,
    &.Mui-disabled:hover {
      .MuiOutlinedInput-notchedOutline {
        border: 2px solid #f9f9f9;
      }
    }
  }

  && .MuiInputBase-inputMarginDense {
    font-size: 11px;
    line-height: 15px;
  }

  && .MuiInputLabel-marginDense {
    color: rgba(0, 0, 0, 0.34);
    font-size: 11px;
    line-height: 15px;
  }

  &&.MuiFormControl-marginNormal {
    width: 94px;
    margin-right: 7px;
  }

  &&.MuiFormControl-marginDense {
    && .MuiInputLabel-shrink {
      display: none;
    }

    legend {
      width: 0;
    }
  }
`;

const StyledFormHelperText = styled(FormHelperText)`
  &&.MuiFormHelperText-root {
    font-size: 9px;
  }
`;

const ColumnModalFormComponent = ({
  isOpen,
  columnTypes,
  formValues,
  handleClose,
  handleSubmit,
  branchName,
  columns,
}) => {
  const { id: editedColumnId, isDefault, typeId, inputBy } = formValues;
  const { t } = useCustomTranslation();

  const uniqueColumnTypesTaken = columnTypes.filter((type) => {
    const matchingColumn = columns.find(
      column => column.id !== editedColumnId && column.typeId === type.id
    );

    return matchingColumn && type.isUnique;
  });

  const columnTypesAvailable = isDefault
    ? columnTypes
    : columnTypes.filter(type => !uniqueColumnTypesTaken.includes(type));

  const matchingType = columnTypesAvailable.find(type => type.id === typeId);

  const typeName = !!matchingType && matchingType.type;

  return (
    <Dialog
      open={isOpen}
      onClose={handleClose}
      aria-labelledby="form-dialog-title"
      maxWidth="md"
      disableEscapeKeyDown
    >
      <StyledIconButton onClick={handleClose}>
        <Close />
      </StyledIconButton>
      <Form onSubmit={handleSubmit}>
        <Box width="662px" padding="60px 178px">
          <StyledHeading>{t('Specify your column')}</StyledHeading>
          <StyledLabel>{t('Column Name')}</StyledLabel>
          <StyledField
            name="name"
            type="text"
            component={ReduxTextField}
            placeholder={t('e.g. Unit Price')}
            margin="dense"
            variant="outlined"
            fullWidth
            validate={validators.require}
          />
          <StyledLabel>{t('Column Type')}</StyledLabel>
          <StyledField
            name="typeId"
            disabled={isDefault}
            type="text"
            component={ReduxTextField}
            label={t('Choose ...')}
            margin="dense"
            variant="outlined"
            fullWidth
            select
            validate={validators.require}
          >
            {columnTypesAvailable.map(type => (
              <MenuItem key={type.id} value={type.id}>
                {type.text}
              </MenuItem>
            ))}
          </StyledField>
          <StyledLabel>{t('Input By')}</StyledLabel>
          <StyledField
            name="inputBy"
            disabled={isDefault}
            type="text"
            component={ReduxTextField}
            label={t('Choose ...')}
            margin="dense"
            variant="outlined"
            fullWidth
            select
            validate={validators.require}
          >
            <MenuItem value="supplier">{t('Supplier')}</MenuItem>
            <MenuItem value="buyer">{branchName}</MenuItem>
          </StyledField>
          <StyledLabel>{t('Input Requirement')}</StyledLabel>
          <Field
            disabled={isDefault}
            name="isMandatory"
            component={StyledReduxRadioButton}
            label={t('Mandatory')}
            option
          />
          <Field
            disabled={isDefault}
            name="isMandatory"
            component={StyledReduxRadioButton}
            label={t('Optional')}
            option={false}
          />
          <StyledFormHelperText>
            {t(
              'If set to Mandatory, the user is notified when an input value is missing.'
            )}
          </StyledFormHelperText>
          {inputBy === 'buyer' && (
            <>
              <StyledLabel>{t('Visible to Supplier')}</StyledLabel>
              <Field
                name="isVisible"
                disabled={isDefault}
                component={StyledReduxRadioButton}
                label={t('Yes')}
                option
              />
              <Field
                name="isVisible"
                disabled={isDefault}
                component={StyledReduxRadioButton}
                label={t('No')}
                option={false}
              />
              <StyledFormHelperText>
                {t('Define if the column can be seen by the supplier.')}
              </StyledFormHelperText>
            </>
          )}
          {typeName === 'number' && (
            <>
              <StyledLabel>{t('Limit Values')}</StyledLabel>
              <StyledField
                name="limitMin"
                disabled={isDefault}
                type="text"
                label={t('Min')}
                component={ReduxTextField}
                variant="outlined"
                normalize={onlyDecimal}
              />
              <StyledField
                name="limitMax"
                disabled={isDefault}
                type="text"
                label={t('Max')}
                component={ReduxTextField}
                variant="outlined"
                normalize={onlyDecimal}
                validate={validators.maxGreaterThanMin}
              />
            </>
          )}
          {typeName === 'single_choice' && (
            <div>
              <StyledLabel>{t('Add Single Choice Elements')}</StyledLabel>
              <Field
                name="singleChoice"
                disabled={isDefault}
                component={ReduxOptionChips}
              />
              <StyledFormHelperText>
                {t(
                  'These are the single choice elements the Supplier can choose from.'
                )}
              </StyledFormHelperText>
            </div>
          )}
          <StyledSaveButton type="submit" color="secondary">
            {t('Save')}
          </StyledSaveButton>
        </Box>
      </Form>
    </Dialog>
  );
};

ColumnModalFormComponent.propTypes = {
  branchName: PropTypes.string,
  columnTypes: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  columns: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  formValues: PropTypes.shape({}),
  handleClose: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
};

ColumnModalFormComponent.defaultProps = {
  branchName: 'Buyer',
  formValues: {},
};

export const ColumnModalForm = reduxForm({
  enableReinitialize: true,
  form: COLUMN_MODAL_FORM_NAME,
  keepDirtyOnReinitialize: true,
})(ColumnModalFormComponent);
