import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import ReactDataSheet from 'react-datasheet';
import 'react-datasheet/lib/react-datasheet.css';

import { Scrollbars } from 'react-custom-scrollbars';

const StyledDataSheetContainer = styled.div`
  height: 52vh;
  margin-bottom: -100px;
`;

const StyledReactDataSheet = styled(ReactDataSheet)`
  && {
    width: auto;

    .value-viewer {
      height: 36px;
      padding: 0 21px;
      font-size: 16px;
      line-height: 36px;
    }

    td.custom-cell {
      overflow: hidden;
      width: 172px;
      min-width: 172px;
      max-width: 172px;
      height: 42px;
      border: 1px solid #cacaca;
      padding: 0;
      text-align: left;
      user-select: none;
      cursor: cell;
      vertical-align: middle;

      &.selected {
        box-shadow: none;
        border: 3px solid #ffcb30;
        vertical-align: middle;

        .value-viewer {
          width: 166px;
          min-width: 166px;
          max-width: 166px;
          height: 36px;
          padding: 0 21px;
        }
      }

      &--header.read-only {
        background-color: white;
        color: black;
        font-weight: 500;
        text-align: left;
        text-overflow: ellipsis;
        white-space: nowrap;
      }

      &--error {
        border: 2px solid red;
      }

      &--error.selected {
        box-shadow: inset 0 0 0 2px red;
      }

      &.cell > input.data-editor {
        width: 100%;
        height: 100%;
        outline: none;
        border: none;
        padding: 0 21px;
        font-size: 16px;
        line-height: 21px;
        text-align: left;
      }
    }
  }
`;

export const Spreadsheet = ({
  grid,
  setGrid,
  selectedCells,
  setSelectedCells,
  columns,
  validateGrid,
  generateEmptyRows,
}) => {
  const fillRangeWithClipboardData = (clipboardData, rowsToFill, columnsToFill) => {
    const rows = [];

    for (let i = 0, j = 0; i < rowsToFill; i += 1) {
      const column = [];

      for (let k = 0, l = 0; k < columnsToFill; k += 1) {
        column.push(clipboardData[j][l]);
        l = clipboardData[j][l + 1] ? l + 1 : 0;
      }
      rows.push([...column]);
      j = clipboardData[j + 1] ? j + 1 : 0;
    }

    return rows;
  };

  const onSelect = (selected) => {
    if (typeof selected.start.i === 'undefined') {
      setSelectedCells({
        end: selected.end,
        start: selectedCells.start,
      });
    }

    if (selected.start.i && selected.start.i > 0) {
      setSelectedCells(selected);
    }
  };

  const parsePaste = (clipboardData) => {
    const { start, end } = selectedCells;
    const selectedStartRow = start.i;
    const selectedRows = Math.abs(end.i - start.i) + 1;
    const selectedColumns = Math.abs(end.j - start.j) + 1;

    const parsedClipboardData = clipboardData
      .split(/\r\n|\n|\r/)
      .map(row => row.split('\t'));

    const clipboardRows = parsedClipboardData.length;
    const clipboardColumns = parsedClipboardData[0].length;

    const isSelectedInsideClipboardRange =
      clipboardRows >= selectedRows &&
      clipboardColumns >= selectedColumns;

    const gridLastRow = grid.length - 1;
    const clipboardLastRow = selectedStartRow + parsedClipboardData.length - 1;
    const clipboardGridRowCountDifference = clipboardLastRow - gridLastRow;

    if (clipboardGridRowCountDifference > 0) {
      setGrid([
        ...grid,
        ...generateEmptyRows(clipboardGridRowCountDifference, gridLastRow),
      ]);
    }

    return isSelectedInsideClipboardRange ?
      parsedClipboardData :
      fillRangeWithClipboardData(parsedClipboardData, selectedRows, selectedColumns);
  };

  const onCellsChanged = (changes) => {
    const newGrid = grid.map(row => [...row]);

    changes.forEach(({ row, col, value }) => {
      newGrid[row][col] = {
        ...grid[row][col],
        value,
      };
    });

    const validatedGrid = validateGrid(newGrid, columns);

    setGrid(validatedGrid);
  };

  return (
    <StyledDataSheetContainer>
      <Scrollbars autoHide>
        <StyledReactDataSheet
          data={grid}
          valueRenderer={cell => cell.value}
          onSelect={onSelect}
          selected={selectedCells}
          parsePaste={parsePaste}
          onCellsChanged={onCellsChanged}
        />
      </Scrollbars>
    </StyledDataSheetContainer>
  );
};

Spreadsheet.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  generateEmptyRows: PropTypes.func.isRequired,
  grid: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.shape({}))).isRequired,
  selectedCells: PropTypes.shape({}).isRequired,
  setGrid: PropTypes.func.isRequired,
  setSelectedCells: PropTypes.func.isRequired,
  validateGrid: PropTypes.func.isRequired,
};
