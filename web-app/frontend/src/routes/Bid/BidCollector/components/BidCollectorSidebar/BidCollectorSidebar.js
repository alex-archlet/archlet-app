import React from 'react';
import {
  Box,
} from '@material-ui/core';
import PropTypes from 'prop-types';

import {
  AttachMoney,
  BarChart,
  BorderAll,
  BorderColor,
  CalendarTodayOutlined,
  Delete,
  Lock,
  Menu,
  PersonAddOutlined,
  Subject,
} from '@material-ui/icons';

import { SidebarSection } from '@common/components/Sidebar/SidebarSection';
import { SidebarItem } from '@common/components/Sidebar/SidebarItem';
import { SidebarBackButton } from '@common/components/Sidebar/SidebarBackButton';
import { SidebarActionButton } from '@common/components/Sidebar/SidebarActionButton';

export const BidCollectorSidebar = ({
  id,
  higherLevelPath,
  saveProgressAction,
  deleteRound,
}) => (
  <Box>
    <SidebarBackButton
      toPath={higherLevelPath}
      toName="Bid Collector"
    />
    <Box>
      <SidebarSection title="Settings">
        <SidebarItem
          icon={<Subject />}
          isSecondary
          name="Description"
          to={`${higherLevelPath}/${id}/description/`}
        />
        <SidebarItem
          icon={<BorderColor />}
          isSecondary
          name="Table Design"
          to={`${higherLevelPath}/${id}/table-design/`}
        />
        <SidebarItem
          icon={<BorderAll />}
          isSecondary
          name="Table Content"
          to={`${higherLevelPath}/${id}/table-content`}
        />
        <SidebarItem
          icon={<AttachMoney />}
          isSecondary
          name="Cost Calculation"
          to={`${higherLevelPath}/${id}/cost-calculation`}
        />
        <SidebarItem
          icon={<PersonAddOutlined />}
          isSecondary
          name="Invite Suppliers"
          to={`${higherLevelPath}/${id}/invite-suppliers`}
        />
        <SidebarItem
          icon={<CalendarTodayOutlined />}
          isSecondary
          name="Schedule"
          to={`${higherLevelPath}/${id}/schedule`}
        />
      </SidebarSection>
      <SidebarSection title="Overview">
        <SidebarItem
          icon={<Menu />}
          isSecondary
          name="Round Summary"
          to={`${higherLevelPath}/${id}/round-summary`}
        />
        <SidebarItem
          icon={<BarChart />}
          isSecondary
          name="Supplier Bids"
          to={`${higherLevelPath}/${id}/supplier-bids`}
        />
      </SidebarSection>
    </Box>
    <Box mt={103}>
      <SidebarActionButton
        icon={<Lock />}
        name="Save progress"
        onClick={saveProgressAction}
      />
      <SidebarActionButton
        icon={<Delete />}
        name="Delete round"
        onClick={deleteRound}
      />
    </Box>
  </Box>
);

BidCollectorSidebar.propTypes = {
  deleteRound: PropTypes.func.isRequired,
  higherLevelPath: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  saveProgressAction: PropTypes.func.isRequired,
};
