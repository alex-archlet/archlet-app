import React from 'react';
import PropTypes from 'prop-types';
import MomentUtils from '@date-io/moment';
import styled from 'styled-components';
import { Field, Form, reduxForm } from 'redux-form';

import { Box, MenuItem } from '@material-ui/core';
import { Button } from '@common/components/Button/Button';
import { Add } from '@material-ui/icons';

import { validators } from '@utils/validators';
import { timezones } from '@utils/timezones';
import { useCustomTranslation } from '@utils/translate';
import { ROUND_SCHEDULE_FORM_NAME } from '@utils/formNames';

import { MuiPickersUtilsProvider } from '@material-ui/pickers';

import { ReduxDatePicker } from '@common/components/ReduxForms/ReduxDatePicker';
import { ReduxTimePicker } from '@common/components/ReduxForms/ReduxTimePicker';
import { ReduxTextField } from '@common/components/ReduxForms/ReduxTextField';
import { ReduxRadioButton } from '@common/components/ReduxForms/ReduxRadioButton';

const StyledField = styled(Field)`
  && {
    margin: 0 14px 0 0;

    .MuiInputBase-input {
      padding: 10px 0;
      font-weight: 500;
    }

    .MuiInput-underline {
      &::before {
        border-bottom: 2px solid #ffd454;
      }
    }

    .MuiSvgIcon-root {
      height: 19px;
    }
  }
`;

const StyledDateField = styled(StyledField)`
  && {
    width: 205px;

    .MuiInputBase-input {
      padding: 10px 0 10px 5px;
    }
  }
`;

const StyledTimeField = styled(StyledField)`
  && {
    width: 50px;

    .MuiInputBase-input {
      padding: 10px 0;
    }
  }
`;

const StyledTimezoneField = styled(StyledField)`
  && {
    width: 160px;

    label + .MuiInput-formControl {
      margin-top: 0;
    }

    .MuiInputLabel-formControl {
      top: -12px;
      font-weight: 500;
    }

    .MuiFormLabel-root {
      color: rgba(0, 0, 0, 0.87);
    }

    .MuiInputBase-input {
      overflow: hidden;
      padding: 10px 20px 10px 0;
      text-overflow: ellipsis;
      white-space: nowrap;
    }

    .MuiInputLabel-shrink {
      display: none;
    }

    legend {
      width: 0;
    }
  }
`;

const StyledHeading = styled.h3`
  margin: 0 0 14px 0;
  font-weight: 500;
  font-size: 22px;
  line-height: 29px;
`;

const StyledParagraph = styled.p`
  width: 369px;
  color: #2c2c2c;
  font-size: 14px;
  line-height: 19px;
`;

const StyledReduxRadioButton = styled(ReduxRadioButton)`
  &&.MuiButton-contained {
    box-shadow: none;
  }

  &&.MuiButton-textPrimary {
    margin: 6px 7px 0 0;
    border: 2px solid #ffd454;
    border-radius: 8px;
    padding: 5px 15px;
    background-color: white;
    font-weight: 500;
    font-size: 16px;
    line-height: 21px;
    transition: none;
  }

  &&.MuiButton-containedPrimary {
    box-shadow: none;
    margin: 6px 7px 0 0;
    border-radius: 8px;
    background-color: #ffcb30;
    color: black;
    font-weight: 500;
    font-size: 16px;
    line-height: 21px;
    transition: none;
  }
`;

const StyledButton = styled(Button)`
  &&.MuiButton-textSecondary {
    margin-top: 54px;
    padding: 12px 25px;

    .MuiButton-startIcon {
      margin-right: 20px;
      margin-left: -14px;
    }
  }
`;

const ScheduleFormComponent = ({ handleSubmit, formValues }) => {
  const { isReminder } = formValues;
  const { t } = useCustomTranslation();

  return (
    <MuiPickersUtilsProvider utils={MomentUtils}>
      <Box padding="100px 213px">
        <Form onSubmit={handleSubmit}>
          <Box mb={15}>
            <StyledHeading>{t('Bidding Start')}</StyledHeading>
            <StyledDateField
              name="startDate"
              component={ReduxDatePicker}
              validate={validators.require}
            />
            <StyledTimeField
              name="startDate"
              component={ReduxTimePicker}
              validate={validators.require}
            />
            <StyledTimezoneField
              name="startTimezone"
              label="Time Zone"
              component={ReduxTextField}
              validate={validators.require}
              select
            >
              {timezones.map(timezone => (
                <MenuItem key={timezone.value} value={timezone}>
                  {timezone.value}
                </MenuItem>
              ))}
            </StyledTimezoneField>
          </Box>
          <Box mb={50}>
            <StyledHeading>{t('Bidding End')}</StyledHeading>
            <StyledDateField
              name="endDate"
              component={ReduxDatePicker}
              validate={[validators.require, validators.laterThanStart]}
            />
            <StyledTimeField
              name="endDate"
              component={ReduxTimePicker}
              validate={[validators.require, validators.laterThanStart]}
            />
            <StyledTimezoneField
              name="endTimezone"
              label="Time Zone"
              component={ReduxTextField}
              validate={validators.require}
              select
            >
              {timezones.map(timezone => (
                <MenuItem key={timezone.value} value={timezone}>
                  {timezone.value}
                </MenuItem>
              ))}
            </StyledTimezoneField>
          </Box>
          <Box mb={50}>
            <StyledHeading>{t('Set Reminder')}</StyledHeading>
            <StyledParagraph>
              {t(
                'Set a custom automatic reminder for suppliers before the end of the bidding round.'
              )}
            </StyledParagraph>
            <Box mb={40}>
              <Field
                name="isReminder"
                component={StyledReduxRadioButton}
                label={t('Yes')}
                option
              />
              <Field
                name="isReminder"
                component={StyledReduxRadioButton}
                label={t('No')}
                option={false}
              />
            </Box>
            {!!isReminder && (
              <Box>
                <StyledDateField
                  name="reminderDate"
                  component={ReduxDatePicker}
                  validate={validators.require}
                />
                <StyledTimeField
                  name="reminderDate"
                  component={ReduxTimePicker}
                  validate={validators.require}
                />
                <StyledTimezoneField
                  name="reminderTimezone"
                  label="Time Zone"
                  component={ReduxTextField}
                  validate={validators.require}
                  select
                >
                  {timezones.map(timezone => (
                    <MenuItem key={timezone.value} value={timezone}>
                      {timezone.value}
                    </MenuItem>
                  ))}
                </StyledTimezoneField>
              </Box>
            )}
            <StyledButton type="submit" color="secondary" startIcon={<Add />}>
              {t('Create Round')}
            </StyledButton>
          </Box>
        </Form>
      </Box>
    </MuiPickersUtilsProvider>
  );
};

ScheduleFormComponent.propTypes = {
  formValues: PropTypes.shape({}),
  handleSubmit: PropTypes.func.isRequired,
};

ScheduleFormComponent.defaultProps = {
  formValues: {},
};

export const ScheduleForm = reduxForm({
  destroyOnUnmount: false,
  enableReinitialize: true,
  form: ROUND_SCHEDULE_FORM_NAME,
  keepDirtyOnReinitialize: true,
})(ScheduleFormComponent);
