import React from 'react';
import PropTypes from 'prop-types';
import { IconButton, Table, TableCell, TableRow } from '@material-ui/core';
import styled from 'styled-components';
import { Close } from '@material-ui/icons';
import { colors } from '@utils/uiTheme';

const StyledTable = styled(Table)`
  && {
    padding-top: 50px;
    border-collapse: separate;
    border-spacing: 0 5px;
  }
`;

const StyledTableCell = styled(TableCell)`
  && {
    border: solid 1px #979797;
    border-left: 0;
    border-right: 0;
    padding: 10px 18px;
    width: 22.5%;
    
    &:first-child {
      border-top-left-radius: 3px;
      border-bottom-left-radius: 3px;
      border-left: solid 1px #979797;
      padding-left: 36px;
    }
    
    &:last-child {
      border-top-right-radius: 3px;
      border-bottom-right-radius: 3px;
      border-right: solid 1px #979797;
      padding-right: 24px;
      padding-top: 2px;
      padding-bottom: 2px;
      width: 10%;
      text-align: right;
    }
  }
`;

const StyledClose = styled(Close)`
  && {
    fill: ${colors.iconGrey};
  }
`;

const StyledIconButton = styled(IconButton)`
  && {
    padding: 8px;
  }
`;

export const SuppliersTable = ({
  suppliers,
  onDelete,
}) => (
  <>
    {suppliers && !!suppliers.length && (
      <StyledTable>
        {suppliers.map(supplier => (
          <TableRow>
            <StyledTableCell>{supplier.supplierName}</StyledTableCell>
            <StyledTableCell>{supplier.firstName}</StyledTableCell>
            <StyledTableCell>{supplier.lastName}</StyledTableCell>
            <StyledTableCell>{supplier.username}</StyledTableCell>
            <StyledTableCell>
              <StyledIconButton
                aria-label="delete"
                onClick={() => onDelete(supplier)}
              >
                <StyledClose />
              </StyledIconButton>
            </StyledTableCell>
          </TableRow>
        ))}
      </StyledTable>
    )}
  </>
);

SuppliersTable.defaultProps = ({
  onDelete: () => {},
});

SuppliersTable.propTypes = ({
  onDelete: PropTypes.func,
  suppliers: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
});

