import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { colors } from '@utils/uiTheme';
import { Add } from '@material-ui/icons';

import { Field, Form, reduxForm } from 'redux-form';
import { Box } from '@material-ui/core';
import { Button } from '@common/components/Button/Button';
import { Text } from '@common/components/Text/Text';
import { ReduxTextField } from '@common/components/ReduxForms/ReduxTextField';
import { ROUND_INVITE_SUPPLIERS_FORM } from '@utils/formNames';
import { useCustomTranslation } from '@utils/translate';
import { validators } from '@utils/validators';

const StyledFormContainer = styled(Box)`
  display: flex;
  flex-direction: row;
  position: relative;
  align-items: center;
`;

const StyledBottomLine = styled(Box)`
  position: absolute;
  width: 100%;
  bottom: 0;
  height: 2px;
  background-color: #ffc100;
  top: 49px;
`;

const StyledField = styled(Field)`
  &&.MuiFormControl-root {
    width: 22.5%;
  }

  && .MuiInput-root {
    &:hover {
      .MuiInput-notchedOutline {
        border: 2px solid #ccc;
      }
    }
  }

  && .MuiInput-input {
    height: 19px;
    padding: 8px 10px;
    font-size: 11px;
    line-height: 19px;
  }

  && .MuiInputBase-inputMarginDense {
    font-size: 11px;
    line-height: 19px;
  }

  && .MuiInputLabel-marginDense {
    color: rgba(0, 0, 0, 0.34);
    font-size: 11px;
    line-height: 19px;
    transform: translate(10px, 8px) scale(1);
  }

  .MuiFormHelperText-root.Mui-error {
    position: absolute;
    bottom: -20px;
    left: 10px;
  }
`;

const StyledButton = styled(Button)`
  && {
    height: 30px;
    width: 90px;
    min-width: 90px;
    padding: 0;
  }
`;

const StyledAddIcon = styled(Add)`
  && {
    margin-right: 14px;
  }
`;

const InviteSuppliersFormComponent = ({ error, handleSubmit }) => {
  const { t } = useCustomTranslation();

  return (
    <Box paddingTop="64px">
      <Form onSubmit={handleSubmit}>
        <StyledFormContainer>
          <StyledField
            name="supplierName"
            type="text"
            component={ReduxTextField}
            placeholder={t('Supplier Name')}
            validate={validators.require}
          />
          <StyledField
            name="firstName"
            type="text"
            component={ReduxTextField}
            placeholder={t('Contact First Name')}
            validate={validators.require}
          />
          <StyledField
            name="lastName"
            type="text"
            component={ReduxTextField}
            placeholder={t('Contact Last Name')}
            validate={validators.require}
          />
          <StyledField
            name="email"
            type="text"
            component={ReduxTextField}
            placeholder={t('Email Address')}
            validate={[validators.require, validators.email]}
          />
          <Box>
            <StyledButton color="secondary" onClick={handleSubmit}>
              <StyledAddIcon />
              {t('Add')}
            </StyledButton>
          </Box>
          <StyledBottomLine />
        </StyledFormContainer>
        <Text color={colors.red}>{error}</Text>
      </Form>
    </Box>
  );
};

InviteSuppliersFormComponent.propTypes = {
  error: PropTypes.string,
  handleSubmit: PropTypes.func.isRequired,
};

InviteSuppliersFormComponent.defaultProps = {
  error: '',
};

export const InviteSuppliersForm = reduxForm({
  enableReinitialize: true,
  form: ROUND_INVITE_SUPPLIERS_FORM,
  keepDirtyOnReinitialize: true,
})(InviteSuppliersFormComponent);
