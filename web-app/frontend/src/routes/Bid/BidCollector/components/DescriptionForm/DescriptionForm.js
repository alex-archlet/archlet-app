import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { colors } from '@utils/uiTheme';
import { ROUND_DESCRIPTION_FORM_NAME } from '@utils/formNames';

import {
  Field,
  Form,
  reduxForm,
} from 'redux-form';
import { Box, MenuItem } from '@material-ui/core';
import { useCustomTranslation } from '@utils/translate';
import { Text } from '@common/components/Text/Text';
import { validators } from '@utils/validators';

import { ReduxTextField } from '@common/components/ReduxForms/ReduxTextField';

const StyledLabel = styled.h3`
  margin: 24px 0 0;
  font-weight: 500;
  font-size: 14px;
  line-height: 19px;
`;

const StyledField = styled(Field)`
  &&.MuiFormControl-root {
    width: 280px;
  }

  &&.MuiFormControl-fullWidth {
    width: 100%;
    max-width: 629px;
  }

  && .MuiOutlinedInput-notchedOutline {
    border: 2px solid #efefef;
  }

  && .Mui-focused {
    .MuiOutlinedInput-notchedOutline {
      border: 2px solid #ffc100;
    }
  }

  && .MuiOutlinedInput-root {
    background-color: rgba(109, 114, 120, 0.05);

    &:hover {
      .MuiOutlinedInput-notchedOutline {
        border: 2px solid #ccc;
      }
    }
  }

  && .MuiOutlinedInput-input {
    height: 19px;
    padding: 8px 10px;
    font-size: 11px;
    line-height: 19px;
  }

  && .MuiInputBase-inputMarginDense {
    font-size: 11px;
    line-height: 19px;
  }

  && .MuiInputLabel-marginDense {
    color: rgba(0, 0, 0, 0.34);
    font-size: 11px;
    line-height: 19px;
    transform: translate(10px, 8px) scale(1);
  }

  &&.MuiFormControl-marginDense {
    && .MuiInputLabel-shrink {
      display: none;
    }

    legend {
      width: 0;
    }
  }

  .MuiInputBase-multiline {
    align-items: flex-start;
    min-height: 133px;
    padding: 0;
  }

  .MuiFormHelperText-contained {
    margin: 7px 0 0;
    font-size: 10px;
    line-height: 13px;
  }
`;

const DescriptionFormComponent = ({
  error,
  handleSubmit,
  roundTemplates,
}) => {
  const { t } = useCustomTranslation();

  return (
    <Box
      padding="64px 110px 0 142px"
    >
      <Form onSubmit={handleSubmit}>
        <StyledLabel>
          {t('Bidding Round Name')}
        </StyledLabel>
        <StyledField
          name="name"
          type="text"
          component={ReduxTextField}
          fullWidth
          placeholder={t('e.g. LogisticsTender_2021_Round_01')}
          variant="outlined"
          validate={validators.require}
        />
        <StyledLabel>
          {t('Description (1000 characters max)')}
        </StyledLabel>
        <StyledField
          name="description"
          type="text"
          placeholder={t('Dear Suppliers, we are happy to invite you to this first bidding round of our logistics tender…')}
          component={ReduxTextField}
          fullWidth
          multiline
          variant="outlined"
          helperText={t('Description will be visible for all invited Suppliers.')}
          normalize={value => value.slice(0, 1000)}
          validate={validators.require}
        />
        <StyledLabel>
          {t('Choose Template')}
        </StyledLabel>
        <StyledField
          name="template"
          type="text"
          component={ReduxTextField}
          label={t('Choose ...')}
          margin="dense"
          variant="outlined"
          select
        >
          {roundTemplates.map(({ id, name }) => (
            <MenuItem
              key={id}
              value={id}
            >
              {name}
            </MenuItem>
          ))}
        </StyledField>
        <Text color={colors.red}>
          { error }
        </Text>
      </Form>
    </Box>
  );
};

DescriptionFormComponent.propTypes = {
  error: PropTypes.string,
  handleSubmit: PropTypes.func.isRequired,
  roundTemplates: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
};

DescriptionFormComponent.defaultProps = {
  error: '',
};

export const DescriptionForm = reduxForm({
  enableReinitialize: true,
  form: ROUND_DESCRIPTION_FORM_NAME,
  keepDirtyOnReinitialize: true,
})(DescriptionFormComponent);
