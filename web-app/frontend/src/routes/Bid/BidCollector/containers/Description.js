import React, {
  useEffect,
  useState,
} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { useHistory } from 'react-router';
import { useParams } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { initialize, submit } from 'redux-form';

import { projectsSelectors } from '@selectors/projects';
import { projectDataSelectors } from '@selectors/projectData';
import { biddingRoundsSelectors } from '@selectors/biddingRounds';
import { useCustomTranslation } from '@utils/translate';
import {
  getProjectDetails,
} from '@actions/projects';
import {
  createRound,
  deleteRound,
  getRoundDetails,
  getRoundTemplates,
  updateRound,
} from '@actions/biddingRounds';

import {
  setBackButton,
  setPageTitle,
} from '@actions/interface';

import { ROUND_DESCRIPTION_FORM_NAME } from '@utils/formNames';

import { ProjectsLayout } from '../../../ProjectDetails/components/ProjectsLayout/ProjectsLayout';
import { RoundHeader } from '../../common/RoundHeader/RoundHeader';
import { DescriptionForm } from '../components/DescriptionForm/DescriptionForm';

const StyledContainer = styled.div`
  padding: 0 65px;
`;

const Description = ({
  createRoundAction,
  getProjectDetailsAction,
  getRoundDetailsAction,
  getRoundTemplatesAction,
  roundDetails,
  roundTemplates,
  setBackButtonAction,
  setPageTitleAction,
  submitForm,
  updateRoundAction,
  initializeAction,
  deleteRoundAction,
}) => {
  const {
    id: projectId,
    roundId,
  } = useParams();

  const {
    description,
    name,
    template,
  } = roundDetails;

  const history = useHistory();

  const [nextPageRoundId, setNextPageRoundId] = useState(null);
  const [isRedirectToOverview, setIsRedirectToOverview] = useState(false);
  const { t } = useCustomTranslation();

  useEffect(() => {
    setBackButtonAction('arrow');
    getProjectDetailsAction(projectId).then(project => setPageTitleAction(project.name));
    getRoundTemplatesAction();

    if (roundId !== 'new') {
      getRoundDetailsAction(projectId, roundId);
    } else {
      initializeAction(ROUND_DESCRIPTION_FORM_NAME, {});
    }

    return () => setBackButtonAction('');
  }, [
    projectId,
    roundId,
  ]);

  const onSubmit = (values) => {
    const isUpdating = roundId !== 'new' && !!roundDetails.id;

    const createOrUpdateRoundAction = isUpdating ?
      updateRoundAction(projectId, roundId, values) :
      createRoundAction(projectId, values);

    return createOrUpdateRoundAction
      .then((data) => {
        if (data) {
          setNextPageRoundId(data.id);
        }
      });
  };

  const nextButtonAction = () => {
    submitForm(ROUND_DESCRIPTION_FORM_NAME);
  };

  const saveProgressAction = () => {
    submitForm(ROUND_DESCRIPTION_FORM_NAME);
    setIsRedirectToOverview(true);
  };

  useEffect(() => {
    if (nextPageRoundId) {
      history.push(isRedirectToOverview ?
        `/projects/${projectId}/bid-collector` :
        `/projects/${projectId}/bid-collector/${nextPageRoundId}/table-design`);
    }
  }, [history, isRedirectToOverview, nextPageRoundId, projectId]);

  const roundTemplatesToSelect = roundTemplates
    .filter(roundTemplate => roundTemplate.id !== roundId);

  return (
    <ProjectsLayout
      sidebarLevel={2}
      saveProgressAction={saveProgressAction}
      deleteRoundAction={deleteRoundAction}
    >
      <StyledContainer>
        <RoundHeader
          title={t('Bidding Round Description')}
          textUnderTitle={t('Define the bidding round to inform your suppliers.')}
          nextButtonLabel={t('Table Design')}
          nextButtonAction={nextButtonAction}
        />
        <DescriptionForm
          onSubmit={onSubmit}
          roundTemplates={roundTemplatesToSelect}
          initialValues={{
            description,
            name,
            template,
          }}
        />
      </StyledContainer>
    </ProjectsLayout>
  );
};

Description.propTypes = ({
  createRoundAction: PropTypes.func.isRequired,
  deleteRoundAction: PropTypes.func.isRequired,
  getProjectDetailsAction: PropTypes.func.isRequired,
  getRoundDetailsAction: PropTypes.func.isRequired,
  getRoundTemplatesAction: PropTypes.func.isRequired,
  initializeAction: PropTypes.func.isRequired,
  roundDetails: PropTypes.shape({}).isRequired,
  roundTemplates: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  setBackButtonAction: PropTypes.func.isRequired,
  setPageTitleAction: PropTypes.func.isRequired,
  submitForm: PropTypes.func.isRequired,
  updateRoundAction: PropTypes.func.isRequired,
  validation: PropTypes.shape({}).isRequired,
});

const mapStateToProps = state => ({
  projectDetails: projectsSelectors.getProjectDetails(state),
  roundDetails: biddingRoundsSelectors.getRoundDetails(state),
  roundTemplates: biddingRoundsSelectors.getRoundTemplates(state),
  validation: projectDataSelectors.getValidation(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  createRoundAction: createRound,
  deleteRoundAction: deleteRound,
  getProjectDetailsAction: getProjectDetails,
  getRoundDetailsAction: getRoundDetails,
  getRoundTemplatesAction: getRoundTemplates,
  initializeAction: initialize,
  setBackButtonAction: setBackButton,
  setPageTitleAction: setPageTitle,
  submitForm: submit,
  updateRoundAction: updateRound,
}, dispatch);

export const DescriptionContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Description);
