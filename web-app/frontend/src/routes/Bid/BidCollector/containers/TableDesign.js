import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { useHistory } from 'react-router';
import { useParams } from 'react-router-dom';
import PropTypes from 'prop-types';
import { COLUMN_MODAL_FORM_NAME } from '@utils/formNames';

import { Add } from '@material-ui/icons';
import { Box } from '@material-ui/core';
import { Button } from '@common/components/Button/Button';
import { useCustomTranslation } from '@utils/translate';

import { projectsSelectors } from '@selectors/projects';
import { projectDataSelectors } from '@selectors/projectData';
import { biddingRoundsSelectors } from '@selectors/biddingRounds';
import { userSelectors } from '@selectors/user';

import { getProjectDetails } from '@actions/projects';
import {
  deleteColumn,
  deleteRound,
  getColumnTypes,
  getRoundTableDesign,
  setColumns,
  updateRoundTableDesign,
} from '@actions/biddingRounds';
import { setBackButton, setPageTitle } from '@actions/interface';
import { bindActionCreators } from 'redux';

import { blur, change, getFormValues } from 'redux-form';

import { ConfirmationDialog } from '@common/components/ConfirmationDialog/ConfirmationDialog';
import { ProjectsLayout } from '../../../ProjectDetails/components/ProjectsLayout/ProjectsLayout';
import { RoundHeader } from '../../common/RoundHeader/RoundHeader';
import { Column } from '../../common/Column/Column';
import { ScrollBars } from '../../common/ScrollBars/ScrollBars';
import { ColumnModalForm } from '../components/TableDesign/ColumnModalForm';

const StyledContainer = styled.div`
  padding: 0 65px;
`;

const StyledColumnsContainer = styled.div`
  height: 55vh;
`;

const StyledButton = styled(Button)`
  &&.MuiButton-root {
    display: flex;
    justify-content: flex-start;
    align-items: center;
    width: 121px;
    margin-bottom: 11px;
    color: rgba(44, 44, 44, 0.81);
    font-weight: 500;
    font-size: 12px;
    line-height: 13px;
    text-transform: none;
    cursor: pointer;
  }

  &&.MuiButton-outlined {
    border: 1px solid #ffc100;
    border-radius: 5px;
    padding: 6px 10px;
  }

  svg {
    fill: rgba(0, 0, 0, 0.5);
    margin-right: 6px;
    font-size: 17px;
  }

  &:hover {
    color: rgba(44, 44, 44, 1);
  }
`;

const newColumnIdPrefix = 'new-column-';

const TableDesign = ({
  blurAction,
  changeAction,
  columnTypes,
  deleteColumnAction,
  formValues,
  getProjectDetailsAction,
  setBackButtonAction,
  setPageTitleAction,
  getRoundTableDesignAction,
  getColumnTypesAction,
  updateRoundTableDesignAction,
  columns,
  setColumnsAction,
  deleteRoundAction,
  user,
}) => {
  const { id: projectId, roundId } = useParams();

  const { t } = useCustomTranslation();
  const [deleteColumnId, setDeleteColumnId] = useState(null);
  const [editColumn, setEditColumn] = useState(null);
  const [isColumnForm, setIsColumnForm] = useState(false);

  const history = useHistory();

  useEffect(() => {
    setBackButtonAction('arrow');
    getProjectDetailsAction(projectId).then(project =>
      setPageTitleAction(project.name)
    );
    getRoundTableDesignAction(projectId);
    getColumnTypesAction();

    return () => setBackButtonAction('');
  }, [projectId, roundId]);

  const moveColumn = (dragIndex, hoverIndex) => {
    const dragColumn = columns[dragIndex];
    const newColumns = [...columns];

    newColumns.splice(dragIndex, 1);
    newColumns.splice(hoverIndex, 0, dragColumn);
    setColumnsAction(newColumns);
  };

  const removeColumn = () => {
    const isNew = deleteColumnId.includes(newColumnIdPrefix);

    setColumnsAction(columns.filter(column => column.id !== deleteColumnId));

    if (!isNew) {
      deleteColumnAction(projectId, deleteColumnId);
    }
    setDeleteColumnId(null);
  };

  const updateColumn = (values) => {
    setColumnsAction(
      [...columns].map(column =>
        column.id === editColumn.id ? values : column
      )
    );
  };

  const addColumn = (values) => {
    setColumnsAction([
      ...columns,
      {
        id: `${newColumnIdPrefix}${[columns.length - 1]}`,
        ...values,
      },
    ]);
  };

  const handleEdit = (id) => {
    setIsColumnForm(true);
    const columnToEdit = columns.find(column => column.id === id);

    setEditColumn(columnToEdit);
  };

  const handleSave = (columnFormValues) => {
    if (editColumn) {
      updateColumn(columnFormValues);
    } else {
      addColumn(columnFormValues);
    }

    setIsColumnForm(false);
  };

  const handleNextStep = (isRedirectToOverview) => {
    const columnsToSubmit = columns.map(
      ({
        id,
        inputBy,
        isMandatory,
        isVisible,
        name,
        typeId,
        limitMin,
        limitMax,
        singleChoice,
      }) => {
        const isNew = id.includes(newColumnIdPrefix);

        return {
          ...(!isNew ? { id } : {}),
          inputBy,
          isMandatory,
          isVisible,
          name,
          typeId,
          ...(limitMax ? { limitMax: Number(limitMax) } : {}),
          ...(limitMin ? { limitMin: Number(limitMin) } : {}),
          ...(singleChoice && singleChoice.length ? { singleChoice } : {}),
        };
      }
    );

    updateRoundTableDesignAction(projectId, { columns: columnsToSubmit }).then(
      () => {
        history.push(
          isRedirectToOverview
            ? `/projects/${projectId}/bid-collector`
            : `/projects/${projectId}/bid-collector/${roundId}/table-content`
        );
      }
    );
  };

  const columnFormInitialValues = editColumn || {
    isMandatory: true,
    isVisible: true,
  };

  const { branchName } = user;

  return (
    <>
      {isColumnForm && (
        <ColumnModalForm
          isOpen={isColumnForm}
          handleClose={() => setIsColumnForm(false)}
          formValues={formValues}
          blurAction={blurAction}
          changeAction={changeAction}
          onSubmit={handleSave}
          initialValues={columnFormInitialValues}
          columnTypes={columnTypes}
          branchName={branchName}
          columns={columns}
        />
      )}
      <ConfirmationDialog
        isOpen={!!deleteColumnId}
        handleClose={() => setDeleteColumnId(null)}
        handleConfirm={removeColumn}
        message={t('Do you really want to delete this column?')}
      />
      <ProjectsLayout
        sidebarLevel={2}
        saveProgressAction={() => handleNextStep(true)}
        deleteRoundAction={deleteRoundAction}
      >
        <StyledContainer>
          <RoundHeader
            title={t('Bid Sheet Design')}
            textUnderTitle={t(
              'Setup new columns in the bid sheet below or upload via Excel.'
            )}
            nextButtonLabel={t('Table Content')}
            nextButtonAction={handleNextStep}
          />
          <Box mt={43}>
            <StyledButton
              onClick={() => handleEdit(null)}
              variant="outlined"
              component="button"
            >
              <Add />
              <span>{t('Add Column')}</span>
            </StyledButton>
          </Box>
          <StyledColumnsContainer>
            <ScrollBars
              autoHide
              renderView={({ ...props }) => (
                <div {...props} className="scrollbar-view" />
              )}
            >
              {columns.map(({ name, id, isDefault, typeId }, index) => (
                <Column
                  key={id}
                  index={index}
                  id={id}
                  label={name}
                  onMove={moveColumn}
                  setDeleteColumnId={setDeleteColumnId}
                  onEdit={handleEdit}
                  isDefault={isDefault}
                  columnType={columnTypes.find(({ id: cId }) => cId === typeId)}
                />
              ))}
            </ScrollBars>
          </StyledColumnsContainer>
        </StyledContainer>
      </ProjectsLayout>
    </>
  );
};

TableDesign.propTypes = {
  blurAction: PropTypes.func.isRequired,
  changeAction: PropTypes.func.isRequired,
  columnTypes: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  columns: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  deleteColumnAction: PropTypes.func.isRequired,
  deleteRoundAction: PropTypes.func.isRequired,
  formValues: PropTypes.shape({}),
  getColumnTypesAction: PropTypes.func.isRequired,
  getProjectDetailsAction: PropTypes.func.isRequired,
  getRoundTableDesignAction: PropTypes.func.isRequired,
  setBackButtonAction: PropTypes.func.isRequired,
  setColumnsAction: PropTypes.func.isRequired,
  setPageTitleAction: PropTypes.func.isRequired,
  updateRoundTableDesignAction: PropTypes.func.isRequired,
  user: PropTypes.shape({}).isRequired,
  validation: PropTypes.shape({}).isRequired,
};

TableDesign.defaultProps = {
  formValues: {},
};

const mapStateToProps = state => ({
  columnTypes: biddingRoundsSelectors.getColumnTypes(state),
  columns: biddingRoundsSelectors.getColumnsForTableDesign(state),
  formValues: getFormValues(COLUMN_MODAL_FORM_NAME)(state),
  projectDetails: projectsSelectors.getProjectDetails(state),
  user: userSelectors.getUser(state),
  validation: projectDataSelectors.getValidation(state),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      blurAction: blur,
      changeAction: change,
      deleteColumnAction: deleteColumn,
      deleteRoundAction: deleteRound,
      getColumnTypesAction: getColumnTypes,
      getProjectDetailsAction: getProjectDetails,
      getRoundTableDesignAction: getRoundTableDesign,
      setBackButtonAction: setBackButton,
      setColumnsAction: setColumns,
      setPageTitleAction: setPageTitle,
      updateRoundTableDesignAction: updateRoundTableDesign,
    },
    dispatch
  );

export const TableDesignContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(TableDesign);
