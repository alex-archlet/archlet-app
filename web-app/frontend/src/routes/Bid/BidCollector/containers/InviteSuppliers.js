import React, { useEffect, useState } from 'react';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { reset } from 'redux-form';
import PropTypes from 'prop-types';
import { useHistory, useParams } from 'react-router-dom';
import {
  Box,
  MenuItem,
  TextField,
} from '@material-ui/core';
import { ROUND_INVITE_SUPPLIERS_FORM } from '@utils/formNames';
import { useCustomTranslation } from '@utils/translate';
import {
  deleteRound,
  deleteSupplier,
  getInvitedSuppliersList,
  getTemplatesSuppliersList,
  inviteSupplier,
  inviteSuppliersByTemplate,
} from '@actions/biddingRounds';
import {
  setBackButton,
  setPageTitle,
} from '@actions/interface';
import { getProjectDetails } from '@actions/projects';
import { biddingRoundsSelectors } from '@selectors/biddingRounds';
import { ProjectsLayout } from '../../../ProjectDetails/components/ProjectsLayout/ProjectsLayout';
import { SuppliersTable } from '../components/InviteSuppliers/SuppliersTable';
import { RoundHeader } from '../../common/RoundHeader/RoundHeader';
import { InviteSuppliersForm } from '../components/InviteSuppliers/InviteSuppliersForm';

const StyledContainer = styled.div`
  padding: 0 65px;
`;

const StyledDropdown = styled(TextField)`
  &&.MuiFormControl-root {
    width: 200px;
  }

  && {
    .MuiInput-underline:hover:not(.Mui-disabled):before,
    .MuiInput-underline:before,
    .MuiInput-underline:after {
      border-bottom: 2px solid #6BCAE5;
    }
    
    .MuiSelect-select:focus {
      background-color: #fff;
    }
  }
`;

const InviteSuppliers = ({
  deleteRoundAction,
  deleteSupplierAction,
  getInvitedSuppliersListAction,
  getProjectDetailsAction,
  getTemplatesSuppliersListAction,
  inviteSupplierAction,
  inviteSuppliersByTemplateAction,
  invitedSuppliers,
  resetFormAction,
  setBackButtonAction,
  setPageTitleAction,
  templatesSuppliers,
}) => {
  const {
    id: projectId,
    roundId,
  } = useParams();
  const { t } = useCustomTranslation();

  useEffect(() => {
    setBackButtonAction('arrow');
    getProjectDetailsAction(projectId).then(project => setPageTitleAction(project.name));

    getInvitedSuppliersListAction(projectId, roundId);
    getTemplatesSuppliersListAction();
  }, [
    getInvitedSuppliersListAction,
    getProjectDetailsAction,
    getTemplatesSuppliersListAction,
    projectId,
    roundId,
    setBackButtonAction,
    setPageTitleAction]);

  const [stepError, setStepError] = useState('');

  const [selectedTemplate, setSelectedTemplate] = useState('');

  const history = useHistory();

  const handleNextStep = () => {
    setStepError('');

    history.push(`/projects/${projectId}/bid-collector/${roundId}/schedule`);
  };

  const onSubmit = (values) => {
    inviteSupplierAction(projectId, roundId, values)
      .then(() => resetFormAction(ROUND_INVITE_SUPPLIERS_FORM));
  };

  const handleSelectTemplate = (evt) => {
    const { value } = evt.target;

    setSelectedTemplate(value);

    inviteSuppliersByTemplateAction(projectId, roundId, value)
      .then(() => {
        setSelectedTemplate('');
        getInvitedSuppliersListAction(projectId, roundId);
      });
  };

  const selectedTemplates = templatesSuppliers.filter(template => template.id !== roundId);

  return (
    <ProjectsLayout
      sidebarLevel={2}
      deleteRoundAction={deleteRoundAction}
    >
      <StyledContainer>
        <RoundHeader
          title={t('Invite Suppliers')}
          textUnderTitle={t('Invite your suppliers for this bidding round.')}
          nextButtonLabel={t('Schedule')}
          nextButtonAction={handleNextStep}
          stepError={stepError}
        />
        <Box
          pt={70}
          pl={30}
          pr={30}
        >
          <StyledDropdown
            name="template"
            type="text"
            label={t('Add from Supplier List')}
            margin="dense"
            onChange={handleSelectTemplate}
            value={selectedTemplate}
            select
          >
            {selectedTemplates && !!selectedTemplates.length && selectedTemplates.map(template => (
              <MenuItem
                key={template.id}
                value={template.id}
              >
                {template.name}
              </MenuItem>
            ))}
          </StyledDropdown>

          <InviteSuppliersForm onSubmit={onSubmit} />

          <SuppliersTable
            suppliers={invitedSuppliers}
            onDelete={supplier => deleteSupplierAction(projectId, roundId, supplier.id)}
          />
        </Box>
      </StyledContainer>
    </ProjectsLayout>
  );
};

InviteSuppliers.propTypes = ({
  deleteRoundAction: PropTypes.func.isRequired,
  deleteSupplierAction: PropTypes.func.isRequired,
  getInvitedSuppliersListAction: PropTypes.func.isRequired,
  getProjectDetailsAction: PropTypes.func.isRequired,
  getTemplatesSuppliersListAction: PropTypes.func.isRequired,
  inviteSupplierAction: PropTypes.func.isRequired,
  inviteSuppliersByTemplateAction: PropTypes.func.isRequired,
  invitedSuppliers: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  resetFormAction: PropTypes.func.isRequired,
  setBackButtonAction: PropTypes.func.isRequired,
  setPageTitleAction: PropTypes.func.isRequired,
  templatesSuppliers: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
});

const mapStateToProps = state => ({
  invitedSuppliers: biddingRoundsSelectors.getInvitedSuppliers(state),
  templatesSuppliers: biddingRoundsSelectors.getTemplatesSuppliers(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  deleteRoundAction: deleteRound,
  deleteSupplierAction: deleteSupplier,
  getInvitedSuppliersListAction: getInvitedSuppliersList,
  getProjectDetailsAction: getProjectDetails,
  getTemplatesSuppliersListAction: getTemplatesSuppliersList,
  inviteSupplierAction: inviteSupplier,
  inviteSuppliersByTemplateAction: inviteSuppliersByTemplate,
  resetFormAction: reset,
  setBackButtonAction: setBackButton,
  setPageTitleAction: setPageTitle,
}, dispatch);

export const InviteSuppliersContainer = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(InviteSuppliers);
