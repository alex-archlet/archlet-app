import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useHistory } from 'react-router';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { useParams } from 'react-router-dom';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';

import { getProjectDetails } from '@actions/projects';

import { setBackButton, setPageTitle } from '@actions/interface';

import {
  createOrUpdateCostCalculation,
  deleteRound,
  getRoundTableDesign,
} from '@actions/biddingRounds';

import { biddingRoundsSelectors } from '@selectors/biddingRounds';

import { useCustomTranslation } from '@utils/translate';
import { isValidExpression } from '@utils/expressionParser';

import { CostCalculationExpressionBar } from '@routes/Bid/common/CostCalculation/CostCalculationExpressionBar';
import { ProjectsLayout } from '../../../ProjectDetails/components/ProjectsLayout/ProjectsLayout';
import { RoundHeader } from '../../common/RoundHeader/RoundHeader';
import {
  ALLOWED_OPERATIONS,
  AvailableElements,
} from '../../common/CostCalculation/AvailableElements';

const StyledContainer = styled.div`
  padding: 0 65px;
`;

const getItemFromValue = item => ({
  name: item,
  type: Number.isNaN(Number(item)) ? 'invalid' : 'customNumber',
});

const componentsToExpression = calculationComponents =>
  calculationComponents.map(item => item.name.replace(/\s+/g, '_')).join(' ');

const CostCalculation = ({
  allColumns,
  columns,
  createOrUpdateCostCalculationAction,
  getProjectDetailsAction,
  getRoundTableDesignAction,
  setBackButtonAction,
  setPageTitleAction,
  deleteRoundAction,
}) => {
  const { id: projectId, roundId } = useParams();
  const { t } = useCustomTranslation();
  const history = useHistory();

  useEffect(() => {
    setBackButtonAction('arrow');
    getProjectDetailsAction(projectId).then(project =>
      setPageTitleAction(project.name)
    );
    getRoundTableDesignAction(projectId);

    return () => setBackButtonAction('');
  }, []);

  const [autocompleteValue, setAutocompleteValue] = useState(null);

  const elements = useMemo(
    () => [
      ...columns.map(column => ({
        column,
        name: column.name,
        type: 'column',
      })),
    ],
    [columns]
  );

  const validOptions = useMemo(() => [...elements, ...ALLOWED_OPERATIONS], [
    elements,
  ]);

  const parseAndSetAutocompleteValue = useCallback((valueArray) => {
    const valueFiltered = valueArray.filter(
      item => !(typeof item === 'string' && item.trim() === '')
    );

    const valueMapped = valueFiltered.map((item) => {
      if (!item.type && !item.name) {
        const matchingOption = validOptions.find(
          option => option.name.toLowerCase() === item.trim().toLowerCase()
        );

        if (matchingOption) {
          return matchingOption;
        }

        return getItemFromValue(item);
      }

      return item;
    });

    setAutocompleteValue(valueMapped);
  });

  const onChange = (_, value) => {
    parseAndSetAutocompleteValue(value);
  };

  const onValueClick = (value) => {
    parseAndSetAutocompleteValue([...(autocompleteValue || []), value]);
  };

  useEffect(() => {
    const totalCostColumn = allColumns.find(
      column => column.column === 'price_total'
    );

    if (totalCostColumn && totalCostColumn.calculation) {
      const calculationArray = totalCostColumn.calculation.split(
        // eslint-disable-next-line no-useless-escape
        /([+\-*\()])/
      );

      parseAndSetAutocompleteValue(calculationArray);
    }
  }, [allColumns]);

  const validateCalculation = (calculationComponents) => {
    if (!calculationComponents) {
      return false;
    }

    const isAnyItemInvalid = calculationComponents.some(
      item => item.type === 'invalid'
    );

    if (isAnyItemInvalid) {
      return false;
    }

    return isValidExpression(componentsToExpression(calculationComponents));
  };

  const handleNextStep = (calculationValue) => {
    const isStepValid = validateCalculation(calculationValue);

    if (isStepValid) {
      const calculationString = calculationValue
        .map(item => item.name)
        .join('');

      createOrUpdateCostCalculationAction(
        projectId,
        roundId,
        calculationString
      ).then(() => {
        history.push(
          `/projects/${projectId}/bid-collector/${roundId}/invite-suppliers`
        );
      });
    } else if (autocompleteValue === null) {
      // if invalid, set autocomplete to empty array to show error
      setAutocompleteValue([]);
    }
  };

  const isValid = useMemo(() => {
    // if input not touched yet, don't show an error
    if (autocompleteValue === null) {
      return true;
    }

    return validateCalculation(autocompleteValue);
  }, [autocompleteValue]);

  return (
    <ProjectsLayout
      sidebarLevel={2}
      saveProgressAction={() => handleNextStep(autocompleteValue)}
      deleteRoundAction={deleteRoundAction}
    >
      <StyledContainer>
        <RoundHeader
          title={t('Cost Calculation')}
          textUnderTitle={t(
            'Define how the total cost is calculated in the field below. Write in the input or click on a component below.'
          )}
          nextButtonLabel={t('Invite Suppliers')}
          nextButtonAction={() => handleNextStep(autocompleteValue)}
          next
        />
        <AvailableElements columns={columns} onClick={onValueClick} />
        <CostCalculationExpressionBar
          value={autocompleteValue}
          onChange={onChange}
          isValid={isValid}
          elements={elements}
        />
      </StyledContainer>
    </ProjectsLayout>
  );
};

CostCalculation.propTypes = {
  allColumns: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  columns: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  createOrUpdateCostCalculationAction: PropTypes.func.isRequired,
  deleteRoundAction: PropTypes.func.isRequired,
  getProjectDetailsAction: PropTypes.func.isRequired,
  getRoundTableDesignAction: PropTypes.func.isRequired,
  setBackButtonAction: PropTypes.func.isRequired,
  setPageTitleAction: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  allColumns: biddingRoundsSelectors.getRoundTableDesign(state),
  columns: biddingRoundsSelectors.getColumnsForTableDesign(state),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      createOrUpdateCostCalculationAction: createOrUpdateCostCalculation,
      deleteRoundAction: deleteRound,
      getProjectDetailsAction: getProjectDetails,
      getRoundTableDesignAction: getRoundTableDesign,
      setBackButtonAction: setBackButton,
      setPageTitleAction: setPageTitle,
    },
    dispatch
  );

export const CostCalculationContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(CostCalculation);
