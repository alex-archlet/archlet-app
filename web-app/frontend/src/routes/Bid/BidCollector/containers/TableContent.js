import React, {
  useEffect,
  useState,
} from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { useHistory } from 'react-router';
import { useParams } from 'react-router-dom';
import PropTypes from 'prop-types';
import { useCustomTranslation } from '@utils/translate';
import {
  bindActionCreators,
} from 'redux';

import {
  getProjectCurrencies,
  getProjectDetails,
  getProjectUnits,
} from '@actions/projects';

import {
  setBackButton,
  setPageTitle,
} from '@actions/interface';

import {
  createRoundTableContent,
  deleteRound,
  getColumnTypes,
  getRoundTableContent,
  getRoundTableDesign,
  setGrid,
} from '@actions/biddingRounds';

import { projectsSelectors } from '@selectors/projects';

import { biddingRoundsSelectors } from '@selectors/biddingRounds';

import { ProjectsLayout } from '../../../ProjectDetails/components/ProjectsLayout/ProjectsLayout';
import { RoundHeader } from '../../common/RoundHeader/RoundHeader';
import { SelectedCellForm } from '../components/TableContent/SelectedCellForm';
import { Spreadsheet } from '../components/TableContent/Spreadsheet';

const StyledContainer = styled.div`
  padding: 0 65px;
`;

const TableContent = ({
  columns,
  columnTypes,
  grid,
  setGridAction,
  getProjectDetailsAction,
  setBackButtonAction,
  setPageTitleAction,
  getRoundTableDesignAction,
  getColumnTypesAction,
  getRoundTableContentAction,
  createRoundTableContentAction,
  getProjectCurrenciesAction,
  getProjectUnitsAction,
  projectCurrencies,
  projectUnits,
  deleteRoundAction,
}) => {
  const {
    id: projectId,
    roundId,
  } = useParams();
  const { t } = useCustomTranslation();
  const history = useHistory();

  const [selectedCells, setSelectedCells] = useState({
    end: {
      i: 1,
      j: 0,
    },
    start: {
      i: 1,
      j: 0,
    },
  });

  const [stepError, setStepError] = useState('');

  const generateEmptyRows = (rowCount, startRow = 0) => (
    [...Array(rowCount)]
      .map((row, rowNumber) => [...Array(columns.length)]
        .map((col, columnNumber) => ({
          className: 'custom-cell',
          columnNumber,
          rowNumber: startRow + rowNumber + 1,
          value: '',
        })))
  );

  const getLastRowWithValues = (inputGrid) => {
    const firstReversedGridRowWithValues = [...inputGrid]
      .reverse()
      .findIndex(row => !!row.find(
        column => !!column.value
      ));

    return firstReversedGridRowWithValues >= 0 ?
      inputGrid.length - firstReversedGridRowWithValues - 1 :
      0;
  };

  const numberValidator = (value, min = 0, max) => {
    if (max) {
      return !(value >= min && value <= max);
    }

    return !(value > min);
  };

  const singleChoiceValidator = (value, choices) => (
    !choices.includes(value)
  );

  const validateCell = (cell, lastRowWithValues) => {
    const { rowNumber, columnNumber, value } = cell;

    const column = columns[columnNumber];
    const {
      singleChoice,
      isMandatory,
      limitMax,
      limitMin,
      typeId,
    } = column;

    const matchingType = columnTypes.find(type => type.id === typeId);

    const typeName = !!matchingType && matchingType.type;

    if (rowNumber <= lastRowWithValues && isMandatory && value === '') {
      return true;
    }

    if (value && (typeName === 'number' || typeName === 'volume' || typeName === 'price' || typeName === 'price_total')) {
      return numberValidator(value, limitMin, limitMax);
    }

    if (value && typeName === 'single_choice' && singleChoice) {
      return singleChoiceValidator(value, singleChoice);
    }

    if (value && typeName === 'unit' && projectUnits) {
      return singleChoiceValidator(value, projectUnits.map(unit => unit.name));
    }

    if (value && typeName === 'currency' && projectCurrencies) {
      return singleChoiceValidator(value, projectCurrencies.map(currency => currency.code));
    }

    return undefined;
  };

  const validateGrid = (inputGrid) => {
    const lastRowWithValues = getLastRowWithValues(inputGrid);

    const validatedGrid = inputGrid.map((row, rowIndex) => (rowIndex > 0 ? (
      row.map((cell) => {
        const isCellInvalid = validateCell(cell, lastRowWithValues);

        return ({
          ...cell,
          className: isCellInvalid ?
            'custom-cell custom-cell--error' :
            'custom-cell',
          isInvalid: isCellInvalid,
        });
      })
    ) : (
      row
    )));

    return validatedGrid;
  };

  useEffect(() => {
    const getTableDesignAndContent = async () => {
      await getRoundTableDesignAction(projectId);
      const units = await getProjectUnitsAction();

      getRoundTableContentAction(projectId, roundId, units);
    };

    getTableDesignAndContent();
    setBackButtonAction('arrow');
    getProjectDetailsAction(projectId).then(project => setPageTitleAction(project.name));
    getProjectCurrenciesAction();
    getColumnTypesAction();

    return () => setBackButtonAction('');
  }, [projectId, roundId]);

  const setSelectedCellValue = (event, shouldValidate) => {
    const inputValue = event.target.value;
    const {
      i: rowNumber,
      j: columnNumber,
    } = selectedCells.start;

    let newGrid = [...grid].map((row, rowIndex) => (
      rowIndex === rowNumber ?
        row.map((column, columnIndex) => (
          columnIndex === columnNumber ?
            {
              ...column,
              value: inputValue,
            } :
            column
        )) :
        row
    ));

    if (event.key === 'Enter' || shouldValidate) {
      newGrid = validateGrid(newGrid, columns);
    }

    setGridAction(newGrid);
  };

  const handleNextStep = (isRedirectToOverview) => {
    const validatedGrid = validateGrid(grid, columns);

    setGridAction(validatedGrid);

    const isGridValid = validatedGrid.every(row => row.every(cell => !cell.isInvalid));

    setStepError(isGridValid ? '' : t('Some of the cells have missing or incorrect values.'));

    if (!isGridValid) {
      return;
    }

    const lastRowWithValues = getLastRowWithValues(grid);

    const gridToSubmit = grid.slice(1, lastRowWithValues + 1).map(row => (
      row.reduce((accumulator, current) => {
        const { columnNumber, value } = current;

        const column = columns[columnNumber];
        const {
          id,
          isDefault,
          column: columnName,
        } = column;

        let parsedColumnName = columnName;
        let parsedValue = value;

        if (columnName === 'unit' || columnName === 'currency') {
          parsedColumnName = `${columnName}Id`;
        }

        if (columnName === 'currency') {
          const matchingCurrency = projectCurrencies.find(item => item.code === value);

          parsedValue = matchingCurrency ? matchingCurrency.id : undefined;
        } else if (columnName === 'unit') {
          const matchingUnit = projectUnits.find(item => item.name === value);

          parsedValue = matchingUnit ? matchingUnit.id : undefined;
        } else if (columnName === 'number' && value) {
          parsedValue = Number(value);
        }

        return isDefault ?
          ({
            ...accumulator,
            [parsedColumnName]: parsedValue,
          }) :
          ({
            ...accumulator,
            attributes: {
              ...accumulator.attributes,
              [id]: parsedValue,
            },
          });
      }, {})));

    createRoundTableContentAction(projectId, roundId, { rows: gridToSubmit })
      .then(() => {
        history.push(isRedirectToOverview ?
          `/projects/${projectId}/bid-collector` :
          `/projects/${projectId}/bid-collector/${roundId}/cost-calculation`);
      });
  };

  return (
    <ProjectsLayout
      sidebarLevel={2}
      saveProgressAction={() => handleNextStep(true)}
      deleteRoundAction={deleteRoundAction}
    >
      <StyledContainer>
        <RoundHeader
          title={t('Bid Sheet Content')}
          textUnderTitle={t('Populate the bid table in this step.')}
          nextButtonLabel={t('Cost Calculation')}
          nextButtonAction={handleNextStep}
          stepError={stepError}
        />
        <SelectedCellForm
          selectedCells={selectedCells}
          columns={columns}
          columnTypes={columnTypes}
          projectCurrencies={projectCurrencies.map(item => item.code)}
          projectUnits={projectUnits.map(item => item.name)}
          grid={grid}
          setSelectedCellValue={setSelectedCellValue}
        />
        <Spreadsheet
          grid={grid}
          setGrid={setGridAction}
          selectedCells={selectedCells}
          setSelectedCells={setSelectedCells}
          columns={columns}
          validateGrid={validateGrid}
          generateEmptyRows={generateEmptyRows}
        />
      </StyledContainer>
    </ProjectsLayout>
  );
};

TableContent.propTypes = ({
  columnTypes: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  columns: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  createRoundTableContentAction: PropTypes.func.isRequired,
  deleteRoundAction: PropTypes.func.isRequired,
  getColumnTypesAction: PropTypes.func.isRequired,
  getProjectCurrenciesAction: PropTypes.func.isRequired,
  getProjectDetailsAction: PropTypes.func.isRequired,
  getProjectUnitsAction: PropTypes.func.isRequired,
  getRoundTableContentAction: PropTypes.func.isRequired,
  getRoundTableDesignAction: PropTypes.func.isRequired,
  grid: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.shape({}))).isRequired,
  projectCurrencies: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  projectUnits: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  setBackButtonAction: PropTypes.func.isRequired,
  setGridAction: PropTypes.func.isRequired,
  setPageTitleAction: PropTypes.func.isRequired,
});

const mapStateToProps = state => ({
  columnTypes: biddingRoundsSelectors.getColumnTypes(state),
  columns: biddingRoundsSelectors.getColumnsForTableContent(state),
  grid: biddingRoundsSelectors.getRoundTableContent(state),
  projectCurrencies: projectsSelectors.getCurrencies(state),
  projectDetails: projectsSelectors.getProjectDetails(state),
  projectUnits: projectsSelectors.getUnits(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  createRoundTableContentAction: createRoundTableContent,
  deleteRoundAction: deleteRound,
  getColumnTypesAction: getColumnTypes,
  getProjectCurrenciesAction: getProjectCurrencies,
  getProjectDetailsAction: getProjectDetails,
  getProjectUnitsAction: getProjectUnits,
  getRoundTableContentAction: getRoundTableContent,
  getRoundTableDesignAction: getRoundTableDesign,
  setBackButtonAction: setBackButton,
  setGridAction: setGrid,
  setPageTitleAction: setPageTitle,
}, dispatch);

export const TableContentContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(TableContent);
