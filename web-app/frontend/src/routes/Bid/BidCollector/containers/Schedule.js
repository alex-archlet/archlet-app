import React, {
  useEffect,
  useState,
} from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { useHistory } from 'react-router';
import { useParams } from 'react-router-dom';
import PropTypes from 'prop-types';

import moment from 'moment-timezone';

import {
  getFormValues,
  initialize,
  submit,
} from 'redux-form';

import { convertDateWithTimezoneToUTC } from '@utils/convertDate';
import { ROUND_SCHEDULE_FORM_NAME } from '@utils/formNames';
import { timezones } from '@utils/timezones';
import { useCustomTranslation } from '@utils/translate';

import { projectsSelectors } from '@selectors/projects';
import { projectDataSelectors } from '@selectors/projectData';
import { biddingRoundsSelectors } from '@selectors/biddingRounds';

import {
  getProjectDetails,
} from '@actions/projects';
import {
  createRound,
  deleteRound,
  getRoundDetails,
  getRoundTemplates,
  updateRound,
} from '@actions/biddingRounds';

import {
  setBackButton,
  setPageTitle,
} from '@actions/interface';
import { bindActionCreators } from 'redux';

import { ProjectsLayout } from '../../../ProjectDetails/components/ProjectsLayout/ProjectsLayout';
import { RoundHeader } from '../../common/RoundHeader/RoundHeader';
import { ScheduleForm } from '../components/ScheduleForm/ScheduleForm';

const StyledContainer = styled.div`
  padding: 0 65px;
`;

const Schedule = ({
  getProjectDetailsAction,
  getRoundDetailsAction,
  roundDetails,
  setBackButtonAction,
  setPageTitleAction,
  submitForm,
  updateRoundAction,
  initializeAction,
  formValues,
  deleteRoundAction,
}) => {
  const {
    id: projectId,
    roundId,
  } = useParams();
  const { t } = useCustomTranslation();
  const {
    endAt,
    reminderAt,
    startAt,
  } = roundDetails;

  const { isReminder } = formValues;

  const history = useHistory();

  const [localTimezone, setLocalTimezone] = useState(null);

  useEffect(() => {
    setBackButtonAction('arrow');
    getProjectDetailsAction(projectId).then(project => setPageTitleAction(project.name));

    const guessedLocalTimezone = moment.tz.guess();
    const matchingTimezone = timezones.find(timezone => (
      timezone.utc.includes(guessedLocalTimezone)
    ));

    if (matchingTimezone) {
      setLocalTimezone(matchingTimezone);
    }

    if (roundId !== 'new') {
      getRoundDetailsAction(projectId, roundId);
      initializeAction(ROUND_SCHEDULE_FORM_NAME, {
        reminderTimezone: matchingTimezone,
      });
    } else {
      initializeAction(ROUND_SCHEDULE_FORM_NAME, {
        endTimezone: matchingTimezone,
        isReminder: false,
        reminderTimezone: matchingTimezone,
        startTimezone: matchingTimezone,
      });
    }

    return () => setBackButtonAction('');
  }, [projectId, roundId]);

  const submitScheduleForm = () => submitForm(ROUND_SCHEDULE_FORM_NAME);

  const onSubmit = (values) => {
    const {
      startDate,
      startTimezone,
      endDate,
      endTimezone,
      reminderDate,
      reminderTimezone,
    } = values;

    const startDateConverted = convertDateWithTimezoneToUTC(startDate, startTimezone);
    const endDateConverted = convertDateWithTimezoneToUTC(endDate, endTimezone);
    const reminderDateConverted = convertDateWithTimezoneToUTC(reminderDate, reminderTimezone);

    return updateRoundAction(projectId, roundId, {
      endAt: endDateConverted,
      isCompleted: true,
      reminderAt: isReminder ? reminderDateConverted : 'reset',
      startAt: startDateConverted,
    })
      .then((data) => {
        if (data) {
          history.push(`/projects/${projectId}/bid-collector/${data.id}/round-summary`);
        }
      });
  };

  return (
    <ProjectsLayout
      sidebarLevel={2}
      saveProgressAction={submitScheduleForm}
      deleteRoundAction={deleteRoundAction}
    >
      <StyledContainer>
        <RoundHeader
          title={t('Bidding Round Schedule')}
          textUnderTitle={t('Pick the dates to start and end the bidding round.')}
        />
        <ScheduleForm
          onSubmit={onSubmit}
          formValues={formValues}
          initialValues={{
            endDate: endAt,
            endTimezone: localTimezone,
            isReminder: !!reminderAt,
            reminderDate: reminderAt,
            reminderTimezone: localTimezone,
            startDate: startAt,
            startTimezone: localTimezone,
          }}
        />
      </StyledContainer>
    </ProjectsLayout>
  );
};

Schedule.propTypes = ({
  deleteRoundAction: PropTypes.func.isRequired,
  formValues: PropTypes.shape({}),
  getProjectDetailsAction: PropTypes.func.isRequired,
  getRoundDetailsAction: PropTypes.func.isRequired,
  initializeAction: PropTypes.func.isRequired,
  roundDetails: PropTypes.shape({}).isRequired,
  setBackButtonAction: PropTypes.func.isRequired,
  setPageTitleAction: PropTypes.func.isRequired,
  submitForm: PropTypes.func.isRequired,
  updateRoundAction: PropTypes.func.isRequired,
  validation: PropTypes.shape({}).isRequired,
});

Schedule.defaultProps = {
  formValues: {},
};

const mapStateToProps = state => ({
  formValues: getFormValues(ROUND_SCHEDULE_FORM_NAME)(state),
  projectDetails: projectsSelectors.getProjectDetails(state),
  roundDetails: biddingRoundsSelectors.getRoundDetails(state),
  validation: projectDataSelectors.getValidation(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  createRoundAction: createRound,
  deleteRoundAction: deleteRound,
  getProjectDetailsAction: getProjectDetails,
  getRoundDetailsAction: getRoundDetails,
  getRoundTemplatesAction: getRoundTemplates,
  initializeAction: initialize,
  setBackButtonAction: setBackButton,
  setPageTitleAction: setPageTitle,
  submitForm: submit,
  updateRoundAction: updateRound,
}, dispatch);

export const ScheduleContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Schedule);
