import React from 'react';
import styled from 'styled-components';
import { FormikHelpers, useFormik } from 'formik';

import { Close } from '@material-ui/icons';
import { Box, Dialog, IconButton, FormHelperText } from '@material-ui/core';
import * as Yup from 'yup';
import { useCustomTranslation } from '@utils/translate';
import { Input } from '@common/components/Input/Input';
import { SelectDropdown } from '@common/components/Dropdown/SelectDropdown';
import {
  MultiFilterValue,
  SingleFilterValue,
} from '@common/components/Filter/types';
import { GroupedButtons } from '@common/components/GroupedButtons/GroupedButtons';
import { capitalize } from '@utils/stringFormat';
import { Flex } from '@common/components/Flex/Flex';
import { OptionChips } from '@common/components/OptionChips/OptionsChips';
import { Button } from '@common/components/Button/Button';
import { ColumnType } from '../../BidImporter/containers/BidCorrections/BidCorrections';
import { BidColumn } from '../utils';

const StyledSaveButton = styled(Button)`
  &&.MuiButton-root {
    width: 110px;
    margin-top: 42px;
  }
`;

const StyledIconButton = styled(IconButton)`
  &&.MuiIconButton-root {
    position: absolute;
    top: 3px;
    right: 3px;
    flex: 0;
  }
`;

const StyledHeading = styled.h2`
  font-size: 22px;
`;

const StyledLabel = styled.p`
  margin-bottom: 5px;
  font-weight: 500;
  font-size: 11px;
  line-height: 15px;
`;

const StyledFormHelperText = styled(FormHelperText)`
  &&.MuiFormHelperText-root {
    font-size: 9px;
  }
`;

const getUniqueColumnTypesTaken = (columnTypes, columns, currentColumnId) =>
  columnTypes.filter((type) => {
    const matchingColumn = columns.find(
      column => column.id !== currentColumnId && column.typeId === type.id
    );

    return matchingColumn && type.isUnique;
  });

interface FormValues {
  id: string;
  isDefault: boolean;
  typeId: string;
  inputBy: 'supplier' | 'buyer';
  name: string;
  limitMin: number;
  limitMax: number;
  singleChoice: string[];
  isVisible: boolean;
}

interface Props {
  isOpen: boolean;
  columnTypes: ColumnType[];
  initialValues: FormValues;
  handleClose: (event: React.MouseEvent<HTMLButtonElement>) => void;
  onSubmit: (
    values: FormValues,
    formikHelpers: FormikHelpers<FormValues>
  ) => void | Promise<any>;
  branchName: string;
  columns: BidColumn[];
}

const arrayToDropdown = ({ text, id }) => ({
  label: text,
  value: id,
});

const transformInputByToDropdown = inputBy => ({
  value: inputBy,
  label: capitalize(inputBy),
});

export const BidColumnModalForm: React.FC<Props> = ({
  isOpen,
  columnTypes,
  initialValues,
  handleClose,
  onSubmit,
  columns,
}) => {
  const { t } = useCustomTranslation();

  const ReviewSchema = Yup.object().shape({
    name: Yup.string().required(t('A Bid Column name is required')),
    typeId: Yup.number().required(t('A Column Type is required')),
    inputBy: Yup.string().required(t('Input By is required')),
    isVisible: Yup.boolean().when('inputBy', {
      is: 'buyer',
      then: Yup.boolean().required(t('Visible to supplier is required')),
    }),
    limitMax: Yup.number()
      .nullable()
      .moreThan(
        Yup.ref('limitMin'),
        t('Max limit needs to be more than min limit')
      ),
  });

  const {
    values,
    errors,
    setFieldValue,
    setFieldTouched,
    handleSubmit,
  } = useFormik<FormValues>({
    initialValues: {
      ...initialValues,
    },
    validationSchema: ReviewSchema,
    onSubmit,
    validateOnBlur: true,
    validateOnChange: true,
  });

  const onChange = (field: string, value: string | number | null) => {
    setFieldTouched(field);
    setFieldValue(field, value);
  };

  const uniqueColumnTypesTaken: ColumnType[] = getUniqueColumnTypesTaken(
    columnTypes,
    columns,
    values.id
  );

  const columnTypesAvailable = columnTypes.filter(
    type => !uniqueColumnTypesTaken.includes(type)
  );

  const matchingType = columnTypesAvailable.find(
    type => type.id === values.typeId
  );

  const columnTypesOption: MultiFilterValue = columnTypesAvailable.map(
    arrayToDropdown
  );

  const matchingTypeOption: SingleFilterValue | [] = matchingType
    ? arrayToDropdown(matchingType)
    : [];

  const currentMatchingInputByOption: SingleFilterValue | [] = values.inputBy
    ? transformInputByToDropdown(values.inputBy)
    : [];

  const typeName = !!matchingType && matchingType.type;

  return (
    <Dialog
      open={isOpen}
      onClose={handleClose}
      aria-labelledby="form-dialog-title"
      maxWidth="md"
      disableEscapeKeyDown
    >
      <form onSubmit={handleSubmit}>
        <StyledIconButton onClick={handleClose}>
          <Close />
        </StyledIconButton>
        <Box width="662px" padding="60px 178px">
          <StyledHeading>{t('Specify your column')}</StyledHeading>
          <StyledLabel>{t('Column Name')}</StyledLabel>
          <Input
            value={values.name}
            placeholder={t('e.g. Unit Price')}
            onChange={value => onChange('name', value)}
            onBlur={() => setFieldTouched('name')}
            error={Boolean(errors.name)}
            helperText={errors.name}
            height="19px"
            bidImporterStyle
          />
          <StyledLabel>{t('Column Type')}</StyledLabel>
          <SelectDropdown
            options={columnTypesOption}
            onChange={value => onChange('typeId', value)}
            selectedColumnValue={matchingTypeOption}
            style={{
              controlBorderColor: '#ffc100',
              optionBorderColor: '#efefef',
              optionSelectColor: '#efefef',
              textColor: 'black',
            }}
          />
          <FormHelperText error={Boolean(errors.typeId)}>
            {errors.typeId}
          </FormHelperText>
          <StyledLabel>{t('Input By')}</StyledLabel>
          <SelectDropdown
            options={[
              {
                label: t('Supplier') as string,
                value: 'supplier',
              },
              {
                label: t('Buyer') as string,
                value: 'buyer',
              },
            ]}
            style={{
              controlBorderColor: '#ffc100',
              optionBorderColor: '#efefef',
              optionSelectColor: '#efefef',
              textColor: 'black',
            }}
            onChange={value => onChange('inputBy', value)}
            selectedColumnValue={currentMatchingInputByOption}
          />
          <FormHelperText error={Boolean(errors.inputBy)}>
            {errors.inputBy}
          </FormHelperText>
          {values.inputBy === 'buyer' && (
            <>
              <StyledLabel>{t('Visible to Supplier')}</StyledLabel>
              <GroupedButtons
                options={[
                  { label: t('Yes'), value: true },
                  { label: t('No'), value: false },
                ]}
                values={values.isVisible}
                onChange={value => onChange('isVisible', value)}
              />
              <StyledFormHelperText>
                {t('Define if the column can be seen by the supplier.')}
              </StyledFormHelperText>
            </>
          )}
          {typeName === 'number' && (
            <>
              <StyledLabel>{t('Limit Values')}</StyledLabel>
              <Flex>
                <Input
                  value={values.limitMin ?? null}
                  placeholder={t('Min')}
                  onChange={value =>
                    onChange(
                      'limitMin',
                      value === '' ? null : parseInt(value, 10)
                    )
                  }
                  onBlur={() => setFieldTouched('limitMin')}
                  error={Boolean(errors.limitMin)}
                  helperText={errors.limitMin}
                  height="19px"
                />
                <Input
                  value={values.limitMax ?? null}
                  placeholder={t('Max')}
                  onChange={value =>
                    onChange(
                      'limitMax',
                      value === '' ? null : parseInt(value, 10)
                    )
                  }
                  onBlur={() => setFieldTouched('limitMax')}
                  error={Boolean(errors.limitMax)}
                  helperText={errors.limitMax}
                  height="19px"
                />
              </Flex>
            </>
          )}
          {typeName === 'single_choice' && (
            <div>
              <StyledLabel>{t('Add Single Choice Elements')}</StyledLabel>
              <OptionChips
                onChange={value => onChange('singleChoice', value)}
                values={values.singleChoice ?? []}
              />
              <StyledFormHelperText>
                {t(
                  'These are the single choice elements the Supplier can choose from.'
                )}
              </StyledFormHelperText>
            </div>
          )}
          <StyledSaveButton type="submit" color="secondary">
            {t('Save')}
          </StyledSaveButton>
        </Box>
      </form>
    </Dialog>
  );
};
