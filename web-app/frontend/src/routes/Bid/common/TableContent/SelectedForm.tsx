// @ts-nocheck
import React from 'react';
import styled from 'styled-components';

import { colors } from '@utils/uiTheme';

import { Box, MenuItem, TextField } from '@material-ui/core';
import { Text } from '@common/components/Text/Text';
import {
  Column,
  ColumnType,
} from '../../BidImporter/containers/BidCorrections/BidCorrections';
import { GridCellInfo } from '../GridCellInfo/GridCellInfo';

const getColor = (error, warning, change) => {
  if (error) {
    return colors.red;
  }

  if (warning) {
    return colors.warning;
  }

  if (change) {
    return colors.highlightBlue;
  }

  return '#66c600';
};

const StyledText = styled(Text)`
  height: 41px;
  margin: 16px 0 0;
  border-top: 1px solid rgba(151, 151, 151, 0.5);
  padding: 13px 0 0;
  font-size: 16px;
  line-height: 21px;
`;

const StyledTextField = styled(TextField)`
  && {
    .MuiFormLabel-root {
      color: ${props => props.color};
      font-weight: 500;
    }

    .MuiOutlinedInput-notchedOutline {
      border-color: ${props => props.color};
      border-radius: 9px;
    }

    .MuiOutlinedInput-root {
      &.Mui-focused {
        .MuiOutlinedInput-notchedOutline {
          border-color: ${props => props.color};
        }
      }
    }
  }
`;

type Coordinates = {
  i: number;
  j: number;
};
type GridCoordinates = {
  start: Coordinates;
  end: Coordinates;
};

interface Props {
  columnTypes: ColumnType[];
  columns: Column[];
  grid: any[][];
  projectCurrencies: string[];
  projectUnits: string[];
  selectedCells: GridCoordinates;
  setSelectedCellValue: (event: any, shouldValidate: boolean) => void;
}

const getCellInfo = (error, warning, change, cellValue) => {
  const gridInfoArray = [];

  if (error) {
    gridInfoArray.push({
      color: colors.red,
      value: error,
    });
  }

  if (warning) {
    gridInfoArray.push({
      color: colors.warning,
      value: warning,
    });
  }

  if (change) {
    gridInfoArray.push({
      color: colors.highlightBlue,
      value: `${change} => ${cellValue}`,
    });
  }

  return gridInfoArray;
};

export const SelectedCellForm: React.FC<Props> = ({
  selectedCells,
  columns,
  grid,
  setSelectedCellValue,
  columnTypes,
  projectCurrencies,
  projectUnits,
}) => {
  const { i: rowNumber, j: columnNumber } = selectedCells?.start;

  const { i: endRowNumber, j: endColumnNumber } = selectedCells?.end;

  const isRangeSelected =
    endRowNumber !== rowNumber || endColumnNumber !== columnNumber;

  const { singleChoice, limitMax, limitMin, typeId } =
    columns[columnNumber] || {};
  const { error, warning, isInvalid, value, change } = grid[rowNumber][
    columnNumber
  ] || {
    isInvalid: false,
    value: '',
  };

  const matchingType = columnTypes.find(type => type.id === typeId);

  const typeName = !!matchingType && matchingType.type;

  const isSingleChoice =
    typeName === 'single_choice' ||
    typeName === 'unit' ||
    typeName === 'currency';

  const getChoices = () => {
    if (typeName === 'single_choice') {
      return singleChoice;
    }

    if (typeName === 'unit') {
      return projectUnits;
    }

    if (typeName === 'currency') {
      return projectCurrencies;
    }

    return [];
  };

  const getLabel = () => {
    if (isRangeSelected) {
      return '';
    }

    return matchingType ? matchingType.text : 'Matching type not found';
  };

  const choices = getChoices();

  const cellInfo = getCellInfo(error, warning, change, value);

  return (
    <>
      <Box mt={10} mb={10} height={80}>
        <StyledTextField
          label={getLabel()}
          value={isRangeSelected || !value ? '' : value}
          variant="outlined"
          fullWidth
          color={getColor(error, warning, change)}
          disabled={isRangeSelected}
          select={isSingleChoice}
          onChange={event => setSelectedCellValue(event, isSingleChoice)}
          onKeyPress={event => setSelectedCellValue(event, true)}
          InputProps={{
            onBlur: event => setSelectedCellValue(event, true),
          }}
        >
          {!!choices &&
            choices.map(choice => (
              <MenuItem key={choice} value={choice}>
                {choice}
              </MenuItem>
            ))}
        </StyledTextField>
        <StyledText color={isInvalid ? colors.red : colors.black}>
          {!!limitMax && `Max Value: ${limitMax}`}
          {limitMax && limitMin && ', '}
          {!!limitMin && `Min Value: ${limitMin}`}
        </StyledText>
      </Box>
      <Box height={40}>
        {cellInfo.length ? <GridCellInfo cellInfos={cellInfo} /> : null}
      </Box>
    </>
  );
};
