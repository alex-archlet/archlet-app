import styled from 'styled-components';
import { Scrollbars } from 'react-custom-scrollbars';

export const ScrollBars = styled(Scrollbars)`
  .scrollbar-view {
    display: flex;
  }
`;
