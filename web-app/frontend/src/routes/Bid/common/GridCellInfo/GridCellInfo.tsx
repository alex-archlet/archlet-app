import React from 'react';
import styled from 'styled-components';

import { useCustomTranslation } from '@utils/translate';
import { Text } from '@common/components/Text/Text';
import { colors } from '@utils/uiTheme';
import { Position } from 'react-data-grid/lib/types';
import { BidColumnFlatRow } from '@common/components/Grid/Grid';
import { BidColumn } from '../utils';

const StyledGridInfoText = styled.span`
  height: 41px;
  padding: 11px 0 0;
  font-size: 16px;
  line-height: 10px;
  margin-left: 5px;
  color: ${props => props.color};
`;

const StyledBoldText = styled.span`
  font-weight: 400;
`;

interface Props {
  currentCellInfo: Position;
  grid: BidColumnFlatRow[];
  columns: BidColumn[];
}

const getCellValueFromGridUsingPosition = (currentCell, grid, columns) => {
  const { idx, rowIdx } = currentCell;
  const currentRow: BidColumnFlatRow = grid[rowIdx] as BidColumnFlatRow;
  const currentColumn = columns[idx];
  if (currentColumn && currentRow) {
    return currentRow[currentColumn.id];
  }
  return null;
};

const getCellFromGridUsingPosition = (currentCell, grid, columns) => {
  const { idx, rowIdx } = currentCell;
  const currentRow: BidColumnFlatRow = grid[rowIdx] as BidColumnFlatRow;
  const currentColumn = columns[idx];
  const cellInfo = currentRow?.processing_messages[currentColumn.id];
  return cellInfo ?? {};
};

export const GridCellInfo: React.FC<Props> = ({
  currentCellInfo,
  grid,
  columns,
}) => {
  const { t } = useCustomTranslation();

  const { change, error, warning } = getCellFromGridUsingPosition(
    currentCellInfo,
    grid,
    columns
  );
  const value = getCellValueFromGridUsingPosition(
    currentCellInfo,
    grid,
    columns
  );
  // this is pipe from the python side and is need so the enums dont get delete in translation analytics
  const errorMessages = {
    OFFER_PROCESSING_ERROR_TYPE: t('OFFER_PROCESSING_ERROR_TYPE'),
    OFFER_PROCESSING_WARNING_EMPTY: t('OFFER_PROCESSING_WARNING_EMPTY'),
    OFFER_PROCESSING_WARNING_ZERO: t('OFFER_PROCESSING_WARNING_ZERO'),
    OFFER_PROCESSING_WARNING_OUTLIER: t('OFFER_PROCESSING_WARNING_OUTLIER'),
  };

  return (
    <>
      {currentCellInfo && (
        <Text>
          <StyledBoldText>Messages:</StyledBoldText>

          {error?.enum && (
            <StyledGridInfoText color={colors.error}>
              {errorMessages[error.enum]}
            </StyledGridInfoText>
          )}
          {warning?.enum && (
            <StyledGridInfoText color={colors.warning}>
              {errorMessages[warning.enum]}
            </StyledGridInfoText>
          )}
          {change && (
            <StyledGridInfoText color={colors.highlightBlue}>
              {`${change} => ${value}`}
            </StyledGridInfoText>
          )}
        </Text>
      )}
    </>
  );
};
