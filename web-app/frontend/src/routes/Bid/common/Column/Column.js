import React, {
  useRef,
  useState,
} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {
  useDrag,
  useDrop,
} from 'react-dnd';

import {
  Delete,
  DragIndicator,
  Edit,
} from '@material-ui/icons';

import { IconButton } from '@material-ui/core';

const COLUMN_ITEM_TYPE = 'column';

const StyledCell = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  width: 172px;
  height: 42px;
  margin: 0 -1px -1px 0;
  border: 1px solid #cdcdcd;
  padding: 10px 32px;
  background-color: #fff;
  color: #c2c2c2;
  font-size: 16px;
  white-space: nowrap;
`;

const StyledHeaderCell = styled(StyledCell)`
  position: relative;
  overflow: hidden;
  padding: 10px 5px 10px 10px;
  color: ${props => (props.headerColor || '#000000')};
  background-color: ${props => (props.headerBackgroundColor || '#fff')};
  outline: ${props => (`1px solid ${props.headerStrokeColor}` || undefined)};
  outline-offset: -2px;
  font-weight: 500;
  z-index: 1;
  text-overflow: ellipsis;
  white-space: nowrap;
`;

const StyledDragIndicatorWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 10px;
`;

const StyledDragIndicator = styled(DragIndicator)`
  color: #a5a5a5;
  stroke-width: 1.2px;
  stroke: white;
  cursor: move;
`;

const StyledLabelContainer = styled.div`
  padding-left: 10px;
  padding-top: 4px;
`;

const StyledButtonsContainer = styled.div`
  position: absolute;
  right: 1px;
  background-color: inherit;
`;

const StyledIconButton = styled(IconButton)`
  &&.MuiButtonBase-root {
    width: 30px;
    height: 30px;
    margin-right: 2px;
    padding: 2px;
  }
`;

const StyledDelete = styled(Delete)`
  &&.MuiSvgIcon-root {
    color: #acacac;
    font-size: 17px;
  }
`;

const StyledEdit = styled(Edit)`
  &&.MuiSvgIcon-root {
    color: #acacac;
    font-size: 17px;
  }
`;

export const Column = ({
  headerBackgroundColor,
  headerColor,
  headerStrokeColor,
  label,
  id,
  index,
  onMove,
  setDeleteColumnId,
  onEdit,
  isDefault,
  columnType,
}) => {
  const ref = useRef(null);

  const [isRemoveShown, setIsRemoveShown] = useState(false);

  const { example } = columnType;

  const [, drop] = useDrop({
    accept: COLUMN_ITEM_TYPE,
    hover(item, monitor) {
      if (!ref.current) {
        return;
      }
      const dragIndex = item.index;
      const hoverIndex = index;

      if (dragIndex === hoverIndex) {
        return;
      }
      const hoverBoundingRect = ref.current.getBoundingClientRect();
      const hoverMiddleX = (hoverBoundingRect.right - hoverBoundingRect.left) / 2;
      const clientOffset = monitor.getClientOffset();
      const hoverClientX = clientOffset.x - hoverBoundingRect.left;

      if (dragIndex < hoverIndex && hoverClientX < hoverMiddleX) {
        return;
      }

      if (dragIndex > hoverIndex && hoverClientX > hoverMiddleX) {
        return;
      }
      onMove(dragIndex, hoverIndex);
      // eslint-disable-next-line no-param-reassign
      item.index = hoverIndex; // mutating for performance's sake to avoid expensive index searches
    },
  });

  const [{ isDragging }, drag, preview] = useDrag({
    collect: monitor => ({
      isDragging: !!monitor.isDragging(),
    }),
    item: {
      id,
      index,
      type: COLUMN_ITEM_TYPE,
    },
  });

  preview(drop(ref));

  return (
    <div
      ref={ref}
      style={{
        opacity: isDragging ? 0 : 1,
      }}
    >
      <StyledHeaderCell
        onMouseEnter={() => setIsRemoveShown(true)}
        onMouseLeave={() => setIsRemoveShown(false)}
        headerBackgroundColor={headerBackgroundColor}
        headerColor={headerColor}
        headerStrokeColor={headerStrokeColor}
      >
        {onMove && (
          <StyledDragIndicatorWrapper ref={drag}>
            <StyledDragIndicator />
          </StyledDragIndicatorWrapper>
        )}
        <StyledLabelContainer>
          {label}
        </StyledLabelContainer>
        {isRemoveShown && (
          <StyledButtonsContainer>
            {onEdit && (
              <StyledIconButton
                aria-label="edit"
                onClick={() => onEdit(id)}
              >
                <StyledEdit />
              </StyledIconButton>
            )}
            {setDeleteColumnId && !isDefault && (
              <StyledIconButton
                aria-label="delete"
                onClick={() => setDeleteColumnId(id)}
              >
                <StyledDelete />
              </StyledIconButton>
            )}
          </StyledButtonsContainer>
        )}
      </StyledHeaderCell>
      {Array.isArray(example) ?
        example.map((exampleValue, cellIndex) => (
          // eslint-disable-next-line react/no-array-index-key
          <StyledCell key={cellIndex}>
            {exampleValue}
          </StyledCell>
        )) :
        [...Array(9)].map((cell, cellIndex) => (
          // eslint-disable-next-line react/no-array-index-key
          <StyledCell key={cellIndex}>
            {example}
          </StyledCell>
        ))}
    </div>
  );
};

const VALUE_TYPES = [PropTypes.string, PropTypes.number];

Column.propTypes = {
  columnType: PropTypes.shape({
    example: PropTypes.oneOfType([...VALUE_TYPES, PropTypes.arrayOf(PropTypes.oneOfType(VALUE_TYPES))]).isRequired,
  }),
  headerBackgroundColor: PropTypes.string,
  headerColor: PropTypes.string,
  headerStrokeColor: PropTypes.string,
  id: PropTypes.oneOfType(VALUE_TYPES).isRequired,
  index: PropTypes.number.isRequired,
  isDefault: PropTypes.bool,
  label: PropTypes.oneOfType(VALUE_TYPES).isRequired,
  onEdit: PropTypes.func,
  onMove: PropTypes.func,
  setDeleteColumnId: PropTypes.func,
};

Column.defaultProps = {
  columnType: {},
  headerBackgroundColor: undefined,
  headerColor: undefined,
  headerStrokeColor: undefined,
  isDefault: false,
  onEdit: undefined,
  onMove: undefined,
  setDeleteColumnId: undefined,
};
