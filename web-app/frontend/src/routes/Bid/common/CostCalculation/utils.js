import { isValidExpression } from '@utils/expressionParser';

const getItemFromValue = item => ({
  id: item,
  name: item,
  type: Number.isNaN(Number(item)) ? 'invalid' : 'customNumber',
});

const isUuid = str =>
  str.match(
    /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i
  );
const convertUuidToUnderscore = (str) => {
  if (isUuid(str)) {
    return str.replace(/[-]/g, '_');
  }

  return str;
};

export const componentsToExpression = calculationComponents =>
  calculationComponents
    .map((item) => {
      if (typeof item === 'string') {
        return item;
      }

      return convertUuidToUnderscore(item.id);
    })
    .join(' ');

// for verification, we can't have UUIDs in the formula, so we need to use the string based names
// aka get rid of everything that is not [a-zA-Z_+\-*/()]
const componentsToVerificationExpression = calculationComponents =>
  calculationComponents
    .map(item => item.name.trim().replace(/[^a-zA-Z_+\-*/()]/g, '_'))
    .join(' ');

export const validateCalculation = (calculationComponents) => {
  if (!calculationComponents) {
    return false;
  }

  const isAnyItemInvalid = calculationComponents.some(
    item => item.type === 'invalid'
  );

  if (isAnyItemInvalid) {
    return false;
  }

  const validationExpression = componentsToVerificationExpression(
    calculationComponents
  );

  return isValidExpression(validationExpression);
};

const getUnderscoredLookupMap = (options) => {
  const result = {};

  options.forEach((option) => {
    if (option.id) {
      result[convertUuidToUnderscore(option.id.toLowerCase())] = option;
    }
  });

  return result;
};

export const parseValueArray = (valueArray, validOptions) => {
  const valueFiltered = valueArray.filter(
    item => !(typeof item === 'string' && item.trim() === '')
  );

  const underscoredLookupMap = getUnderscoredLookupMap(validOptions);

  return valueFiltered.map((item) => {
    if (!item.type && !item.name) {
      const matchingOption = underscoredLookupMap[item.trim().toLowerCase()];

      if (matchingOption) {
        return matchingOption;
      }

      return getItemFromValue(item);
    }

    return item;
  });
};

export const parseStringToColumnIds = str =>
  str
    .split(/([+\-*/() ])/)
    .filter(s => s.length > 0)
    .map(s => s.trim());
