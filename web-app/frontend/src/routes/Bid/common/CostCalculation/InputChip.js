import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { colors } from '@utils/uiTheme';
import { Chip } from '@material-ui/core';
import { Close } from '@material-ui/icons';

export const StyledChip = styled(Chip)`
  && {
    position: relative;
    width: 108px;
    height: 35px;
    box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.19);
    margin: 0 4px 0 0;
    border-radius: 8px;
    background-color: rgba(255, 255, 255, 1);
    font-size: 14px;
    line-height: 35px;
    text-align: center;
    user-select: none;
  }

  .MuiChip-deleteIcon {
    position: absolute;
    top: -6px;
    right: -16px;
    z-index: 1;
    opacity: 0;
    fill: #999;
    height: 20px;
  }

  &:hover {
    .MuiChip-deleteIcon {
      opacity: 1;
    }
  }
`;

export const StyledOperationChip = styled(
  ({ isBiggerFont: _isBiggerFont, ...props }) => <StyledChip {...props} />
)`
  && {
    width: 44px;
    color: #ffc100;
    font-size: ${props => (props.isBiggerFont ? '28px' : '21px')};

    .MuiChip-label {
      display: flex;
      justify-content: center;
      align-items: center;
    }
  }
`;

const StyledInvalidChip = styled(StyledChip)`
  && {
    color: ${colors.red};
  }
`;

export const InputChip = ({ option, index, getTagProps }) => {
  if (option.type === 'customNumber') {
    return <StyledChip {...getTagProps({ index })} label={option.name} />;
  }

  if (option.type === 'column') {
    return <StyledChip {...getTagProps({ index })} label={option.name} />;
  }

  if (option.type === 'operation') {
    let label = option.name;

    if (option.name === '-') {
      label = <span>&ndash;</span>;
    } else if (option.name === '*') {
      label = <Close />;
    }

    return (
      <StyledOperationChip
        {...getTagProps({ index })}
        isBiggerFont={option.name === '+' || option.name === '-'}
        label={label}
      />
    );
  }

  if (option.type === 'invalid') {
    return (
      <StyledInvalidChip {...getTagProps({ index })} label={option.name} />
    );
  }

  return (
    <StyledInvalidChip {...getTagProps({ index })} label={String(option)} />
  );
};

InputChip.propTypes = {
  getTagProps: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired,
  option: PropTypes.shape({
    name: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
  }).isRequired,
};
