import React from 'react';
import { Autocomplete, AutocompleteChangeReason } from '@material-ui/lab';
import { TextField } from '@material-ui/core';

import { useCustomTranslation } from '@utils/translate';

import { InputChip } from './InputChip';
import { BidColumn } from '../utils';

export type ValidElement = {
  id: string;
  name: string;
  type: 'operation' | 'column';
  column?: BidColumn;
};

interface Props {
  elements: ValidElement[];
  isValid: boolean | null;
  onChange: (
    event: React.ChangeEvent<{}>,
    value: (string | ValidElement)[],
    reason: AutocompleteChangeReason
  ) => void;
  value: ValidElement[];
}

export const CostCalculationExpressionBar: React.FC<Props> = ({
  elements,
  isValid,
  onChange,
  value,
}) => {
  const { t } = useCustomTranslation();

  return (
    <>
      <Autocomplete
        autoSelect
        multiple
        freeSolo
        disableClearable
        options={elements}
        getOptionSelected={() => false}
        getOptionLabel={option => option.name}
        renderTags={(val, getTagProps) =>
          val.map((option, index) => (
            <InputChip
              // eslint-disable-next-line react/no-array-index-key
              key={index}
              option={option}
              index={index}
              getTagProps={getTagProps}
            />
          ))
        }
        disablePortal
        renderInput={params => (
          <TextField
            {...params}
            error={!isValid}
            variant="outlined"
            placeholder={t('Add component')}
            helperText={
              !isValid
                ? t('Your cost calculation is not valid. Please correct it.')
                : ''
            }
          />
        )}
        value={value || []}
        onChange={onChange}
      />
    </>
  );
};
