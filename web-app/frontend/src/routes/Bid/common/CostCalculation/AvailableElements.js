import React from 'react';
import { Close } from '@material-ui/icons';
import { useCustomTranslation } from '@utils/translate';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const StyledContainer = styled.div`
  margin: 35px 0 45px;
  border-bottom: 1px solid rgba(151, 151, 151, 0.48);
  padding-bottom: 20px;
`;

const StyledHeading = styled.h3`
  margin: 0 0 14px;
  color: rgba(44, 44, 44, 0.6);
  font-weight: normal;
  font-size: 14px;
  line-height: 19px;
`;

const StyledElementsContainer = styled.div`
  display: flex;
  width: 373px;
  margin-bottom: 28px;
`;

const StyledColumnElementContainer = styled.div`
  flex-wrap: wrap;
  display: flex;
  margin-bottom: 28px;
`;

const StyledElement = styled.span`
  height: 35px;
  box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.19);
  margin: 0 8px 8px 0;
  border-radius: 8px;
  background-color: rgba(255, 255, 255, 1);
  font-size: 14px;
  line-height: 35px;
  text-align: center;
  user-select: none;
`;

const StyledColumnElement = styled(StyledElement)`
  overflow: hidden;
  padding: 0 15px;
  text-overflow: ellipsis;
  white-space: nowrap;
  cursor: pointer;
`;

const StyledOperationsContainer = styled.div`
  display: flex;
`;

const StyledOperationsGroup = styled.div`
  margin-right: 43px;
`;

const StyledOperationElement = styled(({
  isBiggerFont: _isBiggerFont,
  ...props
}) => <StyledElement {...props} />)`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 44px;
  color: #ffc100;
  font-size: ${props => (props.isBiggerFont ? '28px' : '21px')};
  cursor: pointer;
`;

const DIVIDE = {
  id: '/',
  name: '/',
  type: 'operation',
};
const MULTIPLY = {
  id: '*',
  name: '*',
  type: 'operation',
};
const ADD = {
  id: '+',
  name: '+',
  type: 'operation',
};
const SUBTRACT = {
  id: '-',
  name: '-',
  type: 'operation',
};
const PARENTHESIS_START = {
  id: '(',
  name: '(',
  type: 'operation',
};
const PARENTHESIS_END = {
  id: ')',
  name: ')',
  type: 'operation',
};

export const ALLOWED_OPERATIONS = [
  DIVIDE,
  MULTIPLY,
  ADD,
  SUBTRACT,
  PARENTHESIS_START,
  PARENTHESIS_END,
];

export const AvailableElements = ({
  columns,
  onClick,
}) => {
  const { t } = useCustomTranslation();

  return (
    <StyledContainer>
      <StyledHeading>
        {t('Available Bid Sheet Column Headers')}
      </StyledHeading>
      <StyledColumnElementContainer>
        {columns.map(column => (
          <StyledColumnElement
            key={column.id}
            onClick={() => onClick(({
              column,
              name: column.name,
              type: 'column',
            }))}
          >
            {column.name}
          </StyledColumnElement>
        ))}
      </StyledColumnElementContainer>
      <StyledElementsContainer>
        <StyledOperationsGroup>
          <StyledHeading>
            {t('Divide, Multiply')}
          </StyledHeading>
          <StyledOperationsContainer>
            <StyledOperationElement onClick={() => onClick(DIVIDE)}>
              /
            </StyledOperationElement>
            <StyledOperationElement onClick={() => onClick(MULTIPLY)}>
              <Close />
            </StyledOperationElement>
          </StyledOperationsContainer>
        </StyledOperationsGroup>
        <StyledOperationsGroup>
          <StyledHeading>
            {t('Add, Subtract')}
          </StyledHeading>
          <StyledOperationsContainer>
            <StyledOperationElement
              isBiggerFont
              onClick={() => onClick(ADD)}
            >
              +
            </StyledOperationElement>
            <StyledOperationElement
              isBiggerFont
              onClick={() => onClick(SUBTRACT)}
            >
              &ndash;
            </StyledOperationElement>
          </StyledOperationsContainer>
        </StyledOperationsGroup>
        <StyledOperationsGroup>
          <StyledHeading>
            {t('Group')}
          </StyledHeading>
          <StyledOperationsContainer>
            <StyledOperationElement onClick={() => onClick(PARENTHESIS_START)}>
              (
            </StyledOperationElement>
            <StyledOperationElement onClick={() => onClick(PARENTHESIS_END)}>
              )
            </StyledOperationElement>
          </StyledOperationsContainer>
        </StyledOperationsGroup>
      </StyledElementsContainer>
    </StyledContainer>
  );
};

AvailableElements.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  onClick: PropTypes.func.isRequired,
};
