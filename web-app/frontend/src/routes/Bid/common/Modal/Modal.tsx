import React from 'react';
import styled from 'styled-components';

import { Close } from '@material-ui/icons';
import { Dialog, IconButton } from '@material-ui/core';

const StyledIconButton = styled(IconButton)`
  &&.MuiIconButton-root {
    position: absolute;
    top: 3px;
    right: 3px;
    flex: 0;
  }
`;

interface Props {
  isOpen: boolean;
  handleClose: () => void;
  children: React.ReactNode;
  maxWidth?: false | 'xs' | 'sm' | 'md' | 'lg' | 'xl';
  fullWidth?: boolean;
}

export const Modal: React.FC<Props> = ({
  isOpen,
  handleClose,
  children,
  maxWidth,
  fullWidth,
}) => (
  <Dialog
    open={isOpen}
    onClose={handleClose}
    aria-labelledby="form-dialog-title"
    maxWidth={maxWidth}
    disableEscapeKeyDown
    fullWidth={fullWidth}
  >
    <StyledIconButton onClick={handleClose}>
      <Close />
    </StyledIconButton>
    {children}
  </Dialog>
);
