import { BidColumnFlatRow } from '@common/components/Grid/Grid';
import { columnMatchingErrors } from './enum/errors';

const isUnmatchedColumn = ({ matching }) => matching === false;
const getUnmatchedColumns = matchings => matchings.filter(isUnmatchedColumn);

export type BidColumn = {
  id: string;
  name: string;
  description?: string | null;
  calculation: string | null;
  isAutomatic: boolean;
  isMandatory: boolean;
  isVisible?: boolean;
  order?: number;
  limitMin?: number;
  limitMax?: number;
  singleChoice?: string[];
  inputBy: string;
  initialTypeId?: number | null;
  matchingConfidence?: number | null;
  initialMatchingConfidence?: number | null;
  createdAt?: Date | null;
  updatedAt?: Date | null;
  isDefault?: boolean;
  column: string;
  typeId?: string;
  isNumeric?: boolean | null;
};

type ExcelColumnToBidColumnMap = Record<string, BidColumn[]>;

type ErrorResult = Record<string, string>;

const getErrorsForSingleFile = (file) => {
  const { matchings } = file;
  const result: ErrorResult = {};

  if (!matchings) {
    return {};
  }

  const unmatchedColumns = getUnmatchedColumns(matchings.matchings);

  // 1. Add error to columns with no mapping
  unmatchedColumns.forEach(({ bid_column }) => {
    result[bid_column] = columnMatchingErrors.NO_MATCH;
  });

  // excel column name => { <bid_col_id>: error }
  const excelColumnToBidColumnMap: ExcelColumnToBidColumnMap = {};

  matchings.matchings.forEach(({ excel_column, bid_column }) => {
    if (!excelColumnToBidColumnMap[excel_column]) {
      excelColumnToBidColumnMap[excel_column] = [];
    }
    excelColumnToBidColumnMap[excel_column].push(bid_column);
  });
  // 2. Add error to columns with a duplicated mapping
  Object.values(excelColumnToBidColumnMap).forEach((bidColumnArray) => {
    if (bidColumnArray.length > 1) {
      bidColumnArray.forEach((bidColumn) => {
        result[bidColumn.id] = columnMatchingErrors.DUPLICATE_MATCH;
      });
    }
  });

  return result;
};

export const getErrorsForAllFiles = (files) => {
  const filesMap = {};

  files.forEach((file) => {
    filesMap[file.id] = getErrorsForSingleFile(file);
  });

  return filesMap;
};

export const calculateParsedOffersPayload = (data: any[]) => {
  if (!data) {
    return [];
  }
  return data.map((row) => {
    return {
      ...row,
      // this method takes in both demands and historic offers, so that's why we check for both
      ...(row?.demand?.attributes ?? {}),
      ...row.attributes,
    };
  });
};

const numberValidator = (value, min = 0, max) => {
  if (!parseInt(value, 10)) {
    return true;
  }

  if (max) {
    return !(value >= min && value <= max);
  }

  return !(value > min);
};

export const checkCellForErrors = (
  value: string | number,
  column: BidColumn,
  row: BidColumnFlatRow
): BidColumnFlatRow => {
  const { limitMax, limitMin, column: columnType } = column;

  const newRow: BidColumnFlatRow = JSON.parse(JSON.stringify(row));

  const setError = (errorMessage: string | null) => {
    const currentRow = newRow.processing_messages[column.id];
    if (currentRow?.error?.enum) {
      currentRow.error.enum = errorMessage;
    } else {
      newRow.processing_messages = {
        [column.id]: {
          error: {
            enum: errorMessage,
          },
        },
      };
    }
  };

  const setWarning = (warningMessage: string | null) => {
    const currentRow = newRow.processing_messages[column.id];
    if (currentRow.warning?.enum) {
      currentRow.warning.enum = warningMessage;
    } else {
      newRow.processing_messages = {
        [column.id]: {
          warning: {
            enum: warningMessage,
          },
        },
      };
    }
  };
  setError('');
  setWarning('');

  if (value === '') {
    setWarning('OFFER_PROCESSING_WARNING_EMPTY');
  } else if (
    value &&
    ['number', 'volume', 'price', 'price_total'].includes(columnType) &&
    (value === '0' || value === 0)
  ) {
    setWarning('OFFER_PROCESSING_WARNING_ZERO');
  } else if (
    value &&
    ['number', 'volume', 'price', 'price_total'].includes(columnType) &&
    numberValidator(value, limitMin, limitMax)
  ) {
    setError('OFFER_PROCESSING_ERROR_TYPE');
  }

  return newRow;
};
