export const columnMatchingErrors = {
  // if an excel column has been selected multiple times, which is not allowed
  DUPLICATE_MATCH: 'DUPLICATE_MATCH',
  // if an excel column matching has not been found
  NO_MATCH: 'NO_MATCH',
};

export const skuGridSubmissionsErrors = {
  NOTHING_TO_SUBMIT: 'Nothing to submit',
};
