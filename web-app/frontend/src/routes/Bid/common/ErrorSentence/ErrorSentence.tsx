import React from 'react';
import styled from 'styled-components';

const StyledContainer = styled.div`
  width: 100%;
  border: red 1px solid;
  padding: 20px 20px;
`;

const StyledText = styled.p`
  color: red;
`;

interface Props {
  errorFields: string[]
}

export const ErrorSentence: React.FC<Props> = ({ errorFields }) => (
  <StyledContainer>
    <StyledText>
      These fields
      {' '}
      {errorFields.map(errorField => <span>{`${errorField},`}</span>)}
      {' '}
      are required for continuing
    </StyledText>

  </StyledContainer>
);
