import React from 'react';
import styled from 'styled-components';

import { useCustomTranslation } from '@utils/translate';

const StyledStatus = styled.div`
  border-radius: 3px;
  border: 2px solid #F7B500;
  color: #F7B500;
  font-size: 11px;
  line-height: 26px;
  font-weight: 400;
  text-align: center;
  width: 84px;
  height: 30px;
`;

const StyledStatusActive = styled(StyledStatus)`
  border-color: #57B4D3;
  color: #57B4D3;
`;

const StyledStatusCancelled = styled(StyledStatus)`
  border-color: #E64D4D;
  color: #E64D4D;
`;

const StyledStatusFinished = styled(StyledStatus)`
  border-color: #3CBA54;
  color: #2EB67D;
`;

interface Props {
  status: string,
}

export const StatusItem: React.FC<Props> = ({
  status,
}) => {
  const { t } = useCustomTranslation();

  const translationStringsMap = {
    Active: t('Active'),
    Cancelled: t('Cancelled'),
    Draft: t('Draft'),
    Finished: t('Finished'),
    placeholder: t('status'),
  };

  const translationComponentsMap = {
    Active: StyledStatusActive,
    Cancelled: StyledStatusCancelled,
    Draft: StyledStatus,
    Finished: StyledStatusFinished,
    placeholder: StyledStatus,
  };

  const Component = translationComponentsMap[status];
  const text = translationStringsMap[status];

  if (!Component || !text) {
    return null;
  }

  return (
    <Component>
      {text}
    </Component>
  );
};
