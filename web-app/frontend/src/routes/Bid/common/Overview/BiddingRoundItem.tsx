import React from 'react';
import styled from 'styled-components';
import { Card, Grid } from '@material-ui/core';

import { colors } from '@utils/uiTheme';
import { BiddingRoundListModel } from '@store/reducers/biddingRounds';
import { useCustomTranslation } from '@utils/translate';
import { PrimaryButton } from '@common/components/Button/PrimaryButton';

import { RoundTitle } from './RoundTitle';
import { StatusItem } from './StatusItem';

const StyledCard = styled(({ isPlaceholder: _, ...props }) => (
  <Card {...props} />
))`
  && {
    margin: 12px 0;
    border-color: ${colors.grey};
    padding: 24px 28px;
    color: ${colors.black};
    font-weight: bold;
    font-size: 18px;
    line-height: 21px;
    ${props =>
      !props.isPlaceholder &&
      `&:hover {
      box-shadow: 0 2px 10px 0 rgba(10, 22, 70, 0.1);
      cursor: pointer;
    }`}
  }
`;

const defaultOnClick = () => {};

interface Props {
  round: BiddingRoundListModel | null;
  onClick?: (id: string) => void;
}

const PLACEHOLDER_ROUND = {
  createdAt: null,
  id: '1',
  name: 'Bidding Round',
  offersCount: null,
  statusName: 'Draft',
  suppliersCount: null,
};

export const BiddingRoundItem: React.FC<Props> = ({
  onClick = defaultOnClick,
  round,
}) => {
  const { t } = useCustomTranslation();

  const { statusName, name, id, offersCount, suppliersCount, createdAt } =
    round || PLACEHOLDER_ROUND;

  const isPlaceholder = round === null;
  const placeholder = <span>&ndash;</span>;

  return (
    <StyledCard
      elevation={0}
      isPlaceholder={isPlaceholder}
      onClick={() => onClick(id)}
    >
      <Grid spacing={5} justify="center" alignItems="center" container>
        <Grid item xs={4}>
          <RoundTitle name={name} createdAt={createdAt} />
        </Grid>

        <Grid item xs={2} justify="center" container>
          <StatusItem status={statusName} />
        </Grid>
        <Grid item xs={2} justify="center" container>
          {!isPlaceholder && offersCount ? offersCount : placeholder}
        </Grid>
        <Grid item xs={2} justify="center" container>
          {!isPlaceholder && suppliersCount ? suppliersCount : placeholder}
        </Grid>
        <Grid item xs={2} justify="center" container>
          <PrimaryButton onClick={() => onClick(id)}>
            {t('Review')}
          </PrimaryButton>
        </Grid>
      </Grid>
    </StyledCard>
  );
};

BiddingRoundItem.defaultProps = {
  onClick: defaultOnClick,
  round: null,
};
