import React from 'react';
import styled from 'styled-components';

import { useCustomTranslation } from '@utils/translate';
import { convertDateWithMonthName } from '@utils/convertDate';

const StyledText = styled.span`
  opacity: 0.5;
  color: #000;
  font-size: 12px;
  font-weight: 500;
`;

interface Props {
  date: string,
}

export const CreatedOn: React.FC<Props> = ({
  date,
}) => {
  const { t } = useCustomTranslation();
  const dateString = convertDateWithMonthName(date);

  return (
    <StyledText>
      {t('Created on {{dateString}}', { dateString })}
    </StyledText>
  );
};
