import React from 'react';
import styled from 'styled-components';

import { CreatedOn } from './CreatedOn';

const StyledTitle = styled.div`
  line-height: 21px;
  font-size: 18px;
  color: #000;
  margin-bottom: 8px;
`;

interface Props {
  name: string,
  createdAt: string | null,
}

export const RoundTitle: React.FC<Props> = ({
  name,
  createdAt,
}) => (
  <div>
    <StyledTitle>
      {name}
    </StyledTitle>
    <div>
      {createdAt && <CreatedOn date={createdAt} />}
    </div>
  </div>
);
