import React, { useCallback, useMemo } from 'react';
import styled from 'styled-components';
import {
  Grid,
  TableSortLabel,
} from '@material-ui/core';
import { useCustomTranslation } from '@utils/translate';
import { BiddingRoundListModel } from '@store/reducers/biddingRounds';

import { BiddingRoundItem } from './BiddingRoundItem';

const StyledTableHeader = styled.div`
  padding: 2px 24px;
  color: #6d7278;
`;

const StyledTableSortLabel = styled(TableSortLabel)`
  &&.MuiTableSortLabel-root{
    /* margin is to center the text (there is a sort arrow on the right of size 18px) */
    margin-left: 18px;
  }
`;

type OrderType = 'asc' | 'desc' | undefined;

interface SortingProps {
  column: string,
  order: OrderType,
}

interface Props {
  onRoundClick: (id: string) => void,
  onSetSortProperties: (props: SortingProps) => void,
  rounds: BiddingRoundListModel[],
  sortProperties: SortingProps,
}

export const BiddingRoundsTable: React.FC<Props> = ({
  rounds,
  onRoundClick,
  onSetSortProperties,
  sortProperties,
}) => {
  const { t } = useCustomTranslation();

  const {
    order,
    column: orderBy,
  } = sortProperties;

  const handleRequestSort = (property: string) => {
    const isDesc = orderBy === property && order === 'desc';
    const newOrder = isDesc ? 'asc' : 'desc';

    onSetSortProperties({
      column: property,
      order: newOrder,
    });
  };

  const sortFunction = useCallback((itemA, itemB) => {
    const multiplier = order === 'asc' ? 1 : -1;

    const valueA = itemA[orderBy];
    const valueB = itemB[orderBy];

    if (valueA < valueB) return -1 * multiplier;

    if (valueA > valueB) return 1 * multiplier;

    return 0;
  }, [orderBy, order]);

  const sortedData = useMemo(() => {
    if (!rounds || !rounds.length) {
      return [];
    }

    return [...rounds].sort(sortFunction);
  }, [rounds, sortFunction]);

  return (
    <>
      <StyledTableHeader>
        <Grid
          container
          spacing={5}
          alignItems="center"
        >
          <Grid
            xs={4}
            item
          >
            <StyledTableSortLabel
              active={orderBy === 'createdAt'}
              direction={order}
              onClick={() => handleRequestSort('createdAt')}
            >
              {t('Bidding Round')}
            </StyledTableSortLabel>
          </Grid>
          <Grid
            item
            xs={2}
            justify="center"
            container
          >
            <StyledTableSortLabel
              active={orderBy === 'statusName'}
              direction={order}
              onClick={() => handleRequestSort('statusName')}
            >
              {t('Status')}
            </StyledTableSortLabel>
          </Grid>
          <Grid
            item
            xs={2}
            justify="center"
            container
          >
            <StyledTableSortLabel
              active={orderBy === 'offersCount'}
              direction={order}
              onClick={() => handleRequestSort('offersCount')}
            >
              {t('Collected Bids')}
            </StyledTableSortLabel>
          </Grid>
          <Grid
            item
            xs={2}
            justify="center"
            container
          >
            <StyledTableSortLabel
              active={orderBy === 'suppliersCount'}
              direction={order}
              onClick={() => handleRequestSort('suppliersCount')}
            >
              {t('Bidding Suppliers')}
            </StyledTableSortLabel>
          </Grid>
          <Grid
            item
            xs={2}
            justify="center"
            container
          />
        </Grid>
      </StyledTableHeader>
      {sortedData && sortedData.length ?
        sortedData.map(round => (
          <BiddingRoundItem
            key={round.id}
            round={round}
            onClick={onRoundClick}
          />
        )) :
        (
          <BiddingRoundItem round={null} />
        )}
    </>
  );
};
