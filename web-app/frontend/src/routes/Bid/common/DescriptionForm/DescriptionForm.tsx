import React from 'react';
import styled from 'styled-components';

import { Box } from '@material-ui/core';
import { useCustomTranslation } from '@utils/translate';

import { Input } from '@common/components/Input/Input';
import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';

const StyledLabel = styled.h3`
  margin: 24px 0 0;
  font-weight: 500;
  font-size: 14px;
  line-height: 19px;
`;

const StyledBox = styled(Box)`
  padding: 64px 110px 0 142px;
`;

export const DescriptionForm = ({
  values,
  onChange,
  errors,
  setFieldTouched,
}) => {
  const { t } = useCustomTranslation();
  return (
    <StyledBox>
      <StyledLabel>{t('Bidding Round Name')}</StyledLabel>
      <VerticalSpacer height={5} />

      <Input
        value={values.name}
        placeholder={t('e.g. LogisticsTender_2021_Round_01')}
        onChange={value => onChange('name', value)}
        onBlur={() => setFieldTouched('name')}
        error={Boolean(errors.name)}
        helperText={errors.name}
        height="19px"
        bidImporterStyle
      />

      <StyledLabel>{t('Description (1000 characters max)')}</StyledLabel>
      <VerticalSpacer height={5} />
      <Input
        value={values.description}
        onChange={value => onChange('description', value)}
        onBlur={() => setFieldTouched('description')}
        error={Boolean(errors.description)}
        helperText={errors.description}
        placeholder={t(
          'The uploaded data contains the offers from all suppliers in this tender'
        )}
        multiline
        rows={6}
        bidImporterStyle
      />
    </StyledBox>
  );
};
