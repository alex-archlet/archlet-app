import React from 'react';
import { useHistory } from 'react-router';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Box } from '@material-ui/core';
import { ArrowForward } from '@material-ui/icons';
import { colors } from '@utils/uiTheme';
import { Text } from '@common/components/Text/Text';

const StyledNextButton = styled.div`
  display: flex;
  align-items: center;
  font-size: 16px;
  cursor: pointer;
`;

const StyledBox = styled(Box)`
  width: 29px;
  height: 29px;
  margin-left: 11px;
  border: 2px solid #6bcae5;
  border-radius: 7px;
  font-size: 18px;
  line-height: 30px;
  text-align: center;
`;

export const NextButton = ({ label, url, action, stepError }) => {
  const history = useHistory();

  const onClick = () => {
    if (action) {
      return action();
    }

    if (url) {
      return history.push(url);
    }

    return null;
  };

  return (
    <Box display="flex" flexDirection="column" alignItems="flex-end">
      <StyledNextButton onClick={onClick}>
        <span>{label}</span>
        <StyledBox>
          <ArrowForward fontSize="inherit" />
        </StyledBox>
      </StyledNextButton>
      {!!stepError && <Text color={colors.red}>{stepError}</Text>}
    </Box>
  );
};

NextButton.propTypes = {
  action: PropTypes.func,
  history: PropTypes.shape({}),
  label: PropTypes.string,
  stepError: PropTypes.string,
  url: PropTypes.string,
};

NextButton.defaultProps = {
  action: () => {},
  history: {},
  label: 'Next Step',
  stepError: '',
  url: '',
};
