import React from 'react';
import styled from 'styled-components';

import { Box } from '@material-ui/core';
import { NextButton } from './NextButton';

const StyledTitle = styled.h2`
  margin: 30px 0 8px;
  font-size: 26px;
`;

const StyledText = styled.p`
  margin: 0;
  color: #999;
  font-size: 14px;
  line-height: 19px;
`;

interface Props {
  title: string;
  textUnderTitle?: string;
  nextButtonLabel?: string;
  nextButtonUrl?: string | null;
  nextButtonAction?: () => void | null;
  stepError?: string | null;
  nextButton?: React.ReactNode;
}

export const RoundHeader: React.FC<Props> = ({
  title,
  textUnderTitle,
  nextButtonLabel,
  nextButtonUrl,
  nextButton,
  nextButtonAction,
  stepError,
}) => (
  <Box
    width="100%"
    display="flex"
    justifyContent="space-between"
    alignItems="flex-end"
    mb={10}
  >
    <Box>
      <StyledTitle>{title}</StyledTitle>
      <StyledText>{textUnderTitle}</StyledText>
    </Box>
    {nextButton ||
      (nextButtonLabel && (
        <NextButton
          label={nextButtonLabel}
          url={nextButtonUrl}
          action={nextButtonAction}
          stepError={stepError}
        />
      ))}
  </Box>
);
