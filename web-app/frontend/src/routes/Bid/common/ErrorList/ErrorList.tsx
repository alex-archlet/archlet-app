import React from 'react';
import styled from 'styled-components';

const StyledContainer = styled.div`
  width: 100%;
  border: red 1px solid;
  padding: 20px 20px;
`;

const StyledText = styled.p`
  color: red;
`;

const StyledListItem = styled.li`
  color: red;
`;

interface ErrorFields {
  [key: string]: string[],
}

interface Props {
  errorFields: ErrorFields[]
  title: string
}

export const ErrorList: React.FC<Props> = ({ errorFields, title }) => (
  <StyledContainer>
    <StyledText>
      {title}
    </StyledText>
    <ul>
      {errorFields.map(errorRow => Object.values(errorRow).map(error => (
        <StyledListItem>
          {error}
        </StyledListItem>
      )))}
    </ul>
  </StyledContainer>
);
