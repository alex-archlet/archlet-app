import { useEffect, useState } from 'react';
import request from '@utils/request';
import queryString from 'query-string';

type SheetsState = {
  sheetNames: string[];
  sheets: {
    [key: string]: {
      data: object[];
      headers: string[];
    };
  };
};

const INITIAL_SHEETS_DATA: SheetsState = {
  sheetNames: [],
  sheets: {},
};

type PreviewState = {
  sheet: string | null;
  startIndex: number;
};

const INITIAL_PREVIEW_DATA: PreviewState = {
  sheet: null,
  startIndex: 1,
};

type ExcelPreviewOptions = {
  row?: number;
  type?: null | 'records';
  sheetName?: string;
};

export const useExcelPreview = (
  fileId: string | null,
  isActive: boolean,
  excelPreviewOptions: ExcelPreviewOptions
) => {
  const [tableData, setTableData] = useState(INITIAL_SHEETS_DATA);
  const [previewState, setPreviewState] = useState(INITIAL_PREVIEW_DATA);

  useEffect(() => {
    if (isActive && fileId) {
      const excelPreviewOptionsUrl = queryString.stringify(excelPreviewOptions);
      const fetchRawPreview = async () => {
        try {
          const result = await request.get(
            `files/xlsx_parse/${fileId}?${excelPreviewOptionsUrl}`
          );

          const { data } = result;

          setPreviewState({
            ...previewState,
            sheet: data.sheetNames[0],
          });
          setTableData({
            ...tableData,
            ...data,
          });
        } catch (error) {
          setTableData(INITIAL_SHEETS_DATA);
        }
      };

      fetchRawPreview();
    } else {
      setTableData(INITIAL_SHEETS_DATA);
    }
  }, [fileId, isActive]);

  return {
    previewState,
    setPreviewState,
    tableData,
  };
};
