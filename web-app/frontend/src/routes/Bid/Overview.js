import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { useHistory, useParams, useRouteMatch } from 'react-router-dom';
import PropTypes from 'prop-types';
import { useCustomTranslation } from '@utils/translate';

import { Add } from '@material-ui/icons';

import { projectsSelectors } from '@selectors/projects';
import { projectDataSelectors } from '@selectors/projectData';

import { getProjectDetails } from '@actions/projects';
import { createRound, getRoundsList } from '@actions/biddingRounds';
import { biddingRoundsSelectors } from '@selectors/biddingRounds';

import { setBackButton, setPageTitle } from '@actions/interface';
import { bindActionCreators } from 'redux';

import { PrimaryButton } from '@common/components/Button/PrimaryButton';

import { ProjectsLayout } from '../ProjectDetails/components/ProjectsLayout/ProjectsLayout';
import { BiddingRoundsTable } from './common/Overview/BiddingRoundsTable';

const StyledContainer = styled.div`
  padding: 0 65px;
`;

const StyledTitle = styled.h2`
  margin: 48px 0 65px;
  font-size: 26px;
  display: flex;
  justify-content: space-between;
`;

const stripTrailingSlash = (str) => {
  if (str.substr(-1) === '/') {
    return str.substr(0, str.length - 1);
  }

  return str;
};

const Overview = ({
  match,
  getProjectDetailsAction,
  getRoundsListAction,
  createRoundAction,
  roundsList,
  setBackButtonAction,
  setPageTitleAction,
}) => {
  const { id: projectId } = useParams();
  const history = useHistory();
  const { url } = useRouteMatch();

  const { t } = useCustomTranslation();
  const [sortProperties, setSortProperties] = useState({
    column: 'createdAt',
    order: 'asc',
  });

  useEffect(() => {
    setBackButtonAction('arrow');
    getProjectDetailsAction(projectId).then(project =>
      setPageTitleAction(project.name)
    );
    getRoundsListAction(projectId);

    return () => setBackButtonAction('');
  }, [projectId]);

  return (
    <ProjectsLayout match={match}>
      <StyledContainer>
        <StyledTitle>
          <span>{t('Bidding Rounds')}</span>
          <PrimaryButton
            startIcon={<Add />}
            onClick={async () => {
              const newRound = await createRoundAction(projectId, {
                name: '',
                description: '',
              });
              history.push(
                `${stripTrailingSlash(url)}/${newRound.id}/description`
              );
            }}
          >
            {t('Add Round')}
          </PrimaryButton>
        </StyledTitle>
        <BiddingRoundsTable
          rounds={roundsList}
          onRoundClick={id =>
            history.push(`${stripTrailingSlash(url)}/${id}/description`)
          }
          onSetSortProperties={setSortProperties}
          sortProperties={sortProperties}
        />
      </StyledContainer>
    </ProjectsLayout>
  );
};

Overview.propTypes = {
  getProjectDetailsAction: PropTypes.func.isRequired,
  getRoundsListAction: PropTypes.func.isRequired,
  match: PropTypes.shape({}).isRequired,
  roundsList: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  setBackButtonAction: PropTypes.func.isRequired,
  setPageTitleAction: PropTypes.func.isRequired,
  validation: PropTypes.shape({}).isRequired,
};

const mapStateToProps = state => ({
  projectDetails: projectsSelectors.getProjectDetails(state),
  roundsList: biddingRoundsSelectors.getRoundsList(state),
  validation: projectDataSelectors.getValidation(state),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getProjectDetailsAction: getProjectDetails,
      getRoundsListAction: getRoundsList,
      setBackButtonAction: setBackButton,
      setPageTitleAction: setPageTitle,
      createRoundAction: createRound,
    },
    dispatch
  );

export const OverviewContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Overview);
