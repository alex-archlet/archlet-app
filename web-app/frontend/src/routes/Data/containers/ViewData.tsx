import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { getData } from '@actions/projectData';
import { projectDataSelectors } from '@selectors/projectData';
import { setBackButton, setPageTitle } from '@actions/interface';
import { Box } from '@material-ui/core';
import { useCustomTranslation } from '@utils/translate';
import { CustomColumn, Table } from '@common/components/Table/Table';

const StyledTitle = styled.h2`
  margin: 48px 0 30px;
  font-size: 26px;
`;

const StyledContainer = styled.div`
  width: 100%;
  margin: 30px;
  overflow-x: auto;
  white-space: nowrap;
`;

type ObjectKeyPairValues = {
  key: string;
  value: string;
};

type KeyPairValues = {
  [key: string]: string;
};

type FileTypes = {
  [key: string]: { 
    example: string,
    mandatory: boolean,
    name: string
  };
};

interface Props {
  data: { headers: ObjectKeyPairValues[]; rows: KeyPairValues[] };
  filesTypes: FileTypes;
  getDataAction: Function;
  setBackButtonAction: Function;
  setPageTitleAction: Function;
  parentUrl: string;
}

const ViewData: React.FC<Props> = ({
  data,
  filesTypes,
  getDataAction,
  setBackButtonAction,
  setPageTitleAction,
  parentUrl,
}) => {
  const { type, id } = useParams();
  const { t } = useCustomTranslation();
  const title = Object.keys(filesTypes).length
    ? `${filesTypes[type].name} ${t('Data')}`
    : '';

  useEffect(() => {
    getDataAction(type, id);
    setBackButtonAction('close', `${parentUrl}/${type}`);
    setPageTitleAction(title);

    return () => setBackButtonAction('');
  }, [id, parentUrl, title, type]);

  const columns: CustomColumn[] = data?.headers?.map(column => ({
    Header: column.value,
    id: column.value,
    accessor: value => value[column.key],
  }));
  return (
    <StyledContainer>
      <StyledTitle>{title}</StyledTitle>
      <Box mb={70}>
        {Object.keys(data).length ? (
          <Table
            data={data.rows}
            columns={columns}
            getUniqueKey={(_, key) => key.toString()}
          />
        ) : null}
      </Box>
    </StyledContainer>
  );
};

const mapStateToProps = state => ({
  data: projectDataSelectors.getData(state),
  filesTypes: projectDataSelectors.getFilesTypes(state),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getDataAction: getData,
      setBackButtonAction: setBackButton,
      setPageTitleAction: setPageTitle,
    },
    dispatch
  );

export const ViewDataContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ViewData);
