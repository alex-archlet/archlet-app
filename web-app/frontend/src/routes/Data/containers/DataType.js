import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { bindActionCreators } from 'redux';
import { useHistory, useParams } from 'react-router-dom';
import PropTypes from 'prop-types';
import FileDownload from 'js-file-download';
import isEqual from 'lodash.isequal';

import { Delete, GetApp, RemoveRedEye } from '@material-ui/icons';
import { projectDataSelectors } from '@selectors/projectData';
import {
  createValidationJobAndGetProjectInfo,
  deleteData,
  getDataValidity,
  getProjectFiles,
  uploadFiles,
} from '@actions/projectData';
import { setBackButton, setPageTitle } from '@actions/interface';
import { Box } from '@material-ui/core';
import { Button } from '@common/components/Button/Button';
import { getCardStyle } from '@utils/getCardStyle';
import { getFile } from '@utils/getFile';
import { MarginWrapper } from '@common/components/MarginWrapper/MarginWrapper';
import { ErrorsList } from '@common/components/ErrorsList/ErrorsList';
import { ConfirmationDialog } from '@common/components/ConfirmationDialog/ConfirmationDialog';
import { ScenarioProgressContainer } from '@common/components/ScenarioProgress/ScenarioProgressContainer';
import { useCustomTranslation } from '@utils/translate';
import { UploadField } from '../components/ProjectData/UploadField';
import { UploadDetails } from '../components/ProjectData/UploadDetails';

const StyledTitle = styled.h2`
  margin: 48px 0 30px;
  font-size: 36px;
  text-transform: capitalize;
`;

const DataType = ({
  createValidationJobAndGetProjectInfoAction,
  deleteDataAction,
  files,
  filesTypes,
  getDataValidityAction,
  getProjectFilesAction,
  setBackButtonAction,
  setPageTitleAction,
  uploadFilesAction,
  validation,
  parentUrl,
}) => {
  const history = useHistory();
  const params = useParams();
  const { type } = params;
  const projectId = params.id;
  const { t } = useCustomTranslation();

  const StyledDesc = styled.p`
    margin: 0 0 0 10px;
  `;

  const StyledButton = styled(Button)`
    && {
      margin-left: 10px;
    }
  `;

  useEffect(() => {
    setBackButtonAction('close', parentUrl);
    getProjectFilesAction(projectId);

    return () => setBackButtonAction('');
  }, [parentUrl, projectId]);

  useEffect(() => {
    setPageTitleAction(
      filesTypes[type] ? `${filesTypes[type].name} ${t('Data')}` : ''
    );
    getDataValidityAction(projectId, filesTypes);
  }, [filesTypes, projectId, type]);

  const [isDialogVisible, setDialogVisible] = useState(false);

  const onDrop = async (formData) => {
    console.info(formData);
    const uploadedFiles = await uploadFilesAction(formData);
    const allFiles = {
      ...files,
      ...uploadedFiles,
    };

    const shouldValidateData = isEqual(
      Object.keys(allFiles).sort(),
      Object.keys(filesTypes).sort()
    );

    if (shouldValidateData) {
      await createValidationJobAndGetProjectInfoAction(
        projectId,
        allFiles,
        filesTypes
      );
    }
  };

  const deleteFile = () => {
    deleteDataAction(files[type]).then(() => {
      history.goBack();
    });
    setDialogVisible(false);
  };

  const downloadFile = file =>
    getFile(file.id).then(resp =>
      FileDownload(resp.data, `${file.name}${file.fileExtension}`)
    );

  const displayValidation = !!(
    validation[type] &&
    (validation[type].errors !== 0 || validation[type].warnings !== 0)
  );

  const file = files[type];

  return (
    <MarginWrapper size="small" display="flex" flexWrap="wrap">
      <ScenarioProgressContainer title={t('Validating data...')} />
      <ConfirmationDialog
        isOpen={isDialogVisible}
        handleClose={() => setDialogVisible(false)}
        handleConfirm={deleteFile}
        message={t('Do you really want to delete this file?')}
      />
      <StyledTitle>
        {filesTypes[type] && `${filesTypes[type].name} ${t('Data Upload')}`}
      </StyledTitle>
      <UploadField
        onDrop={onDrop}
        projectId={projectId}
        fileType={type}
        filesTypes={filesTypes}
      />
      <Box width="100%">
        {files && (
          <Box
            display="flex"
            alignItems="center"
            justifyContent="space-between"
          >
            {file && (
              <>
                <UploadDetails
                  file={file}
                  cardStyle={getCardStyle(validation[type])}
                />
                <Box>
                  <StyledButton
                    variant="outlined"
                    onClick={() => history.push(`${type}/view`)}
                  >
                    <RemoveRedEye />
                    <StyledDesc>{t('View')}</StyledDesc>
                  </StyledButton>
                  <StyledButton
                    variant="outlined"
                    onClick={() => downloadFile(file)}
                  >
                    <GetApp />
                    <StyledDesc>{t('Download')}</StyledDesc>
                  </StyledButton>
                  <StyledButton
                    variant="outlined"
                    onClick={() => setDialogVisible(true)}
                  >
                    <Delete />
                    <StyledDesc>{t('Delete')}</StyledDesc>
                  </StyledButton>
                </Box>
              </>
            )}
          </Box>
        )}
      </Box>
      {displayValidation && (
        <ErrorsList
          validation={validation[type]}
          cardStyle={getCardStyle(validation[type])}
        />
      )}
    </MarginWrapper>
  );
};

DataType.propTypes = {
  createValidationJobAndGetProjectInfoAction: PropTypes.func.isRequired,
  deleteDataAction: PropTypes.func.isRequired,
  files: PropTypes.shape({}).isRequired,
  filesTypes: PropTypes.shape({}).isRequired,
  getDataValidityAction: PropTypes.func.isRequired,
  getProjectFilesAction: PropTypes.func.isRequired,
  parentUrl: PropTypes.string.isRequired,
  setBackButtonAction: PropTypes.func.isRequired,
  setPageTitleAction: PropTypes.func.isRequired,
  uploadFilesAction: PropTypes.func.isRequired,
  validation: PropTypes.shape({}).isRequired,
};

const mapStateToProps = state => ({
  files: projectDataSelectors.getFiles(state),
  filesTypes: projectDataSelectors.getFilesTypes(state),
  validation: projectDataSelectors.getValidation(state),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      createValidationJobAndGetProjectInfoAction: createValidationJobAndGetProjectInfo,
      deleteDataAction: deleteData,
      getDataValidityAction: getDataValidity,
      getProjectFilesAction: getProjectFiles,
      setBackButtonAction: setBackButton,
      setPageTitleAction: setPageTitle,
      uploadFilesAction: uploadFiles,
    },
    dispatch
  );

export const DataTypeContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(DataType);
