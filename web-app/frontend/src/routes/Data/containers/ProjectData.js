/* eslint-disable max-len */
import React, { useEffect } from 'react';
import { connect, useSelector } from 'react-redux';
import styled from 'styled-components';
import { useHistory, useParams } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Box, Grid } from '@material-ui/core';
import { SecondaryButton } from '@common/components/Button/SecondaryButton';
import { colors } from '@utils/uiTheme';
import { projectsSelectors } from '@selectors/projects';
import { projectDataSelectors } from '@selectors/projectData';

import { getProjectDetails } from '@actions/projects';
import { getErrorMessage } from '@utils/error';
import {
  createValidationJobAndGetProjectInfo,
  getDataValidity,
  getProjectFiles,
  uploadFiles,
} from '@actions/projectData';
import { setBackButton, setPageTitle } from '@actions/interface';
import { getCardStyle } from '@utils/getCardStyle';
import isEqual from 'lodash.isequal';
import { bindActionCreators } from 'redux';
import { ScenarioProgressContainer } from '@common/components/ScenarioProgress/ScenarioProgressContainer';
import { useCustomTranslation } from '@utils/translate';

import { ProjectsLayout } from '../../ProjectDetails/components/ProjectsLayout/ProjectsLayout';
import { ErrorBox } from '../components/ProjectData/ErrorBox';
import { FileButton } from '../components/ProjectData/FileButton';
import { UploadField } from '../components/ProjectData/UploadField';

const StyledContainer = styled.div`
  padding: 0 20%;
`;

const StyledTitle = styled.h2`
  margin: 48px 0 30px;
  font-size: 26px;
`;

const StyledDescription = styled.p`
  margin: 0 0 30px 0;
  color: ${colors.black};
  font-weight: 300;
  font-size: 14px;
  line-height: 24px;
  word-spacing: 0.1em;
`;

const StyledGridItem = styled(({ isEven: _isEven, ...props }) => (
  <Grid {...props} />
))`
  && {
    &.MuiGrid-item {
      padding-bottom: 15px;
      padding-${props => (props.isEven ? 'right' : 'left')}: 11px;
    }
  }
`;

const ProjectData = ({
  createValidationJobAndGetProjectInfoAction,
  files,
  filesTypes,
  getDataValidityAction,
  getProjectDetailsAction,
  getProjectFilesAction,
  setBackButtonAction,
  setPageTitleAction,
  uploadFilesAction,
  validation,
}) => {
  const projectId = useParams().id;
  const history = useHistory();
  const { t } = useCustomTranslation();
  const hasProjectsFiles = useSelector(projectDataSelectors.hasProjectsFiles);

  useEffect(() => {
    setBackButtonAction('arrow');
    getProjectDetailsAction(projectId).then(project =>
      setPageTitleAction(project.name)
    );
    getProjectFilesAction(projectId);

    return () => setBackButtonAction('');
  }, [projectId]);

  useEffect(() => {
    if (hasProjectsFiles) {
      getDataValidityAction(projectId, filesTypes);
    }
  }, [filesTypes, projectId, hasProjectsFiles]);

  const invalidDataTypes = Object.keys(filesTypes).filter((type) => {
    const validationData = validation[type];

    return validationData && (validationData.errors || validationData.warnings);
  });

  const hasGeneralValidationErrors =
    validation.default &&
    validation.default.data &&
    validation.default.data.some(
      item => item.severity === 'error' && item.message.includes('unknown')
    );

  const hideNextButton =
    !Object.keys(validation).length ||
    Object.keys(filesTypes).filter(
      type => validation[type] && validation[type].errors
    ).length > 0 ||
    hasGeneralValidationErrors;

  const onDrop = async (formData) => {
    const uploadedData = await uploadFilesAction(formData);

    const uploadedFiles = {
      ...files,
      ...uploadedData,
    };
    const fileTypesKeys = Object.keys(filesTypes);
    const uploadedFilesKeys = Object.keys(uploadedFiles);
    const shouldValidateData = isEqual(
      uploadedFilesKeys.sort(),
      fileTypesKeys.sort()
    );

    if (shouldValidateData) {
      await createValidationJobAndGetProjectInfoAction(
        projectId,
        uploadedFiles,
        filesTypes
      );
    }
  };

  return (
    <>
      <ScenarioProgressContainer title={t('Validating data...')} />
      <ProjectsLayout>
        <StyledContainer>
          {!hideNextButton && (
            <Box
              display="flex"
              justifyContent="flex-end"
              alignItems="center"
              mb={25}
            >
              <SecondaryButton
                size="large"
                onClick={() => history.push('analytics')}
              >
                {t('Continue')}
              </SecondaryButton>
            </Box>
          )}
          <StyledTitle>{t('Required Data')}</StyledTitle>
          <StyledDescription>
            {t('Upload your supplier bids before starting the')}
            <br />
            {t('scenario optimization process.')}
          </StyledDescription>
          {hasGeneralValidationErrors && (
            <ErrorBox isFormError>
              {getErrorMessage('data processing')}
            </ErrorBox>
          )}
          <UploadField
            onDrop={onDrop}
            projectId={projectId}
            filesTypes={filesTypes}
          />

          <Box display="flex" flexWrap="wrap" mb={12}>
            <Grid container spacing={10}>
              {Object.keys(filesTypes).map((type, index) => (
                <StyledGridItem key={type} item xs={6} isEven={index % 2 === 0}>
                  <FileButton
                    title={filesTypes[type] && filesTypes[type].name}
                    path={`/projects/${projectId}/data/${type}`}
                    file={files[type]}
                    validation={validation[type]}
                    cardStyle={getCardStyle(validation[type])}
                  />
                </StyledGridItem>
              ))}
            </Grid>
          </Box>

          <Box display="flex" flexWrap="wrap">
            {invalidDataTypes.map(type => (
              <ErrorBox
                key={type}
                fileType={type}
                cardStyle={getCardStyle(validation[type])}
              />
            ))}
          </Box>
        </StyledContainer>
      </ProjectsLayout>
    </>
  );
};

ProjectData.propTypes = {
  createValidationJobAndGetProjectInfoAction: PropTypes.func.isRequired,
  files: PropTypes.shape({}).isRequired,
  filesTypes: PropTypes.shape({}).isRequired,
  getDataValidityAction: PropTypes.func.isRequired,
  getProjectDetailsAction: PropTypes.func.isRequired,
  getProjectFilesAction: PropTypes.func.isRequired,
  setBackButtonAction: PropTypes.func.isRequired,
  setPageTitleAction: PropTypes.func.isRequired,
  uploadFilesAction: PropTypes.func.isRequired,
  validation: PropTypes.shape({}).isRequired,
};

const mapStateToProps = state => ({
  files: projectDataSelectors.getFiles(state),
  filesTypes: projectDataSelectors.getFilesTypes(state),
  projectDetails: projectsSelectors.getProjectDetails(state),
  validation: projectDataSelectors.getValidation(state),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      createValidationJobAndGetProjectInfoAction: createValidationJobAndGetProjectInfo,
      getDataValidityAction: getDataValidity,
      getProjectDetailsAction: getProjectDetails,
      getProjectFilesAction: getProjectFiles,
      setBackButtonAction: setBackButton,
      setPageTitleAction: setPageTitle,
      uploadFilesAction: uploadFiles,
    },
    dispatch
  );

export const ProjectDataContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProjectData);
