import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { Card } from '@material-ui/core';
import { colors } from '@utils/uiTheme';
import { useCustomTranslation } from '@utils/translate';
import { UploadDetails } from './UploadDetails';

const StyledCard = styled(Card)`
  && {
    width: 100%;
    height: 130px;
    border-color: ${props => colors[props.type]};
  }
`;

const StyledLink = styled(Link)`
  display: block;
  padding: 19px 16px;
  color: #000;
  text-decoration: none;
`;

const StyledTitle = styled.h3`
  margin: 0 0 9px;
  font-size: 18px;
`;

export const FileButton = ({
  cardStyle,
  file,
  path,
  title,
}) => {
  const { t } = useCustomTranslation();

  return (
    <StyledCard
      elevation={0}
      type={cardStyle.type}
    >
      <StyledLink to={path}>
        <div>
          <StyledTitle>{ `${title} ${t('Data')}` }</StyledTitle>
          <UploadDetails
            file={file}
            cardStyle={cardStyle}
          />
        </div>
      </StyledLink>
    </StyledCard>
  );
};
FileButton.defaultProps = {
  file: null,
};

FileButton.propTypes = {
  cardStyle: PropTypes.shape({}).isRequired,
  file: PropTypes.shape({}),
  path: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
};
