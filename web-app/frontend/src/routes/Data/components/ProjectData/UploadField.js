import React from 'react';
import PropTypes from 'prop-types';
import { useDropzone } from 'react-dropzone';
import { CloudUpload } from '@material-ui/icons';
import { Box } from '@material-ui/core';
import styled from 'styled-components';
import { useCustomTranslation } from '@utils/translate';

const StyledUploadField = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 115px;
  margin: 0 0 30px;
  border: 3px dashed #e4e4e4;
  border-radius: 13px;
  font-size: 15px;
  cursor: pointer;
`;

const StyledCloudUpload = styled(CloudUpload)`
  && {
    width: 36px;
    height: 36px;
    margin-right: 15px;
  }
`;

export const UploadField = ({
  fileType,
  onDrop,
  projectId,
  filesTypes,
}) => {
  const onUpload = (acceptedFiles) => {
    const formData = new FormData();
    const fileTypesKeys = fileType ? [fileType] : Object.keys(filesTypes);

    acceptedFiles.forEach((file) => {
      const { name } = file;

      fileTypesKeys.forEach((type) => {
        if (name.toLowerCase().includes(type)) {
          formData.append(type, file);
        }
      });
    });

    formData.append('projectId', projectId);
    formData.append('fileType', fileType);

    onDrop(formData);
  };

  const {
    getRootProps,
    getInputProps,
    isDragActive,
  } = useDropzone({ onDrop: onUpload });

  const { t } = useCustomTranslation();

  return (
    <StyledUploadField {...getRootProps()}>
      <input {...getInputProps()} />
      <Box
        display="flex"
        alignItems="center"
      >
        <StyledCloudUpload />
        <p>
          {isDragActive ? t('Drop the files here ...') : t('Drag and drop files here or click')}
        </p>
      </Box>
    </StyledUploadField>
  );
};

UploadField.propTypes = {
  fileType: PropTypes.string,
  filesTypes: PropTypes.shape({}).isRequired,
  onDrop: PropTypes.func.isRequired,
  projectId: PropTypes.string.isRequired,
};

UploadField.defaultProps = {
  fileType: null,
};
