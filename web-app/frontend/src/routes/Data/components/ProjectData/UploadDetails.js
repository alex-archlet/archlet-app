import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Box } from '@material-ui/core';
import { LastEdit } from '@common/components/LastEdit/LastEdit';
import { useCustomTranslation } from '@utils/translate';

const StyledName = styled.h4`
  margin: 0 0 6px;
  font-size: 14px;
`;

const StyledDetails = styled.p`
  margin: 2px 0;
  color: #767676;
  font-size: 12px;
`;

export const UploadDetails = ({
  file,
  cardStyle,
}) => {
  const { t } = useCustomTranslation();

  return (file ? (
    <div>
      <Box display="flex">
        { file && <StyledName>{ `${file.name}${file.fileExtension}` }</StyledName>}
        { cardStyle.icon }
      </Box>
      { file.updatedAt && <LastEdit date={file.updatedAt} /> }
    </div>
  ) :
    <StyledDetails>{t('No file uploaded')}</StyledDetails>
  );
};

UploadDetails.propTypes = {
  cardStyle: PropTypes.shape({}).isRequired,
  file: PropTypes.shape({}),
};

UploadDetails.defaultProps = {
  file: {},
};
