import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { colors } from '@utils/uiTheme';
import { Box } from '@material-ui/core';
import { Text } from '@common/components/Text/Text';
import { StyledError } from '@utils/getCardStyle';

const StyledBox = styled(({
  isFormError: _isFormError,
  ...props
}) => <Box {...props} />)`
  width: 100%;
  margin: 15px 0 ${props => (props.isFormError ? '30px' : '5px')};
  border: 1px solid ${props => colors[props.type]};
  border-radius: 13px;
  padding: 17px 15px 15px;
  color: ${props => colors[props.type]};
`;

const StyledTitle = styled(Text)`
  margin-left: 7px;
  font-weight: bold;
  text-transform: capitalize;
`;

const StyledDescription = styled(Text)`
  margin-top: 2px;
  margin-left: 33px;
  font-weight: normal;
`;

export const ErrorBox = ({
  cardStyle: {
    icon,
    type,
  },
  children,
  fileType,
  isFormError,
}) => (
  <StyledBox
    type={type}
    isFormError={isFormError}
  >
    <Box
      display="flex"
      alignItems="center"
    >
      { icon }
      <StyledTitle
        color={colors[type]}
        fontWeight={500}
        fontSize={17}
      >
        { type }
      </StyledTitle>
    </Box>
    <StyledDescription
      fontSize={12}
      color={colors[type]}
    >
      {children || `The ${isFormError ? 'form' : `${fileType} file`} contains ${type}s.` }
    </StyledDescription>
  </StyledBox>
);

ErrorBox.propTypes = ({
  cardStyle: PropTypes.shape({}),
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]),
  fileType: PropTypes.string,
  isFormError: PropTypes.bool,
});

ErrorBox.defaultProps = {
  cardStyle: {
    icon: <StyledError />,
    type: 'error',
  },
  children: null,
  fileType: '',
  isFormError: false,
};
