import React from 'react';
import PropTypes from 'prop-types';
import {
  Route,
  Switch,
} from 'react-router-dom';
import { ProjectDataContainer } from './containers/ProjectData';
import { DataTypeContainer } from './containers/DataType';
import { ViewDataContainer } from './containers/ViewData';

export const DataRouter = ({ match }) => (
  <div style={{ paddingTop: 16 }}>
    <Switch>
      <Route
        path={`${match.path}/`}
        exact
        component={ProjectDataContainer}
        />
      <Route
        path={`${match.path}/:type`}
        exact
        render={props => (
          <DataTypeContainer
          parentUrl={match.url}
          {...props}
          />
          )}
          />
      <Route
        path={`${match.path}/:type/view`}
        render={props => (
          <ViewDataContainer
          parentUrl={match.url}
          {...props}
          />
          )}
          />
    </Switch>
  </div>
);

DataRouter.propTypes = ({
  match: PropTypes.shape({}).isRequired,
});
