import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  initialize,
  reset,
} from 'redux-form';

import {
  setBackButton,
  setPageTitle,
} from '@actions/interface';

import {
  createBranch,
  deleteBranch,
  getBranches,
  getCompanies,
  getRoles,
  updateBranch,
} from '@actions/admin';
import { adminSelectors } from '@selectors/admin';
import { useCustomTranslation } from '@utils/translate';

import { Entries } from '../components/Entries/Entries';

const Branches = ({
  branches,
  companies,
  createBranchAction,
  updateBranchAction,
  deleteBranchAction,
  getBranchesAction,
  getCompaniesAction,
  getRolesAction,
  initializeFormAction,
  resetFormAction,
  roles,
  setBackButtonAction,
  setPageTitleAction,
  location,
}) => {
  const {
    state,
  } = location;
  const { t } = useCustomTranslation();

  useEffect(() => {
    getBranchesAction();

    if (!companies.length) {
      getCompaniesAction();
    }

    getRolesAction();

    setBackButtonAction('close', (state && state.referrer) || '/');
    setPageTitleAction(t('Admin Page'));

    return () => setBackButtonAction('');
  }, [companies.length, state]);

  const fields = [
    {
      label: 'Name',
      name: 'name',
    },
    {
      label: 'Location',
      name: 'location',
    },
    {
      label: 'Type',
      name: 'accountType',
      options: [
        {
          label: 'admin',
          value: 'admin',
        },
        {
          label: 'user',
          value: 'user',
        },
      ],
    },
    {
      isJson: true,
      label: 'json',
      name: 'json',
    },
    {
      label: 'Company',
      name: 'companyId',
      options: companies.map(company => ({
        label: company.name,
        value: company.id,
      })),
    },
    {
      label: 'Role',
      name: 'roleId',
      options: roles.map(role => ({
        label: role.name,
        value: role.id,
      })),
    },
  ];

  return (
    <Entries
      title={t('Branches')}
      formName="BranchesAdminForm"
      fields={fields}
      entries={branches}
      createEntryAction={createBranchAction}
      updateEntryAction={updateBranchAction}
      deleteEntryAction={deleteBranchAction}
      initializeFormAction={initializeFormAction}
      resetFormAction={resetFormAction}
    />
  );
};

Branches.propTypes = ({
  branches: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  companies: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  createBranchAction: PropTypes.func.isRequired,
  deleteBranchAction: PropTypes.func.isRequired,
  getBranchesAction: PropTypes.func.isRequired,
  getCompaniesAction: PropTypes.func.isRequired,
  getRolesAction: PropTypes.func.isRequired,
  initializeFormAction: PropTypes.func.isRequired,
  location: PropTypes.string.isRequired,
  resetFormAction: PropTypes.func.isRequired,
  roles: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  setBackButtonAction: PropTypes.func.isRequired,
  setPageTitleAction: PropTypes.func.isRequired,
  updateBranchAction: PropTypes.func.isRequired,
});

const mapStateToProps = state => ({
  branches: adminSelectors.branches(state),
  companies: adminSelectors.companies(state),
  roles: adminSelectors.roles(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  createBranchAction: createBranch,
  deleteBranchAction: deleteBranch,
  getBranchesAction: getBranches,
  getCompaniesAction: getCompanies,
  getRolesAction: getRoles,
  initializeFormAction: initialize,
  resetFormAction: reset,
  setBackButtonAction: setBackButton,
  setPageTitleAction: setPageTitle,
  updateBranchAction: updateBranch,
}, dispatch);

export const BranchesContainer = connect(mapStateToProps, mapDispatchToProps)(Branches);
