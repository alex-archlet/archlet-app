import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  initialize,
  reset,
} from 'redux-form';

import {
  setBackButton,
  setPageTitle,
} from '@actions/interface';

import {
  createCompany,
  deleteCompany,
  getCompanies,
  updateCompany,
} from '@actions/admin';
import { adminSelectors } from '@selectors/admin';
import { useCustomTranslation } from '@utils/translate';
import { Entries } from '../components/Entries/Entries';

const fields = [
  {
    isBigger: true,
    label: 'Name',
    name: 'name',
  },
  {
    isJson: true,
    label: 'json',
    name: 'json',
  },
];

const Companies = ({
  companies,
  createCompanyAction,
  deleteCompanyAction,
  getCompaniesAction,
  initializeFormAction,
  resetFormAction,
  updateCompanyAction,
  setBackButtonAction,
  setPageTitleAction,
  location,
}) => {
  const {
    state,
  } = location;
  const { t } = useCustomTranslation();

  const title = t('Admin Page');

  useEffect(() => {
    getCompaniesAction();
    setBackButtonAction('close', (state && state.referrer) || '/');
    setPageTitleAction(title);

    return () => setBackButtonAction('');
  }, [state, title]);

  return (
    <Entries
      title={t('Companies')}
      formName="CompaniesAdminForm"
      fields={fields}
      entries={companies}
      createEntryAction={createCompanyAction}
      updateEntryAction={updateCompanyAction}
      deleteEntryAction={deleteCompanyAction}
      initializeFormAction={initializeFormAction}
      resetFormAction={resetFormAction}
    />
  );
};

Companies.propTypes = ({
  companies: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  createCompanyAction: PropTypes.func.isRequired,
  deleteCompanyAction: PropTypes.func.isRequired,
  getCompaniesAction: PropTypes.func.isRequired,
  initializeFormAction: PropTypes.func.isRequired,
  location: PropTypes.string.isRequired,
  resetFormAction: PropTypes.func.isRequired,
  setBackButtonAction: PropTypes.func.isRequired,
  setPageTitleAction: PropTypes.func.isRequired,
  updateCompanyAction: PropTypes.func.isRequired,
});

const mapStateToProps = state => ({
  companies: adminSelectors.companies(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  createCompanyAction: createCompany,
  deleteCompanyAction: deleteCompany,
  getCompaniesAction: getCompanies,
  initializeFormAction: initialize,
  resetFormAction: reset,
  setBackButtonAction: setBackButton,
  setPageTitleAction: setPageTitle,
  updateCompanyAction: updateCompany,
}, dispatch);

export const CompaniesContainer = connect(mapStateToProps, mapDispatchToProps)(Companies);
