import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  initialize,
  reset,
} from 'redux-form';
import { userSelectors } from '@selectors/user';

import {
  setBackButton,
  setPageTitle,
} from '@actions/interface';

import {
  createUser,
  deleteUser,
  getBranches,
  getRoles,
  getUsers,
  updateUser,
} from '@actions/admin';
import { adminSelectors } from '@selectors/admin';
import { useCustomTranslation } from '@utils/translate';
import { Entries } from '../components/Entries/Entries';

const Users = ({
  users,
  branches,
  currentUser,
  createUserAction,
  updateUserAction,
  deleteUserAction,
  getBranchesAction,
  getRolesAction,
  getUsersAction,
  initializeFormAction,
  resetFormAction,
  setBackButtonAction,
  setPageTitleAction,
  location,
  roles,
}) => {
  const {
    state,
  } = location;
  const { t } = useCustomTranslation();

  useEffect(() => {
    getUsersAction();

    getRolesAction();

    if (!branches.length) {
      getBranchesAction();
    }

    setBackButtonAction('close', (state && state.referrer) || '/');
    setPageTitleAction(t('Admin Page'));

    return () => setBackButtonAction('');
  }, []);

  const fields = [
    {
      label: 'First Name',
      name: 'firstName',
    },
    {
      label: 'Last Name',
      name: 'lastName',
    },
    {
      isNotEditable: true,
      label: 'Username (email)',
      name: 'username',
      type: 'email',
    },
    {
      isNotEditable: true,
      label: 'Initial PW',
      name: 'password',
      type: 'password',
    },
    {
      label: 'Branch',
      name: 'branchId',
      options: branches.map(branch => ({
        label: branch.name,
        value: branch.id,
      })),
    },
    {
      label: 'Role',
      name: 'roleId',
      options: roles.map(role => ({
        label: role.name,
        value: role.id,
      })),
    },
  ];

  return (
    <Entries
      title={t('Users')}
      formName="UsersAdminForm"
      fields={fields}
      entries={users}
      createEntryAction={createUserAction}
      updateEntryAction={updateUserAction}
      deleteEntryAction={deleteUserAction}
      initializeFormAction={initializeFormAction}
      resetFormAction={resetFormAction}
      currentUser={currentUser}
    />
  );
};

Users.propTypes = ({
  branches: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  createUserAction: PropTypes.func.isRequired,
  currentUser: PropTypes.shape({}).isRequired,
  deleteUserAction: PropTypes.func.isRequired,
  getBranchesAction: PropTypes.func.isRequired,
  getRolesAction: PropTypes.func.isRequired,
  getUsersAction: PropTypes.func.isRequired,
  initializeFormAction: PropTypes.func.isRequired,
  location: PropTypes.string.isRequired,
  resetFormAction: PropTypes.func.isRequired,
  roles: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  setBackButtonAction: PropTypes.func.isRequired,
  setPageTitleAction: PropTypes.func.isRequired,
  updateUserAction: PropTypes.func.isRequired,
  users: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
});

const mapStateToProps = state => ({
  branches: adminSelectors.branches(state),
  currentUser: userSelectors.getUser(state),
  roles: adminSelectors.roles(state),
  users: adminSelectors.users(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  createUserAction: createUser,
  deleteUserAction: deleteUser,
  getBranchesAction: getBranches,
  getRolesAction: getRoles,
  getUsersAction: getUsers,
  initializeFormAction: initialize,
  resetFormAction: reset,
  setBackButtonAction: setBackButton,
  setPageTitleAction: setPageTitle,
  updateUserAction: updateUser,
}, dispatch);

export const UsersContainer = connect(mapStateToProps, mapDispatchToProps)(Users);
