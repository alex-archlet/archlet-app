import React from 'react';
import PropTypes from 'prop-types';
import {
  Redirect,
  Route,
  Switch,
} from 'react-router-dom';

import { CompaniesContainer } from './containers/Companies';
import { BranchesContainer } from './containers/Branches';
import { UsersContainer } from './containers/Users';

export const AdminRouter = ({
  match,
  location,
}) => (
  <Switch>
    <Route
      path={`${match.path}/companies`}
      component={CompaniesContainer}
    />
    <Route
      path={`${match.path}/branches`}
      component={BranchesContainer}
    />
    <Route
      path={`${match.path}/users`}
      component={UsersContainer}
    />
    <Route
      path={`${match.path}/`}
      exact
      render={() => (
        <Redirect
          to={{
            pathname: `${match.path}/companies`,
            state: location.state,
          }}
        />
      )}
    />
  </Switch>
);

AdminRouter.propTypes = ({
  location: PropTypes.shape({}).isRequired,
  match: PropTypes.shape({}).isRequired,
});
