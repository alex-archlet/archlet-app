import React from 'react';
import styled from 'styled-components';

import { Business, People, Share } from '@material-ui/icons';
import { Box } from '@material-ui/core';

import { Sidebar } from '@common/components/Sidebar/Sidebar';
import { SidebarItem } from '@common/components/Sidebar/SidebarItem';
import { ProjectsLayout } from '@routes/ProjectDetails/components/ProjectsLayout/ProjectsLayout';
import { AdminRouter } from '../../AdminRouter';

const StyledBox = styled(Box)`
  padding-top: 25px;
`;

const StyledContainer = styled.div`
  padding: 40px 30px 0;
`;

interface Props {
  match: {};
  location: {};
}

export const AdminLayout: React.FC<Props> = ({ match, location }) => (
  <ProjectsLayout>
    <Sidebar>
      <StyledBox>
        <SidebarItem
          icon={<Business />}
          name="Companies"
          to="/admin/companies"
        />
        <SidebarItem icon={<Share />} name="Branches" to="/admin/branches" />
        <SidebarItem icon={<People />} name="Users" to="/admin/users" />
      </StyledBox>
    </Sidebar>

    <StyledContainer>
      <AdminRouter match={match} location={location} />
    </StyledContainer>
  </ProjectsLayout>
);
