import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Field, Form, reduxForm } from 'redux-form';
import { Box, MenuItem } from '@material-ui/core';
import { Button } from '@common/components/Button/Button';
import { Add } from '@material-ui/icons';

import { validators } from '@utils/validators';
import { colors } from '@utils/uiTheme';
import { useCustomTranslation } from '@utils/translate';
import { ReduxTextField } from '@common/components/ReduxForms/ReduxTextField';
import { Text } from '@common/components/Text/Text';

const StyledForm = styled(Form)`
  && {
    display: flex;
    justify-content: space-between;
    align-items: flex-start;
    width: 100%;
    margin-bottom: 4px;
    padding-left: 21px;
  }
`;

const StyledField = styled(({ isBigger: _isBigger, ...props }) => (
  <Field {...props} />
))`
  && {
    width: ${props => (props.isBigger ? '13vw' : '7vw')};
    min-width: ${props => (props.isBigger ? '150px' : '80px')};
    margin-top: 0;
    margin-right: 25px;

    .MuiInputBase-root {
      margin-top: 0;
    }

    .MuiInputLabel-formControl {
      top: -15px;
    }
  }
`;

const StyledAddButton = styled(Button)`
  && {
    padding: 5px 16px 5px 12px;

    .MuiButton-label {
      padding-right: 10px;
      font-weight: bold;
      font-size: 13px;
      line-height: 16px;
    }

    .MuiButton-startIcon {
      margin-right: 18px;
    }
  }
`;

const StyledButton = styled(Button)`
  && {
    margin-right: 10px;
    padding: 7px 12px;

    &&:last-child {
      margin-right: 0;
    }

    .MuiButton-label {
      font-weight: bold;
      font-size: 13px;
      line-height: 16px;
    }
  }
`;

export const EntryFormComponent = ({
  handleSubmit,
  error,
  valid,
  dirty,
  fields,
  isEditMode,
  isEditModeSuccess,
  handleCancel,
  submitSucceeded,
}) => {
  const { t } = useCustomTranslation();
  const submitSucceededText = isEditModeSuccess
    ? `${t('Successfully updated')}`
    : `${t('Successfully added')}`;

  return (
    <StyledForm onSubmit={handleSubmit}>
      <Box>
        {fields &&
          fields.map(field => (
            <StyledField
              key={field.name}
              label={field.label}
              name={field.name}
              type={field.type || 'text'}
              component={ReduxTextField}
              select={!!field.options}
              validate={[
                ...(!(field.type === 'password' && isEditMode)
                  ? [validators.require]
                  : []),
                ...(field.isJson ? [validators.json] : []),
                ...(field.type === 'email' ? [validators.email] : []),
                ...(field.type === 'password' && !isEditMode
                  ? [validators.password]
                  : []),
              ]}
              disabled={field.isNotEditable && isEditMode}
              isBigger={field.isBigger}
            >
              {!!field.options &&
                field.options.map(option => (
                  <MenuItem key={option.value} value={option.value}>
                    {option.label}
                  </MenuItem>
                ))}
            </StyledField>
          ))}
      </Box>
      <Box display="flex" flexDirection="column" alignItems="flex-end">
        {!isEditMode ? (
          <StyledAddButton
            type="submit"
            color="secondary"
            disabled={!valid}
            startIcon={<Add />}
          >
            {t('Add')}
          </StyledAddButton>
        ) : (
          <Box display="flex">
            <StyledButton
              type="submit"
              color="secondary"
              disabled={!valid || !dirty}
            >
              {t('Update')}
            </StyledButton>
            <StyledButton type="button" color="primary" onClick={handleCancel}>
              {t('Cancel')}
            </StyledButton>
          </Box>
        )}
        <Text color={colors.red}>{error}</Text>
        {submitSucceeded && <Text>{submitSucceededText}</Text>}
      </Box>
    </StyledForm>
  );
};

EntryFormComponent.propTypes = {
  dirty: PropTypes.bool.isRequired,
  error: PropTypes.string,
  fields: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  handleCancel: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  isEditMode: PropTypes.bool.isRequired,
  isEditModeSuccess: PropTypes.bool.isRequired,
  submitSucceeded: PropTypes.bool.isRequired,
  valid: PropTypes.bool.isRequired,
};

EntryFormComponent.defaultProps = {
  error: '',
};

export const EntryForm = reduxForm({ form: 'EntryForm' })(EntryFormComponent);
