import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import {
  Box,
  Typography,
} from '@material-ui/core';

import { EntryForm } from '../EntryForm/EntryForm';
import { EntriesTable } from '../EntriesTable/EntriesTable';

const StyledTitle = styled(Typography)`
  && {
    margin-bottom: 38px;
    font-weight: 500;
    font-size: 18.5px;
    line-height: 21px;
  }
`;

export const Entries = ({
  fields,
  entries,
  formName,
  createEntryAction,
  updateEntryAction,
  deleteEntryAction,
  initializeFormAction,
  resetFormAction,
  title,
}) => {
  const [editEntryId, setEditEntryId] = useState(null);
  const [isEditModeSuccess, setIsEditModeSuccess] = useState(null);

  const onRemoveEntry = id => deleteEntryAction(id);

  const onEditEntry = (id) => {
    setEditEntryId(id);
    let initialValues = {};

    if (id) {
      const matchingEntry = entries.find(entry => entry.id === id);

      initialValues = fields.reduce((accumulator, field) => ({
        ...accumulator,
        [field.name]: field.isJson ?
          JSON.stringify(matchingEntry[field.name]) :
          matchingEntry[field.name],
      }), { id });
    }

    initializeFormAction(formName, initialValues);
  };

  const onEntryFormSubmit = (values) => {
    if (editEntryId) {
      return updateEntryAction(values)
        .then(() => {
          setIsEditModeSuccess(true);
          onEditEntry(null);
        });
    }

    return createEntryAction(values)
      .then(() => {
        setIsEditModeSuccess(null);
        resetFormAction(formName);
      });
  };

  return (
    <Box
      width="80%"
      minWidth={736}
      maxWidth={1070}
    >
      <StyledTitle variant="h2">{title}</StyledTitle>
      <EntryForm
        form={formName}
        onSubmit={onEntryFormSubmit}
        fields={fields}
        isEditMode={!!editEntryId}
        isEditModeSuccess={isEditModeSuccess}
        handleCancel={() => onEditEntry(null)}
      />
      <EntriesTable
        data={entries}
        columns={fields}
        handleRemove={onRemoveEntry}
        handleEdit={onEditEntry}
      />
    </Box>
  );
};

Entries.propTypes = ({
  createEntryAction: PropTypes.func.isRequired,
  deleteEntryAction: PropTypes.func.isRequired,
  entries: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  fields: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  formName: PropTypes.string.isRequired,
  initializeFormAction: PropTypes.func.isRequired,
  resetFormAction: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  updateEntryAction: PropTypes.func.isRequired,
});
