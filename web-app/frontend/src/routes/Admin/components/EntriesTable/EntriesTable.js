import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {
  Box,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableRow,
  Tooltip,
} from '@material-ui/core';
import {
  Close,
  Edit,
} from '@material-ui/icons';
import { colors } from '@utils/uiTheme';

import { useCustomTranslation } from '@utils/translate';

const StyledTable = styled(Table)`
  table-layout: fixed;
`;

const StyledSpan = styled.span`
  font-size: 14px;
  line-height: 16px;
  cursor: pointer;
`;

const StyledTableCell = styled(TableCell)`
  && {
    position: relative;
    overflow: hidden;
    width: 175px;
    padding: 20px 25px 19px 0;
    font-size: 14px;
    line-height: 16px;
    text-overflow: ellipsis;
    white-space: nowrap;
    vertical-align: top;

    @media (min-width: 1180px) {
      width: calc(13vw + 25px);
    }

    &&.MuiTableCell-sizeSmall {
      width: 105px;

      @media (min-width: 1180px) {
        width: calc(7vw + 25px);
      }
    }

    &&:first-child {
      width: 196px;
      padding-left: 21px;

      @media (min-width: 1180px) {
        width: calc(13vw + 21px + 25px);
      }

      &&.MuiTableCell-sizeSmall {
        width: 126px;

        @media (min-width: 1180px) {
          width: calc(7vw + 21px + 25px);
        }
      }
    }

    &&:last-child {
      width: auto;
      padding: 4px 6px;
      text-align: right;
    }
  }
`;

const StyledBox = styled(Box)`
  overflow-y: auto;
  max-height: calc(100vh - 349px);
  border: 0.5px solid #c4c2c2;
  border-radius: 4px;
`;

const StyledClose = styled(Close)`
  && {
    fill: ${colors.iconGrey};
  }
`;

const StyledEdit = styled(Edit)`
  && {
    fill: ${colors.iconGrey};
  }
`;

const copyStringToClipboard = (text) => {
  const { userAgent } = window.navigator;
  const isInternetExplorer = userAgent.indexOf('MSIE ');

  // eslint-disable-next-line no-useless-escape
  if (isInternetExplorer > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
    window.clipboardData.setData('Text', text);
  } else {
    const element = document.createElement('textarea');

    element.value = text;
    element.style = {
      left: '-9999px',
      position: 'absolute',
    };
    document.body.appendChild(element);
    element.select();
    document.execCommand('copy');
    document.body.removeChild(element);
  }
};

const renderColumnValue = (column, item, title) => {
  if (column.isJson) {
    return (
      <>
        <Tooltip
          title={title}
          placement="top-start"
        >
          <StyledSpan
            onClick={() => copyStringToClipboard(JSON.stringify(item[column.name]))}
            role="presentation"
          >
            {JSON.stringify(item[column.name])}
          </StyledSpan>
        </Tooltip>
      </>
    );
  }

  if (column.type === 'password') {
    return '*****';
  }

  if (column.options && column.options.length) {
    const matchingOption = column.options.find(option => option.value === item[column.name]);

    return matchingOption ? matchingOption.label : '-';
  }

  return item[column.name];
};

const sortDataById = data => [...data].sort((a, b) => a.id - b.id);

export const EntriesTable = ({
  data,
  columns,
  handleEdit,
  handleRemove,
  currentUserId,
}) => {
  const { t } = useCustomTranslation();

  return (
    <StyledBox>
      <StyledTable>
        <TableBody>
          { data && sortDataById(data).map(item => (
            <TableRow key={item.id}>
              {columns && columns.map(column => (
                <StyledTableCell
                  key={column.name}
                  size={column.isBigger ? 'medium' : 'small'}
                  padding="none"
                >
                  { renderColumnValue(column, item, t('Click to copy to clipboard')) || '-' }
                </StyledTableCell>
              ))}
              <StyledTableCell>
                {!(currentUserId && item.id === currentUserId) && (
                  <Box>
                    <IconButton
                      aria-label="edit"
                      onClick={() => handleEdit(item.id)}
                    >
                      <StyledEdit />
                    </IconButton>
                    <IconButton
                      aria-label="delete"
                      onClick={() => handleRemove(item.id)}
                    >
                      <StyledClose />
                    </IconButton>
                  </Box>
                )}
              </StyledTableCell>
            </TableRow>
          )) }
        </TableBody>
      </StyledTable>
    </StyledBox>
  );
};

EntriesTable.propTypes = ({
  columns: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  currentUserId: PropTypes.number,
  data: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  handleEdit: PropTypes.func.isRequired,
  handleRemove: PropTypes.func.isRequired,
});

EntriesTable.defaultProps = {
  currentUserId: null,
};
