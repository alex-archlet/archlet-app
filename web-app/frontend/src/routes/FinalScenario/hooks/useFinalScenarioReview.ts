import { useNotifications } from "@common/hooks/useNotifications"
import { request } from "@utils/request"
import { useCustomTranslation } from "@utils/translate";

export const useFinalScenarioReview = () => {
  const { t } = useCustomTranslation();
  const { showSuccess, showError } = useNotifications();

  const onRequestReview = async (scenarioId: string, comment: string, email: string) => {
    try {
      await request.post(`scenarios/${scenarioId}/request-review`, { comment, email });
      showSuccess({
        title: t('Scenario sent for review'),
      });
    } catch (error) {
      showError({
        title: t('Scenario sending to review failed'),
        error,
      });
    }
  }

  return {
    onRequestReview,
  }
}
