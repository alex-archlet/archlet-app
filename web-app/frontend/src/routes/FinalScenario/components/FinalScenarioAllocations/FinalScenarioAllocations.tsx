import React from 'react';
import { Allocations } from '@routes/ScenarioDetails/Allocations/Allocations';

interface Props {
  scenarioId: string;
}

export const FinalScenarioAllocations: React.FC<Props> = ({ scenarioId }) => {
  if (!scenarioId) {
    return null;
  }

  return <Allocations scenarioId={scenarioId} />;
};
