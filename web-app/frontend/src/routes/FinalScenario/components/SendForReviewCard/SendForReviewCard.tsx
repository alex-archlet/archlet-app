import React from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import styled from 'styled-components';

import { useCustomTranslation } from '@utils/translate';
import { Card } from '@common/components/Card/Card';
import { Input } from '@common/components/Input/Input';
import { TextArea } from '@common/components/TextArea/TextArea';
import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';
import { SecondaryButton } from '@common/components/Button/SecondaryButton';
import { useFinalScenarioReview } from '@routes/FinalScenario/hooks/useFinalScenarioReview';

interface FormValues {
  email: string;
  text: string;
}

interface Props {
  scenarioId: string | undefined;
}

const StyledButtonContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

const ReviewSchema = Yup.object().shape({
  email: Yup.string()
    .email('A valid e-mail is required')
    .required('A valid e-mail is required'),
  text: Yup.string(),
});


export const SendForReviewCard: React.FC<Props> = ({ scenarioId }) => {
  const { t } = useCustomTranslation();
  const {
    values,
    errors,
    setFieldValue,
    setFieldTouched,
    isValid,
    validateForm,
  } = useFormik<FormValues>({
    initialValues: {
      email: '',
      text: '',
    },
    validationSchema: ReviewSchema,
    onSubmit: console.info,
    validateOnBlur: true,
    validateOnChange: true,
  });

  const { onRequestReview } = useFinalScenarioReview();
  

  const onChange = (field: string, value: string) => {
    setFieldTouched(field);
    setFieldValue(field, value);
  };

  const buttonsEnabled = Boolean(scenarioId) && isValid;

  const validateValues = async () => {
    const result = await validateForm(values);
    console.info('result', result);
    return Object.keys(result).length === 0;
  };

  const onAward = async () => {
    const isFormValid = await validateValues();
    if (isFormValid) {
      // set scenario ID
    }
  };

  const onSendForReview = async () => {
    const isFormValid = await validateValues();
    if (isFormValid && scenarioId) {
      await onRequestReview(scenarioId, values.text, values.email);
    }
  };

  return (
    <Card title={t('Send for Review')}>
      <Input
        value={values.email}
        label={t('Email')}
        placeholder={t('Write Email Address here')}
        onChange={value => onChange('email', value)}
        onBlur={() => setFieldTouched('email')}
        error={Boolean(errors.email)}
        helperText={errors.email}
      />
      <VerticalSpacer height={16} />
      <TextArea
        value={values.text}
        label={t('Add Comment (1000 characters max)')}
        onChange={value => onChange('text', value)}
        onBlur={() => setFieldTouched('text')}
        error={Boolean(errors.text)}
        rows={5}
      />
      <VerticalSpacer height={16} />
      <StyledButtonContainer>
        <SecondaryButton onClick={onSendForReview} disabled={!buttonsEnabled}>
          {t('Send for Review')}
        </SecondaryButton>
        <SecondaryButton onClick={onAward} disabled={!buttonsEnabled}>
          {t('Award Scenario')}
        </SecondaryButton>
      </StyledButtonContainer>
    </Card>
  );
};
