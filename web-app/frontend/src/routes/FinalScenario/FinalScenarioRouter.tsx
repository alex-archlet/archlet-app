import React from 'react';
import {
  Route,
  Switch,
} from 'react-router-dom';
import { ProjectFinalScenarioContainer } from './containers/ProjectFinalScenario';

interface Props {
  match: {
    path: string;
  };
}

export const FinalScenarioRouter: React.FC<Props> = ({ match }) => (
  <Switch>
    <Route
      path={`${match.path}/`}
      exact
      component={ProjectFinalScenarioContainer}
    />
  </Switch>
);
