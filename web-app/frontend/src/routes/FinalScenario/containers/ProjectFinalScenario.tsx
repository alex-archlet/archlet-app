import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { projectsSelectors } from '@selectors/projects';
import { setBackButton, setPageTitle } from '@actions/interface';
import {
  getScenarioAllocations,
  getScenarios,
  getScenarioKpi,
} from '@actions/projectScenarios';
import { projectScenariosSelectors } from '@selectors/projectScenarios';
import { useCustomTranslation } from '@utils/translate';
import { PageTitle } from '@common/components/PageTitle/PageTitle';
import { Filter } from '@common/components/Filter/Filter';
import { colors } from '@utils/uiTheme';
import { useFilter } from '@common/components/Filter/filterHooks';
import { TYPE } from '@common/components/Filter/filterUtils';
import { Scenario } from '@routes/Scenarios/components/useAllocations';
import { SingleFilterValue } from '@common/components/Filter/types';

import { ProjectsLayout } from '../../ProjectDetails/components/ProjectsLayout/ProjectsLayout';
import { FinalScenarioAllocations } from '../components/FinalScenarioAllocations/FinalScenarioAllocations';
import { SendForReviewCard } from '../components/SendForReviewCard/SendForReviewCard';
import { StatsTable } from '../../ScenarioDetails/components/StatsTable';

interface Params {
  id: string;
}

export const ProjectFinalScenarioContainer = () => {
  const projectDetails = useSelector(projectsSelectors.getProjectDetails);
  const scenarios: Scenario[] = useSelector(projectScenariosSelectors.getScenarios);

  const { t } = useCustomTranslation();

  const dispatch = useDispatch();
  const getScenariosAction = (id: string) => dispatch(getScenarios(id));
  const setBackButtonAction = action => dispatch(setBackButton(action));
  const setPageTitleAction = title => dispatch(setPageTitle(title));
  const getScenarioAllocationsAction = (id: string) =>
    dispatch(getScenarioAllocations(id));
  const getScenarioKpiAction = (id: string, scenarioId: string) => dispatch(getScenarioKpi(id, [scenarioId]));

  const { id } = useParams<Params>();

  useEffect(() => {
    setBackButtonAction('arrow');
    setPageTitleAction(projectDetails?.name);

    return () => {
      setBackButtonAction('');
    };
  }, [projectDetails]);

  useEffect(() => {
    getScenariosAction(id);
  }, [id]);

  const options = scenarios.map(({ id: value, name: label }) => ({
    label,
    value,
  }));

  const supplierFilter = {
    id: 'scenario',
    label: t('Scenario'),
    text: t('Choose final scenario'),
    options,
    type: TYPE.SINGLE,
  };

  const filterDeclaration = [supplierFilter];

  const { filterProps, setFilterState, filterState } = useFilter({
    filterDeclaration,
    reduxFilterId: `${id}_final_scenario`,
  });

  const filterValue = filterState?.scenario as SingleFilterValue;

  const selectedScenario = scenarios.find(({ id: scenarioId }) => {
    return filterValue?.value === scenarioId;
  });

  useEffect(() => {
    if (selectedScenario?.id) {
      getScenarioAllocationsAction(selectedScenario?.id);
      getScenarioKpiAction(id, selectedScenario?.id);
    }
  }, [selectedScenario?.id]);

  return (
    // @ts-ignore
    <ProjectsLayout backgroundColor="#FBFBFB">
      <PageTitle
        title={t('Scenario Reporting')}
        titleColor={colors.textColor}
        rightSide={
          <Filter
            {...filterProps}
            onChange={setFilterState}
            enableClearButton
          />
        }
      />
      <SendForReviewCard scenarioId={selectedScenario?.id} />

      { selectedScenario?.id ? <StatsTable scenarioId={selectedScenario.id} /> : null}

      { selectedScenario?.id ? <FinalScenarioAllocations scenarioId={selectedScenario.id} /> : null}
    </ProjectsLayout>
  );
};
