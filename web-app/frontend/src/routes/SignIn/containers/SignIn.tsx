/* eslint-disable react/button-has-type */
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { signIn } from '@actions/user';
import { setPageTitle } from '@actions/interface';
import { userSelectors } from '@selectors/user';
import { useFormik } from 'formik';
import { useCustomTranslation } from '@utils/translate';
import * as Yup from 'yup';
import { useHistory, useLocation } from 'react-router';
import { SignInForm } from '../components/SignInForm/SignInForm';

export interface FormValues {
  email: string;
  password: string;
  rememberMe: boolean;
}

interface FromState {
  from?: {
    pathname?: string;
    search?: string;
  };
}

export const SignIn: React.FC<void> = () => {
  const { t } = useCustomTranslation();
  const location = useLocation<FromState>();
  const history = useHistory();
  const error = useSelector(userSelectors.error);
  const dispatch = useDispatch();

  const returnToPathName = location.state?.from?.pathname ?? '/';
  const returnToQuery = location.state?.from?.search ?? '';
  const localReturnTo = `${returnToPathName}${returnToQuery}`;
  const returnToWithHostname = `${window.location.origin}${localReturnTo}`;

  useEffect(() => {
    dispatch(setPageTitle(t('Sign in')));
  }, [setPageTitle, t]);

  const ReviewSchema = Yup.object().shape({
    email: Yup.string()
      .email(t('A valid e-mail is required'))
      .required(t('A valid e-mail is required')),
  });

  const onSubmitForm = async (formValues: FormValues) => {
    await dispatch(signIn(formValues));
    history.push(localReturnTo);
  };

  const {
    values,
    errors,
    setFieldValue,
    setFieldTouched,
    handleSubmit,
  } = useFormik<FormValues>({
    initialValues: {
      email: '',
      password: '',
      rememberMe: false,
    },
    validationSchema: ReviewSchema,
    onSubmit: onSubmitForm,
    validateOnBlur: true,
    validateOnChange: false,
  });

  const onChange = (field: string, value: string) => {
    setFieldTouched(field);
    setFieldValue(field, value);
  };

  return (
    <SignInForm
      handleSubmit={handleSubmit}
      onChange={onChange}
      values={values}
      returnTo={returnToWithHostname}
      backendError={error}
      errors={errors}
    />
  );
};
