import React from 'react';
import styled from 'styled-components';

const StyledContainer = styled.div`
  overflow: hidden;
  text-align: center;
`;

const StyledSpan = styled.span`
  position: relative;
  display: inline-block;
  color: #9c9c9c;

  &:before, &:after {
    content: '';
    position: absolute;
    top: 50%;
    border-bottom: 2px solid #9c9c9c;
    width: 100vw;
    margin: 0 20px;
  }

  &:before {
    right: 100%;
  }
  &:after {
    left: 100%;
  }
`
interface Props {
  text: string;
}

export const TextWithLine: React.FC<Props> = ({ text }) => {
  return (
    <StyledContainer>
      <StyledSpan>
        {text}
      </StyledSpan>
    </StyledContainer>
  );
};
