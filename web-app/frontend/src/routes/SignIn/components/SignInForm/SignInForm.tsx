import React from 'react';
import styled from 'styled-components';
import { FormikProps } from 'formik';
import { Link } from 'react-router-dom';

import { Box, Card, Typography } from '@material-ui/core';
import { Input } from '@common/components/Input/Input';
import { colors } from '@utils/uiTheme';
import { Text } from '@common/components/Text/Text';
import { useCustomTranslation } from '@utils/translate';
import { getEnvVariable } from 'src/env';
import { isProdEnv } from 'src/config/config';
import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';
import { SecondaryButton } from '@common/components/Button/SecondaryButton';
import { TextWithLine } from '../TextWithLine/TextWithLine';
import { FormValues } from '../../containers/SignIn';

const StyledCard = styled(Card)`
  display: flex;
  flex-direction: column;
  width: 410px;
  padding: 40px 50px 60px;
  position: relative;
`;

const StyledTitle = styled(Typography)`
  && {
    font-weight: 500;
    text-align: center;
  }
`;

const StyledInput = styled(Input)`
  height: 65px;
  margin-top: 16px;
  margin-bottom: 8px;
`;

const StyledBackendError = styled(Text)`
  height: 34px;
  margin-top: 15px;
`;

const StyledLink = styled(Link)`
  display: block;
  color: ${colors.palette.secondary.main};
  font-size: 12.5px;
  text-decoration: none;
`;

const StyledButton = styled(SecondaryButton)`
  && {
    margin-top: 5px;
    margin-bottom: 16px;
  }
`;

const StyledVersionNumberBox = styled.div`
  position: absolute;
  bottom: 10px;
  text-align: center;
  width: calc(100% - 100px);
  color: #d8d8d8;
`;

const StyledOuterContainer = styled(Box)`
  height: calc(100vh - 64px);
  background-color: #fbfbfb;
`;
interface Global {
  document: Document;
  window: Window;
}

const globalApp: Global & { appVersion?: string } = global;

interface OtherProps {
  backendError: string;
  onChange: (key: string, value: any) => void;
  handleSubmit: () => void;
  returnTo: string;
}

type FormProps = FormikProps<FormValues>;

type Props = OtherProps & Pick<FormProps, 'values' | 'errors'>;

const BACKEND_REST_REQUEST_PREFIX = getEnvVariable(
  'BACKEND_REST_REQUEST_PREFIX',
  'http://localhost:4201/api/v1'
);

const API_PREFIX = isProdEnv
  ? `${window.location.origin}/api/v1`
  : BACKEND_REST_REQUEST_PREFIX;
const SSO_URL = `${API_PREFIX}/auth/login`;

export const SignInForm: React.FC<Props> = ({
  backendError,
  values,
  onChange,
  errors,
  handleSubmit,
  returnTo,
}) => {
  const { t } = useCustomTranslation();

  return (
    <StyledOuterContainer
      display="flex"
      justifyContent="center"
      flexDirection="column"
      alignItems="center"
    >
      <StyledCard elevation={0}>
        <Box>
          <StyledTitle variant="h5">{t('Sign in')}</StyledTitle>
        </Box>
        <StyledBackendError color={colors.red}>
          {backendError}
        </StyledBackendError>
        <StyledButton
          href={`${SSO_URL}?returnTo=${returnTo}`}
          fullWidth
        >
          {t('Sign In with Company Identity (SSO)')}
        </StyledButton>
        <TextWithLine text={t('or use Archlet Login')} />
        <form onSubmit={handleSubmit}>
          <Box display="flex" flexDirection="column">
            <VerticalSpacer height={16} />
            <StyledInput
              value={values.email}
              label={t('Email')}
              type="email"
              placeholder={t('Email')}
              onChange={value => onChange('email', value)}
              error={Boolean(errors.email)}
              helperText={errors.email}
            />
            <VerticalSpacer height={16} />
            <StyledInput
              value={values.password}
              label={t('Password')}
              type="password"
              placeholder={t('Password')}
              onChange={value => onChange('password', value)}
              error={Boolean(errors.password)}
              helperText={errors.password}
            />
            <VerticalSpacer height={16} />
            <StyledButton type="submit" fullWidth>
              {t('Sign In')}
            </StyledButton>
            <Box display="flex" justifyContent="flex-end" alignItems="flex-end">
              <StyledLink
                to={{
                  pathname: '/forgot-password',
                }}
              >
                {t('Forgot Password?')}
              </StyledLink>
            </Box>
          </Box>
        </form>
        <StyledVersionNumberBox>
          {`${t('Archlet version')} ${globalApp.appVersion}`}
        </StyledVersionNumberBox>
      </StyledCard>
    </StyledOuterContainer>
  );
};
