import { nameIdDropdownOption } from '@common/components/Filter/filterUtils';
import { useState, useEffect } from 'react';

export const useBidColumnsOptionForSettings = (
  settings,
  bidColumns,
  bidColumnsOption
) => {
  const [currentSettingBidColumns, setCurrentSettingBidColumns] = useState([]);
  const [
    nonDuplicatedBidColumnOptions,
    setNonDuplicatedBidColumnOptions,
  ] = useState([]);

  useEffect(() => {
    if (settings?.length && bidColumns?.length - 1) {
      const foundBidColumnsSettings = settings
        .map(filter => bidColumns.find(bidColumn => bidColumn.id === filter))
        .filter(Boolean);

      if (foundBidColumnsSettings.length) {
        setCurrentSettingBidColumns(
          foundBidColumnsSettings.map(nameIdDropdownOption)
        );
      }
    } else {
      setCurrentSettingBidColumns([]);
    }
  }, [settings, bidColumns, bidColumnsOption]);

  useEffect(() => {
    if (settings?.length && bidColumnsOption.length) {
      const nonDuplicate = bidColumnsOption.filter(
        bidColumnOption => !settings.includes(bidColumnOption.value)
      );
      setNonDuplicatedBidColumnOptions(nonDuplicate);
    } else {
      setNonDuplicatedBidColumnOptions(bidColumnsOption);
    }
  }, [bidColumnsOption, settings, bidColumns]);

  return {
    currentSettingBidColumns,
    nonDuplicatedBidColumnOptions,
  };
};
