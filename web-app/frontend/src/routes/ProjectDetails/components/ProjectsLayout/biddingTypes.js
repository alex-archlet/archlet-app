export const MANUAL_UPLOAD_ID = 1;
export const BID_COLLECTOR_ID = 2;
export const BID_IMPORTER_ID = 3;

export const MANUAL_UPLOAD_URL = 'data';
export const BID_IMPORTER_URL = 'bid-importer';
export const BID_COLLECTOR_URL = 'bid-collector';

export const BID_TYPE_TO_URL = {
  [BID_COLLECTOR_ID]: BID_COLLECTOR_URL,
  [BID_IMPORTER_ID]: BID_IMPORTER_URL,
  [MANUAL_UPLOAD_ID]: MANUAL_UPLOAD_URL,
};
