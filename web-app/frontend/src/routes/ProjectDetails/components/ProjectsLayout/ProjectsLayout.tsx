import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { useHistory, useParams } from 'react-router';
import { connect, useDispatch, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';
import clsx from 'clsx';
import { setPageTitle } from '@actions/interface';
import {
  BarChart,
  CheckCircleOutline,
  FormatListBulleted,
  OpenInBrowser,
  Share,
  Tune,
} from '@material-ui/icons';
import { Box } from '@material-ui/core';

import { colors, drawer } from '@utils/uiTheme';
import { ConfirmationDialog } from '@common/components/ConfirmationDialog/ConfirmationDialog';
import { CancelButton } from '@common/components/Button/CancelButton';
import { DeleteButton } from '@common/components/Button/DeleteButton';
import { IconImageBeta } from '@common/components/ImageWrapper/ImageWapper';
import { Sidebar } from '@common/components/Sidebar/Sidebar';
import { SidebarItem } from '@common/components/Sidebar/SidebarItem';
import { projectsSelectors } from '@selectors/projects';
import { useCustomTranslation } from '@utils/translate';
import { BackgroundContainer } from '@common/components/BackgroundContainer/BackgroundContainer';

import { deleteRound } from '@store/actions/biddingRounds';
import { sidebarSelectors } from '@store/selectors/sidebar';
import {
  BID_COLLECTOR_ID,
  BID_IMPORTER_ID,
  MANUAL_UPLOAD_ID,
} from './biddingTypes';
import { BidCollectorSidebar } from '../../../Bid/BidCollector/components/BidCollectorSidebar/BidCollectorSidebar';
import { BidImporterSidebarContainer } from '../../../Bid/BidImporter/components/BidImporterSidebar/BidImporterSidebar';

const StyledBox = styled(Box)`
  flex: 1;
  padding-top: 25px;
`;

const StyledLayout = styled.div`
  display: flex;
`;

const StyledDrawerFooter = styled.div`
  display: flex;
  align-items: center;
  height: 55px;
  border-top: 1px solid rgba(0, 0, 0, 0.12);
`;

type URL = {
  id: string;
  roundId: string;
};

export type Match = {
  params: URL;
};

type ProjectDetails = {
  id: string;
  name: string;
  status: string;
  settings: any;
  categoryId: number;
  unitId: number;
  currencyId: number;
  biddingTypeId: number;
  categoryName: string;
  typeName: string;
};

export interface MatchParams {
  id: string;
  roundId: string;
}

interface Props {
  children: React.ReactNode;
  projectDetails: ProjectDetails;
  setPageTitleAction: (params: any) => void;
  deleteRoundAction?: (projectId: string, roundId: string) => Promise<void>;
  saveProgressAction?: (params: any) => void;
  backgroundColor?: string;
  sidebarLevel?: number;
}

export const ProjectsLayoutComponent: React.FC<Props> = ({
  children,
  saveProgressAction = () => {},
  projectDetails,
  setPageTitleAction,
  backgroundColor = colors.white,
  sidebarLevel = 1
}) => {
  const { id, roundId } = useParams<MatchParams>();
  const dispatch = useDispatch();
  useEffect(() => {
    if (projectDetails?.name) {
      setPageTitleAction(projectDetails?.name);
    }
  }, [projectDetails?.name]);

  const { biddingTypeId } = projectDetails;

  const history = useHistory();

  const [deleteRoundId, setDeleteRoundId] = useState<string | null>(null);

  const isSecondaryOpen = sidebarLevel === 2;

  const handleDeleteRound = async () => {
    try {
      if (id && roundId) {
        await dispatch(deleteRound(id, roundId));
        if (biddingTypeId === BID_IMPORTER_ID) {
          history.push(`/projects/${id}/bid-importer`);
        } else {
          history.push(`/projects/${id}/bid-collector`);
        }
      }
    } catch (error) {
      console.error(error);
    }
  };

  const isSidebarOpen = useSelector(sidebarSelectors.getSidebarSettings);
  
  const handleDeleteRoundButtonClick = () => setDeleteRoundId(roundId);
  const { t } = useCustomTranslation();

  const getFileUploadItem = () => {
    switch (biddingTypeId) {
      case BID_COLLECTOR_ID:
        return (
          <SidebarItem
            icon={<Share />}
            isActive={false}
            name={t('Bid Collector')}
            to={`/projects/${id}/bid-collector`}
            isTooltip={isSecondaryOpen}
          >
            {sidebarLevel === 2 && (
              <BidCollectorSidebar
                id={roundId}
                higherLevelPath={`/projects/${id}/bid-collector`}
                saveProgressAction={saveProgressAction}
                // @ts-ignore
                handleDeleteRoundButtonClick={handleDeleteRoundButtonClick}
              />
            )}
          </SidebarItem>
        );
      case BID_IMPORTER_ID:
        return (
          <SidebarItem
            icon={<FormatListBulleted />}
            isActive={false}
            name={t('Bidding Rounds')}
            to={`/projects/${id}/bid-importer`}
            sideIcon={<IconImageBeta />}
            sideIconTooltip={t(
              'Feature that was recently added in beta version'
            )}
            isTooltip={isSecondaryOpen}
          >
            {sidebarLevel === 2 && (
              <BidImporterSidebarContainer
                id={roundId}
                higherLevelPath={`/projects/${id}/bid-importer`}
                saveProgressAction={saveProgressAction}
                // @ts-ignore
                handleDeleteRoundButtonClick={handleDeleteRoundButtonClick}
              />
            )}
          </SidebarItem>
        );
      case MANUAL_UPLOAD_ID:
      default:
        return (
          <SidebarItem
            icon={<OpenInBrowser />}
            name={t('Data')}
            to={`/projects/${id}/data`}
            isTooltip={isSecondaryOpen}
          />
        );
    }
  };

  return (
    <StyledLayout>
      <ConfirmationDialog
        isOpen={!!deleteRoundId}
        handleClose={() => setDeleteRoundId(null)}
        ButtonActions={
          <>
            <DeleteButton onClick={handleDeleteRound} autoFocus>
              {t('Delete')}
            </DeleteButton>
            <CancelButton onClick={() => setDeleteRoundId(null)}>
              {t('No')}
            </CancelButton>
          </>
        }
        message={t('Are you sure you want to delete this round?')}
      />

      {!!id && (
        <Sidebar isSecondary={isSecondaryOpen}>
          <StyledBox>
            {getFileUploadItem()}
            <SidebarItem
              icon={<BarChart />}
              name={t('Analytics & Insights')}
              to={`/projects/${id}/analytics`}
              isTooltip={isSecondaryOpen}
            />
            <SidebarItem
              icon={<Tune />}
              name={t('Optimization & Scenarios')}
              to={`/projects/${id}/scenarios`}
              isTooltip={isSecondaryOpen}
            />
            <SidebarItem
              icon={<CheckCircleOutline />}
              name={t('Final Scenario')}
              to={`/projects/${id}/final-scenario`}
              isTooltip={isSecondaryOpen}
            />
          </StyledBox>

          <StyledDrawerFooter>
            <SidebarItem
              icon={<Tune />}
              isActive
              name={t('Settings')}
              to={`/projects/${id}/settings`}
              isTooltip={isSecondaryOpen}
            />
          </StyledDrawerFooter>
        </Sidebar>
      )}

      <BackgroundContainer
        backgroundColor={backgroundColor}
        className={clsx({
          'contentShift': isSidebarOpen,
        })}
        marginLeft={Number(isSecondaryOpen ? `${drawer.widthSecondLevel}` : `${drawer.width}`)}
      >
        {children}
      </BackgroundContainer>
    </StyledLayout>
  );
};


const mapStateToProps = state => ({
  projectDetails: projectsSelectors.getProjectDetails(state),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setPageTitleAction: setPageTitle,
    },
    dispatch
  );

export const ProjectsLayout = connect(
  mapStateToProps,
  mapDispatchToProps
  // @ts-ignore
)(ProjectsLayoutComponent);
