import React, { useMemo } from 'react';
import styled from 'styled-components';
import { Input } from '@common/components/Input/Input';
import { Box } from '@material-ui/core';
import { useCustomTranslation } from '@utils/translate';
import { FilterDropdown } from '@common/components/Filter/FilterDropdown/FilterDropdown';
import { TYPE } from '@common/components/Filter/filterUtils';
import { projectsSelectors } from '@selectors/projects';
import { useSelector } from 'react-redux';

const StyledTitle = styled.div`
  font-size: 16px;
  line-height: 21px;
  margin-bottom: 4px;
  padding: 0 12px 0 0;
`;

export const ProjectSettingsForm = ({ formValues, onFormChange }) => {
  const { t } = useCustomTranslation();
  const projectCurrencies = useSelector(projectsSelectors.getCurrencies);
  const projectUnits = useSelector(projectsSelectors.getUnits);

  const memoizedProjectUnitsValue = useMemo(() => {
    if (projectUnits.length === 0 || !formValues?.unitId) {
      return [];
    }
    const foundUnit = projectUnits.find(
      projectUnit => projectUnit.id === formValues.unitId
    );

    return {
      label: foundUnit.name,
      value: foundUnit.id,
    };
  }, [formValues.unitId, projectUnits]);

  const memoizedProjectCurrenciesValue = useMemo(() => {
    if (projectCurrencies.length === 0 || !formValues?.currencyId) {
      return [];
    }
    const foundCurrency = projectCurrencies.find(
      projectCurrency => projectCurrency.id === formValues.currencyId
    );

    return {
      label: foundCurrency.code,
      value: foundCurrency.id,
    };
  }, [formValues.currencyId, projectCurrencies]);

  const memoizedProjectCurrenciesOption = useMemo(() => {
    return projectCurrencies.map(({ id, code }) => ({
      label: code,
      value: id,
    }));
  }, [projectCurrencies]);

  const memoizedProjectUnitsOption = useMemo(() => {
    return projectUnits.map(({ id, name }) => ({ label: name, value: id }));
  }, [projectUnits]);

  return (
    <Box width="400px">
      <Box display="flex" alignItems="center" width="200px">
        <Box>
          <StyledTitle>{t('Project Name')}</StyledTitle>
          <Input
            value={formValues.name}
            height="10px"
            placeholder={t('Chemical Tender 2020')}
            onChange={value => onFormChange('name', value)}
          />
        </Box>
      </Box>
      <Box
        display="flex"
        justifyContent="space-between"
        alignItems="center"
        mt={10}
      >
        <Box>
          <StyledTitle>{t('Currency')}</StyledTitle>
          <FilterDropdown
            values={memoizedProjectCurrenciesValue}
            options={memoizedProjectCurrenciesOption}
            type={TYPE.SINGLE}
            clearable={false}
            onChange={option => onFormChange('currencyId', option.value)}
            label={t('Choose...')}
          />
        </Box>
        <Box>
          <StyledTitle>{t('Unit of Measurement')}</StyledTitle>
          <FilterDropdown
            values={memoizedProjectUnitsValue}
            options={memoizedProjectUnitsOption}
            type={TYPE.SINGLE}
            clearable={false}
            onChange={option => onFormChange('unitId', option.value)}
            label={t('Choose...')}
          />
        </Box>
      </Box>
    </Box>
  );
};
