import React from 'react';
import styled from 'styled-components';
import { Card } from '@common/components/Card/Card';
import { SingleFilterValue } from '@common/components/Filter/types';
import { Checkbox } from '@common/components/Checkbox/Checkbox';
import { FilterDropdown } from '@common/components/Filter/FilterDropdown/FilterDropdown';
import { Text } from '@common/components/Text/Text';
import { useSelector } from 'react-redux';
import { useCustomTranslation } from '@utils/translate';
import { TYPE } from '@common/components/Filter/filterUtils';
import { Autocomplete } from '@common/components/Autocomplete/Autocomplete';
import { biddingRoundsSelectors } from '@store/selectors/biddingRounds';
import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';
import { Box, Grid } from '@material-ui/core';
import { Row } from '@common/components/Row/Row';
import { colors } from '@utils/uiTheme';
import { useBidColumnsOptionForSettings } from '../hook/useBidColumnsOptionForSetting';

const parserProjectFilter = (element: SingleFilterValue) => element.value;

interface Props {
  onFormChange: Function;
  formValues: { second_axis: string[] };
  projectSettingsKey: string;
}

const StyledGridContainer = styled(Grid)`
  && {
    align-items: center;
    border-bottom: 1px solid ${colors.grey};
    padding: 20px;

    &:hover {
      background-color: #fafafa;
    }
  }
`;

const StyledSubTitleText = styled(Text)`
  color: #2c2c2c;
  line-height: 20px;
  opacity: 0.4;
`;

export const AnalyticProjectSettings: React.FC<Props> = ({
  onFormChange,
  formValues,
  projectSettingsKey,
}) => {
  const { t } = useCustomTranslation();

  const analyticsOptions = [
    {
      code: 'analytics_total_difference',
      subTitle: t('Show total spend differences for each item in the table.'),
      title: t('Total Spend'),
    },
    {
      code: 'analytics_unit_difference',
      subTitle: t('Show unit price differences for each item in the table.'),
      title: t('Unit Price'),
    },
    {
      code: 'analytics_percentage_difference',
      subTitle: t(
        'Show percentage differences of prices for each item in the table.'
      ),
      title: t('Percentage'),
    },
  ];

  const bidColumns = useSelector(
    biddingRoundsSelectors.getColumnsForTableDesign
  );

  const bidColumnsOptions = useSelector(
    biddingRoundsSelectors.getBidColumnsOptions
  );

  const hasNoBidColumns = useSelector(biddingRoundsSelectors.hasNoBidColumns);

  const {
    currentSettingBidColumns: currentCustomColumnSettingBidColumns,
    nonDuplicatedBidColumnOptions: currentCustomColumnSettingBidColumnsOptions,
  } = useBidColumnsOptionForSettings(
    formValues[projectSettingsKey],
    bidColumns,
    bidColumnsOptions
  );

  const bidColumnsSupplierOptions = useSelector(
    biddingRoundsSelectors.getNumericColumnsForSupplierOptions
  );
  const bidColumnsNumeric = useSelector(
    biddingRoundsSelectors.getNumericSupplierBidColumns
  );

  const {
    currentSettingBidColumns,
    nonDuplicatedBidColumnOptions,
  } = useBidColumnsOptionForSettings(
    formValues.second_axis,
    bidColumnsNumeric,
    bidColumnsSupplierOptions
  );

  const onChange = (secondAxisValue) => {
    if (secondAxisValue) {
      onFormChange('second_axis', [secondAxisValue.value]);
    } else {
      onFormChange('second_axis', []);
    }
  };

  return (
    <>
      <VerticalSpacer height={10} />
      <Row>
        {hasNoBidColumns ? (
          <Card
            title={t('Second Axis')}
            description={t(
              'You can define here a numeric column that is used as a second axis in the Analytics section.'
            )}
          >
            <VerticalSpacer height={10} />
            <Box ml={10} mb={10}>
              <FilterDropdown
                values={currentSettingBidColumns}
                options={nonDuplicatedBidColumnOptions}
                type={TYPE.SINGLE}
                clearable
                onChange={value => onChange(value)}
                label={t('Choose...')}
              />
            </Box>
          </Card>
        ) : (
          <Card
            title={t('Second Axis')}
            description={t('Please upload your data first.')}
          >
            <VerticalSpacer height={10} />
          </Card>
        )}
        {hasNoBidColumns ? (
          <Card
            title={t('Ranking Columns')}
            description={t(
              'You can specify here which data columns you want to consider in the Supplier Ranking.'
            )}
          >
            <VerticalSpacer height={10} />
            <Box ml={10}>
              <Autocomplete
                options={currentCustomColumnSettingBidColumnsOptions}
                value={currentCustomColumnSettingBidColumns}
                placeholder={t('Add Column')}
                onChange={(event, values) =>
                  onFormChange(
                    projectSettingsKey,
                    values.map(parserProjectFilter)
                  )
                }
              />
            </Box>
          </Card>
        ) : (
          <Card
            title={t('Ranking Columns')}
            description={t('Please upload your data first.')}
          >
            <div />
          </Card>
        )}
      </Row>
      <Row>
        <Card
          title={t('Analytics Comparison')}
          description={t(
            'You can select here which price comparisons you want to see in the Supplier Feedback'
          )}
        >
          <VerticalSpacer height={10} />
          <Card
            style={{ margin: '0px', 'margin-left': '10px', padding: '0px 0px' }}
          >
            {analyticsOptions.map(analyticsOption => (
              <StyledGridContainer>
                <Box display="flex" justifyContent="space-between">
                  <Box>
                    {analyticsOption.title}
                    <Grid>
                      <StyledSubTitleText>
                        {analyticsOption.subTitle}
                      </StyledSubTitleText>
                    </Grid>
                  </Box>
                  <>
                    <Checkbox
                      value={formValues[analyticsOption.code]}
                      onChange={event =>
                        onFormChange(analyticsOption.code, event.target.checked)
                      }
                    />
                  </>
                </Box>
              </StyledGridContainer>
            ))}
          </Card>
        </Card>
        <Box width="calc(50% - 5px)" />
      </Row>
    </>
  );
};
