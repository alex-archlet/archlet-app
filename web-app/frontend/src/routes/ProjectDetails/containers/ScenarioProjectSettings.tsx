import React from 'react';
import { Card } from '@common/components/Card/Card';
import { Box } from '@material-ui/core';
import { Autocomplete } from '@common/components/Autocomplete/Autocomplete';
import { useCustomTranslation } from '@utils/translate';
import { useSelector } from 'react-redux';
import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';
import { biddingRoundsSelectors } from '@store/selectors/biddingRounds';
import { Row } from '@common/components/Row/Row';
import { useBidColumnsOptionForSettings } from '../hook/useBidColumnsOptionForSetting';

interface Props {
  onFormChange: Function;
  formValues: string[];
  projectSettingsKey: string;
}

const parserProjectFilter = ({ value }) => value;

export const ScenarioProjectSettings: React.FC<Props> = ({
  onFormChange,
  formValues,
  projectSettingsKey,
}) => {
  const { t } = useCustomTranslation();

  const supplierNumericBidColumns = useSelector(
    biddingRoundsSelectors.getNumericSupplierBidColumns
  );

  const hasNoBidColumns = useSelector(biddingRoundsSelectors.hasNoBidColumns);

  const supplierNumericBidColumnOptions = useSelector(
    biddingRoundsSelectors.getNumericColumnsForSupplierOptions
  );

  const {
    currentSettingBidColumns,
    nonDuplicatedBidColumnOptions,
  } = useBidColumnsOptionForSettings(
    formValues[projectSettingsKey],
    supplierNumericBidColumns,
    supplierNumericBidColumnOptions
  );

  return (
    <>
      <VerticalSpacer height={10} />
      <Row>
        {hasNoBidColumns ? (
          <Card
            title={t('Soft Constraints')}
            description={t(
              // eslint-disable-next-line max-len
              'Here, you can select the columns that you want to use as soft constraints in the scenario creation. Only numeric columns can be selected here.'
            )}
          >
            <VerticalSpacer height={10} />
            <Box ml={10}>
              <Autocomplete
                options={nonDuplicatedBidColumnOptions}
                value={currentSettingBidColumns}
                placeholder={t('Add Soft Constraint')}
                onChange={(event, values) =>
                  onFormChange(
                    projectSettingsKey,
                    values.map(parserProjectFilter)
                  )
                }
              />
            </Box>
          </Card>
        ) : (
          <Card
            title={t('Soft Constraints')}
            description={t('Please upload your data first.')}
          >
            <VerticalSpacer height={10} />
          </Card>
        )}
        <Box width="50%" />
      </Row>
    </>
  );
};
