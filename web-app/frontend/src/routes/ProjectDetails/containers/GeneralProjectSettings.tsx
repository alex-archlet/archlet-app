import React from 'react';
import { Card } from '@common/components/Card/Card';
import { Box } from '@material-ui/core';
import { Autocomplete } from '@common/components/Autocomplete/Autocomplete';
import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';
import { useCustomTranslation } from '@utils/translate';
import { biddingRoundsSelectors } from '@store/selectors/biddingRounds';
import { useSelector } from 'react-redux';
import { Row } from '@common/components/Row/Row';
import { ProjectSettingsForm } from '../components/ProjectSettingsForm/ProjectSettingsForm';
import { useBidColumnsOptionForSettings } from '../hook/useBidColumnsOptionForSetting';

const parserProjectFilter = ({ value }) => value;

interface Props {
  onFormChange: Function;
  formValues: string[];
  projectSettingsKey: string;
}

export const GeneralProjectSettings: React.FC<Props> = ({
  onFormChange,
  formValues,
  projectSettingsKey,
}) => {
  const { t } = useCustomTranslation();

  const bidColumns = useSelector(
    biddingRoundsSelectors.getColumnsForTableContent
  );

  const bidColumnsOption = useSelector(
    biddingRoundsSelectors.getColumnsForBuyerOptions
  );

  const hasNoBidColumns = useSelector(biddingRoundsSelectors.hasNoBidColumns);

  const {
    currentSettingBidColumns,
    nonDuplicatedBidColumnOptions,
  } = useBidColumnsOptionForSettings(
    formValues[projectSettingsKey],
    bidColumns,
    bidColumnsOption
  );

  return (
    <>
      <VerticalSpacer height={10} />
      <Row>
        <Card title={t('Project Details')}>
          <Box ml={10}>
            <ProjectSettingsForm
              formValues={formValues}
              onFormChange={onFormChange}
            />
            <VerticalSpacer height={10} />
          </Box>
        </Card>
        {hasNoBidColumns ? (
          <Card
            title={t('Filters')}
            description={t(
              // eslint-disable-next-line max-len
              'Here, you can select the columns that you want to use as filters in this sourcing project. Until now, only columns can be selected that provided by the buyer.'
            )}
          >
            <VerticalSpacer height={10} />
            <Box ml={10}>
              <Autocomplete
                options={nonDuplicatedBidColumnOptions}
                value={currentSettingBidColumns}
                placeholder={t('Add Filter')}
                onChange={(event, values) => {
                  if (values.length) {
                    onFormChange(
                      projectSettingsKey,
                      values.map(parserProjectFilter)
                    );
                  }
                }}
              />
            </Box>
          </Card>
        ) : (
          <Card
            title={t('Filters')}
            description={t('Please upload your data first.')}
          >
            <div />
          </Card>
        )}
      </Row>
    </>
  );
};
