import React, { useEffect } from 'react';
import { connect, useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation, useParams } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { useFormik } from 'formik';

import { projectsSelectors } from '@selectors/projects';
import {
  getProjectCurrencies,
  getProjectDetails,
  getProjectUnits,
  updateProject,
  updateProjectSettings,
} from '@actions/projects';
import { setBackButton, setPageTitle } from '@actions/interface';
import { useTabParams } from '@common/components/Tabs/useTabParams';
import { LeavingDialog } from '@common/components/LeavingDialog/LeavingDialog';
import { Tabs } from '@common/components/Tabs/Tabs';
import { useCustomTranslation } from '@utils/translate';
import { BaseLayout } from '@common/containers/Layout/BaseLayout';
import { GeneralProjectSettings } from './GeneralProjectSettings';
import { AnalyticProjectSettings } from './AnalyticProjectSettings';
import { ScenarioProjectSettings } from './ScenarioProjectSettings';

const closingNavigate = (location, history) => {
  if (location?.state?.prevPath) {
    return history.push(location?.state?.prevPath);
  }

  return history.goBack();
};

const ProjectSettings = ({
  getProjectDetailsAction,
  getProjectCurrenciesAction,
  getProjectUnitsAction,
  setBackButtonAction,
  setPageTitleAction,
}) => {
  const history = useHistory();
  const location = useLocation();
  // @ts-ignore
  const { id } = useParams();
  const { t } = useCustomTranslation();
  const dispatch = useDispatch();
  const projectSettings = useSelector(projectsSelectors.getSettings);
  const projectDetails = useSelector(projectsSelectors.getProjectDetails);

  useEffect(() => {
    // @ts-ignore
    setBackButtonAction('close', () => closingNavigate(location, history));
    getProjectDetailsAction(id).then(project =>
      setPageTitleAction(project.name)
    );
    getProjectCurrenciesAction();
    getProjectUnitsAction();

    return () => setBackButtonAction('');
  }, [id]);

  const { values, setFieldValue, setFieldTouched } = useFormik({
    initialValues: {
      ...projectSettings,
      name: projectDetails.name,
      currencyId: projectDetails.currencyId,
      unitId: projectDetails.unitId,
    },
    onSubmit: console.info,
    validateOnBlur: true,
    validateOnChange: true,
    enableReinitialize: true,
  });

  const onFormChange = (field: string, value: string[]) => {
    setFieldTouched(field);
    setFieldValue(field, value);
  };

  const tabs = [
    {
      disabled: false,
      id: 'general',
      label: t('General'),
      render: () => (
        <GeneralProjectSettings
          onFormChange={onFormChange}
          formValues={values}
          projectSettingsKey="filters"
        />
      ),
    },
    {
      disabled: false,
      id: 'analytics',
      label: t('Analytics'),
      render: () => (
        <AnalyticProjectSettings
          onFormChange={onFormChange}
          formValues={values}
          projectSettingsKey="custom_columns"
        />
      ),
    },
    {
      disabled: false,
      id: 'scenario',
      label: t('Scenario'),
      render: () => (
        <ScenarioProjectSettings
          onFormChange={onFormChange}
          formValues={values}
          projectSettingsKey="soft_constraints"
        />
      ),
    },
  ];

  const { tab, onChange } = useTabParams({
    options: ['general', 'analytics', 'scenario'],
    initialValue: 'general',
  });

  return (
    <BaseLayout>
      <LeavingDialog
        when
        confirmedCallback={async () => {
          await dispatch(updateProjectSettings(id, values));
          await dispatch(
            updateProject({
              ...projectDetails,
              ...{
                name: values.name,
                currencyId: values.currencyId,
                unitId: values.unitId,
              },
            })
          );
        }}
      />
      <Tabs tabs={tabs} initialValue={tab} onChange={onChange} />
    </BaseLayout>
  );
};

const mapStateToProps = state => ({
  projectCurrencies: projectsSelectors.getCurrencies(state),
  projectDetails: projectsSelectors.getProjectDetails(state),
  projectUnits: projectsSelectors.getUnits(state),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getProjectCurrenciesAction: getProjectCurrencies,
      getProjectDetailsAction: getProjectDetails,
      getProjectUnitsAction: getProjectUnits,
      setBackButtonAction: setBackButton,
      setPageTitleAction: setPageTitle,
      updateProjectAction: updateProject,
    },
    dispatch
  );

export const ProjectSettingsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProjectSettings);
