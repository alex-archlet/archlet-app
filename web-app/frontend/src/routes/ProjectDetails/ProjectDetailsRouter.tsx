import React from 'react';
import {
  Route,
  Switch,
} from 'react-router-dom';
import { ProjectSettingsContainer } from './containers/ProjectSettings';
import { ScenariosRouter } from '../Scenarios/ScenariosRouter';
import { ScenarioDetailsRouter } from '../ScenarioDetails/ScenarioDetailsRouter';
import { DataRouter } from '../Data/DataRouter';
import { FinalScenarioRouter } from '../FinalScenario/FinalScenarioRouter';
import { ShareRouter } from '../Share/ShareRouter';
import { AnalyticsDetailsContainer } from '../Analytics/AnalyticsDetails';
import { BidCollectorRouter } from '../Bid/BidCollector/BidCollectorRouter';
import { BidImporterRouter } from '../Bid/BidImporter/BidImporterRouter';
import { OverviewContainer } from '../Bid/Overview';
import { useFetchProjectInfo } from './useFetchProjectInfo';

export const ProjectDetailsRouter = ({ match }) => {

  useFetchProjectInfo();

  return (
    <Switch>
      <Route
        path={`${match.path}/settings`}
        component={ProjectSettingsContainer}
      />
      <Route
        path={`${match.path}/bid-collector/:roundId`}
        component={BidCollectorRouter}
      />
      <Route
        path={`${match.path}/bid-importer/:roundId`}
        component={BidImporterRouter}
      />
      <Route
        path={`${match.path}/bid-collector/`}
        exact
        component={OverviewContainer}
      />
      <Route
        path={`${match.path}/bid-importer/`}
        exact
        component={OverviewContainer}
      />
      <Route
        path={`${match.path}/data`}
        component={DataRouter}
      />
      <Route
        path={`${match.path}/scenarios`}
        component={ScenariosRouter}
      />
      <Route
        path={`${match.path}/scenariosDetails/:scenarioId`}
        component={ScenarioDetailsRouter}
      />
      <Route
        path={`${match.path}/final-scenario`}
        component={FinalScenarioRouter}
      />
      <Route
        path={`${match.path}/share`}
        component={ShareRouter}
      />
      <Route
        path={`${match.path}/analytics`}
        component={AnalyticsDetailsContainer}
      />
    </Switch>
  );
};
