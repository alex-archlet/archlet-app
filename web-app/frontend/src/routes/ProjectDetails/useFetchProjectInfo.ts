import { useDispatch, useSelector } from 'react-redux';
import { useParams, useLocation } from 'react-router';
import { useEffect } from 'react';

import { getProjectDetails, getProjectInfo, getProjectKpiDefs } from '@store/actions/projects';
import { getRoundTableDesign } from '@store/actions/biddingRounds';
import { projectsSelectors } from '@store/selectors/projects';

interface MatchParams {
  id: string;
  roundId: string;
}

export const useFetchProjectInfo = () => {
  const projectDetails = useSelector(projectsSelectors.getProjectDetails);
  const dispatch = useDispatch();
  const { id } = useParams<MatchParams>();
  const location = useLocation();

  const getProjectDetailsAction = (projectId: string) =>
    dispatch(getProjectDetails(projectId));
  const getProjectInfoAction = (projectId: string) =>
    dispatch(getProjectInfo(projectId));
  const getRoundTableDesignAction = (projectId: string) =>
    dispatch(getRoundTableDesign(projectId));
  const getProjectKpiDefsAction = (projectId: string) =>
    dispatch(getProjectKpiDefs(projectId));

  useEffect(() => {
    getRoundTableDesignAction(id);
    getProjectKpiDefsAction(id);
  }, [id]);

  useEffect(() => {
    getProjectDetailsAction(id);
  }, [id, location.pathname]);
  
  useEffect(() => {
    if (projectDetails?.updated_at) {
      getProjectInfoAction(id);
    }
  }, [projectDetails?.updated_at]);
};
