import React, { useState, useMemo, useEffect } from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

import { projectScenariosSelectors } from '@selectors/projectScenarios';
import { useCustomTranslation } from '@utils/translate';
import { MultiSelectTable } from '@common/components/Table/MultiSelectTable';
import { Table, CustomColumn } from '@common/components/Table/Table';
import { projectsSelectors } from '@selectors/projects';
import { formatCell } from '@common/components/Table/utils';
import { Filter } from '@common/components/Filter/Filter';
import { Flex } from '@common/components/Flex/Flex';
import { Box, Dialog } from '@material-ui/core';
import { SyncAlt, Close } from '@material-ui/icons';
import { Text } from '@common/components/Text/Text';
import { Tooltip } from '@common/components/Tooltip/Tooltip';
import { MultiFilterValue } from '@common/components/Filter/types';
import { ScenarioProgressContainer } from '@common/components/ScenarioProgress/ScenarioProgressContainer';
import { colors } from '@utils/uiTheme';

import { LocaleFormatNumber } from '@common/components/FormatNumber/FormatNumber';
import { Card } from '../../../common/components/Card/Card';
import { TYPE } from '../../../common/components/Filter/filterUtils';
import { useFilter } from '../../../common/components/Filter/filterHooks';

import { AllocationsDownload } from './AllocationsDownload';
import { ManualAllocations } from './ManualAllocations';

import { useAllocationFilter } from './useAllocationFilter';
import { DemandAttributesMap } from '../containers/types';

const StyledDownloadWrapper = styled.div`
  margin-left: 16px;
`;

const StyledIconContainer = styled.div`
  margin-top: 3px;
  margin-left: 7px;
`;

const StyledToRightIconContainer = styled.div`
  cursor: pointer;
  position: absolute;
  top: 4px;
  right: 8px;
`;

const StyledCloseIcon = styled(Close)`
  && {
    fill: ${colors.iconGrey};
    width: 22px;
    cursor: pointer;
    opacity: 0.38;
  }
`;

export type MultiSelectAllocationTable = {
  item: string;
  demandId: string;
  offerId: string;
};

export type Selected = {
  [rowId: string]: MultiSelectAllocationTable;
};

interface Props {
  scenarioId: string;
  isSelectable?: boolean;
}
const getFilterBidColumnsMap = filterBidColumn => ({
  label: filterBidColumn.name,
  value: filterBidColumn.id,
});

const DEFAULT_SORTING = [
  {
    desc: false,
    id: 'item',
  },
];

export const Allocations: React.FC<Props> = ({ isSelectable, scenarioId }) => {
  const [dialog, setDialog] = useState(false);
  const [demandAttributesState, setDemandAttributesState] = useState<
    DemandAttributesMap[]
  >([]);

  const { t } = useCustomTranslation();

  const scenarioAllocations =
    useSelector(
      projectScenariosSelectors.makeGetScenarioAllocations(scenarioId)
    ) ?? [];
  const bidColumns =
    useSelector(projectScenariosSelectors.getScenarioBidColumns) || [];
  const filterBidColumns = useSelector(projectsSelectors.getFilterBidColumns);
  const kpiDefs = useSelector(projectsSelectors.getCustomProjectKpiDefs);

  const allFilterColumns = useMemo(() => {
    return [...kpiDefs.map(getFilterBidColumnsMap), ...bidColumns];
  }, [kpiDefs, bidColumns]);

  const filterDeclaration = [
    {
      clearable: true,
      id: 'columns',
      label: t('Columns'),
      options: allFilterColumns,
      type: TYPE.MULTI,
    },
  ];

  const initialColumnValues = useMemo(() => {
    return filterBidColumns.map(getFilterBidColumnsMap);
  }, [filterBidColumns]);

  const {
    filterProps: columnFilterProps,
    filterState: columnFilterState,
    setFilterState: columnSetFilterState,
  } = useFilter({
    filterDeclaration,
    initialValuesMap: {
      columns: initialColumnValues,
    },
    reduxFilterId: `${scenarioId}_allocations`,
  });

  useEffect(() => {
    if (scenarioAllocations.length) {
      setDemandAttributesState(
        scenarioAllocations.map(({ demandAttributes }) => demandAttributes)
      );
    }
  }, [scenarioAllocations]);

  const { filterProps, setFilterState, filterFunction } = useAllocationFilter({
    filterCombinations: demandAttributesState,
  });

  const tableRows = useMemo(
    () =>
      scenarioAllocations.map(
        ({
          totalPrice,
          volume,
          isManual,
          demandAttributes,
          offerAttributes,
          kpiAttributes,
          ...rest
        }) => ({
          ...demandAttributes,
          ...offerAttributes,
          ...kpiAttributes,
          ...rest,
          totalPrice,
          volume,
          isManual,
        })
      ),
    [scenarioAllocations]
  );
  const filteredTableRows = filterFunction(tableRows);

  const columns = (columnFilterState.columns as MultiFilterValue) || [];

  const allColumns = useMemo(() => {
    const additionalColumns: CustomColumn[] = columns.map(
      ({ label, value }) => ({
        Cell: ({ cell: { value: cellValue } }) =>
          cellValue ? (
            <LocaleFormatNumber value={cellValue} type={{ decimals: 2 }} />
          ) : (
            '-'
          ),
        Header: label,
        accessor: value as string,
        id: value as string,
      })
    );
    const regularColumns: CustomColumn[] = [
      {
        Cell: ({ cell: { row } }) => {
          const { isManual, item } = row.original;
          return (
            <Flex align="center">
              <Text>{item}</Text>
              {isManual && (
                <Tooltip title={t('Manual Allocation')}>
                  <StyledIconContainer>
                    <SyncAlt />
                  </StyledIconContainer>
                </Tooltip>
              )}
            </Flex>
          );
        },
        accessor: 'item',
        Header: t('Item'),
        id: 'item',
        fixed: true,
      },
      {
        Header: t('Supplier') as string,
        accessor: row => (row.supplier ? row.supplier : '-'),
        id: 'supplier',
      },
      {
        Cell: formatCell,
        Header: t('Volume') as string,
        accessor: row => (row.volume ? row.volume : '-'),
        id: 'volume',
      },
      {
        Cell: formatCell,
        Header: t('Total Spend') as string,
        accessor: row => (row.totalPrice ? row.totalPrice : '-'),
        id: 'totalPrice',
      },
    ];
    return regularColumns.concat(additionalColumns);
  }, [columns, t]);

  const [selected, setSelected] = useState<Selected>({});

  const onChange = (row) => {
    const existingRow = selected[row.id];

    if (existingRow) {
      const { [row.id]: _, ...newObject } = selected;
      return setSelected(newObject);
    }
    return setSelected(prev => ({
      ...prev,
      [row.id]: {
        item: row.original.item,
        demandId: row.original.demandId,
        offerId: row.original.offerId,
      },
    }));
  };

  const TableActions = [
    {
      label: t('Change'),
      onClick: () => setDialog(true),
    },
  ];

  return (
    <>
      <Card
        title={t('Allocations')}
        tooltipInfoMessage={
          isSelectable
            ? t(
                'Select those items for which you want to change the allocated supplier. Click then on Change do perform the adaptions.'
              )
            : t('Filter, sort and export the allocations of this scenario.')
        }
      >
        <Box pb="20px">
          <Filter
            {...filterProps}
            onChange={setFilterState}
            enableClearButton
            label={t('Filter by')}
          />

          <Flex align="flex-end" justify="flex-end">
            <Filter
              {...columnFilterProps}
              onChange={columnSetFilterState}
              enableClearButton
              label={t('Select columns')}
            />
            <StyledDownloadWrapper>
              <AllocationsDownload
                columns={allColumns}
                data={tableRows}
                name="Allocations"
              />
            </StyledDownloadWrapper>
          </Flex>
        </Box>

        {isSelectable ? (
          <MultiSelectTable
            columns={allColumns}
            data={filteredTableRows}
            defaultSorting={DEFAULT_SORTING}
            onChange={onChange}
            selected={selected}
            tableActions={TableActions}
          />
        ) : (
          <Table
            columns={allColumns}
            data={filteredTableRows}
            defaultSorting={DEFAULT_SORTING}
            maxHeight={500}
            stickyHeader
          />
        )}
      </Card>
      {selected && allColumns && (
        <Dialog
          open={dialog}
          onClose={() => setDialog(!dialog)}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          maxWidth="lg"
        >
          <StyledToRightIconContainer>
            <StyledCloseIcon onClick={() => setDialog(false)} />
          </StyledToRightIconContainer>
          <ManualAllocations selected={selected} columns={allColumns} />
        </Dialog>
      )}
      <ScenarioProgressContainer title="Optimizing scenario..." />
    </>
  );
};
