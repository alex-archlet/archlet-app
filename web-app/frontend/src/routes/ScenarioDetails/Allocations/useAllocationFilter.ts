import { useSelector } from 'react-redux';

import { useSmartFilter } from '@common/components/Filter/filterHooks';
import { projectsSelectors } from '@store/selectors/projects';
import { TYPE } from '@common/components/Filter/filterUtils';

export const useAllocationFilter = ({
  filterCombinations,
}) => {
  const filters = useSelector(projectsSelectors.getFilterDemandBidColumns);

  const projectFilters = filters.map(({ name, id }) => ({
    id,
    accessor: item => item[id],
    label: name,
    type: TYPE.MULTI,
  }));

  return useSmartFilter({
    filterCombinations,
    filterDeclaration: projectFilters,
    reduxFilterId: `allocations`,
  });
};
