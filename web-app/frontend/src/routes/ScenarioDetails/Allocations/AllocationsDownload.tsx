import React from 'react';
import { exportCsv } from '@utils/csvexport';
import { DownloadBox } from '../../../common/components/DownloadBox/DownloadBox';
import { useCustomTranslation } from '../../../utils/translate';

export const AllocationsDownload = ({ columns, data, name }) => {
  const { t } = useCustomTranslation();
  const onDownload = () => {
    exportCsv(data, columns, name);
  };

  return <DownloadBox onClick={onDownload}>{t('Export .csv')}</DownloadBox>;
};
