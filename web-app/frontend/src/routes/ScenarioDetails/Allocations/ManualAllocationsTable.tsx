import React from 'react';
import { SingleSelectTable } from '@common/components/Table/SingleSelectTable';

export const ManualAllocationsTable = ({
  data,
  select,
  onChange,
  tableColumns,
}) => (
  <SingleSelectTable
    columns={tableColumns}
    data={data}
    selected={select}
    onChange={onChange}
  />
);
