import React, { useEffect, useState, useMemo } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import queryString from 'query-string';
import {
  createManualAllocations as createManualAllocationsAction,
  runOptimizationForScenarios as runOptimizationForScenariosAction,
} from '@actions/projectScenarios';
import { useCustomTranslation } from '@utils/translate';

import styled from 'styled-components';
import { Box } from '@material-ui/core';
import { Button } from '@common/components/Button/Button';
import { useGetRequest } from '@common/components/Hooks/useRequest';
import { CustomColumn } from '@common/components/Table/Table';
import { Flex } from '@common/components/Flex/Flex';
import { ArrowOutlineLeft } from '@common/components/Arrow/ArrowOutlineLeft';
import { formatCell } from '@common/components/Table/utils';
import { ArrowOutlineRight } from '@common/components/Arrow/ArrowOutlineRight';
import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';
import { Selected } from '../Allocations/Allocations';
import { ManualAllocationsTable } from './ManualAllocationsTable';

const StyledContainer = styled(Box)`
  padding: 24px;
  height: 100%;
  width: 100%;
`;
const StyledPlaceholderContainer = styled(Box)`
  padding: 24px;
  height: 250px;
  width: 800px;
`;

const StyledButton = styled(Button)`
  && {
    text-transform: none;
  }
`;

const StyledTitle = styled.p`
  color: #44566c;
  font-family: Roboto;
  font-size: 18px;
  letter-spacing: 0;
  margin: 0 10px;
`;

type OfferDemandId = {
  offerId: string;
  demandId: string;
};

type RowState = {
  [x: string]: {
    item?: string;
    offerId: string | null;
    demandId: string | null;
  };
};

const translateSelectedStateToFlatStructure = (selectedRows) => {
  const array: OfferDemandId[] = [];

  selectedRows.forEach((allocation) => {
    const key = Object.keys(allocation);

    const { demandId, offerId } = allocation[key[0]];

    if (demandId && offerId) {
      array.push({
        demandId,
        offerId,
      });
    }
  });

  return array;
};

const setInitialState = (selected: Selected, response: object[][] | null) => {
  if (response === null) {
    return [];
  }
  const tableRows = Object.keys(selected).map(key => selected[key]);

  return tableRows.map((tableRow) => {
    const { offerId, item }: { offerId: string; item: string } = tableRow;

    return {
      [offerId]: {
        item,
        offerId,
        demandId: null,
      },
    };
  });
};
const flatAttributes = response =>
  response.map(({ totalPrice, attributes, demand, ...rest }) => ({
    ...attributes,
    ...rest,
    ...demand.attributes,
    totalPrice,
  }));

const mapAllDemandId = selected =>
  Object.keys(selected).map(key => selected[key].demandId);

const mapAllItemTitles = selected =>
  Object.keys(selected).map(key => selected[key].item);

const getParsedURLString = demandIds =>
  queryString.stringify({ demand: demandIds });

const removeNotUsedColumns = columns =>
  columns.filter(
    column =>
      !(
        column.id === 'item' ||
        column.id === 'volume' ||
        column.id === 'totalPrice'
      )
  );

interface Props {
  selected: Selected;
  columns: CustomColumn[];
}

export const ManualAllocations: React.FC<Props> = ({ selected, columns }) => {
  const [index, setIndex] = useState(0);
  const history = useHistory();
  const dispatch = useDispatch();
  const { t } = useCustomTranslation();

  const { scenarioId, id: projectId } = useParams<{
    scenarioId: string;
    id: string;
  }>();
  const demands = mapAllDemandId(selected);
  const itemTitles = mapAllItemTitles(selected);
  const parsedURLString = getParsedURLString(demands);
  const url = `scenarios/${scenarioId}/manual-allocation?${parsedURLString}`;
  const { response } = useGetRequest<object[][]>(url);

  const data = response ?? null;
  const [selectManualAllocations, setSelectManualAllocations] = useState<
    RowState[] | []
  >([]);

  const filteredColumns = removeNotUsedColumns(columns);

  const defaultColumns = [
    {
      Cell: formatCell,
      Header: t('Volume') as string,
      accessor: row => (row.totalVolume ? row.totalVolume : '-'),
      id: 'volume',
    },
    {
      Cell: formatCell,
      Header: t('Total Spend') as string,
      accessor: row =>
        row.total_var_price ? row.total_var_price * row.totalVolume : '-',
      id: 'totalPrice',
    },
  ];

  useMemo(() => filteredColumns.splice(1, 0, ...defaultColumns), [
    filteredColumns,
  ]);

  useEffect(() => {
    const state = setInitialState(selected, data);

    setSelectManualAllocations(state);
  }, [data]);

  const onChange = (row) => {
    const allocationRow = {
      [row.id]: {
        demandId: row.original.demandId,
        offerId: row.original.id,
      },
    };

    setSelectManualAllocations((prevValue) => {
      const newArray: RowState[] = [...prevValue];

      newArray[index] = allocationRow;

      return newArray;
    });
  };

  const onSubmit = async () => {
    try {
      const translatedAllocations = translateSelectedStateToFlatStructure(
        selectManualAllocations
      );
      await dispatch(
        createManualAllocationsAction(scenarioId, {
          allocations: translatedAllocations,
        })
      );
      await dispatch(
        runOptimizationForScenariosAction(projectId, [scenarioId])
      );
      history.push(
        `/projects/${projectId}/scenariosDetails/${scenarioId}/?tab=insights`
      );
    } catch (error) {
      console.error(error);
    }
  };

  const selectedValues = selectManualAllocations[index]
    ? selectManualAllocations[index]
    : [];

  const currentData = data && data.length ? data[index] : [];
  const hasPrevious = data && data?.length > 0 && index > 0;
  const flatData = flatAttributes(currentData);

  return (
    <>
      <StyledContainer>
        {data ? (
          <>
            <StyledTitle>
              {t('Select Offer for {{item}}', {
                item: itemTitles[index],
              })}
            </StyledTitle>
            <ManualAllocationsTable
              data={flatData}
              select={selectedValues}
              onChange={onChange}
              tableColumns={filteredColumns}
            />
            <VerticalSpacer height={10} />
            <Flex
              align="center"
              justify={hasPrevious ? 'space-between' : 'flex-end'}
            >
              {hasPrevious && (
                <ArrowOutlineLeft
                  onClick={() => setIndex(prevValue => prevValue - 1)}
                />
              )}
              {data?.length === index + 1 ? (
                <StyledButton
                  color="secondary"
                  variant="text"
                  onClick={() => onSubmit()}
                >
                  {t('Complete')}
                </StyledButton>
              ) : (
                <ArrowOutlineRight
                  onClick={() => setIndex(prevValue => prevValue + 1)}
                />
              )}
            </Flex>
          </>
        ) : (
          <StyledPlaceholderContainer />
        )}
      </StyledContainer>
    </>
  );
};
