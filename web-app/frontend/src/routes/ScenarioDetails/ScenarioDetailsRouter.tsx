import React from 'react';
import {
  match as Match,
  Route,
  Switch,
} from 'react-router-dom';
import { ScenarioDetailsContainer } from './containers/ScenarioDetails';

interface Params {
  id: string,
  scenarioId: string
}

interface Props {
  match: Match<Params>,
}

export const ScenarioDetailsRouter: React.FC<Props> = ({ match }) => (
  <Switch>
    <Route
      path={`${match.path}/`}
      exact
      component={ScenarioDetailsContainer}
    />
  </Switch>
);
