import React from 'react';
import { MarginWrapper } from '@common/components/MarginWrapper/MarginWrapper';
import { CreateScenarioForm } from '@routes/Scenarios/components/CreateScenarioForm/CreateScenarioForm';


export const RerunScenario = ({
  customError,
  customScenarioConstraints,
  setCustomScenarioConstraints,
  itemCategories,
  regions,
  scenarioFormValues,
  scenarioDetails,
  scenarioOverrides,
  suppliersPerRegions,
  suppliersPerItem,
  suppliersPerItemAlternative,
  criteria,
  onSetExcludedSuppliers,
  onSetMaxSpend,
  onSetNumOfSuppliers,
  onSetSplit,
  projectCurrency,
  projectUnit,
}) => {
  
  return (
    <MarginWrapper style={{ marginTop: 10 }}>
      <CreateScenarioForm
        scenarioFormValues={scenarioFormValues}
        onSubmitForm={console.info}
        customError={customError}
        scenarioDetails={scenarioDetails}
        isEdit
        itemCategories={itemCategories}
        scenarioOverrides={scenarioOverrides}
        suppliersPerRegions={suppliersPerRegions}
        suppliersPerItem={suppliersPerItem}
        suppliersPerItemAlternative={suppliersPerItemAlternative}
        onSetNumOfSuppliers={onSetNumOfSuppliers}
        onSetMaxSpend={onSetMaxSpend}
        onSetExcludedSuppliers={onSetExcludedSuppliers}
        customScenarioConstraints={customScenarioConstraints}
        setCustomScenarioConstraints={setCustomScenarioConstraints}
        onSetSplit={onSetSplit}
        regions={regions}
        criteria={criteria}
        initialValues={{
          capacity: scenarioDetails.json.capacity,
          incumbent: scenarioDetails.json.incumbent,
          itemType: scenarioDetails.json.itemType,
          name: scenarioDetails ? scenarioDetails.name : '',
          partialAllocation: scenarioDetails.json.partial_allocation,
          priceComponent: scenarioDetails.json.price_component,
          rebates: scenarioDetails.json.rebates,
          splitItems: scenarioDetails.json.split_items,
          volume: scenarioDetails.json.volume,
        }}
        projectCurrency={projectCurrency}
        projectUnit={projectUnit}
      />
    </MarginWrapper>
  );
};
