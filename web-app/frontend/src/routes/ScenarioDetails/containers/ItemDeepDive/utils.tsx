import React from 'react';
import { groupByKey, GroupedMapOfArray, sum } from '@utils/array';
import { formatNumber } from '@utils/formatNumber';
import { TFunction } from 'i18next';

import { LocaleFormatNumber } from '@common/components/FormatNumber/FormatNumber';
import {
  formatCell,
  getCellColor,
  calculateColumnSumWithFormatting,
  calculateColumnSum,
  StyledLabel,
} from '@common/components/Table/utils';
import {
  Allocation,
  CalculatedAllocationObject,
  ScenarioObject,
  ScenarioObjectMap,
} from '../types';
import { Scenario } from './ItemDeepDive';

type ScenarioEntries = any[];

const getAllocationsCountForScenario = (scenario: ScenarioObject) =>
  scenario.allocations.length;

const getScenariosObjectForIndex = (
  parentScenarios: ScenarioObjectMap,
  index: number
) => {
  const scenarios = {};

  Object.entries(parentScenarios).forEach(([scenarioId, scenario]) => {
    if (scenario.allocations.length > 1) {
      const allocationAtIndex = scenario.allocations[index];

      if (allocationAtIndex) {
        scenarios[scenarioId] = {
          allocations: [],
          supplier: allocationAtIndex.supplier,
          totalPrice: allocationAtIndex.totalPrice,
          volume: allocationAtIndex.volume,
        };
      }
    }
  });

  return scenarios;
};

const calculateSubRows = (aggregatedObject: CalculatedAllocationObject) => {
  const subRows: CalculatedAllocationObject[] = [];
  const allocationDepths: number[] = Object.values(
    aggregatedObject.scenarios
  ).map(getAllocationsCountForScenario);
  const maxAllocationDepth = Math.max(...allocationDepths);

  if (maxAllocationDepth < 2) {
    return subRows;
  }

  return Array(maxAllocationDepth)
    .fill(null)
    .map((_, index) => {
      const scenarios = getScenariosObjectForIndex(
        aggregatedObject.scenarios,
        index
      );

      return {
        _depth: 1,
        id: `${aggregatedObject.id}_${index}`,
        item: aggregatedObject.item,
        scenarios,
        subRows: [],
        allSuppliers: [],
        demandAttributes: {},
      };
    });
};

const calculateSingleDeepDive = (
  demandId: string,
  aGrouped: GroupedMapOfArray<Allocation>,
  bGrouped: GroupedMapOfArray<Allocation>
) => {
  const aData = aGrouped[demandId] || [];
  const bData = bGrouped[demandId] || [];
  const scenarioAllocationList = [...aData, ...bData].filter(Boolean);
  const sharedDataObject = scenarioAllocationList[0];

  const allSuppliers = new Set<string>();

  const result: CalculatedAllocationObject = {
    _depth: 0,
    id: demandId,
    item: sharedDataObject.item,
    scenarios: {},
    subRows: [],
    demandAttributes: sharedDataObject.demandAttributes,
    allSuppliers: [],
  };

  let hasAnyScenarioMultipleAllocations = false;
  const scenarios = {};

  scenarioAllocationList.forEach((scenarioAllocation) => {
    const { scenarioId } = scenarioAllocation;

    if (!scenarios[scenarioId]) {
      scenarios[scenarioId] = {
        allocations: [],
        supplier: null,
        totalPrice: 0,
        volume: 0,
      };
    } else {
      hasAnyScenarioMultipleAllocations = true;
    }
    scenarios[scenarioId].allocations.push(scenarioAllocation);
  });

  const scenarioEntries: ScenarioEntries = Object.entries(scenarios);

  const aggregatedScenarios = {};

  scenarioEntries.forEach(([scenarioId, scenarioItem]) => {
    const { allocations } = scenarioItem;
    const volumes = allocations.map(({ volume }) => volume);
    const totalPrices = allocations.map(({ totalPrice }) => totalPrice);

    const aggregatedScenario = {
      ...scenarioItem,
      totalPrice: sum(totalPrices),
      volume: sum(volumes),
    };

    if (allocations.length === 1) {
      aggregatedScenario.supplier = allocations[0].supplier;
    }

    allocations.forEach((allocation) => {
      if (allocation.supplier) {
        allSuppliers.add(allocation.supplier);
      }
    });

    aggregatedScenarios[scenarioId] = aggregatedScenario;
  });

  result.scenarios = aggregatedScenarios;
  result.allSuppliers = Array.from(allSuppliers);

  if (hasAnyScenarioMultipleAllocations) {
    result.subRows = calculateSubRows(result);
  }

  return result;
};

export const calculateItemDeepDive = (
  aScenarioAllocations: Allocation[],
  bScenarioAllocations: Allocation[]
) => {
  const aGroupedByDemand = groupByKey(aScenarioAllocations, 'demandId');
  const bGroupedByDemand = groupByKey(bScenarioAllocations, 'demandId');
  const allDemandIds = Array.from(
    new Set([
      ...Object.keys(aGroupedByDemand),
      ...Object.keys(bGroupedByDemand),
    ])
  );

  return allDemandIds.map(demandId =>
    calculateSingleDeepDive(demandId, aGroupedByDemand, bGroupedByDemand)
  );
};

interface Row {
  original: CalculatedAllocationObject;
}

const getUniqueSupplierCount = (rows: Row[], scenarioId: string) => {
  const uniqueSupplierNames = new Set<string>();
  rows.forEach((row) => {
    const scenarioData = row.original.scenarios[scenarioId];

    if (scenarioData) {
      const supplierNames = scenarioData.allocations.map(
        ({ supplier }) => supplier
      );

      supplierNames.forEach((supplierName) => {
        uniqueSupplierNames.add(supplierName);
      });
    }
  });
  return uniqueSupplierNames.size;
};

export const getScenarioColumns = (
  scenario: Scenario | undefined,
  uniqueId: string,
  t: TFunction
) => {
  const { id, name } = scenario ?? {
    id: '',
    name: t('No 2nd Scenario selected'),
  };

  return [
    {
      Header: name,
      columns: [
        {
          Footer: ({ rows }) => {
            if (!scenario) {
              return '-';
            }

            const supplierCount = getUniqueSupplierCount(rows, id);
            return t('{{count}} Supplier', { count: supplierCount });
          },
          Header: t('Supplier'),
          accessor: (row) => {
            const scenarioData = row.scenarios[id];

            if (!scenarioData) {
              return '-';
            }

            const supplierCount = scenarioData.allocations.length;

            if (supplierCount > 1) {
              return t('{{count}} Supplier', { count: supplierCount });
            }

            return scenarioData.supplier ?? '-';
          },

          id: `${uniqueId}_supplier`,
        },
        {
          Footer: footer =>
            scenario ? calculateColumnSumWithFormatting(footer) : '-',
          Cell: formatCell,
          Header: t('Total Spend'),
          accessor: row => row.scenarios[id]?.totalPrice || '-',
          id: `${uniqueId}_total`,
        },
        {
          Footer: footer =>
            scenario ? calculateColumnSumWithFormatting(footer) : '-',
          Cell: formatCell,
          Header: t('Volume'),
          accessor: row => row.scenarios[id]?.volume || '-',
          id: `${uniqueId}_volume`,
        },
      ],
      id,
    },
  ];
};

const getSavingsPercentage = (
  rows: Row[],
  scenarioAId: string,
  scenarioBId: string
) => {
  const { aTotal, bTotal } = rows.reduce(
    (acc, row) => {
      const aScenarioData =
        row.original.scenarios[scenarioAId]?.totalPrice ?? 0;
      const bScenarioData =
        row.original.scenarios[scenarioBId]?.totalPrice ?? 0;
      return {
        aTotal: acc.aTotal + aScenarioData,
        bTotal: acc.bTotal + bScenarioData,
      };
    },
    {
      aTotal: 0,
      bTotal: 0,
    }
  );

  const difference = aTotal ? (bTotal - aTotal) / aTotal : 0;
  return difference;
};

export const getDifferenceColumns = (
  aScenario: Scenario | undefined,
  bScenario: Scenario | undefined,
  t: TFunction,
  projectCurrency: string
) => {
  const scenarioAId = aScenario?.id ?? '';
  const scenarioBId = bScenario?.id ?? '';

  if (!scenarioAId || !scenarioBId) {
    return [];
  }
  return [
    {
      Header: t('Difference between Scenarios'),
      columns: [
        {
          Footer: (footer) => {
            const sumFooter = calculateColumnSum(footer);
            return (
              <StyledLabel color={getCellColor(sumFooter)}>
                <LocaleFormatNumber value={sumFooter} />
              </StyledLabel>
            );
          },
          Cell: ({ cell }) => {
            return (
              <StyledLabel color={getCellColor(cell.value)}>
                  <LocaleFormatNumber value={cell.value} />
              </StyledLabel>
            );
          },
          Header: `${t('Savings')} (${projectCurrency})`,
          accessor: (row) => {
            const aScenarioData = row.scenarios[scenarioAId]?.totalPrice ?? 0;
            const bScenarioData = row.scenarios[scenarioBId]?.totalPrice ?? 0;

            return bScenarioData - aScenarioData;
          },
          id: 'diff_total',
          sortType: (a, b) => b.values.diff_total - a.values.diff_total,
        },
        {
          Footer: ({ rows }) => {
            const rawSavingPercentage = getSavingsPercentage(
              rows,
              scenarioAId,
              scenarioBId
            );
            return (
              <StyledLabel color={getCellColor(rawSavingPercentage * 100)}>
                {formatNumber(rawSavingPercentage * 100, { type: 'percent' })}
              </StyledLabel>
            );
          },
          Cell: ({ cell }) => {
            return (
              <StyledLabel color={getCellColor(cell.value)}>
                {formatNumber(cell.value, { type: 'percent' })}
              </StyledLabel>
            );
          },
          Header: `${t('% Savings')}`,
          accessor: (row) => {
            const aScenarioData = row.scenarios[scenarioAId]?.totalPrice ?? 0;
            const bScenarioData = row.scenarios[scenarioBId]?.totalPrice ?? 0;

            const difference = aScenarioData
              ? (bScenarioData - aScenarioData) / aScenarioData
              : 0;

            return difference * 100;
          },
          id: 'diff_percentage',
          sortType: (a, b) =>
            a.values.diff_percentage - b.values.diff_percentage,
        },
        {
          Footer: (footer) => {
            const sumFooter = calculateColumnSum(footer);
            return (
              <StyledLabel color={getCellColor(sumFooter)}>
                <LocaleFormatNumber value={sumFooter} />
              </StyledLabel>
            );
          },
          Cell: formatCell,
          Header: t('Volume'),
          accessor: (row) => {
            const aScenarioData = row.scenarios[scenarioAId]?.volume ?? 0;
            const bScenarioData = row.scenarios[scenarioBId]?.volume ?? 0;

            return aScenarioData - bScenarioData;
          },
          id: 'diff_volume',
        },
      ],
      id: 'difference',
    },
  ];
};

export const getExportScenarioColumns = (
  scenario: Scenario | undefined,
  t: TFunction
) => {
  if (!scenario) {
    return [];
  }

  const { id, name } = scenario;

  return [
    {
      Header: `${t('Supplier')} - ${name}`,
      accessor: (row) => {
        const scenarioData = row.scenarios[id];

        if (!scenarioData) {
          return '-';
        }

        const supplierCount = scenarioData.allocations.length;

        if (supplierCount > 1) {
          return t('{{count}} Supplier', { count: supplierCount });
        }

        return scenarioData.supplier;
      },
    },
    {
      Header: `${t('Volume')} - ${name}`,
      accessor: (row) => {
        const scenarioData = row.scenarios[id];

        if (!scenarioData) {
          return '-';
        }

        return scenarioData.volume;
      },
    },
    {
      Header: `${t('Total Spend')} - ${name}`,
      accessor: (row) => {
        const scenarioData = row.scenarios[id];

        if (!scenarioData) {
          return '-';
        }

        return scenarioData.totalPrice;
      },
    },
  ];
};

export const getExportDifferenceColumns = (
  aScenario: Scenario | undefined,
  bScenario: Scenario | undefined,
  t: TFunction
) => {
  if (!aScenario || !bScenario) {
    return [];
  }

  return [
    {
      Header: `${t('Absolute Price')} - ${t('Difference')}`,
      accessor: (row) => {
        const aScenarioData = row.scenarios[aScenario.id];
        const bScenarioData = row.scenarios[bScenario.id];

        if (!aScenarioData || !bScenarioData) {
          return '-';
        }

        return aScenarioData.totalPrice - bScenarioData.totalPrice;
      },
    },
    {
      Header: `${t('% Price')} - ${t('Difference')}`,
      accessor: (row) => {
        const aScenarioData = row.scenarios[aScenario.id];
        const bScenarioData = row.scenarios[bScenario.id];

        if (!aScenarioData || !bScenarioData) {
          return '-';
        }

        const difference =
          (aScenarioData.totalPrice - bScenarioData.totalPrice) /
          aScenarioData.totalPrice;

        return formatNumber(difference * 100, { type: 'percent' });
      },
    },
    {
      Header: `${t('Volume')} - ${t('Difference')}`,
      accessor: (row) => {
        const aScenarioData = row.scenarios[aScenario.id];
        const bScenarioData = row.scenarios[bScenario.id];

        if (!aScenarioData || !bScenarioData) {
          return '-';
        }

        return aScenarioData.volume - bScenarioData.volume;
      },
    },
  ];
};
