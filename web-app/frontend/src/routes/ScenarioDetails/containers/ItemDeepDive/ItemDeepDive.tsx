import React, { useEffect, useState, useMemo } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import { PageTitle } from '@common/components/PageTitle/PageTitle';
import { colors } from '@utils/uiTheme';
import { Card } from '@common/components/Card/Card';
import { useCustomTranslation } from '@utils/translate';
import { projectScenariosSelectors } from '@store/selectors/projectScenarios';
import { Filter } from '@common/components/Filter/Filter';
import { getScenarioAllocations } from '@store/actions/projectScenarios';
import { Table } from '@common/components/Table/Table';
import { arraysIntersect, getEmptyArrayIfConditionFalse } from '@utils/array';
import { DownloadBox } from '@common/components/DownloadBox/DownloadBox';
import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';

import { exportCsv, flattenNestedData } from '@utils/csvexport';
import { projectsSelectors } from '@store/selectors/projects';
import { SingleFilterValue } from '@common/components/Filter/types';
import {
  TYPE,
  stringToDropdownOption,
  getValueFromOption,
} from '@common/components/Filter/filterUtils';
import {
  calculateItemDeepDive,
  getDifferenceColumns,
  getExportDifferenceColumns,
  getExportScenarioColumns,
  getScenarioColumns,
} from './utils';
import { useDeepDiveFilter } from '../useDeepDiveFilter';
import {
  DemandAttributesMap,
  Allocation,
  CalculatedAllocationObject,
} from '../types';

const StyledContainer = styled.div`
  width: 100%;
  overflow-x: auto;
  white-space: nowrap;
`;

export interface Scenario {
  id: string;
  name: string;
}

const getSuppliersFromAllocations = (allAllocations: Allocation[]) => {
  const supplierNames = allAllocations.map(({ supplier }) => supplier);
  return Array.from(new Set(supplierNames))
    .filter(Boolean)
    .map(stringToDropdownOption);
};

const DEFAULT_SORTING = [
  {
    desc: false,
    id: 'item',
  },
];

const EMPTY_ARRAY = [];

export const ItemDeepDive = () => {
  const scenarios = useSelector(projectScenariosSelectors.getScenarios);
  const scenariosMap = useSelector(projectScenariosSelectors.getScenariosMap);

  const { t } = useCustomTranslation();
  const { scenarioId } = useParams();
  const dispatch = useDispatch();
  const [secondScenarioId, setSecondScenarioId] = useState<string | null>(null);

  const mainScenarioAllocations = useSelector(
    projectScenariosSelectors.makeGetScenarioAllocations(scenarioId)
  );

  const secondScenarioAllocations = useSelector(
    projectScenariosSelectors.makeGetScenarioAllocations(secondScenarioId)
  );

  const allAllocations = useMemo(
    () => [
      ...(mainScenarioAllocations ?? EMPTY_ARRAY),
      ...(secondScenarioAllocations ?? EMPTY_ARRAY),
    ],
    [mainScenarioAllocations, secondScenarioAllocations]
  );

  const [demandAttributesState, setDemandAttributesState] = useState<
    DemandAttributesMap[]
  >([]);

  const additionalFilters = [
    {
      clearable: true,
      customFilter: (row: CalculatedAllocationObject, state) => {
        const { supplier } = state;
        const stringValues = supplier.map(getValueFromOption);
        return arraysIntersect(stringValues, row.allSuppliers);
      },
      id: 'supplier',
      label: t('Supplier'),
      options: getSuppliersFromAllocations(allAllocations),
      text: t('Filter by'),
      type: TYPE.MULTI,
    },
  ];

  const {
    filterState,
    filterProps,
    setFilterState,
    filterFunction,
  } = useDeepDiveFilter({
    scenarioId,
    scenarios,
    filterCombinations: demandAttributesState,
    additionalFilters,
  });

  const secondScenarioFromFilterId = filterState.scenario
    ? (filterState.scenario as SingleFilterValue).value
    : null;

  useEffect(() => {
    setSecondScenarioId(secondScenarioFromFilterId as string | null);
  }, [secondScenarioFromFilterId, setSecondScenarioId]);

  useEffect(() => {
    dispatch(getScenarioAllocations(scenarioId));
  }, [scenarioId]);

  useEffect(() => {
    if (secondScenarioId && !secondScenarioAllocations) {
      dispatch(getScenarioAllocations(secondScenarioId));
    }
  }, [secondScenarioId, secondScenarioAllocations, dispatch]);

  useEffect(() => {
    setDemandAttributesState(
      allAllocations.map(({ demandAttributes }) => demandAttributes)
    );
  }, [allAllocations]);

  const projectCurrency =
    useSelector(projectsSelectors.getProjectCurrencyCode) ?? {};

  const data = calculateItemDeepDive(
    mainScenarioAllocations ?? EMPTY_ARRAY,
    secondScenarioAllocations ?? EMPTY_ARRAY
  );

  const hasSecondAllocations =
    secondScenarioAllocations && secondScenarioAllocations.length > 0;
  const hasFirstAllocations =
    mainScenarioAllocations && mainScenarioAllocations.length > 0;

  const mainScenario = scenariosMap && scenariosMap[scenarioId];
  const secondScenario = secondScenarioId && scenariosMap[secondScenarioId];

  const filteredData = filterFunction(data);

  const columns = [
    {
      Header: t('Item'),
      Footer: t('{{count}} Item', { count: filteredData.length }),
      accessor: 'item',
      id: 'item',
      tooltip: true,
      fixed: true,
    },
    ...getScenarioColumns(mainScenario, 'scenario_1', t),
    ...getScenarioColumns(secondScenario, 'scenario_2', t),
    ...getDifferenceColumns(mainScenario, secondScenario, t, projectCurrency),
  ];

  const onExport = () => {
    const exportColumns = [
      {
        Header: t('Item'),
        accessor: value => value.item,
      },
      ...getEmptyArrayIfConditionFalse(
        getExportScenarioColumns(mainScenario, t),
        hasFirstAllocations
      ),
      ...getEmptyArrayIfConditionFalse(
        getExportScenarioColumns(secondScenario, t),
        hasSecondAllocations
      ),
      ...getEmptyArrayIfConditionFalse(
        getExportDifferenceColumns(mainScenario, secondScenario, t),
        mainScenario && secondScenario
      ),
    ];

    const flattenedData = flattenNestedData(filteredData);

    exportCsv(flattenedData, exportColumns, 'Item Comparison');
  };

  return (
    <>
      <PageTitle
        title={t('Item Comparison')}
        titleColor={colors.textColor}
        rightSide={
          <Filter
            {...filterProps}
            onChange={setFilterState}
            enableClearButton
            alwaysShowFilters
          />
        }
      />
      <Card
        title={t('Items List')}
        rightSide={
          <DownloadBox onClick={onExport}>{t('Export .csv')}</DownloadBox>
        }
        tooltipInfoMessage={t(
          'Compare the selected offers from two scenarios with each other.'
        )}
      >
        {data && data.length > 0 ? (
          <>
            <VerticalSpacer height={32} />
            <StyledContainer>
              <Table
                data={filteredData}
                columns={columns}
                defaultSorting={DEFAULT_SORTING}
                maxColumnWidth={200}
                maxHeight="70vh"
                stickyHeader
                expandable
              />
            </StyledContainer>
          </>
        ) : null}
      </Card>
    </>
  );
};
