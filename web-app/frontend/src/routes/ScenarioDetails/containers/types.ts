export interface Allocation {
  id: string;
  demandId: string;
  offerId: string;
  scenarioId: string;
  item: string;
  supplier: string;
  totalPrice: number;
  volume: number;
  demandAttributes: object;
  offerAttributes: object;
}

export interface ScenarioObject {
  allocations: Allocation[];
  totalPrice: number;
  volume: number;
  supplier: string | null;
}

export interface ScenarioObjectMap {
  [key: string]: ScenarioObject;
}

export interface DemandAttributesMap {
  [id: string]: any;
}

export interface CalculatedAllocationObject {
  _depth: number;
  id: string;
  item: string;
  scenarios: ScenarioObjectMap;
  subRows: CalculatedAllocationObject[];
  demandAttributes: DemandAttributesMap;
  allSuppliers: string[];
}
