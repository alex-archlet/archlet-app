import React from 'react';
import { formatNumber } from '@utils/formatNumber';

import { LocaleFormatNumber } from '@common/components/FormatNumber/FormatNumber';
import { TFunction } from 'i18next';
import {
  formatCell,
  getCellColor,
  calculateColumnSumWithFormatting,
  calculateColumnSum,
  StyledLabel,
} from '@common/components/Table/utils';
import { Scenario } from './SupplierDeepDive';

export const getScenarioColumns = (
  scenario: Scenario | undefined,
  t: TFunction,
  selectorId: string,
) => {
  const { id, name } = scenario ?? {
    id: '',
    name: t('No 2nd Scenario selected'),
  };

  return [
    {
      Header: name,
      columns: [
        {
          Footer: footer =>
            scenario ? calculateColumnSumWithFormatting(footer) : '-',
          Cell: formatCell,
          Header: t('Total Spend'),
          accessor: `${selectorId}TotalPrice`,
          id: `${id}_total`,
        },
        {
          Footer: footer =>
            scenario ? calculateColumnSumWithFormatting(footer) : '-',
          Cell: formatCell,
          Header: t('Volume'),
          accessor: `${selectorId}Volume`,
          id: `${id}_volume`,
        },
        {
          Footer: scenario ? formatNumber(100, { type: 'percent' }) : '-',
          Cell: ({ cell: { value } }) => {
            return formatNumber(value * 100, { type: 'percent' });
          },
          Header: t('Percentage'),
          accessor: `${selectorId}Percentage`,
          id: `${id}_percentage`,
        },
      ],
      id,
    },
  ];
};

export const getDifferenceColumns = (
  areTwoScenariosSelected: boolean,
  t: TFunction
) => {
  if (!areTwoScenariosSelected) {
    return [];
  }
  return [
    {
      Header: t('Difference between Scenarios'),
      columns: [
        {
          Footer: (footer) => {
            const sumFooter = calculateColumnSum(footer);
            return (
              <StyledLabel color={getCellColor(sumFooter)}>
                <LocaleFormatNumber value={sumFooter} />
              </StyledLabel>
            );
          },
          Cell: ({ cell }) => {
            return (
              <StyledLabel color={getCellColor(cell.value)}>
                <LocaleFormatNumber value={cell.value} />
              </StyledLabel>
            );
          },
          Header: t('Volume Change'),
          accessor: (row) => {
            return row.volumeChange ?? 0;
          },
          id: 'diff_volume',
        },
        {
          Footer: (footer) => {
            const sumFooter = calculateColumnSum(footer);
            return (
              <StyledLabel color={getCellColor(sumFooter)}>
                <LocaleFormatNumber value={sumFooter} />
              </StyledLabel>
            );
          },
          Cell: ({ cell }) => {
            return (
              <StyledLabel color={getCellColor(cell.value)}>
                <LocaleFormatNumber value={cell.value} />
              </StyledLabel>
            );
          },
          Header: t('Spend Change'),
          accessor: (row) => {
            return row.spendChange ?? 0;
          },
          id: 'diff_total',
        },
      ],
      id: 'difference',
    },
  ];
};
export const getExportScenarioColumns = (
  scenario: Scenario | undefined,
  t: TFunction,
  selectorId: string,
) => {
  if (!scenario) {
    return [];
  }
  const { name } = scenario;

  return [
    {
      Header: `${t('Volume')} - ${name}`,
      accessor: (row) => {
        return row[`${selectorId}Volume`] ?? '-';
      },
    },
    {
      Header: `${t('Total Spend')} - ${name}`,
      accessor: (row) => {
        return row[`${selectorId}TotalPrice`] ?? '-';
      },
    },
  ];
};


export const getExportDifferenceColumns = (
  areTwoScenariosSelected: boolean,
  t: TFunction
) => {
  if (!areTwoScenariosSelected) {
    return [];
  }

  return [
    {
      Header: `${t('Volume')} - ${t('Difference')}`,
      accessor: (row) => {
        return row.volumeChange ?? '-';
      },
    },
    {
      Header: `${t('Total Spend')} - ${t('Difference')}`,
      accessor: (row) => {
        if (!row.firstTotalPrice || !row.secondTotalPrice) {
          return '-';
        }

        const difference =
          (row.firstTotalPrice - row.secondTotalPrice) /
          row.firstTotalPrice;

        return formatNumber(difference * 100, { type: 'percent' });
      },
    },
  ];
};
