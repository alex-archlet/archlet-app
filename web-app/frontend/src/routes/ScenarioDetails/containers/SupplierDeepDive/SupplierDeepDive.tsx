import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import { useCustomTranslation } from '@utils/translate';
import { projectScenariosSelectors } from '@store/selectors/projectScenarios';
import { getScenarioAllocations } from '@store/actions/projectScenarios';
import { PageTitle } from '@common/components/PageTitle/PageTitle';
import { colors } from '@utils/uiTheme';
import { Filter } from '@common/components/Filter/Filter';
import { Card } from '@common/components/Card/Card';
import { SingleFilterValue } from '@common/components/Filter/types';
import { Row } from '@common/components/Row/Row';
import { DownloadBox } from '@common/components/DownloadBox/DownloadBox';
import { Table } from '@common/components/Table/Table';
import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';
import { getEmptyArrayIfConditionFalse } from '@utils/array';
import { exportCsv } from '@utils/csvexport';

import {
  getDifferenceColumns,
  getScenarioColumns,
  getExportScenarioColumns,
  getExportDifferenceColumns,
} from './utils';
import { useDeepDiveFilter } from '../useDeepDiveFilter';
import { SupplierSpendGraph } from './SupplierCharts';
import { DemandAttributesMap } from '../types';
import {
  calculateAndMergeScenarios,
  SupplierDeepDiveType,
} from './calculations';

const StyledContainer = styled.div`
  width: 100%;
  overflow-x: auto;
  white-space: nowrap;
`;

export interface Scenario {
  id: string;
  name: string;
}

export interface Supplier {
  id: string;
  scenarios: object;
  supplier: string;
}

const DEFAULT_SORTING = [
  {
    desc: false,
    id: 'supplier',
  },
];

export const SupplierDeepDive = () => {
  const { t } = useCustomTranslation();
  const { scenarioId } = useParams();
  const dispatch = useDispatch();
  const scenarios = useSelector(projectScenariosSelectors.getScenarios);
  const scenariosMap = useSelector(projectScenariosSelectors.getScenariosMap);

  const [demandAttributesState, setDemandAttributesState] = useState<
    DemandAttributesMap[]
  >([]);

  const {
    filterProps,
    filterState,
    setFilterState,
    filterFunction,
  } = useDeepDiveFilter({
    scenarioId,
    scenarios,
    filterCombinations: demandAttributesState,
  });

  const secondScenarioId = filterState.scenario
    ? (filterState.scenario as SingleFilterValue).value
    : null;

  const mainScenarioAllocations = useSelector(
    projectScenariosSelectors.makeGetScenarioAllocations(scenarioId)
  );
  const secondScenarioAllocations = useSelector(
    projectScenariosSelectors.makeGetScenarioAllocations(secondScenarioId)
  );

  useEffect(() => {
    dispatch(getScenarioAllocations(scenarioId));
  }, [scenarioId]);

  useEffect(() => {
    if (secondScenarioId && !secondScenarioAllocations) {
      dispatch(getScenarioAllocations(secondScenarioId));
    }
  }, [secondScenarioId, secondScenarioAllocations, dispatch]);

  useEffect(() => {
    const allAllocations = [
      ...(mainScenarioAllocations ?? []),
      ...(secondScenarioAllocations ?? []),
    ];
    setDemandAttributesState(
      allAllocations.map(({ demandAttributes }) => demandAttributes)
    );
  }, [mainScenarioAllocations, secondScenarioAllocations]);

  const filteredMainScenarioAllocations = filterFunction(
    mainScenarioAllocations ?? []
  );
  const filteredSecondScenarioAllocations = filterFunction(
    secondScenarioAllocations ?? []
  );

  const rawMergedData = calculateAndMergeScenarios(
    filteredMainScenarioAllocations,
    filteredSecondScenarioAllocations
  );
  const mergedData: SupplierDeepDiveType[] = rawMergedData.toArray();

  const hasSecondAllocations =
    secondScenarioAllocations && secondScenarioAllocations.length > 0;
  const hasFirstAllocations =
    mainScenarioAllocations && mainScenarioAllocations.length > 0;

  const mainScenario = scenariosMap[scenarioId];
  const secondScenario = secondScenarioId && scenariosMap[secondScenarioId];

  const onExport = () => {
    const exportColumns = [
      {
        Header: t('Supplier'),
        accessor: 'supplier',
      },
      ...getEmptyArrayIfConditionFalse(
        getExportScenarioColumns(mainScenario, t, 'first'),
        hasFirstAllocations
      ),
      ...getEmptyArrayIfConditionFalse(
        getExportScenarioColumns(secondScenario, t, 'second'),
        hasSecondAllocations
      ),
      ...getEmptyArrayIfConditionFalse(
        getExportDifferenceColumns(Boolean(mainScenario && secondScenario), t),
        mainScenario && secondScenario
      ),
    ];

    exportCsv(mergedData, exportColumns, 'Supplier Comparison');
  };

  const columns = [
    {
      Footer: t('{{count}} Supplier', { count: mergedData.length }),
      Header: t('Supplier'),
      accessor: 'supplier',
      id: 'supplier',
    },
    ...getScenarioColumns(mainScenario, t, 'first'),
    ...getScenarioColumns(secondScenario, t, 'second'),
    ...getDifferenceColumns(Boolean(mainScenario && secondScenario), t),
  ];

  const hasSupplierChartData =
    mergedData.length > 0 && Object.keys(scenariosMap).length;

  return (
    <>
      <PageTitle
        title={t('Supplier Comparison')}
        titleColor={colors.textColor}
        rightSide={
          <Filter
            {...filterProps}
            onChange={setFilterState}
            enableClearButton
            alwaysShowFilters
          />
        }
      />
      <Row>
        <Card
          style={{
            flex: 'unset',
            width: '60%',
          }}
          title={t('Suppliers List')}
          rightSide={
            <DownloadBox onClick={onExport}>{t('Export .csv')}</DownloadBox>
          }
          tooltipInfoMessage={t(
            'Compare the awarded suppliers from two scenarios with each other.'
          )}
        >
          {mergedData.length > 0 && (
            <>
              <VerticalSpacer height={32} />
              <StyledContainer>
                {mainScenario && (
                  <Table
                    data={mergedData}
                    columns={columns}
                    defaultSorting={DEFAULT_SORTING}
                    maxColumnWidth={200}
                    maxHeight="70vh"
                    stickyHeader
                    expandable
                  />
                )}
              </StyledContainer>
            </>
          )}
        </Card>
        <Card
          style={{
            flex: 'unset',
            width: '40%',
          }}
          title={t('Supplier Spend')}
        >
          {hasSupplierChartData && (
            <SupplierSpendGraph
              data={mergedData}
              mainScenario={mainScenario}
              secondScenario={secondScenario}
            />
          )}
        </Card>
      </Row>
    </>
  );
};
