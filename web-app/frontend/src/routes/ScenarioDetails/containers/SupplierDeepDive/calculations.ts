import { DataFrame, IDataFrame } from 'data-forge';
import { Allocation } from '../types';

const calculateSingleScenario = (allocations: Allocation[]) => {
  const df = new DataFrame({
    values: allocations,
    columnNames: ['supplier', 'totalPrice', 'volume'],
  });
  const grouped = df.groupBy(row => row.supplier);

  const total = df.getSeries('totalPrice').sum();
  const summarized = grouped
    .select(group => ({
      id: group.first().supplier,
      supplier: group.first().supplier,
      totalPrice: group.deflate(row => row.totalPrice).sum(),
      volume: group.deflate(row => row.volume).sum(),
      percentage: total
        ? group.deflate(row => row.totalPrice).sum() / total
        : 0,
    }))
    .inflate()
    .where(row => Boolean(row.supplier))
    .setIndex('id');
  return summarized;
};

export interface SupplierDeepDiveType {
  id: string;
  supplier: string;
  firstTotalPrice: number;
  firstVolume: number;
  firstPercentage: number;
  secondTotalPrice: number;
  secondVolume: number;
  secondPercentage: number;
  spendChange: number;
  volumeChange: number;
}

export const calculateAndMergeScenarios = (
  aAllocations: Allocation[],
  bAllocations: Allocation[]
): IDataFrame<SupplierDeepDiveType> => {
  const aGrouped = calculateSingleScenario(aAllocations);
  const aRenamed = aGrouped.renameSeries({
    totalPrice: 'firstTotalPrice',
    volume: 'firstVolume',
    percentage: 'firstPercentage',
  });
  const bGrouped = calculateSingleScenario(bAllocations);
  const bRenamed = bGrouped.renameSeries({
    totalPrice: 'secondTotalPrice',
    volume: 'secondVolume',
    percentage: 'secondPercentage',
  });
  const scenariosMerged = aRenamed.merge(bRenamed);
  return scenariosMerged
    .select((rawRow) => {
      return {
        ...rawRow,
        spendChange: (rawRow.secondTotalPrice ?? 0) - (rawRow.firstTotalPrice ?? 0),
        volumeChange: (rawRow.secondVolume ?? 0) - (rawRow.firstVolume ?? 0),
      };
    })
    .setIndex('id');
};
