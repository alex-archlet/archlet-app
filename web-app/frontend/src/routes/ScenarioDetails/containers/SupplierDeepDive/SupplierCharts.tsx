import React from 'react';
import {
  Bar,
  BarChart,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
  Legend,
} from 'recharts';
import styled from 'styled-components';
import { useSelector } from 'react-redux';

import { colors } from '@utils/uiTheme';
import { formatNumber } from '@utils/formatNumber';
import { LocaleFormatNumber } from '@common/components/FormatNumber/FormatNumber';
import { projectsSelectors } from '@selectors/projects';
import { useCustomTranslation } from '@utils/translate';
import { getEmptyArrayIfConditionFalse } from '@utils/array';

import { CustomLegend } from '@common/components/Graph/Legend/CustomLegend';
import { CustomTooltip } from '@common/components/Graph/Legend/CustomTooltip';

import { SupplierDeepDiveType } from './calculations';
import { Scenario } from './SupplierDeepDive';

const BAR_HEIGHT_PX = 20;
const MAX_LABEL_LENGTH = 25;

interface Props {
  data: SupplierDeepDiveType[];
  mainScenario: Scenario;
  secondScenario?: Scenario;
}

// Show overflow in order to correctly display axis label
const StyledResponsiveContainer = styled(ResponsiveContainer)`
  .recharts-surface {
    overflow: visible;
  }
`;

const mapPayloadToCustomTooltip = (payload, scenarioMap) => {
  return payload.map(({ dataKey: scenarioId, color, value }) => ({
    shape: 'largeSquare',
    label: scenarioMap[scenarioId],
    color,
    value: <LocaleFormatNumber value={value} />,
  }));
};

export const SupplierSpendGraph: React.FC<Props> = ({
  data,
  mainScenario,
  secondScenario,
}) => {
  const { t } = useCustomTranslation();

  const currencyCode = useSelector(projectsSelectors.getProjectCurrencyCode);

  const truncateString = (str: string) =>
    str.length > MAX_LABEL_LENGTH
      ? `${str.substr(0, MAX_LABEL_LENGTH - 2)}...`
      : str;

  const sortedData = data.sort((aRow, bRow) => aRow.id.localeCompare(bRow.id));
  const chartData = sortedData.map(({ id, firstTotalPrice, secondTotalPrice }) => {
    const scenarioData = {
      [mainScenario.id]: firstTotalPrice,
    };

    if (secondScenario) {
      scenarioData[secondScenario.id] = secondTotalPrice;
    }

    return {
      name: id,
      ...scenarioData,
    };
  });

  const chartHeight = chartData.length * BAR_HEIGHT_PX * 3;

  const scenarioMap = {
    [mainScenario.id]: mainScenario.name,
  };

  if (secondScenario) {
    scenarioMap[secondScenario.id] = secondScenario.name;
  }

  return (
    <>
      <StyledResponsiveContainer width="100%" height={chartHeight}>
        <BarChart
          data={chartData}
          layout="vertical"
          barGap={0}
          barCategoryGap="20%"
          barSize={BAR_HEIGHT_PX}
          margin={{
            left: 16,
            top: 16,
          }}
        >
          <Legend
            verticalAlign="top"
            align="center"
            height={36}
            payloadUniqBy
            content={() => {
              const items = [
                {
                  value: mainScenario.name,
                  color: colors.graphs.supplierDeepDive[0],
                  type: 'circle',
                },
                ...getEmptyArrayIfConditionFalse(
                [
                  {
                    value: secondScenario?.name ?? '',
                    color: colors.graphs.supplierDeepDive[1],
                    type: 'circle',
                  },
                ], Boolean(secondScenario)),
              ];
              return (
                <CustomLegend payload={items} alignment="center" />
              );
            }}
          />
          <XAxis
            tickFormatter={val => formatNumber(val)}
            type="number"
            // @ts-ignore this property exists as per their api, but is not included in the type definitions
            label={{
              dy: 20,
              value: t('Total Spend Label', { currencyCode }),
            }}
          />
          <YAxis
            dataKey="name"
            type="category"
            allowDataOverflow
            width={100}
            tickFormatter={truncateString}
          />
          <Tooltip
            content={(props: any) => {
              const payload = props?.payload ?? null;
              if (payload && payload.length === 0 && payload[0] === null) {
                return null;
              }
              const title = payload[0]?.payload?.name ?? '';
              const parsedPayload = mapPayloadToCustomTooltip(props?.payload, scenarioMap);
              return <CustomTooltip items={parsedPayload} title={title} />;
            }}
          />
          <Tooltip formatter={value => <LocaleFormatNumber value={value} />} />
          <Bar
            dataKey={mainScenario.id}
            fill={colors.graphs.supplierDeepDive[0]}
          />
          {secondScenario && (
            <Bar
              dataKey={secondScenario.id}
              fill={colors.graphs.supplierDeepDive[1]}
            />
          )}
        </BarChart>
      </StyledResponsiveContainer>
    </>
  );
};
