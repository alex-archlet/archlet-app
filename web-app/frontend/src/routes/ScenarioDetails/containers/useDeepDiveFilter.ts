import { useMemo } from 'react';
import { useSelector } from 'react-redux';

import { useSmartFilter } from '@common/components/Filter/filterHooks';
import { useCustomTranslation } from '@utils/translate';
import { projectsSelectors } from '@store/selectors/projects';
import { TYPE } from '@common/components/Filter/filterUtils';
import { FilterDeclaration } from '@common/components/Filter/types';
import { DemandAttributesMap } from './types';

interface UseDeepDiveFilterParams {
  scenarioId: string;
  scenarios: any[];
  filterCombinations: DemandAttributesMap[];
  additionalFilters?: FilterDeclaration[];
}

export const useDeepDiveFilter = ({
  scenarioId,
  scenarios,
  filterCombinations,
  additionalFilters = [],
}: UseDeepDiveFilterParams) => {
  const { t } = useCustomTranslation();
  const filters = useSelector(projectsSelectors.getFilterDemandBidColumns);

  const otherScenarios = useMemo(
    () => scenarios.filter(({ id }) => id !== scenarioId),
    [scenarios, scenarioId]
  );
  const scenarioOptions = useMemo(
    () =>
      otherScenarios.map(({ id, name }) => ({
        label: name,
        value: id,
      })),
    [otherScenarios]
  );

  const projectFilters = filters.map(({ name, id }) => ({
    id,
    accessor: item => item.demandAttributes[id],
    label: name,
    type: TYPE.MULTI,
  }));

  const filterDeclaration = [
    {
      clearable: true,
      id: 'scenario',
      label: t('Scenario'),
      options: scenarioOptions,
      text: t('Select second scenario'),
      type: TYPE.SINGLE,
    },
    ...additionalFilters,
    ...projectFilters,
  ];

  return useSmartFilter({
    filterCombinations,
    filterDeclaration,
    reduxFilterId: `${scenarioId}_scenario_deep_dive`,
  });
};
