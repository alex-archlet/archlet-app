import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { useHistory, useParams } from 'react-router-dom';
import get from 'lodash.get';
import { setBackButton, setPageTitle } from '@actions/interface';
import { useCustomTranslation } from '@utils/translate';
import { userSelectors } from '@selectors/user';
import {
  deleteScenario,
  getScenarioAllocations,
  getScenarioDetails,
  getScenarios,
  getScenarioKpi,
  runOptimization,
  updateScenario,
} from '@actions/projectScenarios';
import { getProjectInfo } from '@actions/projects';
import { isValid } from 'redux-form';
import { projectsSelectors } from '@selectors/projects';
import { projectScenariosSelectors } from '@selectors/projectScenarios';
import { ScenarioProgressContainer } from '@common/components/ScenarioProgress/ScenarioProgressContainer';
import { ConfirmationDialog } from '@common/components/ConfirmationDialog/ConfirmationDialog';
import { CancelButton } from '@common/components/Button/CancelButton';
import { DeleteButton } from '@common/components/Button/DeleteButton';

import { SCENARIO_INSIGHTS_REGION_FORM_NAME } from '@utils/formNames';
import { getCreateUpdateScenarioPayload } from '@utils/scenario/scenarioSettingsHelpers';
import { Tabs } from '@common/components/Tabs/Tabs';
import { useTabParams } from '@common/components/Tabs/useTabParams';
import { LastEdit } from '@common/components/LastEdit/LastEdit';

import { BaseLayout } from '@common/containers/Layout/BaseLayout';
import { InsightsTabContainer as InsightsTab } from '../components/InsightsTab';
import { SettingsTab } from '../components/SettingsTab';
import { ItemDeepDive } from './ItemDeepDive/ItemDeepDive';
import { SupplierDeepDive } from './SupplierDeepDive/SupplierDeepDive';

interface ProjectInfo {
  projectId: string;
}

interface ScenarioDetails {
  name: string;
  isDefault: boolean;
  updatedAt: string | null;
}

interface ScenarioFormValues {
  name: string;
}

interface Props {
  deleteScenarioAction: Function;
  getScenarioAllocationsAction: Function;
  getScenarioDetailsAction: Function;
  getScenarioKpiAction: Function;
  getScenariosAction: Function;
  isFormValid: boolean;
  permissions: string[];
  projectCurrency: string;
  projectInfo: ProjectInfo;
  projectUnit: string;
  runOptimizationAction: Function;
  getProjectInfoAction: Function;
  scenarioDetails: ScenarioDetails;
  scenarioFormValues: ScenarioFormValues;
  setBackButtonAction: Function;
  setPageTitleAction: Function;
  updateScenarioAction: Function;
}

const ScenarioDetails: React.FC<Props> = ({
  deleteScenarioAction,
  getScenarioAllocationsAction,
  getScenarioKpiAction,
  getScenarioDetailsAction,
  getScenariosAction,
  isFormValid,
  permissions,
  projectCurrency,
  projectInfo,
  projectUnit,
  runOptimizationAction,
  getProjectInfoAction,
  scenarioDetails,
  scenarioFormValues,
  setBackButtonAction,
  setPageTitleAction,
  updateScenarioAction,
}) => {  
  const { id, scenarioId } = useParams<{
    id: string;
    scenarioId: string;
  }>();
  const history = useHistory();
  const { t } = useCustomTranslation();

  useEffect(() => {
    setBackButtonAction('close', `/projects/${id}/scenarios`);
    getScenariosAction(id);
    getScenarioDetailsAction(scenarioId);
    getScenarioAllocationsAction(scenarioId);
    getScenarioKpiAction(id, scenarioId);
  
    return () => setBackButtonAction('');
  }, []);

  useEffect(() => {
    if (!Object.keys(projectInfo).length) {
      getProjectInfoAction(id);
    }
  }, [projectInfo]);

  useEffect(() => {
    setPageTitleAction(scenarioDetails.name);
  }, [scenarioDetails, setPageTitleAction]);

  const [progressTitle, setProgressTitle] = useState('');

  const [numOfSuppliers, setNumOfSuppliers] = useState([]);

  const [maxSpend, setMaxSpend] = useState([]);

  const [excludedSuppliers, setExcludedSuppliers] = useState([]);

  const [customScenarioConstraints, setCustomScenarioConstraints] = useState(
    {}
  );

  const [errorView, setErrorView] = useState(false);

  const [isDialogOpen, setIsDialogOpen] = useState(false);

  const runScenarioOptimization = async (currentScenarioId) => {
    setProgressTitle('Optimizing scenario...');

    try {
      const optimization = await runOptimizationAction(id, currentScenarioId);

      return optimization;
    } catch (error) {
      setErrorView(error.errorMessage);

      return error;
    }
  };

  const scenarioUpdate = async () => {
    setProgressTitle(t('Updating scenario...'));

    const payload = getCreateUpdateScenarioPayload(
      scenarioFormValues,
      customScenarioConstraints,
      excludedSuppliers,
      maxSpend,
      numOfSuppliers
    );

    await updateScenarioAction(
      scenarioId,
      payload,
      scenarioFormValues.name,
      id
    );

    const optimization = await runScenarioOptimization(scenarioId);

    if (optimization.statusId === 5) {
      history.push(`/projects/${id}/scenarios`);
    }
  };

  const scenarioDelete = () => {
    if (!scenarioDetails.isDefault) {
      deleteScenarioAction(scenarioId)
        .then(() => {
          setIsDialogOpen(false);
          history.push(`/projects/${id}/scenarios`);
        })
        .catch(() => {
          setIsDialogOpen(false);
          history.push(`/projects/${id}/scenarios`);
        });
    }
  };

  const settingsRegions = [
    'Global',
    'Every Region',
    ...get(projectInfo, ['output', 'params', 'project_info', 'regions'], []),
  ];

  const suppliersPerRegions = [
    ...get(
      projectInfo,
      ['output', 'params', 'project_info', 'suppliers_per_regions'],
      []
    ),
  ];

  const suppliersPerItem = [
    ...get(
      projectInfo,
      ['output', 'params', 'project_info', 'suppliers_per_item'],
      []
    ),
  ];

  const suppliersPerItemAlternative = [
    ...get(
      projectInfo,
      ['output', 'params', 'project_info', 'suppliers_per_item_alternative'],
      []
    ),
  ];

  const criteria = [
    'Exclude All',
    ...get(projectInfo, ['output', 'params', 'project_info', 'criteria'], []),
  ];

  const itemCategories = [
    ...new Set(
      get(
        projectInfo,
        ['output', 'params', 'project_info', 'itemCategories'],
        []
      ).map(category => category.itemCategory)
    ),
  ];

  const tabs = [
    {
      disabled: !permissions.includes('CAN_READ_INSIGHTS'),
      id: 'insights',
      label: t('Insights'),
      render: () => <InsightsTab />,
    },
    {
      disabled: !permissions.includes('CAN_READ_ITEM_DEEP_DIVE'),
      id: 'item_deep_dive',
      label: t('Item Comparison'),
      render: () => <ItemDeepDive />,
    },
    {
      disabled: !permissions.includes('CAN_READ_SUPPLIER_DEEP_DIVE'),
      id: 'supplier_deep_dive',
      label: t('Supplier Comparison'),
      render: () => <SupplierDeepDive />,
    },
    {
      disabled:
        !permissions.includes('CAN_READ_SCENARIO_SETTINGS') ||
        scenarioDetails.isDefault,
      id: 'settings',
      label: t('Manual Allocation'),
      render: () => (
        // @ts-ignore
        <SettingsTab
          // @ts-ignore
          customError={errorView}
          isFormValid={
            !!(Object.keys(scenarioFormValues).length && isFormValid)
          }
          scenarioFormValues={scenarioFormValues}
          onScenarioDelete={() => setIsDialogOpen(true)}
          onScenarioUpdate={scenarioUpdate}
          suppliersPerRegions={suppliersPerRegions}
          suppliersPerItem={suppliersPerItem}
          suppliersPerItemAlternative={suppliersPerItemAlternative}
          customScenarioConstraints={customScenarioConstraints}
          setCustomScenarioConstraints={setCustomScenarioConstraints}
          regions={settingsRegions}
          criteria={criteria}
          permissions={permissions}
          // @ts-ignore
          itemCategories={itemCategories}
          onSetNumOfSuppliers={setNumOfSuppliers}
          onSetMaxSpend={setMaxSpend}
          onSetExcludedSuppliers={setExcludedSuppliers}
          projectCurrency={projectCurrency}
          projectUnit={projectUnit}
        />
      ),
    },
  ];

  const { tab, onChange } = useTabParams({
    options: ['insights', 'item_deep_dive', 'supplier_deep_dive', 'settings'],
    initialValue: 'insights',
  });

  return (
    <BaseLayout>
      <ConfirmationDialog
        isOpen={isDialogOpen}
        handleClose={() => setIsDialogOpen(false)}
        ButtonActions={
          <>
            <DeleteButton onClick={scenarioDelete} autoFocus>
              {t('Delete')}
            </DeleteButton>
            <CancelButton onClick={() => setIsDialogOpen(false)}>
              {t('No')}
            </CancelButton>
          </>
        }
        message={t('Do you really want to delete this scenario?')}
      />
      <ScenarioProgressContainer title={progressTitle} />
      <Tabs
        tabs={tabs}
        initialValue={tab}
        onChange={onChange}
        rightSide={
          scenarioDetails.updatedAt && (
            <LastEdit date={scenarioDetails.updatedAt} />
          )
        }
      />
    </BaseLayout>
  );
};

const mapStateToProps = state => ({
  isFormValid: isValid('CreateScenarioForm')(state),
  permissions: userSelectors.getUserPermissions(state),
  projectCurrency: projectsSelectors.getProjectCurrencyCode(state),
  projectDetails: projectsSelectors.getProjectDetails(state),
  projectInfo: projectsSelectors.getProjectInfo(state),
  projectUnit: projectsSelectors.getProjectUnitName(state),
  scenarioDetails: projectScenariosSelectors.getScenarioDetails(state),
  scenarioFormValues: projectScenariosSelectors.getScenarioFormValues(state),
  selectedRegionInsights: projectScenariosSelectors.getSelectedRegion(
    state,
    SCENARIO_INSIGHTS_REGION_FORM_NAME
  ),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      deleteScenarioAction: deleteScenario,
      getScenarioAllocationsAction: getScenarioAllocations,
      getScenarioKpiAction: getScenarioKpi,
      getScenarioDetailsAction: getScenarioDetails,
      getScenariosAction: getScenarios,
      runOptimizationAction: runOptimization,
      getProjectInfoAction: getProjectInfo,
      setBackButtonAction: setBackButton,
      setPageTitleAction: setPageTitle,
      updateScenarioAction: updateScenario,
    },
    dispatch
  );

export const ScenarioDetailsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ScenarioDetails);
