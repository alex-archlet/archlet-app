import React from 'react';
import { connect } from 'react-redux';
import { projectScenariosSelectors } from '@selectors/projectScenarios';
import { CustomColumn, Table } from '@common/components/Table/Table';
import { useCustomTranslation } from '@utils/translate';
import styled from 'styled-components';
import { LocaleFormatNumber } from '@common/components/FormatNumber/FormatNumber';

const StyledLabel = styled.p`
  color: ${props => props.color};
`;

interface Discount {
  [key: string]: any;
}

interface Discounts {
  discounts: Discount[];
}

const DiscountsTable: React.FC<Discounts> = ({ discounts }) => {
  const { t } = useCustomTranslation();

  const columns: CustomColumn[] = [
    {
      Header: t('Bundle'),
      accessor: row => row.bundle,
      id: 'bundle',
    },
    {
      Header: t('Supplier'),
      accessor: row => row.supplier,
      id: 'supplier',
    },
    {
      Cell: ({ cell: { value } }) => <LocaleFormatNumber value={value} />,
      Header: t('Target'),
      accessor: row => row.target_value,
      id: 'target',
    },
    {
      Cell: ({ cell: { row, value } }) => (
        <StyledLabel
          color={row.original.target_value < value ? 'green' : 'black'}
        >
          <LocaleFormatNumber value={value} />
        </StyledLabel>
      ),
      Header: t('Achieved'),
      accessor: row => row.reached_value,
      id: 'achieved',
    },
    {
      Header: t('Trigger'),
      accessor: row => t(row.unit),
      id: 'trigger',
    },
    {
      Header: t('Unit Discount'),
      accessor: row => row.discount_amount,
      id: 'unit discount',
    },
    {
      Header: t('Type'),
      accessor: row => t(row.type),
      id: 'type',
    },
    {
      Cell: ({ cell: { value } }) => <LocaleFormatNumber value={value} />,
      Header: t('Total Discount'),
      accessor: row => row.total_discount,
      id: 'totalDiscount',
    },
  ];

  return (
    <Table
      data={discounts}
      columns={columns}
      getUniqueKey={row => `${row.bundle}_${row.supplier}`}
    />
  );
};

const mapStateToProps = state => ({
  discounts: projectScenariosSelectors.getScenarioDetailOutputDiscounts(state),
});

export const DiscountsTableContainer = connect(mapStateToProps)(DiscountsTable);
