import React, { useEffect } from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';
import { Box } from '@material-ui/core';
import { SecondaryButton } from '@common/components/Button/SecondaryButton';
import { DeleteButton } from '@common/components/Button/DeleteButton';
import { getScenarioAllocations } from '@store/actions/projectScenarios';
import { projectScenariosSelectors } from '@selectors/projectScenarios';
import { useCustomTranslation } from '@utils/translate';
import { Card } from '@common/components/Card/Card';
import { RerunScenario } from '../containers/Scenario/RerunScenario';
import { Allocations } from '../Allocations/Allocations';

const StyledContainer = styled.div`
  margin: 10px 20px 10px;
`;

const StyledUpdateButton = styled(SecondaryButton)`
  && {
    margin-right: 10px;
  }
`;

export const SettingsTab = ({
  isFormValid,
  customError,
  customScenarioConstraints,
  setCustomScenarioConstraints,
  itemCategories,
  onScenarioDelete,
  onScenarioUpdate,
  permissions,
  regions,
  scenarioFormValues,
  scenarioOverrides,
  suppliersPerRegions,
  suppliersPerItem,
  suppliersPerItemAlternative,
  criteria,
  onSetExcludedSuppliers,
  onSetMaxSpend,
  onSetNumOfSuppliers,
  onSetSplit,
  projectCurrency,
  projectUnit,
}) => {
  const { t } = useCustomTranslation();
  const { scenarioId } = useParams();
  const scenarioDetails = useSelector(
    projectScenariosSelectors.getScenarioDetails
  );
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getScenarioAllocations(scenarioId));
  }, [dispatch]);

  return (
    <>
      <Box display="flex" justifyContent="flex-end" mr={30} mt={10}>
        <StyledUpdateButton
          disabled={!isFormValid}
          color="secondary"
          onClick={onScenarioUpdate}
        >
          {t('Update')}
        </StyledUpdateButton>
        {permissions.includes('CAN_DELETE_SCENARIO') && (
          <DeleteButton onClick={onScenarioDelete} variant="outlined">
            {t('Delete scenario')}
          </DeleteButton>
        )}
      </Box>
      <StyledContainer>
        <Allocations isSelectable scenarioId={scenarioId} />
      </StyledContainer>
      <StyledContainer>
        <Card
          title={t('Rules Settings')}
          tooltipInfoMessage={t(
            'Adapt the business rules that are applied in this scenario.'
          )}
        >
          {Object.keys(scenarioDetails).length !== 0 && (
            <RerunScenario
              scenarioFormValues={scenarioFormValues}
              customError={customError}
              scenarioDetails={scenarioDetails}
              itemCategories={itemCategories}
              scenarioOverrides={scenarioOverrides}
              suppliersPerRegions={suppliersPerRegions}
              suppliersPerItem={suppliersPerItem}
              suppliersPerItemAlternative={suppliersPerItemAlternative}
              onSetNumOfSuppliers={onSetNumOfSuppliers}
              onSetMaxSpend={onSetMaxSpend}
              onSetExcludedSuppliers={onSetExcludedSuppliers}
              customScenarioConstraints={customScenarioConstraints}
              setCustomScenarioConstraints={setCustomScenarioConstraints}
              onSetSplit={onSetSplit}
              regions={regions}
              criteria={criteria}
              projectCurrency={projectCurrency}
              projectUnit={projectUnit}
            />
          )}
        </Card>
      </StyledContainer>
    </>
  );
};
