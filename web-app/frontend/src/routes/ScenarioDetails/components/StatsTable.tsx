import React from 'react';
import styled from 'styled-components';
import { useCustomTranslation } from '@utils/translate';
import { useSelector } from 'react-redux';
import { Card as CommonCard } from '@common/components/Card/Card';

import { Card, Grid } from '@material-ui/core';
import { Text } from '@common/components/Text/Text';
import { LocaleFormatNumber } from '@common/components/FormatNumber/FormatNumber';
import { colors } from '@utils/uiTheme';
import { projectScenariosSelectors } from '@store/selectors/projectScenarios';
import { projectsSelectors } from '@store/selectors/projects';
import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';
import { translateKpiName } from '../../Scenarios/components/ScenarioList/ScenarioListTable';

const StyledGridItem = styled(Grid)`
  display: flex;
  flex-direction: column;
  align-items: center;
  height: 100px;
  width: 150px;
  border: none;
  padding: 20px 0;
`;

const StyledValue = styled(Text)`
  margin-top: 5px;
  letter-spacing: -1.5px;
`;

const StyledDescription = styled(Text)`
  display: flex;
  align-items: center;
  min-height: 48px;
  margin-top: 1px;
  color: ${colors.black};
`;

const StyledCard = styled(Card)`
  margin-bottom: 30px;
  border: none !important;
`;

const StyledParentGrid = styled(({ ...props }) => <Grid {...props} />)`
  & .MuiPaper-card {
    border: none !important;
  }
`;

interface GridItemProps {
  value: string | React.ReactNode;
  description: string;
}

const GridItem: React.FC<GridItemProps> = ({ value, description }) => (
  <StyledGridItem item xs>
    <StyledValue fontSize={25} fontWeight={500}>
      {value}
    </StyledValue>
    <StyledDescription fontSize={12} textAlign="center">
      {description}
    </StyledDescription>
  </StyledGridItem>
);

interface Props {
  scenarioId: string;
}

export const StatsTable: React.FC<Props> = ({ scenarioId }) => {
  const { t } = useCustomTranslation();

  const scenarioKpi = useSelector(projectScenariosSelectors.getScenarioKpiList);
  const scenarioDefKpiList = useSelector(projectsSelectors.getProjectKpiDefs);
  const currentScenarioKpi = scenarioKpi[scenarioId];

  return (
    <CommonCard style={{ flex: 2 }} title={t('Scenario KPIs')}>
      <VerticalSpacer height={20} />
      <StyledCard elevation={0}>
        <StyledParentGrid container width="100%">
          {scenarioDefKpiList.map((kpi) => {
            return (
              <GridItem
                key={kpi.id}
                value={
                  currentScenarioKpi ? (
                    <LocaleFormatNumber value={currentScenarioKpi[kpi.id]} />
                  ) : (
                    '-'
                  )
                }
                description={translateKpiName(kpi.name, t)}
              />
            );
          })}
        </StyledParentGrid>
      </StyledCard>
    </CommonCard>
  );
};
