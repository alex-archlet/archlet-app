import React, { useEffect } from 'react';
import { Card } from '@common/components/Card/Card';
import { useCustomTranslation } from '@utils/translate';
import { Box } from '@material-ui/core';
import { connect, useDispatch } from 'react-redux';
import { useParams } from 'react-router';
import { getScenarioAllocations } from '@store/actions/projectScenarios';

import { projectScenariosSelectors } from '@selectors/projectScenarios';
import { getScenarioKpi } from '@actions/projectScenarios';
import { StatsTable } from './StatsTable';
import { CapacitiesTableContainer } from './Capacities/CapacitiesTable';
import { DiscountsTableContainer } from './Discounts/DiscountsTable';
import { Allocations } from '../Allocations/Allocations';

interface Props {
  hasCapacities: boolean;
  hasDiscounts: boolean;
}

const InsightsTab: React.FC<Props> = ({ hasCapacities, hasDiscounts }) => {
  const { t } = useCustomTranslation();

  const { id, scenarioId } = useParams<{
    id: string;
    scenarioId: string;
  }>();

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getScenarioAllocations(scenarioId));
    dispatch(getScenarioKpi(id, scenarioId));
  }, [dispatch, id, scenarioId]);

  return (
    <>
      <Box
        display="flex"
        style={{
          marginTop: 10,
        }}
      >
        <Box display="flex" flexDirection="row" style={{ width: '100%' }}>
          <StatsTable scenarioId={scenarioId} />
        </Box>
      </Box>
      <Allocations scenarioId={scenarioId} />
      <Box display="flex">
        {hasDiscounts && (
          <Card title={t('Discounts')}>
            <DiscountsTableContainer />
          </Card>
        )}
        {hasCapacities && (
          <Card
            title={t('Capacities')}
            tooltipInfoMessage={t(
              'Overview of all capacity restrictions and their impact on this scenario'
            )}
          >
            <CapacitiesTableContainer />
          </Card>
        )}
      </Box>
    </>
  );
};

const mapStateToProps = state => ({
  hasCapacities: projectScenariosSelectors.getScenarioDetailOutputHasCapacities(
    state
  ),
  hasDiscounts: projectScenariosSelectors.getScenarioDetailOutputHasDiscounts(
    state
  ),
});

export const InsightsTabContainer = connect(mapStateToProps)(InsightsTab);
