import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Box } from '@material-ui/core';
import { GetApp } from '@material-ui/icons';
import { Text } from '@common/components/Text/Text';
import { colors } from '@utils/uiTheme';

const StyledGetApp = styled(GetApp)`
  && {
    fill: ${colors.iconGrey};
    margin-right: 15px;
  }
`;

const StyledDownloadBox = styled(Box)`
  border-bottom: 1px solid ${colors.grey};
  padding: 12px 5px;
  cursor: pointer;
`;

export const DownloadBox = ({ text, onDownloadClick }) => (
  <StyledDownloadBox
    display="flex"
    onClick={() => onDownloadClick()}
  >
    <StyledGetApp />
    <Text
      fontSize={17}
      fontWeight={500}
    >
      {text}
    </Text>
  </StyledDownloadBox>
);

DownloadBox.propTypes = {
  onDownloadClick: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
};
