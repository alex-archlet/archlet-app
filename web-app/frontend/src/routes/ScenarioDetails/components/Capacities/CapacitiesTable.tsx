import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { projectScenariosSelectors } from '@selectors/projectScenarios';
import styled from 'styled-components';
import { useCustomTranslation } from '@utils/translate';
import { Table } from '@common/components/Table/Table';

export const StyledLabel = styled.p`
  color: ${props => props.color};
`;

export const getColor = (value, greenThreshold, orangeThreshold) => {
  if (value > greenThreshold) {
    return 'green';
  }

  if (value > orangeThreshold || Number.isNaN(value)) {
    return 'orange';
  }

  return 'red';
};

interface LooseObject {
  [key: string]: any;
}

const processCapacities = ({ table, list }) => {
  if (!table || table.length < 0) {
    return [];
  }
  const result: LooseObject[] = [];
  
  Object.keys(table).forEach((parentKey) => {
    const obj: LooseObject = {};

    list.forEach((capacityName) => {
      if (table[parentKey] && table[parentKey][capacityName]) {
        const currentDataRow = table[parentKey][capacityName];

        obj.supplierName = parentKey;
        obj[capacityName] = {
          limit: currentDataRow.limit,
          ratio: 1 - currentDataRow.reached / currentDataRow.limit,
          reached: currentDataRow.reached,
        };
      }
    });
    
    result.push(obj);
  });

  return result;
};

const capacitiesHeaders = ({ list }) => {
  if (!list || list.length < 0) {
    return [];
  }

  return list.map(headerName => ({
    // eslint-disable-next-line react/prop-types
    Cell: ({ cell }) => {
      const row = cell.row.original[headerName];

      return (
        <StyledLabel color={getColor(row.ratio, 0.2, 0)}>
          {`${row.reached} / ${row.limit}`}
        </StyledLabel>
      );
    },
    Header: headerName,
    accessor: value => value[headerName].ratio,
  }));
};

interface CapacitiesProps {
  capacities: {
    table: LooseObject
    list: LooseObject
  }
}

const CapacitiesTable: React.FC<CapacitiesProps> = ({ capacities }) => {  
  const processed = processCapacities(capacities);

  const { t } = useCustomTranslation();

  const columns = [
    {
      Header: t('Supplier'),
      // eslint-disable-next-line no-underscore-dangle
      accessor: value => value.supplierName,
    },
    ...capacitiesHeaders(capacities),
  ];

  return (
    <Table
      data={processed}
      columns={columns}
      getUniqueKey={({ supplierName }) => supplierName}
    />
  );
};

const mapStateToProps = state => ({
  capacities: projectScenariosSelectors.getScenarioDetailOutputCapacities(
    state
  ),
});

export const CapacitiesTableContainer = connect(mapStateToProps)(
  CapacitiesTable
);

CapacitiesTable.propTypes = {
  capacities: PropTypes.shape({
    list: PropTypes.arrayOf(PropTypes.string).isRequired,
    table: PropTypes.shape({}).isRequired,
  }).isRequired,
};
