
import {
  TextField,
} from '@material-ui/core';
import styled from 'styled-components';

export const StyledTextField = styled(TextField)`
  box-sizing: border-box;
  opacity: 0.74;
  border-radius: 5px;
  background-color: rgba(109,114,120,0.05);
  height: 30px;
  margin-top: 10px !important;

  && .MuiOutlinedInput-root  {
    height: 30px;
    width: 100px;
  }
`;
