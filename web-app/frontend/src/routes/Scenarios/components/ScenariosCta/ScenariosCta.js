import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { Box } from '@material-ui/core';
import { Button } from '@common/components/Button/Button';
import { Text } from '@common/components/Text/Text';
import { colors } from '@utils/uiTheme';
import { CloudUpload, InsertChartOutlined } from '@material-ui/icons';
import { useCustomTranslation } from '@utils/translate';
import { Tooltip } from '@common/components/Tooltip/Tooltip';

const StyledButton = styled(Button)`
  && {
    margin-left: 15px;
    padding: 11px 14px;
  }
`;

const StyledLink = styled(Link)`
  text-decoration: none;
`;

const StyledIcon = styled.div`
  display: flex;
  margin-right: 10px;
`;

export const ScenariosCta = ({
  isOptimizationButtonDisabled,
  isOptimizationButtonVisible,
  onOptimizationClick,
}) => {
  const { t } = useCustomTranslation();

  return (
    <Box display="flex" justifyContent="flex-end" alignItems="center" mb={25}>
      {isOptimizationButtonVisible ? (
        <Tooltip
          title={t('Press this button if you want to run all your scenarios')}
        >
          <StyledButton
            color="secondary"
            variant="text"
            onClick={onOptimizationClick}
            disabled={isOptimizationButtonDisabled}
          >
            <StyledIcon>
              <InsertChartOutlined />
            </StyledIcon>
            {t('Run Optimization')}
          </StyledButton>
        </Tooltip>
      ) : (
        <>
          <Text color={colors.error} fontWeight={400}>
            {t(
              'You have not uploaded all the data yet or your data contains errors.'
            )}
          </Text>
          <StyledLink to="data">
            <StyledButton color="secondary" variant="text">
              <StyledIcon>
                <CloudUpload />
              </StyledIcon>
              {t('Upload data')}
            </StyledButton>
          </StyledLink>
        </>
      )}
    </Box>
  );
};

ScenariosCta.propTypes = {
  isOptimizationButtonDisabled: PropTypes.bool.isRequired,
  isOptimizationButtonVisible: PropTypes.bool.isRequired,
  onOptimizationClick: PropTypes.func.isRequired,
};
