import React from 'react';
import { PropTypes } from 'prop-types';
import styled from 'styled-components';
import { Box, Card, Grid } from '@material-ui/core';
import { Close } from '@material-ui/icons';
import EditIcon from '@material-ui/icons/Edit';
import { Text } from '@common/components/Text/Text';
import { colors } from '@utils/uiTheme';
import { useCustomTranslation } from '@utils/translate';
import { connect, useSelector } from 'react-redux';
import { projectsSelectors } from '@selectors/projects';
import { translateObject } from '@utils/scenario/scenarioSettingsHelpers';
import {
  getTitleFromValues,
  tooltipArrayCSVFormatted,
} from '@utils/stringFormat';
import { Tooltip } from '@common/components/Tooltip/Tooltip';
import clsx from 'clsx';

const StyledSpan = styled.span`
  font-size: 12px;
`;

const StyledGridContainer = styled(Grid)`
  && {
    align-items: center;
    border-bottom: 1px solid ${colors.grey};
    padding: 20px;
    transition: background 150ms ease-out;
    &:hover {
      background: ${colors.darkWhite};
    }
  }

  &&.editable {
    background-color: ${colors.lightGrey};
    font-style: italic;
    &:hover {
      background-color: ${colors.lightGrey}
    }
  }
`;

const sharedIconStyles = `
  && {
    width: 25px;
    fill: ${colors.iconGrey};
    cursor: pointer;
    margin: 0 0 0 5px;

    ${StyledGridContainer}.editable & {
      visibility: hidden
    }
  }
`
const StyledCloseIcon = styled(Close)`
  ${sharedIconStyles}
`;

const StyledEditIcon = styled(EditIcon)`
  ${sharedIconStyles}
`;

const StyledCard = styled(({ alignTop: _alignTop, ...props }) => (
  <Card {...props} />
))`
  && {
    overflow: auto;
    display: flex;
    flex-direction: column;
    justify-content: ${props => (props.alignTop ? 'flex-start' : 'center')};
    height: 262px;
    box-shadow: none;
    font-size: 12px;
  }
`;

const StyledText = styled(Text)`
  font-size: 20px;
  color: ${colors.iconGrey};
  text-align: center;
`;

const getItems = (
  item,
  viewStructure,
  itemLevel,
  projectCurrency,
  projectUnit,
  projectSuppliers
) => {
  const array = [];

  if (!item) {
    return array;
  }

  if (Array.isArray(projectSuppliers)) {
    projectSuppliers.forEach((viewName) => {
      array.push(
        <Grid item xs={1} key={itemLevel + viewName}>
          <Tooltip title={tooltipArrayCSVFormatted(item[viewName])}>
            <StyledSpan>{getTitleFromValues(item[viewName])}</StyledSpan>
          </Tooltip>
        </Grid>
      );
    });
  }

  viewStructure.forEach((viewName) => {
    if (!item[viewName]) {
      array.push(<Grid item xs={1} key={itemLevel + viewName} />);
    } else if (Array.isArray(item[viewName])) {
      array.push(
        <Grid item xs={1} key={itemLevel + viewName}>
          <Tooltip title={tooltipArrayCSVFormatted(item[viewName])}>
            <StyledSpan>{getTitleFromValues(item[viewName])}</StyledSpan>
          </Tooltip>
        </Grid>
      );
    } else {
      array.push(
        <Grid item xs={1} key={itemLevel + viewName}>
          <StyledSpan>
            {translateObject(
              viewName,
              item[viewName].value ? item[viewName].value : item[viewName],
              projectCurrency,
              projectUnit
            )}
          </StyledSpan>
        </Grid>
      );
    }
  });

  return array;
};

export const GridBox = ({
  elements,
  hasSupplier,
  onRemoveElement,
  onHandleEdit,
  staticViewStructure,
  projectCurrency,
  projectUnit,
  indexEditableObj,
  isDisplayInViewMode
}) => {
  const { t } = useCustomTranslation();
  const filters = useSelector(projectsSelectors.getProjectFilters);
  const hasElements = elements && elements.length > 0;
  
  return (
    <Box mb={20} mt={20} elevation={0}>
      <StyledCard 
        alignTop={hasElements}>
        {hasElements ? (
          elements.map((item, index) => (
            <StyledGridContainer
              container
              className={clsx({
                'editable': indexEditableObj === index,
              })}
              // eslint-disable-next-line react/no-array-index-key
              key={index}
            >
              {hasSupplier &&
                getItems(
                  item.filters,
                  [],
                  index,
                  projectCurrency,
                  projectUnit,
                  ['supplier']
                )}

              {getItems(
                item.filters,
                filters.map(filter => filter.name),
                index,
                projectCurrency,
                projectUnit
              )}

              {staticViewStructure &&
                getItems(
                  item,
                  staticViewStructure,
                  index,
                  projectCurrency,
                  projectUnit
              )}
              {!isDisplayInViewMode && (
                <Grid item xs> 
                  <Box display="flex" justifyContent="flex-end">
                    <StyledEditIcon onClick={() => onHandleEdit(item, index)} />
                    <StyledCloseIcon onClick={() => onRemoveElement(index)} />
                  </Box>
                </Grid>
                )
              }  
            </StyledGridContainer>
          ))
        ) : (
          <Box width="100%">
            <StyledText>
              {t('Add your constraints with the dropdown above.')}
            </StyledText>
          </Box>
        )}
      </StyledCard>
    </Box>
  );
};

GridBox.propTypes = {
  isDisplayInViewMode: PropTypes.bool,
  elements: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  hasSupplier: PropTypes.bool,
  onRemoveElement: PropTypes.func,
  projectCurrency: PropTypes.string.isRequired,
  projectUnit: PropTypes.string.isRequired,
  staticViewStructure: PropTypes.arrayOf(PropTypes.string.isRequired)
    .isRequired,
};

GridBox.defaultProps = {
  hasSupplier: false,
  onRemoveElement: null,
  isDisplayInViewMode: false
};

const mapStateToProps = state => ({
  projectCurrency: projectsSelectors.getProjectCurrencyCode(state),
  projectUnit: projectsSelectors.getProjectUnitName(state),
});

export const GridBoxElement = connect(mapStateToProps)(GridBox);
