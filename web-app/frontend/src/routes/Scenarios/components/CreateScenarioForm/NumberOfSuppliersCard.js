import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Filter } from '@common/components/Filter/Filter';
import {
  FilterDropdown,
  TYPE,
} from '@common/components/Filter/FilterDropdown/FilterDropdown';
import { useProjectFilter } from '@common/components/Filter/filterHooks';
import { Box } from '@material-ui/core';

import { useCustomTranslation } from '@utils/translate';
import { mutation, types } from '@utils/scenario/scenarioSettingsHelpers';

import clsx from 'clsx';
import { colors } from '@utils/uiTheme';
import { GridBoxElement } from './GridBoxElement';
import { StyledTextField as TextField } from '../ScenarioCreation/TextField';
import { ButtonGroupConstraint } from './ButtonGroupConstraint';

const StyledContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  flex-wrap: wrap;

  && > * {
    margin-left: 12px;
  }
  && > *:first-child {
    margin-left: 0;
  }
`;

const StyledItemContainer = styled.div`
  display: flex;
  align-items: center;
`;

const StyledBox = styled.div`
  text-align: right;
  margin: 10px 0 0 0;
`;

const StyledTextField = styled(TextField)`
  &&.editable {
    .MuiInputBase-root {
      background-color: ${colors.lightGrey};
      border: 1px solid ${colors.lightGrey};
      box-shadow: none;
    }
  }
`;

const INITIAL_FILTER_STATE = {
  mutation: {
    label: 'For all',
    value: 'for_all',
  },
  operation: {
    label: 'At most',
    value: 'max',
  },
};

const NumberOfSuppliersComponent = ({
  numOfSuppliers,
  addElement,
  removeElement,
  handleEdit,
}) => {
  const { filterProps, filterState, setFilterState } = useProjectFilter({
    additionalFilters: [],
    initialState: {},
  });
  const [numOfSupplier, setNumOfSupplier] = useState(INITIAL_FILTER_STATE);

  const { t } = useCustomTranslation();

  // todo refactor logic, see task https://archlet.atlassian.net/browse/AA-1162
  const INIT_INDEX = -1;
  const [editableObj, setEditableObject] = useState(null); // todo types <{} | null>
  const [indexEditableObj, setIndexOfEditableObj] = useState(INIT_INDEX);

  const addValue = (key, value, deleteValue, inputType) => {
    const func = editableObj ? setEditableObject : setNumOfSupplier;
    func((prevValue) => {
      const newValue = { ...prevValue };

      if (deleteValue) {
        delete newValue[key];
      } else if (inputType === 'textInput') {
        newValue[key] = {
          label: value,
          value,
        };
      } else {
        newValue[key] = {
          ...value,
        };
      }

      return newValue;
    });
  };

  const addPositiveValue = (key, value) => {
    const func = editableObj ? setEditableObject : setNumOfSupplier;
    func((prevVal) => {
      const newVal = { ...prevVal };
      if (value <= 0 && value !== '') {
        return newVal;
      }

      newVal[key] = {
        label: value,
        value,
      };

      return newVal;
    });
  };

  // Init NumberOfSuppliersCard constraint card
  const initCardConstraintState = () => {
    setFilterState({});
    setNumOfSupplier(INITIAL_FILTER_STATE);
    setEditableObject(null);
    setIndexOfEditableObj(INIT_INDEX);
  };

  const addConstraint = () => {
    if (Object.keys(filterState).length > 0) {
      addElement({
        ...numOfSupplier,
        filters: {
          ...filterState,
        },
      });

      initCardConstraintState();
    }
  };

  const updateConstraintCard = () => {
    if (Object.keys(filterState).length > 0) {

      handleEdit(
        {
          ...numOfSupplier,
          value: (editableObj?.value && editableObj.value.value !== '') ? editableObj.value : '',
          operation: {
            ...editableObj.operation,
          },
          mutation: {
            ...editableObj.mutation,
          },
          filters: {
            ...filterState,
          },
        },
        indexEditableObj
      );

      initCardConstraintState();
    }
  };

  const customHandleEdit = (item, index) => {
    setEditableObject(item);
    setIndexOfEditableObj(index);
    setFilterState(item.filters);
  };

  const cancelConstraintUpdate = () => {
    if (Object.keys(filterState).length > 0) {
      initCardConstraintState();
    }
  };

  return (
    <>
      <Box>
        <StyledContainer>
          <Filter
            alwaysShowFilters
            {...filterProps}
            onChange={setFilterState}
          />
          <StyledItemContainer>
            <FilterDropdown
              key="types"
              label={t('e.g. At least')}
              options={types}
              type={TYPE.SINGLE}
              values={editableObj?.operation || numOfSupplier.operation}
              onChange={(value, deleteValue) =>
                addValue('operation', value, deleteValue, 'combo')
              }
            />
          </StyledItemContainer>

          <StyledItemContainer>
            <FilterDropdown
              key="mutation"
              label={t('e.g. For all')}
              options={mutation}
              type={TYPE.SINGLE}
              values={editableObj?.mutation || numOfSupplier.mutation}
              onChange={(value, deleteValue) =>
                addValue('mutation', value, deleteValue, 'combo')
              }
            />
          </StyledItemContainer>

          <StyledItemContainer>
            <StyledTextField
              id="outlined-number"
              type="number"
              InputLabelProps={{
                shrink: true,
              }}
              placeholder={t('e.g 12')}
              variant="outlined"
              onChange={evt => addPositiveValue('value', evt.target.value)}
              className={clsx({
                editable: editableObj?.value?.value,
              })}
              value={
                editableObj?.value?.value ||
                (numOfSupplier.value ? numOfSupplier.value.value : '')
              }
            />
          </StyledItemContainer>
        </StyledContainer>

        <StyledBox>
          <ButtonGroupConstraint
            editable={editableObj}
            addConstraint={() => addConstraint()}
            updateConstraint={() => updateConstraintCard()}
            cancelConstraint={() => cancelConstraintUpdate()}
          />
        </StyledBox>
      </Box>

      <Box>
        <GridBoxElement
          elements={numOfSuppliers}
          onRemoveElement={removeElement}
          onHandleEdit={(item, index) => customHandleEdit(item, index)}
          staticViewStructure={['operation', 'mutation', 'value']}
          indexEditableObj={indexEditableObj}
        />
      </Box>
    </>
  );
};

NumberOfSuppliersComponent.propTypes = {
  addElement: PropTypes.func.isRequired,
  numOfSuppliers: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  regions: PropTypes.arrayOf(PropTypes.string).isRequired,
  removeElement: PropTypes.func.isRequired,
};

export const NumberOfSuppliersCard = NumberOfSuppliersComponent;
