import React from 'react';
import { PropTypes } from 'prop-types';
import styled from 'styled-components';
import {
  Box,
  Card,
  Grid,
} from '@material-ui/core';
import { Close } from '@material-ui/icons';
import { Text } from '@common/components/Text/Text';
import { colors } from '@utils/uiTheme';
import { useCustomTranslation } from '@utils/translate';

const StyledGridContainer = styled(Grid)`
  && {
    align-items: center;
    border-bottom: 1px solid ${colors.grey};
    padding: 20px;

    &:hover {
      background-color: #fafafa;
    }
  }
`;

const StyledCloseIcon = styled(Close)`
  && {
    visibility: hidden;
    width: 25px;
    fill: ${colors.iconGrey};
    cursor: pointer;

    ${StyledGridContainer}:hover & {
      visibility: visible;
    }
  }
`;

const StyledCard = styled(({
  alignTop: _alignTop,
  ...props
}) => <Card {...props} />)`
  && {
    overflow: auto;
    display: flex;
    flex-direction: column;
    justify-content: ${props => (props.alignTop ? 'flex-start' : 'center')};
    height: 262px;
    box-shadow: none;
    font-size: 17px;
  }
`;

export const SelectedItemsList = ({
  items,
  onRemoveElement,
  displayOnly,
  itemsWidthArray,
  keysOrder,
}) => {
  const { t } = useCustomTranslation();
  const keys = keysOrder || (items.length ? Object.keys(items[0]) : []);

  const itemWidth = itemsWidthArray.length ?
    itemsWidthArray :
    new Array(Math.max(keys.length, 0)).fill(Math.floor(11 / keys.length));
  const closeWidth = 12 - itemWidth.reduce((sum, item) => sum + item, 0);

  return (
    <Box
      mb={20}
      mt={20}
      elevation={0}
    >
      <StyledCard alignTop={!!items.length}>
        { items.length ?
          items.map((item, index) => {
            const key = Object.values(item).join('_');

            return (
              <StyledGridContainer
                container
                key={key}
              >
                { keys.map((itemKey, keyIndex) => (
                  <Grid
                    item
                    xs={itemWidth[keyIndex]}
                    key={itemKey}
                  >
                    {item[itemKey]}
                  </Grid>
                ))}
                <Grid
                  item
                  xs={closeWidth}
                >
                  { !displayOnly && (
                    <Box
                      display="flex"
                      justifyContent="flex-end"
                    >
                      <StyledCloseIcon onClick={() => onRemoveElement(index)} />
                    </Box>
                  )}
                </Grid>
              </StyledGridContainer>
            );
          }) : (
            <Box
              width="100%"
            >
              <Text
                fontSize={20}
                color={colors.iconGrey}
                textAlign="center"
              >
                {t('Add your constraints with the dropdown above.')}
              </Text>
            </Box>
          )}
      </StyledCard>
    </Box>
  );
};

SelectedItemsList.propTypes = ({
  displayOnly: PropTypes.bool,
  items: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  itemsWidthArray: PropTypes.arrayOf(PropTypes.number),
  keysOrder: PropTypes.arrayOf(PropTypes.string),
  onRemoveElement: PropTypes.func,
});

SelectedItemsList.defaultProps = ({
  displayOnly: false,
  itemsWidthArray: [],
  keysOrder: null,
  onRemoveElement: null,
});
