import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Box } from '@material-ui/core';
import { Filter } from '@common/components/Filter/Filter';
import {
  FilterDropdown,
  TYPE,
} from '@common/components/Filter/FilterDropdown/FilterDropdown';
import {
  RESERVED_FILTER_IDS,
  useProjectFilter,
} from '@common/components/Filter/filterHooks';

import { useCustomTranslation } from '@utils/translate';
import { limits, mutation } from '@utils/scenario/scenarioSettingsHelpers';
import clsx from 'clsx';
import { colors } from '@utils/uiTheme';
import { GridBoxElement } from './GridBoxElement';
import { ButtonGroupConstraint } from './ButtonGroupConstraint';

import { StyledTextField as TextField } from '../ScenarioCreation/TextField';

const StyledContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  flex-wrap: wrap;

  /* any direct child to have left margin */
  && > * {
    margin-left: 12px;
  }
  /* first direct child NOT to have left margin */
  && > *:first-child {
    margin-left: 0;
  }
`;

const StyledItemContainer = styled.div`
  display: flex;
  align-items: center;
`;

const StyledBox = styled.div`
  text-align: right;
  margin: 10px 0 0 0;
`;

const StyledTextField = styled(TextField)`
  &&.editable {
    .MuiInputBase-root {
      background-color: ${colors.lightGrey};
      border: 1px solid ${colors.lightGrey};
      box-shadow: none;
    }
  }
`;

const supplierFilter = [
  {
    id: RESERVED_FILTER_IDS.PROJECT_SUPPLIERS,
    alias: 'supplier',
    label: 'Supplier',
    type: TYPE.MULTI,
  },
];

const INITIAL_FILTER_STATE = {
  mutation: {
    label: 'For all',
    value: 'for_all',
  },
  operation: {
    label: 'At most',
    value: 'max',
  },
  unit: {
    label: 'Percentage Spend',
    value: 'percentage',
  },
};

const LimitSpendsComponent = ({
  maxSpend,
  addElement,
  removeElement,
  projectCurrency,
  projectUnit,
  handleEdit,
}) => {
  const { t } = useCustomTranslation();

  const [limitSpendCard, setLimitSpendCard] = useState(INITIAL_FILTER_STATE);

  const { filterProps, filterState, setFilterState } = useProjectFilter({
    additionalFilters: supplierFilter,
    initialState: {},
  });

  // todo refactor logic, see task https://archlet.atlassian.net/browse/AA-1162
  const INIT_INDEX = -1;
  const [editableObj, setEditableObject] = useState(null); // todo types <{} | null>
  const [indexEditableObj, setIndexOfEditableObj] = useState(INIT_INDEX); // todo undefined? <number>

  const PERCENTAGE_FILTER_UNITS = [
    {
      label: `Volume (${projectUnit})`,
      value: 'volume',
    },
    {
      label: 'Percentage Spend',
      value: 'percentage',
    },
    {
      label: `Spend (${projectCurrency})`,
      value: 'spend',
    },
  ];

  const addValue = (key, value, deleteValue, inputType) => {
    const func = editableObj ? setEditableObject : setLimitSpendCard;
    func((prevValue) => {
      const newValue = { ...prevValue };

      if (deleteValue) {
        delete newValue[key];
      } else if (inputType === 'textInput') {
        newValue[key] = {
          label: value,
          value,
        };
      } else {
        newValue[key] = {
          ...value,
        };
      }

      return newValue;
    });
  };

  const addPositiveValue = (key, value) => {
    const func = editableObj ? setEditableObject : setLimitSpendCard;

    func((prevValue) => {
      const newValue = { ...prevValue };

      if (value <= 0 && value !== '') {
        return newValue;
      }

      newValue[key] = {
        label: value,
        value,
      };

      return newValue;
    });
  };

  // Init LimitSpend constraint card
  const initCardConstraintState = () => {
    setFilterState({});
    setLimitSpendCard(INITIAL_FILTER_STATE);

    setEditableObject(null);
    setIndexOfEditableObj(INIT_INDEX);
  };

  const addSpend = () => {
    if (
      Object.keys(filterState).length > 0 &&
      Object.keys(filterState).includes('supplier')
    ) {
      addElement({
        ...limitSpendCard,
        filters: {
          ...filterState,
        },
      });

      initCardConstraintState();
    }
  };

  const updateConstraintCard = () => {
    if (
      Object.keys(filterState).length > 0 &&
      Object.keys(filterState).includes('supplier')
    ) {
      handleEdit(
        {
          ...limitSpendCard,
          value: (editableObj?.value && editableObj.value.value !== '') ? editableObj.value : '',
          operation: {
            ...editableObj.operation,
          },
          mutation: {
            ...editableObj.mutation,
          },
          unit: {
            ...editableObj.unit,
          },
          filters: {
            ...filterState,
          },
        },
        indexEditableObj
      );

      initCardConstraintState();
    }
  };

  const cancelConstraintUpdate = () => {
    if (Object.keys(filterState).length > 0) {
      initCardConstraintState();
    }
  };

  const customHandleEdit = (item, index) => {
    setEditableObject(item);
    setIndexOfEditableObj(index);
    setFilterState(item.filters);
  };

  return (
    <>
      <Box>
        <StyledContainer>
          <Filter
            {...filterProps}
            alwaysShowFilters
            onChange={setFilterState}
          />
          <StyledItemContainer>
            <FilterDropdown
              key="operation"
              label={t('e.g. At least')}
              options={limits}
              type={TYPE.SINGLE}
              values={editableObj?.operation || limitSpendCard.operation}
              onChange={(value, deleteValue) =>
                addValue('operation', value, deleteValue, 'combo')
              }
            />
          </StyledItemContainer>

          <StyledItemContainer>
            <FilterDropdown
              key="mutation"
              label={t('e.g. For all')}
              options={mutation}
              type={TYPE.SINGLE}
              values={editableObj?.mutation || limitSpendCard.mutation}
              onChange={(value, deleteValue) =>
                addValue('mutation', value, deleteValue, 'combo')
              }
            />
          </StyledItemContainer>

          <StyledItemContainer>
            <FilterDropdown
              key="unit"
              label={t('e.g. Percentage')}
              options={PERCENTAGE_FILTER_UNITS}
              type={TYPE.SINGLE}
              values={editableObj?.unit || limitSpendCard.unit}
              onChange={(value, deleteValue) =>
                addValue('unit', value, deleteValue, 'combo')
              }
            />
          </StyledItemContainer>
          <StyledItemContainer>
            <StyledTextField
              id="outlined-number"
              placeholder={t('e.g 12')}
              type="number"
              InputLabelProps={{
                shrink: true,
              }}
              variant="outlined"
              onChange={evt => addPositiveValue('value', evt.target.value)}
              className={clsx({
                editable: editableObj?.value?.value,
              })}
              value={
                editableObj?.value?.value ||
                (limitSpendCard.value ? limitSpendCard.value.value : '')
              }
            />
          </StyledItemContainer>
        </StyledContainer>

        <StyledBox>
          <ButtonGroupConstraint
            editable={editableObj}
            addConstraint={() => addSpend()}
            updateConstraint={() => updateConstraintCard()}
            cancelConstraint={() => cancelConstraintUpdate()}
          />
        </StyledBox>
      </Box>

      <Box>
        <GridBoxElement
          elements={maxSpend}
          onRemoveElement={removeElement}
          onHandleEdit={(item, index) => customHandleEdit(item, index)}
          hasSupplier
          staticViewStructure={['operation', 'mutation', 'unit', 'value']}
          indexEditableObj={indexEditableObj}
        />
      </Box>
    </>
  );
};

LimitSpendsComponent.propTypes = {
  addElement: PropTypes.func.isRequired,
  maxSpend: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  projectCurrency: PropTypes.string.isRequired,
  projectUnit: PropTypes.string.isRequired,
  removeElement: PropTypes.func.isRequired,
};

export const LimitSpendsCard = LimitSpendsComponent;
