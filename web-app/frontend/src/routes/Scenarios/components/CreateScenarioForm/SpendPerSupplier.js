import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import {
  Box,
  Divider,
  FormControl,
  TextField,
} from '@material-ui/core';
import { useCustomTranslation } from '@utils/translate';

const StyledTextField = styled(TextField)`
  && {
    .MuiInputBase-input {
      padding-top: 14px;
      font-size: 13px;
    }

    margin: 8px 0 8px 20px;
  }
`;

export const SpendPerSupplier = ({
  defaultValue,
  input: {
    onChange,
    value,
  },
  meta,
}) => {
  if (!value && !meta.dirty) onChange(defaultValue);
  const { t } = useCustomTranslation();

  return (
    <FormControl>
      <Box
        display="flex"
        alignItems="baseline"
      >
        <div>{t('Maximum spend per supplier in USD')}</div>
        <StyledTextField
          value={Number(value) !== -1 ? value : ''}
          placeholder={t('e.g. 450,000')}
          onChange={onChange ? ({ target }) => onChange(target.value ? target.value : '-1') : null}
        />
      </Box>
      <Divider />
    </FormControl>
  );
};

SpendPerSupplier.propTypes = ({
  defaultValue: PropTypes.string.isRequired,
  input: PropTypes.shape({}).isRequired,
  meta: PropTypes.shape({}).isRequired,
});
