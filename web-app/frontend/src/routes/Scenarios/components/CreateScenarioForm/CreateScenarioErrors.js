import React from 'react';
import styled from 'styled-components';
import { colors } from '@utils/uiTheme';
import {
  Box,
  Card,
} from '@material-ui/core';
import PropTypes from 'prop-types';
import { Text } from '@common/components/Text/Text';
import { ErrorsList } from '@common/components/ErrorsList/ErrorsList';
import {
  getCardStyle,
  StyledError,
} from '@utils/getCardStyle';
import { useCustomTranslation } from '@utils/translate';
import { categorizeErrors } from '@utils/error';

const StyledTitleText = styled.h5`
  margin: 0;
  color: ${colors.darkGrey};
  font-size: 15px;
`;

const StyledSubtitle = styled.h5`
  margin: 10px 40px 10px 0;
  font-size: 25px;
`;

const StyledTitleContainer = styled.div`
  width: 100%;
`;

const StyledExclamation = styled.h5`
  margin: 0;
  margin-right: 122px;
  color: ${props => props.color};
  font-size: 63px;
  line-height: 63px;
`;

const StyledCard = styled(Card)`
  && {
    margin-top: 20px;
    border: 1px solid ${props => props.color};
    padding: 30px;
  }
`;

const StyledText = styled(Text)`
  margin-left: 7px;
`;

const StyledErrorsList = styled(ErrorsList)`
  padding-left: 141px;
`;

export const CreateScenarioErrors = ({
  error,
  validation,
}) => {
  const cardColor = (validation.errors || error) ? colors.error : colors.warning;
  const { t } = useCustomTranslation();

  return (
    <StyledCard
      color={cardColor}
      elevation={0}
    >
      <Box display="flex">
        <StyledExclamation color={cardColor}>!</StyledExclamation>
        <StyledTitleContainer>
          <StyledTitleText>{t('Errors and Warnings')}</StyledTitleText>
          <StyledSubtitle>
            {t('Please review the following errors and/or warnings before creating your scenario.')}
          </StyledSubtitle>
        </StyledTitleContainer>
      </Box>
      {error.length > 0 ? (
        <StyledErrorsList
          validation={categorizeErrors(error)}
          cardStyle={getCardStyle(error)}
          hideBorder
        />
      ) : (
        <Box
          display="flex"
          alignItems="center"
          mt={35}
          padding="20px"
        >
          <StyledError />
          <StyledText
            fontSize={18}
            color={colors.red}
            fontWeight={500}
          >
            { error }
          </StyledText>
        </Box>
      )}
    </StyledCard>
  );
};

CreateScenarioErrors.propTypes = {
  error: PropTypes.string,
  validation: PropTypes.shape({}),
};

CreateScenarioErrors.defaultProps = {
  error: '',
  validation: {},
};
