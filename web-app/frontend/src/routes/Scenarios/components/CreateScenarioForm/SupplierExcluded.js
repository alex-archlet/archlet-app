import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {
  Box,
  Checkbox,
  FormControl,
  Input,
  ListItemText,
  MenuItem,
  Select,
} from '@material-ui/core';
import { Button } from '@common/components/Button/Button';
import { useCustomTranslation } from '@utils/translate';
import { Cancel } from '@material-ui/icons';
import { colors } from '@utils/uiTheme';

const StyledSelect = styled(Select)`
  && {
    width: 300px;

    &.MuiFormControl-root {
      display: none;
    }
  }
`;

const StyledInput = styled(Input)`
  && {
    opacity: 0;
  }
`;

const StyledButton = styled(Button)`
  && {
    margin: 0 12px 12px 0;
    font-weight: 400;
    text-transform: none;
  }
`;

const StyledCancelIcon = styled(Cancel)`
  && {
    fill: ${colors.darkGrey};
    width: 18px;
    height: 18px;
    margin-left: 20px;
  }
`;

const AddButton = styled(Button)`
  && {
    margin-top: 20px;
  }
`;

export const SupplierExcluded = ({
  input: { onChange, value },
  meta,
  suppliers,
}) => {
  if (!value && !meta.dirty) onChange([]);
  const { t } = useCustomTranslation();

  const [open, setOpen] = useState(false);

  const [anchorEl, setAnchorEl] = useState(null);

  function handleClose() {
    setOpen(false);
  }

  function handleOpen(event) {
    if (event.currentTarget) setAnchorEl(event.currentTarget);

    setOpen(true);
  }

  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: 300,
        top: 300,
      },
    },
    anchorEl,
  };

  const removeSupplier = supplier =>
    onChange(value.filter(item => item !== supplier));

  return (
    <div>
      {!!value.length && (
        <Box>
          {!!value.length &&
            value.map(selectedSupplier => (
              <StyledButton
                key={selectedSupplier}
                color="primary"
                onClick={() => removeSupplier(selectedSupplier)}
              >
                {selectedSupplier}
                <StyledCancelIcon />
              </StyledButton>
            ))}
        </Box>
      )}
      <AddButton onClick={handleOpen} variant="outlined">
        {t('Add Suppliers')}
      </AddButton>
      <FormControl>
        <StyledSelect
          open={open}
          onClose={handleClose}
          onOpen={handleOpen}
          multiple
          value={value || []}
          onChange={onChange}
          input={<StyledInput />}
          MenuProps={MenuProps}
        >
          {suppliers.map(supplier => (
            <MenuItem key={supplier} value={supplier}>
              <Checkbox checked={value.indexOf(supplier) > -1} />
              <ListItemText primary={supplier} />
            </MenuItem>
          ))}
        </StyledSelect>
      </FormControl>
    </div>
  );
};

SupplierExcluded.propTypes = {
  input: PropTypes.shape({}).isRequired,
  meta: PropTypes.shape({}).isRequired,
  suppliers: PropTypes.arrayOf(PropTypes.string).isRequired,
};
