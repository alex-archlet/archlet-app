import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Box } from '@material-ui/core';
import { stringToDropdownOption } from '@common/components/Filter/filterUtils';
import { Filter } from '@common/components/Filter/Filter';
import {
  FilterDropdown,
  TYPE,
} from '@common/components/Filter/FilterDropdown/FilterDropdown';
import {
  RESERVED_FILTER_IDS,
  useProjectFilter,
} from '@common/components/Filter/filterHooks';
import { useCustomTranslation } from '@utils/translate';
import { operations } from '@utils/scenario/scenarioSettingsHelpers';
import clsx from 'clsx';
import { colors } from '@utils/uiTheme';
import { GridBoxElement } from './GridBoxElement';
import { StyledTextField as TextField } from '../ScenarioCreation/TextField';
import { ButtonGroupConstraint } from './ButtonGroupConstraint';

const StyledContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  flex-wrap: wrap;

  /* any direct child to have left margin */
  && > * {
    margin-left: 12px;
  }
  /* first direct child NOT to have left margin */
  && > *:first-child {
    margin-left: 0;
  }
`;

const StyledItemContainer = styled.div`
  display: flex;
  align-items: center;
`;

const StyledBox = styled.div`
  text-align: right;
  margin: 10px 0 0 0;
`;

const StyledTextField = styled(TextField)`
  &&.editable {
    .MuiInputBase-root {
      background-color: ${colors.lightGrey};
      border: 1px solid ${colors.lightGrey};
      box-shadow: none;
    }
  }
`;

const EXCLUDE_ALL = 'Exclude All';
const KEEP_HIGHEST = 'keep_highest';
const KEEP_LOWEST = 'keep_lowest';

const supplierFilter = [
  {
    id: RESERVED_FILTER_IDS.PROJECT_SUPPLIERS,
    label: 'Supplier',
    alias: 'supplier',
    type: TYPE.MULTI,
  },
];

const ExcludeFromAnalysisComponent = ({
  suppliersExcluded,
  addElement,
  removeElement,
  criteria,
  handleEdit,
}) => {
  const { t } = useCustomTranslation();

  const [excludeFromAnalysis, setExcludeFromAnalysis] = useState({});

  const { filterProps, filterState, setFilterState } = useProjectFilter({
    additionalFilters: supplierFilter,
    initialState: {},
  });

  // todo refactor logic, see task https://archlet.atlassian.net/browse/AA-1162
  const INIT_INDEX = -1;
  const [editableObj, setEditableObject] = useState(null); // todo types <{} | null>
  const [indexEditableObj, setIndexOfEditableObj] = useState(INIT_INDEX); // todo undefined? <number>

  const addValue = (key, value, deleteValue, inputType) => {
    const func = editableObj ? setEditableObject : setExcludeFromAnalysis;

    func((prevValue) => {
      const newValue = { ...prevValue };

      if (deleteValue) {
        delete newValue[key];
      } else if (inputType === 'textInput') {
        newValue[key] = {
          label: value,
          value,
        };
      } else {
        newValue[key] = {
          ...value,
        };
      }

      return newValue;
    });
  };

  const addPositiveValue = (key, value) => {
    const func = editableObj ? setEditableObject : setExcludeFromAnalysis;
    func((prevValue) => {
      const newValue = { ...prevValue };

      if (value <= 0 && value !== '') {
        return newValue;
      }

      newValue[key] = {
        label: value,
        value,
      };

      return newValue;
    });
  };

  const isExcludeAll = (
    excludeFromAnalysis.criteria && 
    excludeFromAnalysis.criteria.value === EXCLUDE_ALL
  ) || (
    editableObj?.criteria && 
    editableObj.criteria.value === EXCLUDE_ALL
  );

  const keepHighestLowest = (
    excludeFromAnalysis.operation &&
    (
      excludeFromAnalysis.operation.value === KEEP_HIGHEST ||
      excludeFromAnalysis.operation.value === KEEP_LOWEST
    )
  ) || (
    editableObj?.operation &&
    (
      editableObj.operation.value === KEEP_HIGHEST ||
      editableObj.operation.value === KEEP_LOWEST
    )
  )
  
  // Init NumberOfSuppliersCard constraint card
  const initCardConstraintState = () => {
    setFilterState({});
    setExcludeFromAnalysis({});
    setEditableObject(null);
    setIndexOfEditableObj(INIT_INDEX);
  };

  const addExcludedSupplier = () => {
    if (Object.keys(filterState).includes('supplier')) {
      addElement({
        ...excludeFromAnalysis,
        filters: {
          ...filterState,
        },
      });

      initCardConstraintState();
    }
  };

  const updateConstraintCard = () => {
    if (Object.keys(filterState).length > 0) {
      handleEdit(
        {
          ...excludeFromAnalysis,
          value: (editableObj?.value && editableObj.value.value !== '') ? editableObj.value : '',
          operation: (editableObj?.operation ? editableObj.operation : null),
          criteria: (editableObj?.criteria ? editableObj.criteria : null),
          filters: {
            ...filterState,
          },
        },
        indexEditableObj
      );

      initCardConstraintState();
    }
  };

  const customHandleEdit = (item, index) => {
    setEditableObject(item);
    setIndexOfEditableObj(index);
    setFilterState(item.filters);
  };

  const cancelConstraintUpdate = () => {
    if (Object.keys(filterState).length > 0) {
      initCardConstraintState();
    }
  };

  return (
    <>
      <Box>
        <StyledContainer>
          <Filter
            {...filterProps}
            onChange={setFilterState}
            alwaysShowFilters
          />
          <StyledItemContainer>
            <FilterDropdown
              key="criteria"
              label={t('Criteria')}
              options={criteria.map(stringToDropdownOption)}
              type={TYPE.SINGLE}
              values={editableObj?.criteria || excludeFromAnalysis.criteria}
              onChange={(value, deleteValue) =>
                addValue('criteria', value, deleteValue, 'combo')
              }
            />
          </StyledItemContainer>

          {!isExcludeAll && (
            <StyledItemContainer>
              <FilterDropdown
                key="operations"
                label={t('Operations')}
                type={TYPE.SINGLE}
                options={operations}
                values={editableObj?.operation || excludeFromAnalysis.operation}
                onChange={(value, deleteValue) =>
                  addValue('operation', value, deleteValue, 'combo')
                }
              />
            </StyledItemContainer>
          )}

          {!isExcludeAll && !keepHighestLowest && (
            <StyledItemContainer>
              <StyledTextField
                id="outlined-number"
                placeholder={t('e.g 12')}
                type="number"
                InputLabelProps={{
                  shrink: true,
                }}
                variant="outlined"
                onChange={evt => addPositiveValue('value', evt.target.value)}
                className={clsx({
                  editable: editableObj?.value?.value,
                })}
                value={
                  editableObj?.value?.value ||
                  (excludeFromAnalysis.value
                    ? excludeFromAnalysis.value.value
                    : '')
                }
              />
            </StyledItemContainer>
          )}
        </StyledContainer>

        <StyledBox>
          <ButtonGroupConstraint
            editable={editableObj}
            addConstraint={() => addExcludedSupplier()}
            updateConstraint={() => updateConstraintCard()}
            cancelConstraint={() => cancelConstraintUpdate()}
          />
        </StyledBox>
      </Box>

      <Box>
        <GridBoxElement
          elements={suppliersExcluded}
          onRemoveElement={removeElement}
          onHandleEdit={(item, index) => customHandleEdit(item, index)}
          hasSupplier
          staticViewStructure={['criteria', 'operation', 'value']}
          indexEditableObj={indexEditableObj}
        />
      </Box>
    </>
  );
};

ExcludeFromAnalysisComponent.propTypes = {
  addElement: PropTypes.func.isRequired,
  criteria: PropTypes.arrayOf(PropTypes.string).isRequired,
  removeElement: PropTypes.func.isRequired,
  suppliersExcluded: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  suppliersPerRegions: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
};

export const ExcludeFromAnalysisCard = ExcludeFromAnalysisComponent;
