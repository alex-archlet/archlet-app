import React from 'react';
import styled from 'styled-components';
import { PrimaryButton } from '@common/components/Button/PrimaryButton';
import { SecondaryButton } from '@common/components/Button/SecondaryButton';
import { CancelButton } from '@common/components/Button/CancelButton';
import { useCustomTranslation } from '@utils/translate';

const StyledCancelButton = styled(CancelButton)`
  && {
    margin: 0 0 0 10px;
  }
`;

interface Props {
  editable: boolean;
  addConstraint: () => {};
  updateConstraint: () => {};
  cancelConstraint: () => {};
}

export const ButtonGroupConstraint: React.FC<Props> = ({
  editable,
  addConstraint,
  updateConstraint,
  cancelConstraint,
}) => {
  const { t } = useCustomTranslation();

  return (
    editable ? <>
      <SecondaryButton onClick={updateConstraint}>
        {t('Update')}
      </SecondaryButton>
      <StyledCancelButton onClick={cancelConstraint}>
        {t('Cancel')}
      </StyledCancelButton>
    </> : <PrimaryButton onClick={addConstraint}>{t('Add')}</PrimaryButton>
  )  
};
