import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form';
import styled from 'styled-components';
import { colors } from '@utils/uiTheme';
import { useCustomTranslation } from '@utils/translate';
import { biddingRoundsSelectors } from '@store/selectors/biddingRounds';
import { Text } from '@common/components/Text/Text';
import { Box } from '@material-ui/core';
import { Error } from '@material-ui/icons';
import { useParams } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { getScenarios } from '@store/actions/projectScenarios';
import { getRoundsList } from '@store/actions/biddingRounds';
import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';
import {
  stringToDropdownOption,
  nameIdDropdownOption,
} from '@common/components/Filter/filterUtils';
import {
  findValueFromIncumbentArray,
  findValueFromIdArray,
  translateObject,
} from '@utils/scenario/scenarioSettingsHelpers';
import { projectScenariosSelectors } from '@store/selectors/projectScenarios';
import { transformFilters } from '@utils/scenario/transform-filters';
import { FormExpansionPanel } from './FormExpansionPanel';
import { GeneralSettingsCard } from './GeneralSettingsCard';
import { NumberOfSuppliersCard } from './NumberOfSuppliersCard';
import { LimitSpendsCard } from './LimitSpendsCard';
import { ExcludeFromAnalysisCard } from './ExcludeFromAnalysisCard';
import { ScenarioSubmitCard } from './ScenarioSubmitCard';
import { CreateScenarioErrors } from './CreateScenarioErrors';
import { GridBoxElement } from './GridBoxElement';

const StyledValue = styled.p`
  margin-bottom: 10px;
  color: ${colors.iconGrey};
  font-size: 25px;
`;

export const StyledError = styled(Error)`
  && {
    fill: ${colors.error};
    width: 18px;
    height: 18px;
    margin-right: 8px;
  }
`;
const OPENED_SECTION = 'opened';

const CreateScenarioFormComponent = ({
  customError,
  scenarioFormValues,
  customScenarioConstraints,
  isEdit,
  onSubmitForm,
  onSetExcludedSuppliers,
  onSetNumOfSuppliers,
  onSetMaxSpend,
  projectCurrency,
  projectUnit,
  regions,
  suppliersPerRegions,
  setCustomScenarioConstraints,
  criteria,
  scenarioDetails: { json: values, name, validation },
}) => {
  const { t } = useCustomTranslation();

  const { id: projectId } = useParams();
  const rounds = useSelector(
    biddingRoundsSelectors.getBiddingRoundListOrderByCreatedAt
  );
  const scenarios = useSelector(projectScenariosSelectors.getScenarios);

  const dispatch = useDispatch();

  const getScenariosActions = () => dispatch(getScenarios(projectId));
  const getRoundsListActions = () => dispatch(getRoundsList(projectId));

  useEffect(() => {
    getScenariosActions();
    getRoundsListActions();
  }, []);

  const [state, setState] = useState({
    expanded: isEdit ? null : 1,
    historyPanel: { 1: OPENED_SECTION },
  });

  const [numOfSuppliers, setNumOfSuppliers] = useState([]);
  const [maxSpend, setMaxSpend] = useState([]);

  const [suppliersExcluded, setSuppliersExcluded] = useState([]);

  useEffect(() => {
    if (rounds.length > 1 && !isEdit) {
      setCustomScenarioConstraints({
        round: {
          ...rounds.map(nameIdDropdownOption)[0],
        },
      });
    }
  }, [rounds]);

  const transformValues = (items) => {
    return items.map((item) => {
      const result = {};
      // eslint-disable-next-line no-restricted-syntax
      for (const [key, value] of Object.entries(item)) {
        const label = translateObject(key, value, projectCurrency, projectUnit);
        if (key !== 'filters') {
          result[key] = {
            label,
            value,
          };
        } else {
          result[key] = transformFilters(value);
        }
      }
      return result;
    });
  };

  useEffect(() => {
    if (isEdit) {
      setNumOfSuppliers(
        values.number_of_suppliers
          ? transformValues(values.number_of_suppliers)
          : []
      );
      onSetNumOfSuppliers(
        values.number_of_suppliers
          ? transformValues(values.number_of_suppliers)
          : []
      );

      setMaxSpend(
        values.limit_spend ? transformValues(values.limit_spend) : []
      );
      onSetMaxSpend(
        values.limit_spend ? transformValues(values.limit_spend) : []
      );

      setSuppliersExcluded(
        values.excluded_suppliers ? transformValues(values.excluded_suppliers) : []
      );
      onSetExcludedSuppliers(
        values.excluded_suppliers
          ? transformValues(values.excluded_suppliers)
          : []
      );

      const { general_settings } = values;
      setCustomScenarioConstraints({
        capacities: general_settings.capacities
          ? general_settings.capacities.map(stringToDropdownOption)
          : [],
        incumbent: general_settings.incumbent
          ? findValueFromIncumbentArray(general_settings.incumbent)
          : [],
        itemType: general_settings.item_type
          ? general_settings.item_type.map(stringToDropdownOption)
          : [],
        partialAllocation: general_settings.partial_allocation,
        priceComponent: general_settings.price_component
          ? general_settings.price_component.map(stringToDropdownOption)[0]
          : [],
        rebates: general_settings.rebates,
        splitItems: general_settings.split_items,
        volume: general_settings.volume
          ? general_settings.volume.map(stringToDropdownOption)
          : null,
        round: general_settings.round_id
          ? findValueFromIdArray(general_settings.round_id, rounds)
          : [],
        scenario: general_settings.scenario_id
          ? findValueFromIdArray(general_settings.scenario_id, scenarios)
          : [],
      });
    }
  }, [rounds]);

  const removeElementAtIndex = (list, index) => [
    ...list.slice(0, index),
    ...list.slice(index + 1, list.length),
  ];

  const addElementToStart = (list, item) => [item, ...list];

  const updateElement = (list, value, index) => [
    ...list.slice(0, index),
    value,
    ...list.slice(index + 1, list.length),
  ];

  // add, update, edit for NumberOfSuppliersCard
  const addNumOfSuppliersElement = (value) => {
    setNumOfSuppliers(prevValue => addElementToStart(prevValue, value));
    onSetNumOfSuppliers(prevValue => addElementToStart(prevValue, value));
  };

  const removeNumOfSuppliersElement = (index) => {
    setNumOfSuppliers(prevValue => removeElementAtIndex(prevValue, index));
    onSetNumOfSuppliers(prevValue => removeElementAtIndex(prevValue, index));
  };

  const editNumOfSuppliersElement = (item, index) => {
    setNumOfSuppliers(list => updateElement(list, item, index));
    onSetNumOfSuppliers(list => updateElement(list, item, index));
  };

  // add, update, edit for LimitSpendCard
  const addMaxSpendElement = (value) => {
    setMaxSpend(prevValue => addElementToStart(prevValue, value));
    onSetMaxSpend(prevValue => addElementToStart(prevValue, value));
  };

  const removeMaxSpendElement = (index) => {
    setMaxSpend(prevValue => removeElementAtIndex(prevValue, index));
    onSetMaxSpend(prevValue => removeElementAtIndex(prevValue, index));
  };

  const editMaxSpendElement = (item, index) => {
    setMaxSpend(list => updateElement(list, item, index));
    onSetMaxSpend(list => updateElement(list, item, index));
  };

  // add, update, edit for ExcludeFromAnalysisCard
  const addSuppliersExcludedElement = (value) => {
    setSuppliersExcluded(prevValue => addElementToStart(prevValue, value));
    onSetExcludedSuppliers(prevValue => addElementToStart(prevValue, value));
  };

  const removeSuppliersExcludedElement = (index) => {
    setSuppliersExcluded(prevValue => removeElementAtIndex(prevValue, index));
    onSetExcludedSuppliers(prevValue => removeElementAtIndex(prevValue, index));
  };

  const editSuppliersExcludedElement = (item, index) => {
    setSuppliersExcluded(list => updateElement(list, item, index));
    onSetExcludedSuppliers(list => updateElement(list, item, index));
  };

  const handleChange = (panel) => {
    setState({
      ...state,
      expanded: panel,
      historyPanel: {
        ...state.historyPanel,
        [panel]: OPENED_SECTION,
      },
    });
  };

  const initialValues = values
    ? {
        ...values,
        name,
      }
    : null;

  const isValid = !validation || (!validation.errors && !validation.warnings);

  const isDone = index => state.historyPanel[index] === OPENED_SECTION;

  return (
    <div>
      <VerticalSpacer height={10} />
      <FormExpansionPanel
        hasInputValue
        index={1}
        isEdit={isEdit}
        isExpanded={state.expanded === 1}
        isValid={isValid}
        isDone={isDone(1)}
        onChange={handleChange}
        title={t('General')}
        fullWidth
        subtitle={t('Scenario Settings')}
        formComponent={
          <GeneralSettingsCard
            onClick={handleChange}
            initialValues={initialValues}
            customScenarioConstraints={customScenarioConstraints}
            setCustomScenarioConstraints={setCustomScenarioConstraints}
          />
        }
        valueComponent={
          isEdit &&
          values && <StyledValue>{t('Scenario Settings')}</StyledValue>
        }
      />

      <FormExpansionPanel
        index={2}
        isEdit={isEdit}
        isExpanded={state.expanded === 2}
        isValid={isValid}
        isDone={isDone(2) && numOfSuppliers.length > 0}
        onChange={handleChange}
        title={t('Number of Suppliers')}
        subtitle={t('Do you want to limit the number of winning suppliers?')}
        fullWidth
        formComponent={
          <NumberOfSuppliersCard
            numOfSuppliers={numOfSuppliers}
            addElement={addNumOfSuppliersElement}
            removeElement={removeNumOfSuppliersElement}
            handleEdit={editNumOfSuppliersElement}
            initialValues={initialValues}
            regions={regions}
          />
        }
        valueComponent={
          isEdit &&
          values &&
          numOfSuppliers && (
            <GridBoxElement
              elements={numOfSuppliers}
              isDisplayInViewMode
              staticViewStructure={['operation', 'mutation', 'value']}
            />
          )
        }
      />

      <FormExpansionPanel
        index={3}
        isEdit={isEdit}
        isExpanded={state.expanded === 3}
        isValid={isValid}
        isDone={isDone(3) && maxSpend.length > 0}
        onChange={handleChange}
        title={t('Limit Spend')}
        subtitle={t('Do you want to limit the spend per supplier and region?')}
        fullWidth
        formComponent={
          <LimitSpendsCard
            maxSpend={maxSpend}
            addElement={addMaxSpendElement}
            removeElement={removeMaxSpendElement}
            handleEdit={editMaxSpendElement}
            initialValues={initialValues}
            projectCurrency={projectCurrency}
            projectUnit={projectUnit}
            suppliersPerRegions={suppliersPerRegions}
          />
        }
        valueComponent={
          isEdit &&
          values &&
          maxSpend && (
            <GridBoxElement
              elements={maxSpend}
              isDisplayInViewMode
              hasSupplier
              staticViewStructure={['operation', 'mutation', 'unit', 'value']}
            />
          )
        }
      />

      <FormExpansionPanel
        index={4}
        isEdit={isEdit}
        isExpanded={state.expanded === 4}
        isValid={isValid}
        isDone={isDone(4) && suppliersExcluded.length > 0}
        onChange={handleChange}
        title={t('Exclude from Analysis')}
        subtitle={t(
          'Do you want to exclude any suppliers or constraints from this scenario?'
        )}
        fullWidth
        formComponent={
          <ExcludeFromAnalysisCard
            suppliersExcluded={suppliersExcluded}
            addElement={addSuppliersExcludedElement}
            removeElement={removeSuppliersExcludedElement}
            handleEdit={editSuppliersExcludedElement}
            suppliersPerRegions={suppliersPerRegions}
            criteria={criteria}
          />
        }
        valueComponent={
          isEdit &&
          values &&
          suppliersExcluded && (
            <GridBoxElement
              elements={suppliersExcluded}
              isDisplayInViewMode
              hasSupplier
              staticViewStructure={['criteria', 'operation', 'value']}
            />
          )
        }
      />

      <FormExpansionPanel
        index={8}
        isEdit={isEdit}
        isExpanded={state.expanded === 8}
        isValid={isValid}
        isDone={isDone(8)}
        onChange={handleChange}
        title={t('Scenario details')}
        subtitle={t('How do you want to name your scenario?')}
        formComponent={
          <ScenarioSubmitCard onSubmit={onSubmitForm} isEdit={isEdit} />
        }
        valueComponent={
          isEdit && (
            <Box mt={20}>
              <Text fontSize={17} fontWeight={400}>
                {scenarioFormValues.name}
              </Text>
            </Box>
          )
        }
      />

      {customError && (
        <CreateScenarioErrors validation={validation} error={customError} />
      )}
    </div>
  );
};

CreateScenarioFormComponent.propTypes = {
  criteria: PropTypes.arrayOf(PropTypes.string).isRequired,
  customError: PropTypes.arrayOf(PropTypes.object.isRequired).isRequired,
  customScenarioConstraints: PropTypes.objectOf(PropTypes.array).isRequired,
  isEdit: PropTypes.bool,
  onSetExcludedSuppliers: PropTypes.func,
  onSetMaxSpend: PropTypes.func,
  onSetNumOfSuppliers: PropTypes.func,
  onSubmitForm: PropTypes.func.isRequired,
  projectCurrency: PropTypes.string.isRequired,
  projectUnit: PropTypes.string.isRequired,
  regions: PropTypes.arrayOf(PropTypes.string).isRequired,
  scenarioDetails: PropTypes.shape({}),
  scenarioFormValues: PropTypes.shape({}),
  setCustomScenarioConstraints: PropTypes.func.isRequired,
  suppliersPerRegions: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
};

CreateScenarioFormComponent.defaultProps = {
  isEdit: false,
  onSetExcludedSuppliers: () => {},
  onSetMaxSpend: () => {},
  onSetNumOfSuppliers: () => {},
  scenarioDetails: {
    json: {
      preprocessing: {},
    },
    validation: {},
  },
  scenarioFormValues: null,
};

export const CreateScenarioForm = reduxForm({
  destroyOnUnmount: true,
  form: 'CreateScenarioForm',
})(CreateScenarioFormComponent);
