import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Box } from '@material-ui/core';
import { SecondaryButton } from '@common/components/Button/SecondaryButton';
import { Field, Form, reduxForm } from 'redux-form';
import { useCustomTranslation } from '@utils/translate';
import { ReduxTextField } from '@common/components/ReduxForms/ReduxTextField';

const validate = (values) => {
  const errors = {};

  if (!values.name) errors.name = 'Scenario name is required';

  return errors;
};

const StyledField = styled(Field)`
  && {
    margin: 8px 0;
  }
`;

const CreateButton = styled(SecondaryButton)`
  && {
    margin: 10px auto 0;
    max-width: 300px;
  }
`;

const ScenarioSubmitComponent = ({ valid, handleSubmit, isEdit }) => {
  const { t } = useCustomTranslation();

  return (
    <Form onSubmit={handleSubmit}>
      <Box display="flex" flexDirection="column" marginLeft="146px">
        <StyledField
          label={t('Scenario Name')}
          name="name"
          type="text"
          component={ReduxTextField}
          variant="outlined"
        />
        <StyledField
          label={t('Scenario Description (Optional)')}
          name="description"
          type="text"
          component={ReduxTextField}
          variant="outlined"
          multiline
          rows="6"
        />
        {!isEdit && (
          <CreateButton type="submit" disabled={!valid}>
            {t('CREATE SCENARIO')}
          </CreateButton>
        )}
      </Box>
    </Form>
  );
};

ScenarioSubmitComponent.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  isEdit: PropTypes.bool.isRequired,
  valid: PropTypes.bool.isRequired,
};

export const ScenarioSubmitCard = reduxForm({
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
  form: 'CreateScenarioForm',
  validate,
})(ScenarioSubmitComponent);
