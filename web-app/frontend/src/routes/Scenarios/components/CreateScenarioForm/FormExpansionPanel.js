import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {
  Box,
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
} from '@material-ui/core';
import { CheckCircle } from '@material-ui/icons';
import { colors } from '@utils/uiTheme';
import { ColorShape } from '@common/components/ColorShape/ColorShape';

const StyledExpansionPanel = styled(ExpansionPanel)`
  && {
    margin-bottom: 20px;
    border: 1px solid #e2e2e2;
    border-radius: 4px;

    &::before {
      background: none;
    }
  }
`;

const StyledExpansionPanelSummary = styled(ExpansionPanelSummary)`
  && {
    margin: 10px 20px;

    & .MuiExpansionPanelSummary-content {
      display: flex;
      flex-direction: column;
    }
  }
`;

const StyledExpansionPanelDetails = styled(({
  isEdit: _isEdit,
  ...props
}) => <ExpansionPanelDetails {...props} />)`
  margin-left: ${props => (props.isEdit ? 40 : 0)}px;
  padding: 8px 24px 24px !important;
`;

const StyledTitleText = styled.h5`
  margin: 0;
  color: ${props => (props.isExpanded ? colors.darkGrey : colors.black)};
  font-size: ${props => (props.isExpanded ? 15 : 18)}px;
`;

const StyledSubtitle = styled.h5`
  display: ${props => (props.isExpanded ? 'block' : 'none')};
  margin: 10px 0;
  font-size: 25px;
`;

const StyledTitleContainer = styled.div`
  width: 100%;
`;

const StyledCheckCircle = styled(CheckCircle)`
  && {
    fill: ${colors.lightGreen};
  }
`;

export const FormExpansionPanel = ({
  formComponent,
  fullWidth,
  index,
  isDone,
  isEdit,
  isExpanded,
  isValid,
  onChange,
  subtitle,
  title,
  valueComponent,
}) => {
  const handleChange = (expanded) => {
    onChange(index, expanded);
  };

  const displayValidCircle = isValid && isDone && !isExpanded && !isEdit;

  return (
    <StyledExpansionPanel
      elevation={0}
      expanded={isExpanded}
      onChange={(event, expanded) => handleChange(expanded)}
    >
      <StyledExpansionPanelSummary
        aria-controls="panel1bh-content"
        id="panel1bh-header"
      >
        <Box display="flex">
          <ColorShape
            shape="largeRectangleShape"
            color={colors.indicationColors[index]}
          />
          <StyledTitleContainer isExpanded={isExpanded}>
            <StyledTitleText isExpanded={isExpanded}>{ title }</StyledTitleText>
            <StyledSubtitle isExpanded={isExpanded}>{ subtitle }</StyledSubtitle>
          </StyledTitleContainer>
          { displayValidCircle && <StyledCheckCircle /> }
        </Box>
        <Box>
          { !isExpanded && valueComponent }
        </Box>
      </StyledExpansionPanelSummary>
      <StyledExpansionPanelDetails isEdit={isEdit}>
        <Box width={fullWidth ? '100%' : '70%'}>
          { isExpanded && formComponent }
        </Box>
      </StyledExpansionPanelDetails>
    </StyledExpansionPanel>
  );
};

FormExpansionPanel.propTypes = ({
  formComponent: PropTypes.node.isRequired,
  fullWidth: PropTypes.bool,
  index: PropTypes.number.isRequired,
  isDone: PropTypes.bool,
  isEdit: PropTypes.bool.isRequired,
  isExpanded: PropTypes.bool.isRequired,
  isValid: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
  subtitle: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  valueComponent: PropTypes.node,
});

FormExpansionPanel.defaultProps = {
  fullWidth: false,
  isDone: false,
  valueComponent: null,
};
