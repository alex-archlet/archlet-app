import React from 'react';
import { PropTypes } from 'prop-types';
import styled from 'styled-components';
import {
  Box,
  Card,
  Grid,
} from '@material-ui/core';
import { Close } from '@material-ui/icons';
import { Text } from '@common/components/Text/Text';
import { colors } from '@utils/uiTheme';
import { useCustomTranslation } from '@utils/translate';

const StyledGridContainer = styled(Grid)`
  && {
    align-items: center;
    border-bottom: 1px solid ${colors.grey};
    padding: 20px;

    &:hover {
      background-color: #fafafa;
    }
  }
`;

const StyledCloseIcon = styled(Close)`
  && {
    visibility: hidden;
    width: 25px;
    fill: ${colors.iconGrey};
    cursor: pointer;

    ${StyledGridContainer}:hover & {
      visibility: visible;
    }
  }
`;

const StyledCard = styled(({
  alignTop: _alignTop,
  ...props
}) => <Card {...props} />)`
  && {
    overflow: auto;
    display: flex;
    flex-direction: column;
    justify-content: ${props => (props.alignTop ? 'flex-start' : 'center')};
    height: 262px;
    box-shadow: none;
    font-size: 17px;
  }
`;

export const MaxSpendList = ({
  maxSpend,
  onRemoveElement,
  displayOnly,
}) => {
  const { t } = useCustomTranslation();

  return (
    <Box
      mb={20}
      mt={20}
      elevation={0}
    >
      <StyledCard alignTop={!!maxSpend.length}>
        { maxSpend.length ?
          maxSpend.map((item, index) => {
            const keys = Object.keys(item);

            return (
              <StyledGridContainer
                container
                key={keys}
              >
                {keys.map(key => (
                  <Grid
                    item
                    xs={3}
                  >
                    {item[key].value}
                  </Grid>
                )) }
                <Grid
                  item
                  xs={2}
                >
                  { !displayOnly && (
                  <Box
                    display="flex"
                    justifyContent="flex-end"
                  >
                    <StyledCloseIcon onClick={() => onRemoveElement(index)} />
                  </Box>
                  )}
                </Grid>
              </StyledGridContainer>
            );
          }) : (
            <Box width="100%">
              <Text
                fontSize={20}
                color={colors.iconGrey}
                textAlign="center"
              >
                {t('Add your constraints with the dropdown above.')}
              </Text>
            </Box>
          )}
      </StyledCard>
    </Box>
  );
};

MaxSpendList.propTypes = ({
  displayOnly: PropTypes.bool,
  maxSpend: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  onRemoveElement: PropTypes.func,
});

MaxSpendList.defaultProps = ({
  displayOnly: false,
  onRemoveElement: null,
});
