import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { useCustomTranslation } from '@utils/translate';
import { Button } from '@common/components/Button/Button';

const StyledContinueButton = styled(Button)`
  && {
    margin-top: 35px;
    padding: 12px 30px;
  }
`;

export const ContinueButton = ({ disabled }) => {
  const { t } = useCustomTranslation();

  return (
    <StyledContinueButton type="submit" color="secondary" disabled={disabled}>
      {t('Continue')}
    </StyledContinueButton>
  );
};

ContinueButton.propTypes = {
  disabled: PropTypes.bool,
};

ContinueButton.defaultProps = {
  disabled: false,
};
