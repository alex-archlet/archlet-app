import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Box, Card, Checkbox, Grid } from '@material-ui/core';
import { connect, useSelector } from 'react-redux';

import { projectsSelectors } from '@selectors/projects';
import {
  stringToDropdownOption,
  nameIdDropdownOption,
} from '@common/components/Filter/filterUtils';
import { useCustomTranslation } from '@utils/translate';
import { colors } from '@utils/uiTheme';
import {
  FilterDropdown,
  TYPE,
} from '@common/components/Filter/FilterDropdown/FilterDropdown';
import { projectScenariosSelectors } from '@store/selectors/projectScenarios';
import { biddingRoundsSelectors } from '@store/selectors/biddingRounds';

const StyledGridContainer = styled(Grid)`
  && {
    align-items: center;
    border-bottom: 1px solid ${colors.grey};
    padding: 20px;

    &:hover {
      background-color: #fafafa;
    }
  }
`;

const StyledSubTitleText = styled.h5`
  margin: 0;
  color: ${colors.darkGrey};
  font-size: 15 px;
`;

const incumbentSupplier = [
  {
    label: 'Standard Scenario',
    value: 'custom',
  },
  {
    label: 'General Incumbency Scenario',
    value: 'general',
  },
  {
    label: 'Item-Level Incumbency Scenario',
    value: 'item_level',
  },
  {
    label: 'Baseline Scenario',
    value: 'baseline',
  },
];

const StyledCard = styled(({ alignTop: _alignTop, ...props }) => (
  <Card {...props} />
))`
  && {
    overflow: auto;
    display: flex;
    flex-direction: column;
    justify-content: ${props => (props.alignTop ? 'flex-start' : 'center')};
    height: 400px;
    width: 100%;
    box-shadow: none;
    font-size: 17px;
    margin-bottom: 20px;
  }
`;

const GeneralSettingsComponent = ({
  customScenarioConstraints,
  projectCapacities,
  projectComponents,
  projectItemType,
  projectVolumes,
  setCustomScenarioConstraints,
}) => {
  const { t } = useCustomTranslation();

  const scenarios = useSelector(projectScenariosSelectors.getScenarios);
  const rounds = useSelector(
    biddingRoundsSelectors.getBiddingRoundListOrderByCreatedAt
  );

  const scenarioSettingsStep1 = [
    {
      clearable: false,
      code: 'incumbent',
      label: 'Choose...',
      options: incumbentSupplier,
      subTitle: t(
        // eslint-disable-next-line max-len
        'Decide if you want to create a baseline, incumbency or standard scenario. If you choose a baseline scenario, no further constraints can be set.'
      ),
      title: t('Scenario Type'),
      type: TYPE.SINGLE,
    },
    {
      clearable: false,
      code: 'round',
      label: t('Choose ...'),
      options: rounds.map(nameIdDropdownOption),
      subTitle: t('Choose the bidding round for which scenario is calculated'),
      title: t('Bidding Round'),
      type: TYPE.SINGLE,
    },
    {
      clearable: true,
      code: 'volume',
      label: t('Choose ...'),
      options: projectVolumes.map(stringToDropdownOption),
      subTitle: t('Select the volume to be considered for this scenario'),
      title: t('Volume'),
      type: TYPE.MULTI,
    },
    {
      clearable: true,
      code: 'itemType',
      label: t('Choose ...'),
      options: projectItemType.map(stringToDropdownOption),
      subTitle: t('Select the item type to be considered for this scenario'),
      title: t('Item Type'),
      type: TYPE.MULTI,
    },
    {
      code: 'rebates',
      subTitle: t('Consider the available rebates'),
      title: t('Rebates'),
      type: 'Checkbox',
    },
    {
      clearable: true,
      code: 'capacities',
      label: t('Choose...'),
      options: projectCapacities.map(stringToDropdownOption),
      subTitle: t('Select the capacities to be considered for this scenario'),
      title: t('Capacities'),
      type: TYPE.MULTI,
    },
    {
      code: 'splitItems',
      subTitle: t('Consider more than one winning supplier per item'),
      title: t('Supplier per Item'),
      type: 'Checkbox',
    },
    {
      code: 'partialAllocation',
      subTitle: t('Allow a solution that does not allocate all items'),
      title: t('Partial Allocation'),
      type: 'Checkbox',
    },
    {
      clearable: false,
      code: 'priceComponent',
      label: t('Choose ...'),
      options: projectComponents.map(stringToDropdownOption),
      subTitle: t(
        'Select the price component to be considered for this scenario'
      ),
      title: t('Price Component'),
      type: TYPE.SINGLE,
    },
    {
      clearable: true,
      code: 'scenario',
      label: t('Choose ...'),
      options: scenarios.map(nameIdDropdownOption),
      subTitle: t('Calculate success KPI compared to following scenario'),
      title: t('Compare Scenario'),
      type: TYPE.SINGLE,
    },
  ];

  const onFilterChange = (filterId, value, deleteValue) => {
    const newValue = { ...customScenarioConstraints };

    if (deleteValue) {
      delete newValue[filterId];
    } else if (Array.isArray(value)) {
      newValue[filterId] = value;
    } else {
      newValue[filterId] = {
        ...value,
      };
    }

    setCustomScenarioConstraints(newValue);
  };

  const onChange = (value, key) => {
    setCustomScenarioConstraints({
      ...customScenarioConstraints,
      [key]: value,
    });
  };

  return (
    <StyledCard alignTop={1}>
      {scenarioSettingsStep1.map(
        scenarioSetting =>
          (scenarioSetting.type === 'Checkbox' ||
            scenarioSetting.options.length > 1) && (
            <StyledGridContainer>
              <Box
                display="flex"
                justifyContent="space-between"
                alignItem="center"
              >
                <Box>
                  {scenarioSetting.title}
                  <Grid>
                    <StyledSubTitleText>
                      {scenarioSetting.subTitle}
                    </StyledSubTitleText>
                  </Grid>
                </Box>
                {scenarioSetting.type === 'Checkbox' ? (
                  <>
                    <Checkbox
                      inputProps={{ 'aria-label': 'uncontrolled-checkbox' }}
                      checked={customScenarioConstraints[scenarioSetting.code]}
                      onChange={event =>
                        onChange(event.target.checked, scenarioSetting.code)
                      }
                    />
                  </>
                ) : (
                  <FilterDropdown
                    key={scenarioSetting.code}
                    label={scenarioSetting.label}
                    options={scenarioSetting.options}
                    type={scenarioSetting.type}
                    values={customScenarioConstraints[scenarioSetting.code]}
                    onChange={(value, deleteValue) =>
                      onFilterChange(scenarioSetting.code, value, deleteValue)
                    }
                    clearable={scenarioSetting.clearable}
                  />
                )}
              </Box>
            </StyledGridContainer>
          )
      )}
    </StyledCard>
  );
};

GeneralSettingsComponent.propTypes = {
  customScenarioConstraints: PropTypes.objectOf(PropTypes.array).isRequired,
  projectCapacities: PropTypes.arrayOf(PropTypes.string).isRequired,
  projectComponents: PropTypes.arrayOf(PropTypes.string).isRequired,
  projectItemType: PropTypes.arrayOf(PropTypes.string).isRequired,
  projectVolumes: PropTypes.arrayOf(PropTypes.string).isRequired,
  setCustomScenarioConstraints: PropTypes.func.isRequired,
};
const mapStateToProps = state => ({
  projectCapacities: projectsSelectors.getProjectCapacities(state),
  projectComponents: projectsSelectors.getProjectPriceComponents(state),
  projectItemType: projectsSelectors.getProjectItemTypes(state),
  projectVolumes: projectsSelectors.getProjectVolumes(state),
});

export const GeneralSettingsCard = connect(mapStateToProps)(
  GeneralSettingsComponent
);
