import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { jobsSelectors } from '@store/selectors/jobs';

export const useBlockingJobEnd = (callback: Function) => {
  const progressVisible = useSelector(jobsSelectors.getIsGlobalJobRunning);

  useEffect(() => {
    if (!progressVisible && callback) {
      callback();
    }
  }, [progressVisible]);
};
