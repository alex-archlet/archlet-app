import React from 'react';
import { colors } from '@utils/uiTheme';
import { Flex } from '@common/components/Flex/Flex';
import {
  Bar,
  BarChart,
  CartesianGrid,
  ResponsiveContainer,
  XAxis,
  YAxis,
  Label,
  Tooltip,
} from 'recharts';
import { useSelector } from 'react-redux';
import { LocaleFormatNumber } from '@common/components/FormatNumber/FormatNumber';
import { formatNumberWithLetters } from '@utils/formatNumber';
import { CustomTooltip } from '@common/components/Graph/Legend/CustomTooltip';
import { sumBy } from '@utils/array';
import { projectsSelectors } from '@store/selectors/projects';
import { useCustomTranslation } from '@utils/translate';
import { truncate } from '@utils/stringFormat';

import { ScenarioWithAllocations } from '../useAllocations';

interface Props {
  scenarios: ScenarioWithAllocations[];
}

const GRAPH_LINE_COLOR = '#f1f3f6';

export const SpendingGraph = ({ scenarios }: Props) => {
  const { t } = useCustomTranslation();
  const projectCurrency = useSelector(projectsSelectors.getProjectCurrencyCode);

  if (!scenarios.length) {
    return null;
  }

  const UNIT_PRICE_SUB_TEXT = `${t('Total Spend')} (${projectCurrency})`;

  const data = scenarios.map(({ allocations, ...rest }) => ({
    ...rest,
    totalSpend: sumBy(allocations, 'totalPrice'),
  }));

  return (
    <Flex direction="column">
      <ResponsiveContainer width="100%" height={300}>
        <BarChart
          data={data}
          maxBarSize={27}
          margin={{
            bottom: 0,
            left: 40,
            right: 5,
            top: 30,
          }}
        >
          <CartesianGrid vertical={false} stroke={GRAPH_LINE_COLOR} />
          <XAxis
            dataKey="name"
            tickLine={false}
            axisLine={false}
            tickFormatter={val => truncate(val)}
          />
          {/* @ts-ignore */}
          <YAxis
            tickFormatter={val => formatNumberWithLetters(val, { decimals: 1 })}
            axisLine={false}
            tickLine={false}
          >
            <Label
              value={UNIT_PRICE_SUB_TEXT}
              // @ts-ignore
              angle={-90}
              dy={-20}
              position="left"
              style={{
                textAnchor: 'middle',
              }}
            />
          </YAxis>
          <Bar
            dataKey="totalSpend"
            fill={colors.graphs.graphComparison[0]}
            stroke={colors.graphs.graphComparison[0]}
          />
          <Tooltip
            // @ts-ignore
            cursor={{ fill: 'transparent' }}
            content={({ payload }) => {
              if (!payload || !payload.length) {
                return null;
              }

              const items = payload.map(
                // @ts-ignore
                ({ value, payload: barPayload, color }) => ({
                  label: barPayload.name,
                  color,
                  value: <LocaleFormatNumber value={value as number} />,
                })
              );

              return <CustomTooltip items={items} />;
            }}
          />
        </BarChart>
      </ResponsiveContainer>
    </Flex>
  );
};
