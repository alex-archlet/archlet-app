import React from 'react';
import styled from 'styled-components';
import { ExpansionPanelDetails } from '@material-ui/core';
import { colors } from '@utils/uiTheme';
import { Flex } from '@common/components/Flex/Flex';
import { Text } from '@common/components/Text/Text';
import { LocaleFormatNumber } from '@common/components/FormatNumber/FormatNumber';
import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';

import { ScenarioActions } from './ScenarioActions';
import { Scenario } from '../useAllocations';
import { CustomKpiColumn } from './ScenarioListElement';

const StyledExpansionPanelDetails = styled(ExpansionPanelDetails)`
  && {
    border-top: 1px solid ${colors.grey};
    padding: 30px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
  }
`;

const StyledStat = styled.div`
  display: flex;
  flex-basis: 30%;
  align-items: flex-start;
  justify-content: center;
  flex-direction: column;
  width: 200px;
  margin-bottom: 10px;
`;

interface Props {
  scenario: Scenario;
  customColumns: CustomKpiColumn[];
}

const Stat = ({ label, value }) => (
  <StyledStat>
    <Text fontWeight={400}>{label}</Text>
    <VerticalSpacer height={8} />
    <Text fontWeight={400} fontSize={12} color="#7f7f7f">
      <LocaleFormatNumber value={value} />
    </Text>
  </StyledStat>
);

export const ScenarioListItemDetails: React.FC<Props> = ({
  scenario,
  customColumns,
}) => {
  return (
    <StyledExpansionPanelDetails>
      <Flex wrap="wrap">
        {customColumns.map((column) => {
          return (
            <Stat label={column.label} value={column.getValue(scenario)} />
          );
        })}
      </Flex>
      <ScenarioActions scenario={scenario} />
    </StyledExpansionPanelDetails>
  );
};
