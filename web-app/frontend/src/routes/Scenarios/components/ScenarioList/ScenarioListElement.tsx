import React, { useState } from 'react';
import {
  ExpansionPanel,
  ExpansionPanelSummary,
  GridJustification,
  Grid,
} from '@material-ui/core';
import { useHistory } from 'react-router';
import { useParams } from 'react-router-dom';
import styled from 'styled-components';

import { StyledGrid } from './styles';
import { ScenarioListItemDetails } from './ScenarioListItemDetails';
import { Scenario } from '../useAllocations';

const StyledExpansionPanel = styled(ExpansionPanel)`
  && {
    padding-top: 24px;
    margin: 16px 0;
    border: 1px solid #e2e2e2;
    border-radius: 3px;
    &:hover {
      box-shadow: 0 2px 10px 0 rgba(10, 22, 70, 0.1);
      cursor: pointer;
    }
  }
`;

const StyledExpansionPanelSummary = styled(ExpansionPanelSummary)`
  && {
    margin: 0;
    padding: 0 30px 24px;

    & .MuiExpansionPanelSummary-content {
      margin: 0;
    }

    & .MuiExpansionPanelSummary-root {
      padding: 0;
    }
  }
`;

interface Props {
  scenario: Scenario;
  columns: Column[];
  customColumns: CustomKpiColumn[];
}

export interface Column {
  key: string;
  label: string;
  render: (scenario: Scenario | null, onExpand: () => void) => React.ReactNode;
  width:
    | boolean
    | 'auto'
    | 1
    | 2
    | 3
    | 4
    | 5
    | 6
    | 7
    | 8
    | 9
    | 10
    | 11
    | 12
    | undefined;
  accessor?: (row: Scenario) => number;
  justify?: GridJustification;
  getValue?: (row: Scenario) => number | string;
}
export interface CustomKpiColumn extends Column {
  getValue: (row: Scenario) => number | string;
}

export const ScenarioListElement: React.FC<Props> = ({
  scenario,
  columns,
  customColumns,
}) => {
  const [isExpanded, setIsExpanded] = useState(false);
  const { id: projectId } = useParams<{ id: string }>();
  const history = useHistory();

  const onOpenScenario = () => {
    history.push(`/projects/${projectId}/scenariosDetails/${scenario.id}`);
  };

  return (
    <StyledExpansionPanel
      expanded={isExpanded}
      onClick={onOpenScenario}
      elevation={0}
    >
      <StyledExpansionPanelSummary>
        <Grid container spacing={5}>
          {columns.map(({ render, width, key }) => {
            return (
              <StyledGrid item xs={width} key={key}>
                {render(scenario, () => setIsExpanded(!isExpanded))}
              </StyledGrid>
            );
          })}
        </Grid>
      </StyledExpansionPanelSummary>
      <ScenarioListItemDetails
        scenario={scenario}
        customColumns={customColumns}
      />
    </StyledExpansionPanel>
  );
};
