import React, { useState, useMemo } from 'react';
import styled from 'styled-components';
import { Grid, TableSortLabel } from '@material-ui/core';
import { projectScenariosSelectors } from '@store/selectors/projectScenarios';
import { projectsSelectors } from '@store/selectors/projects';
import { useSelector } from 'react-redux';
import { TFunction } from 'i18next';
import { formatNumber } from '@utils/formatNumber';
import { useCustomTranslation } from '@utils/translate';
import { Scenario } from '../useAllocations';

import {
  Actions,
  ScenarioName,
  KpiStatValue,
  ItemsAllocated,
} from './ScenarioListElements';
import { ScenarioListElement, Column } from './ScenarioListElement';
import { StyledGrid } from './styles';

const StyledTableHeader = styled.div`
  margin: 24px 24px 0;
  color: #6d7278;
`;

const StyledTableSortLabel = styled(TableSortLabel)`
  color: #6d7278;
  text-align: center !important;
`;

type Options = 'asc' | 'desc' | undefined;

interface Props {
  scenarios: Scenario[];
}
export const translateKpiName = (name: string, t: TFunction) => {
  switch (name) {
    case 'total_spend':
      return t('Total Spend');
    case 'savings':
      return t('Savings');
    case 'volume':
      return t('Volume');
    case 'number_of_changes':
      return t('Number Of Changes');
    case 'items_allocated':
      return t('Items Allocated');
    case 'winning_bidders':
      return t('Winning Bidders');
    default:
      return name;
  }
};
const defaultColumns = [
  'total_spend',
  'savings',
  'winning_bidders',
  'items_allocated',
];

export const ScenarioListTable = ({ scenarios }: Props) => {
  const { t } = useCustomTranslation();
  const scenarioKpi = useSelector(projectScenariosSelectors.getScenarioKpiList);
  const scenarioDefKpiList = useSelector(projectsSelectors.getProjectKpiDefs);

  const defaultKpiColumns = scenarioDefKpiList.filter(({ name }) =>
    defaultColumns.includes(name)
  );
  const customKpiColumns = scenarioDefKpiList.filter(
    ({ name }) => !defaultColumns.includes(name)
  );
  const KpiColumnsWithoutItemAllocation = defaultKpiColumns.filter(
    ({ name }) => name !== 'items_allocated'
  );
  const itemAllocationColumn = scenarioDefKpiList.filter(
    ({ name }) => name === 'items_allocated'
  );

  const defaultKpiColumnsOrdered = [
    ...KpiColumnsWithoutItemAllocation,
    ...itemAllocationColumn,
  ];
  const [order, setOrder] = useState<Options>('desc');

  const [orderBy, setOrderBy] = useState('updatedAt');

  const handleRequestSort = (property) => {
    const isDesc = orderBy === property && order === 'desc';
    const newOrder: Options = isDesc ? 'asc' : 'desc';

    setOrder(newOrder);
    setOrderBy(property);
  };
  const defColumns: Column[] = defaultKpiColumnsOrdered.map((scenarioDefKpi) => {
    const getKpiValue = scenario =>
      scenario &&
      scenarioKpi[scenario.id] &&
      scenarioKpi[scenario.id][scenarioDefKpi.id];

    if (scenarioDefKpi.name === 'items_allocated') {
      return {
        key: scenarioDefKpi.id,
        label: translateKpiName(scenarioDefKpi.name, t),
        render: scenario => (
          <ItemsAllocated allocationPercentage={getKpiValue(scenario) * 100} />
        ),
        getValue: scenario =>
          formatNumber(getKpiValue(scenario) * 100, { type: 'percent' }),
        accessor: getKpiValue,
        width: 1,
        justify: 'space-evenly',
      };
    }

    return {
      key: scenarioDefKpi.id,
      label: translateKpiName(scenarioDefKpi.name, t),
      render: scenario => <KpiStatValue value={getKpiValue(scenario)} />,
      getValue: scenario => getKpiValue(scenario),
      accessor: getKpiValue,
      width: 2,
      justify: 'space-evenly',
    };
  });

  const customExpansionColumns = customKpiColumns.map((scenarioDefKpi) => {
    const getKpiValue = scenario =>
      scenario &&
      scenarioKpi[scenario.id] &&
      scenarioKpi[scenario.id][scenarioDefKpi.id];

    return {
      key: scenarioDefKpi.id,
      label: translateKpiName(scenarioDefKpi.name, t),
      render: scenario => <KpiStatValue value={getKpiValue(scenario)} />,
      getValue: scenario => getKpiValue(scenario),
      accessor: getKpiValue,
      width: 1,
      justify: 'space-evenly',
    };
  });

  const columns: Column[] = [
    {
      justify: 'flex-start',
      key: 'updatedAt',
      label: t('Scenario'),
      render: scenario => scenario && <ScenarioName scenario={scenario} />,
      width: 3,
    },
    ...defColumns,
    {
      key: 'actions',
      label: '',
      render: (_, onExpand) => <Actions onExpand={onExpand} />,
      width: 2,
    },
  ];

  const sortingColumn = useMemo(
    () => columns.find(({ key }) => key === orderBy) || columns[0],
    [columns, orderBy]
  );

  const sortFunction = (itemA, itemB) => {
    const column = orderBy;
    const multiplier = order === 'asc' ? 1 : -1;

    const { accessor } = sortingColumn;

    const valueA = accessor ? accessor(itemA) : itemA[column];
    const valueB = accessor ? accessor(itemB) : itemB[column];

    if (valueA < valueB) return -1 * multiplier;

    if (valueA > valueB) return 1 * multiplier;

    return 0;
  };

  return (
    <>
      <StyledTableHeader>
        <Grid container>
          {columns.map(({ justify, width, label, key }) => (
            <StyledGrid justify={justify} xs={width} key={key} item>
              <>
                <StyledTableSortLabel
                  active={orderBy === key}
                  direction={order}
                  hideSortIcon
                  onClick={() => handleRequestSort(key)}
                >
                  {label}
                </StyledTableSortLabel>
              </>
            </StyledGrid>
          ))}
        </Grid>
      </StyledTableHeader>
      {scenarios.sort(sortFunction).map(scenario => (
        <ScenarioListElement
          scenario={scenario}
          columns={columns}
          customColumns={customExpansionColumns}
        />
      ))}
    </>
  );
};
