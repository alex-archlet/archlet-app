import React, { useState, MouseEvent } from 'react';
import { Flex } from '@common/components/Flex/Flex';
import { Button } from '@common/components/Button/Button';
import { CancelButton } from '@common/components/Button/CancelButton';
import { DeleteButton } from '@common/components/Button/DeleteButton';
import styled from 'styled-components';
import { ConfirmationDialog } from '@common/components/ConfirmationDialog/ConfirmationDialog';


import { useCustomTranslation } from '@utils/translate';

import { useDispatch, useSelector } from 'react-redux';
import {
  deleteScenario,
  duplicateScenario,
  getScenarios,
} from '@store/actions/projectScenarios';
import { useParams } from 'react-router-dom';
import { useHistory } from 'react-router';

import { userSelectors } from '@selectors/user';
import { Scenario } from '../useAllocations';

const StyledButton = styled(Button)`
  && {
    margin-left: 10px;
    margin-right: 10px;
    text-transform: none;
  }
`;

interface Props {
  scenario: Scenario;
}

export const ScenarioActions = ({ scenario }: Props) => {
  const { t } = useCustomTranslation();
  const { id: projectId } = useParams();
  const dispatch = useDispatch();
  const history = useHistory();
  const permissions = useSelector(userSelectors.getUserPermissions);

  const [isDeleting, setIsDeleting] = useState(false);

  const onDuplicate = (e: React.MouseEvent<HTMLButtonElement, globalThis.MouseEvent>) => {
    e.stopPropagation();
    e.preventDefault();

    // @ts-ignore dispatch().then is not typed correctly
    dispatch(duplicateScenario(scenario.id)).then((duplicatedScenario) => {
      history.push(
        `/projects/${projectId}/scenariosDetails/${duplicatedScenario.id}`
      );
    });
  };

  const onDeleteScenario = async (e: Event) => {
    e.preventDefault();
    e.stopPropagation();
    await dispatch(deleteScenario(scenario.id));
    await dispatch(getScenarios(projectId));
  };

  const onClickDelete = (event: MouseEvent) => {
    event.preventDefault();
    event.stopPropagation();
    setIsDeleting(true);
  };

  const onCloseConfirm = (event: MouseEvent) => {
    event.preventDefault();
    event.stopPropagation();
    setIsDeleting(false);
  };

  return (
    <Flex direction="row" align="center">
      <ConfirmationDialog
        isOpen={isDeleting}
        handleClose={onCloseConfirm}
        ButtonActions={
          <>
            <DeleteButton
              // @ts-ignore
              onClick={(e: MouseEvent<HTMLButtonElement, MouseEvent>) =>
                // @ts-ignore
                onDeleteScenario(e)
              }
              autoFocus
            >
              {t('Delete')}
            </DeleteButton>
            <CancelButton onClick={onCloseConfirm}>{t('No')}</CancelButton>
          </>
        }
        message={t('Do you really want to delete this scenario?')}
      />
      {!scenario.isDefault && (
        <StyledButton
          variant="contained"
          color="secondary"
          onClick={onDuplicate}
          disableElevation
        >
          {t('Duplicate')}
        </StyledButton>
      )}
      {permissions.includes('CAN_DELETE_SCENARIO') && (
        <DeleteButton
          variant="contained"
          onClick={onClickDelete}
          disableElevation
        >
          {t('Delete')}
        </DeleteButton>
      )}
    </Flex>
  );
};
