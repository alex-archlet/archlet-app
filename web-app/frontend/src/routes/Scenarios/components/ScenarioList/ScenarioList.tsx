import React from 'react';
import { Card } from '@common/components/Card/Card';
import { useCustomTranslation } from '@utils/translate';
import { ActionButtons } from './ActionButtons';
import { ScenarioListTable } from './ScenarioListTable';
import { Scenario } from '../useAllocations';

interface Props {
  scenarios: Scenario[];
}

const ScenarioListComp = ({ scenarios }: Props) => {
  const { t } = useCustomTranslation();

  return (
    <Card
      style={{ flex: 1 }}
      title={t('Scenario List')}
      rightSide={<ActionButtons scenarios={scenarios} />}
    >
      <ScenarioListTable scenarios={scenarios} />
    </Card>
  );
};

export const ScenarioList = ScenarioListComp;
