import React, { MouseEvent } from 'react';
import styled from 'styled-components';
import { ExpandMore } from '@material-ui/icons';
import { Card } from '@material-ui/core';
import moment from 'moment';

import { formatNumber } from '@utils/formatNumber';
import { colors } from '@utils/uiTheme';
import { Text } from '@common/components/Text/Text';
import { Flex } from '@common/components/Flex/Flex';
import { useCustomTranslation } from '@utils/translate';
import { PrimaryButton } from '@common/components/Button/PrimaryButton';

import { LocaleFormatNumber } from '@common/components/FormatNumber/FormatNumber';
import { Scenario } from '../useAllocations';

const StyledExpandMore = styled(ExpandMore)`
  && {
    margin-left: 10px;
  }
`;

export const StyledCard = styled(
  ({ isActive: _active, isPlaceholder: _isPlaceholder, ...props }) => (
    <Card {...props} />
  )
)`
  && {
    margin: 12px 0;
    border-color: ${props =>
      props.isActive ? colors.grey : 'rgba(226, 226, 226, 0.4)'};
    padding: 36px 24px;
    color: ${props => (props.isActive ? colors.black : '#999')};
    font-weight: bold;
    font-size: 18px;
    line-height: 21px;

    &:hover {
      box-shadow: 0 1px 5px 0 ${colors.darkGrey};
      border: 1px solid transparent;
      cursor: pointer;
    }
  }
`;

const StyledBadge = styled.div`
  padding: 0 12px;
  border-radius: 4px;
  border: 1px solid ${colors.palette.secondary.light};
  color: ${colors.palette.secondary.light};
  background-color: ${colors.white};
  font-size: 11px;
  line-height: 24px;
  font-weight: 400;
  height: fit-content;
  margin-left: 16px;
  white-space: nowrap;
  &:hover {
    background-color: ${colors.palette.secondary.main};
    color: ${colors.palette.secondary.contrastText};
  }
`;

const StyledButton = styled(PrimaryButton)`
  && {
    display: flex;
    line-height: 1;
    justify-content: center;
    align-items: center;
    width: 138px;
    height: 40px;
    padding: 5px;
    margin-left: 15px;
  }
`;

interface ScenarioNameProps {
  scenario: Scenario;
}

export const ScenarioName: React.FC<ScenarioNameProps> = ({ scenario }) => {
  const { t } = useCustomTranslation();
  const formattedDate = moment(scenario.updatedAt).format('MMMM Do, YYYY - HH:mm');

  return (
    <Flex
      direction="row"
      justify="space-between"
      align="center"
      style={{ width: '100%' }}
    >
      <Flex direction="column">
        <Text fontWeight={500}>{scenario.name}</Text>
        <Text fontSize={12} color="#7f7f7f">
          {t('Updated on', { date: formattedDate })}
        </Text>
      </Flex>
      <StyledBadge>Round 1</StyledBadge>
    </Flex>
  );
};

const StyledStat = styled.span`
  font-size: 18px;
  font-weight: 500;
  color: black;
  align-self: center;
`;

export const KpiStatValue = ({ value }) => {
  if (value === undefined) {
    return <StyledStat>-</StyledStat>;
  }
  return (
    <StyledStat>
      <LocaleFormatNumber value={value} />
    </StyledStat>
  );
};

const getAllocationsPercentageColor = (value: number) => {
  if (value > 99.9) return colors.valid;

  if (value > 95) return colors.warning;

  return colors.error;
};

const StyledPercentage = styled.div.attrs(({ color }) => ({
  color,
}))`
  font-size: 16px;
  white-space: pre;
`;

const StyledProgressBarWrapper = styled.div`
  overflow: hidden;
  border-radius: 4px;
  height: 8px;
  width: 32px;
  margin-left: 8px;
  background-color: #d2d2d2;
`;

const StyledProgressBar = styled.div.attrs(({ color }) => ({
  style: {
    backgroundColor: color,
  },
}))`
  width: ${({ percentage }: { percentage: number }) => percentage};
  height: 100%;
`;
interface ItemsAllocatedProps {
  allocationPercentage: number;
}

export const ItemsAllocated: React.FC<ItemsAllocatedProps> = ({
  allocationPercentage,
}) => {
  const color = getAllocationsPercentageColor(allocationPercentage);
  const formatPercentage = formatNumber(allocationPercentage, {
    type: 'percent',
  });

  return (
    <Flex direction="row" align="center" style={{ color }}>
      <StyledPercentage>{`${formatPercentage}`}</StyledPercentage>
      <StyledProgressBarWrapper>
        <StyledProgressBar color={color} percentage={allocationPercentage} />
      </StyledProgressBarWrapper>
    </Flex>
  );
};

export const Actions = ({ onExpand }) => {
  const { t } = useCustomTranslation();

  const onClickExpand = (e: MouseEvent<SVGSVGElement, MouseEvent>) => {
    e.stopPropagation();
    onExpand();
  };

  return (
    <Flex
      direction="row"
      align="center"
      justify="space-evenly"
      style={{ width: '100%' }}
    >
      <StyledButton>{t('Deep Dive')}</StyledButton>
      {/* @ts-ignore */}
      <StyledExpandMore onClick={onClickExpand} />
    </Flex>
  );
};
