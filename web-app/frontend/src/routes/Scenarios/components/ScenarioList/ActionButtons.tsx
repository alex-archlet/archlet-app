import React from 'react';

import { Flex } from '@common/components/Flex/Flex';
import { Tooltip } from '@common/components/Tooltip/Tooltip';
import { useCustomTranslation } from '@utils/translate';
import { useHistory, useParams } from 'react-router';
import { useDispatch } from 'react-redux';
import { runOptimizationForScenarios } from '@store/actions/projectScenarios';
import { SecondaryButton } from '@common/components/Button/SecondaryButton';
import styled from 'styled-components';
import { Scenario } from '../useAllocations';

const StyledButton = styled(SecondaryButton)`
  && {
    margin-right: 8px;
  }
`;

interface Props {
  scenarios: Scenario[];
}

export const ActionButtons = ({ scenarios }: Props) => {
  const { t } = useCustomTranslation();
  const { id: projectId } = useParams();
  const dispatch = useDispatch();
  const history = useHistory();

  const onRunOptimization = () => {
    const scenarioIds = scenarios.map(({ id }) => id);

    dispatch(runOptimizationForScenarios(projectId, scenarioIds));
  };

  const onAddScernario = () => {
    history.push(`/projects/${projectId}/scenarios/new`);
  };

  return (
    <Flex direction="row" justify="space-between">
      <Tooltip
        title={t('Press this button if you want to run all your scenarios')}
      >
        <StyledButton onClick={onRunOptimization}>
          {t('Run Optimization')}
        </StyledButton>
      </Tooltip>
      <Tooltip title={t('Press this button if you want to add a scenario')}>
        <SecondaryButton onClick={onAddScernario}>
          {t('Add Scenario')}
        </SecondaryButton>
      </Tooltip>
    </Flex>
  );
};
