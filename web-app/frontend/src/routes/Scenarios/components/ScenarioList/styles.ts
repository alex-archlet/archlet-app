import { Grid, GridProps } from '@material-ui/core';
import styled from 'styled-components';

export const StyledGrid = styled(Grid)`
  display: flex;
  align-items: center;
  justify-content: ${(props: GridProps) =>
    props.justify ? props.justify : 'center'} !important;
`;
