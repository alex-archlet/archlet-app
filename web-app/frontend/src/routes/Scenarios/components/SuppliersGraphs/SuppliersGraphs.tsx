import React, { useEffect, useState } from 'react';

import { Flex } from '@common/components/Flex/Flex';
import ScenarioPicker from './ScenarioPicker';
import { SuppliersGraph } from './SuppliersGraph';
import { ScenarioWithAllocations } from '../useAllocations';

interface Props {
  scenarios: ScenarioWithAllocations[];
}

export const SuppliersGraphs = ({ scenarios }: Props) => {
  const [currentScenarioId, setCurrentScenarioId] = useState<string | null>(
    null
  );
  const currentScenario = scenarios.find(({ id }) => id === currentScenarioId);

  useEffect(() => {
    if (currentScenario || !scenarios.length) return;

    setCurrentScenarioId(scenarios[0].id);
  }, [scenarios, currentScenario]);

  const scenarioIds = scenarios.map(({ id }) => id);

  if (!currentScenario || !currentScenarioId) return null;

  return (
    <Flex direction="column">
      <ScenarioPicker
        currentScenarioId={currentScenarioId}
        scenarioIds={scenarioIds}
        scenarioName={currentScenario.name}
        setCurrentScenarioId={setCurrentScenarioId}
      />
      <SuppliersGraph scenario={currentScenario} />
    </Flex>
  );
};
