import React, { memo } from 'react';
import groupBy from 'lodash.groupby';

import { sumBy } from '@utils/array';
import { useCustomTranslation } from '@utils/translate';
import {
  Cell,
  Legend,
  Pie,
  PieChart,
  ResponsiveContainer,
  Tooltip,
} from 'recharts';
import { formatNumber } from '@utils/formatNumber';
import { colors } from '@utils/uiTheme';
import { CustomLegend } from '@common/components/Graph/Legend/CustomLegend';
import { CustomTooltip } from '@common/components/Graph/Legend/CustomTooltip';
import { LocaleFormatNumber } from '@common/components/FormatNumber/FormatNumber';

import { ScenarioWithAllocations } from '../useAllocations';

interface Props {
  scenario: ScenarioWithAllocations;
}

const MAX_DISPLAYED_SUPPLIERS = 13;

const COLORS = colors.graphs.graphComparison;

interface LabelProps {
  percent: number;
}


const renderCustomizedLabel = (props: LabelProps): SVGElement => {
  const { percent, ...rest } = props;

  // @ts-ignore
  return (
    <text
      {...rest}
      fontWeight="500"
    >
      {formatNumber(percent * 100, { type: 'percent' })}
    </text>
  );
};

const SuppliersGraphComponent = ({ scenario }: Props) => {
  const { t } = useCustomTranslation();

  const bySupplier = groupBy(scenario.allocations, 'supplier');

  const sortedSupplierList = Object.keys(bySupplier)
    .map((name) => {
      const allocations = bySupplier[name];
      const totalPrice = sumBy(allocations, 'totalPrice');

      return {
        name,
        totalPrice,
      };
    })
    .sort((a, b) => b.totalPrice - a.totalPrice);

  const groupedSuppliers = sortedSupplierList.splice(MAX_DISPLAYED_SUPPLIERS);

  if (groupedSuppliers.length) {
    const groupedElement = {
      name: t('Other suppliers'),
      totalPrice: sumBy(groupedSuppliers, 'totalPrice'),
    };

    sortedSupplierList.push(groupedElement);
  }

  return (
    <ResponsiveContainer width="100%" height={300}>
      <PieChart>
        <Pie
          data={sortedSupplierList}
          dataKey="totalPrice"
          nameKey="name"
          cx="50%"
          cy="50%"
          outerRadius={100}
          innerRadius={50}
          labelLine={false}
          label={renderCustomizedLabel}
          isAnimationActive={false}
        >
          {sortedSupplierList.map((_, index) => (
            <Cell fill={COLORS[index % COLORS.length]} />
          ))}
        </Pie>
        <Tooltip
          // @ts-ignore
          cursor={{ fill: 'transparent' }}
          content={({ payload }) => {
            if (!payload || !payload.length) {
              return null;
            }

            // @ts-ignore
            const items = payload.map(({ value, payload: barPayload }) => ({
              label: barPayload.name,
              color: barPayload.fill,
              value: <LocaleFormatNumber value={value as number} />,
            }));

            return <CustomTooltip items={items} />;
          }}
        />
        <Legend
          verticalAlign="bottom"
          align="left"
          height={36}
          payloadUniqBy
          content={({ payload }) =>
            // @ts-ignore
            payload && <CustomLegend payload={payload} alignment="center" />
          }
        />
      </PieChart>
    </ResponsiveContainer>
  );
};

export const SuppliersGraph = memo(SuppliersGraphComponent);
