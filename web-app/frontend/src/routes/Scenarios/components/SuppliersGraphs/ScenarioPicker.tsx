import React from 'react';
import { Flex } from '@common/components/Flex/Flex';
import styled from 'styled-components';
import { KeyboardArrowLeft, KeyboardArrowRight } from '@material-ui/icons';
import { Text } from '@common/components/Text/Text';

const StyledKeyboardArrowLeft = styled(KeyboardArrowLeft)`
  opacity: 0.75;
  font-size: 15px;
  cursor: pointer;
`;

const StyledKeyboardArrowRight = styled(KeyboardArrowRight)`
  opacity: 0.75;
  font-size: 15px;
  cursor: pointer;
`;

interface Props {
  scenarioIds: string[];
  scenarioName: string;
  currentScenarioId: string;
  setCurrentScenarioId: (id: string) => void;
}

const ScenarioPicker = ({
  scenarioIds,
  scenarioName,
  setCurrentScenarioId,
  currentScenarioId,
}: Props) => {
  const currentIndex = scenarioIds.indexOf(currentScenarioId);
  const canChangeScenario = scenarioIds.length > 1;

  const onLeft = () => {
    if (!canChangeScenario) return;

    if (!currentIndex) {
      setCurrentScenarioId(scenarioIds[scenarioIds.length - 1]);
    } else {
      setCurrentScenarioId(scenarioIds[currentIndex - 1]);
    }
  };
  const onRight = () => {
    if (!canChangeScenario) return;

    if (currentIndex === scenarioIds.length - 1) {
      setCurrentScenarioId(scenarioIds[0]);
    } else {
      setCurrentScenarioId(scenarioIds[currentIndex + 1]);
    }
  };

  return (
    <Flex
      justify="space-between"
      align="center"
    >
      <span>
        {canChangeScenario && <StyledKeyboardArrowLeft onClick={onLeft} />}
      </span>
      <Text textAlign="center" fontWeight={500} fontSize={13} color="#44566C">{scenarioName}</Text>
      <span>
        {canChangeScenario && <StyledKeyboardArrowRight onClick={onRight} />}
      </span>
    </Flex>
  );
};

export default ScenarioPicker;
