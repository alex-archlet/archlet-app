import { useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

import { useCustomTranslation } from '@utils/translate';
import { projectScenariosSelectors } from '@selectors/projectScenarios';
import { TYPE } from '@common/components/Filter/filterUtils';
import { useFilter } from '@common/components/Filter/filterHooks';
import { projectsSelectors } from '@selectors/projects';
import { getScenarios } from '@actions/projectScenarios';
import { getScenarioAllocations } from '@store/actions/projectScenarios';
import { MultiFilterValue } from '@common/components/Filter/types';

const NUMBER_OF_DEFAULT_SCENARIOS = 2;

interface Kpi {
  allocation_changes: number;
  allocation_percentage: number;
  discounts: number;
  incumbent_supplier_spend: number;
  incumbent_suppliers: number;
  new_supplier_spend: number;
  new_suppliers: number;
  num_items: number;
  savings: number;
  spend: number;
  volume: number;
}

interface Optimization {
  output: {
    kpis: {
      Global: Kpi;
    };
  };
}

export interface Scenario {
  id: string;
  name: string;
  updatedAt: string;
  isDefault: boolean;
  optimization?: Optimization;
}

interface Attributes {
  [key: string]: string | number | null;
}

interface Allocation {
  id: string;
  item: string;
  supplier: string;
  volume: number;
  totalPrice: number;
  offerAttributes: Attributes;
  demandAttributes: Attributes;
}

export interface ScenarioWithAllocations extends Scenario {
  allocations: Allocation[];
}

interface ScenarioParams {
  id: string;
  scenarioId: string;
}

export const useAllocations = () => {
  const { t } = useCustomTranslation();
  const { id: projectId, scenarioId } = useParams<ScenarioParams>();
  const dispatch = useDispatch();
  const [filterOptions, setFilterOptions] = useState({});
  const getScenariosActions = () => dispatch(getScenarios(projectId));

  useEffect(() => {
    getScenariosActions();
  }, []);

  const scenarios: Scenario[] = useSelector(
    projectScenariosSelectors.getScenarios
  );

  const allocations: { [key: string]: Allocation[] } = useSelector(
    projectScenariosSelectors.getScenarioAllocations
  );

  const filterBidColumns = useSelector(projectsSelectors.getFilterDemandBidColumns);

  const otherScenarios = useMemo(
    () => scenarios.filter(({ id }) => id !== scenarioId),
    [scenarios, scenarioId]
  );
  const scenarioOptions = useMemo(
    () =>
      otherScenarios.map(({ id, name }) => ({
        label: name,
        value: id,
      })),
    [otherScenarios]
  );

  const projectFilters = filterBidColumns.map(({ name, id }) => ({
    id,
    label: name,
    options: filterOptions[id] || [],
    type: TYPE.MULTI,
  }));

  const filterDeclaration = [
    {
      clearable: true,
      id: 'scenario',
      label: t('Scenarios'),
      options: scenarioOptions,
      text: t('Select'),
      type: TYPE.MULTI,
    },
    ...projectFilters,
  ];

  const { filterProps, filterState, setFilterState } = useFilter({
    filterDeclaration,
  });

  const scenarioObjects = (filterState?.scenario as MultiFilterValue | undefined) ?? [];

  const fetchScenarioData = async () => {
    await getScenariosActions();
    scenarioObjects.forEach(({ value }) => dispatch(getScenarioAllocations(value)));
  };

  useEffect(() => {
    
    if (!scenarioObjects.length) {
      return;
    }

    fetchScenarioData();
  }, [dispatch, scenarioObjects.length]);

  useEffect(() => {
    if (!scenarioOptions.length) return;

    const numberOfDisplayedScenarios = Math.min(
      NUMBER_OF_DEFAULT_SCENARIOS,
      scenarioOptions.length
    );

    setFilterState({
      scenario: scenarioOptions.slice(0, numberOfDisplayedScenarios),
    });
  }, [scenarioOptions.length]);

  const selectedScenarioIds: string[] = scenarioObjects.map(
    ({ value }) => value as string
  );

  const actualFilters = Object.keys(filterState)
    .filter(id => id !== 'scenario')
    .map(id => ({
      activeFilters: (filterState[id] as MultiFilterValue).map(({ value }) => value as string),
      id,
    }))
    .filter(({ activeFilters }) => activeFilters.length);

  const filterData = (
    dataToFilter: any[],
    filters: { id: string; activeFilters: string[] }[]
    // eslint-disable-next-line max-len
  ) =>
    dataToFilter.filter(({ demandAttributes, offerAttributes }) =>
      filters.every(({ id, activeFilters }) =>
        activeFilters.includes(demandAttributes[id] || offerAttributes[id])
      )
    );

  const scenariosWithAllocations = useMemo(
    () => {
      const possiblyNullScenarios = selectedScenarioIds.map((id) => {
        const scenarioAllocations: Allocation[] = allocations[id] ?? [];
        const filteredAllocations = filterData(
          scenarioAllocations,
          actualFilters
        );
        const scenario = scenarios.find(
          ({ id: currentId }) => currentId === id
        );

        if (!scenario) {
          return null;
        }

        return {
          ...scenario,
          allocations: filteredAllocations,
        };
      });
      return possiblyNullScenarios.filter(item => item !== null) as ScenarioWithAllocations[];
    },
    [allocations, selectedScenarioIds.length, filterState]
  );

  const allAllocations = useMemo(
    () =>
      selectedScenarioIds
        .map(id =>
          (allocations[id] || []).map(
            ({ demandAttributes, offerAttributes }) => ({
              ...demandAttributes,
              ...offerAttributes,
            })
          )
        )
        .flat(),
    [allocations, selectedScenarioIds.length]
  );

  useEffect(() => {
    const newFilterOptions = projectFilters.reduce((acc, { id }) => {
      const otherFilters = actualFilters.filter(
        ({ id: filterId }) => id !== filterId
      );
      const availableOptions = allAllocations
        .filter(allocation =>
          otherFilters.every(({ id: currentId, activeFilters }) =>
            activeFilters.includes(allocation[currentId] as string)
          )
        )
        .map(allocationsMap => allocationsMap?.[id])
        .filter(Boolean);

      const uniqueOptions = [...new Set(availableOptions)].map(label => ({
        label,
        value: label,
      }));

      return {
        [id]: uniqueOptions,
        ...acc,
      };
    }, {});

    setFilterOptions(newFilterOptions);
  }, [scenariosWithAllocations]);

  return {
    filterProps,
    filterState,
    filteredScenarios: scenariosWithAllocations,
    scenarios,
    setFilterState,
    fetchScenarioData,
  };
};
