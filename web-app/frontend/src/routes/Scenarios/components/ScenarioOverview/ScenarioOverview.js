/* eslint-disable array-bracket-newline */
/* eslint-disable array-element-newline */
import React, {
  useEffect,
  useState,
} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {
  Box,
  Grid,
} from '@material-ui/core';
import get from 'lodash.get';
import find from 'lodash.find';
import { Text } from '@common/components/Text/Text';
import { useCustomTranslation } from '@utils/translate';
import { AddScenario } from './AddScenario';
import { Graph } from './Graph';
import { GraphLegend } from './GraphLegend';

const StyledGridContainer = styled(Grid)`
  && {
    margin: 40px 0 25px;
  }
`;

const StyledText = styled(Text)`
  margin-left: ${props => (props.isFirst ? 85 : 60)}px;
  text-align: center;
`;

const scenarioColors = [
  '#dadada',
  '#4db3d3',
  '#7bccc4',
];

export const ScenarioOverview = ({
  hideTitle,
  isAnyOptimized,
  projectCurrency,
  scenarios,
  firstPreselectedScenario,
  maxValue,
  step,
  unit,
}) => {
  const [
    selectedScenarios,
    setSelectedScenarios,
  ] = useState([]);
  const { t } = useCustomTranslation();

  const selectScenario = (index, scenario) => {
    setSelectedScenarios((prev) => {
      const newSelectedScenarios = [...prev];

      newSelectedScenarios[index] = scenario ? scenario.id : null;

      return newSelectedScenarios;
    });
  };

  useEffect(() => {
    const maxSavings = find(scenarios, scenario => scenario.name === 'Best Price');

    if (firstPreselectedScenario) {
      selectScenario(0, firstPreselectedScenario);
    } else if (maxSavings) {
      selectScenario(0, maxSavings);
    }
  }, [firstPreselectedScenario, scenarios]);

  const getScenario = (scenarioId) => {
    const scenario = find(scenarios, singleScenario => singleScenario.id === scenarioId);

    return scenario;
  };

  const getEmptyGraphData = () => [
    {
      baseline: get(scenarios, [
        0,
        'graphData',
        0,
        'baseline',
      ], 0),
      baselineColor: 'rgba(0,0,0,0)',
      id: 'empty',
    },
  ];

  return (
    <Box>
      { !hideTitle && (
        <Text
          fontSize={25}
          fontWeight={500}
        >
          {t('Scenario Overview')}
        </Text>
      )}
      <StyledGridContainer
        container
        spacing={5}
      >
        <GraphLegend />
        {[1, 2, 3].map((value, index) => {
          const scenario = getScenario(selectedScenarios[index]);
          const scenarioColor = scenarioColors[index];

          return (
            <Grid
              item
              xs={4}
              key={value}
            >
              <AddScenario
                items={scenarios}
                selectedScenario={scenario}
                onClick={scenarioItem => selectScenario(index, scenarioItem)}
                title={t('Add Scenario')}
                hideRemoveButton={index === 0}
                isFirst={index === 0}
                scenarioColor={scenarioColor}
                isAnyOptimized={isAnyOptimized}
              />
              <Graph
                data={scenario ? scenario.graphData : getEmptyGraphData()}
                isFirst={index === 0}
                scenarioColor={scenarioColor}
                maxValue={maxValue}
                step={step}
                unit={unit}
                projectCurrency={projectCurrency}
              />
              <StyledText fontSize={15}>
                {scenario && isAnyOptimized ? scenario.name : ''}
              </StyledText>
            </Grid>
          );
        })}
      </StyledGridContainer>
    </Box>
  );
};

ScenarioOverview.propTypes = ({
  firstPreselectedScenario: PropTypes.shape({}),
  hideTitle: PropTypes.bool,
  isAnyOptimized: PropTypes.bool,
  maxValue: PropTypes.number.isRequired,
  projectCurrency: PropTypes.string.isRequired,
  scenarios: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  step: PropTypes.number.isRequired,
  unit: PropTypes.string.isRequired,
});

ScenarioOverview.defaultProps = {
  firstPreselectedScenario: null,
  hideTitle: false,
  isAnyOptimized: true,
};
