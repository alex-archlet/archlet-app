import React from 'react';
import styled from 'styled-components';
import { Box } from '@material-ui/core';
import { colors } from '@utils/uiTheme';
import { useCustomTranslation } from '@utils/translate';

const StyledBox = styled(Box)`
  position: relative;
  top: 100px;
  width: calc(300% - 90px);
  margin-left: 90px;
`;

const StyledLegendItem = styled.div`
  &::before {
    content: '';
    position: relative;
    top: 2px;
    display: inline-block;
    width: 15px;
    height: 15px;
    margin: 0 10px 0 20px;
    background-color: ${props => props.color};
  }

  font-size: 13px;
`;

export const GraphLegend = () => {
  const { t } = useCustomTranslation();

  return (
    <StyledBox
      display="flex"
      flexDirection="row"
    >
      <StyledLegendItem color={colors.graphs.baseline}>{t('Baseline Spend')}</StyledLegendItem>
      <StyledLegendItem color={colors.graphs.savings}>{t('Savings')}</StyledLegendItem>
      <StyledLegendItem color={colors.graphs.savingsFromRebates}>
        {t('Savings from Rebates')}
      </StyledLegendItem>
    </StyledBox>
  );
};
