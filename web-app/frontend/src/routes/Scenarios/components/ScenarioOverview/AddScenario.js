import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import {
  Box,
  ClickAwayListener,
  List,
  ListItem,
  Paper,
} from '@material-ui/core';
import { Button } from '@common/components/Button/Button';
import { useCustomTranslation } from '@utils/translate';
import { Add, Close } from '@material-ui/icons';

const StyledPaper = styled(Paper)`
  min-width: 140px;
`;

const StyledButton = styled(
  ({
    hideRemoveButton: _hideRemoveButton,
    isFirst: _isFirst,
    isSelected: _isSelected,
    isAnyOptimized: _isAnyOptimized,
    ...props
  }) => <Button {...props} />
)`
  && {
    justify-content: space-between;
    width: calc(100% - ${({ isFirst }) => (isFirst ? 85 : 60)}px);
    margin-left: ${({ isFirst }) => (isFirst ? 85 : 60)}px;
    border: 1px solid #e2e2e2;
    padding: 12px 13px 13px;
    background-color: ${props =>
      props.isSelected || !props.isAnyOptimized ? '#ffffff' : '#e2e2e2'};
    color: ${props => (props.isSelected ? '#000000' : 'rgba(0, 0, 0, 0.38)')};
    font-weight: 500;
    font-size: 17px;
    text-transform: none;
    cursor: ${props => (props.hideRemoveButton ? 'default' : 'pointer')};
  }
`;

const Circle = styled.div`
  width: 20px;
  height: 20px;
  border-radius: 10px;
  background-color: ${props => props.color};
`;

const StyledStatus = styled.span`
  margin-left: 10px;
`;

const StyledClose = styled(Close)`
  && {
    width: 16px;
    height: 16px;
    border: 1px solid #9e9e9e;
    border-radius: 8px;
    background-color: #9e9e9e;
    color: white;
  }
`;

const StyledDropdown = styled(({ isFirst: _isFirst, ...props }) => (
  <Box {...props} />
))`
  position: absolute;
  left: ${props => (props.isFirst ? 85 : 60)}px;
  z-index: 10;
  width: calc(100% - ${props => (props.isFirst ? '85px' : '60px')});
  margin-right: ${props => (props.isFirst ? 85 : 60)}px;
  padding-top: 7px;
`;

export const AddScenario = ({
  hideRemoveButton,
  isAnyOptimized,
  isFirst,
  items,
  onClick,
  scenarioColor,
  selectedScenario,
}) => {
  const [state, setState] = React.useState({
    color: '#dadada',
    isOpen: false,
    selected: '',
  });

  const { t } = useCustomTranslation();

  const handleButtonClick = () => {
    setState({
      ...state,
      isOpen: !state.isOpen,
    });
  };

  const handleElementClick = (item) => {
    onClick(item);
    setState({
      ...state,
      color: scenarioColor,
      isOpen: !state.isOpen,
      selected: item.name,
    });
  };

  const handleCloseClick = (event) => {
    event.preventDefault();
    onClick(null);
    setState({
      ...state,
      color: '',
      selected: null,
    });
  };

  const handleClickAway = () => {
    setState({
      ...state,
      isOpen: false,
    });
  };

  return (
    <ClickAwayListener onClickAway={handleClickAway}>
      <Box mb={80} position="relative">
        <StyledButton
          color="primary"
          variant="text"
          hideRemoveButton={hideRemoveButton}
          isFirst={isFirst}
          isSelected={selectedScenario}
          onClick={selectedScenario ? null : handleButtonClick}
          disabled={!isAnyOptimized}
          isAnyOptimized={isAnyOptimized}
        >
          {isAnyOptimized ? (
            <Box display="flex" alignItems="center">
              {selectedScenario ? <Circle color={scenarioColor} /> : <Add />}
              <StyledStatus>
                {selectedScenario ? selectedScenario.name : t('Add Scenario')}
              </StyledStatus>
            </Box>
          ) : (
            '-'
          )}
          {!!selectedScenario && !hideRemoveButton && (
            <StyledClose onClick={handleCloseClick} />
          )}
        </StyledButton>
        {state.isOpen ? (
          <StyledDropdown isFirst={isFirst}>
            <StyledPaper>
              <List>
                {items.map(item => (
                  <ListItem
                    key={item.id}
                    button
                    onClick={() => handleElementClick(item)}
                  >
                    {item.name}
                  </ListItem>
                ))}
              </List>
            </StyledPaper>
          </StyledDropdown>
        ) : null}
      </Box>
    </ClickAwayListener>
  );
};

AddScenario.propTypes = {
  hideRemoveButton: PropTypes.bool,
  isAnyOptimized: PropTypes.bool.isRequired,
  isFirst: PropTypes.bool.isRequired,
  items: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  onClick: PropTypes.func.isRequired,
  scenarioColor: PropTypes.string.isRequired,
  selectedScenario: PropTypes.shape({}),
};

AddScenario.defaultProps = {
  hideRemoveButton: true,
  selectedScenario: null,
};
