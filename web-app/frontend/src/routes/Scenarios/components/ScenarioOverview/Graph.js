import React from 'react';
import { ResponsiveBar } from '@nivo/bar';
import { Box } from '@material-ui/core';
import PropTypes from 'prop-types';
import { GraphTooltip } from './GraphTooltip';

export const Graph = ({
  data,
  isFirst,
  maxValue,
  scenarioColor,
  step,
  unit,
}) => (
  <Box
    width="100%"
    height={250}
  >
    <ResponsiveBar
      data={data}
      maxValue={maxValue}
      keys={[
        'diff',
        'baseline',
        'savings',
        'savingsFromRebates',
      ]}
      margin={{
        bottom: 20,
        left: isFirst ? 85 : 60,
        right: 0,
        top: 20,
      }}
      padding={isFirst ? 0.55 : 0.6}
      colors={({
        data: scenarioData, id,
      }) => (id === 'diff' ? scenarioColor : scenarioData[`${id}Color`])}
      axisTop={null}
      axisRight={null}
      axisBottom={null}
      axisLeft={isFirst ?
        {
          format: (value) => {
            if (value === 0) return '0';

            return Number.isInteger(value / step) ? `${Number(value)} ${unit}` : '';
          },
          tickPadding: 10,
          tickSize: 0,
        } :
        null}
      enableLabel={false}
      animate={false}
      tooltip={({
        id,
        value,
        color,
      }) => (
        <GraphTooltip
          id={id}
          value={String(value)}
          unit={unit}
          color={color}
          projectCurrency={null}
        />
      )}
    />
  </Box>
);

Graph.propTypes = ({
  data: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  isFirst: PropTypes.bool,
  maxValue: PropTypes.number.isRequired,
  scenarioColor: PropTypes.string.isRequired,
  step: PropTypes.number.isRequired,
  unit: PropTypes.string.isRequired,
});

Graph.defaultProps = ({
  isFirst: false,
});
