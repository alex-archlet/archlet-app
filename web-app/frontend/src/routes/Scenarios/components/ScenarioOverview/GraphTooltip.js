import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { useCustomTranslation } from '@utils/translate';
import { Box } from '@material-ui/core';

const Square = styled.div`
  width: 10px;
  height: 10px;
  margin-right: 5px;
  background-color: ${props => props.color};
`;

const TooltipDescription = styled.p`
  margin: 0 3px 0;
  font-size: 10px;
`;

const StyledValue = styled.span`
  font-weight: 500;
  font-size: 10px;
`;

export const GraphTooltip = ({
  id,
  projectCurrency,
  value,
  color,
  unit,
}) => {
  const { t } = useCustomTranslation();

  const labels = {
    baseline: `${t('Baseline Spend')} (${projectCurrency})`,
    diff: `${t('Spend')}  (${projectCurrency})`,
    savings: `${t('Savings')} (${projectCurrency})`,
    savingsFromRebates: `${t('Savings from Rebates')} (${projectCurrency})`,
  };

  return (
    <Box
      display="flex"
      flexDirection="row"
      alignItems="center"
      pt={3}
      pb={3}
    >
      <Square color={color} />
      <TooltipDescription>
        {labels[id]}
        :
        <StyledValue>{` ${Number(value).toFixed(2)} ${unit}`}</StyledValue>
      </TooltipDescription>
    </Box>
  );
};

GraphTooltip.propTypes = {
  color: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  projectCurrency: PropTypes.string.isRequired,
  unit: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
};
