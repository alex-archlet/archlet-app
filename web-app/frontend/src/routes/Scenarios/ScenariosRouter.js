import React from 'react';
import PropTypes from 'prop-types';
import {
  Route,
  Switch,
} from 'react-router-dom';
import { CreateScenarioContainer } from './containers/CreateScenario';
import { ProjectScenarioOverview } from './containers/ProjectScenarioOverview';

export const ScenariosRouter = ({ match }) => (
  <Switch>
    <Route
      exact
      path={`${match.path}/new`}
      render={props => (
        <CreateScenarioContainer
          parentUrl={match.url}
          {...props}
        />
      )}
    />
    <Route
      exact
      path={`${match.path}/`}
      component={ProjectScenarioOverview}
    />
  </Switch>
);

ScenariosRouter.propTypes = ({
  match: PropTypes.shape({}).isRequired,
});
