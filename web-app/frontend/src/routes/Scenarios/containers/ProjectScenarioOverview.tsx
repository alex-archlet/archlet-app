import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router';
import { getScenarioKpi } from '@store/actions/projectScenarios';

import { colors } from '@utils/uiTheme';
import { useCustomTranslation } from '@utils/translate';
import { Filter } from '@common/components/Filter/Filter';
import { PageTitle } from '@common/components/PageTitle/PageTitle';
import { ProjectsLayout } from '@routes/ProjectDetails/components/ProjectsLayout/ProjectsLayout';
import { Flex } from '@common/components/Flex/Flex';
import { Card } from '@common/components/Card/Card';
import { ScenarioProgressContainer } from '@common/components/ScenarioProgress/ScenarioProgressContainer';
import { Row } from '@common/components/Row/Row';
import { setBackButton } from '@actions/interface';
import { PlaceholderText } from '@common/components/PlaceholderText/PlaceholderText';

import { SpendingGraph } from '../components/SpendingGraph/SpendingGraph';
import { useAllocations } from '../components/useAllocations';
import { SuppliersGraphs } from '../components/SuppliersGraphs/SuppliersGraphs';
import { ScenarioList } from '../components/ScenarioList/ScenarioList';
import { useBlockingJobEnd } from '../components/useBlockingJobEnd';

const getScenarioIds = row => row.id;

export const ProjectScenarioOverview = () => {
  const { t } = useCustomTranslation();
  const { id } = useParams();
  const dispatch = useDispatch();
  const {
    filterProps,
    setFilterState,
    scenarios,
    filteredScenarios,
    fetchScenarioData,
  } = useAllocations();
  useBlockingJobEnd(fetchScenarioData);

  
  useEffect(() => {
    const scenarioIds = scenarios.map(getScenarioIds);
    if (scenarioIds.length) {
      dispatch(getScenarioKpi(id, scenarioIds));
    }
  }, [scenarios, id])

  const noScenariosPlaceholder = (
    <PlaceholderText text={t('Select scenarios above to see visualisations')} />
  );

  useEffect(() => {
    dispatch(setBackButton('arrow'));
  }, []);

  return (
    // @ts-ignore
    <ProjectsLayout backgroundColor="#FBFBFB">
      <ScenarioProgressContainer title={t('Optimizing scenarios...')} />
      <Flex direction="column" style={{ overflow: 'scroll' }}>
        <PageTitle
          title={t('Scenario Dashboard')}
          titleColor={colors.textColor}
          rightSide={
            <Filter
              {...filterProps}
              onChange={setFilterState}
              enableClearButton
            />
          }
        />
        <Row>
          <Card title={t('Scenario Comparison')}>
            {filteredScenarios.length ? (
              <SpendingGraph scenarios={filteredScenarios} />
            ) : (
              noScenariosPlaceholder
            )}
          </Card>
          <Card title={t('Scenario Suppliers')}>
            {filteredScenarios.length ? (
              <SuppliersGraphs scenarios={filteredScenarios} />
            ) : (
              noScenariosPlaceholder
            )}
          </Card>
        </Row>
        <ScenarioList scenarios={scenarios} />
      </Flex>
    </ProjectsLayout>
  );
};
