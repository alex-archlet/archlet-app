import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { useHistory, useParams } from 'react-router';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import {
  createScenario,
  resetScenarioDetails,
  updateScenario,
} from '@actions/projectScenarios';
import { getProjectInfo } from '@actions/projects';
import { setBackButton, setPageTitle } from '@actions/interface';
import { getCreateUpdateScenarioPayload } from '@utils/scenario/scenarioSettingsHelpers';
import { projectsSelectors } from '@selectors/projects';
import { MarginWrapper } from '@common/components/MarginWrapper/MarginWrapper';
import { ScenarioProgressContainer } from '@common/components/ScenarioProgress/ScenarioProgressContainer';
import { useCustomTranslation } from '@utils/translate';
import { projectScenariosSelectors } from '@selectors/projectScenarios';
import { runOptimization } from '@store/actions/projectScenarios';
import { CustomDialog } from '@common/components/CustomDialog/CustomDialog';
import { CreateScenarioForm } from '../components/CreateScenarioForm/CreateScenarioForm';

const CreateScenario = ({
  createScenarioAction,
  projectCurrency,
  projectInfo,
  projectItemCategories,
  projectCriteria,
  projectRegions,
  projectSuppliersPerRegions,
  projectSuppliersPerItem,
  projectSuppliersPerItemAlternative,
  projectUnit,
  resetScenarioDetailsAction,
  runOptimizationAction,
  getProjectInfoAction,
  setBackButtonAction,
  scenarioDetails,
  setPageTitleAction,
  updateScenarioAction,
  parentUrl,
}) => {
  const { id } = useParams();
  const history = useHistory();
  const { t } = useCustomTranslation();

  useEffect(() => {
    setBackButtonAction('close', parentUrl);
    setPageTitleAction(t('Create New Scenario'));
    resetScenarioDetailsAction();

    return () => setBackButtonAction('');
  }, []);

  useEffect(() => {
    if (!Object.keys(projectInfo).length) {
      getProjectInfoAction(id);
    }
  }, [projectInfo]);

  const [customScenarioConstraints, setCustomScenarioConstraints] = useState({
    capacities: [],
    incumbent: {
      label: 'Standard Scenario',
      value: 'custom',
    },
    itemType: [],
    partialAllocation: false,
    priceComponent: {
      label: 'Total Price',
      value: 'Total Price',
    },
    rebates: false,
    splitItems: false,
    volume: [],
  });

  const [progressTitle, setProgressTitle] = useState('');

  const [numOfSuppliers, setNumOfSuppliers] = useState([]);

  const [maxSpend, setMaxSpend] = useState([]);

  const [excludedSuppliers, setExcludedSuppliers] = useState([]);

  const [errorView, setErrorView] = useState(false);

  const [isDialogOpen, setIsDialogOpen] = useState(false);

  const runScenarioOptimization = async (scenarioId) => {
    setProgressTitle('Optimizing scenario...');

    try {
      const optimization = await runOptimizationAction(id, scenarioId);

      return optimization;
    } catch (error) {
      setErrorView(error.errorMessage);

      return error;
    }
  };

  const handleSubmitForm = async (values) => {
    setProgressTitle('Creating scenario...');

    const payload = getCreateUpdateScenarioPayload(
      values,
      customScenarioConstraints,
      excludedSuppliers,
      maxSpend,
      numOfSuppliers
    );

    const isUpdating = !!scenarioDetails.id;

    const createOrUpdateScenarioAction = isUpdating
      ? updateScenarioAction(
          scenarioDetails.id,
          payload,
          scenarioDetails.name,
          id
        )
      : createScenarioAction({
          ...payload,
          projectId: id,
        });

    const scenario = await createOrUpdateScenarioAction;
    const scenarioId = isUpdating ? scenarioDetails.id : scenario.id;

    const optimization = await runScenarioOptimization(scenarioId);

    if (optimization.statusId !== 5) {
      setIsDialogOpen(true);
    } else {
      history.push(`/projects/${id}/scenarios`);
    }
  };

  const regions = ['Global', 'Every Region', ...projectRegions];

  const criteria = ['Exclude All', ...projectCriteria];

  return (
    <MarginWrapper>
      <CustomDialog
        isOpen={isDialogOpen}
        message={t(
          'Oops. Some constraints cannot be taken into consideration for the scenario optimization. Please adjust the settings.'
        )}
        buttonText={t('Adjust settings')}
        handleClose={() => setIsDialogOpen(false)}
      />
      <ScenarioProgressContainer title={progressTitle} />
      <CreateScenarioForm
        customError={errorView}
        regions={regions}
        criteria={criteria}
        customScenarioConstraints={customScenarioConstraints}
        itemCategories={projectItemCategories}
        suppliersPerRegions={projectSuppliersPerRegions}
        suppliersPerItem={projectSuppliersPerItem}
        suppliersPerItemAlternative={projectSuppliersPerItemAlternative}
        onSubmitForm={handleSubmitForm}
        scenarioDetails={scenarioDetails}
        onSetNumOfSuppliers={setNumOfSuppliers}
        onSetMaxSpend={setMaxSpend}
        onSetExcludedSuppliers={setExcludedSuppliers}
        initialValues={{
          capacity: false,
          incumbent: false,
          partialAllocation: false,
          rebates: false,
          splitItems: false,
          supplierExcluded: [],
        }}
        projectCurrency={projectCurrency}
        projectUnit={projectUnit}
        setCustomScenarioConstraints={setCustomScenarioConstraints}
      />
    </MarginWrapper>  
  );
};

const mapStateToProps = state => ({
  projectCriteria: projectsSelectors.getProjectCriteria(state),
  projectCurrency: projectsSelectors.getProjectCurrencyCode(state),
  projectInfo: projectsSelectors.getProjectInfo(state),
  projectItemCategories: projectsSelectors.getProjectItemCategories(state),
  projectRegions: projectsSelectors.getProjectRegions(state),
  projectSuppliersPerItem: projectsSelectors.getProjectSuppliersPerItem(state),
  projectSuppliersPerItemAlternative: projectsSelectors.getProjectSuppliersPerItemAlternative(
    state
  ),
  projectSuppliersPerRegions: projectsSelectors.getProjectSuppliersPerRegions(
    state
  ),
  projectUnit: projectsSelectors.getProjectUnitName(state),
  scenarioDetails: projectScenariosSelectors.getScenarioDetails(state),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      createScenarioAction: createScenario,
      resetScenarioDetailsAction: resetScenarioDetails,
      runOptimizationAction: runOptimization,
      getProjectInfoAction: getProjectInfo,
      setBackButtonAction: setBackButton,
      setPageTitleAction: setPageTitle,
      updateScenarioAction: updateScenario,
    },
    dispatch
  );

CreateScenario.propTypes = {
  createScenarioAction: PropTypes.func.isRequired,
  parentUrl: PropTypes.string.isRequired,
  projectCriteria: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  projectCurrency: PropTypes.string.isRequired,
  projectInfo: PropTypes.shape({}).isRequired,
  projectItemCategories: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  projectRegions: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  projectSuppliersPerItem: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  projectSuppliersPerItemAlternative: PropTypes.arrayOf(PropTypes.shape({}))
    .isRequired,
  projectSuppliersPerRegions: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  projectUnit: PropTypes.string.isRequired,
  resetScenarioDetailsAction: PropTypes.func.isRequired,
  runOptimizationAction: PropTypes.func.isRequired,
  getProjectInfoAction: PropTypes.func.isRequired,
  scenarioDetails: PropTypes.shape({}).isRequired,
  setBackButtonAction: PropTypes.func.isRequired,
  setPageTitleAction: PropTypes.func.isRequired,
  updateScenarioAction: PropTypes.func.isRequired,
};

export const CreateScenarioContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateScenario);
