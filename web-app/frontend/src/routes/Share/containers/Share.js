import React, {
  useEffect,
  useState,
} from 'react';
import { connect } from 'react-redux';
import { useParams } from 'react-router-dom';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import includes from 'lodash.includes';
import { setBackButton } from '@actions/interface';
import { MarginWrapper } from '@common/components/MarginWrapper/MarginWrapper';
import styled from 'styled-components';
import {
  addProjectMembers,
  deleteProjectMember,
  getProjectMembers,
  updateProjectMember,
} from '@actions/projectMembers';
import { Text } from '@common/components/Text/Text';
import { colors } from '@utils/uiTheme';
import { projectMembersSelectors } from '@selectors/projectMembers';
import { MembersList } from '../components/MembersList/MembersList';
import { InviteMembers } from '../components/InviteMembers/InviteMembers';

const StyledBackendError = styled(Text)`
  height: 34px;
  margin-top: 15px;
`;

const Share = ({
  addProjectMembersAction,
  deleteProjectMemberAction,
  error,
  getProjectMembersAction,
  updateProjectMemberAction,
  setBackButtonAction,
  members,
}) => {
  const projectId = useParams().id;

  useEffect(() => {
    getProjectMembersAction(projectId);
    setBackButtonAction('close');

    return () => {
      setBackButtonAction('');
    };
  }, [projectId]);

  const [
    membersToDelete,
    setMembersToDelete,
  ] = useState([]);

  const [
    accessType,
    setAccessType,
  ] = useState({});

  const [
    newMembers,
    setNewMembers,
  ] = useState([{}]);

  const [
    showError,
    setShowError,
  ] = useState(false);

  const deleteMember = id => setMembersToDelete(prevValue => [
    ...prevValue,
    id,
  ]);

  const setAccess = (id, access) => setAccessType(prevValue => ({
    ...prevValue,
    [id]: access,
  }));

  const saveMembers = () => {
    if (membersToDelete.length) {
      deleteProjectMemberAction({
        ids: membersToDelete,
        projectId,
      });
      setMembersToDelete([]);
    }

    updateProjectMemberAction(accessType);
  };

  const addMembers = () => {
    const selectedMembers = newMembers.filter(member => member.length);

    setShowError(true);

    if (selectedMembers) {
      addProjectMembersAction({
        emails: selectedMembers,
        projectId,
      }).then(() => setNewMembers([{}]));
    }
  };

  const addNewMember = (email, index) => {
    setNewMembers(prevValue => [
      ...prevValue.slice(0, index),
      email,
      ...prevValue.slice(index + 1, prevValue.length),
    ]);
  };

  const addNewRow = () => {
    setNewMembers(prevValue => [
      ...prevValue,
      '',
    ]);
  };

  const displayedMembers = members.filter(member => !includes(membersToDelete, member.id));

  return (
    <MarginWrapper>
      <MembersList
        members={displayedMembers}
        onDeleteProjectMember={deleteMember}
        onSave={saveMembers}
        onSetAccessType={setAccess}
      />
      <InviteMembers
        newMembers={newMembers}
        addNewMember={addNewMember}
        addNewRow={addNewRow}
        addMembers={addMembers}
      />

      {showError && error && <StyledBackendError color={colors.red}>{ error }</StyledBackendError> }
    </MarginWrapper>
  );
};

const mapStateToProps = state => ({
  error: projectMembersSelectors.error(state),
  members: projectMembersSelectors.getProjectMembers(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  addProjectMembersAction: addProjectMembers,
  deleteProjectMemberAction: deleteProjectMember,
  getProjectMembersAction: getProjectMembers,
  setBackButtonAction: setBackButton,
  updateProjectMemberAction: updateProjectMember,
}, dispatch);

Share.propTypes = {
  addProjectMembersAction: PropTypes.func.isRequired,
  deleteProjectMemberAction: PropTypes.func.isRequired,
  error: PropTypes.string.isRequired,
  getProjectMembersAction: PropTypes.func.isRequired,
  members: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  setBackButtonAction: PropTypes.func.isRequired,
  updateProjectMemberAction: PropTypes.func.isRequired,
};

export const ShareContainer = connect(mapStateToProps, mapDispatchToProps)(Share);
