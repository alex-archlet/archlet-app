import React from 'react';
import PropTypes from 'prop-types';
import {
  Route,
  Switch,
} from 'react-router-dom';
import { ShareContainer } from './containers/Share';

export const ShareRouter = ({ match }) => (
  <Switch>
    <Route
      path={`${match.path}/`}
      exact
      component={ShareContainer}
    />
  </Switch>
);

ShareRouter.propTypes = ({
  match: PropTypes.shape({}).isRequired,
});
