import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { useCustomTranslation } from '@utils/translate';
import {
  FormControl,
  FormControlLabel,
  Radio,
  RadioGroup,
} from '@material-ui/core';

const StyledRadioGroup = styled(RadioGroup)`
  && {
    display: flex;
    flex-direction: row;
  }
`;

const StyledFormControlLabel = styled(FormControlLabel)`
  && {
    margin-left: 8px;

    & .MuiFormControlLabel-label {
      font-size: 16px;
    }
  }
`;

export const AccessForm = ({
  defaultAccess,
  memberId,
  setAccessType,
}) => {
  const { t } = useCustomTranslation();

  return (
    <FormControl component="fieldset">
      <StyledRadioGroup
        aria-label="access"
        name="projectAccess"
        defaultValue={defaultAccess}
        onChange={({ target }) => setAccessType(memberId, target.value)}
      >
        <StyledFormControlLabel
          value="read"
          control={<Radio />}
          label={t('Can view')}
        />
        <StyledFormControlLabel
          value="read and write"
          control={<Radio />}
          label={t('Can edit')}
        />
      </StyledRadioGroup>
    </FormControl>
  );
};

AccessForm.propTypes = {
  defaultAccess: PropTypes.string.isRequired,
  memberId: PropTypes.number.isRequired,
  setAccessType: PropTypes.func.isRequired,
};
