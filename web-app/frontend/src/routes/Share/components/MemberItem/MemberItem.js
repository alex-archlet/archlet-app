import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {
  Box,
  Card,
  CardContent,
} from '@material-ui/core';
import { Close } from '@material-ui/icons';
import { Text } from '@common/components/Text/Text';
import { colors } from '@utils/uiTheme';
import { AccessForm } from '../AccessForm/AccessForm';

const StyledCardContent = styled(CardContent)`
  && {
    padding: 10px 15px 20px 20px;

    &:last-child {
      padding-bottom: 16px;
    }
  }
`;

const StyledCard = styled(Card)`
  width: 450px;
  margin-right: 25px;
`;

const StyledCloseIcon = styled(Close)`
  && {
    fill: ${colors.iconGrey};
    width: 22px;
    cursor: pointer;
  }
`;

export const MemberItem = ({
  deleteProjectMember,
  member: {
    accountType,
    id,
    username,
    firstName,
    lastName,
    ProjectAccesses,
  },
  setAccessType,
}) => (
  <Box
    mb={20}
    display="flex"
    alignItems="center"
  >
    <StyledCard elevation={0}>
      <StyledCardContent>
        <Box
          display="flex"
          justifyContent="space-between"
          alignItems="flex-end"
        >
          <div>
            <Text
              fontSize={16}
              fontWeight={500}
              lineHeight={35}
            >
              { `${firstName} ${lastName}` }
            </Text>
            <Text fontSize={16}>{ username }</Text>
          </div>
          { accountType !== 'admin' && <StyledCloseIcon onClick={() => deleteProjectMember(id)} /> }
        </Box>
      </StyledCardContent>
    </StyledCard>
    <AccessForm
      defaultAccess={ProjectAccesses.accessType}
      memberId={id}
      setAccessType={setAccessType}
    />
  </Box>
);

MemberItem.propTypes = {
  deleteProjectMember: PropTypes.func.isRequired,
  member: PropTypes.shape({}).isRequired,
  setAccessType: PropTypes.func.isRequired,
};
