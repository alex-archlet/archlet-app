import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Box, TextField } from '@material-ui/core';
import { PrimaryButton } from '@common/components/Button/PrimaryButton';
import { SecondaryButton } from '@common/components/Button/SecondaryButton';
import { useCustomTranslation } from '@utils/translate';
import { Send } from '@material-ui/icons';
import { SectionTitle } from '../SectionTitle/SectionTitle';
import { AccessForm } from '../AccessForm/AccessForm';

const SendInvitationsButton = styled(SecondaryButton)`
  && {
    margin-top: 30px;
  }
`;

const StyledSendIcon = styled(Send)`
  && {
    margin-right: 10px;
  }
`;

const StyledInput = styled(TextField)`
  && {
    width: 450px;
    margin-right: 25px;
  }
`;

export const InviteMembers = ({
  addNewMember,
  addNewRow,
  newMembers,
  addMembers,
}) => {
  const { t } = useCustomTranslation();

  return (
    <Box>
      <SectionTitle
        title={t('Invite Members')}
        subtitle={t('Invite other team members to the project')}
      />
      {newMembers.map((_, index) => (
        <Box display="flex" mb={25} alignItems="center">
          <StyledInput
            variant="outlined"
            label="Email"
            onChange={({ target }) => addNewMember(target.value, index)}
          />
          <AccessForm
            defaultAccess="read"
            memberId=""
            setAccessType={() => {}}
          />
        </Box>
      ))}
      <Box display="flex" flexDirection="column" alignItems="flex-start">
        <PrimaryButton onClick={addNewRow}>
          {t('Add Members')}
        </PrimaryButton>
        <SendInvitationsButton onClick={addMembers}>
          <StyledSendIcon />
          {t('Send Invitations')}
        </SendInvitationsButton>
      </Box>
    </Box>
  );
};

InviteMembers.propTypes = {
  addMembers: PropTypes.func.isRequired,
  addNewMember: PropTypes.func.isRequired,
  addNewRow: PropTypes.func.isRequired,
  newMembers: PropTypes.arrayOf(PropTypes.string).isRequired,
};
