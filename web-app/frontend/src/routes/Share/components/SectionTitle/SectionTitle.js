import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Box } from '@material-ui/core';
import { Text } from '@common/components/Text/Text';

const StyledTitle = styled.h2`
  margin: 48px 0 0 0;
  font-size: 24px;
`;

export const SectionTitle = ({
  subtitle,
  title,
}) => (
  <Box mb={30}>
    <StyledTitle>{ title }</StyledTitle>
    <Text
      fontSize={16}
      lineHeight={40}
    >
      { subtitle }
    </Text>
  </Box>
);

SectionTitle.propTypes = {
  subtitle: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
};
