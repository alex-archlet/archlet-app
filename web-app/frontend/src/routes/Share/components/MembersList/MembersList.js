import React from 'react';
import PropTypes from 'prop-types';
import { useCustomTranslation } from '@utils/translate';
import { Box } from '@material-ui/core';
import { SecondaryButton } from '@common/components/Button/SecondaryButton';
import { SectionTitle } from '../SectionTitle/SectionTitle';
import { MemberItem } from '../MemberItem/MemberItem';

export const MembersList = ({
  onDeleteProjectMember,
  onSetAccessType,
  onSave,
  members,
}) => {
  const { t } = useCustomTranslation();

  return (
    <Box mb={65}>
      <SectionTitle
        title={t('Members')}
        subtitle={t('Who currently has access to the project')}
      />
      {members &&
        members.map(member => (
          <MemberItem
            key={member.id}
            member={member}
            deleteProjectMember={onDeleteProjectMember}
            setAccessType={onSetAccessType}
          />
        ))}
      <SecondaryButton onClick={onSave}>
        {t('Save')}
      </SecondaryButton>
    </Box>
  );
};

MembersList.propTypes = {
  members: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  onDeleteProjectMember: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  onSetAccessType: PropTypes.func.isRequired,
};
