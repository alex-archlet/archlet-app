import React, { useState } from 'react';
import styled from 'styled-components';
import { Box, Divider, Typography } from '@material-ui/core';
import { Input } from '@common/components/Input/Input';
import { useCustomTranslation } from '@utils/translate';
import * as Yup from 'yup';
import { FormikHelpers, useFormik } from 'formik';
import { useDispatch, useSelector } from 'react-redux';
import { userSelectors } from '@store/selectors/user';
import { updateUserDetails } from '@store/actions/user';
import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';
import { colors } from '@utils/uiTheme';
import { Text } from '@common/components/Text/Text';
import { Button } from '@common/components/Button/Button';
import { getFieldErrors } from '@utils/helpers';

const StyledTitle = styled(Typography)`
  && {
    margin-bottom: 22px;
    font-weight: 500;
    font-size: 18.5px;
  }
`;

const StyledMessage = styled(Text)`
  height: 34px;
  margin-top: 15px;
`;

const StyledErrorMessage = styled(Text)`
  height: 34px;
  margin-top: 15px;
  color: ${colors.red};
`;

interface FormValues {
  firstName: string;
  id: string;
  lastName: string;
}

export const UserDetailsForm = () => {
  const { t } = useCustomTranslation();
  const [errorResponse, setErrorResponse] = useState<string>('');
  const { firstName, id, lastName } = useSelector(userSelectors.getUser);
  const isFetching = useSelector(userSelectors.isFetching);
  const dispatch = useDispatch();

  const ReviewSchema = Yup.object().shape({
    firstName: Yup.string().required(t('First name is required')),
    lastName: Yup.string().required(t('Last name is required')),
  });

  const onDetailsFormSubmit = async (
    formSubmitValues: FormValues,
    formikHelpers: FormikHelpers<FormValues>
  ) => {
    const response = await dispatch(updateUserDetails(formSubmitValues));
    // @ts-ignore
    if (response.status === 'success') {
      formikHelpers.setStatus(true);
    } else {
      const fieldError = getFieldErrors(response);
      setErrorResponse(Object.values(fieldError).join(','));
    }
  };

  const {
    values,
    errors,
    setFieldValue,
    setFieldTouched,
    isValid,
    touched,
    status,
    setStatus,
    handleSubmit,
  } = useFormik<FormValues>({
    initialValues: {
      firstName,
      id,
      lastName,
    },
    validationSchema: ReviewSchema,
    onSubmit: onDetailsFormSubmit,
    validateOnBlur: true,
    validateOnChange: true,
  });

  const onChange = (field: string, value: string) => {
    setStatus(false);
    setFieldTouched(field);
    setFieldValue(field, value);
  };
  const hasButtonEnabled = isValid && Object.keys(touched).length > 0;

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <StyledTitle variant="h5">{t('User Name')}</StyledTitle>
        <Input
          value={values.firstName}
          label={t('User First Name')}
          type="text"
          placeholder={t('User First Name')}
          onChange={value => onChange('firstName', value)}
          error={Boolean(errors.firstName)}
          helperText={errors.firstName}
        />
        <VerticalSpacer height={20} />
        <Input
          value={values.lastName}
          label={t('User Last Name')}
          type="text"
          placeholder={t('User Last Name')}
          onChange={value => onChange('lastName', value)}
          error={Boolean(errors.lastName)}
          helperText={errors.lastName}
        />
        <Box mb={40} mt={20}>
          <Divider variant="middle" />
        </Box>
        <Button type="submit" color="secondary" disabled={!hasButtonEnabled}>
          {t('Save Settings')}
        </Button>
        {errorResponse && !isFetching && (
          <StyledErrorMessage>{errorResponse}</StyledErrorMessage>
        )}
        {status && !errorResponse && !isFetching && (
          <StyledMessage>
            {t('User details have been successfully updated.')}
          </StyledMessage>
        )}
      </div>
    </form>
  );
};
