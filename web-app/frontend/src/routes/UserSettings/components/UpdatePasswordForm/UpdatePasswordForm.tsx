import React, { useState } from 'react';
import styled from 'styled-components';
import { Box, Divider, Typography } from '@material-ui/core';
import { Input } from '@common/components/Input/Input';
import { useCustomTranslation } from '@utils/translate';
import * as Yup from 'yup';
import { FormikHelpers, useFormik } from 'formik';
import { useDispatch, useSelector } from 'react-redux';
import { updateUserPassword } from '@store/actions/user';
import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';
import { passwordRegex } from '@utils/validators';
import { userSelectors } from '@store/selectors/user';
import { Text } from '@common/components/Text/Text';
import { Button } from '@common/components/Button/Button';
import { getFieldErrors } from '@utils/helpers';
import { colors } from '@utils/uiTheme';

const StyledTitle = styled(Typography)`
  && {
    margin-bottom: 22px;
    font-weight: 500;
    font-size: 18.5px;
  }
`;

const StyledMessage = styled(Text)`
  height: 34px;
  margin-top: 15px;
`;

const StyledErrorMessage = styled(Text)`
  height: 34px;
  margin-top: 15px;
  color: ${colors.red} !important;
`;
interface FormValues {
  oldPassword: string;
  newPassword: string;
  confirmNewPassword: string;
}

export const UpdatePasswordForm = () => {
  const { t } = useCustomTranslation();
  const dispatch = useDispatch();
  const [errorResponse, setErrorResponse] = useState<string>('');
  const isFetching = useSelector(userSelectors.isFetching);

  const ReviewSchema = Yup.object().shape({
    oldPassword: Yup.string().required(t('A Old Password is required')),
    newPassword: Yup.string().required(t('A New Password is required')).matches(
      passwordRegex,
      t(
        // eslint-disable-next-line max-len
        'Password must be minimum eight characters long, contain at least one uppercase and one lowercase letter, one number and one special character (#?!@$%^&*-._).'
      )
    ),
    confirmNewPassword: Yup.string()
      .required(t('A New Confirm Password is required'))
      .oneOf([Yup.ref('newPassword')], t('Passwords must match')),
  });

  const onPasswordFormSubmit = async (
    formSubmitValues: FormValues,
    formikHelpers: FormikHelpers<FormValues>
  ) => {
    const response = await dispatch(updateUserPassword(formSubmitValues));

    // @ts-ignore
    if (response.status === 'success') {
      formikHelpers.setStatus(true);
      formikHelpers.resetForm();
    } else {
      const fieldError = getFieldErrors(response);
      setErrorResponse(Object.values(fieldError).join(','));
    }
  };

  const {
    values,
    errors,
    setFieldValue,
    setFieldTouched,
    isValid,
    touched,
    handleSubmit,
    status,
  } = useFormik<FormValues>({
    initialValues: {
      oldPassword: '',
      newPassword: '',
      confirmNewPassword: '',
    },
    validationSchema: ReviewSchema,
    onSubmit: onPasswordFormSubmit,
    validateOnBlur: true,
    validateOnChange: true,
  });

  const onChange = (field: string, value: string) => {
    setFieldTouched(field);
    setFieldValue(field, value);
  };
  const hasButtonEnabled = isValid && Object.keys(touched).length > 0;

  const hasOldPasswordError = Boolean(
    touched.oldPassword && errors.oldPassword
  );
  const hasNewPasswordError = Boolean(
    touched.newPassword && errors.newPassword
  );
  const hasConfirmNewPasswordError = Boolean(
    touched.confirmNewPassword && errors.confirmNewPassword
  );
  return (
    <form onSubmit={handleSubmit}>
      <div>
        <StyledTitle variant="h5">{t('User Password')}</StyledTitle>
        <Input
          value={values.oldPassword}
          label={t('Old Password')}
          type="password"
          placeholder={t('Old Password')}
          onChange={value => onChange('oldPassword', value)}
          error={hasOldPasswordError}
          helperText={hasOldPasswordError ? errors.oldPassword : undefined}
        />
        <VerticalSpacer height={20} />
        <Input
          value={values.newPassword}
          label={t('New Password')}
          type="password"
          placeholder={t('New Password')}
          onChange={value => onChange('newPassword', value)}
          error={hasNewPasswordError}
          helperText={hasNewPasswordError ? errors.newPassword : undefined}
        />
        <VerticalSpacer height={20} />
        <Input
          value={values.confirmNewPassword}
          label={t('Confirm New Password')}
          type="password"
          placeholder={t('Confirm New Password')}
          onChange={value => onChange('confirmNewPassword', value)}
          error={hasConfirmNewPasswordError}
          helperText={
            hasConfirmNewPasswordError ? errors.confirmNewPassword : undefined
          }
        />
        <Box mb={40} mt={20}>
          <Divider variant="middle" />
        </Box>
        <Button type="submit" color="secondary" disabled={!hasButtonEnabled}>
          {t('Update Password')}
        </Button>
        {errorResponse && !isFetching && (
          <StyledErrorMessage>{errorResponse}</StyledErrorMessage>
        )}
        {status && !errorResponse && !isFetching && (
          <StyledMessage>
            {t('Password has been successfully updated.')}
          </StyledMessage>
        )}
      </div>
    </form>
  );
};
