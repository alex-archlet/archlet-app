import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { clearFields } from 'redux-form';
import styled from 'styled-components';
import { updateUserPassword } from '@actions/user';
import { userSelectors } from '@selectors/user';
import { setBackButton, setPageTitle } from '@actions/interface';
import { Box } from '@material-ui/core';
import { useLocation } from 'react-router';
import { UserDetailsForm } from '../components/UserDetailsForm/UserDetailsForm';
import { UpdatePasswordForm } from '../components/UpdatePasswordForm/UpdatePasswordForm';

const StyledBox = styled(Box)`
  && {
    margin-bottom: 58px;
  }
`;
interface Props {
  userDetails: { id: string; firstName: string; lastName: string };
  isFetching: boolean;
  clearFieldsAction: Function;
  setBackButtonAction: Function;
  setPageTitleAction: Function;
  updateUserDetailsAction: Function;
  updateUserPasswordAction: Function;
}

const UserSettings: React.FC<Props> = ({
  setBackButtonAction,
  setPageTitleAction,
}) => {
  // @ts-ignore
  const { state }: { referrer: string } = useLocation();

  useEffect(() => {
    setBackButtonAction('close', (state && state.referrer) || '/');
    setPageTitleAction('User Settings');
  }, [state]);

  return (
    <Box display="flex" justifyContent="center" alignItems="flex-start" mt="5%">
      <Box width={410}>
        <StyledBox>
          <UserDetailsForm />
        </StyledBox>
        <Box>
          <UpdatePasswordForm />
        </Box>
      </Box>
    </Box>
  );
};

const mapStateToProps = state => ({
  error: userSelectors.error(state),
  isFetching: userSelectors.isFetching(state),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      clearFieldsAction: clearFields,
      setBackButtonAction: setBackButton,
      setPageTitleAction: setPageTitle,
      updateUserPasswordAction: updateUserPassword,
    },
    dispatch
  );

export const UserSettingsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(UserSettings);
