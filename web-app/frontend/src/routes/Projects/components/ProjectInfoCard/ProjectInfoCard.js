import React, { useState } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Box, Card, CardContent, Typography } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { colors } from '@utils/uiTheme';
import { useCustomTranslation } from '@utils/translate';
import { ConfirmationDialog } from '@common/components/ConfirmationDialog/ConfirmationDialog';
import { CancelButton } from '@common/components/Button/CancelButton';
import { DeleteButton } from '@common/components/Button/DeleteButton';
import { EditProjectDropdown } from './EditProjectDropdown';
import { ProjectDate } from './ProjectDate';
import { UserInitials } from './UserInitials';
import { FinishedLabel } from './FinishedLabel';
import {
  BID_TYPE_TO_URL,
  MANUAL_UPLOAD_URL,
} from '../../../ProjectDetails/components/ProjectsLayout/biddingTypes';

const StyledLink = styled(Link)`
  width: 100%;
  color: ${colors.black};
  text-decoration: none;
`;

const StyledCard = styled(Card)`
  width: 100%;
  height: 100%;
`;

const StyledCardContent = styled(CardContent)`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: flex-start;
  width: 100%;
  height: 100%;
`;

const ProjectTitle = styled(Typography)`
  height: 89px;
  padding-top: 20px;
  text-align: left;
`;

export const ProjectInfoCard = ({
  onProjectDelete,
  project: { name, createdAt, updatedAt, status, users, id, biddingTypeId },
}) => {
  const [isDialogVisible, setDialogVisible] = useState(false);

  const { t } = useCustomTranslation();
  const showFinishedLabel = statusValue => statusValue === 'Finished';

  const hideDialog = () => {
    setDialogVisible(false);
  };

  const showDialog = () => {
    setDialogVisible(true);
  };

  const handleProjectDelete = () => {
    onProjectDelete(id);
    hideDialog();
  };

  const dataUrl = BID_TYPE_TO_URL[biddingTypeId] || MANUAL_UPLOAD_URL;

  return (
    <Box display="flex" m={10} mb={20} width={300} height={300}>
      <ConfirmationDialog
        isOpen={isDialogVisible}
        message="You have unsaved changes. Are you sure you want to leave this page without saving?"
        handleClose={hideDialog}
        ButtonActions={
          <>
            <DeleteButton onClick={handleProjectDelete} autoFocus>
              {t('Delete')}
            </DeleteButton>
            <CancelButton onClick={hideDialog}>
              {t('No')}
            </CancelButton>
          </>
        }
      />
      <StyledCard elevation={0}>
        <StyledCardContent>
          <StyledLink to={`/projects/${id}/${dataUrl}`}>
            <Box
              width="100%"
              height={210}
              display="flex"
              flexDirection="column"
              justifyContent="space-between"
            >
              <Box>
                <FinishedLabel isVisible={showFinishedLabel(status)} />
                <ProjectTitle variant="h5">{name}</ProjectTitle>
              </Box>
              <Box>
                <ProjectDate date={updatedAt} description={t('Last updated')} />
                <ProjectDate date={createdAt} description={t('Created')} />
              </Box>
            </Box>
          </StyledLink>
          <Box
            display="flex"
            justifyContent="space-between"
            alignItems="flex-end"
            width="100%"
          >
            <Box display="flex">
              {!!users &&
                users.map(user => (
                  <Box key={`${id}${user.firstName}`}>
                    <UserInitials
                      firstName={user.firstName}
                      lastName={user.lastName}
                    />
                  </Box>
                ))}
            </Box>
            <EditProjectDropdown deleteProject={showDialog} projectId={id} />
          </Box>
        </StyledCardContent>
      </StyledCard>
    </Box>
  );
};

ProjectInfoCard.propTypes = {
  onProjectDelete: PropTypes.func.isRequired,
  project: PropTypes.shape({
    biddingTypeId: PropTypes.number.isRequired,
    createdAt: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    status: PropTypes.string.isRequired,
    updatedAt: PropTypes.string.isRequired,
    users: PropTypes.arrayOf(
      PropTypes.shape({
        firstName: PropTypes.string.isRequired,
        lastName: PropTypes.string.isRequired,
      })
    ),
  }).isRequired,
};
