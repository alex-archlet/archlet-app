import React from 'react';
import PropTypes from 'prop-types';
import { Box } from '@material-ui/core';
import styled from 'styled-components';
import { Lock } from '@material-ui/icons';
import { useCustomTranslation } from '@utils/translate';

const StyledLock = styled(Lock)`
  && {
    position: relative;
    top: 2px;
    margin-right: 5px;
    font-size: 15px;
  }
`;

const StyledLabel = styled.span`
  && {
    font-weight: 300;
    font-size: 12px;
  }
`;

export const FinishedLabel = ({ isVisible = false }) => {
  const { t } = useCustomTranslation();

  return (
    <Box height={20}>
      { isVisible &&
      (
        <Box>
          <StyledLock />
          <StyledLabel>{t('Finished')}</StyledLabel>
        </Box>
      )}
    </Box>
  );
};

FinishedLabel.propTypes = ({
  isVisible: PropTypes.bool,
});

FinishedLabel.defaultProps = ({
  isVisible: false,
});
