import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Avatar } from '@material-ui/core';
import { colors } from '@utils/uiTheme';

const StyledAvatar = styled(Avatar)`
  && {
    width: 35px;
    height: 35px;
    margin-right: 6px;
    background-color: ${colors.palette.secondary.main};
    font-size: 15px;
  }
`;

export const UserInitials = ({
  firstName,
  lastName,
}) => {
  const getInitials = (first, last) => {
    const firstInitial = first[0];
    const secondInitial = last[0];

    return firstInitial && secondInitial ? `${firstInitial}${secondInitial}` : undefined;
  };

  const initials = getInitials(firstName, lastName);

  return (
    initials ? <StyledAvatar>{ initials }</StyledAvatar> : null
  );
};

UserInitials.propTypes = ({
  firstName: PropTypes.string.isRequired,
  lastName: PropTypes.string.isRequired,
});
