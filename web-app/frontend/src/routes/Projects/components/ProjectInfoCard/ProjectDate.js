import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import styled from 'styled-components';
import {
  Grid,
} from '@material-ui/core';
import { Text } from '@common/components/Text/Text';
import { colors } from '@utils/uiTheme';

const StyledGrid = styled(Grid)`
  margin-bottom: 7px;
`;

const StyledDescription = styled(Text)`
  color: ${colors.darkGrey};
`;

export const ProjectDate = ({
  date,
  description,
}) => (
  <StyledGrid
    container
    justify="flex-start"
  >
    <Grid
      item
      xs={5}
    >
      <StyledDescription>{description}</StyledDescription>
    </Grid>
    <Grid
      item
      xs={7}
    >
      <Text fontWeight="500">{ moment(date).calendar() }</Text>
    </Grid>
  </StyledGrid>
);

ProjectDate.propTypes = ({
  date: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
});
