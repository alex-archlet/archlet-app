import React, { useState } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import {
  Box,
  ClickAwayListener,
  List,
  ListItem,
  Paper,
} from '@material-ui/core';
import { useCustomTranslation } from '@utils/translate';
import { Link, useLocation } from 'react-router-dom';
import { MoreHoriz } from '@material-ui/icons';
import { colors } from '@utils/uiTheme';

const StyledMoreHoriz = styled(MoreHoriz)`
  position: relative;
  top: 15px;
  margin-top: 10px;
  cursor: pointer;
`;

const StyledPaper = styled(Paper)`
  min-width: 140px;
`;

const StyledLink = styled(Link)`
  text-decoration: none;
  color: ${colors.black};
`;

export const EditProjectDropdown = ({ deleteProject, projectId }) => {
  const [open, setOpen] = useState(false);
  const location = useLocation();

  const { t } = useCustomTranslation();
  const handleClick = () => {
    setOpen(prev => !prev);
  };

  const handleClickAway = () => {
    setOpen(false);
  };

  return (
    <ClickAwayListener onClickAway={handleClickAway}>
      <Box>
        <StyledMoreHoriz onClick={handleClick} />
        {open ? (
          <Box position="absolute" pt={10} ml={-117}>
            <StyledPaper>
              <List>
                <ListItem button onClick={deleteProject}>
                  {t('Delete')}
                </ListItem>
                <StyledLink
                  to={{
                    pathname: `projects/${projectId}/settings`,
                    state: { prevPath: location.pathname },
                  }}
                >
                  <ListItem button>{t('Settings')}</ListItem>
                </StyledLink>
              </List>
            </StyledPaper>
          </Box>
        ) : null}
      </Box>
    </ClickAwayListener>
  );
};

EditProjectDropdown.propTypes = {
  deleteProject: PropTypes.func.isRequired,
  projectId: PropTypes.number.isRequired,
};
