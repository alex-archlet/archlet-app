import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Field, Form, reduxForm } from 'redux-form';
import { Box, Divider } from '@material-ui/core';
import { Button } from '@common/components/Button/Button';
import { colors } from '@utils/uiTheme';
import { Text } from '@common/components/Text/Text';
import { ReduxTextField } from '@common/components/ReduxForms/ReduxTextField';
import { validators } from '@utils/validators';
import { ReduxRadioButton } from '@common/components/ReduxForms/ReduxRadioButton';
import { SelectDropdown } from '@common/components/ReduxSelect/Select';
import { useCustomTranslation } from '@utils/translate';

const StyledNameField = styled(Field)`
  && {
    width: 410px;
    margin-bottom: 60px;
  }
`;

const StyledBackendError = styled(Text)`
  height: 34px;
  margin-top: 15px;
`;

export const CreateProjectFormComponent = ({
  handleSubmit,
  projectBiddingTypes,
  projectCategories,
  projectTypes,
  projectCurrencies,
  projectUnits,
  valid,
  error,
}) => {
  const { t } = useCustomTranslation();

  return (
    <Box
      display="flex"
      justifyContent="center"
      alignItems="flex-start"
      height="85vh"
      mt="15%"
    >
      <Box width={410}>
        <Form onSubmit={handleSubmit}>
          <StyledNameField
            label={t('Project Name')}
            name="name"
            type="text"
            component={ReduxTextField}
            variant="outlined"
            validate={validators.require}
          />
          <Box mb={40}>
            <Text fontWeight={500} fontSize={18}>
              {t('Bid Collection Method')}
            </Text>
            <Text color="#999" fontWeight={400}>
              {t(
                'Select Archlet Bid Importer to setup your own sheet structure. Select Manual Import if you want to use a pre-defined structure.'
              )}
            </Text>
            <Box mt={22}>
              <Field
                component={SelectDropdown}
                name="biddingTypeId"
                label={t('Select Bid Collection Method')}
                items={projectBiddingTypes}
                valueKey="id"
                labelKey="name"
                validate={validators.require}
              />
            </Box>
          </Box>
          <Box mb={40}>
            <Text fontWeight={500} fontSize={18}>
              {t('Type')}
            </Text>
            {projectTypes.map(type => (
              <Field
                key={type.id}
                name="typeId"
                component={ReduxRadioButton}
                label={type.name}
                option={type}
                field="id"
                validate={validators.require}
              />
            ))}
          </Box>
          {!!projectCategories.length && (
            <Box mb={60}>
              <Text fontWeight={500} fontSize={18}>
                {t('Category')}
              </Text>
              {projectCategories.map(category => (
                <Field
                  key={category.id}
                  name="categoryId"
                  component={ReduxRadioButton}
                  label={category.name}
                  field="id"
                  isDisabled={category.isDisabled}
                  option={category}
                  validate={validators.require}
                />
              ))}
            </Box>
          )}
          <Box mb={40}>
            <Text fontWeight={500} fontSize={18}>
              {t('Currency')}
            </Text>
            <Box mt={22}>
              <Field
                component={SelectDropdown}
                name="currencyId"
                label={t('Select Currency')}
                items={projectCurrencies}
                valueKey="id"
                labelKey="code"
                validate={validators.require}
              />
            </Box>
          </Box>
          <Box mb={40}>
            <Text fontWeight={500} fontSize={18}>
              {t('Unit of Measurement')}
            </Text>
            <Box mt={22}>
              <Field
                component={SelectDropdown}
                name="unitId"
                label={t('Select Unit of Measurement')}
                items={projectUnits}
                valueKey="id"
                labelKey="name"
                validate={validators.require}
              />
            </Box>
          </Box>
          <Box mb={40}>
            <Divider variant="middle" />
          </Box>
          <Button type="submit" color="secondary" disabled={!valid}>
            {t('Create Project')}
          </Button>
          <StyledBackendError color={colors.red}>{error}</StyledBackendError>
        </Form>
      </Box>
    </Box>
  );
};

CreateProjectFormComponent.propTypes = {
  error: PropTypes.string,
  handleSubmit: PropTypes.func.isRequired,
  projectBiddingTypes: PropTypes.arrayOf(
    PropTypes.shape({
      code: PropTypes.string.isRequired,
      createdAt: PropTypes.string.isRequired,
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      updatedAt: PropTypes.string.isRequired,
    })
  ).isRequired,
  projectCategories: PropTypes.arrayOf(
    PropTypes.shape({
      createdAt: PropTypes.string.isRequired,
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      typeId: PropTypes.number.isRequired,
      updatedAt: PropTypes.string.isRequired,
    })
  ).isRequired,
  projectCurrencies: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  projectTypes: PropTypes.arrayOf(
    PropTypes.shape({
      createdAt: PropTypes.string.isRequired,
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      updatedAt: PropTypes.string.isRequired,
    })
  ).isRequired,
  projectUnits: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  valid: PropTypes.bool.isRequired,
};

CreateProjectFormComponent.defaultProps = {
  error: '',
};

export const CreateProjectForm = reduxForm({ form: 'CreateProjectForm' })(
  CreateProjectFormComponent
);
