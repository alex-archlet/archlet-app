import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import {
  Box,
  Card,
  Typography,
} from '@material-ui/core';
import { useCustomTranslation } from '@utils/translate';
import { AddCircleOutline } from '@material-ui/icons';

const StyledCard = styled(Card)`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
`;

const StyledAddCircle = styled(AddCircleOutline)`
  && {
    margin-bottom: 10px;
    font-size: 50px;
  }
`;

const StyledLink = styled(Link)`
  text-decoration: none;
`;

export const NewProjectCard = () => {
  const { t } = useCustomTranslation();

  return (
    <StyledLink to="projects/new">
      <Box
        display="flex"
        m={10}
        width={300}
        height={300}
      >
        <StyledCard elevation={0}>
          <StyledAddCircle />
          <Typography variant="subtitle1">{t('New project')}</Typography>
        </StyledCard>
      </Box>
    </StyledLink>
  );
};
