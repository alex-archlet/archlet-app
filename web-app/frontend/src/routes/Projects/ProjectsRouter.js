import React from 'react';
import PropTypes from 'prop-types';
import {
  Route,
  Switch,
} from 'react-router-dom';

import { ProjectDetailsRouter } from '@routes/ProjectDetails/ProjectDetailsRouter';
import { DashboardContainer } from './containers/Dashboard';
import { CreateProjectContainer } from './containers/CreateProject';

export const ProjectsRouter = ({ match }) => (
  <Switch>
    <Route
      path={`${match.path}/new`}
      exact
      component={CreateProjectContainer}
    />
    <Route
      path={`${match.path}/`}
      exact
      component={DashboardContainer}
    />
    <Route
      path="/projects/:id"
      component={ProjectDetailsRouter}
    />
  </Switch>
);

ProjectsRouter.propTypes = ({
  match: PropTypes.shape({}).isRequired,
});
