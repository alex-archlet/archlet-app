import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';

import { useCustomTranslation } from '@utils/translate';
import { projectsSelectors } from '@selectors/projects';
import {
  createProject,
  getProjectBiddingTypes,
  getProjectCategories,
  getProjectCurrencies,
  getProjectTypes,
  getProjectUnits,
} from '@actions/projects';
import {
  setBackButton,
  setPageTitle,
} from '@actions/interface';
import { CreateProjectForm } from '../components/CreateProjectForm/CreateProjectForm';

const CreateProject = ({
  createProjectAction,
  getProjectBiddingTypesAction,
  getProjectCategoriesAction,
  getProjectTypesAction,
  getProjectCurrenciesAction,
  getProjectUnitsAction,
  projectCategories,
  projectBiddingTypes,
  projectTypes,
  projectCurrencies,
  projectUnits,
  setBackButtonAction,
  setPageTitleAction,
}) => {
  const { t } = useCustomTranslation();

  useEffect(() => {
    setBackButtonAction('arrow');
    setPageTitleAction(t('Create New Project'));
    getProjectTypesAction();
    getProjectCategoriesAction();
    getProjectCurrenciesAction();
    getProjectUnitsAction();
    getProjectBiddingTypesAction();

    return () => setBackButtonAction('');
  }, [t]);

  const history = useHistory();

  function onSubmitForm(values) {
    return createProjectAction({
      ...values,
      status: 'open',
    })
      .then(() => history.push('/projects'));
  }

  return (
    <div>
      <CreateProjectForm
        onSubmit={onSubmitForm}
        projectBiddingTypes={projectBiddingTypes}
        projectCategories={projectCategories}
        projectTypes={projectTypes}
        projectCurrencies={projectCurrencies}
        projectUnits={projectUnits}
      />
    </div>
  );
};

CreateProject.propTypes = ({
  createProjectAction: PropTypes.func.isRequired,
  getProjectBiddingTypesAction: PropTypes.func.isRequired,
  getProjectCategoriesAction: PropTypes.func.isRequired,
  getProjectCurrenciesAction: PropTypes.func.isRequired,
  getProjectTypesAction: PropTypes.func.isRequired,
  getProjectUnitsAction: PropTypes.func.isRequired,
  projectBiddingTypes: PropTypes.arrayOf(
    PropTypes.shape({
      code: PropTypes.string.isRequired,
      createdAt: PropTypes.string.isRequired,
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      updatedAt: PropTypes.string.isRequired,
    })
  ).isRequired,
  projectCategories: PropTypes.arrayOf(
    PropTypes.shape({
      createdAt: PropTypes.string.isRequired,
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      typeId: PropTypes.number.isRequired,
      updatedAt: PropTypes.string.isRequired,
    })
  ).isRequired,
  projectCurrencies: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  projectTypes: PropTypes.arrayOf(PropTypes.shape({
    createdAt: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    updatedAt: PropTypes.string.isRequired,
  })).isRequired,
  projectUnits: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  setBackButtonAction: PropTypes.func.isRequired,
  setPageTitleAction: PropTypes.func.isRequired,
});

const mapStateToProps = state => ({
  projectBiddingTypes: projectsSelectors.getProjectBiddingTypes(state),
  projectCategories: projectsSelectors.getCategories(state),
  projectCurrencies: projectsSelectors.getCurrencies(state),
  projectTypes: projectsSelectors.getTypes(state),
  projectUnits: projectsSelectors.getUnits(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  createProjectAction: createProject,
  getProjectBiddingTypesAction: getProjectBiddingTypes,
  getProjectCategoriesAction: getProjectCategories,
  getProjectCurrenciesAction: getProjectCurrencies,
  getProjectTypesAction: getProjectTypes,
  getProjectUnitsAction: getProjectUnits,
  setBackButtonAction: setBackButton,
  setPageTitleAction: setPageTitle,
}, dispatch);

export const CreateProjectContainer = connect(mapStateToProps, mapDispatchToProps)(CreateProject);
