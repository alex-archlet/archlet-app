import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Box } from '@material-ui/core';
import { bindActionCreators } from 'redux';

import { projectsSelectors } from '@selectors/projects';
import { userSelectors } from '@selectors/user';
import { useCustomTranslation } from '@utils/translate';
import {
  deleteProject,
  getProjects,
} from '@actions/projects';
import { setPageTitle } from '@actions/interface';

import { ProjectInfoCard } from '../components/ProjectInfoCard/ProjectInfoCard';
import { NewProjectCard } from '../components/NewProjectCard/NewProjectCard';

const Dashboard = ({
  deleteProjectAction,
  getProjectsAction,
  projects,
  setPageTitleAction,
  user,
}) => {
  const { t } = useCustomTranslation();

  useEffect(() => {
    setPageTitleAction(t('Dashboard'));
    getProjectsAction(user.id);
  }, [user.id, t]);

  return (
    <Box
      display="flex"
      flexWrap="wrap"
      pl={32}
      pr={32}
    >
      <NewProjectCard />
      { projects.map(project => (
        <ProjectInfoCard
          key={project.id}
          project={project}
          onProjectDelete={deleteProjectAction}
        />
      ))}
    </Box>
  );
};

Dashboard.propTypes = ({
  deleteProjectAction: PropTypes.func.isRequired,
  getProjectsAction: PropTypes.func.isRequired,
  projects: PropTypes.arrayOf(PropTypes.shape({
    createdAt: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    status: PropTypes.string.isRequired,
    updatedAt: PropTypes.string.isRequired,
  })).isRequired,
  setPageTitleAction: PropTypes.func.isRequired,
  user: PropTypes.shape({
    email: PropTypes.string,
    firstName: PropTypes.string,
    lastName: PropTypes.string,
  }).isRequired,
});

const mapStateToProps = state => ({
  projects: projectsSelectors.getProjects(state),
  user: userSelectors.getUser(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  deleteProjectAction: deleteProject,
  getProjectsAction: getProjects,
  setPageTitleAction: setPageTitle,
}, dispatch);

export const DashboardContainer = connect(mapStateToProps, mapDispatchToProps)(Dashboard);
