import React, { useEffect } from 'react';
import { useCustomTranslation } from '@utils/translate';
import { useDispatch } from 'react-redux';
import { setPageTitle } from '@store/actions/interface';
import { ForgotPasswordForm } from '../components/ForgotPasswordForm';

export const ForgotPassword = () => {
  const { t } = useCustomTranslation();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setPageTitle(t('Forgot password')));
  }, [setPageTitle, t]);

  return <ForgotPasswordForm />;
};
