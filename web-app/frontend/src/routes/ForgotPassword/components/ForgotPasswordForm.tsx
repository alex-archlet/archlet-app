import React from 'react';
import styled from 'styled-components';
import { Link, useHistory } from 'react-router-dom';
import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';
import { Box, Card, Typography } from '@material-ui/core';
import { colors } from '@utils/uiTheme';
import { Input } from '@common/components/Input/Input';
import { useCustomTranslation } from '@utils/translate';
import { useDispatch } from 'react-redux';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { forgotPassword } from '@store/actions/user';
import { Button } from '@common/components/Button/Button';

const StyledCard = styled(Card)`
  display: flex;
  flex-direction: column;
  width: 410px;
  height: 375px;
  padding: 40px 50px;
`;
const StyledTitle = styled(Typography)`
  && {
    font-weight: 500;
    text-align: center;
  }
`;

const StyledInput = styled(Input)`
  && {
    height: 65px;
    margin-top: 16px;
    margin-bottom: 8px;
  }
`;

const StyledLink = styled(Link)`
  display: block;
  color: ${colors.palette.secondary.main};
  font-size: 12.5px;
  text-decoration: none;
  padding-top
`;

const StyledDescription = styled.p`
  margin: 0 0 10px 0;
  color: ${colors.black};
  font-weight: 300;
  font-size: 14px;
  line-height: 24px;
  word-spacing: 0.1em;
`;

const StyledOuterContainer = styled(Box)`
  height: calc(100vh - 64px);
  background-color: #fbfbfb;
`;

interface FormValues {
  email: string;
}

export const ForgotPasswordForm = () => {
  const { t } = useCustomTranslation();
  const dispatch = useDispatch();
  const history = useHistory();

  const ReviewSchema = Yup.object().shape({
    email: Yup.string()
      .email(t('A valid e-mail is required'))
      .required(t('A valid e-mail is required')),
  });

  const onSubmitForm = async (formValues: FormValues) => {
    await dispatch(forgotPassword(formValues));

    history.push('/');
  };

  const {
    values,
    errors,
    setFieldValue,
    setFieldTouched,
    handleSubmit,
  } = useFormik<FormValues>({
    initialValues: {
      email: '',
    },
    validationSchema: ReviewSchema,
    onSubmit: onSubmitForm,
    validateOnBlur: true,
    validateOnChange: true,
  });

  const onChange = (field: string, value: string) => {
    setFieldTouched(field);
    setFieldValue(field, value);
  };

  return (
    <StyledOuterContainer
      display="flex"
      justifyContent="center"
      alignItems="center"
    >
      <StyledCard elevation={0}>
        <Box>
          <form onSubmit={handleSubmit}>
            <StyledTitle variant="h5">{t('Forgot Password')}</StyledTitle>
            <VerticalSpacer height={16} />
            <Box display="flex" flexDirection="column">
              <StyledDescription>
                {t(
                  'Please enter your email address. You will receive a new password via email.'
                )}
              </StyledDescription>
              <StyledInput
                value={values.email}
                label={t('Email')}
                type="email"
                placeholder={t('Email')}
                onChange={value => onChange('email', value)}
                error={Boolean(errors.email)}
                helperText={errors.email}
              />
              <VerticalSpacer height={8} />
              <Box display="flex" justifyContent="flex-end">
                <StyledLink
                  to={{
                    pathname: '/sign-in',
                  }}
                >
                  {t('Already have an account?')}
                </StyledLink>
              </Box>
              <VerticalSpacer height={16} />
              <Box display="flex" justifyContent="flex-end">
                <Button type="submit" color="secondary">
                  {t('Reset Password')}
                </Button>
              </Box>
            </Box>
          </form>
        </Box>
      </StyledCard>
    </StyledOuterContainer>
  );
};
