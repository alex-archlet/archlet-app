import React from 'react';
import {
  Box,
} from '@material-ui/core';

import { Text } from '@common/components/Text/Text';
import { ArrowLeft } from '@common/components/Arrow/ArrowLeft';
import { ArrowRight } from '@common/components/Arrow/ArrowRight';

const onDecrement = (prevValue, itemsLength) => (itemsLength > 1 && prevValue < itemsLength - 1 ? prevValue + 1 : 0);
const onIncrement = (prevValue, itemsLength) => (itemsLength > 1 && prevValue > 0 ? prevValue - 1 : itemsLength - 1);

interface Props {
  selectedItemsLength: number;
  setTitleIndex: (newIndex: number) => void;
  title: string;
  titleIndex: number;
}

export const ArrowTitle: React.FC<Props> = ({
  title,
  titleIndex,
  setTitleIndex,
  selectedItemsLength,
}) => (
  <Box
    display="flex"
    justifyContent="space-around"
    alignItems="center"
  >
    <ArrowLeft onClick={() => setTitleIndex(onDecrement(titleIndex, selectedItemsLength))} />
    <Text textAlign="center">{title}</Text>
    <ArrowRight onClick={() => setTitleIndex(onIncrement(titleIndex, selectedItemsLength))} />
  </Box>
);

