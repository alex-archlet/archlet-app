import React from 'react';
import styled from 'styled-components';

import { Text } from '@common/components/Text/Text';
import { colors } from '@utils/uiTheme';

export type Status = 'normal' | 'success' | 'warning';

const getColor = (status: Status) => {
  switch (status) {
    case 'warning':
      return colors.warning;
    case 'success':
      return colors.green;
    case 'normal':
    default:
      return colors.black;
  }
};

const StyledFlexContainer = styled.div`
  flex: 1;
`;

const StyledValueContainer = styled.div`
  margin-bottom: 4px;
  white-space: pre;
`;

interface Props {
  title: string;
  value: string;
  type: Status;
}

export const KpiCell: React.FC<Props> = ({ value, title, type }) => (
  <StyledFlexContainer>
    <StyledValueContainer>
      <Text color={getColor(type)} fontSize={16} fontWeight={500}>
        {value}
      </Text>
    </StyledValueContainer>
    <Text fontSize={10} color="#44566C" lineHeight={13}>
      {title}
    </Text>
  </StyledFlexContainer>
);
