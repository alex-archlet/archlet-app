import { sum } from '@utils/array';

export interface Bucket {
  id: string;
  min: number;
  max: number;
}

export interface GroupedItem {
  bucket: Bucket;
  items: any[];
}

export interface GroupedBucketMap {
  [id: string]: GroupedItem;
}

interface SupplierData {
  [itemType: string]: {
    deviations: {
      [key: string]: number;
    };
    prices: {
      [key: string]: number;
    };
  };
}

const TOTAL_PRICE = 'Total Price';

const getBestSupplierDeviation = (
  item: any,
  supplierName: string,
  itemTypes
) => {
  const values: number[] = [];

  const supplierDesigns: SupplierData | undefined =
    item.suppliers[supplierName];

  itemTypes.forEach((itemType) => {
    if (supplierDesigns && supplierDesigns[itemType]) {
      const totalPrice = supplierDesigns[itemType].deviations[TOTAL_PRICE];
      if (!Number.isNaN(totalPrice)) {
        values.push(totalPrice);
      }
    }
  });
  if (values.length === 0) {
    return null;
  }
  return Math.min(...values);
};

export const getBestSupplierPrice = (item: any, supplierName: string, itemTypes: string[]) => {
  const values: number[] = [];

  const supplierDesigns: SupplierData | undefined =
    item.suppliers[supplierName];

  itemTypes.forEach((itemType) => {
    if (supplierDesigns && supplierDesigns[itemType]) {
      const totalPrice = supplierDesigns[itemType].prices[TOTAL_PRICE];
      if (!Number.isNaN(totalPrice)) {
        values.push(totalPrice);
      }
    }
  });
  if (values.length === 0) {
    return null;
  }
  return Math.min(...values);
};

export const groupItemsToDeviationBuckets = (
  data: any[],
  supplierName: string,
  buckets: Bucket[],
  itemTypes: string[]
): GroupedBucketMap => {
  const result: GroupedBucketMap = {};

  const getBucketByDeviation = (item: any) => {
    const deviation = getBestSupplierDeviation(item, supplierName, itemTypes);
    if (!Number.isFinite(deviation) || deviation === null) {
      return null;
    }
    return buckets.find(({ min, max }) => {
      return deviation >= min && deviation < max;
    });
  };

  data.forEach((item) => {
    const bucket = getBucketByDeviation(item);
    if (bucket) {
      if (!result[bucket.id]) {
        const groupedItem: GroupedItem = {
          bucket,
          items: [],
        };
        result[bucket.id] = groupedItem;
      }
      result[bucket.id].items.push(item);
    }
  });

  return result;
};

export const getSupplierDeviationToBestPrice = (
  items: any[],
  supplierName: string,
  itemTypes: string[]
) => {
  const getItemTotalPrice = (item: any) => {
    const bestPrice = getBestSupplierPrice(item, supplierName, itemTypes);
    if (!Number.isFinite(bestPrice) || !bestPrice) {
      return 0;
    }
    const volumeCount = item.volume;
    return bestPrice * volumeCount || 0;
  };

  const getItemBestTotalPrice = (item: any) => {
    const supplierBestPrice = getBestSupplierPrice(item, supplierName, itemTypes);
    if (!Number.isFinite(supplierBestPrice) || !supplierBestPrice) {
      return 0;
    }

    const itemBestPrice = item.bestPrice[TOTAL_PRICE];
    const volumeCount = item.volume;
    return itemBestPrice * volumeCount || 0;
  };

  const totalPrice = sum(items.map(getItemTotalPrice).filter(Boolean));
  const bestPrice = sum(items.map(getItemBestTotalPrice));

  if (totalPrice === 0) {
    return null;
  }

  return {
    percentage: bestPrice ? (bestPrice - totalPrice) / bestPrice : 1,
    total: +(bestPrice - totalPrice).toFixed(2),
  };
};

export const getSupplierDeviationToBaselinePrice = (
  items: any[],
  supplierName: string,
  itemTypes: string[]
) => {
  const getItemTotalPrice = (item: any) => {
    const bestPrice = getBestSupplierPrice(item, supplierName, itemTypes);
    const volumeCount = item.volume;

    if (!Number.isFinite(bestPrice) || !bestPrice) {
      return 0;
    }
    return bestPrice * volumeCount || 0;
  };

  const getItemHistoricPrice = (item: any) => {
    const supplierBestPrice = getBestSupplierPrice(item, supplierName, itemTypes);
    if (!Number.isFinite(supplierBestPrice) || !supplierBestPrice) {
      return 0;
    }
    
    const historicPrice = item?.historicPrice[TOTAL_PRICE];
    const volumeCount = item.volume;
    return historicPrice * volumeCount || 0;
  };
  const totalPrice = sum(items.map(getItemTotalPrice).filter(Boolean));
  const historicPrice = sum(items.map(getItemHistoricPrice));

  if (Number.isNaN(historicPrice) || totalPrice === 0) {
    return null;
  }

  return {
    percentage: historicPrice
      ? (historicPrice- totalPrice) / historicPrice
      : 1,
    total: +(historicPrice - totalPrice).toFixed(2),
  };
};
