import React from 'react';
import { Cell, Pie, PieChart, ResponsiveContainer, Tooltip } from 'recharts';

import { LocaleFormatNumber } from '@common/components/FormatNumber/FormatNumber';
import { colors } from '@utils/uiTheme';
import { sum } from '@utils/array';
import { useCustomTranslation } from '@utils/translate';
import { CustomTooltip } from '@common/components/Graph/Legend/CustomTooltip';

import { getBestSupplierPrice, GroupedBucketMap } from './utils';

interface Props {
  data: GroupedBucketMap;
  supplierName: string;
  itemTypes: string[];
}

const COLORS = colors.graphs.graphComparison;

interface LabelProps {
  payload: {
    name: string;
  };
  midAngle: number;
  innerRadius: number;
  outerRadius: number;
  x: number;
  y: number;
}

// # of pixels to offset in a wished direction
const OFFSET_MULTIPLIER = 20;
// label height is 10px, let's center the label
const TEXT_HEIGHT_OFFSET = 3;
const RADIAN = Math.PI / 180;
const renderCustomizedLabel = (props: LabelProps): SVGElement => {
  const { payload, x, y, midAngle, ...rest } = props;
  const xOffset = Math.cos(-midAngle * RADIAN) * OFFSET_MULTIPLIER;
  const yOffset = Math.sin(-midAngle * RADIAN) * OFFSET_MULTIPLIER;

  const newX = x + xOffset;
  const newY = y + yOffset + TEXT_HEIGHT_OFFSET;

  // @ts-ignore
  return (
    <text fontWeight="500" fontSize="20" x={newX} y={newY} {...rest}>
      {payload.name}
    </text>
  );
};

export const BiddingAnalyticsGraph: React.FC<Props> = ({
  data,
  supplierName,
  itemTypes,
}) => {
  const { t } = useCustomTranslation();

  const preparedData = Object.values(data)
    .map((item) => {
      const supplierValues = item.items
        .map((bucketItem) => {
          const bestPrice = getBestSupplierPrice(bucketItem, supplierName, itemTypes);
          return bestPrice ? bestPrice * bucketItem.volume : null;
        })
        .filter(val => val !== null) as number[]
      return {
        name: item.bucket.id,
        count: item.items.length,
        bucket: item.bucket,
        totalSpend: sum(supplierValues),
      };
    })
    .sort((a, b) => a.bucket.min - b.bucket.min);

  return (
    <ResponsiveContainer width="100%" height={300}>
      <PieChart
        margin={{
          top: 10,
          left: 10,
          right: 10,
          bottom: 10,
        }}
      >
        <Pie
          data={preparedData}
          dataKey="totalSpend"
          nameKey="name"
          cx="50%"
          cy="50%"
          outerRadius={100}
          innerRadius={0}
          label={renderCustomizedLabel}
          labelLine
          // isAnimationActive={false}
        >
          {preparedData.map((_, index) => (
            <Cell fill={COLORS[index % COLORS.length]} />
          ))}
        </Pie>
        <Tooltip
          // @ts-ignore
          cursor={{ fill: 'transparent' }}
          content={({ payload }) => {
            if (!payload || !payload.length) {
              return null;
            }

            // @ts-ignore
            const fullObject = payload[0].payload.payload;

            const items = [
              {
                label: t('Number of items'),
                // color: barPayload.fill,
                value: fullObject.count,
              },
              {
                label: t('Total spend'),
                // color: barPayload.fill,
                value: <LocaleFormatNumber value={fullObject.totalSpend} />,
              },
            ];

            const title = `${fullObject.name} ${t('above Best Price')}`;

            return <CustomTooltip items={items} title={title} />;
          }}
        />
      </PieChart>
    </ResponsiveContainer>
  );
};
