import React from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

import { formatNumber } from '@utils/formatNumber';
import { useCustomTranslation } from '@utils/translate';
import { Text } from '@common/components/Text/Text';
import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';
import { projectsSelectors } from '@store/selectors/projects';

import { KpiCell, Status } from './KpiCell';

interface Props {
  demandedItems: number;
  offeredItems: number;
  bestPrice: {
    percentage: number;
    total: number;
  } | null;
  baselinePrice: {
    percentage: number;
    total: number;
  } | null;
}

const StyledContainer = styled.div`
  margin-left: 10px;
`;

const StyledKpiValuesContainer = styled.div`
  display: flex;
  flex-direction: row;
`;

const getStatusFromValue = (value: number): Status => {
  if (value < 0) {
    return 'warning';
  }
  if (value > 0) {
    return 'success';
  }
  return 'normal';
};

const formatPercentage = (value: number) => {
  return formatNumber(value * 100, { decimals: 1, type: 'percent', showPlusSign: true });
};

const formatTotal = (value: number) => {
  return formatNumber(value, { decimals: 0, showPlusSign: true });
};

export const BiddingAnalyticsKpis: React.FC<Props> = ({
  demandedItems,
  offeredItems,
  bestPrice,
  baselinePrice,
}) => {
  const projectCurrency = useSelector(projectsSelectors.getProjectCurrencyCode);
  const { t } = useCustomTranslation();

  const kpis = [
    {
      title: t('Quoted Items'),
      value: `${offeredItems}/${demandedItems}`,
      type: getStatusFromValue(0),
    },
    {
      title: t('to Best Price'),
      value: bestPrice && bestPrice.total ? `${formatPercentage(bestPrice.percentage)}   ${formatTotal(bestPrice.total)} ${projectCurrency}` : '-',
      type: getStatusFromValue((bestPrice?.total || 0)),
    },
  ];

  if (baselinePrice) {
    kpis.push({
      title: t('to Baseline'),
      value: baselinePrice.total ? `${formatPercentage(baselinePrice.percentage)}   ${formatTotal(baselinePrice.total)} ${projectCurrency}` : '-',
      type: getStatusFromValue(baselinePrice.total),
    });
  }

  return (
    <StyledContainer>
      <Text color="#44566C" fontSize={11}>{t('Analytics KPIs')}</Text>
      <VerticalSpacer height={14} />
      <StyledKpiValuesContainer>
        {kpis.map(kpi => (
          <KpiCell
            key={kpi.title}
            title={kpi.title}
            value={kpi.value}
            type={kpi.type}
          />
        ))}
      </StyledKpiValuesContainer>
    </StyledContainer>
  );
};
