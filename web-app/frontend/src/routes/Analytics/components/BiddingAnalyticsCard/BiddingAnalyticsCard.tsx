import React from 'react';
import styled from 'styled-components';

import { Card } from '@common/components/Card/Card';
import { useCustomTranslation } from '@utils/translate';
import { Text } from '@common/components/Text/Text';
import { Tooltip } from '@common/components/Tooltip/Tooltip';

import {
  groupItemsToDeviationBuckets,
  Bucket,
  getSupplierDeviationToBestPrice,
  getSupplierDeviationToBaselinePrice,
} from './utils';
import { BiddingAnalyticsGraph } from './BiddingAnalyticsGraph';
import { BiddingAnalyticsKpis } from './BiddingAnalyticsKpis';

interface Props {
  supplierData: any[];
  filteredHeatmap: any[];
  supplierName: string;
  itemTypes: string[];
}

const BUCKETS: Bucket[] = [
  {
    id: '0 - 5 %',
    min: 0,
    max: 0.05,
  },
  {
    id: '5 - 10 %',
    min: 0.05,
    max: 0.1,
  },
  {
    id: '10 - 20 %',
    min: 0.1,
    max: 0.2,
  },
  {
    id: '20 - 50 %',
    min: 0.2,
    max: 0.5,
  },
  {
    id: '> 50 %',
    min: 0.5,
    max: Number.MAX_VALUE,
  },
];

const StyledSubtitleContainer = styled.div`
  margin-left: 10px;
`;

const countRelevantSupplierDataByItemType = (supplierData, supplierName, itemTypes) => {
  let count = 0;
  supplierData.forEach((row) => {
    let countOffers = 0;
    itemTypes.forEach((itemType) => {
      if (
        row.suppliers[supplierName] &&
        row.suppliers[supplierName][itemType]
      ) {
        countOffers += 1
      }
    });
    if (countOffers > 0) {
      count += 1;
    }
  });
  return count;
};

export const BiddingAnalyticsCard: React.FC<Props> = ({
  supplierData,
  filteredHeatmap,
  supplierName,
  itemTypes,
}) => {
  const { t } = useCustomTranslation();

  const offerItemsCount =  countRelevantSupplierDataByItemType(supplierData, supplierName, itemTypes);
  const demandItemsCount = filteredHeatmap.length;

  const grouped = groupItemsToDeviationBuckets(
    supplierData,
    supplierName,
    BUCKETS,
    itemTypes
  );

  const bestPriceDeviation = getSupplierDeviationToBestPrice(
    supplierData,
    supplierName,
    itemTypes
  );
  const baselineDeviation = getSupplierDeviationToBaselinePrice(
    supplierData,
    supplierName,
    itemTypes
  );

  return (
    <Card
      title={supplierName && t('Bidding Analytics')}
      tooltipInfoMessage={
        supplierName
          ? `${t('Overview of the competitiveness of')} ${supplierName}`
          : undefined
      }
    >
      {supplierName ? (
        <>
          <StyledSubtitleContainer>
            <Tooltip
              title={t(
                'The price deviation is the difference from lowest bidding to highest bidding price'
              )}
            >
              <Text color="#44566C" fontSize={11}>
                {t('Deviation Break-Down')}
              </Text>
            </Tooltip>
          </StyledSubtitleContainer>
          <BiddingAnalyticsGraph
            data={grouped}
            supplierName={supplierName}
            itemTypes={itemTypes}
          />
          <BiddingAnalyticsKpis
            demandedItems={demandItemsCount}
            offeredItems={offerItemsCount}
            bestPrice={bestPriceDeviation}
            baselinePrice={baselineDeviation}
          />
        </>
      ) : null}
    </Card>
  );
};
