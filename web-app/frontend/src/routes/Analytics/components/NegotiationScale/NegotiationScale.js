import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Box } from '@material-ui/core';
import {
  ArrowLeft,
} from '@material-ui/icons';
import { Text } from '@common/components/Text/Text';
import { useCustomTranslation } from '@utils/translate';

const StyledDiv = styled.div`
  height: 200px;
  width: 20px;
  background: linear-gradient( #3296ED 0%, #F7FBFF 100%);
`;

const StyledLabelDiv = styled.div`
  white-space: pre;
`;

const StyledForBoxArrowDiv = styled.div`
  height: 200px;
  width: 30px;
  margin-top: 25px;
  position: relative;
`;

const StyledForArrowDiv = styled.div`
  position: absolute;
  bottom: ${props => props.scale - 16}px;
`;

const StyledHighLabel = Text;

const StyledLowLabel = Text;

export const NegotiationScale = ({ scale }) => {
  const { t } = useCustomTranslation();

  return (
    <Box
      display="flex"
      flexDirection="column"
      alignItems="bottom"
      mt={55}
    >
      <StyledLabelDiv>
        <Text
          fontSize={12}
          fontWeight={500}
        >
          {t(`Negotiation
Opportunity`)}
        </Text>
      </StyledLabelDiv>
      <Box
        display="flex"
        justifyContent="space-between"
        alignItems="top"
        mb={25}
      >
        <Box>
          <StyledHighLabel>{t('High')}</StyledHighLabel>
          <StyledDiv />
          <StyledLowLabel>{t('Low')}</StyledLowLabel>
        </Box>
        <StyledForBoxArrowDiv>
          <StyledForArrowDiv scale={scale}>
            <ArrowLeft />
          </StyledForArrowDiv>
        </StyledForBoxArrowDiv>
      </Box>
    </Box>
  );
};

NegotiationScale.propTypes = ({
  scale: PropTypes.number.isRequired,
});
