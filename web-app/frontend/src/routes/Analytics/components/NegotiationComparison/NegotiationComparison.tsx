/* eslint-disable multiline-ternary */
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import {
  Bar,
  Scatter,
  ComposedChart,
  CartesianGrid,
  Label,
  Legend,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts';
import styled from 'styled-components';
import { truncate } from '@utils/stringFormat';

import { projectsSelectors } from '@selectors/projects';
import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';
import { useCustomTranslation } from '@utils/translate';
import { Box } from '@material-ui/core';
import { formatNumberWithLetters } from '@utils/formatNumber';
import { colors } from '@utils/uiTheme';
import { CustomLegend } from '@common/components/Graph/Legend/CustomLegend';
import { CustomTooltip } from '@common/components/Graph/Legend/CustomTooltip';
import { LocaleFormatNumber } from '@common/components/FormatNumber/FormatNumber';
import { ArrowTitle } from '../ArrowTitle/ArrowTitle';

const StyledDescription = styled.p`
  height: 27px;
  opacity: 0.55;
  color: #2c2c2c;
  font-size: 14px;
  line-height: 19px;
  margin-left: 30px;
  margin-top: 20px;
`;

const getIndexForGraphColorPriceComponent = (
  currentPriceComponent,
  priceComponents
) => {
  if (currentPriceComponent === null) {
    return null;
  }

  const resultIndex = priceComponents.findIndex(
    priceComponent => priceComponent === currentPriceComponent
  );

  const res = resultIndex > 9 ? resultIndex % 10 : resultIndex;

  return res;
};

const generateItems = (payload, currentTooltipOptions, color) => {
  const fullObject = payload[0].payload;

  if (
    !payload ||
    !payload.length ||
    !currentTooltipOptions ||
    !currentTooltipOptions.itemType ||
    !currentTooltipOptions.key ||
    !fullObject.itemTypes[currentTooltipOptions.itemType]
  ) {
    return null;
  }

  const value =
    fullObject.itemTypes[currentTooltipOptions.itemType] &&
    fullObject.itemTypes[currentTooltipOptions.itemType][
      currentTooltipOptions.key
    ]
      ? fullObject.itemTypes[currentTooltipOptions.itemType][
          currentTooltipOptions.key
        ]
      : fullObject.itemTypes[currentTooltipOptions.itemType].secondAxis[
          currentTooltipOptions.key
        ];

  return [
    {
      color: color || currentTooltipOptions.color,
      label: currentTooltipOptions.key,
      shape: 'square',
      value: <LocaleFormatNumber value={value} />,
    },
  ];
};

const GRAPH_LINE_COLOR = '#f1f3f6';
const reverseColors = [...colors.graphs.graphComparison].reverse();
type DataWithPrice = {
  price: any;
  itemName: string;
};

interface Props {
  data: DataWithPrice[];
  itemTypes: string[];
  priceComponents: string[];
  selectedItemsLength: number;
  setTitleIndex: Function;
  titleIndex: number | null;
}

export const NegotiationComparison: React.FC<Props> = ({
  data,
  priceComponents,
  setTitleIndex,
  titleIndex,
  selectedItemsLength,
  itemTypes,
}) => {
  const projectCurrency = useSelector(projectsSelectors.getProjectCurrencyCode);
  const projectHasItemTypes = useSelector(
    projectsSelectors.getProjectHasItemTypes
  );
  const projectSecondAxis = useSelector(
    projectsSelectors.getProjectSecondAxisKeys
  ) as string[];

  const tempSecondAxis = projectSecondAxis.length ? projectSecondAxis[0] : null;
  const [currentTooltipOptions, setCurrentTooltipOptions] = useState(null);
  const { t } = useCustomTranslation();

  const getTooltipTitle = (supplierName: string) => {
    if (projectHasItemTypes && currentTooltipOptions !== null) {
      // @ts-ignore
      return `${supplierName} - ${currentTooltipOptions.itemType}`;
    }

    return supplierName;
  };

  const UNIT_PRICE_SUB_TEXT = `${t('Unit Price')} (${projectCurrency})`;

  return (
    <>
      <Box
        display="flex"
        height="325px"
        justifyContent="center"
        alignItems="center"
        mt={30}
      >
        {/* @ts-ignore */}
        {data.price && data.price.length > 0 ? (
          <Box width="100%">
            <ArrowTitle
              // @ts-ignore
              title={data.itemName}
              // @ts-ignore
              setTitleIndex={setTitleIndex}
              // @ts-ignore
              titleIndex={titleIndex}
              selectedItemsLength={selectedItemsLength}
            />
            <ResponsiveContainer width="100%" height={325}>
              <ComposedChart
                // @ts-ignore
                data={data.price}
                maxBarSize={27}
                barCategoryGap={0}
                margin={{
                  bottom: 0,
                  left: 20,
                  right: 5,
                  top: 30,
                }}
              >
                <Legend
                  verticalAlign="bottom"
                  align="left"
                  height={36}
                  payloadUniqBy
                  content={({ payload }) => (
                    <CustomLegend
                      // @ts-ignore
                      payload={payload}
                      alignment="center"
                    />
                  )}
                />
                <CartesianGrid vertical={false} stroke={GRAPH_LINE_COLOR} />
                <XAxis
                  dataKey="name"
                  xAxisId="name"
                  tickFormatter={value => truncate(value, 10)}
                  tickLine={false}
                  axisLine={false}
                  scale="band"
                  tick={{
                    fontSize: '10px',
                    width: '50px',
                  }}
                />
                {/* @ts-ignore */}
                <YAxis
                  tickFormatter={value => formatNumberWithLetters(value)}
                  axisLine={false}
                  tickLine={false}
                  orientation="left"
                  yAxisId="left"
                >
                  {/* @ts-ignore */}
                  <Label
                    value={UNIT_PRICE_SUB_TEXT}
                    // @ts-ignore
                    angle="-90"
                    dy={-10}
                    position="left"
                    style={{
                      textAnchor: 'middle',
                    }}
                  />
                </YAxis>

                {tempSecondAxis && (
                  // @ts-ignore
                  <YAxis
                    tickFormatter={value => formatNumberWithLetters(value)}
                    axisLine={false}
                    tickLine={false}
                    name={tempSecondAxis}
                    orientation="right"
                    yAxisId="right"
                  >
                    <Label
                      value={tempSecondAxis}
                      // @ts-ignore
                      angle="-90"
                      dy={-10}
                      offset={-5}
                      position="right"
                      style={{
                        textAnchor: 'middle',
                      }}
                    />
                  </YAxis>
                )}
                <Tooltip
                  // @ts-ignore
                  cursor={{ fill: 'transparent' }}
                  content={({ payload }) => {
                    if (
                      // @ts-ignore
                      payload.length === 0 ||
                      currentTooltipOptions === null ||
                      // @ts-ignore
                      !currentTooltipOptions.itemType
                    ) {
                      return null;
                    }
                    // @ts-ignore
                    const colorIndex = getIndexForGraphColorPriceComponent(
                      // @ts-ignore
                      currentTooltipOptions.key,
                      priceComponents
                    );
                    const items = generateItems(
                      payload,
                      currentTooltipOptions,
                      colors.graphs.graphComparison[colorIndex]
                    );

                    return items && colorIndex !== null ? (
                      <CustomTooltip
                        // @ts-ignore
                        items={items}
                        // @ts-ignore
                        title={getTooltipTitle(payload[0]?.payload?.name)}
                      />
                    ) : null;
                  }}
                />

                {itemTypes.map(itemType =>
                  priceComponents.map((priceComponent, index) => {
                    if (priceComponent === 'Total Price') {
                      return null;
                    }

                    return (
                      <Bar
                        dataKey={value =>
                          value.itemTypes[itemType] &&
                          value.itemTypes[itemType][priceComponent]
                            ? value.itemTypes[itemType][priceComponent]
                            : 0
                        }
                        fill={colors.graphs.graphComparison[index % 10]}
                        stroke={colors.graphs.graphComparison[index]}
                        stackId={itemType}
                        yAxisId="left"
                        barSize={20}
                        xAxisId="name"
                        name={priceComponent}
                        onMouseEnter={() => {
                          setCurrentTooltipOptions({
                            // @ts-ignore
                            itemType,
                            key: priceComponent,
                          });
                        }}
                      />
                    );
                  })
                )}
                {tempSecondAxis &&
                  itemTypes.map((itemType, index) => {
                    const color = reverseColors[index];
                    return (
                      <Scatter
                        yAxisId="right"
                        type="monotone"
                        key={itemType}
                        name={
                          projectHasItemTypes
                            ? tempSecondAxis + itemType
                            : tempSecondAxis
                        }
                        xAxisId="name"
                        onMouseEnter={() => {
                          setCurrentTooltipOptions({
                            // @ts-ignore
                            itemType,
                            key: tempSecondAxis,
                            color,
                          });
                        }}
                        // @ts-ignore
                        shape={(shapeItem) => {
                          if (!shapeItem.cx || !shapeItem.cy) {
                            return null;
                          }
                          return (
                            <circle
                              cx={shapeItem.cx}
                              cy={shapeItem.cy}
                              r={index ? 5 : 10}
                              stroke={color}
                              strokeWidth={1}
                              fill={color}
                            />
                          );
                        }}
                        dataKey={value =>
                          value.itemTypes[itemType]?.secondAxis[
                            tempSecondAxis
                          ] || null
                        }
                        fill={color}
                        stroke={color}
                      />
                    );
                  })}

                <VerticalSpacer height={5} />
                <Box display="flex" justifyContent="center">
                  <StyledDescription>
                    {t('Total price overview for selected items.')}
                  </StyledDescription>
                </Box>
              </ComposedChart>
            </ResponsiveContainer>
          </Box>
        ) : (
          <StyledDescription>
            {t('Click on the items in the graph on the left')}
          </StyledDescription>
        )}
      </Box>
    </>
  );
};
