import React from 'react';
import { TFunction } from 'i18next';

import { SORTING_TYPE } from '@common/components/Table/Table';
import { formatNumber } from '@utils/formatNumber';
import { getItemTypeForSupplier } from '@routes/Analytics/utils/filters';
import { LocaleFormatNumber } from '@common/components/FormatNumber/FormatNumber';
import {
  calculateColumnSum,
  calculateColumnSumWithFormatting,
  getCellColor,
  StyledLabel,
} from '@common/components/Table/utils';

const TOTAL_PRICE = 'Total Price';

const calculateDifference: (price1: number, price2: number) => number = (
  price1,
  price2
) => price1 - price2;

interface BaseParams {
  DEVIATION_SUB_TEXT: string;
  DEVIATION_BASE_SUB_TEXT: string;
  DEVIATION_INCU_SUB_TEXT: string;
  DIFFERENCE_BASE_SUB_TEXT: string;
  DIFFERENCE_INCU_SUB_TEXT: string;
  DIFFERENCE_SUB_TEXT: string;
  UNIT_PRICE_SUB_TEXT: string;
  projectHasHistoricPrices: boolean;
  projectHasIncumbentPrices: boolean;
  supplierName: string;
  t: TFunction;
}

interface SingleParams extends BaseParams {
  hasPercentageDifference: boolean;
  hasTotalDifference: boolean;
  hasUnitDifference: boolean;
  itemType: string;
  projectSecondAxis: string[];
}

const getSingleItemTypeColumns = ({
  DEVIATION_SUB_TEXT,
  DEVIATION_BASE_SUB_TEXT,
  DEVIATION_INCU_SUB_TEXT,
  DIFFERENCE_BASE_SUB_TEXT,
  DIFFERENCE_INCU_SUB_TEXT,
  DIFFERENCE_SUB_TEXT,
  UNIT_PRICE_SUB_TEXT,
  hasPercentageDifference,
  hasTotalDifference,
  hasUnitDifference,
  itemType,
  projectHasHistoricPrices,
  projectHasIncumbentPrices,
  projectSecondAxis,
  supplierName,
  t,
}: SingleParams) => [
  {
    Cell: ({ cell: { value } }) => <LocaleFormatNumber value={value} />,
    Footer: footer =>
      calculateColumnSumWithFormatting(footer, (row) => {
        const values = getItemTypeForSupplier(row, supplierName, itemType);
        return values ? values.total : 0;
      }),
    Header: t('Supplier Bid'),
    HeaderSub: UNIT_PRICE_SUB_TEXT,
    accessor: (value) => {
      const values = getItemTypeForSupplier(value, supplierName, itemType);

      return values && values.prices[TOTAL_PRICE];
    },
    id: `${itemType}_supplierPrice`,
    sortType: SORTING_TYPE.NUMBER,
  },
  // add second axis if there is any
  ...projectSecondAxis.map((secondAxis) => {
    return {
      Cell: ({ cell: { value } }) => <LocaleFormatNumber value={value} />,
      Footer: footer => calculateColumnSumWithFormatting(footer),
      Header: t(secondAxis),
      HeaderSub: '',
      accessor: (value) => {
        const values = getItemTypeForSupplier(value, supplierName, itemType);
        if (!values || !values.second_axis) {
          return null;
        }
        return values.second_axis[secondAxis];
      },
      id: `${itemType}_${secondAxis}`,
      sortType: SORTING_TYPE.NUMBER,
    };
  }),
  // show baseline and incumbent difference if there are any
  ...(projectHasHistoricPrices
    ? [
        // baseline differences
        ...(hasPercentageDifference
          ? [
              {
                // eslint-disable-next-line react/prop-types
                Cell: ({ cell: { value } }) => {
                  const isPositive = value > 0;

                  return (
                    <b>
                      {isPositive && '+ '}
                      {formatNumber(value * 100, { type: 'percent' })}
                    </b>
                  );
                },
                Footer: '-',
                Header: t('Deviation'),
                HeaderSub: DEVIATION_BASE_SUB_TEXT,
                accessor: (value) => {
                  const values = getItemTypeForSupplier(
                    value,
                    supplierName,
                    itemType
                  );

                  if (!values || !value?.historicPrice[TOTAL_PRICE]) {
                    return null;
                  }

                  const historicPrice = value.historicPrice[TOTAL_PRICE];
                  const currentPrice = values.prices[TOTAL_PRICE];
                  const deviation = currentPrice / historicPrice - 1;

                  return deviation;
                },
                id: `${itemType}_supplier_percent_deviation_baseline`,
                sortType: SORTING_TYPE.NUMBER,
              },
            ]
          : []),

        ...(hasUnitDifference
          ? [
              {
                Cell: ({ cell: { value } }) => {
                  return (
                    <StyledLabel color={getCellColor(value)}>
                      <LocaleFormatNumber value={value} />
                    </StyledLabel>
                  );
                },
                Footer: (footer) => {
                  const sumFooter = calculateColumnSum(footer);
                  return (
                    <StyledLabel color={getCellColor(sumFooter)}>
                      <LocaleFormatNumber value={sumFooter} />
                    </StyledLabel>
                  );
                },
                Header: t('Unit Difference'),
                HeaderSub: DIFFERENCE_BASE_SUB_TEXT,
                accessor: (value) => {
                  const values = getItemTypeForSupplier(
                    value,
                    supplierName,
                    itemType
                  );
                  if (!values || !value?.historicPrice[TOTAL_PRICE]) {
                    return null;
                  }

                  return calculateDifference(
                    value.historicPrice[TOTAL_PRICE],
                    values.prices[TOTAL_PRICE]
                  );
                },
                id: `${itemType}_unit_difference_baseline`,
                sortType: SORTING_TYPE.NUMBER,
              },
            ]
          : []),
        ...(hasTotalDifference
          ? [
              {
                Cell: ({ cell: { value } }) => {
                  return (
                    <StyledLabel color={getCellColor(value)}>
                      <LocaleFormatNumber value={value} />
                    </StyledLabel>
                  );
                },
                Footer: (footer) => {
                  const sumFooter = calculateColumnSum(footer);
                  return (
                    <StyledLabel color={getCellColor(sumFooter)}>
                      <LocaleFormatNumber value={sumFooter} />
                    </StyledLabel>
                  );
                },
                Header: t('Spend Difference'),
                HeaderSub: DIFFERENCE_BASE_SUB_TEXT,
                accessor: (value) => {
                  const values = getItemTypeForSupplier(
                    value,
                    supplierName,
                    itemType
                  );
                  if (!values || !value?.historicPrice[TOTAL_PRICE]) {
                    return null;
                  }

                  return (
                    value.volume *
                    calculateDifference(
                      value.historicPrice[TOTAL_PRICE],
                      values.prices[TOTAL_PRICE]
                    )
                  );
                },
                id: `${itemType}_difference_baseline`,
                sortType: SORTING_TYPE.NUMBER,
              },
            ]
          : []),
      ]
    : []),
  ...(projectHasIncumbentPrices
    ? [
        // incumbent
        ...(hasPercentageDifference
          ? [
              {
                // eslint-disable-next-line react/prop-types
                Cell: ({ cell: { value } }) => {
                  const isPositive = value > 0;

                  return (
                    <b>
                      {isPositive && '+ '}
                      {formatNumber(value * 100, { type: 'percent' })}
                    </b>
                  );
                },
                Footer: '-',
                Header: t('Deviation'),
                HeaderSub: DEVIATION_INCU_SUB_TEXT,
                accessor: (value) => {
                  const values = getItemTypeForSupplier(
                    value,
                    supplierName,
                    itemType
                  );

                  if (!values || !value?.incumbentPrice) {
                    return null;
                  }

                  const incumbentPrice = value.incumbentPrice[TOTAL_PRICE];
                  const currentPrice = values.prices[TOTAL_PRICE];
                  const deviation = currentPrice / incumbentPrice - 1;

                  return deviation;
                },
                id: `${itemType}_supplier_percent_deviation_incumbent`,
                sortType: SORTING_TYPE.NUMBER,
              },
            ]
          : []),
        ...(hasUnitDifference
          ? [
              {
                Cell: ({ cell: { value } }) => {
                  return (
                    <StyledLabel color={getCellColor(value)}>
                      <LocaleFormatNumber value={value} />
                    </StyledLabel>
                  );
                },
                Footer: (footer) => {
                  const sumFooter = calculateColumnSum(footer);
                  return (
                    <StyledLabel color={getCellColor(sumFooter)}>
                      <LocaleFormatNumber value={sumFooter} />
                    </StyledLabel>
                  );
                },
                Header: t('Unit Difference'),
                HeaderSub: DIFFERENCE_INCU_SUB_TEXT,
                accessor: (value) => {
                  const values = getItemTypeForSupplier(
                    value,
                    supplierName,
                    itemType
                  );

                  if (!values || !value?.incumbentPrice) {
                    return null;
                  }

                  return calculateDifference(
                    value.incumbentPrice[TOTAL_PRICE],
                    values.prices[TOTAL_PRICE]
                  );
                },
                id: `${itemType}_unit_difference_incumbent`,
                sortType: SORTING_TYPE.NUMBER,
              },
            ]
          : []),
        ...(hasTotalDifference
          ? [
              {
                Cell: ({ cell: { value } }) => {
                  return (
                    <StyledLabel color={getCellColor(value)}>
                      <LocaleFormatNumber value={value} />
                    </StyledLabel>
                  );
                },
                Footer: (footer) => {
                  const sumFooter = calculateColumnSum(footer);
                  return (
                    <StyledLabel color={getCellColor(sumFooter)}>
                      <LocaleFormatNumber value={sumFooter} />
                    </StyledLabel>
                  );
                },
                Header: t('Spend Difference'),
                HeaderSub: DIFFERENCE_INCU_SUB_TEXT,
                accessor: (value) => {
                  const values = getItemTypeForSupplier(
                    value,
                    supplierName,
                    itemType
                  );

                  if (!values || !value?.incumbentPrice) {
                    return null;
                  }
                  return (
                    value.volume *
                    calculateDifference(
                      value.incumbentPrice[TOTAL_PRICE],
                      values.prices[TOTAL_PRICE]
                    )
                  );
                },
                id: `${itemType}_difference_incumbent`,
                sortType: SORTING_TYPE.NUMBER,
              },
            ]
          : []),
      ]
    : []),
  // show differences to best price
  ...(hasPercentageDifference
    ? [
        {
          // eslint-disable-next-line react/prop-types
          Cell: ({ cell: { value } }) => {
            const isPositive = value > 0;

            return (
              <b>
                {isPositive && '+ '}
                {formatNumber(value * 100, { type: 'percent' })}
              </b>
            );
          },
          Footer: '-',
          Header: t('Deviation'),
          HeaderSub: DEVIATION_SUB_TEXT,
          accessor: (value) => {
            const values = getItemTypeForSupplier(
              value,
              supplierName,
              itemType
            );

            if (!values) {
              return null;
            }

            const bestUnitPrice = value.bestPrice[TOTAL_PRICE];
            const currentPrice = values.prices[TOTAL_PRICE];
            const deviation = currentPrice / bestUnitPrice - 1;

            return deviation;
          },
          id: `${itemType}_supplier_percent_deviation_best`,
          sortType: SORTING_TYPE.NUMBER,
        },
      ]
    : []),
  ...(hasUnitDifference
    ? [
        {
          Cell: ({ cell: { value } }) => {
            return (
              <StyledLabel color={getCellColor(value)}>
                <LocaleFormatNumber value={value} />
              </StyledLabel>
            );
          },
          Footer: (footer) => {
            const sumFooter = calculateColumnSum(footer);
            return (
              <StyledLabel color={getCellColor(sumFooter)}>
                <LocaleFormatNumber value={sumFooter} />
              </StyledLabel>
            );
          },
          Header: t('Unit Difference'),
          HeaderSub: DIFFERENCE_SUB_TEXT,
          accessor: (value) => {
            const values = getItemTypeForSupplier(
              value,
              supplierName,
              itemType
            );

            if (!values) {
              return null;
            }

            return calculateDifference(
              value.bestPrice[TOTAL_PRICE],
              values.prices[TOTAL_PRICE]
            );
          },
          id: `${itemType}_unit_difference_best`,
          sortType: SORTING_TYPE.NUMBER,
        },
      ]
    : []),
  ...(hasTotalDifference
    ? [
        {
          Cell: ({ cell: { value } }) => {
            return (
              <StyledLabel color={getCellColor(value)}>
                <LocaleFormatNumber value={value} />
              </StyledLabel>
            );
          },
          Footer: (footer) => {
            const sumFooter = calculateColumnSum(footer);
            return (
              <StyledLabel color={getCellColor(sumFooter)}>
                <LocaleFormatNumber value={sumFooter} />
              </StyledLabel>
            );
          },
          Header: t('Spend Difference'),
          HeaderSub: DIFFERENCE_SUB_TEXT,
          accessor: (value) => {
            const values = getItemTypeForSupplier(
              value,
              supplierName,
              itemType
            );

            if (!values) {
              return null;
            }

            return (
              value.volume *
              calculateDifference(
                value.bestPrice[TOTAL_PRICE],
                values.prices[TOTAL_PRICE]
              )
            );
          },
          id: `${itemType}_spend_difference_best`,
          sortType: SORTING_TYPE.NUMBER,
        },
      ]
    : []),
];

interface MultiParams extends BaseParams {
  projectHasItemTypes: boolean;
  selectedItemTypes: string[];
  projectSecondAxis: string[];
  projectSettings: {
    analytics_percentage_difference: boolean;
    analytics_total_difference: boolean;
    analytics_unit_difference: boolean;
  };
}

export const getItemTypeColumns = ({
  DEVIATION_SUB_TEXT,
  DEVIATION_BASE_SUB_TEXT,
  DEVIATION_INCU_SUB_TEXT,
  DIFFERENCE_BASE_SUB_TEXT,
  DIFFERENCE_INCU_SUB_TEXT,
  DIFFERENCE_SUB_TEXT,
  UNIT_PRICE_SUB_TEXT,
  projectHasHistoricPrices,
  projectHasIncumbentPrices,
  projectHasItemTypes,
  projectSecondAxis,
  projectSettings,
  selectedItemTypes,
  supplierName,
  t,
}: MultiParams) => {
  // get settings for analytics and set default settings
  const hasPercentageDifference =
    projectSettings && projectSettings.analytics_percentage_difference
      ? projectSettings.analytics_percentage_difference
      : false;
  const hasTotalDifference =
    projectSettings && projectSettings.analytics_total_difference
      ? projectSettings.analytics_total_difference
      : true;
  const hasUnitDifference =
    projectSettings && projectSettings.analytics_unit_difference
      ? projectSettings.analytics_unit_difference
      : false;
  if (projectHasItemTypes) {
    return selectedItemTypes.map(itemType => ({
      Header: itemType,
      columns: getSingleItemTypeColumns({
        DEVIATION_SUB_TEXT,
        DEVIATION_BASE_SUB_TEXT,
        DEVIATION_INCU_SUB_TEXT,
        DIFFERENCE_BASE_SUB_TEXT,
        DIFFERENCE_INCU_SUB_TEXT,
        DIFFERENCE_SUB_TEXT,
        UNIT_PRICE_SUB_TEXT,
        hasPercentageDifference,
        hasTotalDifference,
        hasUnitDifference,
        itemType,
        projectHasHistoricPrices,
        projectHasIncumbentPrices,
        projectSecondAxis,
        supplierName,
        t,
      }),
      id: itemType,
    }));
  }

  return getSingleItemTypeColumns({
    DEVIATION_SUB_TEXT,
    DEVIATION_BASE_SUB_TEXT,
    DEVIATION_INCU_SUB_TEXT,
    DIFFERENCE_BASE_SUB_TEXT,
    DIFFERENCE_INCU_SUB_TEXT,
    DIFFERENCE_SUB_TEXT,
    UNIT_PRICE_SUB_TEXT,
    hasPercentageDifference,
    hasTotalDifference,
    hasUnitDifference,
    itemType: selectedItemTypes[0],
    projectHasHistoricPrices,
    projectHasIncumbentPrices,
    projectSecondAxis,
    supplierName,
    t,
  });
};
