import React from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

import { projectsSelectors } from '@selectors/projects';
import { SORTING_TYPE, Table } from '@common/components/Table/Table';
import { useCustomTranslation } from '@utils/translate';
import { truncate } from '@utils/stringFormat';
import { LocaleFormatNumber } from '@common/components/FormatNumber/FormatNumber';
import { calculateColumnSumWithFormatting } from '@common/components/Table/utils';

import { getItemTypeColumns } from './utils';

const DEFAULT_SORTING = [
  {
    desc: false,
    id: 'item',
  },
];

const TOTAL_PRICE = 'Total Price';

const StyledContainer = styled.div`
  width: 100%;
  overflow-x: auto;
  white-space: nowrap;
`;

interface Props {
  data: any[];
  projectCurrency: string;
  projectHasHistoricPrices: boolean;
  projectHasIncumbentPrices: boolean;
  projectHasItemTypes: boolean;
  projectUnit: string;
  selectedItemTypes: string[];
  projectSecondAxis: string[];
  supplierName: string;
}

export const SupplierNegotiationTable: React.FC<Props> = ({
  data,
  projectCurrency,
  projectUnit,
  selectedItemTypes,
  supplierName,
  projectHasHistoricPrices,
  projectHasIncumbentPrices,
  projectHasItemTypes,
  projectSecondAxis,
}) => {
  const { t } = useCustomTranslation();
  const projectSettings = useSelector(projectsSelectors.getSettings);

  const UNIT_PRICE_SUB_TEXT = `${t('Unit Price')} (${projectCurrency})`;
  const DIFFERENCE_SUB_TEXT = `${projectCurrency} ${t('to Best Price')} `;
  const DEVIATION_SUB_TEXT = `% ${t('from Best Price')}`;
  const DEVIATION_INCU_SUB_TEXT = `% ${t('from Incumbent Price')}`;
  const DEVIATION_BASE_SUB_TEXT = `% ${t('from Baseline Price')}`;
  const DIFFERENCE_INCU_SUB_TEXT = `${projectCurrency} ${t(
    'to Incumbent Price'
  )} `;
  const DIFFERENCE_BASE_SUB_TEXT = `${projectCurrency} ${t(
    'to Baseline Price'
  )} `;
  const INCUMBENT_SUPPLIER = t('Incumbent Supplier');
  const LOWEST_BIDDERS = t('Lowest Bidder');
  const secondAxisName = projectSecondAxis.length ? projectSecondAxis[0] : null;

  return (
    <StyledContainer>
      <Table
        data={data}
        columns={[
          {
            Cell: ({ cell: { value } }) => truncate(value, 30),
            Footer: t('{{count}} Item', { count: data.length }),
            Header: t('Item'),
            accessor: 'item',
            id: 'item',
            fixed: true,
            tooltip: true,
          },
          {
            Cell: ({ cell: { value, row } }) =>
              // eslint-disable-next-line no-underscore-dangle
              row.original._depth === 0 && <LocaleFormatNumber value={value} />,
            Footer: footer => calculateColumnSumWithFormatting(footer),
            Header: t('Volume'),
            HeaderSub: `(${projectUnit})`,
            accessor: value => value.volume,
            id: 'volume',
            sortType: SORTING_TYPE.NUMBER,
          },
          {
            Header: '',
            id: 'group 1',
            columns: [
              ...(projectHasHistoricPrices
                ? [
                    {
                      Cell: ({ cell: { value } }) => (
                        <LocaleFormatNumber value={value} />
                      ),
                      Footer: footer =>
                        calculateColumnSumWithFormatting(
                          footer,
                          row => row.historic
                        ),
                      Header: t('Baseline Price'),
                      HeaderSub: UNIT_PRICE_SUB_TEXT,
                      accessor: value => value.historicPrice[TOTAL_PRICE],
                      id: 'historicPrice',
                      sortType: SORTING_TYPE.NUMBER,
                      tooltip: ({ cell }) =>
                        `${INCUMBENT_SUPPLIER}: ${cell.row.original.historicSupplier}`,
                    },
                  ]
                : []),
              ...(projectHasIncumbentPrices
                ? [
                    {
                      Footer: footer =>
                        calculateColumnSumWithFormatting(
                          footer,
                          row => row.incumbentTotal
                        ),
                      Cell: ({ cell: { value } }) => (
                        <LocaleFormatNumber value={value} />
                      ),
                      Header: t('Incumbent Price'),
                      HeaderSub: UNIT_PRICE_SUB_TEXT,
                      accessor: value =>
                        value.incumbentPrice &&
                        value.incumbentPrice[TOTAL_PRICE],
                      id: 'incumbentPrice',
                      sortType: SORTING_TYPE.NUMBER,
                      tooltip: ({ cell }) =>
                        `${INCUMBENT_SUPPLIER}: ${cell.row.original.historicSupplier}`,
                    },
                  ]
                : []),
              {
                Cell: ({ cell: { value } }) => (
                  <LocaleFormatNumber value={value} />
                ),
                Footer: footer =>
                  calculateColumnSumWithFormatting(footer, row => row.total),
                Header: t('Best Price'),
                HeaderSub: UNIT_PRICE_SUB_TEXT,
                accessor: (value) => {
                  return value.bestPrice[TOTAL_PRICE];
                },
                id: 'bestPrice',
                sortType: SORTING_TYPE.NUMBER,
                tooltip: ({ cell }) =>
                  `${LOWEST_BIDDERS}: ${cell.row.original.bestPriceSupplier}`,
              },
            ],
          },
          ...(secondAxisName
            ? [
                {
                  Cell: ({ cell: { value } }) => (
                    <LocaleFormatNumber value={value} />
                  ),
                  Footer: footer => calculateColumnSumWithFormatting(footer),
                  Header: secondAxisName,
                  HeaderSub: t('Baseline'),
                  accessor: value =>
                    value.historicSecondAxis &&
                    value.historicSecondAxis[secondAxisName],
                  id: 'tempSecondAxis',
                  sortType: SORTING_TYPE.NUMBER,
                },
              ]
            : []),
          {
            Header: '',
            id: 'group 2',
            columns: [
              ...getItemTypeColumns({
                DEVIATION_SUB_TEXT,
                DEVIATION_BASE_SUB_TEXT,
                DEVIATION_INCU_SUB_TEXT,
                DIFFERENCE_BASE_SUB_TEXT,
                DIFFERENCE_INCU_SUB_TEXT,
                DIFFERENCE_SUB_TEXT,
                UNIT_PRICE_SUB_TEXT,
                projectHasHistoricPrices,
                projectHasIncumbentPrices,
                projectHasItemTypes,
                projectSecondAxis,
                projectSettings,
                selectedItemTypes,
                supplierName,
                t,
              }),
            ],
          },
        ]}
        defaultSorting={DEFAULT_SORTING}
        maxHeight="60vh"
        stickyHeader
        expandable
      />
    </StyledContainer>
  );
};
