interface MapOfValues {
  [key: string]: any[];
}

export const calculateCountsOfValuesForItemTypes = (
  map: MapOfValues,
  supplierName: string,
  volumeKey: string,
  itemTypes: string[]
) => {
  const result = {};

  Object.keys(map).forEach((key) => {
    const items = map[key];
    itemTypes.forEach((itemType) => {
      const withVolume = items
        .map(
          item =>
            item.suppliers[supplierName] &&
            item.suppliers[supplierName][itemType] &&
            item.suppliers[supplierName][itemType].volumes[volumeKey]
        )
        .filter(Boolean);
      if (!result[key] && withVolume.length) {
        result[key] = {};
      }
      if (withVolume.length) {
        result[key][itemType] = withVolume.length;
      }
    });
  });

  return result;
};

export const calculateCountsOfValuesForAnyItemType = (
  map: MapOfValues,
  supplierName: string,
  volumeKey: string
) => {
  const result = {};

  Object.keys(map).forEach((key) => {
    const items = map[key];
    const withVolume = items
      .map((item) => {
        const supplierTypes = item.suppliers[supplierName];
        return Object.keys(supplierTypes).find(itemType =>
          supplierTypes[itemType] &&
          supplierTypes[itemType].volumes[volumeKey]
        );
      })
      .filter(Boolean);

      if (withVolume.length) {
        result[key] = withVolume.length;
      }
  });

  return result;
};

export const calculateCountsOfValues = (map: MapOfValues) => {
  const result = {};

  Object.keys(map).forEach((key) => {
    const items = map[key];
    result[key] = items.length;
  });

  return result;
};
