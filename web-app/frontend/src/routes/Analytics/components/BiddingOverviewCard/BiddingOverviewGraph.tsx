import React from 'react';
import {
  Bar,
  BarChart,
  CartesianGrid,
  Label,
  Legend,
  Tooltip,
  XAxis,
  YAxis,
  ResponsiveContainer,
} from 'recharts';

import { projectsSelectors } from '@selectors/projects';
import { formatNumberWithLetters, formatNumber } from '@utils/formatNumber';
import { truncate } from '@utils/stringFormat';
import { useCustomTranslation } from '@utils/translate';
import { colors } from '@utils/uiTheme';
import { CustomLegend } from '@common/components/Graph/Legend/CustomLegend';
import { CustomTooltip } from '@common/components/Graph/Legend/CustomTooltip';
import { useSelector } from 'react-redux';
import { LocaleFormatNumber } from '@common/components/FormatNumber/FormatNumber';
import { getEmptyArrayIfConditionFalse } from '@utils/array';

const GRAPH_LINE_COLOR = '#f1f3f6';
const BAR_WIDTH = 27;

// reduce value length so that all labels are shown
const formatXAxis = (value: string) => truncate(value, 12);

const getColor = (index: number) => colors.graphs.graphComparison[index % 10];

interface SupplierItemTypeCounts {
  [itemType: string]: number;
}

interface Data {
  name: string;
  demandItemsCount: number;
  offerItemsCount: number;
  supplierItemsCounts: SupplierItemTypeCounts;
}

interface Props {
  data: Data[];
  supplierName: string;
  selectedItemTypes: string[];
}

const itemTypeSelector = (itemType: string) => (data: Data) =>
  data.supplierItemsCounts[itemType] || 0;

export const BiddingOverviewGraph: React.FC<Props> = ({
  data,
  supplierName,
  selectedItemTypes,
}) => {
  const hasItemTypes = useSelector(projectsSelectors.getProjectHasItemTypes);
  const { t } = useCustomTranslation();

  const getSupplierName = (itemType: string) => {
    if (hasItemTypes) {
      return `${itemType} - ${supplierName} `;
    }
    return supplierName;
  };

  const getTooltipContent = (payload) => {
    const fullObject = payload[0].payload;
    const itemTypeValues = selectedItemTypes.map(itemType => ({
      label: `${t('Item Coverage')} - ${getSupplierName(itemType)}`,
      value: formatNumber(
        (fullObject.supplierItemsCounts[itemType] /
          fullObject.demandItemsCount) *
          100,
        { type: 'percent', decimals: 0 }
      ),
    }));
    const items = [
      {
        label: t('Number of demanded Items'),
        value: <LocaleFormatNumber value={fullObject.demandItemsCount} />,
      },
      ...getEmptyArrayIfConditionFalse(
        [
          {
            label: t('Overall Item Coverage'),
            value: formatNumber(
              (fullObject.offerItemsCount /
                fullObject.demandItemsCount) *
                100,
              { type: 'percent', decimals: 0 }
            ),
          },
        ],
        hasItemTypes
      ),
      ...itemTypeValues,
    ];
    return {
      items,
      title: fullObject.name,
    };
  };

  const getLegendPayload = () => {
    const result = selectedItemTypes.map((itemType, index) => ({
      color: getColor(index),
      id: `supplier_${itemType}`,
      value: getSupplierName(itemType),
    }));

    return [
      {
        color: getColor(selectedItemTypes.length),
        id: 'all',
        value: t('All demands'),
      },
      ...getEmptyArrayIfConditionFalse(
        [
          {
            color: getColor(selectedItemTypes.length + 1),
            id: 'offered',
            value: `${t('Overall')} ${supplierName}`,
          },
        ],
        hasItemTypes
      ),
      ...result,
    ];
  };

  return (
    <ResponsiveContainer width="100%" height={325}>
      <BarChart
        data={data}
        margin={{
          bottom: 5,
          left: 20,
          right: 30,
          top: 5,
        }}
        barGap={0}
        barSize={BAR_WIDTH}
      >
        <CartesianGrid vertical={false} stroke={GRAPH_LINE_COLOR} />
        <XAxis
          dataKey="name"
          tickLine={false}
          axisLine={false}
          tick={{
            fill: '#000000',
            fontFamily: 'sans-serif',
            fontSize: 11,
            opacity: 0.7,
          }}
          tickFormatter={formatXAxis}
          // @ts-ignore
          style={{
            transform: 'translate(0, 6px)',
          }}
        />
        {/* @ts-ignore */}
        <YAxis
          tick={{
            fill: '#000000',
            fontFamily: 'sans-serif',
            fontSize: 11,
            opacity: 0.7,
          }}
          tickFormatter={val => formatNumberWithLetters(val, { decimals: 0 })}
          tickCount={5}
          tickLine={false}
          axisLine={false}
          stroke={GRAPH_LINE_COLOR}
        >
          <Label
            value={`${t('Number of bids')}`}
            // @ts-ignore
            angle="-90"
            offset={0}
            position="insideLeft"
            style={{
              textAnchor: 'middle',
            }}
          />
        </YAxis>
        <Tooltip
          // @ts-ignore
          cursor={{ fill: 'transparent' }}
          content={({ payload }) =>
            payload &&
            payload.length && <CustomTooltip {...getTooltipContent(payload)} />
          }
        />
        <Legend
          content={({ payload }) => (
            // @ts-ignore
            <CustomLegend payload={payload} alignment="right" />
          )}
          // @ts-ignore
          payload={getLegendPayload()}
          align="right"
          verticalAlign="top"
          wrapperStyle={{
            fontSize: 11,
            paddingBottom: 12,
          }}
        />
        <Bar
          dataKey="demandItemsCount"
          fill={getColor(selectedItemTypes.length)}
        />
        {hasItemTypes && (
          <Bar
            dataKey="offerItemsCount"
            fill={getColor(selectedItemTypes.length + 1)}
          />
        )}
        {selectedItemTypes.map((itemType, index) => (
          <Bar
            key={itemType}
            dataKey={itemTypeSelector(itemType)}
            fill={getColor(index)}
          />
        ))}
      </BarChart>
    </ResponsiveContainer>
  );
};
