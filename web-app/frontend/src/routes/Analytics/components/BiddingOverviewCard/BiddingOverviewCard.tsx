import React from 'react';
import { useParams } from 'react-router';
import { useSelector } from 'react-redux';

import { projectsSelectors } from '@store/selectors/projects';
import { Card } from '@common/components/Card/Card';
import { useCustomTranslation } from '@utils/translate';
import { Filter } from '@common/components/Filter/Filter';
import { useFilter } from '@common/components/Filter/filterHooks';
import { TYPE } from '@common/components/Filter/FilterDropdown/FilterDropdown';
import { SingleFilterValue } from '@common/components/Filter/types';
import { groupByCallback } from '@utils/array';
import {
  calculateCountsOfValues,
  calculateCountsOfValuesForAnyItemType,
  calculateCountsOfValuesForItemTypes,
} from './utils';
import { BiddingOverviewGraph } from './BiddingOverviewGraph';

interface Props {
  supplierData: any[];
  filteredHeatmap: any[];
  volume: string;
  itemTypes: string[];
  supplierName: string;
}

type Params = {
  id: string;
};

export const BiddingOverviewCard: React.FC<Props> = ({
  supplierData,
  filteredHeatmap,
  volume,
  itemTypes,
  supplierName,
}) => {
  const { id } = useParams<Params>();
  const { t } = useCustomTranslation();
  const projectFilters: any[] = useSelector(
    projectsSelectors.getProjectFilters
  );

  const projectFilterColumns = projectFilters?.map(({ name }) => name) ?? [];
  const options =
    projectFilterColumns?.map(name => ({
      label: name,
      value: name,
    })) ?? [];
  const filterDeclaration = [
    {
      id: 'group',
      clearable: false,
      label: t('Group by'),
      options,
      type: TYPE.SINGLE,
    },
  ];

  const { filterState, filterProps, setFilterState } = useFilter({
    filterDeclaration,
    initialValuesMap: {
      group: projectFilterColumns,
    },
    reduxFilterId: `${id}_bidding_overview`,
  });

  const selectedFilter = (filterState?.group as SingleFilterValue)?.value;

  const supplierOffersGroupedByFilter = selectedFilter
    ? groupByCallback(supplierData, row => row.filters[selectedFilter])
    : {};
  const demandsGroupedByFilter = selectedFilter
    ? groupByCallback(filteredHeatmap, row => row.filters[selectedFilter])
    : {};

  const demandGroupedCounts = calculateCountsOfValues(demandsGroupedByFilter);
  const supplierTotalCounts = calculateCountsOfValuesForAnyItemType(
    supplierOffersGroupedByFilter,
    supplierName,
    volume,
  );
  const supplierItemTypesCounts = calculateCountsOfValuesForItemTypes(
    supplierOffersGroupedByFilter,
    supplierName,
    volume,
    itemTypes
  );

  const allKeysForGroupBy = Object.keys(demandsGroupedByFilter);

  const data = allKeysForGroupBy.map(key => ({
    name: key,
    demandItemsCount: demandGroupedCounts[key] ?? 0,
    offerItemsCount: supplierTotalCounts[key] ?? 0,
    supplierItemsCounts: supplierItemTypesCounts[key] ?? {},
  })).filter(({ supplierItemsCounts }) => Object.keys(supplierItemsCounts).length > 0);

  return (
    <Card
      title={supplierName && t('Bidding Overview')}
      tooltipInfoMessage={
        supplierName
          ? `${t('Overview of the bidding characteristics of')} ${supplierName}`
          : undefined
      }
      rightSide={
        supplierName && <Filter {...filterProps} onChange={setFilterState} />
      }
    >
      {supplierName ? (
        <BiddingOverviewGraph
          data={data}
          selectedItemTypes={itemTypes}
          supplierName={supplierName}
        />
      ) : null}
    </Card>
  );
};
