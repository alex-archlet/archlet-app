import React, { useMemo } from 'react';
import styled from 'styled-components';

import { useSelector } from 'react-redux';
import { projectsSelectors } from '@store/selectors/projects';
import { useCustomTranslation } from '@utils/translate';
import { SORTING_TYPE, Table } from '@common/components/Table/Table';
import { LocaleFormatNumber } from '@common/components/FormatNumber/FormatNumber';
import { calculateColumnSumWithFormatting } from '@common/components/Table/utils';
import { truncate } from '@utils/stringFormat';
import { getEmptyArrayIfConditionFalse } from '@utils/array';
import { getSupplierPrice } from './utils';

const TOTAL_PRICE = 'Total Price';

const DEFAULT_SORTING = [
  {
    desc: false,
    id: 'item',
  },
];

const StyledContainer = styled.div`
  width: 100%;
  overflow-x: auto;
  white-space: nowrap;
`;

interface Props {
  data: object[];
  itemTypes: string[];
  projectCurrency: string;
  projectHasHistoricPrices: boolean;
  projectHasIncumbentPrices: boolean;
  projectHasItemTypes: boolean;
  projectSuppliers: string[];
  projectUnit: string;
}

export const ItemsTable: React.FC<Props> = ({
  data,
  projectHasHistoricPrices,
  projectHasIncumbentPrices,
  projectHasItemTypes,
  projectSuppliers,
  projectCurrency,
  projectUnit,
  itemTypes,
}) => {
  const { t } = useCustomTranslation();
  const projectSecondAxis = useSelector(
    projectsSelectors.getProjectSecondAxisKeys
  ) as string[];

  const tempSecondAxis = projectSecondAxis.length ? projectSecondAxis[0] : null;

  const UNIT_PRICE_SUB_TEXT = `${t('Unit Price')} (${projectCurrency})`;

  const getSuppliersWithData = (items) => {
    const allSuppliers = {};

    items.forEach((item) => {
      const { suppliers } = item;
      const supplierNames = Object.keys(suppliers);

      supplierNames.forEach((supplierName) => {
        allSuppliers[supplierName] = true;
      });
    });

    return allSuppliers;
  };

  const isSupplierInData = (supplier, supplierNameMap) =>
    supplierNameMap[supplier];

  const supplierNameMap = data.length ? getSuppliersWithData(data) : null;
  const projectSuppliersInData = supplierNameMap
    ? projectSuppliers.filter(s => isSupplierInData(s, supplierNameMap))
    : projectSuppliers;

  const INCUMBENT_SUPPLIER = t('Incumbent Supplier');
  const LOWEST_BIDDERS = t('Lowest Bidder');

  const columns = useMemo(
    () => [
      {
        Footer: t('{{count}} Item', { count: data.length }),
        Cell: ({ cell: { value } }) => truncate(value, 30),
        Header: t('Item'),
        accessor: 'item',
        fixed: true,
        id: 'item',
        tooltip: ({ cell: { value } }) => value,
      },
      {
        Footer: footer => calculateColumnSumWithFormatting(footer),
        Cell: ({ cell: { value, row } }) =>
          // eslint-disable-next-line no-underscore-dangle
          row.original._depth === 0 && <LocaleFormatNumber value={value} />,
        Header: t('Volume'),
        HeaderSub: `(${projectUnit})`,
        accessor: value => value.volume,
        id: 'volume',
        sortType: SORTING_TYPE.NUMBER,
      },
      {
        Header: '',
        id: 'group 1',
        columns: [
          ...getEmptyArrayIfConditionFalse(
            [
              {
                Footer: footer => calculateColumnSumWithFormatting(footer),
                Cell: ({ cell: { value } }) => (
                  <LocaleFormatNumber value={value} />
                ),
                Header: t('Baseline Price'),
                HeaderSub: UNIT_PRICE_SUB_TEXT,
                accessor: value => value.historicPrice[TOTAL_PRICE],
                id: 'historicPrice',
                sortType: SORTING_TYPE.NUMBER,
                tooltip: ({ cell }) =>
                  `${INCUMBENT_SUPPLIER}: ${cell.row.original.historicSupplier}`,
              },
            ],
            projectHasHistoricPrices
          ),
          ...getEmptyArrayIfConditionFalse(
            [
              {
                Footer: footer => calculateColumnSumWithFormatting(footer),
                Cell: ({ cell: { value } }) => (
                  <LocaleFormatNumber value={value} />
                ),
                Header: t('Incumbent Price'),
                HeaderSub: UNIT_PRICE_SUB_TEXT,
                accessor: value =>
                  value.incumbentPrice && value.incumbentPrice[TOTAL_PRICE],
                id: 'incumbentPrice',
                sortType: SORTING_TYPE.NUMBER,
                tooltip: ({ cell }) =>
                  `${INCUMBENT_SUPPLIER}: ${cell.row.original.historicSupplier}`,
              },
            ],
            projectHasIncumbentPrices
          ),
          {
            Footer: footer => calculateColumnSumWithFormatting(footer),
            Cell: ({ cell: { value } }) => <LocaleFormatNumber value={value} />,
            Header: t('Best Price'),
            HeaderSub: UNIT_PRICE_SUB_TEXT,
            accessor: value => value.bestPrice[TOTAL_PRICE],
            id: 'bestPrice',
            sortType: SORTING_TYPE.NUMBER,
            tooltip: ({ cell }) =>
              `${LOWEST_BIDDERS}: ${cell.row.original.bestPriceSupplier}`,
          },
        ],
      },
      {
        Header: '',
        id: 'group 2',
        columns: getSupplierPrice({
          UNIT_PRICE_SUB_TEXT,
          itemTypes,
          projectHasItemTypes,
          projectSuppliers: projectSuppliersInData,
          secondAxisKey: tempSecondAxis,
        }),
      },
    ],
    [
      projectSuppliersInData,
      UNIT_PRICE_SUB_TEXT,
      projectUnit,
      t,
      projectHasHistoricPrices,
      itemTypes,
      projectHasItemTypes,
    ]
  );

  return (
    <StyledContainer>
      <Table
        data={data}
        columns={columns}
        defaultSorting={DEFAULT_SORTING}
        maxHeight="60vh"
        stickyHeader
        expandable
      />
    </StyledContainer>
  );
};
