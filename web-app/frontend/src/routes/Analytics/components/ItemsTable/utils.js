import React from "react";
import { SORTING_TYPE } from '@common/components/Table/Table';
import { LocaleFormatNumber } from '@common/components/FormatNumber/FormatNumber';
import { calculateColumnSumWithFormatting } from "@common/components/Table/utils";
import { getItemTypeForSupplier } from "@routes/Analytics/utils/filters";

const TOTAL_PRICE = 'Total Price';

const getValueForSupplier = (value, supplierName, itemType) =>
  (value.suppliers[supplierName] &&
    value.suppliers[supplierName][itemType] &&
    value.suppliers[supplierName][itemType].prices[TOTAL_PRICE]) ||
  null;

const getSecondAxisForSupplier = (value, supplierName, itemType, secondAxisKey) =>
  (value.suppliers[supplierName] &&
    value.suppliers[supplierName][itemType] &&
    value.suppliers[supplierName][itemType].second_axis &&
    value.suppliers[supplierName][itemType].second_axis[secondAxisKey]) ||
  null;

export const getSupplierPrice = ({
  UNIT_PRICE_SUB_TEXT,
  itemTypes,
  projectHasItemTypes,
  projectSuppliers,
  secondAxisKey
}) => {
  const result = [];


  projectSuppliers.forEach((supplierName) => {
    if (projectHasItemTypes) {
      const columns = itemTypes.map(itemType => ({
        Footer: footer => calculateColumnSumWithFormatting(footer, (row) => {
          const values = getItemTypeForSupplier(row, supplierName, itemType);
          return values ? values.total : 0;
        }),
        Cell: ({ cell: { value } }) => <LocaleFormatNumber value={value} />,
        Header: itemType,
        HeaderSub: UNIT_PRICE_SUB_TEXT,
        accessor: value => getValueForSupplier(value, supplierName, itemType),
        id: `${supplierName}_${itemType}`,
        sortType: SORTING_TYPE.NUMBER,
        tooltip: ({ cell }) => {
          if (!secondAxisKey) {
            return '';
          }
          const secondAxisValue = getSecondAxisForSupplier(cell.row.original, supplierName, itemType, secondAxisKey);
          if (!secondAxisValue) {
            return '';
          }
          return `${secondAxisKey}: ${secondAxisValue}`;
        }
      }));

      result.push({
        Header: supplierName,
        columns,
        id: supplierName,
      });
    } else {
      result.push({
        Footer: footer => calculateColumnSumWithFormatting(footer),
        Cell: ({ cell: { value } }) => <LocaleFormatNumber value={value} />,
        Header: supplierName,
        HeaderSub: UNIT_PRICE_SUB_TEXT,
        accessor: value =>
          getValueForSupplier(value, supplierName, itemTypes[0]),
        id: supplierName,
        sortType: SORTING_TYPE.NUMBER,
        tooltip: ({ cell }) => {
          if (!secondAxisKey) {
            return '';
          }
          const secondAxisValue = getSecondAxisForSupplier(cell.row.original, supplierName, itemTypes[0], secondAxisKey);
          if (!secondAxisValue) {
            return '';
          }
          return `${secondAxisKey}: ${secondAxisValue}`;
        }
      });
    }
  });

  return result;
};
