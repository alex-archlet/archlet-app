import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {
  ANALYTICS_ACTION_FORM_NAME,
  ANALYTICS_REGION_FORM_NAME,
  ANALYTICS_SUPPLIERS_FORM_NAME,
} from '@utils/formNames';
import {
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TableSortLabel,
  Tooltip,
} from '@material-ui/core';
import { InfoOutlined } from '@material-ui/icons';
import { Text } from '@common/components/Text/Text';
import { Select as CustomSelect } from '@common/components/ReduxSelect/Select';
import { colors } from '@utils/uiTheme';
import { useCustomTranslation } from '@utils/translate';
import { LocaleFormatNumber } from '@common/components/FormatNumber/FormatNumber';

const StyledTableRow = styled(TableRow)`
  && {
    vertical-align: top;
  }
`;

const StyledTableCell = styled(TableCell)`
  && {
    min-width: 160px;
    height: 76px;
    padding: 10px;
  }
`;

const StyledTableHeaderCell = styled(TableCell)`
  && {
    min-width: 160px;
    height: 76px;
    padding: 10px;
  }
`;

const StyledInfoOutlined = styled(InfoOutlined)`
  && {
    position: relative;
    top: 2px;
    left: 2px;
    width: 10px;
    height: 10px;
    color: ${colors.iconGrey};
  }
`;

const StyledInfo = styled(Text)`
  margin-top: 10px;
  color: ${colors.iconGrey};
  cursor: pointer;
`;

const StyledAction = styled.div`
  width: 80%;
  margin: 3px 0;
  border-radius: 5px;
  padding: 2px 0;
  background-color: ${props => props.color};
  color: white;
  font-size: 11px;
  text-align: center;
  cursor: pointer;
`;

const StyledPrice = styled.div`
  color: ${props => (props.isMin ? colors.valid : colors.black)};
  font-weight: ${props => (props.isMin ? 500 : 400)};
`;

const StyledFilterBy = styled(Text)`
  margin-right: 20px;
`;

const StyledTableWrapper = styled(Box)`
  overflow: auto;
  max-height: calc(100vh - 370px);
`;

export const Heatmap = ({
  actions,
  heatmapData,
  onSetSortProperties,
  projectCurrency,
  regions,
  suppliers,
  selectedSuppliers,
}) => {
  const { t } = useCustomTranslation();

  const [order, setOrder] = useState('asc');

  const [orderBy, setOrderBy] = useState('item');

  const handleRequestSort = (property) => {
    const isDesc = orderBy === property && order === 'desc';
    const newOrder = isDesc ? 'asc' : 'desc';

    setOrder(newOrder);
    setOrderBy(property);

    onSetSortProperties({
      column: property,
      order: newOrder,
    });
  };

  const getMinValue = row =>
    Math.min(
      ...Object.values(row)
        .map(item => item && item.price)
        .filter(item => item && parseInt(item, 10))
    ).toFixed(0);

  const unitPriceText = `Unit Price (${projectCurrency})`;

  return (
    <Box mt={50}>
      <Box display="flex" justifyContent="space-between" align-items="center">
        <Text fontSize={18} fontWeight={500}>
          {t('Price Heatmap')}
        </Text>
        <Box display="flex" alignItems="center">
          <StyledFilterBy fontWeight={400}>{t('Filter by')}</StyledFilterBy>
          <CustomSelect
            items={actions}
            medium
            grey
            label={t('Actions')}
            form={ANALYTICS_ACTION_FORM_NAME}
            allText="All actions"
            multiple
            allSelected
          />
          <CustomSelect
            items={regions}
            medium
            grey
            label={t('Business region')}
            form={ANALYTICS_REGION_FORM_NAME}
            multiple
            allSelected
          />
          {!!suppliers && (
            <CustomSelect
              items={suppliers.map(supplier => ({
                key: supplier,
                value: supplier,
              }))}
              medium
              grey
              label={t('Suppliers')}
              form={ANALYTICS_SUPPLIERS_FORM_NAME}
              multiple
              allSelected
            />
          )}
        </Box>
      </Box>
      {heatmapData.length ? (
        <StyledTableWrapper display="flex" mt={40}>
          <Table stickyHeader>
            <TableHead>
              <StyledTableRow>
                <StyledTableHeaderCell
                  sortDirection={orderBy === 'item' ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === 'item'}
                    direction={order}
                    onClick={() => handleRequestSort('item')}
                  >
                    <Text fontSize={15} color={colors.black} fontWeight={500}>
                      Item
                    </Text>
                  </TableSortLabel>
                </StyledTableHeaderCell>
                <StyledTableHeaderCell>
                  <Text fontSize={15} color={colors.black} fontWeight={500}>
                    Actions
                  </Text>
                </StyledTableHeaderCell>
                {selectedSuppliers.map(supplier => (
                  <StyledTableHeaderCell key={supplier}>
                    <TableSortLabel
                      active={orderBy === supplier}
                      direction={order}
                      onClick={() => handleRequestSort(supplier)}
                    >
                      <Text fontSize={15} fontWeight={500}>
                        {supplier}
                      </Text>
                    </TableSortLabel>
                    <Text fontSize={10}>{unitPriceText}</Text>
                  </StyledTableHeaderCell>
                ))}
              </StyledTableRow>
            </TableHead>
            <TableBody>
              {heatmapData.map((item) => {
                const min = getMinValue(item);

                return (
                  <TableRow key={item.item}>
                    <StyledTableCell>
                      <Text fontSize={12}>{item.item}</Text>
                    </StyledTableCell>
                    <StyledTableCell>
                      {item && item.actions && (
                        <>
                          {item.actions.verify ? (
                            <Tooltip
                              title={item.actions.verify}
                              placement="right-start"
                            >
                              <StyledAction color={colors.error}>
                                Verify
                              </StyledAction>
                            </Tooltip>
                          ) : null}
                          {item.actions.negotiate ? (
                            <Tooltip
                              title={item.actions.negotiate}
                              placement="right-start"
                            >
                              <StyledAction color={colors.valid}>
                                Negotiate
                              </StyledAction>
                            </Tooltip>
                          ) : null}
                        </>
                      )}
                    </StyledTableCell>
                    {selectedSuppliers.map(supplier => (
                      <StyledTableCell key={`${supplier}-${item.item}`}>
                        {item && item[supplier] && (
                          <>
                            {item[supplier].price && (
                              <StyledPrice
                                isMin={min === item[supplier].price.toFixed(0)}
                              >
                                <LocaleFormatNumber
                                  value={item[supplier].price}
                                  type={{
                                    decimals: 2,
                                    type: 'general',
                                  }}
                                 />
                              </StyledPrice>
                            )}
                            <StyledInfo fontSize={10} lineHeight={12}>
                              {item[supplier].range}
                              {item[supplier].alert && (
                                <Tooltip
                                  title={item[supplier].alert}
                                  placement="right-start"
                                >
                                  <StyledInfoOutlined />
                                </Tooltip>
                              )}
                            </StyledInfo>
                          </>
                        )}
                      </StyledTableCell>
                    ))}
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </StyledTableWrapper>
      ) : (
        <Box mt={30}>
          <Text>No items matching selected criteria.</Text>
        </Box>
      )}
    </Box>
  );
};

Heatmap.propTypes = {
  actions: PropTypes.arrayOf(PropTypes.string).isRequired,
  heatmapData: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  onSetSortProperties: PropTypes.func.isRequired,
  projectCurrency: PropTypes.string.isRequired,
  regions: PropTypes.arrayOf(PropTypes.string).isRequired,
  selectedSuppliers: PropTypes.arrayOf(PropTypes.string).isRequired,
  suppliers: PropTypes.arrayOf(PropTypes.string).isRequired,
};
