import React from 'react';
import {
  CartesianGrid,
  Scatter,
  ScatterChart,
  Text,
  Tooltip as RechartsTooltip,
  XAxis,
  YAxis,
  ZAxis,
} from 'recharts';
import { Box } from '@material-ui/core';
import { useCustomTranslation } from '@utils/translate';
import { projectsSelectors } from '@selectors/projects';
import { useSelector } from 'react-redux';

import { colors } from '@utils/uiTheme';
import { LocaleFormatNumber } from '@common/components/FormatNumber/FormatNumber';
import { formatNumber } from '@utils/formatNumber';
import { Tooltip } from '@common/components/Tooltip/Tooltip';
import { CustomTooltip } from '@common/components/Graph/Legend/CustomTooltip';
import { CustomDot } from '@common/components/Graph/Legend/CustomDot';

const HEIGHT_WEIGHT = {
  height: 400,
  width: 500,
};

const TOTAL_PRICE = 'Total Price';
const GRAPH_LINE_COLOR = '#f1f3f6';

const generateItems = (payload) => {
  if (!payload || !payload.length) {
    return null;
  }
  const fullObject = payload[0].payload;

  return [
    {
      label: 'Volume',
      value: <LocaleFormatNumber value={fullObject.volume} />
    },
    {
      label: 'Total Cost',
      value: <LocaleFormatNumber value={fullObject.total} />,
    },
    {
      label: 'Price Deviation',
      value: formatNumber(fullObject.deviations[TOTAL_PRICE] * 100, {
        type: 'percent',
      }),
    },
    {
      label: '# of Suppliers',
      value: fullObject.supplierCount,
    },
  ];
};

const generateTitle = (payload) => {
  if (!payload || !payload.length) {
    return null;
  }

  return payload[0].payload.item;
};

const isItemVisible = (visibleData, itemName) =>
  !visibleData || visibleData.some(({ item }) => item === itemName);

interface Props {
  data: any[];
  handleButtonCircleClick: Function;
  selectedItemMap: any;
}

export const NegotiationMatrix: React.FC<Props> = ({
  selectedItemMap,
  handleButtonCircleClick,
  data
}) => {
  const projectCurrency = useSelector(projectsSelectors.getProjectCurrencyCode);
  const { t } = useCustomTranslation();

  return (
    data.length > 0 ? (
      <Box
        display="flex"
        height="400px"
        width="500px"
        justifyContent="center"
        alignItems="center"
      >
        <ScatterChart
          width={HEIGHT_WEIGHT.width}
          height={HEIGHT_WEIGHT.height}
          margin={{
            bottom: 20,
            left: 20,
            right: 20,
            top: 35,
          }}
        >
          <XAxis
            type="number"
            dataKey="normalizationBestPrice"
            name="Total Price"
            tickCount={5}
            tickLine={false}
            domain={[-0.1, 1.1]}
            tick={{
              opacity: 0,
            }}
            // @ts-ignore
            label={
              <>
                <Text
                  style={{ color: colors.textColor }}
                  x={HEIGHT_WEIGHT.width / 1.36}
                  y={HEIGHT_WEIGHT.height - 30}
                  offset={0}
                  angle={0}
                >
                  {t('High')}
                </Text>
                <Text
                  style={{ color: colors.textColor }}
                  x={HEIGHT_WEIGHT.width / 2.9}
                  y={HEIGHT_WEIGHT.height - 30}
                  offset={0}
                  angle={0}
                >
                  {t('Low')}
                </Text>
                <Tooltip
                  title={t(
                    'Total spend is calculated by multiplying volume with unit price'
                  )}
                >
                  <Text
                    style={{ color: colors.textColor }}
                    x={HEIGHT_WEIGHT.width / 2.1}
                    y={HEIGHT_WEIGHT.height - 15}
                    offset={0}
                    angle={0}
                  >
                    {`${t('Total Spend')} (${projectCurrency})`}
                  </Text>
                </Tooltip>
              </>
            }
          />
          <YAxis
            type="number"
            dataKey="normalizationDeviation"
            name="Price Deviation"
            tickCount={5}
            tickLine={false}
            domain={[-0.1, 1.1]}
            tick={{
              opacity: 0,
            }}
            // @ts-ignore
            label={
              <>
                <Text
                  style={{ color: colors.textColor }}
                  x={65}
                  y={HEIGHT_WEIGHT.height / 3.1}
                  offset={0}
                  angle={-90}
                >
                  {t('High')}
                </Text>
                <Text
                  style={{ color: colors.textColor }}
                  x={65}
                  y={HEIGHT_WEIGHT.height / 1.4}
                  offset={0}
                  angle={-90}
                >
                  {t('Low')}
                </Text>
                <Tooltip
                  title={t(
                    'The price deviation is the difference from lowest bidding to highest bidding price'
                  )}
                >
                  <Text
                    style={{ color: colors.textColor }}
                    x={45}
                    y={HEIGHT_WEIGHT.height / 1.6}
                    offset={0}
                    angle={-90}
                  >
                    {t('% Price Deviation')}
                  </Text>
                </Tooltip>
              </>
            }
          />
          <ZAxis type="number" dataKey="supplierCount" name="Suppliers" />
          <CartesianGrid stroke={GRAPH_LINE_COLOR} strokeWidth={1.25} />
          <RechartsTooltip
            content={({ payload }) => {
              const items = generateItems(payload);
              const title = generateTitle(payload);

              return items ? (
                <CustomTooltip title={title} items={items} />
              ) : null;
            }}
          />
          <Scatter
            name="offer"
            data={data}
            // @ts-ignore
            shape={shapeItem =>
              isItemVisible(data, shapeItem.item) ? (
                <CustomDot
                  isSelected={
                    Object.keys(selectedItemMap).length === 0 ||
                    selectedItemMap[shapeItem.item]
                  }
                  shapeItem={shapeItem}
                />
              ) : null
            }
            onClick={(value) => {
              handleButtonCircleClick(value);
            }}
          />
        </ScatterChart>
      </Box>
    ) : null
  );
};

