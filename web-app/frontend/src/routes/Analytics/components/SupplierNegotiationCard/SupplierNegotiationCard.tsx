import React from 'react';

import { Card } from '@common/components/Card/Card';
import { useCustomTranslation } from '@utils/translate';
import { SupplierNegotiationGraph } from './SupplierNegotiationGraph';

interface Props {
  data: any[];
  supplierName: string;
  itemTypes: string[];
}

export const SupplierNegotiationCard: React.FC<Props> = ({
  data,
  supplierName,
  itemTypes,
}) => {
  const { t } = useCustomTranslation();
  return (
    <Card
      title={supplierName ? t('Negotiation Potential') : undefined}
      tooltipInfoMessage={
        supplierName
          ? `${t(
              'Five items with highest negotiation potential for'
            )} ${supplierName}`
          : undefined
      }
    >
      {supplierName && (
        <SupplierNegotiationGraph
          data={data.slice(0, 5)}
          supplierName={supplierName}
          selectedItemTypes={itemTypes}
        />
      )}
    </Card>
  );
};
