import React, { useState } from 'react';
import {
  Bar,
  BarChart,
  CartesianGrid,
  Label,
  Legend,
  Tooltip,
  XAxis,
  YAxis,
  ResponsiveContainer,
} from 'recharts';
import { useSelector } from 'react-redux';

import { projectsSelectors } from '@selectors/projects';
import { formatNumber, formatNumberWithLetters } from '@utils/formatNumber';
import { truncate } from '@utils/stringFormat';
import { useCustomTranslation } from '@utils/translate';
import { colors } from '@utils/uiTheme';
import { CustomLegend } from '@common/components/Graph/Legend/CustomLegend';
import { CustomTooltip } from '@common/components/Graph/Legend/CustomTooltip';
import { LocaleFormatNumber } from '@common/components/FormatNumber/FormatNumber';

const BEST_PRICE_COLOR = '#92bedd';
const GRAPH_LINE_COLOR = '#f1f3f6';
const BAR_WIDTH = 27;

const supplierPriceSelector = (supplierName, currentProjectItemType) => item => (
  item.suppliers[supplierName][currentProjectItemType] ?
    item.suppliers[supplierName][currentProjectItemType].total :
    null
);

const bestPriceSelector = item => item.total;

// reduce value length so that all labels are shown
const formatXAxis = value => truncate(value, 12);

const TOTAL_PRICE = 'Total Price';
const BEST_PRICE = '_best_price';

const getColor = index => colors.graphs.graphComparison[index % 10];

interface Props {
  data: any[];
  selectedItemTypes: string[];
  supplierName: string;
}

type CurrentTypeState = string | null;

export const SupplierNegotiationGraph: React.FC<Props> = ({ data, supplierName, selectedItemTypes }) => {
  const hasItemTypes = useSelector(projectsSelectors.getProjectHasItemTypes);
  const projectCurrency = useSelector(projectsSelectors.getProjectCurrencyCode);
  const [currentItemType, setCurrentItemType] = useState<CurrentTypeState>(null);
  const { t } = useCustomTranslation();
  
  const getSupplierName = (itemType: string) => {
    if (hasItemTypes) {
      return `${itemType} - ${supplierName} `;
    }
    return supplierName;
  };

  const getTooltipContent = (payload) => {
    const fullObject = payload[0].payload;
    const supplierData = fullObject.suppliers[supplierName];
    const supplierDataForItemType = currentItemType ? supplierData[currentItemType] : undefined;

    if (supplierDataForItemType) {
      return {
        items: [
          ...(selectedItemTypes.length > 1 ?
            [
              {
                label: t('Item Type'),
                value: currentItemType,
              },
            ] :
            []),
          {
            label: t('Volume'),
            value: <LocaleFormatNumber value={fullObject.volume} />,
          },
          {
            label: t('Total Cost'),
            value: <LocaleFormatNumber value={supplierDataForItemType.total} />,
          },
          {
            label: t('Price Deviation'),
            value: formatNumber(supplierDataForItemType.deviations[TOTAL_PRICE] * 100, { type: 'percent' }),
          },
        ],
        title: fullObject.item,
      };
    }
    if (currentItemType === BEST_PRICE) {
      return {
        items:[
          {
            label: t('Volume'),
            value: <LocaleFormatNumber value={fullObject.volume} />,
          },
          {
            label: t('Total Cost'),
            value: <LocaleFormatNumber value={fullObject.total} />,
          },
          {
            label: t('Price Deviation'),
            value: 0,
          },
        ],
        title: fullObject.item,
      };
    }
    return null;
  };

  const getLegendPayload = () => {
    const bestPrice = {
      color: BEST_PRICE_COLOR,
      id: 'bestPrice',
      value: 'Best price',
    };

    const result = selectedItemTypes.map((itemType, index) => ({
      color: getColor(index),
      id: `supplier_${itemType}`,
      value: getSupplierName(itemType),
    }));

    result.push(bestPrice);

    return result;
  };

  return (
    <ResponsiveContainer width="100%" height={325}>
      <BarChart
        data={data}
        margin={{
          bottom: 5,
          left: 20,
          right: 30,
          top: 5,
        }}
        barGap={0}
        barSize={BAR_WIDTH}
        onMouseLeave={() => setCurrentItemType(null)}
      >
        <CartesianGrid
          vertical={false}
          stroke={GRAPH_LINE_COLOR}
        />
        <XAxis
          dataKey="item"
          tickLine={false}
          axisLine={false}
          tick={{
            fill: '#000000',
            fontFamily: 'sans-serif',
            fontSize: 11,
            opacity: 0.7,
          }}
          tickFormatter={formatXAxis}
          // @ts-ignore
          style={{
            transform: 'translate(0, 6px)',
          }}
        />
        <YAxis
          tick={{
            fill: '#000000',
            fontFamily: 'sans-serif',
            fontSize: 11,
            opacity: 0.7,
          }}
          // @ts-ignore
          tickFormatter={formatNumberWithLetters}
          tickCount={5}
          tickLine={false}
          axisLine={false}
          stroke={GRAPH_LINE_COLOR}
        >
          <Label
            value={`${t('Total Spend')} (${projectCurrency})`}
            // @ts-ignore
            angle="-90"
            offset={0}
            position="insideLeft"
            style={{
              textAnchor: 'middle',
            }}
          />
        </YAxis>
        <Tooltip
          // @ts-ignore
          cursor={{ fill: 'transparent' }}
          content={({ payload }) => currentItemType && payload && payload.length && (
            // @ts-ignore
            <CustomTooltip {...getTooltipContent(payload)} />
          )}
        />
        <Legend
          content={({ payload }) => payload && (
            <CustomLegend
              payload={payload}
              alignment="right"
            />
          )}
          // @ts-ignore
          payload={getLegendPayload()}
          align="right"
          verticalAlign="top"
          wrapperStyle={{
            fontSize: 11,
            paddingBottom: 12,
          }}
        />
        {selectedItemTypes.map((itemType, index) => (
          <Bar
            key={itemType}
            dataKey={supplierPriceSelector(supplierName, itemType)}
            fill={getColor(index)}
            onMouseEnter={() => setCurrentItemType(itemType)}
          />
        ))}
        <Bar
          dataKey={bestPriceSelector}
          fill={BEST_PRICE_COLOR}
          onMouseEnter={() => setCurrentItemType(BEST_PRICE)}
        />
      </BarChart>
    </ResponsiveContainer>
  );
};
