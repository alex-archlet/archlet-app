import React from 'react';
import {
  Bar,
  BarChart,
  CartesianGrid,
  Label,
  Legend,
  Tooltip,
  XAxis,
  YAxis,
  ResponsiveContainer,
} from 'recharts';
import { useSelector } from 'react-redux';

import { projectsSelectors } from '@selectors/projects';
import { useCustomTranslation } from '@utils/translate';
import { LocaleFormatNumber } from '@common/components/FormatNumber/FormatNumber';
import { formatNumber } from '@utils/formatNumber';
import { colors } from '@utils/uiTheme';
import { truncate } from '@utils/stringFormat';
import { CustomLegend } from '@common/components/Graph/Legend/CustomLegend';
import { CustomTooltip } from '@common/components/Graph/Legend/CustomTooltip';
import { getEmptyArrayIfConditionFalse } from '@utils/array';
import { ShapeOption } from '@common/components/ColorShape/ColorShape';

import { AverageCostItem } from './utils';

interface Props {
  data: AverageCostItem[];
  selectedItemTypes: string[];
  supplierName: string;
}
// reduce value length so that all labels are shown
const formatXAxis = (value: string) => truncate(value, 12);
const getColor = (index: number) => colors.graphs.graphComparison[index % 10];
const itemTypeSelector = (itemType: string) => (data: AverageCostItem) =>
  data.itemTypes[itemType] || 0;

const BAR_WIDTH = 27;
const GRAPH_LINE_COLOR = '#f1f3f6';
const TABLE_ITEM_SHAPE: ShapeOption = 'square';

export const AverageCostGraph: React.FC<Props> = ({
  data,
  selectedItemTypes,
  supplierName,
}) => {
  const hasItemTypes = useSelector(projectsSelectors.getProjectHasItemTypes);
  const { t } = useCustomTranslation();
  const projectCurrency = useSelector(projectsSelectors.getProjectCurrencyCode);

  const getSupplierName = (itemType: string) => {
    if (hasItemTypes) {
      return `${itemType} - ${supplierName} `;
    }
    return supplierName;
  };

  const getTooltipContent = (payload) => {
    const fullObject: AverageCostItem = payload[0].payload;
    const itemTypeValues = selectedItemTypes.map((itemType, index) => ({
      label: getSupplierName(itemType),
      color: getColor(index),
      value: <LocaleFormatNumber value={fullObject.itemTypes[itemType]} />,
      shape: TABLE_ITEM_SHAPE,
    }));
    const items = [
      ...itemTypeValues,
      {
        label: t('Best Price'),
        value: <LocaleFormatNumber value={fullObject.bestPrice} />,
        shape: TABLE_ITEM_SHAPE,
        color: getColor(selectedItemTypes.length),
      },
      ...getEmptyArrayIfConditionFalse([
        {
          label: t('Baseline'),
          value: <LocaleFormatNumber value={fullObject.baselinePrice || 0} />,
          shape: TABLE_ITEM_SHAPE,
          color: getColor(selectedItemTypes.length + 1),
        },
      ], fullObject.baselinePrice !== null),
    ];
    return {
      items,
      title: fullObject.name,
    };
  };

  const getLegendPayload = () => {
    const result = selectedItemTypes.map((itemType, index) => ({
      color: getColor(index),
      id: `supplier_${itemType}`,
      value: getSupplierName(itemType),
    }));

    return [
      ...result,
      {
        color: getColor(selectedItemTypes.length),
        id: 'best',
        value: t('Best Price'),
      },
      ...getEmptyArrayIfConditionFalse([
        {
          color: getColor(selectedItemTypes.length + 1),
          id: 'baseline',
          value: t('Baseline'),
        },
      ], data[0] && data[0].baselinePrice !== null),
    ];
  };

  return (
    <ResponsiveContainer width="100%" height={325}>
      <BarChart
        data={data}
        margin={{
          bottom: 5,
          left: 20,
          right: 30,
          top: 5,
        }}
        barGap={0}
        barSize={BAR_WIDTH}
      >
        <CartesianGrid vertical={false} stroke={GRAPH_LINE_COLOR} />
        <XAxis
          dataKey="name"
          tickLine={false}
          axisLine={false}
          tick={{
            fill: '#000000',
            fontFamily: 'sans-serif',
            fontSize: 11,
            opacity: 0.7,
          }}
          tickFormatter={formatXAxis}
          // @ts-ignore
          style={{
            transform: 'translate(0, 6px)',
          }}
        />
        {/* @ts-ignore */}
        <YAxis
          tick={{
            fill: '#000000',
            fontFamily: 'sans-serif',
            fontSize: 11,
            opacity: 0.7,
          }}
          tickFormatter={val => formatNumber(val, { decimals: 0 })}
          tickCount={5}
          tickLine={false}
          axisLine={false}
          stroke={GRAPH_LINE_COLOR}
        >
          <Label
            value={`${t('Average Cost')} (${projectCurrency})`}
            // @ts-ignore
            angle="-90"
            offset={0}
            position="insideLeft"
            style={{
              textAnchor: 'middle',
            }}
          />
        </YAxis>
        <Tooltip
          // @ts-ignore
          cursor={{ fill: 'transparent' }}
          content={({ payload }) =>
            payload &&
            payload.length && <CustomTooltip {...getTooltipContent(payload)} />
          }
        />
        <Legend
          content={({ payload }) => (
            // @ts-ignore
            <CustomLegend payload={payload} alignment="right" />
          )}
          // @ts-ignore
          payload={getLegendPayload()}
          align="right"
          verticalAlign="top"
          wrapperStyle={{
            fontSize: 11,
            paddingBottom: 12,
          }}
        />
        {selectedItemTypes.map((itemType, index) => (
          <Bar
          key={itemType}
          dataKey={itemTypeSelector(itemType)}
          fill={getColor(index)}
          />
        ))}
        <Bar dataKey="bestPrice" fill={getColor(selectedItemTypes.length)} />
        <Bar dataKey="baselinePrice" fill={getColor(selectedItemTypes.length + 1)} />
      </BarChart>
    </ResponsiveContainer>
  );
};

