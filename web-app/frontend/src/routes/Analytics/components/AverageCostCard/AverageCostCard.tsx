import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

import { Card } from '@common/components/Card/Card';
import { useCustomTranslation } from '@utils/translate';
import { projectsSelectors } from '@store/selectors/projects';
import { splitIntoChunks } from '@utils/array';

import { calculateAverageCosts } from './utils';
import { AverageCostGraph } from './AverageCostGraph';
import { ArrowTitle } from '../ArrowTitle/ArrowTitle';

interface Props {
  supplierData: any[];
  supplierName: string;
  itemTypes: string[];
}

const PAGE_SIZE = 7;

const getPageRangeString = (index: number, max: number) => {
  const beginning = (PAGE_SIZE * index) + 1;
  const unsafeMax = PAGE_SIZE * (index + 1);
  const safeMax = Math.min(max, unsafeMax);
  return `${beginning} -  ${safeMax}`;
};

export const AverageCostCard: React.FC<Props> = ({
  supplierData,
  supplierName,
  itemTypes,
}) => {
  const priceComponents = useSelector(projectsSelectors.getProjectPriceComponents);
  const { t } = useCustomTranslation();
  const [titleIndex, setTitleIndex] = useState(0);

  const data = calculateAverageCosts(supplierData, supplierName, itemTypes, priceComponents);
  const totalPriceBars = data.filter(row => row.name === 'Total Price');
  const dataWithoutPriceBar = data.filter(row => row.name !== 'Total Price');
  const pagedData = splitIntoChunks(dataWithoutPriceBar, PAGE_SIZE);

  useEffect(() => {
    setTitleIndex(0);
  }, [supplierName, data.length]);

  const max = data.length;
  const range = getPageRangeString(titleIndex, max);
  const title = t('Price Components {{range}} of {{max}}', { range, max });

  const dataAtCurrentPage = pagedData[titleIndex] ?? [];

  return (
    <Card
      title={supplierName && t('Average Cost')}
      tooltipInfoMessage={
        supplierName
          ? `${t('Overview of the weighted average prices of')} ${supplierName}`
          : undefined
      }
    >
      {supplierName ? (
        <>
          <ArrowTitle
            title={title}
            setTitleIndex={setTitleIndex}
            titleIndex={titleIndex}
            selectedItemsLength={pagedData.length}
          />
          <AverageCostGraph data={[...totalPriceBars, ...dataAtCurrentPage]} selectedItemTypes={itemTypes} supplierName={supplierName} />
        </>
      ) : null}
    </Card>
  );
};
