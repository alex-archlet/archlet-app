import { sum } from '@utils/array';

interface DesignAverageCostMap {
  [designName: string]: number;
}

export interface AverageCostItem {
  name: string;
  bestPrice: number;
  baselinePrice: number | null;
  itemTypes: DesignAverageCostMap;
}

const getSupplierDataForItemType = (
  item: any,
  supplierName: string,
  priceComponent: string,
  itemType: string
) => {
  return (
    item.suppliers[supplierName] &&
    item.suppliers[supplierName][itemType] &&
    item.suppliers[supplierName][itemType].prices[priceComponent]
  );
};

const calculateItemBestPriceTotalTimesVolume = (
  item: any,
  priceComponent: string
) => {
  return item.volume * item.bestPrice[priceComponent] || 0;
};

const calculateItemBaselinePriceTotalTimesVolume = (
  item: any,
  priceComponent: string
) => {
  return item.volume * item.historicPrice[priceComponent] || 0;
};

const calculateItemTypePriceTotalTimesVolume = (
  item: any,
  supplierName: string,
  priceComponent: string,
  itemType: string
) => {
  const priceForItemTypeAndComponent = getSupplierDataForItemType(
    item,
    supplierName,
    priceComponent,
    itemType
  );
  return item.volume * priceForItemTypeAndComponent || 0;
};

export const calculateAverageCosts = (
  items: any[],
  supplierName: string,
  itemTypes: string[],
  priceComponents: string[]
): AverageCostItem[] => {
  const totalVolume = sum(items.map(({ volume }) => volume));
  const calculateBestPrice = (priceComponent: string) => {
    const sumForAllItems = sum(
      items.map(item =>
        calculateItemBestPriceTotalTimesVolume(item, priceComponent)
      )
    );
    return totalVolume ? sumForAllItems / totalVolume : 0;
  };

  const calculateBaselinePrice = (priceComponent: string) => {
    const sumForAllItems = sum(
      items.map(item =>
        calculateItemBaselinePriceTotalTimesVolume(item, priceComponent)
      )
    );
    return totalVolume && sumForAllItems ? sumForAllItems / totalVolume : null;
  };

  const calculateItemTypePrice = (itemType: string, priceComponent: string) => {
    const totalVolumeForOfferedItems = sum(
      items.map((item) => {
        const value = getSupplierDataForItemType(
          item,
          supplierName,
          priceComponent,
          itemType
        );
        return value ? item.volume : 0;
      })
    );
    const allTotalValues = items.map(item =>
      calculateItemTypePriceTotalTimesVolume(
        item,
        supplierName,
        priceComponent,
        itemType
      )
    );
    const sumForAllItems = sum(allTotalValues);
    return totalVolumeForOfferedItems ? sumForAllItems / totalVolumeForOfferedItems : 0;
  };

  return priceComponents.map((priceComponent) => {
    const object: AverageCostItem = {
      name: priceComponent,
      bestPrice: calculateBestPrice(priceComponent),
      baselinePrice: calculateBaselinePrice(priceComponent),
      itemTypes: {},
    };

    itemTypes.forEach((itemType) => {
      object.itemTypes[itemType] = calculateItemTypePrice(
        itemType,
        priceComponent
      );
    });

    return object;
  });
};
