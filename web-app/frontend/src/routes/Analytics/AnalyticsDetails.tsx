import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { connect, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Tabs } from '@common/components/Tabs/Tabs';
import { useTabParams } from '@common/components/Tabs/useTabParams';
import { setPageTitle, setBackButton } from '@actions/interface';
import { getEmptyArrayIfConditionFalse } from '@utils/array';
import { useCustomTranslation } from '@utils/translate';
import { featureFlagSelectors } from '@store/selectors/featureFlags';

import { SupplierFeedbackContainer } from './containers/SupplierFeedback/SupplierFeedback';
import { SupplierRankingContainer } from './containers/SupplierRanking/SupplierRanking';
import { PriceHeatmapContainer } from './containers/PriceHeatmap/PriceHeatmap';
import { PricePivotContainer } from './containers/PricePivot/PricePivot';
import { NegotiationOverviewContainer } from './containers/NegotiationOverview/NegotiationOverview';
import { ProjectsLayout } from '../ProjectDetails/components/ProjectsLayout/ProjectsLayout';

export const AnalyticsDetails = ({ setBackButtonAction }) => {
  const { t } = useCustomTranslation();
  const showPricePivotTable = useSelector(
    featureFlagSelectors.hasPivotTableFeatureFlag
  );
  const { id } = useParams();

  const tabs = [
    {
      disabled: false,
      id: 'dashboard',
      label: t('Dashboard'),
      render: () => <NegotiationOverviewContainer />,
    },
    {
      disabled: false,
      id: 'heatmap',
      label: t('Price Heatmap'),
      // @ts-ignore
      render: () => <PriceHeatmapContainer />,
    },
    ...getEmptyArrayIfConditionFalse(
      [
        {
          id: 'pivot',
          label: t('Price Pivot'),
          // @ts-ignore
          render: () => <PricePivotContainer />,
        },
      ],
      showPricePivotTable
    ),
    {
      disabled: false,
      id: 'supplier',
      label: t('Supplier Feedback'),
      // @ts-ignore
      render: () => <SupplierFeedbackContainer />,
    },
    {
      disabled: false,
      id: 'supplier_ranking',
      label: t('Supplier Ranking'),
      render: () => <SupplierRankingContainer />,
    },
  ];

  useEffect(() => {
    setBackButtonAction('arrow');

    return () => setBackButtonAction('');
  }, [id]);

  const { tab, onChange } = useTabParams({
    options: ['dashboard', 'heatmap', 'pivot', 'supplier', 'supplier_ranking'],
    initialValue: 'dashboard',
  });
  return (
    // @ts-ignore
    <ProjectsLayout backgroundColor="#FBFBFB">
      <Tabs tabs={tabs} initialValue={tab} onChange={onChange} />
    </ProjectsLayout>
  );
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setPageTitleAction: setPageTitle,
      setBackButtonAction: setBackButton,
    },
    dispatch
  );

export const AnalyticsDetailsContainer = connect(
  null,
  mapDispatchToProps
)(AnalyticsDetails);
