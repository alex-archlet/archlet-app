import React, { useEffect, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';

import { projectsSelectors } from '@selectors/projects';
import { Box } from '@material-ui/core';

import { Card } from '@common/components/Card/Card';
import { useCustomTranslation } from '@utils/translate';
import { Row } from '@common/components/Row/Row';
import {
  calculateMatrixNegotiationPotential,
  calculateNegotiationPotential,
  calculateSupplierBarData,
} from '@utils/negotiationPotentialCalculator';
import { PageTitle } from '@common/components/PageTitle/PageTitle';
import { Filter } from '@common/components/Filter/Filter';
import { TYPE } from '@common/components/Filter/FilterDropdown/FilterDropdown';
import { colors } from '@utils/uiTheme';
import {
  RESERVED_FILTER_IDS,
  useProjectFilter,
} from '@common/components/Filter/filterHooks';
import { getValueFromOption } from '@common/components/Filter/filterUtils';
import { getEmptyArrayIfConditionFalse } from '@utils/array';
import {
  SingleFilterValue,
  MultiFilterValue,
} from '@common/components/Filter/types';

import { hasItemBidForAnySupplierAndItemTypes } from '../../utils/filters';
import { NegotiationMatrix } from '../../components/NegotiationMatrix/NegotiationMatrix';
import { NegotiationComparison } from '../../components/NegotiationComparison/NegotiationComparison';
import { NegotiationScale } from '../../components/NegotiationScale/NegotiationScale';
import { ItemsTable } from '../../components/ItemsTable/ItemsTable';

const TOTAL_PRICE = 'Total Price';

const INITIAL_PRICE_COMPONENT = {
  label: TOTAL_PRICE,
  value: TOTAL_PRICE,
};

const arrayToMap = (array) => {
  const result = {};

  array.forEach((item) => {
    result[item.value] = item.value;
  });

  return result;
};

const getLastElementOfArray = (array: Array<any>) => {
  if (!array.length) {
    return [];
  }
  return array[array.length - 1];
};

const getNameFromPriceComponent = ({ name }) => name;

type Item = {
  item: string;
};

export const NegotiationOverviewContainer: React.FC = () => {
  const projectCurrency = useSelector(projectsSelectors.getProjectCurrencyCode);
  const projectHasHistoricPrices = useSelector(
    projectsSelectors.getProjectHasHistoricPrices
  );
  const projectHasIncumbentPrices = useSelector(
    projectsSelectors.getProjectHasIncumbentPrices
  );
  const projectHasItemTypes = useSelector(
    projectsSelectors.getProjectHasItemTypes
  );
  const projectHeatmap = useSelector(projectsSelectors.getProjectHeatmap);
  const projectItemTypes = useSelector(projectsSelectors.getProjectItemTypes);
  const projectPriceComponentsMap = useSelector(
    projectsSelectors.getProjectPriceComponentsStructureMap
  );
  const projectSuppliers = useSelector(projectsSelectors.getProjectSuppliers);
  const projectUnit = useSelector(projectsSelectors.getProjectUnitName);

  const [negotiationMatrixPotential, setNegotiationMatrixPotential] = useState(
    []
  );

  const [scaleNegotiationPotential, setScaleNegotiationPotential] = useState(0);

  const [titleItemId, setTitleItemId] = useState<null | string>(null);

  const { t } = useCustomTranslation();

  const filterDeclaration = [
    ...getEmptyArrayIfConditionFalse(
      [
        {
          customFilter: (row, filterState) =>
            hasItemBidForAnySupplierAndItemTypes(
              row,
              filterState,
              projectItemTypes
            ),
          id: RESERVED_FILTER_IDS.PROJECT_ITEM_TYPE,
          label: 'Item Type',
          type: TYPE.MULTI,
        },
      ],
      projectHasItemTypes
    ),
    {
      clearable: false,
      id: RESERVED_FILTER_IDS.PROJECT_VOLUME,
      label: 'Volume',
      type: TYPE.SINGLE,
    },
    {
      clearable: false,
      id: RESERVED_FILTER_IDS.PROJECT_COST_COMPONENT,
      label: 'Cost Component',
      type: TYPE.SINGLE,
    },
    {
      id: RESERVED_FILTER_IDS.PROJECT_ITEMS,
      label: 'Selected Items',
      type: TYPE.MULTI,
    },
  ];

  const {
    filterFunction,
    filterProps,
    filterState,
    setFilterState,
  } = useProjectFilter({
    additionalFilters: filterDeclaration,
    initialState: {
      [RESERVED_FILTER_IDS.PROJECT_COST_COMPONENT]: INITIAL_PRICE_COMPONENT,
    },
    reduxFilterId: 'analytics',
  });

  const {
    [RESERVED_FILTER_IDS.PROJECT_COST_COMPONENT]: costComponent,
    [RESERVED_FILTER_IDS.PROJECT_ITEMS]: items = [],
    [RESERVED_FILTER_IDS.PROJECT_ITEM_TYPE]: itemTypes,
    [RESERVED_FILTER_IDS.PROJECT_VOLUME]: volume,
  } = filterState;

  const currentProjectPriceComponent = costComponent
    ? (costComponent as SingleFilterValue).value
    : INITIAL_PRICE_COMPONENT.value;
  const itemTypesList = itemTypes && (itemTypes as MultiFilterValue);
  const currentProjectItemTypes =
    itemTypesList && itemTypesList.length
      ? itemTypesList.map(getValueFromOption)
      : projectItemTypes;
  const selectedItemsList = items as MultiFilterValue;
  const currentProjectVolume = volume
    ? (volume as SingleFilterValue).value
    : '';

  const selectedItemIdMap = useMemo(() => (items ? arrayToMap(items) : {}), [
    items,
  ]);

  const selectedItems = useMemo(() => {
    if (negotiationMatrixPotential.length && selectedItemsList.length) {
      return selectedItemsList
        .map(({ value }) =>
          negotiationMatrixPotential.find(
            (row: { item: string }) => row.item === value
          )
        )
        .filter(Boolean);
    }
    return [];
  }, [selectedItemsList, negotiationMatrixPotential]);

  useEffect(() => {
    if (selectedItems.length && !titleItemId) {
      const currentSelectedItemList = getLastElementOfArray(selectedItems);
      setTitleItemId(currentSelectedItemList.item);
    }
  }, [selectedItems, titleItemId]);

  const supplierSelectedItem = useMemo(() => {
    if (titleItemId) {
      return selectedItems.find((row) => {
        if (row) {
          // @ts-ignore
          return row.item === titleItemId;
        }
        return false;
      });
    }

    return null;
  }, [selectedItems, titleItemId]);

  const supplierSelectedItemIndex = useMemo(() => {
    if (titleItemId) {
      return selectedItems.findIndex((row) => {
        if (row) {
          // @ts-ignore
          return row.item === titleItemId;
        }
        return false;
      });
    }

    return null;
  }, [selectedItems, titleItemId]);

  const handleButtonCircleClick = (item) => {
    const itemId = item.item;

    if (selectedItemIdMap[itemId]) {
      const newItems = selectedItemsList.filter(
        ({ value }) => value !== itemId
      );

      setFilterState({
        ...filterState,
        [RESERVED_FILTER_IDS.PROJECT_ITEMS]: newItems,
      });
      setScaleNegotiationPotential(0);

      if (titleItemId === itemId) {
        const lastItemAfterThis = selectedItemsList
          .map(({ value: selectedItem }) => selectedItem)
          .filter(selectedItem => selectedItem !== itemId)
          .slice(-1)
          .pop();

        setTitleItemId((lastItemAfterThis as string) ?? null);
      }
    } else {
      setFilterState({
        ...filterState,
        [RESERVED_FILTER_IDS.PROJECT_ITEMS]: [
          ...selectedItemsList,
          {
            label: itemId,
            value: itemId,
          },
        ],
      });
      setScaleNegotiationPotential(item.scale * 2 * 100);
      setTitleItemId(itemId);
    }
  };

  const currentProjectPriceComponentObject =
    projectPriceComponentsMap[currentProjectPriceComponent];
  const currentProjectPriceComponentChildren = useMemo(() => {
    if (currentProjectPriceComponentObject) {
      const { children } = currentProjectPriceComponentObject;

      if (children && children.length) {
        return children.map(getNameFromPriceComponent);
      }

      return [getNameFromPriceComponent(currentProjectPriceComponentObject)];
    }

    return [];
  }, [currentProjectPriceComponentObject]);

  const supplierBarData = useMemo(() => {
    return supplierSelectedItem
      ? calculateSupplierBarData(
          supplierSelectedItem,
          currentProjectPriceComponentChildren
        )
      : {};
  }, [
    supplierSelectedItem,
    currentProjectPriceComponentChildren,
    selectedItems,
  ]);

  const rootPriceComponentObject = projectPriceComponentsMap[TOTAL_PRICE];

  useEffect(() => {
    if (
      projectSuppliers &&
      projectHeatmap &&
      rootPriceComponentObject &&
      currentProjectPriceComponent &&
      currentProjectVolume
    ) {
      const calculatedNegotiationPotential = calculateNegotiationPotential(
        projectHeatmap,
        projectSuppliers,
        rootPriceComponentObject,
        currentProjectVolume
      );
      // first filter data and the auto-scale
      const filteredData = filterFunction(calculatedNegotiationPotential);

      setNegotiationMatrixPotential(
        calculateMatrixNegotiationPotential(
          filteredData,
          currentProjectPriceComponent
        )
      );
    }
  }, [
    projectSuppliers,
    projectHeatmap,
    currentProjectPriceComponent,
    rootPriceComponentObject,
    currentProjectVolume,
    filterState,
  ]);

  const setTitleItemNumeric = (index) => {
    setTitleItemId(items[index].value);
  };

  return (
    <>
      <PageTitle
        title={t('Dashboard')}
        titleColor={colors.textColor}
        rightSide={
          <Filter
            enableClearButton
            {...filterProps}
            label={t('Filter by')}
            onChange={setFilterState}
          />
        }
      />
      <Row>
        <Card
          title={t('Opportunity Guidance')}
          tooltipInfoMessage={t(
            // eslint-disable-next-line max-len
            `Opportunity Matrix shows the greatest opportunities in the upper right section. The price deviation is the difference from lowest bidding to highest bidding price`
          )}
        >
          <Box display="flex" alignItems="center" justifyContent="center">
            <NegotiationMatrix
              data={negotiationMatrixPotential}
              selectedItemMap={selectedItemIdMap}
              handleButtonCircleClick={handleButtonCircleClick}
            />
            <NegotiationScale scale={scaleNegotiationPotential} />
          </Box>
        </Card>
        <Card
          title={t('Selected Items')}
          tooltipInfoMessage={t('Total price overview for selected items')}
        >
          <NegotiationComparison
            // @ts-ignore
            data={supplierBarData}
            itemTypes={currentProjectItemTypes}
            priceComponents={currentProjectPriceComponentChildren}
            setTitleIndex={setTitleItemNumeric}
            selectedItemsLength={selectedItems.length}
            titleIndex={supplierSelectedItemIndex}
          />
        </Card>
      </Row>
      <Row>
        <Card
          title={t('Price Heatmap')}
          tooltipInfoMessage={t(
            'The first row in the table below (marked grey) is the summary row where all the values are added up.'
          )}
        >
          <ItemsTable
            // @ts-ignore
            data={selectedItems}
            projectSuppliers={projectSuppliers}
            projectCurrency={projectCurrency}
            projectUnit={projectUnit}
            projectHasHistoricPrices={projectHasHistoricPrices}
            projectHasIncumbentPrices={projectHasIncumbentPrices}
            itemTypes={currentProjectItemTypes}
            projectHasItemTypes={projectHasItemTypes}
          />
        </Card>
      </Row>
    </>
  );
};
