import React, { useMemo } from 'react';
import { useSelector } from 'react-redux';

import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';
import { projectsSelectors } from '@selectors/projects';
import { calculateNPForSingleSupplier } from '@utils/negotiationPotentialCalculator';
import { roundToDecimals } from '@utils/formatNumber';
import { useCustomTranslation } from '@utils/translate';
import { exportCsv, flattenNestedData } from '@utils/csvexport';
import { Card } from '@common/components/Card/Card';
import { PageTitle } from '@common/components/PageTitle/PageTitle';
import { TYPE } from '@common/components/Filter/FilterDropdown/FilterDropdown';
import { Filter } from '@common/components/Filter/Filter';
import { getValueFromOption } from '@common/components/Filter/filterUtils';
import {
  RESERVED_FILTER_IDS,
  useProjectFilter,
} from '@common/components/Filter/filterHooks';
import { colors } from '@utils/uiTheme';
import { getEmptyArrayIfConditionFalse } from '@utils/array';
import { DownloadBox } from '@common/components/DownloadBox/DownloadBox';
import {
  MultiFilterValue,
  SingleFilterValue,
} from '@common/components/Filter/types';
import { Row } from '@common/components/Row/Row';
import { PlaceholderText } from '@common/components/PlaceholderText/PlaceholderText';

import { Flex } from '@common/components/Flex/Flex';
import { AverageCostCard } from '../../components/AverageCostCard/AverageCostCard';
import { getItemTypeForSupplier } from '../../utils/filters';
import { SupplierNegotiationTable } from '../../components/SupplierNegotiationTable/SupplierNegotiationTable';
import { BiddingOverviewCard } from '../../components/BiddingOverviewCard/BiddingOverviewCard';
import { BiddingAnalyticsCard } from '../../components/BiddingAnalyticsCard/BiddingAnalyticsCard';

import { sortSupplierData } from './utils';

const TOTAL_PRICE = 'Total Price';

const calculateDifference: (price1: number, price2: number) => number = (
  price1,
  price2
) => price1 - price2;

const onExport = ({
  currentProjectItemTypes,
  data,
  projectHasHistoricPrices,
  projectHasIncumbentPrices,
  projectUnit,
  secondAxisName,
  supplierName,
  t,
}) => {
  // define columns for data export
  const columns = [
    {
      Header: t('Item'),
      accessor: value => `${value.itemName ? `${value.itemName} - ` : ''}${value.item}`,
      fixed: true,
    },
    {
      Header: `${t('Volume')} (${projectUnit})`,
      accessor: value => roundToDecimals(value.volume),
    },
    ...(projectHasHistoricPrices
      ? [
          {
            Header: t('Baseline Price'),
            accessor: value =>
              roundToDecimals(value.historicPrice[TOTAL_PRICE]),
          },
          {
            Header: t('Incumbent Price'),
            accessor: value =>
              value.incumbentPrice &&
              roundToDecimals(value.incumbentPrice[TOTAL_PRICE]),
          },
        ]
      : []),
    {
      Header: t('Best Price'),
      accessor: value => roundToDecimals(value.bestPrice[TOTAL_PRICE]),
    },
    ...(secondAxisName
      ? [
        {
          Header: `${secondAxisName} - ${t('Baseline')}`,
          accessor: value =>
            value.historicSecondAxis &&
            value.historicSecondAxis[secondAxisName],
          },
        ]
      : []),
  ];

  currentProjectItemTypes.forEach((itemType) => {
    columns.push(
      ...[
        {
          Header: `${t('Supplier Bid')} - ${itemType}`,
          accessor: (value) => {
            const values = getItemTypeForSupplier(
              value,
              supplierName,
              itemType
            );

            return values && values.prices[TOTAL_PRICE];
          },
        },
        // add second axis if there is any
        ...(secondAxisName ? [{
          Header: `${t(secondAxisName)} - ${itemType}`,
      accessor: (value) => {
        const values = getItemTypeForSupplier(value, supplierName, itemType);
        if (!values || !values.second_axis) {
          return null;
        }
        return values.second_axis[secondAxisName];
      },},
        ] : []
          ),
        // add baseline and differences to it only if historic prices are available
        ...(projectHasHistoricPrices
          ? [
              {
                Header: `${t('Deviation to Baseline Price')} - ${itemType}`,
                accessor: (value) => {
                  const values = getItemTypeForSupplier(
                    value,
                    supplierName,
                    itemType
                  );

                  if (!values || !value.historicPrice[TOTAL_PRICE]) {
                    return null;
                  }

                  const historicPrice = value.historicPrice[TOTAL_PRICE];
                  const currentPrice = values.prices[TOTAL_PRICE];
                  return currentPrice / historicPrice - 1;
                },
              },
              {
                Header: `${t(
                  'Unit Difference to Baseline Price'
                )} - ${itemType}`,
                accessor: (value) => {
                  const values = getItemTypeForSupplier(
                    value,
                    supplierName,
                    itemType
                  );

                  if (!values || !value.historicPrice[TOTAL_PRICE]) {
                    return null;
                  }

                  return calculateDifference(
                    value.historicPrice[TOTAL_PRICE],
                    values.prices[TOTAL_PRICE]
                  );
                },
              },
              {
                Header: `${t(
                  'Spend Difference to Baseline Price'
                )} - ${itemType}`,
                accessor: (value) => {
                  const values = getItemTypeForSupplier(
                    value,
                    supplierName,
                    itemType
                  );

                  if (!values || !value.historicPrice[TOTAL_PRICE]) {
                    return null;
                  }

                  return (
                    value.volume *
                    calculateDifference(
                      value.historicPrice[TOTAL_PRICE],
                      values.prices[TOTAL_PRICE]
                    )
                  );
                },
              },
            ]
          : []),
        // add incumebnt and difference to it only if incumbent prices are available
        ...(projectHasIncumbentPrices
          ? [
              {
                Header: `${t('Deviation to Incumbent Price')} - ${itemType}`,
                accessor: (value) => {
                  const values = getItemTypeForSupplier(
                    value,
                    supplierName,
                    itemType
                  );

                  if (!values || !value.incumbentPrice[TOTAL_PRICE]) {
                    return null;
                  }

                  const incumbentPrice = value.incumbentPrice[TOTAL_PRICE];
                  const currentPrice = values.prices[TOTAL_PRICE];
                  return currentPrice / incumbentPrice - 1;
                },
              },
              {
                Header: `${t(
                  'Unit Difference to Incumbent Price'
                )} - ${itemType}`,
                accessor: (value) => {
                  const values = getItemTypeForSupplier(
                    value,
                    supplierName,
                    itemType
                  );

                  if (!values || !value.incumbentPrice[TOTAL_PRICE]) {
                    return null;
                  }

                  return calculateDifference(
                    value.incumbentPrice[TOTAL_PRICE],
                    values.prices[TOTAL_PRICE]
                  );
                },
              },
              {
                Header: `${t(
                  'Spend Difference to Incumbent Price'
                )} - ${itemType}`,
                accessor: (value) => {
                  const values = getItemTypeForSupplier(
                    value,
                    supplierName,
                    itemType
                  );

                  if (!values || !value.incumbentPrice[TOTAL_PRICE]) {
                    return null;
                  }

                  return (
                    value.volume *
                    calculateDifference(
                      value.incumbentPrice[TOTAL_PRICE],
                      values.prices[TOTAL_PRICE]
                    )
                  );
                },
              },
            ]
          : []),
        {
          Header: `${t('Deviation to Best Price')} - ${itemType}`,
          accessor: (value) => {
            const values = getItemTypeForSupplier(
              value,
              supplierName,
              itemType
            );
            
            if (!values) {
              return null;
            }
            
            const bestUnitPrice = value.bestPrice[TOTAL_PRICE];
            const currentPrice = values.prices[TOTAL_PRICE];

            return currentPrice / bestUnitPrice - 1;
          },
        },
        {
          Header: `${t('Unit Difference to Best Price')} - ${itemType}`,
          accessor: (value) => {
            const values = getItemTypeForSupplier(
              value,
              supplierName,
              itemType
            );

            if (!values) {
              return null;
            }

            return roundToDecimals(
              calculateDifference(
                value.bestPrice[TOTAL_PRICE],
                values.prices[TOTAL_PRICE]
              )
            );
          },
        },
        {
          Header: `${t('Spend Difference to Best Price')} - ${itemType}`,
          accessor: (value) => {
            const values = getItemTypeForSupplier(
              value,
              supplierName,
              itemType
            );

            if (!values) {
              return null;
            }

            return roundToDecimals(
              value.volume *
                calculateDifference(
                  value.bestPrice[TOTAL_PRICE],
                  values.prices[TOTAL_PRICE]
                )
            );
          },
        },
      ]
    );
  });

  const flattenedData = flattenNestedData(data);

  exportCsv(flattenedData, columns, supplierName);
};

export const SupplierFeedbackContainer = () => {
  const projectCurrency: string = useSelector(
    projectsSelectors.getProjectCurrencyCode
  );
  const projectHasHistoricPrices: boolean = useSelector(
    projectsSelectors.getProjectHasHistoricPrices
  );
  const projectHasIncumbentPrices = useSelector(
    projectsSelectors.getProjectHasIncumbentPrices
  );
  const projectHasItemTypes: boolean = useSelector(
    projectsSelectors.getProjectHasItemTypes
  );
  const projectHeatmap: any[] = useSelector(
    projectsSelectors.getProjectHeatmap
  );
  const projectItemTypes: string[] = useSelector(
    projectsSelectors.getProjectItemTypes
  );
  const projectPriceComponentRoot: any[] = useSelector(
    projectsSelectors.getProjectPriceComponentsRoot
  );
  const projectSuppliers: string[] = useSelector(
    projectsSelectors.getProjectSuppliers
  );
  const projectUnit: string = useSelector(projectsSelectors.getProjectUnitName);
  const projectSecondAxis: string[] = useSelector(
    projectsSelectors.getProjectSecondAxisKeys
  );

  const { t } = useCustomTranslation();

  const supplierFilter = {
    id: RESERVED_FILTER_IDS.PROJECT_SUPPLIER_SINGLE,
    label: t('Supplier'),
    text: t('Choose a supplier'),
    type: TYPE.SINGLE,
  };

  const filterDeclaration = [
    supplierFilter,
    ...getEmptyArrayIfConditionFalse(
      [
        {
          id: RESERVED_FILTER_IDS.PROJECT_ITEM_TYPE,
          label: t('Item Type'),
          text: t('Filter by'),
          type: TYPE.MULTI,
        },
      ],
      projectHasItemTypes
    ),
    {
      // don't allow clearing of the item
      clearable: false,
      id: RESERVED_FILTER_IDS.PROJECT_VOLUME,
      label: t('Volume'),
      type: TYPE.SINGLE,
    },
  ];

  const {
    filterFunction,
    filterProps,
    filterState,
    setFilterState,
  } = useProjectFilter({
    additionalFilters: filterDeclaration,
    reduxFilterId: 'analytics',
  });

  const {
    [RESERVED_FILTER_IDS.PROJECT_SUPPLIER_SINGLE]: supplier,
    [RESERVED_FILTER_IDS.PROJECT_ITEM_TYPE]: itemTypes,
    [RESERVED_FILTER_IDS.PROJECT_VOLUME]: volume,
  } = filterState;

  const itemTypeObjects = itemTypes ? (itemTypes as MultiFilterValue) : [];
  const currentProjectItemTypes = itemTypeObjects.length
    ? itemTypeObjects.map(getValueFromOption)
    : projectItemTypes;
  const currentProjectVolume = volume
    ? (volume as SingleFilterValue).value
    : '';

  const supplierName =
    supplier && ((supplier as SingleFilterValue).value as string);

  const supplierData = useMemo(() => {
    if (supplierName && currentProjectVolume && projectPriceComponentRoot) {
      const processed = calculateNPForSingleSupplier(
        projectHeatmap,
        projectPriceComponentRoot,
        projectSuppliers,
        currentProjectVolume,
        supplierName
      );

      return processed.sort(sortSupplierData(supplierName));
    }

    return [];
  }, [
    supplierName,
    projectHeatmap,
    projectSuppliers,
    projectPriceComponentRoot,
    currentProjectVolume,
  ]);

  const filteredSupplierData = filterFunction(supplierData);
  const filteredProjectHeatmap = filterFunction(projectHeatmap);
  const secondAxisName = projectSecondAxis.length ? projectSecondAxis[0] : null;

  const onExportClick = () => {
    onExport({
      currentProjectItemTypes,
      data: filteredSupplierData,
      projectHasHistoricPrices,
      projectHasIncumbentPrices,
      projectUnit,
      secondAxisName,
      supplierName,
      t,
    });
  };

  return (
    <>
      <PageTitle
        title={t('Supplier Feedback')}
        titleColor={colors.textColor}
        rightSide={
          <Filter
            enableClearButton
            {...filterProps}
            onChange={setFilterState}
          />
        }
      />
      {!supplierName && (
        <Row>
          <Card>
            <PlaceholderText text={t('Select a supplier to see data')} />
          </Card>
        </Row>
      )}
      {supplierName && (
        <>
          <Row>
            <BiddingOverviewCard
              supplierData={filteredSupplierData}
              filteredHeatmap={filteredProjectHeatmap}
              volume={currentProjectVolume as string}
              supplierName={supplierName}
              itemTypes={currentProjectItemTypes}
            />
            <BiddingAnalyticsCard
              supplierData={filteredSupplierData}
              filteredHeatmap={filteredProjectHeatmap}
              supplierName={supplierName}
              itemTypes={currentProjectItemTypes}
            />
          </Row>
          <Row>
            <AverageCostCard
              supplierData={filteredSupplierData}
              supplierName={supplierName}
              itemTypes={currentProjectItemTypes}
            />
          </Row>
        </>
      )}
      {supplierName && (
        <Card
          title={t('Price Overview')}
          tooltipInfoMessage={t(
            'The first row in the table below (marked grey) is the summary row where all the values are added up.'
          )}
        >
          <Flex direction="column" align="flex-end">
            <DownloadBox onClick={onExportClick}>{t('Export')}</DownloadBox>
          </Flex>
          <VerticalSpacer height={32} />
          <SupplierNegotiationTable
            data={filteredSupplierData}
            projectCurrency={projectCurrency}
            projectUnit={projectUnit}
            selectedItemTypes={currentProjectItemTypes}
            supplierName={supplierName}
            projectHasHistoricPrices={projectHasHistoricPrices}
            projectHasIncumbentPrices={projectHasIncumbentPrices}
            projectHasItemTypes={projectHasItemTypes}
            projectSecondAxis={projectSecondAxis}
          />
        </Card>
      )}
    </>
  );
};
