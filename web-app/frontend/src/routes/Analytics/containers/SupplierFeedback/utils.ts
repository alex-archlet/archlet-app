const TOTAL_PRICE = 'Total Price';

interface SupplierData {
  [itemType: string]: {
    deviations: {
      [name: string]: number;
    };
  };
}

interface Row {
  suppliers: {
    [supplierName: string]: SupplierData;
  };
  volume: number;
}

const getLargestTotalPriceDeviation = (supplierData: SupplierData) => {
  const allDeviations: number[] = [];

  Object.values(supplierData).forEach(({ deviations }) => {
    allDeviations.push(deviations[TOTAL_PRICE]);
  });

  return Math.max(...allDeviations, 0);
};

export const sortSupplierData = (supplierName: string) => (a: Row, b: Row) => {
  const aData = a.suppliers[supplierName];
  const bData = b.suppliers[supplierName];
  const aDeviation = a.volume * getLargestTotalPriceDeviation(aData);
  const bDeviation = b.volume * getLargestTotalPriceDeviation(bData);

  if (aDeviation > bDeviation) {
    return -1;
  }

  if (aDeviation < bDeviation) {
    return 1;
  }

  return 0;
};
