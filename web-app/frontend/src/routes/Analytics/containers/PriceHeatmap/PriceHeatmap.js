import React, { useMemo } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { useCustomTranslation } from '@utils/translate';
import { roundToDecimals } from '@utils/formatNumber';
import { exportCsv, flattenNestedData } from '@utils/csvexport';
import { colors } from '@utils/uiTheme';

import { projectsSelectors } from '@selectors/projects';
import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';
import { Card } from '@common/components/Card/Card';
import { calculateNegotiationPotential } from '@utils/negotiationPotentialCalculator';
import { PageTitle } from '@common/components/PageTitle/PageTitle';
import { getValueFromOption } from '@common/components/Filter/filterUtils';
import {
  RESERVED_FILTER_IDS,
  useProjectFilter,
} from '@common/components/Filter/filterHooks';
import { Filter } from '@common/components/Filter/Filter';
import { TYPE } from '@common/components/Filter/FilterDropdown/FilterDropdown';
import { getEmptyArrayIfConditionFalse } from '@utils/array';
import { DownloadBox } from '@common/components/DownloadBox/DownloadBox';

import { Flex } from '@common/components/Flex/Flex';
import { ItemsTable } from '../../components/ItemsTable/ItemsTable';
import {
  getItemTypeForSupplier,
  hasItemBidForAnySupplierAndItemTypes,
} from '../../utils/filters';

const TOTAL_PRICE = 'Total Price';

const getSupplierPrice = (projectSuppliers, itemTypes) => {
  const result = [];

  projectSuppliers.forEach((supplierName) => {
    itemTypes.forEach((itemType) => {
      result.push({
        Header: `${supplierName} - ${itemType}`,
        accessor: (value) => {
          const values = getItemTypeForSupplier(value, supplierName, itemType);

          if (!values) {
            return '';
          }

          return roundToDecimals(values.prices[TOTAL_PRICE]);
        },
      });
    });
  });

  return result;
};

const PriceHeatmap = ({
  projectCurrency,
  projectHeatmap,
  projectItemTypes,
  projectPriceComponentRoot,
  projectSuppliers,
  projectUnit,
  projectHasHistoricPrices,
  projectHasIncumbentPrices,
  projectHasItemTypes,
}) => {
  const { t } = useCustomTranslation();

  const filterDeclaration = [
    ...getEmptyArrayIfConditionFalse(
      [
        {
          customFilter: (row, filterState) =>
            hasItemBidForAnySupplierAndItemTypes(
              row,
              filterState,
              projectItemTypes
            ),
          id: RESERVED_FILTER_IDS.PROJECT_ITEM_TYPE,
          label: 'Item Type',
          type: TYPE.MULTI,
        },
      ],
      projectHasItemTypes
    ),
    {
      // don't allow clearing of the item
      clearable: false,
      id: RESERVED_FILTER_IDS.PROJECT_VOLUME,
      label: 'Volume',
      type: TYPE.SINGLE,
    },
    {
      id: RESERVED_FILTER_IDS.PROJECT_SUPPLIERS,
      label: 'Suppliers',
      type: TYPE.MULTI,
    },
  ];

  const {
    filterFunction,
    filterProps,
    filterState,
    setFilterState,
  } = useProjectFilter({
    additionalFilters: filterDeclaration,
    reduxFilterId: 'analytics',
  });

  const {
    [RESERVED_FILTER_IDS.PROJECT_SUPPLIERS]: suppliers,
    [RESERVED_FILTER_IDS.PROJECT_ITEM_TYPE]: itemTypes,
    [RESERVED_FILTER_IDS.PROJECT_VOLUME]: volume,
  } = filterState;
  const currentProjectItemTypes =
    itemTypes && itemTypes.length
      ? itemTypes.map(getValueFromOption)
      : projectItemTypes;
  const currentProjectVolume = volume ? volume.value : '';

  const selectedSuppliers = useMemo(() => {
    if (!suppliers || suppliers.length === 0) {
      return projectSuppliers;
    }

    return suppliers.map(({ value }) => value);
  }, [suppliers, projectSuppliers]);

  const calculatedNegotiationPotential = useMemo(
    () =>
      calculateNegotiationPotential(
        projectHeatmap,
        selectedSuppliers,
        projectPriceComponentRoot,
        currentProjectVolume
      ),
    [
      projectHeatmap,
      selectedSuppliers,
      projectPriceComponentRoot,
      currentProjectVolume,
    ]
  );

  const filteredData = filterFunction(calculatedNegotiationPotential);

  const onExport = () => {
    // define columns for data export
    const columns = [
      {
        Header: t('Item'),
        accessor: value =>
          `${value.itemName ? `${value.itemName} - ` : ''}${value.item}`,
        fixed: true,
      },
      {
        Header: t('Volume'),
        accessor: value => roundToDecimals(value.volume),
      },
      ...getEmptyArrayIfConditionFalse(
        [
          {
            Cell: ({ cell: value }) => value,
            Header: t('Baseline Price'),
            accessor: value =>
              roundToDecimals(value.historicPrice[TOTAL_PRICE]),
          },
        ],
        projectHasHistoricPrices
      ),
      ...getEmptyArrayIfConditionFalse(
        [
          {
            Cell: ({ cell: value }) => value,
            Header: t('Incumbent Price'),
            accessor: value =>
              value.incumbentPrice &&
              roundToDecimals(value.incumbentPrice[TOTAL_PRICE]),
          },
        ],
        projectHasIncumbentPrices
      ),
      {
        Header: t('Best Price'),
        accessor: value => roundToDecimals(value.bestPrice[TOTAL_PRICE]),
      },
      ...getSupplierPrice(selectedSuppliers, currentProjectItemTypes),
    ];

    const flattenedData = flattenNestedData(filteredData);

    exportCsv(flattenedData, columns, 'Price Heatmap');
  };

  return (
    <>
      <PageTitle
        title={t('Price Overview')}
        titleColor={colors.textColor}
        rightSide={
          <Filter
            enableClearButton
            {...filterProps}
            label={t('Filter by')}
            onChange={setFilterState}
          />
        }
      />
      <Card
        title={t('Price Heatmap')}
        tooltipInfoMessage={t(
          'Compare the offers of the different bidders with each other.'
        )}
      >
        <Flex direction="column" align="flex-end">
          <DownloadBox onClick={onExport}>{t('Export')}</DownloadBox>
        </Flex>
        <VerticalSpacer height={32} />
        <ItemsTable
          data={filteredData}
          projectSuppliers={selectedSuppliers}
          projectCurrency={projectCurrency}
          projectUnit={projectUnit}
          projectHasHistoricPrices={projectHasHistoricPrices}
          projectHasIncumbentPrices={projectHasIncumbentPrices}
          itemTypes={currentProjectItemTypes}
          projectHasItemTypes={projectHasItemTypes}
        />
      </Card>
    </>
  );
};

PriceHeatmap.propTypes = {
  projectCurrency: PropTypes.string.isRequired,
  projectHasHistoricPrices: PropTypes.bool.isRequired,
  projectHasIncumbentPrices: PropTypes.bool.isRequired,
  projectHasItemTypes: PropTypes.bool.isRequired,
  projectHeatmap: PropTypes.arrayOf(PropTypes.object).isRequired,
  projectItemTypes: PropTypes.arrayOf(PropTypes.string).isRequired,
  projectPriceComponentRoot: PropTypes.shape({}).isRequired,
  projectSuppliers: PropTypes.arrayOf(PropTypes.string).isRequired,
  projectUnit: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  projectCurrency: projectsSelectors.getProjectCurrencyCode(state),
  projectHasHistoricPrices: projectsSelectors.getProjectHasHistoricPrices(
    state
  ),
  projectHasIncumbentPrices: projectsSelectors.getProjectHasIncumbentPrices(
    state
  ),
  projectHasItemTypes: projectsSelectors.getProjectHasItemTypes(state),
  projectHeatmap: projectsSelectors.getProjectHeatmap(state),
  projectItemTypes: projectsSelectors.getProjectItemTypes(state),
  projectPriceComponentRoot: projectsSelectors.getProjectPriceComponentsRoot(
    state
  ),
  projectSuppliers: projectsSelectors.getProjectSuppliers(state),
  projectUnit: projectsSelectors.getProjectUnitName(state),
});

export const PriceHeatmapContainer = connect(mapStateToProps)(PriceHeatmap);
