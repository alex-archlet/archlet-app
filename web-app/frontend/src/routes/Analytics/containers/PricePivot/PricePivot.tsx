import React, { useMemo, useRef, useCallback, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { debounce } from 'throttle-debounce';

import { PageTitle } from '@common/components/PageTitle/PageTitle';
import { colors } from '@utils/uiTheme';
import { exportCsv } from '@utils/csvexport';
import { useCustomTranslation } from '@utils/translate';
import { Card } from '@common/components/Card/Card';
import { Filter } from '@common/components/Filter/Filter';
import { DownloadBox } from '@common/components/DownloadBox/DownloadBox';
import { Table, CustomColumn } from '@common/components/Table/Table';
import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';
import {
  useFilter,
  useSmartFilter,
} from '@common/components/Filter/filterHooks';
import {
  TYPE,
  getValueFromOption,
} from '@common/components/Filter/filterUtils';
import { biddingRoundsSelectors } from '@store/selectors/biddingRounds';
import { projectDataSelectors } from '@store/selectors/projectData';
import { resetNewUploadData } from '@store/actions/projectData';
import request from '@utils/request';
import { LocaleFormatNumber } from '@common/components/FormatNumber/FormatNumber';
import { projectsSelectors } from '@store/selectors/projects';
import { CancelTokenSource, CancelToken } from 'axios';
import { SingleFilterValue } from '@common/components/Filter/types';
import { useComponentDidUpdate } from '@common/hooks/useComponentDidUpdate';

import { Flex } from '@common/components/Flex/Flex';
import { usePivotTableState } from './usePivotTableState';

const OPERATIONS = {
  AVG: 'mean',
  MAX: 'max',
  MIN: 'min',
  SUM: 'sum',
  WAVG: 'wavg',
};

const isPriceColumn = ({ column }) =>
  column === 'price' || column === 'price_total';

const sortOrderAscending = ({ order: orderA }, { order: orderB }) =>
  orderA - orderB;

const columnToOption = ({ name, id, order }) => ({
  label: name,
  value: id,
  order,
});

const DEBOUNCE_DELAY = 1500;
const MAX_INITIAL_COLUMNS = 5;

type SingleFilterValueWithOrder = SingleFilterValue & {
  order: number;
};

export const PricePivotContainer: React.FC<void> = () => {
  const allBidColumns = useSelector(
    biddingRoundsSelectors.getColumnsForTableDesign
  );
  const projectFilterColumnIds: string[] = useSelector(
    projectsSelectors.getProjectFilterColumnIds
  );
  const { id: projectId } = useParams();
  const { t } = useCustomTranslation();
  const [dataState, setDataState] = usePivotTableState({
    projectId,
  });
  const cancelTokenSource = useRef<CancelTokenSource | null>(null);

  const priceColumns = useMemo(() => allBidColumns.filter(isPriceColumn), [
    allBidColumns,
  ]);
  const nonPriceColumns = useMemo(
    () => allBidColumns.filter(col => !isPriceColumn(col)),
    [allBidColumns]
  );

  const priceOptions = useMemo(
    () => priceColumns.map(columnToOption).sort(sortOrderAscending),
    [priceColumns]
  );

  const nonPriceOptions = useMemo(
    () => nonPriceColumns.map(columnToOption).sort(sortOrderAscending),
    [nonPriceColumns]
  );

  const operations = [
    {
      label: t('Weighted Average'),
      value: OPERATIONS.WAVG,
    },
    {
      label: t('Average'),
      value: OPERATIONS.AVG,
    },
    {
      label: t('Sum'),
      value: OPERATIONS.SUM,
    },
    {
      label: t('Min'),
      value: OPERATIONS.MIN,
    },
    {
      label: t('Max'),
      value: OPERATIONS.MAX,
    },
  ];

  const groupByDeclaration = [
    {
      clearable: true,
      id: 'results',
      label: t('Grouped values'),
      options: priceOptions,
      type: TYPE.MULTI,
    },
    {
      clearable: true,
      id: 'groupBy',
      label: t('Grouped by'),
      options: nonPriceOptions,
      text: t('by'),
      type: TYPE.MULTI,
    },
    {
      clearable: false,
      id: 'operation',
      label: t('Operation'),
      options: operations,
      type: TYPE.SINGLE,
    },
  ];

  const groupByInitialValues = useMemo(() => {
    const isFilterColumnOrSupplier = ({ id, column }) => {
      return column === 'supplier' || projectFilterColumnIds.includes(id);
    };
    return nonPriceColumns.filter(isFilterColumnOrSupplier).map(columnToOption);
  }, [projectFilterColumnIds, nonPriceColumns]);

  const resultsInitialValues = useMemo(() => {
    return priceOptions.slice(0, MAX_INITIAL_COLUMNS);
  }, [priceOptions]);

  const initialValuesMap = useMemo(
    () => ({
      operation: operations,
      results: resultsInitialValues,
      groupBy: groupByInitialValues,
    }),
    [operations, resultsInitialValues, groupByInitialValues]
  );

  const {
    filterProps: groupByProps,
    filterState: groupByState,
    setFilterState: setGroupByState,
  } = useFilter({
    initialValuesMap,
    filterDeclaration: groupByDeclaration,
    reduxFilterId: `${projectId}_pivot_group_by`,
  });

  const fetchData = useCallback(
    debounce(
      DEBOUNCE_DELAY,
      async (
        groupBy: string[],
        results: string[],
        cancelToken: CancelToken
      ) => {
        const { data } = await request.post(
          `projects/${projectId}/pivot_table`,
          {
            groupBy,
            results,
          },
          { cancelToken }
        );

        setDataState(data);
      }
    ),
    [setDataState]
  );

  const { groupBy, results, operation } = groupByState;

  const groupByArray =
    (groupBy as SingleFilterValueWithOrder[]) ??
    ([] as SingleFilterValueWithOrder[]);
  const resultsArray =
    (results as SingleFilterValueWithOrder[]) ??
    ([] as SingleFilterValueWithOrder[]);

  const groupByIds = useMemo(
    () => groupByArray.sort(sortOrderAscending).map(getValueFromOption) || [],
    [groupBy]
  );
  const resultsIds = useMemo(
    () => resultsArray.sort(sortOrderAscending).map(getValueFromOption) || [],
    [results]
  );

  const isNewFilesUploaded = useSelector(projectDataSelectors.getNewFilesUpload);     
  const dispatch = useDispatch();

  useComponentDidUpdate(() => {
    if (cancelTokenSource.current) {
      cancelTokenSource.current.cancel();
      cancelTokenSource.current = null;
    }
    if (!groupByIds.length || !resultsIds.length) {
      setDataState([]);
    } else {
      const source = request.getCancelTokenSource();
      cancelTokenSource.current = source;
      fetchData(groupByIds, resultsIds, source.token);
    }
  }, [groupByIds, resultsIds, isNewFilesUploaded]);  

  useEffect(() => {   
    if (isNewFilesUploaded) {     
      dispatch(resetNewUploadData());
      fetchData(groupByIds, resultsIds, request.getCancelTokenSource().token);
    }
  }, [isNewFilesUploaded]);

  const tableColumns = useMemo(() => {
    const columns: CustomColumn[] = [];

    if (groupBy) {
      const groupByColumns = groupByArray.map(col => ({
        Header: col.label,
        accessor: col.value as string,
        id: col.value as string,
      }));
      columns.push(...groupByColumns);
    }

    if (results && operation) {
      const resultColumns = resultsArray.map(col => ({
        Cell: ({ cell: { value } }) => (
          <LocaleFormatNumber value={value} type={{ decimals: 2 }} />
        ),
        Header: col.label,
        accessor: row =>
          row[col.value] &&
          row[col.value][(operation as SingleFilterValue).value],
        id: col.value as string,
      }));
      columns.push(...resultColumns);
    }

    return columns;
  }, [groupBy, results, operation]);

  const filterDeclaration = useMemo(() => {
    if (groupByArray.length) {
      return groupByArray.sort(sortOrderAscending).map(({ label, value }) => {
        return {
          accessor: value as string,
          clearable: true,
          id: value as string,
          label: label as string,
          type: TYPE.MULTI,
        };
      });
    }
    return [];
  }, [groupBy]);

  const { filterProps, filterFunction, setFilterState } = useSmartFilter({
    filterCombinations: dataState,
    filterDeclaration,
    reduxFilterId: `${projectId}_pivot_filter`,
  });

  const filteredData = filterFunction(dataState);

  const onExport = () => {
    exportCsv(filteredData, tableColumns, 'Price Pivot');
  };

  return (
    <>
      <PageTitle
        title={t('Price Pivot')}
        titleColor={colors.textColor}
        rightSide={
          <Filter
            {...groupByProps}
            label={t('Group')}
            onChange={setGroupByState}
            enableClearButton
          />
        }
      />
      <Card>
        <Flex direction="column" align="flex-end">
          <DownloadBox onClick={onExport}>{t('Export')}</DownloadBox>
        </Flex>
        <Filter
          {...filterProps}
          label={t('Filter by')}
          onChange={setFilterState}
          enableClearButton
        />
        <VerticalSpacer height={16} />
        <Table
          columns={tableColumns}
          data={filteredData}
          getUniqueKey={(_, key) => key.toString()}
          maxHeight="60vh"
          stickyHeader
        />
      </Card>
    </>
  );
};
