import { useDispatch, useSelector } from 'react-redux';
import { setProjectPivotTableState } from '@store/actions/pivotTable';
import { ProjectPivotTableRow } from '@store/reducers/pivotTable';
import { pivotTableSelectors } from '@store/selectors/pivotTable';

interface UsePivotTableParams {
  projectId: string;
}

const EMPTY_STATE = [];

type SetStateFunction = (data: ProjectPivotTableRow[]) => void;

type UsePivotTableReturn = [ProjectPivotTableRow[], SetStateFunction];

export const usePivotTableState = ({
  projectId,
}: UsePivotTableParams): UsePivotTableReturn => {
  const pivotTableMap = useSelector(pivotTableSelectors.getPivotTableMap);
  const state = pivotTableMap[projectId] ?? EMPTY_STATE;
  const dispatch = useDispatch();
  const setState = (data: ProjectPivotTableRow[]) => {
    dispatch(setProjectPivotTableState(projectId, data));
  };

  return [state, setState];
};
