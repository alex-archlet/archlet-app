import React, { useMemo } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { projectsSelectors } from '@selectors/projects';
import { roundToDecimals } from '@utils/formatNumber';
import { exportCsv, flattenNestedData } from '@utils/csvexport';
import { SORTING_TYPE, Table } from '@common/components/Table/Table';
import { Card } from '@common/components/Card/Card';
import { PageTitle } from '@common/components/PageTitle/PageTitle';
import { useCustomTranslation } from '@utils/translate';
import {
  createNestedRankingItems,
  NestedRankingItem,
} from '@utils/supplierRankingCalculator';
import { TYPE } from '@common/components/Filter/FilterDropdown/FilterDropdown';
import {
  RESERVED_FILTER_IDS,
  useProjectFilter,
} from '@common/components/Filter/filterHooks';
import {
  SingleFilterValue,
  MultiFilterValue,
  FilterDeclaration,
} from '@common/components/Filter/types';
import { Filter } from '@common/components/Filter/Filter';
import { VerticalSpacer } from '@common/components/VerticalSpacer/VerticalSpacer';
import { colors } from '@utils/uiTheme';
import { getEmptyArrayIfConditionFalse } from '@utils/array';
import { getValueFromOption } from '@common/components/Filter/filterUtils';
import { DownloadBox } from '@common/components/DownloadBox/DownloadBox';
import { LocaleFormatNumber } from '@common/components/FormatNumber/FormatNumber';
import { Flex } from '@common/components/Flex/Flex';

const TOTAL_PRICE = 'Total Price';

const INITIAL_PRICE_COMPONENT = {
  label: TOTAL_PRICE,
  value: TOTAL_PRICE,
};

const DEFAULT_SORTING = [
  {
    desc: false,
    id: 'item',
  },
];

const StyledContainer = styled.div`
  width: 100%;
  overflow-x: auto;
  white-space: nowrap;
`;

interface Props {
  projectCurrency: string;
  projectCustomColumns: string[];
  projectHasItemTypes: boolean;
  projectHeatmap: object[];
  projectItemTypes: string[];
  projectSuppliers: string[];
}

interface FilterState {
  projectCostComponent?: SingleFilterValue;
  projectItemType?: MultiFilterValue;
}

const SupplierRanking: React.FC<Props> = ({
  projectCurrency,
  projectCustomColumns,
  projectItemTypes,
  projectHeatmap,
  projectHasItemTypes,
  projectSuppliers,
}) => {
  const { t } = useCustomTranslation();

  const filterDeclaration: FilterDeclaration[] = [
    ...getEmptyArrayIfConditionFalse(
      [
        {
          id: RESERVED_FILTER_IDS.PROJECT_ITEM_TYPE,
          label: 'Item Type',
          type: TYPE.MULTI,
        },
      ],
      projectHasItemTypes
    ),
    {
      clearable: false,
      id: RESERVED_FILTER_IDS.PROJECT_COST_COMPONENT,
      label: 'Cost Component',
      type: TYPE.SINGLE,
    },
  ];

  const {
    filterFunction,
    filterProps,
    filterState,
    setFilterState,
  } = useProjectFilter({
    additionalFilters: filterDeclaration,
    initialState: {
      [RESERVED_FILTER_IDS.PROJECT_COST_COMPONENT]: INITIAL_PRICE_COMPONENT,
    },
    reduxFilterId: 'analytics',
  });

  const UNIT_PRICE_SUB_TEXT = `Unit Price (${projectCurrency})`;
  const {
    // @ts-ignore think of a proper generic typing solution
    [RESERVED_FILTER_IDS.PROJECT_ITEM_TYPE]: itemTypes,
    // @ts-ignore think of a proper generic typing solution
    [RESERVED_FILTER_IDS.PROJECT_COST_COMPONENT]: costComponent,
  } = (filterState as unknown) as FilterState;

  const currentProjectItemTypes = itemTypes?.length
    ? itemTypes.map(getValueFromOption)
    : projectItemTypes;
  const currentProjectPriceComponent = costComponent
    ? costComponent.value
    : INITIAL_PRICE_COMPONENT.value;

  const rankingData: NestedRankingItem[] = useMemo(
    () =>
      createNestedRankingItems(
        projectHeatmap,
        projectSuppliers,
        currentProjectItemTypes,
        currentProjectPriceComponent
      ),
    [
      projectHeatmap,
      projectSuppliers,
      currentProjectItemTypes,
      currentProjectPriceComponent,
    ]
  );

  const columns = useMemo(() => {
    const values = [
      {
        Header: t('Item'),
        accessor: 'item_id',
        id: 'item',
        tooltip: true,
        fixed: true,
      },
      {
        Header: t('Ranking'),
        accessor: 'ranking',
        id: 'ranking',
        sortType: SORTING_TYPE.NUMBER,
      },
      {
        Header: t('Supplier'),
        accessor: 'supplierName',
        id: 'supplierName',
        tooltip: true,
      },
      ...(projectHasItemTypes
        ? [
            {
              Header: t('Item Type'),
              accessor: 'itemType',
              id: 'itemType',
            },
          ]
        : []),
      {
        Cell: ({ cell: { value } }) => <LocaleFormatNumber value={value} />,
        Header: t('Price'),
        HeaderSub: UNIT_PRICE_SUB_TEXT,
        accessor: value =>
          value.supplierValues &&
          value.supplierValues.prices[currentProjectPriceComponent],
        id: 'price',
        sortType: SORTING_TYPE.NUMBER,
      },
    ];

    if (projectCustomColumns) {
      const customColumnsValues = projectCustomColumns.map(col => ({
        Header: col,
        accessor: value =>
          value.supplierValues &&
          value.supplierValues.custom_columns &&
          value.supplierValues.custom_columns[col],
        id: col,
        sortType: SORTING_TYPE.STRING,
      }));

      // @ts-ignore TODO: Fix column definitions to work with our Column type definition
      values.push(...customColumnsValues);
    }

    return values;
  }, [
    UNIT_PRICE_SUB_TEXT,
    projectCustomColumns,
    t,
    projectHasItemTypes,
    currentProjectPriceComponent,
  ]);

  const filteredData = filterFunction(rankingData);

  const onExport = () => {
    const exportColumns = [
      {
        Header: t('Item'),
        accessor: 'item_id',
      },
      {
        Header: t('Ranking'),
        accessor: 'ranking',
      },
      {
        Header: t('Supplier'),
        accessor: 'supplierName',
      },
      ...(projectHasItemTypes
        ? [
            {
              Header: t('Item Type'),
              accessor: 'itemType',
            },
          ]
        : []),
      {
        Header: t('Price'),
        accessor: value =>
          value.supplierValues &&
          roundToDecimals(
            value.supplierValues.prices[currentProjectPriceComponent]
          ),
      },
    ];

    if (projectCustomColumns) {
      const customColumnsValues = projectCustomColumns.map(col => ({
        Header: col,
        accessor: value =>
          value.supplierValues &&
          value.supplierValues.custom_columns &&
          value.supplierValues.custom_columns[col],
      }));

      exportColumns.push(...customColumnsValues);
    }

    const flattenedData = flattenNestedData(filteredData);

    exportCsv(flattenedData, exportColumns, 'Ranking');
  };

  return (
    <>
      <PageTitle
        title={t('Supplier Ranking')}
        titleColor={colors.textColor}
        rightSide={
          <Filter
            enableClearButton
            {...filterProps}
            label={t('Filter by')}
            onChange={setFilterState}
          />
        }
      />
      <Card>
        <Flex direction="column" align="flex-end">
          <DownloadBox onClick={onExport}>{t('Export')}</DownloadBox>
        </Flex>
        <VerticalSpacer height={32} />
        <StyledContainer>
          <Table
            getUniqueKey={({ supplierName, item_id, itemType }) =>
              `${supplierName}_${item_id}_${itemType}`
            }
            data={filteredData}
            columns={columns}
            defaultSorting={DEFAULT_SORTING}
            maxHeight="60vh"
            stickyHeader
            expandable
          />
        </StyledContainer>
      </Card>
    </>
  );
};

const mapStateToProps = state => ({
  projectCurrency: projectsSelectors.getProjectCurrencyCode(state),
  projectCustomColumns: projectsSelectors.getProjectCustomColumns(state),
  projectHasItemTypes: projectsSelectors.getProjectHasItemTypes(state),
  projectHeatmap: projectsSelectors.getProjectHeatmap(state),
  projectItemTypes: projectsSelectors.getProjectItemTypes(state),
  projectSuppliers: projectsSelectors.getProjectSuppliers(state),
});

export const SupplierRankingContainer = connect(mapStateToProps)(
  SupplierRanking
);
