import { RESERVED_FILTER_IDS } from '@common/components/Filter/filterHooks';
import { getValueFromOption } from '@common/components/Filter/filterUtils';

export const getItemTypeForSupplier = (item, supplierName, itemType) => item.suppliers[supplierName] && item.suppliers[supplierName][itemType];

const getSupplierNames = item => Object.keys(item.suppliers);

export const hasItemBidForAnySupplierAndItemTypes = (item, filterState, projectItemTypes) => {
  const {
    [RESERVED_FILTER_IDS.PROJECT_ITEM_TYPE]: itemTypes,
  } = filterState;
  const selectedOrAllItemTypes = itemTypes && itemTypes.length > 0 ? itemTypes.map(getValueFromOption) : projectItemTypes;
  const supplierNames = getSupplierNames(item);

  return supplierNames.some(supplierName => selectedOrAllItemTypes.some(itemType => getItemTypeForSupplier(item, supplierName, itemType)));
};
