import en from '../en/en.json';
import de from '../de/de.json';

describe('translation', () => {
  const enKeys = Object.keys(en).sort();
  const deKeys = Object.keys(de).sort();

  // TODO: Unskip me
  it.skip('translation keys to be equal', () => {
    const enArr = [];

    enKeys.forEach((enKey) => {
      if (!de[enKey]) {
        enArr.push(enKey);
      }
    });

    expect(enArr).toHaveLength(0);

    const deArr = [];

    deKeys.forEach((deKey) => {
      if (!en[deKey]) {
        deArr.push(deKey);
      }
    });

    expect(deArr).toHaveLength(0);
  });
  const enKeysValues = Object.entries(en);
  const deKeysValues = Object.entries(de);

  it('Check translation that have been scan but not translated yet', () => {
    const enArr = [];

    enKeysValues.forEach(([key, value]) => {
      if (value === '__STRING_NOT_TRANSLATED__') {
        enArr.push(key);
      }
    });

    expect(enArr).toHaveLength(0);

    const deArr = [];

    deKeysValues.forEach(([key, value]) => {
      if (value === '__STRING_NOT_TRANSLATED__') {
        deArr.push(key);
      }
    });

    expect(deArr).toHaveLength(0);
  });
});
