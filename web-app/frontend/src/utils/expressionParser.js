/* eslint-disable sort-keys */
import { Parser } from 'expr-eval';

const PARSER_OPTIONS = {
  operators: {
    // These default to true, but are included to be explicit
    add: true,
    // concatenate: true,
    // conditional: true,
    divide: true,
    // factorial: true,
    multiply: true,
    // power: true,
    // remainder: true,
    subtract: true,

    // Disable and, or, not, <, ==, !=, etc.
    logical: false,
    comparison: false,

    // Disable 'in' and = operators
    in: false,
    assignment: false,
  },
};

const parser = new Parser(PARSER_OPTIONS);

export const parseExpression = (expression) => {
  const result = parser.parse(expression);

  // console.log('parseExpression', result, result.toString());

  return result;
};

export const isValidExpression = (expression) => {
  try {
    parseExpression(expression);

    return true;
  } catch (err) {
    return false;
  }
};

