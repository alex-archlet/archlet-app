import { useTranslation } from 'react-i18next';
import { default as i18next }  from 'i18next';

export const useCustomTranslation = () => useTranslation();
export const i18n = i18next;
