import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';
import { removeSpinner, setSpinner } from '@actions/interface';
import { axiosInstance, BACKEND_REST_REQUEST_PREFIX } from './request';

interface CustomAxiosRequestConfig extends AxiosRequestConfig {
  loader?: boolean;
}

interface CustomAxiosResponse extends AxiosResponse {
  config: CustomAxiosRequestConfig;
}

interface ApiErrorBody {
  message?: string;
  errors?: {
    [key: string]: string[];
  };
}

type ApiErrorResponse = AxiosResponse<ApiErrorBody>;

const parseError = (error: AxiosError) => {
  const errorResponse: ApiErrorResponse | undefined = error.response;

  if (!errorResponse) {
    return error;
  }

  if (errorResponse?.data === 'object' || errorResponse?.data?.errors) {
    errorResponse.data.errors = {};
  }

  return error;
};

export const initRequestInterceptors = (store) => {
  const baseURL = BACKEND_REST_REQUEST_PREFIX;

  axiosInstance.defaults.baseURL = baseURL;

  axiosInstance.interceptors.request.use(
    async (config: CustomAxiosRequestConfig) => {
      if (config?.loader !== false) {
        setSpinner()(store.dispatch);
      }

      return {
        ...config,
        data: config.data,
        params: config.params,
      };
    }
  );

  axiosInstance.interceptors.response.use(
    (response: CustomAxiosResponse) => {
      if (response.config.loader !== false) {
        setTimeout(() => {
          removeSpinner()(store.dispatch);
        }, 300);
      }

      return response;
    },
    (error) => {
      const isCancel = axios.isCancel(error);

      if (isCancel || error?.config?.loader !== false) {
        setTimeout(() => {
          removeSpinner()(store.dispatch);
        }, 300);
      }

      if (isCancel) {
        return Promise.reject(error);
      }

      return Promise.reject(parseError(error));
    }
  );
};
