import React from 'react';
import { CheckCircle, Error, Warning } from '@material-ui/icons';
import styled from 'styled-components';
import { colors } from '@utils/uiTheme';

export const StyledError = styled(Error)`
  && {
    fill: ${colors.error};
    width: 18px;
    height: 18px;
    margin-left: 8px;
  }
`;

const StyledCheckCircle = styled(CheckCircle)`
  && {
    fill: ${colors.lightGreen};
    width: 18px;
    height: 18px;
    margin-left: 8px;
  }
`;

export const StyledWarning = styled(Warning)`
  && {
    fill: ${colors.warning};
    width: 18px;
    height: 18px;
    margin-left: 8px;
  }
`;

export const getCardStyle = (data) => {
  if (data && data.errors !== 0) {
    return {
      icon: <StyledError />,
      type: 'error',
    };
  }

  if (data && data.warnings !== 0) {
    return {
      icon: <StyledWarning />,
      type: 'warning',
    };
  }

  if (data) {
    return {
      icon: <StyledCheckCircle />,
      type: 'valid',
    };
  }

  return { type: 'default' };
};
