import { getEmptyArrayIfConditionFalse } from './array';

describe('array utils', () => {
  describe('getEmptyArrayIfConditionFalse', () => {
    const value = [''];

    it('should return empty array if condition is false', () => {
      expect(getEmptyArrayIfConditionFalse(value, false)).toHaveLength(0);
    });
    it('should return original array if condition is true', () => {
      expect(getEmptyArrayIfConditionFalse(value, true)).toBe(value);
    });
  });
});
