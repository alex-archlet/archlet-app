import { formatNumber } from './formatNumber';

describe('formatNumber', () => {
  const swissConfig = { locale: 'de-CH' };
  const percentageConfig = { type: 'percent' };

  describe('without config', () => {
    it('value is zero return zero', () => expect(formatNumber(0)).toBe('0'));

    it('falsy values should return marker', () =>
      expect(formatNumber(false)).toBe('-'));
    it('falsy values should return marker', () =>
      expect(formatNumber(null)).toBe('-'));
    it('falsy values should return marker', () =>
      expect(formatNumber(undefined)).toBe('-'));

    it('value without decimal that is <= 1000 && value >= -1000', () =>
      expect(formatNumber(10)).toBe('10'));

    it('value is decimal that is <= 1000 && value >= -1000', () =>
      expect(formatNumber(10.1)).toBe('10.1'));

    it('value that is >= 1000', () =>
      expect(formatNumber(100000)).toBe('100,000'));

    it('value is decimal that is >= 1000', () =>
      expect(formatNumber(100000.12)).toBe('100,000'));
  });

  describe('with swiss locale config', () => {
    it('value is decimal that is <= 1000 && value >= -1000', () =>
      expect(formatNumber(10.1, swissConfig)).toBe('10.1'));

    it('value that is >= 1000', () =>
      expect(formatNumber(100000, swissConfig)).toBe(`100’000`));

    it('value is decimal that is >= 1000', () =>
      expect(formatNumber(100000.12, swissConfig)).toBe(`100’000`));
  });

  describe('with config percentage', () => {
    it('value is percents that is <= 1000 && value >= -1000', () =>
      expect(formatNumber(10.1, percentageConfig)).toBe('10%'));

    it('value that is >= 1000', () =>
      expect(formatNumber(100000, percentageConfig)).toBe('100000%'));

    it('value is percent that is >= 1000', () =>
      expect(formatNumber(100000.12, percentageConfig)).toBe('100000%'));
  });

  describe('with config decimal', () => {
    it('value is decimal that is <= 1000 && value >= -1000', () =>
      expect(formatNumber(10.11111, { decimals: 3 })).toBe('10.111'));

    it('value without that is >= 1000', () =>
      expect(formatNumber(1000, { decimals: 3 })).toBe('1,000'));

    it('value is decimal that is >= 1000', () =>
      expect(formatNumber(100000.1245, { decimals: 3 })).toBe('100,000.125'));
  });
});
