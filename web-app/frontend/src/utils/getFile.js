import request from '@utils/request';
import FileDownload from 'js-file-download';

export const getFile = fileId => request
  .get(`files/download/${fileId}`, {
    responseType: 'blob',
  });

export const downloadFile = (fileId, fileName) => getFile(fileId)
  .then(resp => FileDownload(resp.data, `${fileName}.csv`));
