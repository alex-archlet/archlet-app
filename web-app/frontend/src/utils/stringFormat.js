export const truncate = (input, length = 20) =>
  input && input.length > length
    ? `${input.substring(0, length).trim()}...`
    : input;

export const tooltipArrayCSVFormatted = array =>
  array.map((element, index) => {
    if (array.length === index + 1) {
      return element?.label ?? element;
    }

    return element.label ? `${element.label},` : `${element},`;
  });

const getTitleForArray = (values) => {
  const cleanedString = values[0]?.label ?? values[0];
  if (values.length > 1) {
    const otherItemCount = values.length - 1;

    return `${truncate(cleanedString, 17)} + ${otherItemCount}`;
  }

  return truncate(cleanedString);
};

export const getTitleFromValues = (values) => {
  if (Array.isArray(values)) {
    return getTitleForArray(values);
  }

  return truncate(values.label);
};

export const capitalize = string =>
  string && string[0].toUpperCase() + string.slice(1);
