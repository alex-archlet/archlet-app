type PriceHeatmap = any;
type Supplier = string;
type ItemType = string;
type PriceComponent = string;
type Prices = {
  [key: string]: number,
}
type SupplierValues = {
  prices: Prices,
};

type SupplierData = {
  itemType: ItemType,
  supplierName: Supplier,
  supplierValues: SupplierValues,
}

export type NestedRankingItem = PriceHeatmap & SupplierData & {
  _depth: number,
  item: string,
  ranking: number,
  subRows?: NestedRankingItem[],
};

export const createNestedRankingItems = (
  heatmap: PriceHeatmap[],
  suppliers: Supplier[],
  itemTypes: ItemType[],
  priceComponent: PriceComponent
): NestedRankingItem[] => {
  if (!heatmap || !heatmap.length || !suppliers || !suppliers.length || !itemTypes.length || !priceComponent) {
    return [];
  }
  const sortObjectsByTotalPrice = (a: SupplierData, b: SupplierData) => (
    a.supplierValues.prices[priceComponent] - b.supplierValues.prices[priceComponent]);

  const prepareItemObject = (itemObject: PriceHeatmap) => {
    const bids: SupplierData[] = [];

    suppliers
      .forEach((supplierName) => {
        const allSupplierBids = itemObject.suppliers[supplierName];

        itemTypes.forEach((itemType) => {
          const supplierValues = allSupplierBids && allSupplierBids[itemType];

          if (supplierValues && supplierValues.prices[priceComponent]) {
            bids.push({
              itemType,
              supplierName,
              supplierValues,
            });
          }
        });
      });

    // if item has no bids, then return null for not showing later
    if (!bids.length) {
      return null;
    }

    const [bestBid, ...others] = [...bids]
      .filter(Boolean)
      .sort(sortObjectsByTotalPrice);

    const subRows = others.map((otherBid, index) => ({
      ...itemObject,
      ...otherBid,
      _depth: 1,
      item: '', // set item ID to empty string for rendering
      ranking: index + 2, // 0-indexed + 1 & +1 since there is a best one anyway
    }));

    const result = {
      ...itemObject,
      ...bestBid,
      _depth: 0,
      ranking: 1,
      subRows,
    };

    return result;
  };

  const prepareItemObjects = () => heatmap
    .map(item => prepareItemObject(item))
    .filter(Boolean);

  return prepareItemObjects();
};
