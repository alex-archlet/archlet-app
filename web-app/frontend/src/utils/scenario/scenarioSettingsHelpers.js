export const findValueFromIncumbentArray = (value) => {
  const incumbentSupplier = [
    {
      label: 'Standard Scenario',
      value: 'custom',
    },
    {
      label: 'General Incumbency Scenario',
      value: 'general',
    },
    {
      label: 'Item-Level Incumbency Scenario',
      value: 'item_level',
    },
    {
      label: 'Baseline Scenario',
      value: 'baseline',
    },
  ];

  return incumbentSupplier.find(element => element.value === value[0]);
};

export const findValueFromIdArray = (value, round) => {
  const foundRound = round.find(element => element.id === value[0]);
  if (!foundRound) {
    return foundRound;
  }
  return {
    label: foundRound.name,
    value: foundRound.id,
  };
};

export const translateObject = (key, value, projectCurrency, projectUnit) => {
  const limits = {
    equal: 'Exactly',
    exclude_above: 'Exclude above',
    exclude_below: 'Exclude below',
    exclude_exact: 'Exclude exact matches',
    keep_highest: 'Keep highest',
    keep_lowest: 'Keep lowest',
    max: 'At most',
    min: 'At least',
  };

  const multiConstraints = {
    max_percentage: 'Max. % Volume per Supplier',
    max_supplier: 'Max. Number of Suppliers',
    min_percentage: 'Min. % Volume per Supplier',
    min_supplier: 'Min. Number of Suppliers',
  };
  const types = {
    percentage: 'Percentage',
    spend: `Spend (${projectCurrency})`,
    volume: `Volume (${projectUnit})`,
  };

  const mutation = {
    for_all: 'For all',
    for_each: 'For each',
  };

  switch (key) {
    case 'operation':
      return limits[value];
    case 'operations':
      return limits[value];
    case 'constraint':
      return multiConstraints[value];
    case 'unit':
      return types[value];
    case 'mutation':
      return mutation[value];
    default:
      return value;
  }
};

const checkValue = (key) => {
  if (key === undefined || key === null) {
    return null;
  }

  if (key.value) {
    return key.value;
  }

  if (key) {
    return key;
  }

  return null;
};

const translateArrayObjectToValue = (object) => {
  const translatedCardValue = {};

  Object.keys(object).forEach((key) => {
    translatedCardValue[key] = object[key].map(checkValue);
  });

  return translatedCardValue;
};

const translateNumOfSuppliersObject = cardValue => ({
  filters: translateArrayObjectToValue(cardValue.filters),
  mutation: checkValue(cardValue.mutation),
  operation: checkValue(cardValue.operation),
  value: checkValue(cardValue.value),
});

const translateLimitSpendObject = cardValue => ({
  filters: translateArrayObjectToValue(cardValue.filters),
  mutation: checkValue(cardValue.mutation),
  operation: checkValue(cardValue.operation),
  unit: checkValue(cardValue.unit),
  value: checkValue(cardValue.value),
});

const translateExcludedSupplierObject = cardValue => ({
  criteria: checkValue(cardValue.criteria),
  filters: translateArrayObjectToValue(cardValue.filters),
  mutation: checkValue(cardValue.mutation),
  operation: checkValue(cardValue.operation),
  value: checkValue(cardValue.value),
});

const parseMultiScenarioSettings = (key, customScenarios) => {
  if (customScenarios[key].value) {
    return [customScenarios[key].value];
  }

  if (customScenarios[key].length > 0) {
    return customScenarios[key].map(customScenario => customScenario.value);
  }

  return [];
};
const parseSingleScenarioSettings = (key, customScenarios) =>
  customScenarios[key] ? [customScenarios[key].value] : [];

export const getCreateUpdateScenarioPayload = (
  values,
  customScenarioConstraints,
  excludedSuppliers,
  maxSpend,
  numOfSuppliers
) => ({
  excluded_suppliers: excludedSuppliers.map(translateExcludedSupplierObject),
  general_settings: {
    capacities: parseMultiScenarioSettings(
      'capacities',
      customScenarioConstraints
    ),
    incumbent: parseSingleScenarioSettings(
      'incumbent',
      customScenarioConstraints
    ),
    item_type: parseMultiScenarioSettings(
      'itemType',
      customScenarioConstraints
    ),
    partial_allocation: customScenarioConstraints.partialAllocation,
    price_component: parseMultiScenarioSettings(
      'priceComponent',
      customScenarioConstraints
    ),
    rebates: customScenarioConstraints.rebates,
    split_items: customScenarioConstraints.splitItems,
    scenario_id: parseSingleScenarioSettings(
      'scenario',
      customScenarioConstraints
    ),
    round_id: parseSingleScenarioSettings('round', customScenarioConstraints),
    volume: parseMultiScenarioSettings('volume', customScenarioConstraints),
  },
  limit_spend: maxSpend.map(translateLimitSpendObject),
  name: values.name,
  number_of_suppliers: numOfSuppliers.map(translateNumOfSuppliersObject),
});

export const mutation = [
  {
    label: 'For each',
    value: 'for_each',
  },
  {
    label: 'For all',
    value: 'for_all',
  },
];

export const types = [
  {
    label: 'At least',
    value: 'min',
  },
  {
    label: 'At most',
    value: 'max',
  },
  {
    label: 'Exactly',
    value: 'equal',
  },
];

export const limits = [
  {
    label: 'At least',
    value: 'min',
  },
  {
    label: 'At most',
    value: 'max',
  },
];

export const operations = [
  {
    label: 'Exclude above',
    value: 'exclude_above',
  },
  {
    label: 'Exclude below',
    value: 'exclude_below',
  },
  {
    label: 'Exclude exact matches',
    value: 'exclude_exact',
  },
  {
    label: 'Keep highest',
    value: 'keep_highest',
  },
  {
    label: 'Keep lowest',
    value: 'keep_lowest',
  },
];
