type Filters = {
  [key: string]: string[];
}

export const transformFilters = (filters: Filters) => {
  const res = {};
  // eslint-disable-next-line no-restricted-syntax
  for (const [key, value] of Object.entries(filters)) {
    const arr = value.map((item) => {
      return {
        label: item,
        value: item,
      };
    });
    res[key] = arr;
  }
  return res;
};
