import moment from 'moment-timezone';
import { convertDateWithTimezoneToUTC } from '@utils/convertDate';

export const emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
export const passwordRegex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-._]).{8,}$/;

export const validators = {
  email: email =>
    email && emailRegex.test(email) ? undefined : 'Email is invalid',
  json: (value) => {
    try {
      JSON.parse(value);
    } catch (error) {
      return 'This is not a valid JSON';
    }

    return undefined;
  },
  laterThanStart: (value, allValues) => {
    const { startDate, startTimezone, endTimezone } = allValues;

    if (!startDate) {
      return undefined;
    }

    if (
      value &&
      startTimezone &&
      endTimezone &&
      startTimezone !== endTimezone
    ) {
      const startDateWithTimezone = convertDateWithTimezoneToUTC(
        startDate,
        startTimezone
      );
      const valueWithTimezone = convertDateWithTimezoneToUTC(
        value,
        endTimezone
      );

      return moment(valueWithTimezone).isAfter(moment(startDateWithTimezone))
        ? undefined
        : 'End date must be later than the start date';
    }

    return value && moment(value).isAfter(moment(startDate))
      ? undefined
      : 'End date must be later than the start date';
  },
  maxGreaterThanMin: (limitMax, allValues) =>
    !allValues.limitMin ||
    limitMax === '' ||
    parseFloat(limitMax) >= parseFloat(allValues.limitMin)
      ? undefined
      : 'Maximum value must be greater or equal to the minimum value',
  password: password =>
    password && passwordRegex.test(password)
      ? undefined
      : // eslint-disable-next-line max-len
        'Password must be minimum eight characters long, contain at least one uppercase and one lowercase letter, one number and one special character (#?!@$%^&*-._).',
  require: value => (value ? undefined : 'Field is required'),
};
