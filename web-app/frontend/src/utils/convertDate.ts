import moment from 'moment-timezone';

export const convertDateWithTimezoneToUTC = (date, timezone) => {
  if (date && timezone && timezone.utc && timezone.utc.length) {
    const guessedLocalTimezone = moment.tz.guess();

    const timezoneUTCName = timezone.utc[0];
    const dateWithTimezone = moment.tz(moment(date).format('YYYY-MM-DD HH:mm'), timezoneUTCName);
    const convertedDate = dateWithTimezone.tz(guessedLocalTimezone);

    return new Date(convertedDate.toDate()).toISOString();
  }

  return null;
};

export const convertDateWithMonthName = (date: Date | string) => {
  if (date) {
    return moment(date).format('DD MMM, YYYY');
  }

  return null;
};
