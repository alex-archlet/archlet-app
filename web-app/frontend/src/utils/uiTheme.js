import { createMuiTheme } from '@material-ui/core/styles';

export const colors = {
  background: {
    main: '#FBFBFB',
  },
  black: '#000000',
  cancel: '#e6dfdf',
  dark: '#2c2c2c',
  darkWhite: '#fafafa', 
  darkGrey: '#a8a8a8',
  darkBackground: '#FBFBFB',
  greyOutline: '#b0bbc7',
  darkRed: '#B00020',
  default: '#e2e2e2',
  error: '#e64d4d',
  graphs: {
    baseline: '#2b8cbe',
    graphComparison: [
      '#3296ED',
      '#77B9F2',
      '#9D53F2',
      '#C398F5',
      '#26AAA4',
      '#4ED4CD',
      '#F6A452',
      '#FACA9B',
      '#F1546D',
      '#F598A7',
    ],
    savings: '#a8ddb5',
    savingsFromRebates: '#ccebc5',
    supplierDeepDive: ['#3296ED', '#9D53F2'],
  },
  green: '#258145',
  grey: '#e2e2e2',
  highlightBlue: '#2b8cbe',
  iconGrey: '#757575',
  indicationColors: [
    '#3296ED',
    '#77B9F2',
    '#9D53F2',
    '#F6A452',
    '#26AAA4',
    '#4ED4CD',
    '#C398F5',
    '#FACA9B',
    '#F1546D',
    '#F598A7',
  ],
  light: '#ffffff',
  lightGreen: '#67c605',
  lightGrey: '#eef2f4',
  palette: {
    primary: {
      contrastText: '#44566C',
      dark: '#d5d5d5',
      light: '#fff',
      main: '#eef2f4',
    },
    secondary: {
      contrastText: '#fff',
      dark: '#4790a5',
      light: '#6bcae5',
      main: '#57b4d3',
    },
  },
  paragraphText: '#555555',
  red: '#ff0000',
  textColor: '#44566C',
  descriptionColor: '#999',
  valid: '#67c605',
  warning: '#f7b500',
  white: '#ffffff',
};

export const dimensions = {
  headerHeight: 64,
};

export const drawer = {
  width: 285,
  widthSecondLevel: 347,
  widthOnlyIcons: 70,
};

export const animation = {
  duration: {
    enteringScreen: 225,
    leavingScreen: 195,
  },
  timingFunction: {
    easeIn: 'cubic-bezier(0.4, 0, 1, 1)',
    easeInOut: 'cubic-bezier(0.4, 0, 0.2, 1)',
    easeOut: 'cubic-bezier(0.0, 0, 0.2, 1)',
    sharp: 'cubic-bezier(0.4, 0, 0.6, 1)',
  },
};

export const theme = createMuiTheme({
  overrides: {
    MuiSlider: {
      root: {
        padding: '8px 0',
      },
    },
    MuiButton: {
      textPrimary: {
        backgroundColor: colors.grey,
        color: colors.black,
        paddingLeft: '15px',
        paddingRight: '15px',
      },
      textSecondary: {
        '&$disabled': {
          backgroundColor: colors.grey,
        },
        '&:hover': {
          backgroundColor: colors.palette.secondary.dark,
        },
        backgroundColor: colors.palette.secondary.main,
        color: colors.light,
        paddingLeft: '15px',
        paddingRight: '15px',
      },
    },
    MuiExpansionPanel: {
      root: {
        '&:before': {
          display: 'none',
        },
      },
    },
    MuiCard: {
      root: {
        border: `1px solid ${colors.grey}`,
      },
    },
    MuiTypography: {
      h1: {
        fontSize: '102px',
      },
      h2: {
        fontSize: '64px',
      },
      h3: {
        fontSize: '51px',
      },
      h4: {
        fontSize: '36px',
      },
      h5: {
        fontSize: '25px',
      },
      h6: {
        fontSize: '21px',
      },
    },
  },
  palette: {
    ...colors.palette,
    background: {
      default: '#ffffff',
    },
  },
  spacing: 1,
  typography: {
    body1: {
      fontSize: '14px',
    },
  },
});
