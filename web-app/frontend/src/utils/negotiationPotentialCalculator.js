// Structure of the expected output data
// {
//     x, // volume
//     y, // deviation
//     z, // total cost
//     item: string,
//     supplierCount: number,
//     suppliers: [
//         {
//             name: string
//             total: number
//             volume: number - probably coming later
//         }
//     ]
// }
import { normalization, normalizationMaxValue } from './math';

const TOTAL_PRICE = 'Total Price';

const getMinMaxValues = (rawHeatmap, priceComponent) => {
  const total = {
    max: 0,
    maxBP: 0,
    min: Number.MAX_VALUE,
    minBP: Number.MAX_VALUE,
  };

  for (let index = 0; index < rawHeatmap.length; index += 1) {
    if (rawHeatmap[index].deviations[priceComponent] > total.max) {
      total.max = rawHeatmap[index].deviations[priceComponent];
    }

    if (rawHeatmap[index].deviations[priceComponent] < total.min) {
      total.min = rawHeatmap[index].deviations[priceComponent];
    }

    if (
      rawHeatmap[index].bestPrice[priceComponent] * rawHeatmap[index].volume >
      total.maxBP
    ) {
      total.maxBP =
        rawHeatmap[index].bestPrice[priceComponent] * rawHeatmap[index].volume;
    }

    if (
      rawHeatmap[index].bestPrice[priceComponent] * rawHeatmap[index].volume <
      total.minBP
    ) {
      total.minBP =
        rawHeatmap[index].bestPrice[priceComponent] * rawHeatmap[index].volume;
    }
  }

  return total;
};

export const calculateSupplierBarData = (
  currentItem,
  projectPriceComponents
) => {
  const array = [];

  if (!currentItem.suppliers) {
    return null;
  }

  Object.entries(currentItem.suppliers).forEach(
    ([supplierName, supplierData]) => {
      const object = {
        itemTypes: {},
        name: supplierName,
      };

      Object.entries(supplierData).forEach(([itemType, designData]) => {
        projectPriceComponents.forEach((priceComponent) => {
          if (designData.prices && designData.prices[priceComponent] !== null) {
            if (!object.itemTypes[itemType]) {
              object.itemTypes[itemType] = {};
            }
            object.itemTypes[itemType][priceComponent] =
              designData.prices[priceComponent];
            if (designData.second_axis) {
              object.itemTypes[itemType].secondAxis = designData.second_axis;
            }
          }
        });
      });

      array.push(object);
    }
  );

  return {
    itemName: currentItem.item,
    price: array,
  };
};

const findKeyLocale = (object, key) => {
  const allKeys = Object.keys(object);

  return allKeys.find(aKey => aKey.toLowerCase() === key.toLowerCase());
};

const getSupplierKey = (rawItem, supplierName) => {
  const { suppliers } = rawItem;

  if (!suppliers) {
    return null;
  }

  const key = findKeyLocale(suppliers, supplierName);
  const supplierData = suppliers[key];

  if (!supplierData) {
    return null;
  }

  return supplierData;
};

const getItemBestPriceTotal = (rawItem, volumeKey) =>
  rawItem.best_prices[TOTAL_PRICE] * rawItem.volumes[volumeKey];
const getItemHistoricPriceTotal = (rawItem, volumeKey) =>
  rawItem.historic_suppliers.prices[TOTAL_PRICE] * rawItem.volumes[volumeKey];
const getItemIncumbentPriceTotal = (rawItem, volumeKey) =>
  rawItem.incumbent_suppliers.prices[TOTAL_PRICE] * rawItem.volumes[volumeKey];

export const getSupplierData = (rawItem, supplierName, volumeKey) => {
  const supplierData = getSupplierKey(rawItem, supplierName);

  if (supplierData === null) {
    return null;
  }
  let result = {};

  Object.keys(supplierData).forEach((itemType) => {
    if (supplierData[itemType].prices[TOTAL_PRICE]) {
      result = {
        ...result,
        [itemType]: {
          ...supplierData[itemType],
          total:
            rawItem.volumes[volumeKey] *
            supplierData[itemType].prices[TOTAL_PRICE],
        },
      };
    }
  });

  if (Object.keys(result).length > 0) {
    return result;
  }

  return null;
};

export const getSubRowForItemAndCost = (item, priceComponentObject, depth) => {
  const { name: priceComponent } = priceComponentObject;
  const bestPrice = item.bestPrice[priceComponent];

  const result = {
    _depth: depth,
    bestPrice: {
      [TOTAL_PRICE]: bestPrice,
    },
    deviations: {
      [TOTAL_PRICE]: item.deviations[priceComponent],
    },
    historicPrice: {
      [TOTAL_PRICE]: item.historicPrice[priceComponent],
    },
    incumbentPrice: {
      [TOTAL_PRICE]: item.incumbentPrice[priceComponent],
    },
    item: priceComponent,
    itemName: item.item,
    id: `${item.item}_${priceComponent}`,
    suppliers: {},
    volume: item.volume,
  };

  Object.entries(item.suppliers).forEach(([supplierName, supplierData]) => {
    Object.entries(supplierData).forEach(([itemType, designData]) => {
      const priceComponentExists =
        designData.prices[priceComponent] !== null &&
        designData.prices[priceComponent] !== undefined;

      if (priceComponentExists) {
        if (!result.suppliers[supplierName]) {
          result.suppliers[supplierName] = {};
        }

        result.suppliers[supplierName][itemType] = {
          deviations: {
            [TOTAL_PRICE]: designData.deviations[priceComponent],
          },
          item: priceComponent,
          prices: {
            [TOTAL_PRICE]: designData.prices[priceComponent],
          },
        };
      }
    });
  });

  if (Object.keys(result.suppliers).length === 0) {
    return null;
  }

  const subRows = priceComponentObject.children
    .map(pc => getSubRowForItemAndCost(item, pc, depth + 1))
    .filter(Boolean);

  result.subRows = subRows;

  return result;
};

const processSingleItem = (
  rawItem,
  supplierNames,
  rootPriceComponent,
  index,
  volumeKey
) => {
  const totalCost = getItemBestPriceTotal(rawItem, volumeKey);
  const historicPrices = rawItem.historic_suppliers
    ? rawItem.historic_suppliers.prices
    : {};
  const historicSupplier = rawItem.historic_suppliers
    ? rawItem.historic_suppliers.supplier
    : {};
  const historicSecondAxis =
    rawItem.historic_suppliers && rawItem.historic_suppliers.second_axis
      ? rawItem.historic_suppliers.second_axis
      : {};
  const historicTotalCost = rawItem.historic_suppliers
    ? getItemHistoricPriceTotal(rawItem, volumeKey)
    : 0;
  const incumbentPrices = rawItem.incumbent_suppliers
    ? rawItem.incumbent_suppliers.prices
    : {};
  const incumbentTotalCost = rawItem.incumbent_suppliers
    ? getItemIncumbentPriceTotal(rawItem, volumeKey)
    : 0;
  const { deviations } = rawItem;

  const suppliers = {};

  supplierNames.forEach((supplierName) => {
    const supplierData = getSupplierData(rawItem, supplierName, volumeKey);

    if (supplierData) {
      suppliers[supplierName] = supplierData;
    }
  });

  if (Object.keys(suppliers).length === 0) {
    return null;
  }

  const result = {
    _depth: 0,
    action: [], // getActions(rawItem),
    bestPrice: rawItem.best_prices,
    historicSupplier,
    bestPriceSupplier: rawItem.best_price_supplier,
    deviations,
    filters: rawItem.filters,
    historic: historicTotalCost,
    historicPrice: historicPrices,
    historicSecondAxis,
    incumbentPrice: incumbentPrices,
    incumbentTotal: incumbentTotalCost,
    index,
    item: rawItem.item_id,
    itemName: '',
    id: rawItem.item_id,
    supplierCount: Object.keys(suppliers).length,
    suppliers,
    total: totalCost,
    volume: rawItem.volumes[volumeKey],
  };

  const subRows = rootPriceComponent.children
    .map(priceComponent => getSubRowForItemAndCost(result, priceComponent, 1))
    .filter(Boolean);

  result.subRows = subRows;

  return result;
};

/**
 * Calculate negotiation potential from raw data
 * @param {*} rawHeatmap - raw heatmap data about the items
 * @param {*} suppliers - array of string supplier names
 */
export const calculateNegotiationPotential = (
  rawHeatmap,
  suppliers,
  priceComponents,
  volumeKey
) => {
  if (!rawHeatmap || !rawHeatmap.length || !suppliers || !suppliers.length) {
    return [];
  }

  return rawHeatmap
    .map((item, index) =>
      processSingleItem(item, suppliers, priceComponents, index, volumeKey)
    )
    .filter(Boolean);
};

export const calculateNPForSingleSupplier = (
  rawHeatmap,
  rootPriceComponent,
  allSuppliers,
  volumeKey,
  selectedSupplier
) => {
  const supplierItems = rawHeatmap.filter(item =>
    getSupplierKey(item, selectedSupplier)
  );

  return calculateNegotiationPotential(
    supplierItems,
    allSuppliers,
    rootPriceComponent,
    volumeKey
  );
};

const processSingleMatrixData = (
  item,
  minMaxValues,
  translatedPriceComponent
) => {
  const itemBP = item.bestPrice[translatedPriceComponent] * item.volume;
  const normalizationBestPrice = normalization(
    minMaxValues.maxBP,
    minMaxValues.minBP,
    itemBP,
    1,
    0
  );

  const normalizationDeviation = normalization(
    minMaxValues.max,
    minMaxValues.min,
    item.deviations[translatedPriceComponent],
    1,
    0
  );

  const normalizationNumberOfSuppliers = normalizationMaxValue(
    item.supplierCount,
    10,
    1,
    0.1
  );

  const scale = (normalizationBestPrice + normalizationDeviation) / 2;

  return {
    ...item,
    normalizationBestPrice,
    normalizationDeviation,
    normalizationNumberOfSuppliers,
    scale,
  };
};

export const calculateMatrixNegotiationPotential = (
  calculatedHeatmap,
  currentProjectPriceComponent
) => {
  const minMaxValues = getMinMaxValues(
    calculatedHeatmap,
    currentProjectPriceComponent
  );

  return calculatedHeatmap.map(item =>
    processSingleMatrixData(item, minMaxValues, currentProjectPriceComponent)
  );
};
