import { isValidExpression, parseExpression } from './expressionParser';

describe('ExpressionParser', () => {
  describe('parseExpression', () => {
    it('should parse simple expression', () => {
      parseExpression('1');
      parseExpression('(1) * (1)');
      parseExpression('-1');
      parseExpression('+1');
    });
    it('should parse simple expression with variable', () => {
      parseExpression('x');
      parseExpression('ASD');
      parseExpression('+ASD');
      parseExpression('-ASD');
      parseExpression('(ASD) * (1)');
    });
    it('should throw on on invalid expression', () => {
      expect(() => parseExpression('')).toThrow();
      expect(() => parseExpression('1 -')).toThrow();
      expect(() => parseExpression('1 )')).toThrow();
      expect(() => parseExpression('(')).toThrow();
      expect(() => parseExpression('+')).toThrow();
    });
  });
  describe('isValidExpression', () => {
    it('should return true for valid expression', () => {
      expect(isValidExpression('1')).toBeTruthy();
      expect(isValidExpression('1 + 1')).toBeTruthy();
      expect(isValidExpression('1 + ASD')).toBeTruthy();
      expect(isValidExpression('1 + (A * B)')).toBeTruthy();
    });
    it('should return false for invalid expression', () => {
      expect(isValidExpression('')).toBeFalsy();
      expect(isValidExpression('1 +')).toBeFalsy();
      expect(isValidExpression('1 )')).toBeFalsy();
      expect(isValidExpression('(')).toBeFalsy();
      expect(isValidExpression('+')).toBeFalsy();
    });
  });
});
