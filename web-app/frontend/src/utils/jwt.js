import jwtDecode from 'jwt-decode';

const tokenKey = 'archlet-token';
const rememberKey = 'archlet-remember-me';

export function setToken(token) {
  localStorage.setItem(tokenKey, token);
}

export function clearToken() {
  localStorage.setItem(tokenKey, '');
}

export function getToken() {
  const token = localStorage.getItem(tokenKey);

  return token || '';
}

export function setRememberMe() {
  localStorage.setItem(rememberKey, true);
}

export function clearRememberMe() {
  localStorage.setItem(rememberKey, '');
}

export function getRememberMe() {
  const rememberMe = localStorage.getItem(rememberKey);

  return rememberMe || '';
}

export function clearLocalStorage() {
  clearToken();
  clearRememberMe();
}

export function tokenExpired(token) {
  if (!token) return true;

  const { exp } = jwtDecode(token);

  return exp - Math.floor(Date.now() / 1000) <= 0;
}

export function tokenCloseToExpiry(token) {
  if (!token) return false;

  const {
    exp,
    orig_iat,
  } = jwtDecode(token);

  return (
    (exp - orig_iat) / 2 > exp - Math.floor(Date.now() / 1000) && !!getToken()
  );
}

export function isAuthenticated() {
  const token = getToken();

  return !tokenExpired(token);
}
