export class CustomError extends Error {
  constructor(message, serverError) {
    super(message);
    this.response = { message };
    this.errorMessage = serverError;
  }
}

export const getErrorMessage = area => (area ?
  `Unknown error occurred during ${area}, please reach out to Archlet support team at support@archlet.ch.` :
  'Unknown error occurred, please reach out to Archlet support team at support@archlet.ch.');

export const categorizeErrors = (data) => {
  const initialValidation = {
    error: [],
    info: [],
    warning: [],
  };

  const validation = data.reduce((prev, curr) => {
    const currentType = curr.severity;

    return {
      ...prev,
      [currentType]: [
        ...prev[currentType],
        curr,
      ],
    };
  }, initialValidation);

  return {
    ...data,
    data: [
      ...validation.error,
      ...validation.warning,
    ],
    errors: validation.error.length,
    warnings: validation.warning.length,
  };
};
