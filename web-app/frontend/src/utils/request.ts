import axios from 'axios';
import { getEnvVariable } from 'src/env';

export const BACKEND_REST_REQUEST_PREFIX = getEnvVariable(
  'BACKEND_REST_REQUEST_PREFIX',
  'http://localhost:4201/api/v1'
);

export const axiosInstance = axios.create({
  withCredentials: true,
});

class Request {
  get = axiosInstance.get;

  post = axiosInstance.post;

  patch = axiosInstance.patch;

  put = axiosInstance.put;

  delete = axiosInstance.delete;

  getCancelTokenSource() {
    return axios.CancelToken.source();
  }
}

export const request = new Request();
export default request;
