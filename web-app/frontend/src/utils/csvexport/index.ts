/**
 * Exports data as CSV.
 *
 * Example usage:
 *
 * import exportCsv from '../utils/csvexport';
 *
 * const data = [
 * {
 *   name: 'Meredith Carney',
 *   age: 23,
 *   friend: {
 *     name: 'Perry Robinson',
 *     age: 33,
 *   },
 * },
 * {
 *   name: 'Savage Weeks',
 *   age: 21,
 *   friend: {
 *     name: 'Tammi Reese',
 *     age: 32,
 *   },
 * }];
 *
 * const columns = [{
 *   Header: 'Name',
 *   accessor: 'name',
 * }, {
 *   Header: 'Age',
 *   accessor: 'age',
 * }, {
 *   Header: 'Friend',
 *   accessor: d => `${d.friend.name}, (${d.friend.age})`,
 * }];
 *
 */

import { CustomColumn } from '@common/components/Table/Table';
import papaparse from 'papaparse';

interface RowWithSubRows {
  subRows: RowWithSubRows[] | undefined;
}

type AccessorFunctionString = (row: object) => string;
type AccessorFunctionNumber = (row: object) => number;
type Accessor = AccessorFunctionString | AccessorFunctionNumber | string;

type Column = Partial<CustomColumn>;

type FilenameParameter = string | undefined;

interface ExportOptions {
  filename: string;
  mimetype: string;
  showHeaders: boolean;
}

type ColumnDataType = number | string | null;

export const replaceNewLines = (datum: ColumnDataType) =>
  datum !== null && datum !== undefined
    ? datum.toString().replace(/[\r\n]+/g, ', ')
    : null;

export const dataForField = (datum: object, accessor: Accessor) =>
  typeof accessor === 'function'
    ? replaceNewLines(accessor(datum))
    : replaceNewLines(datum[accessor]);

export const dataForRow = (datum: object, columns: Column[]) =>
  columns.reduce(
    (acc, col) => acc.concat(dataForField(datum, col.accessor ?? '')),
    [] as ColumnDataType[]
  );

export const processData = (data: object[], columns: Column[]) =>
  data.map(datum => dataForRow(datum, columns));

export const json2csv = (
  data: object[],
  columns: Column[],
  options: ExportOptions
) => {
  const config = {
    header: options.showHeaders,
    delimiter: ';',
  };
  const fields = columns.map(c => c.Header);
  const processedData = processData(data, columns);

  return papaparse.unparse(
    {
      data: processedData,
      fields,
    },
    config
  );
};

// https://github.com/mholt/PapaParse/issues/175#issuecomment-75597039
export const download = (data: string, options: ExportOptions) => {
  const universalBOM = '\uFEFF'; // indicate apps (eg Excel) that this file is in UTF
  const blob = new Blob([universalBOM + data], { type: options.mimetype });

  // Special treatment for IE11 & Edge
  if (window.navigator.msSaveOrOpenBlob) {
    window.navigator.msSaveOrOpenBlob(blob, options.filename);
  } else {
    const url = URL.createObjectURL(blob);
    const downloadLink = document.createElement('a');

    downloadLink.href = url;
    downloadLink.download = options.filename;
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
    URL.revokeObjectURL(url);
  }
};

export const defaultOptions: ExportOptions = {
  filename: 'export.csv',
  mimetype: 'text/csv; charset=utf-8',
  showHeaders: true,
};

const endsWith = (str: string, suffix: string) =>
  str.indexOf(suffix, str.length - suffix.length) !== -1;

const CSV_SUFFIX = '.csv';

const cleanFilename = (text: FilenameParameter) => {
  if (!text) {
    return defaultOptions.filename;
  }
  const clean = text.replace(/[\W_]+/g, '_');

  return endsWith(clean, CSV_SUFFIX) ? clean : `${clean}${CSV_SUFFIX}`;
};

/**
 * Exports data as CSV.
 *
 * @param data Array of objects of data, Array<{[key: String]: any}>)
 * @param columns Column definitions, { header: String, accessor: String | function}
 * @param filename file name on export
 */
export const exportCsv = (
  data: object[],
  columns: Column[],
  filename: FilenameParameter = undefined
) => {
  const options = {
    ...defaultOptions,
    filename: cleanFilename(filename),
  };
  const csv = json2csv(data, columns, options);

  download(csv, options);
};

export const flattenNestedData = (items: RowWithSubRows[]) => {
  const result: RowWithSubRows[] = [];

  items.forEach((item) => {
    result.push(item);

    if (item.subRows) {
      result.push(...flattenNestedData(item.subRows));
    }
  });

  return result;
};
