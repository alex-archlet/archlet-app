import * as csvexport from './index';

const data = [{
  field1: 'value11',
  field2: 'value12',
}, {
  field1: 'value21',
  field2: 'value22',
}];

const columns = [{
  Header: '',
  accessor: 'field1',
}, {
  Header: '',
  accessor: 'field2',
}];

describe('replaceNewLines', () => {
  it('does not crash on null', () => {
    expect(csvexport.replaceNewLines(null)).toStrictEqual(null);
  });

  it('replaces new lines with comma', () => {
    expect(csvexport.replaceNewLines('Big Boss Baby\nOffice\r\n\r\nLOL')).toStrictEqual('Big Boss Baby, Office, LOL');
  });

  it('should not mess up falsy numbers', () => {
    expect(csvexport.replaceNewLines(0)).toStrictEqual('0');
  });
});

describe('dataForField', () => {
  it('accesses field as string', () => {
    expect(csvexport.dataForField({ test: 'value' }, 'test')).toBe('value');
  });

  it('calls accessor', () => {
    const mock = jest.fn();
    const datum = { test: 'value' };

    csvexport.dataForField(datum, mock);
    expect(mock).toHaveBeenCalledWith(datum);
  });
});

describe('dataForRow', () => {
  it('reduces data for row', () => {
    expect(csvexport.dataForRow(data[0], columns)).toStrictEqual(['value11', 'value12']);
  });
});

describe('processData', () => {
  it('reduces data for all rows', () => {
    expect(csvexport.processData(data, columns)).toStrictEqual([['value11', 'value12'], ['value21', 'value22']]);
  });
});
