import { addNumbers, average } from './math';

describe('math util', () => {
  describe('addNumbers', () => {
    it('should work with falsy values', () => {
      expect(addNumbers(0, 0)).toBe(0);
      expect(addNumbers(0, null)).toBe(0);
      expect(addNumbers(0, undefined)).toBe(0);
    });
    it('should work with number values', () => {
      expect(addNumbers(1, 2)).toBe(3);
      expect(addNumbers(-1, 1)).toBe(0);
      expect(addNumbers(0, 1)).toBe(1);
    });
  });
  describe('average', () => {
    it('should get average of falsy list', () => {
      expect(average([])).toBe(0);
      expect(average()).toBe(0);
    });
    it('should get average of list with items', () => {
      expect(average([1])).toBe(1);
      expect(average([1, 0.5])).toBe(0.75);
      expect(average([1, -1])).toBe(0);
    });
  });
});
