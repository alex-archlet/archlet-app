type numberConfig = {
  decimals?: number | null;
  type?: string | null;
  locale?: string;
  showPlusSign?: boolean | null;
};
export interface FormattingConfig extends numberConfig {
  type?: 'percent' | null;
}
export interface FormattingLetterConfig extends numberConfig {
  type?: 'general' | null;
}

export const formatNumber = (
  value: number,
  { decimals, type, locale }: FormattingConfig = {
    decimals: null,
    type: null,
    locale: 'en-GB',
  }
) => {
  if (value === 0) return '0';
  if (!value) return '-';

  let threshold;
  let unit;
  let setDecimals;

  if (type === 'percent') {
    threshold = 1;
    unit = '%';
    setDecimals = decimals || 0;
    return value ? `${(value / threshold).toFixed(setDecimals)}${unit}` : '-';
  }
  if (value && value <= 1000 && value >= -1000) {
    setDecimals = decimals || 2;
    return Number(value.toFixed(setDecimals)).toLocaleString(locale);
  }
  setDecimals = decimals || 0;
  return Number(value.toFixed(setDecimals)).toLocaleString(locale);
};

export const formatNumberWithLetters = (
  value: number,
  { decimals, type, showPlusSign }: FormattingLetterConfig = {
    decimals: null,
    type: null,
    showPlusSign: null,
  }
) => {
  if (value === 0) return '0';

  let treshold;
  let unit;
  let setDecimals;

  if (type === 'general') {
    treshold = 1;
    unit = '';
    setDecimals = decimals ?? 0;
  } else if (value && (value >= 1000000 || value <= -1000000)) {
    treshold = 1000000;
    unit = 'M';
    setDecimals = decimals ?? 2;
  } else if (value && (value >= 1000 || value <= -1000)) {
    treshold = 1000;
    unit = 'K';
    setDecimals = decimals ?? 2;
  } else {
    treshold = 1;
    unit = '';
    setDecimals = decimals ?? 2;
  }

  const prependedSign = showPlusSign && value > 0 ? '+' : '';

  return value
    ? `${prependedSign}${(value / treshold).toFixed(setDecimals)} ${unit}`
    : '-';
};

// round to x decimals and then cast back to number
export const roundToDecimals = (number, decimals = 4) =>
  number && +number.toFixed(decimals);
