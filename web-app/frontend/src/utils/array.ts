export interface GroupedMap<T> {
  [key: string]: T;
}

export function arrayToMap<T>(array: T[], key = 'id'): GroupedMap<T> {
  const result = {};

  if (!array) {
    return result;
  }

  array.forEach((item) => {
    result[item[key]] = item;
  });

  return result;
}

export function getEmptyArrayIfConditionFalse<T>(
  items: T[],
  condition: boolean
): T[] {
  if (!condition) {
    return [];
  }

  return items;
}

export interface GroupedMapOfArray<T> {
  [key: string]: T[];
}

export function groupByKey<T>(array: T[], key = 'id'): GroupedMapOfArray<T> {
  return array.reduce((rv, x) => {
    const map = rv;

    (map[x[key]] = rv[x[key]] || []).push(x);

    return rv;
  }, {});
}

export function groupByCallback<T>(
  array: T[],
  callback: (row: T) => string
): GroupedMapOfArray<T> {
  return array.reduce((rv, x) => {
    const map = rv;
    const key = callback(x);

    (map[key] = rv[key] || []).push(x);

    return rv;
  }, {});
}

export const sum = (array: number[]) =>
  array.reduce((a: number, b: number) => a + b, 0);

export const sumBy = (array: object[], key: string) =>
  array.reduce((total: number, current: object) => total + current[key], 0);

export function splitIntoChunks<T>(array: T[], perChunk: number): T[][] {
  return array.reduce((resultArray: T[][], item: T, index: number) => {
    const chunkIndex = Math.floor(index / perChunk);
    const copy = resultArray;

    if (!resultArray[chunkIndex]) {
      copy[chunkIndex] = []; // start a new chunk
    }

    copy[chunkIndex].push(item);

    return copy;
  }, []);
}

export function getIntersection<T>(a: T[], b: T[]): T[] {
  const setA = new Set(a);
  const setB = new Set(b);
  const intersection = new Set([...setA].filter(x => setB.has(x)));
  return Array.from(intersection);
}

export function arraysIntersect<T>(a: T[], b: T[]): boolean {
  return getIntersection(a, b).length > 0;
}
