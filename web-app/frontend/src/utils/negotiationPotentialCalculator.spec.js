import {
  calculateNegotiationPotential,
  getSupplierData,
} from './negotiationPotentialCalculator';

describe('Negotiation Potential Calculator', () => {
  const supplierName1 = 'Mannei AG';
  const supplierName2 = 'Mannei2 AG';
  const nullSupplierName = 'NULL';
  const nonExistingSupplierName = 'fake';
  const supplierNames = [
    supplierName1,
    supplierName2,
    nullSupplierName,
    nonExistingSupplierName,
  ];
  const rootComponent = {
    children: [],
    name: 'Total Price',
  };
  const logisticsComponent = {
    children: [],
    name: 'Logistics Price',
  };
  const logisticsChildComponent = {
    children: [],
    name: 'Logistics Child Price',
  };
  const totalComponentName = rootComponent.name;
  const logisticsComponentName = logisticsComponent.name;
  const logisticsChildComponentName = logisticsChildComponent.name;
  const volumeComponent2010 = '2010';
  const volumeComponent2011 = '2011';
  const itemType = 'Current Design';
  // const volumeComponents = [volumeComponent2010, volumeComponent2011];
  const item = {
    best_prices: {
      [logisticsChildComponentName]: 5,
      [logisticsComponentName]: 10,
      [totalComponentName]: 100,
    },
    deviations: {
      [logisticsChildComponentName]: null,
      [logisticsComponentName]: null,
      [totalComponentName]: null,
    },
    filters: {
      region: 'Asia',
    },
    historicSecondAxis: {},
    historic_suppliers: {
      prices: {
        [logisticsChildComponentName]: 0.5,
        [logisticsComponentName]: 0.5,
        [totalComponentName]: 0.5,
      },
    },
    incumbent_price: {
      [totalComponentName]: 0.5,
    },
    item_id: 'Item0010',
    suppliers: {
      [nullSupplierName]: {
        [itemType]: {
          deviations: {
            [logisticsChildComponentName]: null,
            [logisticsComponentName]: null,
            [totalComponentName]: null,
          },
          prices: {
            [logisticsChildComponentName]: null,
            [logisticsComponentName]: null,
            [totalComponentName]: null,
          },
        },
      },
      [supplierName1]: {
        [itemType]: {
          deviations: {
            [logisticsChildComponentName]: 0,
            [logisticsComponentName]: 0,
            [totalComponentName]: 0,
          },
          prices: {
            [logisticsChildComponentName]: 5,
            [logisticsComponentName]: 10,
            [totalComponentName]: 100,
          },
        },
      },
      [supplierName2]: {
        [itemType]: {
          deviations: {
            [logisticsChildComponentName]: 0.5,
            [logisticsComponentName]: 0.5,
            [totalComponentName]: 0.5,
          },
          prices: {
            [logisticsChildComponentName]: 10,
            [logisticsComponentName]: 20,
            [totalComponentName]: 200,
          },
        },
      },
    },
    volumes: {
      [volumeComponent2010]: 15,
      [volumeComponent2011]: 20,
    },
  };
  const items = [item];

  describe('getSupplierData', () => {
    it('should get price info if supplier exists', () => {
      expect(getSupplierData(item, supplierName1, volumeComponent2010)).toEqual(
        {
          [itemType]: {
            deviations: {
              [logisticsChildComponentName]: 0,
              [logisticsComponentName]: 0,
              [totalComponentName]: 0,
            },
            prices: {
              [logisticsChildComponentName]: 5,
              [logisticsComponentName]: 10,
              [totalComponentName]: 100,
            },
            total: 1500,
          },
        }
      );
      expect(getSupplierData(item, supplierName2, volumeComponent2010)).toEqual(
        {
          [itemType]: {
            deviations: {
              [logisticsChildComponentName]: 0.5,
              [logisticsComponentName]: 0.5,
              [totalComponentName]: 0.5,
            },
            prices: {
              [logisticsChildComponentName]: 10,
              [logisticsComponentName]: 20,
              [totalComponentName]: 200,
            },
            total: 3000,
          },
        }
      );
    });
    it('should get NULL if supplier does not exist or price is null', () => {
      expect(getSupplierData(item, nonExistingSupplierName)).toEqual(null);
      expect(getSupplierData(item, nullSupplierName)).toEqual(null);
    });
  });
  describe('calculateNegotiationPotential', () => {
    it('should get empty result for falsy input', () => {
      expect(calculateNegotiationPotential()).toEqual([]);
      expect(calculateNegotiationPotential(null, null)).toEqual([]);
      expect(calculateNegotiationPotential([], [])).toEqual([]);
    });
    it('should get price info if item has no suppliers', () => {
      const unsuppliedItem = {
        best_prices: {
          [totalComponentName]: 0,
        },
        deviations: {
          [totalComponentName]: 0,
        },
        filters: {
          region: 'Asia',
        },
        historic_prices: {
          [logisticsComponentName]: 0.5,
          [totalComponentName]: 0.5,
        },
        item_id: 'lol',
        volumes: {
          [volumeComponent2010]: 15,
          [volumeComponent2011]: 20,
        },
      };

      expect(
        calculateNegotiationPotential(
          [unsuppliedItem],
          supplierNames,
          logisticsComponent,
          volumeComponent2010
        )
      ).toEqual([]);
    });
    it('should get price info for item for 2 existing suppliers and 1 level price components', () => {
      const expected = [
        {
          _depth: 0,
          action: [],
          bestPrice: item.best_prices,
          deviations: item.deviations,
          filters: {
            region: 'Asia',
          },
          historic: 7.5,
          historicPrice: {
            [logisticsChildComponentName]: 0.5,
            [logisticsComponentName]: 0.5,
            [totalComponentName]: 0.5,
          },
          historicSecondAxis: {},
          bestPriceSupplier: undefined,
          index: 0,
          item: item.item_id,
          itemName: '',
          id: item.item_id,
          subRows: [],
          supplierCount: 2,
          incumbentTotal: 0,
          incumbentPrice: {},
          historicSupplier: undefined,
          suppliers: {
            [supplierName1]: {
              [itemType]: {
                deviations: {
                  [logisticsChildComponentName]: 0,
                  [logisticsComponentName]: 0,
                  [totalComponentName]: 0,
                },
                prices: {
                  [logisticsChildComponentName]: 5,
                  [logisticsComponentName]: 10,
                  [totalComponentName]: 100,
                },
                total: 1500,
              },
            },
            [supplierName2]: {
              [itemType]: {
                deviations: {
                  [logisticsChildComponentName]: 0.5,
                  [logisticsComponentName]: 0.5,
                  [totalComponentName]: 0.5,
                },
                prices: {
                  [logisticsChildComponentName]: 10,
                  [logisticsComponentName]: 20,
                  [totalComponentName]: 200,
                },
                total: 3000,
              },
            },
          },
          total: 1500,
          volume: 15,
        },
      ];

      expect(
        calculateNegotiationPotential(
          items,
          supplierNames,
          rootComponent,
          volumeComponent2010
        )
      ).toEqual(expected);
    });
    it('should get price info for item for 2 existing suppliers and 2 level price components', () => {
      const expected = [
        {
          _depth: 0,
          action: [],
          bestPrice: item.best_prices,
          deviations: item.deviations,
          filters: {
            region: 'Asia',
          },
          historic: 7.5,
          historicPrice: {
            [logisticsChildComponentName]: 0.5,
            [logisticsComponentName]: 0.5,
            [totalComponentName]: 0.5,
          },
          historicSecondAxis: {},
          index: 0,
          incumbentTotal: 0,
          incumbentPrice: {},
          historicSupplier: undefined,
          bestPriceSupplier: undefined,
          item: item.item_id,
          itemName: '',
          id: item.item_id,
          subRows: [
            {
              _depth: 1,
              bestPrice: {
                [totalComponentName]: 10,
              },
              deviations: {
                [totalComponentName]: null,
              },
              historicPrice: {
                [totalComponentName]: 0.5,
              },
              incumbentPrice: {
                [totalComponentName]: undefined,
              },
              item: logisticsComponentName,
              id: `${item.item_id}_${logisticsComponentName}`,
              itemName: item.item_id,
              subRows: [],
              suppliers: {
                [supplierName1]: {
                  [itemType]: {
                    deviations: {
                      [totalComponentName]: 0,
                    },
                    item: logisticsComponentName,
                    prices: {
                      [totalComponentName]: 10,
                    },
                  },
                },
                [supplierName2]: {
                  [itemType]: {
                    deviations: {
                      [totalComponentName]: 0.5,
                    },
                    item: logisticsComponentName,
                    prices: {
                      [totalComponentName]: 20,
                    },
                  },
                },
              },
              volume: 15,
            },
          ],
          supplierCount: 2,
          suppliers: {
            [supplierName1]: {
              [itemType]: {
                deviations: {
                  [logisticsChildComponentName]: 0,
                  [logisticsComponentName]: 0,
                  [totalComponentName]: 0,
                },
                prices: {
                  [logisticsChildComponentName]: 5,
                  [logisticsComponentName]: 10,
                  [totalComponentName]: 100,
                },
                total: 1500,
              },
            },
            [supplierName2]: {
              [itemType]: {
                deviations: {
                  [logisticsChildComponentName]: 0.5,
                  [logisticsComponentName]: 0.5,
                  [totalComponentName]: 0.5,
                },
                prices: {
                  [logisticsChildComponentName]: 10,
                  [logisticsComponentName]: 20,
                  [totalComponentName]: 200,
                },
                total: 3000,
              },
            },
          },
          total: 1500,
          volume: 15,
        },
      ];

      const rootComponentWithOneLevelChildren = {
        ...rootComponent,
        children: [logisticsComponent],
      };

      expect(
        calculateNegotiationPotential(
          items,
          supplierNames,
          rootComponentWithOneLevelChildren,
          volumeComponent2010
        )
      ).toEqual(expected);
    });
    it('should get price info for item for 2 existing suppliers and 3 level price components', () => {
      const expected = [
        {
          _depth: 0,
          action: [],
          bestPrice: item.best_prices,
          deviations: item.deviations,
          filters: {
            region: 'Asia',
          },
          historic: 7.5,
          bestPriceSupplier: undefined,
          historicSecondAxis: {},
          historicPrice: {
            [logisticsChildComponentName]: 0.5,
            [logisticsComponentName]: 0.5,
            [totalComponentName]: 0.5,
          },
          index: 0,
          id: item.item_id,
          incumbentTotal: 0,
          incumbentPrice: {},
          historicSupplier: undefined,
          item: item.item_id,
          itemName: '',
          subRows: [
            {
              _depth: 1,
              bestPrice: {
                [totalComponentName]: 10,
              },
              deviations: {
                [totalComponentName]: null,
              },
              historicPrice: {
                [totalComponentName]: 0.5,
              },
              item: logisticsComponentName,
              id: `${item.item_id}_${logisticsComponentName}`,
              itemName: item.item_id,
              incumbentPrice: {
                [totalComponentName]: undefined,
              },
              subRows: [
                {
                  _depth: 2,
                  bestPrice: {
                    [totalComponentName]: 5,
                  },
                  deviations: {
                    [totalComponentName]: null,
                  },
                  historicPrice: {
                    [totalComponentName]: 0.5,
                  },
                  incumbentPrice: {
                    [totalComponentName]: undefined,
                  },
                  item: logisticsChildComponentName,
                  itemName: item.item_id,
                  id: `${item.item_id}_${logisticsChildComponentName}`,
                  subRows: [],
                  suppliers: {
                    [supplierName1]: {
                      [itemType]: {
                        deviations: {
                          [totalComponentName]: 0,
                        },
                        item: logisticsChildComponentName,
                        prices: {
                          [totalComponentName]: 5,
                        },
                      },
                    },
                    [supplierName2]: {
                      [itemType]: {
                        deviations: {
                          [totalComponentName]: 0.5,
                        },
                        item: logisticsChildComponentName,
                        prices: {
                          [totalComponentName]: 10,
                        },
                      },
                    },
                  },
                  volume: 15,
                },
              ],
              suppliers: {
                [supplierName1]: {
                  [itemType]: {
                    deviations: {
                      [totalComponentName]: 0,
                    },
                    item: logisticsComponentName,
                    prices: {
                      [totalComponentName]: 10,
                    },
                  },
                },
                [supplierName2]: {
                  [itemType]: {
                    deviations: {
                      [totalComponentName]: 0.5,
                    },
                    item: logisticsComponentName,
                    prices: {
                      [totalComponentName]: 20,
                    },
                  },
                },
              },
              volume: 15,
            },
          ],
          supplierCount: 2,
          suppliers: {
            [supplierName1]: {
              [itemType]: {
                deviations: {
                  [logisticsChildComponentName]: 0,
                  [logisticsComponentName]: 0,
                  [totalComponentName]: 0,
                },
                prices: {
                  [logisticsChildComponentName]: 5,
                  [logisticsComponentName]: 10,
                  [totalComponentName]: 100,
                },
                total: 1500,
              },
            },
            [supplierName2]: {
              [itemType]: {
                deviations: {
                  [logisticsChildComponentName]: 0.5,
                  [logisticsComponentName]: 0.5,
                  [totalComponentName]: 0.5,
                },
                prices: {
                  [logisticsChildComponentName]: 10,
                  [logisticsComponentName]: 20,
                  [totalComponentName]: 200,
                },
                total: 3000,
              },
            },
          },
          total: 1500,
          volume: 15,
        },
      ];

      const rootComponentWithOneLevelChildren = {
        ...rootComponent,
        children: [
          {
            ...logisticsComponent,
            children: [logisticsChildComponent],
          },
        ],
      };

      expect(
        calculateNegotiationPotential(
          items,
          supplierNames,
          rootComponentWithOneLevelChildren,
          volumeComponent2010
        )
      ).toEqual(expected);
    });
  });
});
