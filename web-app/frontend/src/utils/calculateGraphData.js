import get from 'lodash.get';
import find from 'lodash.find';

const emptyScenario = [
  {
    baseline: 3.5,
    baselineColor: 'rgba(0,0,0,0)',
    id: 'empty',
  },
];

const getValues = (scenario, region) => get(
  scenario,
  [
    'optimization',
    'output',
    'kpis',
    region,
  ],
  emptyScenario
);

export const calculateGraphData = (scenarios, region = 'all') => {
  const baseline = find(scenarios, scenario => scenario.name === 'Baseline');
  const baseLineValues = getValues(baseline, region);
  let max = 0;
  let base;

  const parsedScenarios = scenarios.map((scenario) => {
    const scenarioValues = getValues(scenario, region);

    const scenarioMax = Math.max(...Object.values(scenarioValues));

    if (scenarioMax) {
      base = scenarioMax > 1000000 ? 1000000 : 1000;
    }

    if (scenarioMax > max) max = scenarioMax;

    const graphData = [
      {
        baseline: baseLineValues.spend / base,
        baselineColor: '#2B8CBE',
        id: 'test',
      },
      {
        diff: scenarioValues.spend / base,
        diffColor: '#DADADA',
        id: 'test2',
        savings: (scenarioValues.savings - scenarioValues.discounts) / base,
        savingsColor: '#A8DDB5',
        savingsFromRebates: scenarioValues.discounts / base,
        savingsFromRebatesColor: '#CCEBC5',
      },
    ];

    return {
      ...scenario,
      graphData,
    };
  });

  max /= base;
  const intMax = (max > 1 ? parseInt(max, 10) : max);
  const step = (10 ** Math.floor(Math.log10(intMax)));
  const ratio = Math.ceil(intMax / step);

  return {
    list: parsedScenarios,
    max: ratio * step,
    ratio,
    step,
    unit: base === 1000000 ? 'M' : 'K',
  };
};
