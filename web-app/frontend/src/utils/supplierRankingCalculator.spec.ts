import { createNestedRankingItems } from './supplierRankingCalculator';

const itemType = 'Current Design';
const totalPriceComponent = 'Total Price';
const otherPriceComponent = 'Other Price';

describe('Supplier Ranking Calculator', () => {
  describe('createNestedRankingItems', () => {
    const supplier_1 = 'supplier_1';
    const supplier_2 = 'supplier_2';
    const supplier_3 = 'supplier_3';
    const allSuppliers = [
      supplier_1,
      supplier_2,
      supplier_3,
    ];
    const item_1_single = {
      item: 'item_1',
      item_id: 'item_1',
      suppliers: {
        [supplier_1]: {
          [itemType]: {
            prices: {
              [totalPriceComponent]: 10,
            },
          },
        },
      },
    };
    const item_1 = {
      item: 'item_1',
      item_id: 'item_1',
      suppliers: {
        [supplier_1]: {
          [itemType]: {
            prices: {
              [otherPriceComponent]: 30,
              [totalPriceComponent]: 10,
            },
          },
        },
        [supplier_2]: {
          [itemType]: {
            prices: {
              [totalPriceComponent]: 20,
            },
          },
        },
        [supplier_3]: {
          [itemType]: {
            prices: {
              [otherPriceComponent]: 10,
              [totalPriceComponent]: 30,
            },
          },
        },
      },
    };
    const item_2 = {
      item: 'item_2',
      item_id: 'item_2',
      suppliers: {
        [supplier_1]: {
          [itemType]: {
            prices: {
              [totalPriceComponent]: 10,
            },
          },
        },
      },
    };

    it('should work with no items', () => {
      expect(createNestedRankingItems([], allSuppliers, [itemType], totalPriceComponent)).toStrictEqual([]);
    });
    it('should work with single item from single supplier', () => {
      const list = [
        item_1_single,
      ];
      const expected = [
        {
          ...item_1_single,
          _depth: 0,
          itemType,
          ranking: 1,
          subRows: [],
          supplierName: 'supplier_1',
          supplierValues: {
            prices: {
              [totalPriceComponent]: 10,
            },
          },
        },
      ];

      expect(createNestedRankingItems(list, allSuppliers, [itemType], totalPriceComponent)).toStrictEqual(expected);
    });
    it('should work with two items from single supplier', () => {
      const list = [
        item_1_single,
        item_2,
      ];
      const expected = [
        {
          ...item_1_single,
          _depth: 0,
          itemType,
          ranking: 1,
          subRows: [],
          supplierName: supplier_1,
          supplierValues: {
            prices: {
              [totalPriceComponent]: 10,
            },
          },
        },
        {
          ...item_2,
          _depth: 0,
          itemType,
          ranking: 1,
          subRows: [],
          supplierName: supplier_1,
          supplierValues: {
            prices: {
              [totalPriceComponent]: 10,
            },
          },
        },
      ];

      expect(createNestedRankingItems(list, allSuppliers, [itemType], totalPriceComponent)).toStrictEqual(expected);
    });
    it('should work with items from multiple suppliers - total price', () => {
      const list = [
        item_1,
        item_2,
      ];
      const expected = [
        {
          ...item_1,
          _depth: 0,
          itemType,
          ranking: 1,
          subRows: [
            {
              ...item_1,
              _depth: 1,
              item: '',
              itemType,
              ranking: 2,
              supplierName: supplier_2,
              supplierValues: {
                prices: {
                  [totalPriceComponent]: 20,
                },
              },
            },
            {
              ...item_1,
              _depth: 1,
              item: '',
              itemType,
              ranking: 3,
              supplierName: supplier_3,
              supplierValues: {
                prices: {
                  [otherPriceComponent]: 10,
                  [totalPriceComponent]: 30,
                },
              },
            },
          ],
          supplierName: supplier_1,
          supplierValues: {
            prices: {
              [otherPriceComponent]: 30,
              [totalPriceComponent]: 10,
            },
          },
        },
        {
          ...item_2,
          _depth: 0,
          itemType,
          ranking: 1,
          subRows: [],
          supplierName: supplier_1,
          supplierValues: {
            prices: {
              [totalPriceComponent]: 10,
            },
          },
        },
      ];

      expect(createNestedRankingItems(list, allSuppliers, [itemType], totalPriceComponent)).toStrictEqual(expected);
    });
    it('should work with items from multiple suppliers - other price', () => {
      const list = [
        item_1,
        item_2,
      ];
      const expected = [
        {
          ...item_1,
          _depth: 0,
          itemType,
          ranking: 1,
          subRows: [
            {
              ...item_1,
              _depth: 1,
              item: '',
              itemType,
              ranking: 2,
              supplierName: supplier_1,
              supplierValues: {
                prices: {
                  [otherPriceComponent]: 30,
                  [totalPriceComponent]: 10,
                },
              },
            },
          ],
          supplierName: supplier_3,
          supplierValues: {
            prices: {
              [otherPriceComponent]: 10,
              [totalPriceComponent]: 30,
            },
          },
        },
      ];

      expect(createNestedRankingItems(list, allSuppliers, [itemType], otherPriceComponent)).toStrictEqual(expected);
    });
  });
});
