export const addNumbers = (a, b) => (a || 0) + (b || 0);

export const average = (data) => {
  if (!data || !data.length) {
    return 0;
  }

  const sum = data.reduce(addNumbers, 0);

  const avg = sum / data.length;

  return avg;
};

export const normalization = (
  max,
  min,
  actualValue,
  upperRange,
  lowerRange
) => {
  let upperFormula = (actualValue - min) * (upperRange - lowerRange);
  let lowerFormula = max - min;
  if (max === min) {
    // use the sqrt as scaling factor to keep single bubble in the middle
    upperFormula =
      (actualValue - min / Math.sqrt(2)) * (upperRange - lowerRange);
    lowerFormula = min;
  }

  return Math.sqrt(upperFormula / lowerFormula);
};

export const normalizationMaxValue = (
  actualValue,
  maxValue,
  upperRange,
  lowerRange
) => {
  // cutoff value at certain limit for scaling purposes
  const currValue = Math.min(actualValue, maxValue);

  const upperFormula = currValue * (upperRange - lowerRange);
  const lowerFormula = maxValue;

  return lowerRange + upperFormula / lowerFormula;
};
