export const getFieldErrors = (errorResponseData) => {
  if (
    !!errorResponseData.errors &&
    typeof errorResponseData.errors === 'object' &&
    !(errorResponseData.errors instanceof Array)
  ) {
    return errorResponseData.errors;
  }

  return {};
};

export const getIndexFromFilter = (newValue, filterProps) => {
  let resultIndex = 0;

  filterProps.filters.forEach((filterProp, index) => {
    const keys = Object.keys(newValue);

    if (filterProp.id === keys[keys.length - 1]) {
      resultIndex = index;
    }
  });

  return resultIndex;
};

export const getLastIndexFilter = localFilterProps => localFilterProps.filters.length - 1;

