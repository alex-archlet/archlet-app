/* eslint-disable no-template-curly-in-string */
import * as Sentry from '@sentry/react';
import { Integrations } from '@sentry/tracing';

import { version } from '../package.json';
import { getEnvVariable } from './env';

const dsn = getEnvVariable('SENTRY_DSN');
const environment = getEnvVariable('ENVIRONMENT_STRING');

Sentry.init({
  dsn: environment && dsn,
  integrations: [
    new Integrations.BrowserTracing(),
  ],
  tracesSampleRate: 0.1, // send only 10% of monitoring events to sentry
  release: version,
  environment,
});
