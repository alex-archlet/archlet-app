/* eslint-disable @typescript-eslint/no-var-requires */
const typescriptTransform = require('i18next-scanner-typescript');


module.exports = {
  input: [
    'src/**/*.{js,jsx,ts,tsx}',
  ],
  output: './',
  options: {
    removeUnusedKeys: true,
    sort: true,
    debug: false,
    func: {
      list: ['i18next.t', 'i18n.t', 't'],
      extensions: ['.js', '.jsx'],
    },
    trans: {
      component: 'Trans',
      i18nKey: 'i18nKey',
      defaultsKey: 'defaults',
      extensions: ['.js', '.jsx'],
      fallbackKey(ns, value) {
        return value;
      },
      acorn: {
        ecmaVersion: 10, // defaults to 10
        sourceType: 'module', // defaults to 'module'
      },
    },
    lngs: ['en', 'de'],
    defaultLng: 'en',
    defaultNs: 'translation',
    defaultValue(lng, ns, key) {
      if (lng === 'en') {
          // Return key as the default value for English language
          return key;
      }
      return '__STRING_NOT_TRANSLATED__';
    },
    resource: {
      loadPath: 'src/locales/{{lng}}/{{lng}}.json',
      savePath: 'src/locales/{{lng}}/{{lng}}.json',
      jsonIndent: 2,
      lineEnding: '\n',
    },
    nsSeparator: false, // namespace separator
    keySeparator: false, // key separator
    interpolation: {
      prefix: '{{',
      suffix: '}}',
    },
    plural: true,
  },
  transform: typescriptTransform({ extensions: [".tsx", ".ts"] })
};
