#!/bin/sh
# source: https://www.freecodecamp.org/news/how-to-implement-runtime-environment-variables-with-create-react-app-docker-and-nginx-7f9d42a91d70/

echo "Frontend DSN: $SENTRY_DSN"
echo "Frontend ENV: $ENVIRONMENT_STRING"
echo "Frontend API: $BACKEND_REST_REQUEST_PREFIX"

originalfile=/var/www/app/build/env.js
tmpfile=$(mktemp)
# copy file to keep permissions of original file
cp -p $originalfile $tmpfile
# empty the file
cat /dev/null > $tmpfile
echo "window.ARCHLET_ENV = {" >> $tmpfile

echo "SENTRY_DSN: \"$SENTRY_DSN\"," >> $tmpfile
echo "ENVIRONMENT_STRING: \"$ENVIRONMENT_STRING\"," >> $tmpfile
echo "BACKEND_REST_REQUEST_PREFIX: \"$BACKEND_REST_REQUEST_PREFIX\"," >> $tmpfile

echo "}" >> $tmpfile

# replace the old file with the new file
mv $tmpfile $originalfile

exec "$@"
