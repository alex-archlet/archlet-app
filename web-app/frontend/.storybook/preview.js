import React from 'react';
import { themes } from '@storybook/theming';
import { ThemeProvider } from '@material-ui/core/styles';
import { theme } from '@utils/uiTheme';

// or global addParameters
export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  docs: {
    theme: themes.light,
  },
};

// theme decorator
export const decorators = [
  (Story) => (
    <ThemeProvider theme={theme}>
      <Story />
    </ThemeProvider>
  ),
];