const path = require('path');
const paths = require('../config/paths');

module.exports = {
  stories: ['../src/**/*.stories.mdx', '../src/**/*.stories.@(js|jsx|ts|tsx)'],
  addons: [
    '@storybook/addon-links', 
    '@storybook/addon-essentials',
  ],
  // automatic export types from components 
  typescript: {
    reactDocgen: 'react-docgen-typescript',
    reactDocgenTypescriptOptions: {
      shouldExtractLiteralValuesFromEnum: true,
      // exclude inherited props types from parent component
      propFilter: prop =>
        prop.parent ? !/node_modules/.test(prop.parent.fileName) : true,
    },
  },
  webpackFinal: (config) => {
    // loop over existing aliases and append them to storybook webpack configuration
    Object.entries(paths.webpackAliases).forEach(([alias, alias_path]) => {
      config.resolve.alias[alias] = path.resolve(
        __dirname,
        path.join('..', alias_path)
      );
    });
    return config;
  },
};
