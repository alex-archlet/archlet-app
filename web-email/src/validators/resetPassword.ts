import * as yup from 'yup';

export const resetPasswordValidator = yup
    .object()
    .shape({
        name: yup.string().required('A name is required'),
        email: yup.string().email().required('A valid e-mail is required'),
        password: yup.string().required('A password is required'),
    });
