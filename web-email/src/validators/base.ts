import * as yup from 'yup';
import { EmailType } from '../types';

const EMAIL_TYPES = [EmailType.ResetPassword, EmailType.ReviewFinalScenario];

const headerValidator = yup
    .object()
    .shape({
        to: yup.string().email('"to" is required in the header field').required(),
    });

const typeError = 'A valid email type is not provided';
const typeValidator = yup
    .string()
    .required(typeError)
    .oneOf(EMAIL_TYPES, typeError);

export const baseValidator = yup
    .object()
    .shape({
        header: headerValidator,
        type: typeValidator,
    });

