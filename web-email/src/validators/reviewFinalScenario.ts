import * as yup from 'yup';

export const reviewFinalScenarioValidator = yup
    .object()
    .shape({
        user: yup.string().required('A user is required'),
        scenario: yup.string().required('A scenario is required'),
        project: yup.string().required('A project is required'),
        comment: yup.string().required('A comment is required'),
        url: yup.string().required('A comment is required'),
    });
