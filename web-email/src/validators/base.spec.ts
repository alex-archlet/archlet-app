import { baseValidator } from "./base";

describe('base validator', () => {
    it('should throw when no "to" in header', () => {
        expect(() => baseValidator.validateSync({})).toThrow();
    });
    it('should throw when no "type"', () => {
        expect(() => baseValidator.validateSync({})).toThrow();
    });
});
