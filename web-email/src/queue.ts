import amqp, { Connection } from 'amqplib';
import { EmailInterface } from './types';
import { emailHandler } from './handlers';
import { captureSentryException } from './sentry';

const QUEUE_SERVICE_URI = process.env.QUEUE_SERVICE_URI || 'amqp://queue';
const QUEUE_NAME = process.env.QUEUE_NAME || 'email-queue';

let connection;
const getQueueConnection = async (): Promise<Connection> => {
    if (!connection) {
        connection = await amqp.connect(QUEUE_SERVICE_URI);
    }
    return connection;
};

export const initConsumerQueue = async () => {
    const connection = await getQueueConnection();
    const ch = await connection.createChannel();
    await ch.assertQueue(QUEUE_NAME);
    return ch.consume(QUEUE_NAME, async (msg) => {
        if (msg !== null) {
            try {
                const data: EmailInterface<any> = JSON.parse(msg.content.toString());
                console.log('email pre-sent');
                await emailHandler(data);
                console.log('email sent');
                ch.ack(msg);
            } catch (e) {
                let message;
                try {
                    message = JSON.parse(msg.content.toString());
                } catch (e) {}
                captureSentryException(e, message);
                console.log(e);
                ch.nack(msg);
            }
        }
    });
};

