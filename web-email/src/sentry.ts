import * as Sentry from '@sentry/node';

export const captureSentryException = async (
  error: Error,
  extra?: object
) => {
  Sentry.captureException(error, { extra });
};
