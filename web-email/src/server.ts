import express from 'express';
import { initConsumerQueue } from './queue';
import * as Sentry from '@sentry/node';

const port = 8888;
const SENTRY_DSN_KEY = process.env.SENTRY_DSN;
const environment = process.env.ENVIRONMENT_STRING;

Sentry.init({
  dsn: environment && SENTRY_DSN_KEY,
  environment,
});

const app = express();

const setup = async () => {
  try {
    await initConsumerQueue();
  } catch (error) {
    process.exit(1);
  }

  // since we setup the queue connection before implementing this listener, we are safe
  app.get('/', async (_, res) => {
    res.sendStatus(200);
  });
  app.listen(port, err => {
    if (err) {
      return console.error(err);
    }
    return console.log(`server is listening on ${port}`);
  });
};

setup();
