import { EmailInterface, EmailType } from '../types';
import { baseValidator } from '../validators/base';
import { resetPasswordHandler } from './resetPassword';
import { reviewFinalScenarioHandler } from './reviewFinalScenario';

export const emailHandler = async (data: EmailInterface<any>) => {
    baseValidator.validateSync(data);
    const { type } = data;
    switch (type) {
        case EmailType.ResetPassword:
            return resetPasswordHandler(data);
        case EmailType.ReviewFinalScenario:
            return reviewFinalScenarioHandler(data);
        default:
            throw new Error('E-mail type not found');
    }
};
