import { EmailInterface } from "../types";
import { reviewFinalScenarioValidator } from "../validators/reviewFinalScenario";
import { BaseEmailService } from "../services/BaseEmailService";

const FROM_EMAIL = process.env.FROM_EMAIL || 'info@archlet.ch';

export interface ReviewFinalScenarioBody {
    user: string,
    scenario: string,
    project: string,
    comment: string,
    url: string,
}

export interface ReviewFinalScenarioInterface extends EmailInterface<ReviewFinalScenarioBody> {}

export const reviewFinalScenarioHandler = async (emailData: ReviewFinalScenarioInterface) => {
    reviewFinalScenarioValidator.validateSync(emailData.body);
    const {
        body: {
            user,
            scenario,
            project,
            comment,
            url,
        },
        header: {
            to,
        },
    } = emailData;
    const text = `
Hello,

Your colleague ${user} wants to review with you the scenario ${scenario} in the sourcing project ${project} in the Archlet App:

${comment}

Have a look at it:
${url}
    `;

    const SUBJECT = `${user} is asking you to review a scenario in the Archlet App`;

    return BaseEmailService.sendEmail({
        to,
        from: FROM_EMAIL,
        subject: SUBJECT,
        text,
    });
};
