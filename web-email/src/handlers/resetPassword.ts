import { EmailInterface } from "../types";
import { resetPasswordValidator } from "../validators/resetPassword";
import { BaseEmailService } from "../services/BaseEmailService";

const FROM_EMAIL = process.env.FROM_EMAIL || 'info@archlet.ch';

export interface ResetPasswordBody {
    name: string,
    password: string,
    email: string,
}

interface ResetPasswordInterface extends EmailInterface<ResetPasswordBody> {}

const SUBJECT = 'Your password has been reset';

export const resetPasswordHandler = async (emailData: ResetPasswordInterface) => {
    resetPasswordValidator.validateSync(emailData.body);
    const {
        body: {
            name,
            password,
            email,
        },
        header: {
            to,
        },
    } = emailData;
    const text = `
Dear ${name},
    
Your password has been reset. You can log in using the following credentials.

E-mail: ${email}
Password: ${password}

We recommend that you change the password directly after logging in.

Best Regards,
Archlet
    `;
    return BaseEmailService.sendEmail({
        to,
        from: FROM_EMAIL,
        subject: SUBJECT,
        text,
    });
};
