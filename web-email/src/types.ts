export interface EmailHeaders {
    to: string,
}

export enum EmailType {
    ResetPassword = 'RESET_PASSWORD',
    ReviewFinalScenario = 'REVIEW_FINAL_SCENARIO',
}

export interface EmailInterface<T> {
    header: EmailHeaders,
    body: T,
    type: EmailType,
}
