import sgMail from '@sendgrid/mail';

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

interface EmailParameters {
    from: string,
    to: string,
    subject: string,
    text: string,
    html?: string,
}

export class BaseEmailService {

    public static async sendEmail(params: EmailParameters): Promise<any> {
        return sgMail.send(params);
    }

}
