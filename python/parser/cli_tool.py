"""
Simple command line tool to run the aggregator locally
"""

import argparse
import os

from src.v1.routes.excel_parser_service import handle_excel_try_catch

STARTING_PATH = '/Users/jakobmanz/Documents/Arbeit/Archlet/app/filestorage/data/'
if __name__ == "__main__":

    # Parse arguments
    parser = argparse.ArgumentParser(description='CLI for Archlet App parser')

    parser.add_argument('path', help="PK of the project entry in the" \
        " PostgreSQL database that will be execute")

    args = parser.parse_args()

    # Call endpoint
    full_path = STARTING_PATH + args.path
    
    handle_excel_try_catch(full_path, sheet_name=None, row=0, return_type='records')