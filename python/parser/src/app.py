"""
Parser REST service
"""
import logging
import os
from flask import Flask
import sentry_sdk
from sentry_sdk import capture_exception
from sentry_sdk.integrations.flask import FlaskIntegration
from shared.constants import RecordedError

from v1.routes.excel_parser import excel_parser_api
from v1.routes.excel_styler import excel_styler_api


SENTRY_DSN = os.environ.get("SENTRY_DSN")
ENVIRONMENT_STRING = os.environ.get("ENVIRONMENT_STRING")
ERROR_TYPE = 1


def before_send(event, hint):
    """ Process before sending to sentry """
    # ignore stdout/stderr logging, non original errors and RecordedErrors to sentry
    if event.get('logger', None) == 'root' or not hint.get('exc_info') \
        or isinstance(hint.get('exc_info')[ERROR_TYPE], RecordedError):
        return None
    return event


if ENVIRONMENT_STRING:
    sentry_sdk.init(
        dsn=SENTRY_DSN,
        integrations=[FlaskIntegration()],
        traces_sample_rate=0.1,
        environment=ENVIRONMENT_STRING,
        before_send=before_send
        )


app = Flask(__name__)

app.register_blueprint(excel_parser_api, url_prefix="/v1/excel-parser")
app.register_blueprint(excel_styler_api, url_prefix="/v1/excel-styler")

@app.errorhandler(Exception)
def handle_bad_request(error):
    """
    Handle a bad request
    """
    capture_exception(error)
    return "{}".format(error), 500

@app.route("/")
def hello():
    """ just for debugging reasons """
    return "Hello World!"

if __name__ == "__main__":
    app.run(host='0.0.0.0')

if __name__ != '__main__':
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)
