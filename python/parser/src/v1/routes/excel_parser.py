"""
Module for the excel parser route
"""
import os
from flask import Blueprint
from flask import request
from .excel_parser_service import handle_excel_try_catch

excel_parser_api = Blueprint('excel_parser_api', __name__)

STARTING_PATH = '/app/files/data/'
# @excel_parser_api.route("/")
@excel_parser_api.route("/", methods=['POST'])
def excel_parser_handler():
    """
        GET request: that must have the path include in the api call and looks at
        the mounted drive to see if exists
    """
    data = request.get_json()
    return_type = data.get('returnType', )
    fpath = data.get('path')
    full_path = STARTING_PATH + fpath
    if not os.path.exists(full_path):
        raise FileNotFoundError('Invalid path')
    row = data.get('row', 0)
    sheet_name = data.get('sheetName')
    return handle_excel_try_catch(full_path, sheet_name, row, return_type)


# @excel_parser_api.route("/form")
@excel_parser_api.route("/form", methods=['POST'])
def excel_handler():
    """
        POST request: that must have the File must be include in the body of the request
        and parser it the same
    """
    row = request.form.get('row', default=0)
    sheet_name = request.form.get('sheetName')
    return_type = request.form.get('returnType')
    file = request.files['file']
    if file is None:
        raise ValueError("File provide can not be found")
    return handle_excel_try_catch(file, sheet_name, row, return_type)
