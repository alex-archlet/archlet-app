"""
Module with the functionality to parse excel
"""
import pandas as pd
import simplejson as json

def handle_excel_parser(xls: pd.DataFrame, sheet_name: str, start_row: int, return_type: str) -> dict:
    """
        handles api logic by putting sheet name into array,
        using the translate function to get into the correct format to send back
    """
    sheets = [sheet_name] if sheet_name is not None else xls.sheet_names
    output = {}
    for name in sheets:
        data_frame = pd.read_excel(xls, name, header=int(start_row))
        output[name] = translate_dataframe(data_frame, return_type)
    return output

def translate_dataframe(data_frame: pd.DataFrame, return_type: str) -> dict:
    """
        translate dataframe and spilt the columns and the rows into separate property on the output dict.
    """
    if return_type == 'records':
        return {
            "headers": data_frame.columns.tolist(),
            "data":  data_frame.to_dict('records'),
        }
    return {
        "data":  [data_frame.columns.tolist()] + data_frame.to_numpy().tolist(),
    }

def handle_excel_try_catch(file: str, sheet_name: str, row: int, return_type: str) -> dict:
    """
        the function that calls by the each of the api routes that try and calls the handle function
    """
    try:
        xls = pd.ExcelFile(file, None)
        result = handle_excel_parser(xls, sheet_name, row, return_type)
    except OSError as err:
        raise err
    return json.dumps(result, ignore_nan=True)
