"""
Module with the functionality to style excel
"""
import os
from typing import List, Dict, Tuple, Callable
from io import BytesIO
import json
from dataclasses import dataclass
from urllib.request import urlopen
import xlsxwriter
from xlsxwriter import worksheet, workbook
import pandas as pd
from flask import send_file

from .entities.excel_styler.banner import Banner
from .entities.excel_styler.header import Header




START_COLUMN = 0
LOGO_POSITION = 'A1'


def get_entity_keys(class_name: dataclass) -> List[str]:
    """
    Retrieves dataclass attributes

    Args:
        class_name: name of dataclass

    Returns:
        List of dataclass attributes
    """
    return class_name.__dataclass_fields__.keys()


def check_attr_existance(class_name: dataclass, instance: dict) -> None:
    """
    Given a dataclass and a dataclass instance,
    check whether all attributes are present

    Args:
        class_name: name of dataclass
        instance: instance of dataclass

    Returns:
        None
    """
    attr_difference = set(get_entity_keys(class_name)).symmetric_difference(set(instance.keys()))
    if attr_difference:
        for attr in attr_difference:
            raise Exception(f"{class_name.__name__} object in request body contains unknown attribute {attr}")

    for attr in get_entity_keys(class_name):
        if attr not in instance:
            raise Exception(f"{class_name.__name__} object in request body doesn't contain required attribute {attr}")



def handle_styles_mapping(styles: dict) -> dict:
    """
    Reads the styles_translator json and maps
    CSS properties to XLSXWriter properties

    Args:
        styles: dictionary of CSS styles

    Returns:
        dictionary of XLSXWriter styles
    """

    with open(f'{os.getcwd()}/v1/routes/utils/styles_translator.json') as file:
        styles_translator = json.load(file)

    return {styles_translator[style]: styles[style] for style in styles} if styles else {}


def handle_request_body(banner: Dict[str, dict], headers: List[dict], data: List[dict]) -> Tuple[Banner, List[Header], pd.DataFrame]:
    """
    Casts request body to designated
    dataclass entities

    Args:
        banner: sheet logo, title and range in rows and cols
        headers: column header entities
        data: table main body
        styles: custom cell styles

    Returns:
        transformed dataclass entities:
                banner: Banner,
                headers: List[Header],
                data: pd.DataFrame,
                styles: dict
    """

    check_attr_existance(Banner, banner)
    banner = Banner(**banner)

    for column in headers:
        check_attr_existance(Header, column)
    headers = [Header(**column) for column in headers]

    data = pd.DataFrame.from_records(data)

    return banner, headers, data


def add_banner(w_sheet: worksheet, w_book: workbook, banner: Banner, col_dimension: int) -> None:
    """
    Method to add banner to an excel sheet

    Args:
        worksheet: worksheet to add the banner to
        workbook: the workbook, containing the worksheet
        banner: the banner to be added
        col_dimension: the column dimension of the worksheet

    Returns:
        None
    """
    title_styles = handle_styles_mapping(banner.title.style)
    banner_range = xlsxwriter.utility.xl_range(
        banner.range.start_row,
        banner.range.start_col,
        banner.range.end_row,
        col_dimension - 1)
    merge_format = w_book.add_format(title_styles)
    w_sheet.merge_range(banner_range, None, merge_format)
    image_data = BytesIO(urlopen(banner.img_path).read())
    logo_options = {'image_data': image_data, 'x_scale': 0.25, 'y_scale': 0.25, 'x_offset': 20, 'y_offset': 20}
    w_sheet.insert_image(LOGO_POSITION, banner.img_path, logo_options)
    w_sheet.write(0, 0, banner.title.value, merge_format)


def add_header(w_sheet: worksheet, w_book: workbook, headers: List[Header], is_banner_frozen: bool, header_row: int) -> None: # pylint: disable=line-too-long
    """
    Method to add a header row to an excel sheet

    Args:
        worksheet: worksheet to add the header row to
        workbook: the workbook, containing the worksheet
        headers: list of column headers
        is_banner_frozen: flag whether the banner is frozen
        header_row: row position of the header

    Returns:
        None
    """
    for column in headers:
        column_styles = handle_styles_mapping(column.style)
        column_format = w_book.add_format(column_styles)
        column_range = xlsxwriter.utility.xl_range(header_row, column.key, header_row, column.key)
        w_sheet.conditional_format(column_range, {'type': 'no_errors', 'format': column_format})

        # Freeze column if freezing enabled
        if column.frozen:
            frozen_rows = header_row if is_banner_frozen else 0
            w_sheet.freeze_panes(frozen_rows, column.key + 1)


def handle_excel_styler(banner: Banner, headers: List[Header], data: pd.DataFrame, styles: dict) -> Callable[[str], None]:
    """
    Creates an excel worksheet with custom styles

    Args:
    banner: Banner - sheet logo, title and range in rows and cols
    headers: List[Header] - column header entities
    data: pd.DataFrame -  table main body
    styles: dict - custom cell styles

    Returns:
            excel file
    """
    # TODO Add logo resizer
    row_dimension = data.shape[0]
    col_dimension = data.shape[1]

    sheet_title = banner.title.value
    temp_workbook = xlsxwriter.Workbook("temp.xlsx")
    temp_worksheet = temp_workbook.add_worksheet(sheet_title)

    column_names = [{'header': column.title} for column in headers]
    # Table cell range (size)
    cells_range = xlsxwriter.utility.xl_range(
        banner.range.end_row + 1,
        START_COLUMN,
        banner.range.end_row + row_dimension,
        START_COLUMN + col_dimension - 1)
    temp_worksheet.add_table(cells_range, {
        'data': data.values.tolist(),
        'style': None,
        'header_row': True,
        'columns': column_names})

    # Add alignment to cells TODO remove later, this should come from req body
    cell_format = temp_workbook.add_format({'align': 'center', 'valign': 'vcenter'})
    temp_worksheet.set_column(cells_range, 30, cell_format)

    # Add banner image and title
    add_banner(temp_worksheet, temp_workbook, banner, col_dimension)

    # Add background color to header row
    # TODO Handle locale formatting
    header_row = banner.range.end_row + 1
    add_header(temp_worksheet, temp_workbook, headers, banner.frozen, header_row)

    # Add custom cell styles
    if styles:
        for row, sub_dict in styles.items():
            for col, cell_style_dict in sub_dict.items():
                cell_range = xlsxwriter.utility.xl_range(int(row), int(col), int(row), int(col))
                cell_format = temp_workbook.add_format(handle_styles_mapping(cell_style_dict))
                temp_worksheet.conditional_format(cell_range, {'type': 'no_errors', 'format': cell_format})
    # TODO Add formulas when exact format known

    temp_workbook.close()

    return send_file(f"{os.getcwd()}/temp.xlsx")
