"""
Module containing Data Class containing all the fields of a Header Column
"""
from dataclasses import dataclass
from dataclasses import field
from collections import defaultdict

@dataclass
class Header:
    """
    Data Class representing an excel sheet column header


    Args:
        formula: if column is a product of other columns,
                specifies how the row values are calculated
        frozen: (bool) is the column be frozen
        key: column position in table
        style: column custom styles
        title: column title
    """

    key: int
    title: str
    formula: str = None
    frozen: bool = 0
    style: dict = field(default_factory=defaultdict)
