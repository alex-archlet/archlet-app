"""
Module containing Data Class containing all the fields of a Banner
"""
from dataclasses import dataclass

from .cellrange import CellRange
from .title import Title


@dataclass
class Banner:
    """
    Data Class representing a banner


    Args:
        img_path: company logo path
        title: banner title
        range: banner row and col range
    """

    img_path: str
    range: CellRange
    title: Title
    frozen: bool = 1

    def __post_init__(self):
        self.range = CellRange(**self.range)
        self.title = Title(**self.title)
