"""
Module containing Data Class containing all the fields of a Banner Title
"""
from dataclasses import dataclass
from dataclasses import field
from collections import defaultdict


@dataclass
class Title:
    """
    Data Class representing a image


    Args:
        style: excel sheet title custom styles
        value: excel sheet title text
    """

    value: str
    style: dict = field(default_factory=defaultdict)
