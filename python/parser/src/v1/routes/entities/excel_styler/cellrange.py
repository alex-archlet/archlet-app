"""
Module containing Data Class containing all the fields of a CellRange
"""
from dataclasses import dataclass

@dataclass
class CellRange:
    """
    Data Class representing a range of rows and columns


    Args:
        start_row: range starting row
        start_col: range starting column
        end_row: range ending row
        end_col: range ending column
    """

    start_row: int
    start_col: int
    end_row: int
    end_col: int
