"""
Module for the excel styler route
"""
#import os
from flask import Blueprint
from flask import request
#from flask import after_this_request

from .excel_styler_service import handle_excel_styler
from .excel_styler_service import handle_request_body

excel_styler_api = Blueprint('excel_styler_api', __name__)


# @excel_styler_api.route("/")
@excel_styler_api.route("", methods=['POST'])
def excel_styler_handler():
    """
    POST request: must have a body with banner, headers, data and optional style
    """
    req_body = request.get_json()
    required_props = ["banner", "headers", "data"]

    for prop in required_props:
        if prop not in req_body:
            raise Exception(f"Request body doesn't contain required property {prop}")

    banner, headers, data = handle_request_body(
        req_body["banner"],
        req_body["headers"],
        req_body["data"])

    styles = req_body["style"] if "style" in req_body else {}

    # TODO uncomment when implemented in FE, deletes file after sending it successfully
    # @after_this_request
    # def remove_file(response):
    #     try:
    #         os.remove(f"{os.getcwd()}/temp.xlsx")
    #     except Exception as error:
    #         print("Error removing or closing downloaded file handle", error)
    #     return response
    return handle_excel_styler(banner, headers, data, styles)
