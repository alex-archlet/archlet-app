"""
Module containing checkers for the data structures used through the tool.
"""
from typing import Dict

from shared.entities import TableHeader

def is_valid_offers_header(offers_header: TableHeader, project_settings: Dict[str, str]) -> bool:
    """
    Check whether this offers header contains all the necessary columns for optimization
    """
    header_columns = offers_header
    necessary_columns = [UKEYS.OFFER_ID,
                         UKEYS.DEMAND_ID,
                         UKEYS.SUPPLIER]


#def valid_for_project_info(offers: TableData, baseline: TableData, demands: TableData):