import argparse
import pika
import simplejson as json

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="bombard the queue with jobs")
    parser.add_argument('job_id', help="the job to execute")
    parser.add_argument('nb_attempts', help="the number of jobs to put in the queue")
    args = parser.parse_args()

    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host='queue', port=5672))
    channel = connection.channel()

    channel.queue_declare(queue='task_queue', durable=True)
    message = json.dumps({'job_id': str(args.job_id)})
    for _ in range(int(args.nb_attempts)):
        channel.basic_publish(exchange='', routing_key= 'task_queue', body=message)
    print(f"we sent: {args.nb_attempts} messages")
    connection.close()