"""
Filter class
"""
from typing import List
from typing import Tuple

from shared.communication import SingletonLogger
from shared.constants import CONSTRAINT_SPECS
from shared.constants import CONSTRAINT_TYPES
from shared.constants import KEYS
from shared.constants import OPERATION_TYPE
from shared.constants import RecordedError
from shared.constants import SCENARIO_TYPE
from shared.constants import STATUS_MESSAGE_CATEGORY
from shared.constants import UKEYS
from shared.entities import TableData

slogger = SingletonLogger()

class Filter:
    def __init__(self,
                 offers_table: TableData,
                 baseline_table: TableData,
                 demands_table: TableData,
                 discounts_table: TableData) -> None:
        """
        This is the constructor for the filter class.

        Args:
            offers
            baseline
            demands
            discounts
        """
        self.offers = offers_table
        self.baseline = baseline_table
        self.demands = demands_table
        self.discounts = discounts_table

    def run(self, params: dict, project_settings: dict) -> bool:
        """
        This function takes care of any data filtering due to user preferences
        prior to running the optimization algorithm with the demands, offers
        and rebates tables.
        """

        # get relevant info from job params
        scenario_type, volume_select, partial_allocation, split_items = self.parse_params(params['scenario_settings'],
                                                                                          project_settings)

        if self.discounts is not None:
            self._filter_discounts(volume_select)

        #######################################################################
        # Translate outdated scenario types (backward compatibility)
        #######################################################################
        if scenario_type == SCENARIO_TYPE.CHERRY:
            scenario_type = SCENARIO_TYPE.CUSTOM

        elif scenario_type == SCENARIO_TYPE.NO_CHANGE:
            scenario_type = SCENARIO_TYPE.CUSTOM

        #######################################################################
        # Run appropriate filtering depending on scenario type
        #######################################################################

        self.offers = self._filter_offers_without_price(self.offers)

        # Custom: pick the highest savings allocation based on different manual filters
        if scenario_type == SCENARIO_TYPE.CUSTOM:
            if volume_select and volume_select[0]:
                self._set_optimization_volume(volume_select)

        # Baseline: consider only baseline offers
        elif scenario_type == SCENARIO_TYPE.BASELINE:
            self.offers = self.baseline

        # Unknown scenario type
        else:
            slogger.warning(f"Unknown scenario type {scenario_type}")

        #######################################################################
        # Common to all scenario types
        #######################################################################

        # Check filter-based feasibility
        self._check_filter_feasibility(self.offers,
                                       self.demands,
                                       partial_allocation,
                                       split_items)

        return True

    def get_offers(self):
        """
        Return the updated offers table.
        """
        return self.offers

    def get_discounts(self):
        """
        Return the updated discounts table.
        """
        return self.discounts

    def _filter_discounts(self, volume_select: List[str]) -> None:
        """
        Filter discounts to only keep those related to the volume
        used in the optimization
        """
        header = self.discounts.header
        try:
            volume_col = header[UKEYS.VOLUME_NAME]
        except AttributeError:
            return

        # Filter discounts related to volume or general ones
        mask = (self.discounts[volume_col] == volume_select[0]) | (self.discounts[volume_col].isnull())
        self.discounts.data = self.discounts[mask].reset_index(drop=True, inplace=False)
        self.discounts.trim()


    @staticmethod
    def _filter_offers_without_price(offers) -> TableData:
        """
        This function removes offers without price or price equal to zero.

        Args:
            offers (TableData): offers object

        Return:
            offers (TableData): filtered offers object
        """
        header = offers.header
        mask = ((offers[header[UKEYS.NORMALIZED_PRICE]].notnull()) &
                (offers[header[UKEYS.NORMALIZED_PRICE]] > 0))

        offers.data = offers[mask]

        return offers


    @staticmethod
    def parse_params(scenario_settings: dict, project_settings: dict) -> Tuple[str, List[str], bool, bool]:
        """
        Parse the scenario settings from the job input dict

        Args:
            scenario_settings (dict): dictionary containing all scenario settings
            project_settings: dictionary containing project wide settings

        returns:
            scenario_type: str
            volume_select: list
            partial_allocation: bool
            split_items: bool
        """
        # backward compatibility for old scenarios
        if CONSTRAINT_TYPES.GENERAL_SETTINGS in scenario_settings:
            general_settings = scenario_settings[CONSTRAINT_TYPES.GENERAL_SETTINGS]
        else:
            general_settings = {}

        # scenario type is sent within incumbent specifications
        if CONSTRAINT_SPECS.INCUMBENT in general_settings and general_settings[CONSTRAINT_SPECS.INCUMBENT]:
            scenario_type = general_settings[CONSTRAINT_SPECS.INCUMBENT][0]
        # otherwise get scenario type from old type - incl. backward compatibility
        elif SCENARIO_TYPE.TYPE in scenario_settings:
            scenario_type = scenario_settings[SCENARIO_TYPE.TYPE]

        if CONSTRAINT_SPECS.VOLUME in general_settings and general_settings[CONSTRAINT_SPECS.VOLUME]:
            volume_select = general_settings[CONSTRAINT_SPECS.VOLUME]
        else:
            volume_select = [project_settings.default_volume_column]

        partial_allocation = False
        if CONSTRAINT_SPECS.PARTIAL_ALLOCATION in general_settings and general_settings[CONSTRAINT_SPECS.PARTIAL_ALLOCATION]:
            partial_allocation = general_settings[CONSTRAINT_SPECS.PARTIAL_ALLOCATION]

        split_items = False
        if CONSTRAINT_SPECS.SPLIT_ITEMS in general_settings and general_settings[CONSTRAINT_SPECS.SPLIT_ITEMS]:
            split_items = general_settings[CONSTRAINT_SPECS.SPLIT_ITEMS]

        return scenario_type, volume_select, partial_allocation, split_items


    @staticmethod
    def _check_filter_feasibility(offers: TableData,
                                  demands: TableData,
                                  partial_allocation: bool,
                                  split_items: bool) -> bool:
        """
        This function checks if still an allocation of all demands is theoretically possible - without
        considering optimization constraints. If not and partial_allocation = False, an error is raised

        Args:
            offers (TableData): offers object
            demands (TableData): offers object
            partial_allocation (bool): partial allocation desired yes/no
            split_items (bool): multiple offers per demand desired yes/no

        Return:
            (bool): True on successful check

        Throws: RecordedError when no offer or not sufficient volume exists for
        demanded volume
        """

        demands_header = demands.header
        offers_header = offers.header

        # check that all demanded items are still offered
        offered_items = offers[offers_header[UKEYS.DEMAND_ID]].unique()

        mask = demands[demands_header[UKEYS.DEMAND_ID]].isin(offered_items)

        if not all(mask) and not partial_allocation:
            def f(row):
                message = f'No offer for demand {row[demands_header[UKEYS.ITEM_ID]]}'
                slogger.error(message,
                                 save_message=True,
                                 category=STATUS_MESSAGE_CATEGORY.SCENARIO,
                                 card=5,
                                 item=row[demands_header[UKEYS.ITEM_ID]])

            demands.data[~mask].apply(func=f, axis=1)

            slogger.error('Not all demands are satisfied. '\
                             'If you want to still create this scenario, '\
                             'check partial allocation setting.',
                             save_message=True,
                             category=STATUS_MESSAGE_CATEGORY.SCENARIO,
                             card=5,
                             item='')

            raise RecordedError

        # if we do not want to split items and neither have a partial allocation,
        # check that for all demands there is at least one offer offering all volume
        if not split_items and not partial_allocation:
            # get max offered volume
            offered_max_volume = offers[[offers_header[UKEYS.TOTAL_VOLUME], offers_header[UKEYS.DEMAND_ID]]].groupby(offers_header[UKEYS.DEMAND_ID]).max()

            # get demanded volume
            demanded_volume = demands.data.groupby(demands_header[UKEYS.DEMAND_ID]).sum()

            # make check
            mask = offered_max_volume[offers_header[UKEYS.TOTAL_VOLUME]].sub(demanded_volume[demands_header[UKEYS.TOTAL_VOLUME]]) < 0

            if any(mask):
                def f(row):
                    message = f'Not sufficient offered volume by single supplier for demand {row.name}'
                    slogger.error(message,
                                     save_message=True,
                                     category=STATUS_MESSAGE_CATEGORY.SCENARIO,
                                     card=5,
                                     item=row.name)

                offered_max_volume[mask].apply(func=f, axis=1)

                slogger.error('Not all demands are satisfied by one single supplier. '\
                                 'If you want to still create this scenario, '\
                                 'allow spliting items and/or partial allocation setting.',
                                 save_message=True,
                                 category=STATUS_MESSAGE_CATEGORY.SCENARIO,
                                 card=5,
                                 item='')

                raise RecordedError

        # check that for all demanded items enough volume is offered
        offered_volume = offers.data.groupby(offers_header[UKEYS.DEMAND_ID]).sum()

        demanded_volume = demands.data.groupby(demands_header[UKEYS.DEMAND_ID]).sum()

        mask = offered_volume[offers_header[UKEYS.TOTAL_VOLUME]].sub(demanded_volume[demands_header[UKEYS.TOTAL_VOLUME]]) < 0

        if any(mask) and not partial_allocation:
            def f(row):
                message  = f'Not sufficient offered volume for demand {row.name}'
                slogger.error(message,
                                 save_message=True,
                                 category=STATUS_MESSAGE_CATEGORY.SCENARIO,
                                 card=5,
                                 item=row.name)

            offered_volume[mask].apply(func=f, axis=1)

            raise RecordedError

        return True

    def _set_optimization_volume(self, volume_select: List[str]) -> None:
        """
        This function sets the correct volumes for the optimization.

        Multiple volumes at the same time are not yet supported
        """
        # TODO: implement more general case!!
        if len(volume_select) > 1:
            raise NotImplementedError("Currently optimization can only be done on one volume at the time")

        # Update demands header
        demands_header = self.demands.header
        demands_header.set_type(volume_select[0], UKEYS.TOTAL_VOLUME)

        # Update offers header
        offers_header = self.offers.header

        # check if volume column exists in offers header, otherwise get volume from demands
        if volume_select[0] in offers_header.names:
            offers_header.set_type(volume_select[0], UKEYS.TOTAL_VOLUME)
        else:
            self.offers.data = self.offers.data.merge(self.demands[demands_header[[UKEYS.DEMAND_ID, UKEYS.TOTAL_VOLUME]]],
                                                      left_on=offers_header[UKEYS.DEMAND_ID],
                                                      right_on=demands_header[UKEYS.DEMAND_ID])
            self.offers.data.drop(columns=[offers_header[UKEYS.TOTAL_VOLUME]], inplace=True)
            self.offers.data.rename(columns={demands_header[UKEYS.TOTAL_VOLUME]: offers_header[UKEYS.TOTAL_VOLUME]}, inplace=True)
