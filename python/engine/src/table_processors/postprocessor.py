"""
Postprocessor class
"""
import uuid
import numpy as np
import pandas as pd

from numpy import nan

from collections.abc import Iterable
from typing import List
from typing import Tuple
from typing import Dict

from shared.communication import SingletonLogger
from shared.constants import UKEYS, KEYS
from shared.constants import RecordedError
from shared.entities import TableData
from shared.entities import Translator
from shared.utilities.expression_handler import ExpressionHandler
from shared.utilities.kpi_utils import contains_baseline
from shared.utilities.kpi_utils import create_user_renamer
from shared.utilities.kpi_utils import create_uuid_renamer
from shared.utilities.kpi_utils import create_uuid_table_renamer

Str_Dict = Dict[str, str]

slogger = SingletonLogger()

class Postprocessor:
    def __init__(self) -> None:
        """
        This is the constructor for the postprocessor class.

        Args:
            
        """
        pass


    def process_table(self, table: TableData, tablename: str) -> TableData:
        """
        Fetches the postprocessor specific to the table type.
        They are used to format the tables before saving them to the database
        """
        try:
            table_processor = getattr(self, f'process_{tablename}_table')
        except:
            return table
        processed_table = table_processor(table)
        return processed_table

    def process_allocations_table(self, table: TableData) -> TableData:
        """ Allocations table processor. Transforms lists to strings """
        keys = [UKEYS.DISCOUNTED_BY, UKEYS.CONTRIBUTING_TO, UKEYS.DISCOUNT_TYPE]

        header = table.header
        for key in keys:
            if key in header.types:
                table.to_string(header[key], inplace=True)

        return table


    def process_discounts_table(self, table: TableData) -> TableData:
        """ Discounts table processor. Resets index"""
        table.data.reset_index(inplace=True, drop=True)
        return table


    def reverse_translate_allocations(self, table: TableData, translator: Translator) -> TableData:
        """ Translate back the allocations table to the column names used by the supplier """

        dic = {}
        used_tables = ['demands', 'offers', 'allocations']
        for tablename in used_tables:
            dic.update(translator[tablename])


    def process_offers_table(self, table: TableData) -> TableData:
        """ Offers table processor. Transforms lists to strings """
        header = table.header
        if UKEYS.CONTRIBUTING_TO in header.types: # TODO : do that in a cleaner way
            table.to_string(header[UKEYS.CONTRIBUTING_TO], inplace=True)
            table.to_string(header[UKEYS.DISCOUNTED_BY],   inplace=True)
        return table


    def process_demands_table(self, table: TableData) -> TableData:
        """ Demands table processor. Transforms lists to strings """
        header = table.header
        if UKEYS.CONTRIBUTING_TO in header.types:
            table.to_string(header[UKEYS.CONTRIBUTING_TO], inplace=True)
            table.to_string(header[UKEYS.DISCOUNTED_BY],   inplace=True)
        return table


    def run(self, allocations: TableData) -> TableData:
        """
        This function should handle any additional postprocessing not done in
        the optimizer.
        """
        header = allocations.header
        header.add_column(UKEYS.TOTAL_SPENT)
        header.add_column(UKEYS.ALLOCATED_VOLUME)
        header.add_column(UKEYS.TOTAL_DISCOUNT)
        # header.add_column(UKEYS.TOTAL_SPENT, UKEYS.TOTAL_SPENT, '')
        # header.add_column(UKEYS.ALLOCATED_VOLUME, UKEYS.ALLOCATED_VOLUME, '')
        # header.add_column(UKEYS.TOTAL_DISCOUNT, UKEYS.TOTAL_DISCOUNT, '')
        # Populate some fields in the allocations table
        allocations[header[UKEYS.TOTAL_SPENT]]    = \
            allocations[header[UKEYS.UNIT_PRICE]] * allocations[header[UKEYS.TOTAL_VOLUME]]

        allocations[header[UKEYS.ALLOCATED_VOLUME]] = allocations[header[UKEYS.TOTAL_VOLUME]]

        allocations[header[UKEYS.TOTAL_DISCOUNT]] = \
            (allocations[header[UKEYS.MIN_UNIT_PRICE]] - allocations[header[UKEYS.UNIT_PRICE]]) * \
             allocations[header[UKEYS.TOTAL_VOLUME]]

        allocations.header = header
        return allocations


    def handle_incomplete_allocation(self, allocations: TableData, demands: TableData) -> TableData:
        """
        Check if each item is fully allocated.
        """
        alloc_header = allocations.header
        demands_header = demands.header
        mask = demands[demands_header[UKEYS.DEMAND_ID]].isin(allocations[alloc_header[UKEYS.DEMAND_ID]])
        unallocated_items = demands[~mask]
        unallocated_items = unallocated_items[[demands_header[UKEYS.DEMAND_ID]]]
        renamer = {demands_header[UKEYS.DEMAND_ID]: alloc_header[UKEYS.DEMAND_ID]}
        unallocated_items = unallocated_items.rename(columns=renamer)
        unallocated_items[alloc_header[UKEYS.TOTAL_VOLUME]] = 0
        unallocated_items[alloc_header[UKEYS.TOTAL_SPENT]] = 0
        if KEYS.CAPACITY in alloc_header.types:
            unallocated_items[alloc_header[KEYS.CAPACITY]] = 0
        uuids = [uuid.uuid4() for _ in range(len(unallocated_items.index))]
        unallocated_items[alloc_header[UKEYS.ALLOCATION_ID]] = uuids
        all_allocations = allocations.data.append(unallocated_items,
                                                        ignore_index=True,
                                                        sort=False)
        all_allocations = all_allocations.where(all_allocations.notna(), nan)
        result = TableData(all_allocations, alloc_header)
        return result


    def compute_kpis(self, allocations: TableData, baseline: TableData, custom_kpis: List[str]) -> dict:
        """
        Given an allocation matrix, return a dictionary containing KPIs
        """

        kpis = {}

        allocations_header = allocations.header

        # Get a list of all regions

        values = {}

        # Filter by region, "ALL" corresponds to any region
        alloc = allocations.data
        

        alloc_volume = alloc[allocations_header[UKEYS.TOTAL_VOLUME]].sum()

        ###################################################################
        # Spend, savings, discounts
        ###################################################################
        current_spend  = (alloc[allocations_header[UKEYS.UNIT_PRICE]]*alloc[allocations_header[UKEYS.TOTAL_VOLUME]]).sum()
        current_discounts = alloc[allocations_header[UKEYS.TOTAL_DISCOUNT]].sum()

        if baseline is not None:
            baseline_header = baseline.header
            base = baseline.data

            demand_volume = base[baseline_header[UKEYS.TOTAL_VOLUME]].sum()
            # calculate savings based filtered data
            mask = base[baseline_header[UKEYS.DEMAND_ID]].isin(alloc[allocations_header[UKEYS.DEMAND_ID]])
            base_filtered = base[mask]

            baseline_spend = (base_filtered[baseline_header[UKEYS.NORMALIZED_PRICE]]*base_filtered[baseline_header[UKEYS.TOTAL_VOLUME]]).sum()

            values['savings'] = float(baseline_spend - current_spend)

            prior_suppliers = set(baseline[baseline_header[UKEYS.SUPPLIER]].unique())

            # calculate allocation changes
            baseline_item_supplier_combos = baseline[baseline_header[UKEYS.DEMAND_ID]].astype(str) +\
                                            baseline[baseline_header[UKEYS.SUPPLIER]].astype(str)
            allocations_item_supplier_combos = alloc[allocations_header[UKEYS.DEMAND_ID]].astype(str) +\
                                               alloc[allocations_header[UKEYS.SUPPLIER]].astype(str)

            mask = allocations_item_supplier_combos.isin(baseline_item_supplier_combos)
            values['allocation_changes'] = int((~mask).sum())
        else:
            demand_volume = alloc_volume
            values['savings'] = 0
            prior_suppliers = set()

            values['allocations_changes'] = 0

        # Total amount of money spent
        values['spend'] = float(current_spend)

        # Total money saved by discounts versus undiscounted prices
        values['discounts'] = float(current_discounts)

        ###################################################################
        # Suppliers info
        ###################################################################

        current_suppliers = set(alloc[allocations_header[UKEYS.SUPPLIER]].unique())

        incumbent_suppliers = current_suppliers & prior_suppliers
        new_suppliers       = current_suppliers - incumbent_suppliers

        mask = alloc[allocations_header[UKEYS.SUPPLIER]].isin(incumbent_suppliers)
        incumbent_suppliers_spend = alloc.loc[mask, allocations_header[UKEYS.TOTAL_SPENT]].sum()

        mask = alloc[allocations_header[UKEYS.SUPPLIER]].isin(new_suppliers)
        new_suppliers_spend = alloc.loc[mask, allocations_header[UKEYS.TOTAL_SPENT]].sum()

        # Number of suppliers not present in baseline
        values['incumbent_suppliers'] = len(incumbent_suppliers)

        # Number of suppliers present in baseline
        values['new_suppliers'] = len(new_suppliers)

        # Total amount of money spent with suppliers present in baseline
        values['incumbent_supplier_spend'] = float(incumbent_suppliers_spend)

        # Total amount of money spent with suppliers not present in baseline
        values['new_supplier_spend'] = float(new_suppliers_spend)

        # Number of unique items
        values['num_items'] = len(alloc[allocations_header[UKEYS.DEMAND_ID]].unique())

        # Allocated volume
        values['volume'] = round(alloc_volume,2)

        # Percentage allocation
        values['allocation_percentage'] = round(alloc_volume/demand_volume,3)
        # don't allow for rounding to 100%
        # This part uses an absolute volume, but it varies from project to project depending on the order of magnitude, and we can truncate instead of round if we really need to. to check
        # if values['allocation_percentage'] == 1:
        #     if abs(alloc_volume - demand_volume) > OPTIMIZATION_CONSTANTS.ALLOCATION_TOLERANCE:
        #         values['allocation_percentage'] = 0.99

        kpis['Global'] = values

        return kpis


    @staticmethod
    def add_allocation_ids(allocations: TableData):
        """
        Add a column with uuids
        """
        header = allocations.header
        header.add_column(UKEYS.ALLOCATION_ID)
        allocations[header[UKEYS.ALLOCATION_ID]] = allocations.data.apply(lambda _: str(uuid.uuid4()), axis=1)
        return allocations

    @staticmethod
    def compute_kpi_table(allocations: TableData,
                          demands: TableData,
                          offers: TableData,
                          baseline_offers: TableData,
                          kpis_definitions: Dict[str, str]) -> TableData:
        """
        Calculate the table with the scenario kpis.

        Note: allocations table needs an id column
        """
        alloc_header = allocations.header
        kpi_table = pd.DataFrame(data={'id': allocations[alloc_header[UKEYS.ALLOCATION_ID]]})
        

        table = Postprocessor._create_helper_table(allocations,
                                          offers,
                                          baseline_offers,
                                          demands)

        table_index = f"a{alloc_header[UKEYS.ALLOCATION_ID].replace('-', '_')}"

        renamer = create_user_renamer(allocations.header, 'allocations')
        uuid_renamer = create_uuid_renamer(allocations.header, 'allocations')
        headers = [offers.header, demands.header]
        names = ['offers', 'demands']
        for header, name in zip(headers, names):
            renamer.update(create_user_renamer(header, name))
            uuid_renamer.update(create_uuid_renamer(header, name))

        if baseline_offers is not None:
            renamer.update(create_user_renamer(baseline_offers.header, 'baseline'))
            uuid_renamer.update(create_uuid_renamer(baseline_offers.header, 'baseline'))

        mapping = list(uuid_renamer.values())

        kpi_table.set_index('id', inplace=True)
        allocations.data.set_index(alloc_header[UKEYS.ALLOCATION_ID], inplace=True)
        for kpi_id, kpi_calculation in kpis_definitions.items():
            if kpi_calculation == 'total_spend':
                kpi_table[kpi_id] = allocations[alloc_header[UKEYS.TOTAL_SPENT]]
                continue
            if kpi_calculation == 'volume':
                kpi_table[kpi_id] = allocations[alloc_header[UKEYS.TOTAL_VOLUME]]
                continue
            
            if kpi_calculation == 'savings':
                kpi_calculation = "allocations.'total_volume' * baseline.'normalized_price' - allocations.'total_spent'"

            kpi_calculation = Postprocessor._rename(kpi_calculation, renamer)

            if contains_baseline(kpi_calculation) and baseline_offers is None:
                kpi_table[kpi_id] = 0
                continue

            kpi_calculation = Postprocessor._rename(kpi_calculation, uuid_renamer)
            assignment_name = f"k{kpi_id.replace('-', '_')}"
            kpi_calculation = f"{assignment_name} = {kpi_calculation}"
            expression_handler = ExpressionHandler('table', kpi_calculation, mapping)

            slogger.verify(expression_handler.is_assignment(), \
                    "The expression is not an assignment")
            slogger.verify(expression_handler.has_valid_attribute_names(), \
                    "The expression has invalid attribute names")

            calculator = expression_handler.get_original_calculator()
            dictionary_locals = {'table': table}

            exec(calculator, dictionary_locals)
            kpi_table.reset_index(inplace=True)
            kpi_table[kpi_id] = dictionary_locals[assignment_name]
            kpi_table.set_index('id', inplace=True)

        allocations.data.reset_index(inplace=True)
        kpi_table.reset_index(inplace=True)
        kpi_table = kpi_table.fillna(0)
        return kpi_table

    @staticmethod
    def _rename(expression: str, renamer: Str_Dict) -> str:
                for old, new in renamer.items():
                    expression = expression.replace(old, new)
                return expression


    @staticmethod
    def _create_helper_table(allocations,
                            offers,
                            baseline,
                            demands):
        # prepare the helper table:
        table_renamer = create_uuid_table_renamer(allocations.header, 'allocations')
        table = allocations.data.rename(columns=table_renamer)
        alloc_demand_id = table_renamer[allocations.header[UKEYS.DEMAND_ID]]
        alloc_offer_id = table_renamer[allocations.header[UKEYS.OFFER_ID]]

        table_renamer = create_uuid_table_renamer(offers.header, 'offers')
        offers_df = offers.data.rename(columns=table_renamer)
        offers_offer_id = table_renamer[offers.header[UKEYS.OFFER_ID]]
        table = pd.merge(left=table,
                         right=offers_df,
                         left_on=alloc_offer_id,
                         right_on=offers_offer_id)

        if baseline is not None:
            table_renamer = create_uuid_table_renamer(baseline.header, 'baseline')
            baseline_df = baseline.data.rename(columns=table_renamer)
            baseline_demand_id = table_renamer[baseline.header[UKEYS.DEMAND_ID]]
            table = pd.merge(left=table,
                             right=baseline_df,
                             left_on=alloc_demand_id,
                             right_on=baseline_demand_id)

        table_renamer = create_uuid_table_renamer(demands.header, 'demands')
        demands_df = demands.data.rename(columns=table_renamer)
        demands_demand_id = table_renamer[demands.header[UKEYS.DEMAND_ID]]
        table = pd.merge(left=table,
                         right=demands_df,
                         left_on=alloc_demand_id,
                         right_on=demands_demand_id)

        return table


    @staticmethod
    def compute_summed_kpis(allocations: TableData,
                            demands: TableData,
                            baseline: TableData,
                            kpis_pairs: Dict[str, str]) -> Dict[str, str]:
        """
        Calculate the summed kpis

        Returns:
            - Dictionary: <kpi_uuid> -> kpi value
        """
        alloc_header = allocations.header
        result = {}
        if baseline is None:
            result[kpis_pairs['number_of_changes']] = 0
        else:
            baseline_header = baseline.header

            baseline_item_supplier_combos = baseline[baseline_header[UKEYS.DEMAND_ID]].astype(str) +\
                                            baseline[baseline_header[UKEYS.SUPPLIER]].astype(str)
            allocations_item_supplier_combos = allocations[alloc_header[UKEYS.DEMAND_ID]].astype(str) +\
                                               allocations[alloc_header[UKEYS.SUPPLIER]].astype(str)

            mask = allocations_item_supplier_combos.isin(baseline_item_supplier_combos)
            result[kpis_pairs['number_of_changes']] = int((~mask).sum())

        nb_allocated_items = len(allocations[alloc_header[UKEYS.DEMAND_ID]].unique())
        nb_demanded_items = len(demands[demands.header[UKEYS.DEMAND_ID]].unique())
        result[kpis_pairs['items_allocated']] = float(nb_allocated_items/nb_demanded_items)
        result[kpis_pairs['winning_bidders']] = len(allocations[alloc_header[UKEYS.SUPPLIER]].unique())
        return result



    def add_kpi_column(self, allocations: TableData, expression: str) -> Tuple[TableData, str]:
        """
        Add a new column to the allocations with the computed kpi
        from the given expression string.

        Args
            allocations: the allocations table
            expression: the kpi to calculate

        Return
            TableData with the added column as specified in the expression
            the name of the added column
        """
        # To only check the names a list as mapping is sufficient
        mapping = allocations.header.get_all_user_defined_columns()
        expression_handler = ExpressionHandler('allocations', expression, mapping)
        
        slogger.verify(expression_handler.is_assignment(), \
             "The expression is not an assignment")
        slogger.verify(expression_handler.has_valid_attribute_names(), \
             "The expression has invalid attribute names")     

        assignment_name = expression_handler.assignment_name
        calculator = expression_handler.get_original_calculator()
        dictionary_locals = {'allocations': allocations}

        exec(calculator, dictionary_locals)

        allocations[assignment_name] = dictionary_locals[assignment_name]
        allocations.header.add_column(assignment_name, assignment_name, '')

        return allocations, assignment_name


    def create_capacities_output(self, allocations: TableData, max_capacities: TableData, offers: TableData):
        """
        Create a dictionary to display information about the state of 
        the capacities with the given allocations
        """

        allocations_header = allocations.header
        allocations_data = allocations.data

        max_cap_header = max_capacities.header
        max_cap_data = max_capacities.data.fillna(0)

        output = {}

        output['list'] = [max_cap_header.byid[col_id].original_name for col_id in max_cap_header[KEYS.CAPACITY]]

        offers_header = offers.header

        allocations_data.set_index(allocations_header[UKEYS.OFFER_ID], inplace=True)
        offers = offers.data.set_index(offers_header[UKEYS.OFFER_ID], inplace=False)

        for capacity in output['list']:
            allocations_header.add_column(KEYS.CAPACITY, original_name=capacity)
            allocations_data[allocations_header.byname[capacity].id] = offers[offers_header.byname[capacity].id]
            allocations_data[allocations_header.byname[capacity].id] = allocations_data[allocations_header.byname[capacity].id].fillna(0).multiply(allocations_data['allocations'])

        allocations_data.reset_index(inplace=True)

        max_cap_data = max_cap_data.set_index(max_cap_header[UKEYS.SUPPLIER])

        table = {}

        for supplier in allocations_data[allocations_header[UKEYS.SUPPLIER]].dropna().unique():
            table[supplier] = {}
            for capacity in output['list']:
                table[supplier][capacity] = {}
                table[supplier][capacity]['reached'] = float(allocations_data[allocations_data[allocations_header[UKEYS.SUPPLIER]] == supplier][allocations_header.byname[capacity].id].sum())
                table[supplier][capacity]['limit'] = float(max_cap_data[max_cap_header.byname[capacity].id][supplier])

        output['table'] = table

        return {"capacities": output}
