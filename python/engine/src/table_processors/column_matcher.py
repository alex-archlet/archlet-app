""" module that contains a class to perform the column matching job """
import json
import uuid
import numpy as np
import pandas as pd
import scipy.spatial

from pathlib import Path
from sentence_transformers import SentenceTransformer
from typing import Tuple, List, Dict

from shared.constants import KEYS
from shared.constants import PATHS
from shared.entities import TableData
from shared.entities import TableHeader
from shared.entities import MatchingResult

class ColumnMatcher:
    """ Class to find a matching for the column headers of a file """

    def __init__(self):
        pass

    @staticmethod
    def get_excel_column_previews(table: TableData) -> Dict[str, list]:
        """
        Create a preview dictionary for the columns present in this table.

        Args:
            table: the table with the contents from the excel file

        Output:
            Dictionary of the form: {<excel_column_name>: <list of first 9 values>}
        """
        result = {}
        len_output = min(9, len(table.data))
        for col in table.data.keys():
            # Get unique column values and add filling values in order to have the same length of values for each column
            unique_vals = [str(val) for val in table[col].unique()]
            first_vals = unique_vals + [str(table[col][i]) for i in range(len_output)]
            # Convert values to strings to avoid database saving issues
            result[col] = first_vals[:len_output]
        return result


    @staticmethod
    def match_columns(table: TableData, table_header: TableHeader) -> List[dict]:
        """
        Create a matching from the headers of the table to the
        headers in the table_header.

        Args:
            table: the input table to match
            table_header: the template to match to

        output:
            list of the form: {'matching': <boolean>,
                               'bid_column': <bid column id>,
                               'excel_column': <name of excel column>,
                               'message': <string>}
        """
        matching = []
        header_cols = [col.original_name for col in table_header.columns if col.calculation is None]
        # header_cols = table_header.get_all_user_defined_columns()
        excel_cols = table.data.columns

        # Go through all table header columns and find matching in excel header columns
        for bid_col in header_cols:
            if bid_col in excel_cols:
                matching.append({'matching': True,
                                 'bid_column': table_header.byname[bid_col].id,
                                 'excel_column': bid_col,
                                 'message': ''})

            else:
                matching.append({'matching': False,
                                 'bid_column': table_header.byname[bid_col].id,
                                 'excel_column': None,
                                 'message': ''})

        return matching


    def create_template_from_table(self, table: TableData, row_source: np.ndarray, model_path: str, column_types: pd.DataFrame) -> TableHeader:
        """
        Create a template from the column headers of the given table, and try to guess
        the types of the given column names

        Args:
            table: the input table to create a template for
            row_source: the source row of the input table
            model_path: the file storage path to the embeddings model
            column_types: a dataframe of the column types

        Output:
            a TableHeader object with all the headers of the given table
        """

        table_header = TableHeader()

        # cast column indeces to string to have matching working
        columns = table.data.keys().astype(str)

        # remove empty columns and associated rows
        columns, row_source = self._remove_empty_headers(columns, row_source)

        # differentiate input provision buyer or supplier
        row_source = self._get_input_by_list(row_source, model_path)

        # get column-source mapping
        mapping_colname_to_source = self._get_colname_to_source_mapping(columns, row_source)

        embedder = SentenceTransformer(str(model_path))

        # get file from repo (rather ugly but works)
        directory_path = Path(__file__).absolute().parent.parent
        reference_path = directory_path / PATHS.REFERENCE_PATH
        with open(reference_path) as json_file:
            reference = json.load(json_file)


        # get types, names and expected types
        mapping_name_to_type = reference['type']
        df_name_to_type = pd.DataFrame.from_dict(mapping_name_to_type, orient='index', columns=['type'])
        possible_names = list(mapping_name_to_type.keys())
        type_list = self._get_type_list(df_name_to_type, column_types)
        expected_types = reference['typeof']

        # get embeddings
        possible_names_embedded = embedder.encode(possible_names)
        column_names_embedded = embedder.encode(list(columns))

        # calculate matching scores for each column name
        matching_scores = scipy.spatial.distance.cdist(column_names_embedded, possible_names_embedded, 'cosine')

        # get matching per type
        matching_scores_per_type = self._get_matchings_per_type(matching_scores, df_name_to_type)

        # sort matchings by highest matchings
        matching_scores_per_type, column_order = self._get_sorted_matchings(matching_scores_per_type)

        # sort columns accordingly
        sorted_columns = columns.to_numpy()
        sorted_columns = sorted_columns[column_order]

        restricted_types = {
            'buyer': column_types.loc[column_types['is_buyer'], 'type'].tolist(),
            'supplier': column_types.loc[column_types['is_supplier'], 'type'].tolist()
        }

        # go through column names and check if highest matching is above threshold
        idx = 0
        for col, scores in zip(sorted_columns, matching_scores_per_type):
            # get correct order for column
            order_id = column_order[idx]
            idx = idx + 1

            # get source and generate column id
            source = mapping_colname_to_source[col]
            col_id = uuid.uuid4().hex

            # get types and scores
            initial_type, checked_type, initial_score, checked_score, type_list, source = self._get_type_and_score(col, table, expected_types, scores, type_list, restricted_types, source)

            # add column to table header
            table_header.add_column(checked_type,
                                    col_id=col_id,
                                    original_name=col,
                                    attribute=True,
                                    matching_result=MatchingResult(
                                        (1-checked_score),
                                        source,
                                        checked_type,
                                        (1-initial_score),
                                        initial_type,
                                        order_id
                                    )
                                    )

        return table_header


    @staticmethod
    def _check_type_column(column: pd.Series) -> str:
        """ Check which type is present in the given column """
        mask = column.isna()
        nbr_numberics = pd.to_numeric(column[~mask], errors='coerce').notnull().sum()
        nbr_non_numerics = pd.to_numeric(column[~mask], errors='coerce').isnull().sum()
        nbr_values = len(column)

        if nbr_numberics/nbr_values > 0.95:
            return 'number'
        elif nbr_non_numerics/nbr_values > 0.95:
            return 'text'
        else:
            return KEYS.UNKNOWN

    @staticmethod
    def _get_input_by_list(row_source, model_path: str):
        """
        Defines based on source row whether input is provided by buyer or supplier.
        Computes cosine distance between source row values and 'supplier'/'lieferant'.
        Based on the computed score each column is assigned a source.

        Args:
            row_source (ndarray): array containing source row values

        Output:
            sources (ndarray): array showing who provided input (buyer, supplier, '')
        """

        embedder = SentenceTransformer(str(model_path))

        possible_names_embedded = embedder.encode(['supplier','lieferant', 'bidder', 'bieter'])
        column_names_embedded = embedder.encode(list(row_source))

        matching_scores = scipy.spatial.distance.cdist(column_names_embedded, possible_names_embedded, 'cosine')

        # start with empty source row
        sources = np.full_like(row_source, '')

        # if score on average is below distance of 0.3 - deem supplier
        for idx, score_list in enumerate(matching_scores):

            supplier_index = 1
            supplier_score = sum(score_list[:supplier_index])/len(score_list[:supplier_index])
            bidder_score = sum(score_list[supplier_index+1:])/len(score_list[supplier_index+1:])

            if supplier_score < 0.3 or bidder_score < 0.3:
                sources[idx] = 'supplier'
            else:
                sources[idx] = 'buyer'

        return sources


    def _get_colname_to_source_mapping(self, colnames: np.ndarray, row_source: np.ndarray) -> dict:
        """
        Gets source row translated correctly and creates mapping based on it.

        Args:
            colnames (ndarray): list of column names
            row_source (ndarray): array containing source row values

        Output:
            mapping (dict): dictionary containing mapping of colnames to sources
        """

        mapping = {}
        i = 0
        for colname, source in zip(colnames, row_source):
            mapping[colname] = source
            i = i+1

        return mapping


    def _get_matchings_per_type(self, matching_scores: np.ndarray, df_name_to_type: pd.DataFrame) -> np.ndarray:
        """
        Find for each table header the best (min) matchings in has with the
        different column_types

        Args:
            matching_scores (ndarray): ndarray with matchings of headers with column type names
            df_name_to_type (DataFrame): mapping of column type names to column types

        Output:
            matching_scores_per_type (ndarray): best matching scores of table headers with column types
        """

        # prepare inputs
        matching_scores_per_type = np.array([]).reshape(0, len(matching_scores))
        matching_scores = matching_scores.transpose()

        # loop over all types and calculate min matchings
        for col_type in df_name_to_type.type.unique():
            mask = df_name_to_type.type == col_type
            min_matchings = np.amin(matching_scores[mask], axis=0).transpose()
            matching_scores_per_type = np.vstack((matching_scores_per_type, min_matchings))

        return matching_scores_per_type.transpose()


    def _get_sorted_matchings(self, matching_scores: np.ndarray) -> Tuple[np.ndarray, list]:
        """
        Sort matching scores along from best to worst matching

        Args:
            matching_scores (ndarray): ndarray with matchings of headers with column_types

        Output:
            matching_scores (ndarray): sorted ndarray
            sort_list (list): list of idxs along which matching_scores array was sorted
        """

        min_value_per_header = np.amin(matching_scores, axis=1)

        # get idx of sorted scores and sort scores
        sort_idx = np.argsort(min_value_per_header)
        matching_scores = matching_scores[sort_idx]

        sort_list = sort_idx.tolist()

        return matching_scores, sort_list


    def _get_type_and_score(self, col: str, table: TableData, expected_types: dict, scores: np.array, type_list: pd.DataFrame, restricted_types: dict, source: str) -> Tuple[str, str, float, float, pd.DataFrame, str]:
        """
        Finds column_type for each table column based on matchings and type of column data

        Args:
            col (str): column name
            table (TableData): template object
            expected_types (dict): expected types for few column_types
            scores (ndarray): matching scores for specific column
            type_list (DataFrame): table with all column_types
            restricted_types (List[str]): list of types that belong to source

        Output:
            initial_type (str): initially matched column type
            checked_type (str): confirmed column type
            initial_score (float): initial best (min) score
            checked_score (float): confirmed/corrected score
            type_list (DataFrame): updated table with all column_types

        """

        # get mask of free columns and type lookup
        mask = type_list.is_taken == False
        type_lookup = type_list[mask]

        # get min score and related type_id
        initial_score = min(scores[mask])
        idx_min = np.argmin(scores[mask])

        initial_type = type_lookup.type[idx_min]

        supplier_types = restricted_types['supplier']
        buyer_types = restricted_types['buyer']

        # reassign source if row source and row column are mismatching
        if initial_type in supplier_types and initial_type not in buyer_types:
            source = 'supplier'
        if initial_type in buyer_types and initial_type not in supplier_types:
            source = 'buyer'

        # get type of column
        typeof_column = self._check_type_column(table[col])

        # remove from expected types the ones that do not correspond to source
        filtered_types = set(restricted_types[source]) & set(expected_types.keys())
        expected_checked_types = {key: expected_types[key] for key in filtered_types}

        # get expected type or set it to column type
        if initial_type in expected_checked_types.keys():
            expected_type = expected_checked_types[initial_type]
        else:
            expected_type = KEYS.UNKNOWN

        # specify def. type with very high score also for unknown type
        #  if expected type matches and score is good enough
        if initial_score < 0.4 and typeof_column in (expected_type, 'unknown'):
            checked_type = initial_type
            checked_score = initial_score
        # specify def. type with somewhat high score only if expected type matches
        elif initial_score < 0.6 and typeof_column == expected_type:
            checked_type = initial_type
            checked_score = initial_score
        # set checked type if type of column is known
        elif typeof_column != 'unknown':
            checked_type = typeof_column
            checked_score = 0.41
        else:
            checked_type = ''
            checked_score = 1

        # update type list if checked_type is unique
        mask = (type_list.is_unique == True) & (type_list.type == checked_type)
        if any(mask):
            type_list.loc[mask, 'is_taken'] = True


        return initial_type, checked_type, initial_score, checked_score, type_list, source


    def _get_type_list(self, df_name_to_type: pd.DataFrame, column_types: pd.DataFrame) -> pd.DataFrame:
        """
        Creates helper DataFrame with all column_types that are of interest

        Args:
            df_name_to_type (DataFrame): matching of column names to column types
            column_types (DataFrame): table with all column types from database
        Output:
            type_list (DataFrame): table with all column_types
        """

        unique_column_types = column_types[column_types.is_unique == True].type

        # create type list
        type_list = df_name_to_type.drop_duplicates().copy()

        type_list['is_unique'] = type_list.type.isin(unique_column_types)
        type_list['is_taken'] = False

        return type_list


    def _remove_empty_headers(self, columns: np.ndarray, row_source: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
        """
        Removes empty header rows from columns and row_source arrays.

        Args:
            columns (ndarray): list of column names
            row_source (ndarray): array containing source row values

        Output:
            columns (ndarray): cleaned list of column names
            row_source (ndarray): cleaned array containing source row values
        """

        # find columns containing Unnamed: --> empty headers
        # TODO: add in future a test to check that column is really empty
        mask = columns.str.contains('Unnamed: ')

        # remove values from columns and source row
        cols = columns[~mask]
        row_src = row_source[~mask]

        return cols, row_src
