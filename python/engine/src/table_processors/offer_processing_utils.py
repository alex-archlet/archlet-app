"""
Functions used in the offers processing job
"""

import uuid
import json
import numpy as np
import pandas as pd

from scipy import stats

from collections import defaultdict
from collections.abc import Iterable
from numpy import NaN
from pathlib import Path
from typing import List
from typing import Tuple

from shared.communication import DBFile
from shared.communication import SingletonLogger
from shared.communication import external_api_caller
from shared.constants import UKEYS, KEYS
from shared.constants import OUTLIER
from shared.constants import PATHS
from shared.constants import RecordedError
from shared.entities import TableData
from shared.entities import TableHeader

slogger = SingletonLogger()

def is_number(column):
    """
    Returns a Series whose value at a given index is a boolean
    indicating whether or not the corresponding value
    in the input column is a number
    """
    # The replace(NaN, 1) is to avoid giving an error for the type AND a warning for empty cell
    non_valid_values = pd.to_numeric(column.replace(NaN, 1),
                                     errors='coerce').isnull()
    return non_valid_values

def is_text(column):
    """
    Returns a Series whose value at a given index is
    a boolean indicating whether or not the corresponding value
    in the input column is text
    """
    return ~is_number(column)

def check_types(offers: TableData) -> pd.Series:
    """
    Check the type of data contained in each column of the
    offers table, and if it doesn't correspond to the
    expected type, add it to the error messages
    """
    # get file from repo (rather ugly but works)
    directory_path = Path(__file__).absolute().parent.parent
    reference_path = directory_path / PATHS.REFERENCE_PATH

    with open(reference_path) as json_file:
        reference = json.load(json_file)

    expected_types = reference['typeof']

    header = offers.header

    df_offers = offers.data.copy()

    # For each column, we generate a list of the row indexes that have mistakes

    def check_column(column):

        if column.name not in header.ids:
            return pd.Series([])

        column_type = header.byid[column.name].type

        if column_type is None or column_type == KEYS.UNKNOWN:
            return pd.Series([])

        if column_type not in expected_types:
            return pd.Series([])
        expected_type = expected_types[column_type]
        checker = globals()['is_' + expected_type]
    
        return checker(column)

    result = df_offers.apply(check_column, axis=0).replace(True, 'OFFER_PROCESSING_ERROR_TYPE').replace(False, NaN)

    return result

def find_empty_cells(table: TableData) -> pd.DataFrame:
    """"
    Return a dataframe where the cells of the offers that don't contain nan are null, and
    the ones that do contain a warning enum
    """
    data = table.data.copy()
    data = data.replace(NaN, 'OFFER_PROCESSING_WARNING_EMPTY')
    data[data != 'OFFER_PROCESSING_WARNING_EMPTY'] = NaN
    return data


def find_zero_cells(table: TableData) -> pd.DataFrame:
    """
    Return a dataframe where the cells of the offers that don't contain 0 are null, and
    the ones that do contain a warning enum
    """
    data = table.data.copy()
    data = data.replace(0, 'OFFER_PROCESSING_WARNING_ZERO')
    data[data != 'OFFER_PROCESSING_WARNING_ZERO'] = NaN
    return data



def find_outlier_cells(table: TableData) -> pd.DataFrame:
    """
        Returns a dataframe where the cells of the offers that contain outlier values 
        are a warning enum andthe rest are null

        Args:
            table: the table on which the outlier check is performed

        Output:
            A table data containing the detected outlier price cells
    """

    data = table.data.copy()
    prices = table.header[KEYS.PRICE]
    currencies = table.header[KEYS.CURRENCY]

    # Check if currency column exists
    # TODO redo this for future iterations, where more than one currency column is available per table
    currency_column = None
    if len(currencies):
        # TODO add log ERROR for when currency column is missing
        # TODO add lod DEBUG when more there are more than one with the same name
        currency_column = data[currencies[0]]
        currency_column = currency_column.fillna('no_curr')

    for price_column_uuid in prices:
        original_price_column = data[price_column_uuid]

        # If there is a price column with only NaN values (aka optional price column) skip it
        if pd.isnull(original_price_column).all():
            continue

        # Convert prices if currency column exist, otherwise use original prices
        updated_prices = convert_to_currency(original_price_column, currency_column) if currency_column is not None else original_price_column
        # Retrieve all numeric cells in price column only, which are also not 0
        price_column = updated_prices[~is_number(updated_prices)]
        price_column = price_column[price_column != 0].dropna()

        if not np.count_nonzero(price_column): # if price_column contains only 0s
            continue
        # Non Normal and Normal Distribution: Use Tukey fences theory
        # Theory: https://sphweb.bumc.bu.edu/otlt/mph-modules/bs/bs704_summarizingdata/bs704_summarizingdata7.html
        Q1 = np.quantile(price_column, OUTLIER.FIRST_QUANTILE)
        Q3 = np.quantile(price_column, OUTLIER.THIRD_QUANTILE)
        IQR = Q3 - Q1
        upper_bound = Q3 + OUTLIER.HARD_DEVIATION * IQR
        
        # Give outlier values, higher than the upper bound, a flag
        updated_prices = (pd.to_numeric(updated_prices, errors='coerce')).replace(np.nan, 0, regex=True)
        outlier_mask = (~is_number(updated_prices)) & (updated_prices.notna()) & (updated_prices > upper_bound)
        data[price_column_uuid][outlier_mask] = 'OFFER_PROCESSING_WARNING_OUTLIER'
    
    
    data[data != 'OFFER_PROCESSING_WARNING_OUTLIER'] = NaN
    return data


def get_rates(currencies: List, to_currency: str) -> dict:
        """
            Retrieves a dictionary of from_currency -> to_currency conversion rates

        Args:
            currencies: list of all unique currencies

        Output:
            A pd.Series, containing the conversion rates
        """
        rates_to_currency = {}
        for from_currency in currencies:
            result = external_api_caller.get_currency_rates(from_currency)
            if isinstance(result, str):
                slogger.debug(f"The conversion {from_currency} to {to_currency} is not found.\n \
                    Instead using raw prices for entries with this currency.")
                rates_to_currency[from_currency] = 1
            else:
                rates_to_currency[from_currency] = result[to_currency]
        return rates_to_currency


def convert_to_currency(price_column: pd.Series, currency_column: pd.Series, to_currency: str = 'USD') -> pd.Series:
    """
        Convert a price column, given a currency column, to a currency of choice

        Args:
            price_column: the price column which is to be converted
            currency_column: the currency column corresponding to the price column
            to_currency: the currency to which the prices are converted

        Output:
            A pd.Series, containing the coverted prices
    """
    # Get all unique currencies in currency column
    unique_currencies = currency_column.unique()
    # If there is only one currency and it matches to_currency return the column without performing conversion
    if len(unique_currencies) == 1 and unique_currencies[0] == to_currency:
        return price_column

    # Retrieve a dictionary of from_currency -> to_currency conversion rates
    rates = get_rates(unique_currencies, to_currency)
    rates_column = pd.Series([rates[currency] for currency in currency_column])

    # Convert to to_currency all numeric records in the price column
    valid_price_mask = (~is_number(price_column)) & (price_column.notna())
    price_column[valid_price_mask] *= rates_column[valid_price_mask]

    return price_column


def delete_buyer_columns(offers: List[TableData], header: TableHeader, item_col_demands: str) -> List[TableData]:
    """
    Delete the columns that come from the demands, except
    the Item ID column which is needed to match to the demands.
    """
    new_offers = []

    for offers_table in offers:
        # TODO : change that, we need the demands_header to get the name of item_id
        columns_to_keep = list(set(offers_table.data.keys()) & set([name for name in header.names] + [item_col_demands]))
        missing_colums = list(set([name for name in header.names]) - set(offers_table.data.keys()))
        # columns_to_keep = list(set(offers_table.keys()) & set(header.columns.keys()))
        offers_table.data = offers_table[columns_to_keep]
        offers_table.trim()
        for missing in missing_colums:
            offers_table[missing] = None
        new_offers.append(offers_table)

    header.add_column(UKEYS.ITEM_ID, original_name=item_col_demands)
    # header.add_column(item_col_demands, UKEYS.ITEM_ID, '')

    return new_offers


def concatenate_offers(offers_header: TableHeader,
                       offers_tables: List[TableData],
                       table_specifiers: List[DBFile]) -> TableData:
    """
    Consolidate all the offers from the given offers tables into one offers table.
    """
    offers = TableData()

    # Translate all the offer tables column names and put them in a list of data frames
    # offers_columns = offers_header.dump_header().values()
    data_frames = []
    # The buyer side columns are not translated in this step,
    # so here it works since they all have nicely the same name

    for offers_table, file in zip(offers_tables, table_specifiers):
        frame = offers_table.data
        header = offers_table.header
        frame['optimization_file_id'] = file.id
        data_frames.append(frame)

    # Concatenate the frames:
    result_table = pd.concat(data_frames, ignore_index=True)

    offers.data = result_table
    offers.trim()
    offers.header = offers_header
    offers.add_uuid_names()

    # convert ITEM_ID to string for matching purposes
    offers[offers_header[UKEYS.ITEM_ID]] = offers[offers_header[UKEYS.ITEM_ID]].astype(str)

    return offers


def delete_offers_no_demands(offers: TableData, demands: TableData) -> TableData:
    """
    Delete all rows containing offers for which there is no demands

    Args:
        offers (TableData): Offers table to be cleaned
        demands (TableData): Demands table for reference
    
    Returns:
        offers (TableData): Offers table cleaned from unrelated offers
    """
    offers_data = offers.data
    demands_data = demands.data

    demands_header = demands.header
    offers_header = offers.header

    demands_list = demands_data[demands_header[UKEYS.ITEM_ID]].tolist()

    def issue_warning(row):
        if row[offers_header[UKEYS.ITEM_ID]] not in demands_list:
            slogger.warning((f'Offer for item {row[offers_header[UKEYS.ITEM_ID]]} from '
                           f'supplier {row[offers_header[UKEYS.SUPPLIER]]} has no matching demand and was deleted'))
            return False
        else:
            return True

    offers_data = offers_data[offers_data.apply(issue_warning, axis=1)]

    offers_data.reset_index(drop=True, inplace=True)
    offers.data = offers_data
    offers.trim()

    return offers


def delete_empty_offers(offers: TableData) -> TableData:
    """
    Delete all rows containing offers for which all prices are zero

    Args:
        offers (TableData): Offers table to be cleaned
    
    Returns:
        offers (TableData): Offers table cleaned from zero rows
    """
    offers_data = offers.data
    offers_header = offers.header

    # Remove all rows with all zero prices
    # Assumption that offers are not of interest if no prices were provided
    price_components = offers_header[KEYS.PRICE]
    mask = ((offers[price_components] == 0) | (offers[price_components].isnull())).all(axis=1)

    # Issue warning when rows were removed
    if mask.sum() > 0:
        gen_warning = lambda x: slogger.warning(f'For bidder {x[offers_header[UKEYS.SUPPLIER_ID]]} {x[offers_header[UKEYS.ITEM_ID]]} offers were removed because all prices were zero.')
        temp_df = offers_data[mask][offers_header[[UKEYS.SUPPLIER, UKEYS.ITEM_ID]]].groupby(offers_header[UKEYS.SUPPLIER]).count().reset_index()
        temp_df.apply(gen_warning, axis=1)

    # Clean data and return 
    offers_data = offers_data[~mask]
    offers.data = offers_data.reset_index(drop=True)
    return offers


def delete_duplicated_offers(offers: TableData) -> TableData:
    """
    Delete all duplicated offers

    Args:
        offers (TableData): Offers table to be cleaned
    
    Returns:
        offers (TableData): Offers table cleaned from duplicated offers
    """
    offers_data = offers.data
    offers_header = offers.header

    # create identifier_list based on which duplicates are encountered
    identifier_list = [offers_header[UKEYS.ITEM_ID], offers_header[UKEYS.SUPPLIER_ID]]
    if UKEYS.ITEM_CATEGORY in offers_header.types:
        identifier_list.append(offers_header[UKEYS.ITEM_CATEGORY])

    # check for duplicates
    mask = offers_data.duplicated(subset=identifier_list)

    # Issue warning when rows were removed
    if mask.sum() > 0:
        gen_warning = lambda x: slogger.warning(f'For bidder {x[offers_header[UKEYS.SUPPLIER_ID]]} {x[offers_header[UKEYS.ITEM_ID]]} offers were removed because they are duplicates.')
        temp_df = offers_data[mask][offers_header[[UKEYS.SUPPLIER, UKEYS.ITEM_ID]]].groupby(offers_header[UKEYS.SUPPLIER]).count().reset_index()
        temp_df.apply(gen_warning, axis=1)
    
    # Clean data and return
    offers_data = offers_data[~mask]
    offers.data = offers_data.reset_index(drop=True)
    return offers


def compute_changes(new_table: TableData, old_table: TableData) -> pd.Series:
    """
    Compute the difference between the previous offers and the new ones
    """
    old_data = old_table.data
    old_header = old_table.header
    new_data = new_table.data

    header = new_table.header

    columns_to_check = set(new_data.columns) & set(old_data.columns)
    # I don't think that this is needed anymore -> the io_facade already removed this
    # columns_to_check.remove('optimization_file_id')

    def changes_on_row(row):
        previous_values = old_data[(old_data[old_header[UKEYS.SUPPLIER]] == row[header[UKEYS.SUPPLIER]]) & (old_data[old_header[UKEYS.DEMAND_ID]] == row[header[UKEYS.DEMAND_ID]])]
        if previous_values.empty:
            return pd.Series([])
        else:
            previous_row = previous_values[columns_to_check].reset_index(drop=True).loc[0]
            mask = previous_row.eq(row[columns_to_check])
            return previous_row.mask(mask, pd.Series([]))

    result = new_data.apply(changes_on_row, axis=1)

    return result

def create_messages(errors: List[Tuple[str, pd.Series]],
                    warnings: List[Tuple[str, pd.Series]],
                    changes: List[Tuple[str, pd.Series]],
                    offers: TableData,
                    header: TableHeader) -> TableData:
    """
    Process the error/warning DataFrames to create the processing messages column
    """
    # mapping = {dic['name']: dic['id'] for dic in header.columns.values()}
    mapping = {col.original_name: col.id for col in header.columns}
    offers['processing_messages'] = pd.Series([defaultdict(lambda: {'error': {}, 'warning': {}}) for _ in range(offers.data.shape[0])])

    def process_df(df: pd.DataFrame, enum: str, issue_type: str) -> pd.Series:
        df = df.rename(columns=mapping)
        def row_to_dic(row):
            row = row[row.notnull()]
            for col_id, values in row.iteritems():
                if not isinstance(values, Iterable) or isinstance(values, str):
                    if values != enum:
                        values = (values,)
                    else:
                        values = tuple()
                else:
                    values = tuple(values)

                if issue_type == 'change':
                    offers['processing_messages'][int(row.name)][col_id][issue_type] = values[0]
                else:
                    offers['processing_messages'][int(row.name)][col_id][issue_type]['enum'] = enum
                    for counter, value in enumerate(values):
                        offers['processing_messages'][int(row.name)][col_id][issue_type][f'var{counter}'] = value
                    
        df.apply(row_to_dic, axis=1)


    for error_enum, error_df in errors:
        process_df(error_df, error_enum, 'error')

    for warning_enum, warning_df in warnings:
        process_df(warning_df, warning_enum, 'warning')

    if changes is not None:
        process_df(changes, None, 'change')

    # Convert defaultdict to dict so it can be serialized
    offers['processing_messages'] = offers['processing_messages'].apply(dict)

    return offers


# Not used at the moment
def add_volume(offers: TableData, demands: TableData) -> TableData:
    """
    If the offers dont contain a volume column, add the one from the demands
    """
    volumes = demands.header[KEYS.VOLUME]
    offers.data = pd.merge(offers.data,
                           demands[demands.header[[UKEYS.ITEM_ID]] + volumes],
                           left_on=offers.header[UKEYS.ITEM_ID],
                           right_on=demands.header[UKEYS.ITEM_ID])
    renamer = {}
    for vol_id in volumes:
        vol_name = demands.header.byid[vol_id].original_name
        offers.header.add_column(KEYS.VOLUME,
                                 original_name=vol_name,
                                 attribute=True)
        renamer[vol_id] = offers.header.byname[vol_name].id

    offers.data = offers.data.rename(columns=renamer)

    return offers
