"""
Preprocessor class
"""
import copy
import json
import os
import pandas as pd
import uuid

from collections import defaultdict
from numpy import NaN
from numpy import inf
from os.path import abspath
from os.path import dirname
from os.path import join
from typing import Dict
from typing import List
from typing import Tuple

from table_processors.offer_processing_utils import delete_duplicated_offers, delete_empty_offers

from shared.communication import DBFile
from shared.constants import DATA
from shared.communication import SingletonLogger
from shared.constants import UKEYS, KEYS
from shared.constants import RecordedError
from shared.constants import STATUS_MESSAGE_CATEGORY
from shared.entities import TableData
from shared.entities import TableHeader
from shared.entities import Translator
from shared.utilities.db_table_processor import clean_numeric_cols
from shared.utilities.kpi_utils import create_user_renamer
from shared.utilities.price_component_utils import price_components_structure

from shared.communication.singleton_logger import SingletonLogger

Str_Dict = Dict[str, str]

slogger = SingletonLogger()

class Preprocessor:
    def __init__(self) -> None:
        """
        This is the constructor for the processor class.

        Args:
            files (dict): Dictionary containing the absolute path to required
                Excel files
            translator (Translator): Translates column names from client-
                specific to client-invariant

        Raises:
            RecordedError
        """
        pass


    def translate_tables(self, translator: Translator, tables: Dict[str, TableData]) -> Dict[str, TableData]:
        """
        Translates the uploaded file to the standard columns names.
        Fetches the initializer specific to the table type, used to preprocess the tables
        """
        col_names = self.get_necessary_columns(translator)
        for name, table in tables.items():
            table.initialize(translator, name, col_names[name])
            table_initializer = getattr(self, f'initialize_{name}_table', None)
            if table_initializer is not None:
                table = table_initializer(table)
            tables[name] = table

        return tables

    @staticmethod
    def get_necessary_columns(translator: Translator):
        """ Create dictionary of additional necessary columns for each table that can be processed """

        offers_cols = list(set(getattr(translator, 'soft_constraints')) |
                          set(getattr(translator, 'custom_columns')) |
                          set(getattr(translator, 'capacities')) |
                          set(getattr(translator, 'second_axis')))
        demands_cols = getattr(translator, 'filters')
        capacities_cols = getattr(translator, 'capacities') if getattr(translator, 'capacities') else None

        col_dict = defaultdict(lambda : None, {
            'offers': offers_cols,
            'demands': demands_cols,
            'capacities': capacities_cols
        })

        return col_dict


    def initialize_offers_table(self, table: TableData) -> TableData:
        """ Offers table initializer """
        # Drop rows with invalid item_id
        header = table.header
        table.drop_null_rows(header[UKEYS.ITEM_ID])

        # Parse columns of comma-separated data and convert to Python arrays
        if UKEYS.CONTRIBUTING_TO in header.types:
            table.convert_to_lists(header[UKEYS.CONTRIBUTING_TO], inplace=True)
            table.convert_to_lists(header[UKEYS.DISCOUNTED_BY],   inplace=True)

        # Initialize columns not loaded from Excel
        header.add_column(UKEYS.OFFER_ID, UKEYS.OFFER_ID, '')
        header.add_column(UKEYS.DEMAND_ID, UKEYS.DEMAND_ID, '')
        table[header[UKEYS.OFFER_ID]], table[header[UKEYS.DEMAND_ID]] = NaN, NaN

        # Add unique offer IDs
        table.add_unique_ids(header[UKEYS.OFFER_ID])

        # Check for redundant offers
        cols = [UKEYS.ITEM_ID, UKEYS.SUPPLIER]

        if UKEYS.ITEM_CATEGORY in header.types:
            cols.append(UKEYS.ITEM_CATEGORY)

        duplicates_mask = table.data.duplicated(header[cols])
        table.issue_errors(duplicates_mask,
                          'Multiple offers for the same item from same supplier',
                          header[UKEYS.ITEM_ID],
                          raise_error=True)
        # clean offers from non-numeric values
        numeric_cols = list(set([header[UKEYS.RAW_PRICE]]) |
                            set(header[KEYS.PRICE]) |
                            set(header[KEYS.NUMBER]) |
                            set(header[KEYS.CAPACITY]))
        table = clean_numeric_cols(table, numeric_cols, True)

        # clean offers from empty lines
        table = delete_empty_offers(table)

        # Check for redundant offers
        table = delete_duplicated_offers(table)

        # Check for offers with price zero
        invalid_price_mask = table[header[UKEYS.RAW_PRICE]].isnull() | table[header[UKEYS.RAW_PRICE]] == 0
        table.issue_warnings(invalid_price_mask,
                             'Offer with price equal to zero. Will be removed',
                             header[UKEYS.ITEM_ID],
                             True)

        return table


    def initialize_currencies_table(self, table: TableData) -> TableData:
        """ Currencies table initializer """
        header = table.header
        mask = table.data.duplicated(header[KEYS.CURRENCY])
        table.issue_errors(mask,
                          'Duplicate rows with respect to currency',
                          None,
                          raise_error=True)

        return table


    def initialize_demands_table(self, table: TableData) -> TableData:
        """ Demands table initializer """
        # Drop rows with invalid item_id
        header = table.header
        table.drop_null_rows(header[UKEYS.ITEM_ID])

        # Check for non-unique item IDs
        mask = table.data.duplicated(header[UKEYS.ITEM_ID])
        if any(mask):
            table.issue_errors(mask,
                              'Multiple demands for a single item ID',
                              header[UKEYS.ITEM_ID],
                              raise_error=True)

        # Parse columns of comma-separated data and convert to Python arrays
        if UKEYS.CONTRIBUTING_TO in header.types:
            table.convert_to_lists(header[UKEYS.CONTRIBUTING_TO], inplace=True)
            table.convert_to_lists(header[UKEYS.DISCOUNTED_BY],   inplace=True)

        # Initialize columns not loaded from Excel
        header.add_column(UKEYS.DEMAND_ID, UKEYS.DEMAND_ID, '')
        table[header[UKEYS.DEMAND_ID]] = NaN

        # Add unique demands IDs
        table.add_unique_ids(header[UKEYS.DEMAND_ID])

        return table


    def initialize_discounts_table(self, table: TableData) -> TableData:
        """ Discounts table initializer """
        # Drop rows with invalid bucket ID
        header = table.header
        table.drop_null_rows(header[UKEYS.BUCKET_ID])

        # Make sure each bucket has a single type of discount
        for bucket_id in table[header[UKEYS.BUCKET_ID]].values:
            mask = table[header[UKEYS.BUCKET_ID]] == bucket_id
            if len(table.data.loc[mask, header[UKEYS.DISCOUNT_TYPE]].unique()) > 1:
                table.data = table[header[UKEYS.BUCKET_ID] != bucket_id]
                table.trim()

        # Check for redundant discounts
        mask = table.data.duplicated(header[[UKEYS.BUCKET_ID, UKEYS.THRESHOLD]])
        table.issue_warnings(mask,
                             'Multiple discounts for same target volume. First discount is kept.',
                             header[UKEYS.BUCKET_ID],
                             drop=True)

        # Check for non-increasing discount amount
        table.data.sort_values([header[UKEYS.BUCKET_ID], header[UKEYS.DISCOUNT_AMOUNT]],
                               ascending=[True, True],
                               inplace=True)
        mask = table.data.duplicated(header[[UKEYS.BUCKET_ID, UKEYS.DISCOUNT_AMOUNT]]).values
        table.issue_warnings(mask,
                             'Multiple targets for same discount amount. First target is kept.',
                             header[UKEYS.BUCKET_ID],
                             drop=True)

        return table


    def clean_discounts_table(self,
                              discounts_table: TableData,
                              discount_units: Dict[str, str],
                              discount_types: Dict[str, str],
                              offers_header: TableHeader) -> TableData:
        """
        Clean the discounts table such that all columns are conform internal specifications
        """
        discounts_header = discounts_table.header

        necessary_columns = [UKEYS.BUCKET_ID,
                             UKEYS.THRESHOLD,
                             UKEYS.DISCOUNT_AMOUNT,
                             UKEYS.SUPPLIER]

        for col in necessary_columns:
            if col not in discounts_header.types:
                slogger.error(f"Discounts table is missing column: {col}", save_message=True)
                raise RecordedError(f"Discounts table missing {col} column")
            mask = discounts_table[discounts_header[col]].isna()
            if any(mask):
                discounts_table.issue_errors(mask,
                                             "missing value in required column",
                                             col)

        default_columns = [(UKEYS.DISCOUNT_UNIT, 'volume'),
                           (UKEYS.DISCOUNT_TYPE, 'unit'),
                           (UKEYS.VOLUME_NAME, None),
                           (UKEYS.PRICE_COMPONENT, UKEYS.TOTAL_PRICE),
                           (UKEYS.DESCRIPTION, None)]
        for (col_name, default) in default_columns:
            discounts_table.set_default_column(col_name, value=default)

        # TODO: find better way of treating commas in strings to be stored
        discounts_table[discounts_header[UKEYS.DESCRIPTION]] = discounts_table[discounts_header[UKEYS.DESCRIPTION]].str.replace(',', '')

        self.check_restricted_column(discounts_table, UKEYS.DISCOUNT_UNIT, discount_units)
        self.check_restricted_column(discounts_table, UKEYS.DISCOUNT_TYPE, discount_types)

        def price_comp_to_uuid(row, discounts_header, offers_header):
            price_comp = row[discounts_header[UKEYS.PRICE_COMPONENT]]
            if pd.isnull(price_comp) or price_comp == UKEYS.TOTAL_PRICE:
                return None
            if price_comp not in [col.original_name for col in offers_header.columns if col.matching_result is None or col.matching_result.calculation is None]:
                print(f"{price_comp} is not present in the offers table")
                raise RecordedError(f"{price_comp} is not present in the offers table")
            return offers_header.byname[price_comp].id

        discounts_table[discounts_header[UKEYS.PRICE_COMPONENT]] = discounts_table.data.apply(lambda row: price_comp_to_uuid(row, discounts_header, offers_header), axis=1)

        return discounts_table


    @staticmethod
    def check_restricted_column(table: TableData, column_name: str, values: Dict[str, str]) -> None:
        """
        Verify that all entries in table[column_name] are from the given values.
        """
        mask = ~table[table.header[column_name]].isin(values)
        if any(mask):
            table.issue_errors(mask,
                               (f"Wrong value encountered in {column_name}\n"
                                f"only accepted values are: {str(values)}"),
                               table.header[column_name])


    def clean_capacities_table(self, capacities_table: TableData, offers_table: TableData) -> TableData:
        """
        Clean the capacities table.
        """
        capa_header = capacities_table.header
        supplier_name = offers_table.header.byid[offers_table.header[UKEYS.SUPPLIER]].original_name
        capa_header.byid[capa_header[UKEYS.SUPPLIER]].original_name = supplier_name
        capacities_table.header = capa_header
        return capacities_table


    def initialize_duties_table(self, table: TableData) -> TableData:
        """ Duties table initializer """
        header = table.header
        mask = table.data.duplicated(header[[UKEYS.SUPPLIER_COUNTRY, UKEYS.CLIENT_COUNTRY]]).values
        table.issue_errors(mask,
                           'Duplicate rows with respect to origin/destination',
                           None,
                           raise_error=True)

        return table


    def initialize_capacities_table(self, table: TableData) -> TableData:
        return table

    def get_baseline_offers(self, allocations: TableData) -> TableData:
        """
        Construct a baseline offers table from the given allocations table.

        Args:
            allocations: the allocations table from which to extract offers

        Returns:
            TableData object with the extracted offers
        """
        header = allocations.header
        header.add_column(UKEYS.NORMALIZED_PRICE, UKEYS.NORMALIZED_PRICE, '')
        allocations[header[UKEYS.NORMALIZED_PRICE]] = allocations[header[UKEYS.UNIT_PRICE]]
        allocations.header = header
        return allocations


    def add_demand_ids(self, offers: TableData, demands: TableData) -> TableData:
        """
        Add the demand ids to the offers.

        Args:
            offers table
            demands table

        Return:
            modified offers table
        """
        offers_header = offers.header
        demands_header = demands.header
        if (UKEYS.ITEM_ID not in offers_header.types or
            UKEYS.ITEM_ID not in demands_header.types):
            slogger.error("item id column is missing, cannot link offers to demands", save_message=True)
            raise RecordedError

        offers.data.set_index(offers_header[UKEYS.ITEM_ID], inplace=True)
        demands.data.set_index(demands_header[UKEYS.ITEM_ID], inplace=True)

        offers[offers_header[UKEYS.DEMAND_ID]] = demands[demands_header[UKEYS.DEMAND_ID]]

        offers.data.reset_index(inplace=True)
        demands.data.reset_index(inplace=True)
        return offers

    @staticmethod
    def copy_ids(destination: TableData, source: TableData, dst_on: List[str], src_on:List[str], id_column: str) -> TableData:
        """
        Copy ids from source to destination where the matching is done on src_on and dst_on.
        Additionally, add a column to destination which indicates the state of each row (present or new).
        """
        header = destination.header
        source.data.set_index(src_on, inplace=True)
        destination.data.set_index(dst_on, inplace=True)
        dest_id, source_id = destination.header[id_column], source.header[id_column]
        destination[dest_id] = source[source_id]
        # Add column with data status:
        header.add_column(UKEYS.DB_STATUS)
        destination[header[UKEYS.DB_STATUS]] = DATA.PRESENT
        # fill missing ids
        new_row_mask = pd.isnull(destination[dest_id])
        if any(new_row_mask):
            nb_new = new_row_mask.value_counts()[True]
            destination.data.loc[new_row_mask, dest_id] = [str(uuid.uuid4()) for _ in range(nb_new)]
            destination.data.loc[new_row_mask, header[UKEYS.DB_STATUS]] = DATA.NEW
        destination.data.reset_index(inplace=True)
        source.data.reset_index(inplace=True)
        return destination


    @staticmethod
    def copy_header(destination: TableData, source: TableData):
        """
        Copy the header from the source to the destination.
        Rename the columns in the data frame accordingly.
        """
        src_header = source.header
        renamer = {}
        new_header = TableHeader()
        for col in destination.header.columns:
            # Only unique keys should be added with an empty original name
            # and these have as unique key the 
            if col.original_name == '':
                new_header.columns = new_header.columns + [copy.deepcopy(col)]
                continue
            try:
                new_col = src_header.byname[col.original_name]
                # Keep the type from the translator
                new_col.type = col.type
                renamer[col.id] = new_col.id
                new_header.columns = new_header.columns + [copy.deepcopy(new_col)]
            except KeyError:
                slogger.warning(f"the column {col.original_name} was not found in the old table.")
                new_header.columns = new_header.columns + [copy.deepcopy(col)]
        destination.data.rename(columns=renamer, inplace=True)
        destination.header = new_header
        return destination

    def process_table(self, table: TableData, tablename: str) -> TableData:
        """ Fetches the preprocessor specific to the table type """
        try:
            table_processor = getattr(self, f'process_{tablename}_table')
        except:
            return table
        if table is None:
            return table
        processed_table = table_processor(table)
        return processed_table


    def process_allocations_table(self, table: TableData) -> TableData:
        """ Allocations table preprocessor. Transforms strings containing lists to list objects """
        header = table.header
        keys = [UKEYS.DISCOUNTED_BY, UKEYS.CONTRIBUTING_TO, UKEYS.DISCOUNT_TYPES, UKEYS.DISCOUNTED_BY]

        for key in keys:
            if key in header.types:
                table.convert_to_lists(header[key], inplace=True)

        return table


    def process_demands_table(self, table: TableData) -> TableData:
        """ Demands table preprocessor. Transforms strings containing lists to list objects """
        header = table.header

        # Parse columns of comma-separated data and convert to Python arrays
        if UKEYS.CONTRIBUTING_TO in header.types:
            table.convert_to_lists(header[UKEYS.CONTRIBUTING_TO], inplace=True)
        if UKEYS.DISCOUNTED_BY in header.types:
            table.convert_to_lists(header[UKEYS.DISCOUNTED_BY],   inplace=True)

        return table


    def process_offers_table(self, table: TableData) -> TableData:
        """ Offers table preprocessor. Transforms strings containing lists to list objects """
        # If loaded from a file where the table had already added unique IDs,
        # simply populate the `next_free_id` variable
        header = table.header

        # Parse columns of comma-separated data and convert to Python arrays
        if UKEYS.CONTRIBUTING_TO in header.types:
            table.convert_to_lists(header[UKEYS.CONTRIBUTING_TO], inplace=True)
        if UKEYS.DISCOUNTED_BY in header.types:
            table.convert_to_lists(header[UKEYS.DISCOUNTED_BY], inplace=True)

        return table

    def process_discounts_table(self, table: TableData) -> TableData:
        """
        Put the uuid names in the dataframe such that they can be accessed using the table header.

        TO DO: Remove this part when discounts table comes from database.
        """
        header = table.header
        renamer = {col.type: col.id for col in header.columns}
        # renamer = {internal_name: dic['id'] for internal_name, dic in header.columns.items()}
        table.data = table.data.rename(columns=renamer)

        return table


    def create_table(self, table : TableData, tablename: str) -> TableData:
        """ Fetches the method that creates an empty table with the columns specific to the table type """
        try:
            table_creator = getattr(self, f'create_{tablename}_table')
        except:
            return table
        processed_table = table_creator(table)
        return processed_table


    def extract_baseline(self, demands: TableData, offers_header: TableHeader) -> Tuple[TableData, TableData]:
        """
        Extract the baseline offers from the given demands table and
        return the result as a table data object

        Args:
            demands
            offers_header

        Returns:
            (baseline_offers, demands): the extracted historical offers and the updated demands

        NOTE: for this method to work, at least volume, normalized_price,
        supplier and item_id columns have to be present in offers_header
        NOTE 2: at least volume, prior_price, prior_supplier and item_id
        have to be present in the demands table header
        """
        result = TableData()

        # Add all the columns from the offers header to the resulting extracted offers:
        result_df = pd.DataFrame(columns=offers_header.ids)
        result.data = result_df
        result.trim()

        if UKEYS.TOTAL_PRICE not in offers_header.types:
            offers_header.add_column(UKEYS.TOTAL_PRICE)

        result_header = offers_header.copy()
        result.header = result_header

        header = demands.header

        if (UKEYS.SUPPLIER not in header.types or
                UKEYS.TOTAL_PRICE not in header.types):
            slogger.warning("no baseline offers available")
            return result, demands

        result[offers_header[UKEYS.TOTAL_VOLUME]] = demands[header[UKEYS.TOTAL_VOLUME]] # Should be total_volume
        result[offers_header[UKEYS.TOTAL_PRICE]] = demands[header[UKEYS.TOTAL_PRICE]]
        result[offers_header[UKEYS.NORMALIZED_PRICE]] = demands[header[UKEYS.TOTAL_PRICE]]
        result[offers_header[UKEYS.SUPPLIER]] = demands[header[UKEYS.SUPPLIER]]
        result[offers_header[UKEYS.ITEM_ID]] = demands[header[UKEYS.ITEM_ID]]
        result[offers_header[UKEYS.DEMAND_ID]] = demands[header[UKEYS.DEMAND_ID]]

        if UKEYS.ITEM_CATEGORY in offers_header.types and not UKEYS.ITEM_CATEGORY in header.types: # Might be removable, have to test
            result[offers_header[UKEYS.ITEM_CATEGORY]] = 'standard'

        current_columns = [UKEYS.TOTAL_VOLUME, UKEYS.TOTAL_PRICE, UKEYS.SUPPLIER, UKEYS.ITEM_ID, UKEYS.DEMAND_ID]
        indexes_types = [offers_header.types.index(col_type) for col_type in current_columns]
        current_columns = [offers_header.names[index] for index in indexes_types]
        other_columns = set([col.original_name for col in offers_header.columns if col.calculation is None]) - set(current_columns)
        other_columns = other_columns & set([col.original_name for col in header.columns if col.calculation is None])
        for col in other_columns:
            result[offers_header.byname[col].id] = demands[header.byname[col].id]

        del demands[header[UKEYS.TOTAL_PRICE]]
        del demands[header[UKEYS.SUPPLIER]]
        del header[UKEYS.TOTAL_PRICE]
        del header[UKEYS.SUPPLIER]
        # header.remove_column(KEYS.PRICE)
        # header.remove_column(UKEYS.SUPPLIER)

        return result, demands


    @staticmethod
    def extract_project_settings(translator: Translator,
                                 demands_header: TableHeader,
                                 offers_header: TableHeader) -> dict:
        """ Extract the project settings from the translator """
        settings = {}
        if hasattr(translator, 'custom_kpis'):
            settings['custom_kpis'] = getattr(translator, 'custom_kpis')

        def set_if_present(attribute, header):
            if hasattr(translator, attribute):
                # internal_col_names = [header.byname[user_col].type for user_col in getattr(translator, attribute)]
                # internal_col_names = [header.reverse_translate(user_col) for user_col in getattr(translator, attribute)]
                # settings[attribute] = [header]
                settings[attribute] = [header.byname[col_name].id for col_name in getattr(translator, attribute)]

        settings_attributes = {'filters': demands_header,
                               'soft_constraints': offers_header,
                               'custom_columns': offers_header,
                               'second_axis': offers_header}

        for attribute, header in settings_attributes.items():
            set_if_present(attribute, header)

        if translator.price_components:
            settings['price_components_structure'] = price_components_structure(translator.price_components)
        else:
            settings['price_components_structure'] = [{"name": UKEYS.TOTAL_PRICE, "children": []}]

        if UKEYS.ITEM_CATEGORY in offers_header.types:
            settings['offers_filters'] = [offers_header[UKEYS.ITEM_CATEGORY]]
        return settings

    @staticmethod
    def parse_custom_kpis(custom_kpis: Str_Dict,
                          demands_header: TableHeader,
                          offers_header: TableHeader,
                          has_discounts: bool) -> List[str]:
        """
        Convert the custom kpis from user names to uuids

        Args:
            - custom_kpis: dictionary with the kpi definitions from the translator
            - demands_header
            - offers_header
            - has_discounts: if True, add a custom KPI to calculate the discounts

        Returns:
            - Dict: <kpi_name> -> <kpi_calculation>
        """
        if has_discounts:
            custom_kpis['Discount Amount'] = "allocations.'total_volume' * allocations.'discount_amount'"
        renamer = create_user_renamer(offers_header, 'offers')
        renamer.update(create_user_renamer(demands_header, 'demands'))
        def rename(expression: str, renamer: Str_Dict) -> str:
            for old, new in renamer.items():
                expression = expression.replace(old, new)
            return expression
        result = {name: rename(expression, renamer) for name, expression in custom_kpis.items()}
        return result


    @staticmethod
    def get_default_volume(offers: TableData) -> str:
        """
        Get one of the volumes and set it as default
        """
        header = offers.header
        volumes = header[KEYS.VOLUME]
        if not volumes:
            vol_name = ''
        else:
            vol = volumes[0]
            vol_name = header.byid[vol].original_name
        return vol_name


    @staticmethod
    def set_default_volume(table: TableData, volume_col: str) -> TableData:
        """
        Add the previously chosen volume as the default one
        """
        header = table.header
        header.add_column(UKEYS.TOTAL_VOLUME, attribute=False)
        table[header[UKEYS.TOTAL_VOLUME]] = table[header.byname[volume_col].id]
        return table


    def set_total_price(self, table: TableData) -> TableData:
        """
        Add a column called 'Total Price'
        """
        # Note1: for now we use the normalized price also as Total Price
        # This should be updated to be the calculated Total Price once we know
        # how 'Total Price' will be represented in the graphs in project info
        # Note2: there is an error when a user has a column named 'Total Price'
        header = table.header
        header.add_column(UKEYS.TOTAL_PRICE, original_name='price_total', attribute=True)
        # header.add_column('Total Price', 'Total Price', 'price_total', attribute=True)
        table[header[UKEYS.TOTAL_PRICE]] = table[header[UKEYS.NORMALIZED_PRICE]]
        return table


    def _convert_currencies(self, offers: TableData, currencies: TableData) -> TableData:
        """
        This function converts the prices in the offers tables to a specified "base" currency
        using exchange rates present in the currencies table
        in the c

        Args:
            offers (TableData): offers object
            currencies (TableData): currencies object

        Return:
            offers (TableData): offers object with converted currencies
        """
        offers_header = offers.header
        curr_header = currencies.header

        # Add currency rate for each offer from currencies table
        offers.data = pd.merge(left=offers.data,
                             right=currencies.data,
                             left_on=offers_header[KEYS.CURRENCY][0],
                             right_on=curr_header[KEYS.CURRENCY],
                             how='left')

        offers_header.add_column(UKEYS.RATE_USD_TO_CURR, curr_header[UKEYS.RATE_USD_TO_CURR], UKEYS.RATE_USD_TO_CURR)

        # Make sure we have valid currency rates for all relevant entries
        mask = (offers[offers_header[UKEYS.RATE_USD_TO_CURR]].isna() == True)

        if mask.any():
            slogger.debug('Dropping the following offers:')
            slogger.debug(str(offers[mask]))

            # Specific warning on each line that is dropped
            message  = 'Dropping offer due to missing currency rate'
            category = STATUS_MESSAGE_CATEGORY.FILE

            def f(row):
                slogger.warning(message,
                                category=category,
                                location=int(row.name),
                                filetype=offers.filetype,
                                filename=offers.file.name)
            offers[mask].apply(func=f, axis=1)

            # Drop lines with error
            offers.data = offers[~mask]
            offers.trim()

        # Calculate prices in USD
        offers['price_usd'] = offers[offers_header[UKEYS.RAW_PRICE]]/offers[offers_header[UKEYS.RATE_USD_TO_CURR]]

        # Convert prices for price components
        for price_component in offers_header[KEYS.PRICE]:
            offers[price_component] = offers[price_component]/offers[offers_header[UKEYS.RATE_USD_TO_CURR]]

        del offers_header[UKEYS.RATE_USD_TO_CURR]
        offers.header = offers_header

        return offers


    def _normalize_duties(self, offers: TableData, duties: TableData) -> TableData:
        """
        This function normalizes the prices in the offers tables using duties rates
        present in the duties table.

        Args:
            offers (TableData): offers object
            duties (TableData): currencies object

        Return:
            offers (TableData): offers object with converted currencies
        """
        offers_header = offers.header
        duties_header = duties.header

        # Add duty rate for each offer from duties table
        offers.data = pd.merge(offers.data,
                               duties.data,
                               left_on=offers_header[[UKEYS.SUPPLIER_COUNTRY, UKEYS.CLIENT_COUNTRY]],
                               right_on=duties_header[[UKEYS.SUPPLIER_COUNTRY, UKEYS.CLIENT_COUNTRY]],
                               how='left')

        offers_header.add_column(UKEYS.DUTY_PERCENT, duties_header[UKEYS.DUTY_PERCENT], UKEYS.DUTY_PERCENT)
        # Make sure we have valid duty rates for all relevant entries
        mask = (offers[offers_header[UKEYS.DUTY_PERCENT]].isna() == True)

        if mask.any():
            slogger.debug('Dropping the following offers:')
            slogger.debug(str(offers[mask]))

            # Specific warning on each line that is dropped
            message  = 'Dropping offer due to missing duty rate'
            category = STATUS_MESSAGE_CATEGORY.FILE

            def f(row):
                slogger.warning(message,
                                category=category,
                                location=int(row.name),
                                filetype=offers.filetype,
                                filename=offers.file.name)
            offers[mask].apply(func=f, axis=1)

            # Drop lines with error
            offers.data = offers[~mask]
            offers.trim()

        # Calculate cost of duties
        offers['cost_duty'] = offers['price_usd']*offers[offers_header[UKEYS.DUTY_PERCENT]]

        # Add cost of duties
        offers[offers_header[UKEYS.NORMALIZED_PRICE]] += offers['cost_duty']

        offers.header = offers_header

        return offers


    def _add_time_value_money(self,
                              offers: TableData,
                              paymentnorm_day: int,
                              paymentnorm_perc: float,
                              interest_type: str) -> TableData:
        """
        This function normalizes the prices in the offers tables using duties rates
        present in the duties table.

        Args:
            offers (TableData): offers object
            paymentnorm_day (int): Payment norm based on which offers are normalized
            paymentnorm_perc (float): This is the interest rate per day used by
                the client for time value of money (TVM) purposes.
            interest_type (str): Used for TVM computations, currently we only
                support: 'linear', 'compound'.

        Return:
            offers (TableData): offers object with converted currencies
        """
        offers_header = offers.header
        # TODO: For now we use linear interest compensation where the following
        #       holds: FV = PV*(1+i*n), instead we should probably use a compound
        #       interest formula where FV = PV*(1+i)^n
        interest = paymentnorm_perc/100
        num_days = (paymentnorm_day - offers[offers_header[UKEYS.PAYMENT_DURATION]])/30

        # Linear interest
        if interest_type == 'linear':
            interest_rate = 1+(interest*num_days)
        # Compound interest
        elif interest_type == 'compound':
            interest_rate = (1+interest)**num_days
        # Unsuported interest type
        else:
            message = 'Unknown interest rate type {}'.format(interest_type)
            slogger.debug(message)
            raise RuntimeError

        offers[offers_header[UKEYS.NORMALIZED_PRICE]] *= interest_rate

        return offers


    def normalize_offers(self,
                         tables: Dict[str, TableData],
                         expression: str = '',
                         paymentnorm_day: int = 90,
                         paymentnorm_perc: float = 1,
                         interest_type: str = 'linear') -> TableData:
        """
        In order to compare all offers on equal grounds we normalize all of
        them for effects like shipping cost, currency exchange rates, payment
        time, etc.

        Args:
            tables (dict): Dictionary of all TableData objects contained in job
            expression (str): Expression string to normalize offer prices
            paymentnorm_day (int): Payment norm based on which offers are normalized
            paymentnorm_perc (float): This is the interest rate per day used by
                the client for time value of money (TVM) purposes.
            interest_type (str): Used for TVM computations, currently we only
                support: 'linear', 'compound'.

        Raises:
            RuntimeError: If unknown interest type is passed.
        """

        offers = tables['offers']
        header = offers.header
        expression_string = expression

        offers.header = header

        if expression_string != '':
            pass
            # expression_string = 'offers_'+UKEYS.RAW_PRICE+'*offers_'+KEYS.VOLUME+'+offers_'+UKEYS.PAYMENT_DURATION
            # TO DO: fill in with the expression handler!!

        else:
            #######################################################################
            # Convert to USD
            #######################################################################
            # initialize price in USD
            offers['price_usd'] = offers[header[UKEYS.RAW_PRICE]]

            if 'currencies' in tables:
                offers = self._convert_currencies(offers, tables['currencies'])

            #######################################################################
            # Determine cost of freight and duties
            #######################################################################
            # initialize normalized price
            offers[header[UKEYS.NORMALIZED_PRICE]] = offers['price_usd']

            if 'duties' in tables:
                offers = self._normalize_duties(offers, tables['duties'])


            #######################################################################
            # Time value of money computations
            #######################################################################
            if UKEYS.PAYMENT_DURATION in header.types:
                offers = self._add_time_value_money(offers, paymentnorm_day, paymentnorm_perc, interest_type)

        return offers


    def check_data_integrity(self,
                             tables: Dict[str, TableData],
                             has_discount: bool) -> bool:
        """
        This function should handle any additional error checking not done in
        the constructor.

        Args:
            tables (dict): Dictionary of all TableData objects contained in job
            has_discount (bool): Boolean if discounts are present in tables

        Returns:
            True if data integrity checks were successful

        Raises:
            RecordedError: If unknown discount bucket was referenced in offers.
        """
        offers = tables['offers']
        demands = tables['demands']

        offers_header = offers.header
        demands_header = demands.header

        # check for offers with no existing demand -> warning
        mask = ~offers[offers_header[UKEYS.ITEM_ID]].isin(demands[demands_header[UKEYS.ITEM_ID]])
        if any(mask):
            offers.issue_warnings(mask,
                                  'No related demands found for specific offer. Will be removed',
                                  offers_header[UKEYS.ITEM_ID],
                                  True)

        # check for demands with no existing offer -> warning
        mask = ~demands[demands_header[UKEYS.ITEM_ID]].isin(offers[offers_header[UKEYS.ITEM_ID]])
        if any(mask):
            demands.issue_warnings(mask,
                                   'No related offers found for specific demand',
                                   demands_header[UKEYS.ITEM_ID],
                                   False)

        if has_discount:
            discounts = tables['discounts']
            discounts_header = discounts.header
            # get available buckets from offers and discounts
            offer_buckets = set(offers[offers_header[UKEYS.CONTRIBUTING_TO]].sum() if offers[offers_header[UKEYS.CONTRIBUTING_TO]].sum() else []) | \
                set(offers[offers_header[UKEYS.DISCOUNTED_BY]].sum() if offers[offers_header[UKEYS.DISCOUNTED_BY]].sum() else [])
            discounts_buckets = set(discounts[discounts_header[UKEYS.BUCKET_ID]].values)

            # check for rebate buckets in offers with no existing discount -> error
            message  = 'No related rebate specifications found for specific offer'
            category = STATUS_MESSAGE_CATEGORY.FILE

            def f(row):
                missing_rebate = set(row[offers_header[UKEYS.CONTRIBUTING_TO]]).difference(discounts_buckets)
                if missing_rebate:
                    slogger.error(message, save_message=True, category=category, filetype=offers.filetype, filename=offers.file.name, location=str(row[offers_header[UKEYS.ITEM_ID]] + " " + str(missing_rebate)))
            offers.data.apply(func=f, axis=1)

            # check for discounts with no matching offer -> warning
            mask = ~discounts[discounts_header[UKEYS.BUCKET_ID]].isin(offer_buckets)
            if any(mask):
                discounts.issue_warnings(mask,
                                         'No related offers found for specific rebate. Will be removed.',
                                         discounts_header[UKEYS.BUCKET_ID],
                                         True)

            # check for suppliers in discounts that did not quote
            mask = ~discounts[discounts_header[UKEYS.SUPPLIER]].isin(offers[offers_header[UKEYS.SUPPLIER]])
            if any(mask):
                discounts.issue_warnings(mask,
                                         'No related offers found for specific supplier. Will be removed.',
                                         discounts_header[UKEYS.SUPPLIER],
                                         True)
        return True
