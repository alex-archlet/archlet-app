"""
Functions used in various jobs to auto-clean user data
"""

import pandas as pd

from shared.utilities.db_table_processor import uuid_name_to_bid_column_id

from shared.constants import KEYS, UKEYS
from shared.entities import TableData
from shared.communication import SingletonLogger

slogger = SingletonLogger()

def add_baseline_offers(offers: TableData, baseline: TableData) -> pd.DataFrame:
    """
    Adds baseline offers to offers table

    Args:
        offers: offers TableData object
        baseline: baseline TableData object

    Returns:
        combined pd Datafram object
    """
    baseline_offers = baseline.data
    baseline_header = baseline.header
    offers_header = offers.header

    baseline_offers[offers_header[UKEYS.OFFER_ID]] = baseline_offers[baseline_header[UKEYS.OFFER_ID]]
    baseline_offers[offers_header[UKEYS.DEMAND_ID]] = baseline_offers[baseline_header[UKEYS.DEMAND_ID]]
    all_offers = offers.data.append(baseline_offers, ignore_index=True, sort=False)

    return all_offers
