"""Module containing the file processing job class"""
from jobs import Job
from shared.communication import ExecuteTask
from shared.communication import SingletonLogger
from shared.constants import KEYS
from shared.constants import RecordedError
from shared.constants import UKEYS
from shared.entities import TableData
from shared.utilities.db_table_processor import join_offers_demands
from table_processors import Postprocessor
from table_processors import Preprocessor

slogger = SingletonLogger()

class FileProcessingJob(Job):
    """ File processing job class """

    def name(self):
        return "File Processing"

    @staticmethod
    def id():
        """
        Return the job type id
        """
        return 1

    def parse_input(self, job_input: dict) -> bool:
        self.extra_specifiers['translator'] = ''
        if 'files' not in job_input:
            raise RecordedError('no files provided to process')
        files = self.io_facade.fetch_job_files(job_input['files'])
        self.table_specifiers = files
        self.io_facade.update_job_progress(int(1/7*100))
        return False


    def load_tables(self) -> bool:
        self.io_facade.create_bidding_round_if_needed()
        for tablename, path in self.table_specifiers.items():
            tablename = tablename[:-6]
            self.tables[tablename] = TableData(None, tablename, None, path.name)
            self.tables[tablename] = self.io_facade.load_table(self.tables[tablename], path)
        try:
            offer_table = TableData()
            self.extras['offers_db'] = self.io_facade.load_offers_db(offer_table, self.io_facade.round_id)
        except RecordedError:
            self.extras['offers_db'] = None
        try:
            baseline_table = TableData()
            self.extras['baseline_db'] = self.io_facade.load_offers_db(baseline_table, self.io_facade.round_id, is_historic=True)
        except RecordedError:
            self.extras['baseline_db'] = None
        try:
            demand_table = TableData()
            self.extras['demands_db'] = self.io_facade.load_demands_db(demand_table, self.io_facade.round_id)
        except RecordedError:
            self.extras['demands_db'] = None
        self.io_facade.update_job_progress(int(3/7*100))

        return False


    def preprocess(self) -> bool:
        """
        Use a preprocessor object to prepare the data for this job
        """
        # Normalize offers

        preprocessor = Preprocessor()
        self.has_discounts = 'discounts' in self.tables

        with ExecuteTask('translating tables',
                         self.io_facade):
            self.tables = preprocessor.translate_tables(self.extras['translator'],
                                                        self.tables)

        self.io_facade.update_job_progress(int(4/7*100))

        if self.extras['demands_db'] is not None:
            with ExecuteTask('Using old demand ids',
                             self.io_facade):
                self.tables['demands'] = preprocessor.copy_ids(self.tables['demands'],
                                                               self.extras['demands_db'],
                                                               self.tables['demands'].header[UKEYS.ITEM_ID],
                                                               self.extras['demands_db'].header[UKEYS.ITEM_ID],
                                                               UKEYS.DEMAND_ID)
                self.tables['demands'] = preprocessor.copy_header(self.tables['demands'],
                                                                  self.extras['demands_db'])

        if self.extras['offers_db'] is not None:
            with ExecuteTask('Using old offer ids',
                             self.io_facade):
                self.extras['offers_db'].data = join_offers_demands(self.extras['offers_db'].data,
                                                                    self.extras['demands_db'].data,
                                                                    self.extras['offers_db'].header[UKEYS.DEMAND_ID],
                                                                    self.extras['demands_db'].header[UKEYS.DEMAND_ID])
                dest_list = [UKEYS.SUPPLIER]
                srce_id_list = [UKEYS.SUPPLIER]
                if UKEYS.ITEM_CATEGORY in self.tables['offers'].header.types:
                    dest_list.append(UKEYS.ITEM_CATEGORY)
                    srce_id_list.append(UKEYS.ITEM_CATEGORY)
                dest_list.append(UKEYS.ITEM_ID)
                dest_list = self.tables['offers'].header[dest_list]
                srce_id_list = self.extras['offers_db'].header[srce_id_list] + [self.extras['demands_db'].header[UKEYS.ITEM_ID]]
                self.tables['offers'] = preprocessor.copy_ids(self.tables['offers'],
                                                              self.extras['offers_db'],
                                                              dest_list,
                                                              srce_id_list,
                                                              UKEYS.OFFER_ID)
                self.tables['offers'] = preprocessor.copy_header(self.tables['offers'],
                                                                 self.extras['offers_db'])

        with ExecuteTask('adding demand ids to offers',
                         self.io_facade):
            self.tables['offers'] = preprocessor.add_demand_ids(self.tables['offers'],
                                                                self.tables['demands'])

        if self.has_discounts:
            with ExecuteTask('cleaning discounts table',
                             self.io_facade):
                discount_units = self.io_facade.get_discount_units()
                discount_types = self.io_facade.get_discount_types()
                self.tables['discounts'] = preprocessor.clean_discounts_table(self.tables['discounts'],
                                                                              discount_units,
                                                                              discount_types,
                                                                              self.tables['offers'].header)

        if 'capacities' in self.tables:
            with ExecuteTask('cleaning capacities table',
                             self.io_facade):
                self.tables['capacities'] = preprocessor.clean_capacities_table(self.tables['capacities'],
                                                                                self.tables['offers'])

        with ExecuteTask('checking data integrity',
                         self.io_facade):
            preprocessor.check_data_integrity(self.tables,
                                              self.has_discounts)

        with ExecuteTask('normalizing offers table',
                         self.io_facade):
            self.tables['offers'] = preprocessor.normalize_offers(self.tables,
                                                                  self.extras['translator'].expression)

        with ExecuteTask('setting default volume column',
                         self.io_facade):
            default_vol_col = preprocessor.get_default_volume(self.tables['offers'])

            if default_vol_col:
                self.tables['offers'] = preprocessor.set_default_volume(self.tables['offers'],
                                                                        default_vol_col)
                self.tables['demands'] = preprocessor.set_default_volume(self.tables['demands'],
                                                                        default_vol_col)

        with ExecuteTask('setting total price column',
                         self.io_facade):
            self.tables['offers'] = preprocessor.set_total_price(self.tables['offers'])

        with ExecuteTask('extracting baseline offers',
                         self.io_facade):
            baseline, self.tables['demands'] = preprocessor.extract_baseline(self.tables['demands'],
                                                                             self.tables['offers'].header)
            self.tables['baseline_offers'] = baseline

        if self.extras['baseline_db'] is not None:
            with ExecuteTask('Using old baseline ids',
                             self.io_facade):
                self.extras['baseline_db'].data = join_offers_demands(self.extras['baseline_db'].data,
                                                                      self.extras['demands_db'].data,
                                                                      self.extras['baseline_db'].header[UKEYS.DEMAND_ID],
                                                                      self.extras['demands_db'].header[UKEYS.DEMAND_ID])
                dest_list = [UKEYS.SUPPLIER]
                srce_id_list = [UKEYS.SUPPLIER]
                if UKEYS.ITEM_CATEGORY in self.tables['baseline_offers'].header.types:
                    dest_list.append(UKEYS.ITEM_CATEGORY)
                    srce_id_list.append(UKEYS.ITEM_CATEGORY)
                dest_list.append(UKEYS.ITEM_ID)
                dest_list = self.tables['baseline_offers'].header[dest_list]
                srce_id_list = self.extras['baseline_db'].header[srce_id_list] + [self.extras['demands_db'].header[UKEYS.ITEM_ID]]
                self.tables['baseline_offers'] = preprocessor.copy_ids(self.tables['baseline_offers'],
                                                                       self.extras['baseline_db'],
                                                                       dest_list,
                                                                       srce_id_list,
                                                                       UKEYS.OFFER_ID)
                self.tables['baseline_offers'] = preprocessor.copy_header(self.tables['baseline_offers'],
                                                                          self.extras['baseline_db'])

        with ExecuteTask('extracting project settings from translator',
                         self.io_facade):
            self.extras['project_settings'] = preprocessor.extract_project_settings(self.extras['translator'],
                                                                                    self.tables['demands'].header,
                                                                                    self.tables['offers'].header)

            self.extras['project_settings']['default_volume_column'] = default_vol_col

        with ExecuteTask('extracting custom kpis from translator',
                         self.io_facade):
            self.extras['custom_kpis'] = preprocessor.parse_custom_kpis(self.extras['translator'].custom_kpis,
                                                                        self.tables['demands'].header,
                                                                        self.tables['offers'].header,
                                                                        self.has_discounts)

        self.io_facade.update_job_progress(int(5/7*100))
        return False


    def save_results(self) -> bool:
        """Use the io_facade to save the results."""

        if self.io_facade.is_job_canceled():
            raise RecordedError("Aborting save results due to user interrupt.")

        with ExecuteTask('Storing demands in database',
                         self.io_facade):
            if self.extras['demands_db'] is not None:
                self.io_facade.update_demands_db(self.tables['demands'], self.extras['demands_db'])
                self.io_facade.clean_allocations_db()
            else:
                demands_mapping = self.io_facade.store_demands_db(self.tables['demands'])
                # Store the demands header separately
                header = self.tables['demands'].header
                self.io_facade.delete_table_header_db(self.io_facade.project_id, 'buyer')
                self.io_facade.dump_table_header_db(header, 'buyer')
            # don't store demands anymore in the file
            del self.tables['demands']

        with ExecuteTask('Storing baseline offers in database',
                         self.io_facade):
            # Store baseline offers if available
            if not self.tables['baseline_offers'].data.empty:
                if self.extras['baseline_db'] is not None:
                    self.io_facade.update_offers_db(self.tables['baseline_offers'],
                                                    self.extras['baseline_db'],
                                                    is_historic=True)
                else:
                    self.io_facade.store_baseline_offers_db(self.tables['baseline_offers'])

            # Make sure we don't store the baseline offers separately in a file
            del self.tables['baseline_offers']

        with ExecuteTask('Storing offers in database',
                         self.io_facade):
            # deleting potentially discounts as they reference to the item_id
            # TODO: remove that when we use cascading
            if self.has_discounts:
                self.io_facade.remove_discounts()

            # make sure there is no ITEM_ID in the header:
            table = self.tables['offers']
            header = table.header
            if UKEYS.ITEM_ID in header.types:
                del header[UKEYS.ITEM_ID]
                table.header = header
            if self.extras['offers_db'] is not None:
                self.io_facade.update_offers_db(table, self.extras['offers_db'])
            else:
                self.io_facade.store_offers_db(table)
                # Store the offers header
                self.io_facade.delete_table_header_db(self.io_facade.project_id, 'supplier')
                self.io_facade.dump_table_header_db(header, 'supplier')
            # don't store offers anymore in a file
            del self.tables['offers']

        if self.has_discounts:
            with ExecuteTask('Storing discounts in database',
                            self.io_facade):
                table = self.tables['discounts']
                self.io_facade.store_discounts(table)
                del self.tables['discounts']

        with ExecuteTask('Storing the project settings',
                         self.io_facade):
            self.io_facade.update_project_settings(self.extras['project_settings'])

        with ExecuteTask('Storing the custom kpis',
                         self.io_facade):
            self.io_facade.remove_project_kpi_defs()
            self.io_facade.store_kpi_defs(self.extras['custom_kpis'])

        # Store the remaining tables as files
        output = {'files': {}}
        postprocessor = Postprocessor()
        for tablename, table in self.tables.items():
            if table is None:
                continue
            table = postprocessor.process_table(table, tablename)
            shortname = tablename
            tablename = shortname + '_table'
            file_id = self.io_facade.dump_table(table,
                                             tablename,
                                             'processed_files/'+tablename+'s',
                                             shortname)
            output['files'][tablename] = file_id

        self.io_facade.add_job_output(output)

        self.io_facade.set_bidding_round_finished()

        self.io_facade.update_job_progress(int(6/7*100))

        return False
