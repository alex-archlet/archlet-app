"""
Parent class of any type of job
"""
from collections import defaultdict
from typing import Dict
from typing import List

from shared.communication import IOFacade
from shared.communication import StatusMessage
from shared.entities import Translator
from shared.entities import TableData
from table_processors import Preprocessor

EXTRA_NAME_TO_CLASS = {'translator':Translator}

class Job:
    """
    Parent class of any type of job
    """

    def __init__(self, io_facade: IOFacade) -> None:
        self.io_facade = io_facade
        self.translator = None
        self.table_specifiers: Dict[str, str] = {}
        self.db_table_specifiers: List[str] = []
        self.tables: Dict[str, TableData] = defaultdict(lambda: None)
        self.extra_specifiers: Dict[str, str] = {}
        self.extras: dict = {}
        self.messages: List[StatusMessage] = []
        self.output: dict = {}
        self.params = None


    def name(self):
        """
        Return the name of the instantiated job.
        """
        return "Abstract"

    @staticmethod
    def id():
        """
        Return the job type id
        """
        return 0

    def parse_input(self, job_input: dict) -> bool:
        """
        Get the relevant information from the job input json.
        """
        self.extra_specifiers['project_settings'] = ''
        if 'params' in job_input:
            self.params = job_input['params']
        if 'round_id' in job_input:
            self.extra_specifiers['round_id'] = job_input['round_id']
        else:
            self.extra_specifiers['round_id'] = self.io_facade.fetch_round_id()
        self.db_table_specifiers = ['demands', 'offers', 'baseline_offers']
        if 'files' in job_input and job_input['files']:
            files = self.io_facade.fetch_job_files(job_input['files'])
            for file_name, path in files.items():
                self.table_specifiers[file_name] = path
        self.io_facade.update_job_progress(int(1/7*100))
        return False


    def load_extras(self) -> bool:
        """
        Load all extras parameters needed for the job
        """
        # for name, path in extras_specifiers.items():
        #     self.extras[name] = EXTRA_NAME_TO_CLASS[name](self.io_facade.state)
        #     self.extras[name].load(path)
        if 'translator' in self.extra_specifiers.keys():
            self.extras['translator'] = self.io_facade.fetch_translator()
        if 'project_settings' in self.extra_specifiers.keys():
            self.extras['project_settings'] = self.io_facade.fetch_project_settings()
        self.io_facade.update_job_progress(int(2/7*100))
        return False


    def load_tables(self) -> bool:
        """
        Get the specified tables through the io_facade
        """
        for tablename in self.db_table_specifiers:
            loader = getattr(self.io_facade, f"load_{tablename}_db")
            self.tables[tablename] = TableData(None, None, tablename)
            self.tables[tablename] = loader(self.tables[tablename], self.extra_specifiers['round_id'])
        for tablename, db_file in self.table_specifiers.items():
            tablename = tablename[:-6]
            self.tables[tablename] = TableData()
            self.tables[tablename] = self.io_facade.load_table(self.tables[tablename], db_file)
        self.io_facade.update_job_progress(int(3/7*100))
        return False


    def preprocess(self) -> bool:
        """
        Use a preprocessor object to prepare the data for this job
        """
        preprocessor = Preprocessor()
        for tablename in self.tables.keys():
            self.tables[tablename] = preprocessor.process_table(self.tables[tablename], tablename)
        self.io_facade.update_job_progress(int(4/7*100))
        return False


    def run(self) -> bool:
        """Execute the main functionality for this job"""
        self.io_facade.update_job_progress(int(5/7*100))
        return False


    def save_results(self) -> bool:
        """Use the io_facade to save the results."""
        self.io_facade.add_job_output(self.output)
        self.io_facade.update_job_progress(int(6/7*100))


    def save_messages(self) -> bool:
        """Save the messages from this job with the io_facade"""
        output = self.io_facade.fetch_job_output()

        output['messages'] = self.io_facade.get_messages()
        output['num_messages'] = len(self.io_facade.get_messages())

        self.io_facade.add_job_output(output)
        self.io_facade.update_job_progress(int(100))
        return False
