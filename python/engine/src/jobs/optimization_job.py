"""Module containing the optimization job class"""

from shared.communication import ExecuteTask
from shared.communication import SingletonLogger
from shared.constants import CONSTRAINT_SPECS
from shared.constants import CONSTRAINT_TYPES
from shared.constants import UKEYS, KEYS
from shared.constants import RecordedError
from shared.entities import TableData
from shared.utilities.db_table_processor import add_item_ids
from shared.utilities.kpi_utils import prepare_kpi_definitions
from jobs import Job
from optimization import Optimizer
from table_processors import Filter
from table_processors import Postprocessor
from table_processors import Preprocessor

slogger = SingletonLogger()

class OptimizationJob(Job):
    """ optimization job class """

    def name(self):
        return "Optimization"

    @staticmethod
    def id():
        """
        Return the job type id
        """
        return 3

    def parse_input(self, job_input: dict) -> bool:
        super().parse_input(job_input)

        self.get_baseline_scenario_id()

    def load_extras(self):
        super().load_extras()

        if 'baseline_scenario' in self.extra_specifiers:
            scenario_id = self.extra_specifiers['baseline_scenario']
            baseline_allocations = TableData()
            baseline_allocations = self.io_facade.load_extended_allocations_db(baseline_allocations,
                                                                               scenario_id)
            self.extras['baseline_allocations'] = baseline_allocations

        self.io_facade.round_id = self.extra_specifiers['round_id']
        self.extras['custom_kpis'] = self.io_facade.fetch_custom_kpi_defs()
        self.extras['default_kpis'] = self.io_facade.fetch_default_kpi_defs()

    def load_tables(self) -> bool:
        super().load_tables()
        try:
            self.tables['previous_alloc'] = self.io_facade.load_allocations_db(TableData(None, None, 'previous_alloc'),
                                                                               self.io_facade.scenario_id)
        except RecordedError:
            self.tables['previous_alloc'] = None

        if 'rebates' in self.params['scenario_settings']['general_settings'] and self.params['scenario_settings']['general_settings']['rebates']:
            self.tables['discounts'] = self.io_facade.load_discounts(TableData())

    def preprocess(self) -> bool:
        super().preprocess()

        with ExecuteTask('checking missing tables',
                         self.io_facade):
            if 'demands' not in self.tables:
                slogger.error("No demands table found for optimization")
                raise RecordedError

        with ExecuteTask('parsing baseline',
                         self.io_facade):
            if 'baseline_allocations' in self.extras:
                preprocessor = Preprocessor()
                baseline = preprocessor.get_baseline_offers(self.extras['baseline_allocations'])
                self.tables['baseline_offers'] = baseline

        with ExecuteTask('filtering the data',
                         self.io_facade):
            job_filter = Filter(self.tables['offers'],
                                self.tables['baseline_offers'],
                                self.tables['demands'],
                                self.tables['discounts'])

            job_filter.run(self.params,
                           self.extras['project_settings'])
            self.tables['offers'] = job_filter.get_offers()
            self.tables['discounts'] = job_filter.get_discounts()
            self.tables['allocations'] = TableData()

        with ExecuteTask('updating capacities if present',
                         self.io_facade):
            if self.tables['capacities'] is not None:
                header = self.tables['offers'].header
                capacity_names = [header.byid[col_id].original_name for col_id in header[KEYS.CAPACITY]]
                self.tables['capacities'].header.set_type(header.byid[header[UKEYS.SUPPLIER]].original_name, UKEYS.SUPPLIER)
                self.tables['capacities'].header.set_type(capacity_names, KEYS.CAPACITY)
                self.tables['capacities'].add_uuid_names()

        with ExecuteTask('preprocessing kpi definitions',
                         self.io_facade):
            kpi_defs, sum_kpi_defs = prepare_kpi_definitions(self.extras['default_kpis'],
                                                             self.extras['custom_kpis'])
            self.extras['kpi_definitions'] = kpi_defs
            self.extras['summed_kpi_definitions'] = sum_kpi_defs
        self.io_facade.update_job_progress(int(4/7*100))


    def run(self) -> bool:
        optimizer = Optimizer(self.params['scenario_settings'],
                              self.tables['offers'],
                              self.tables['demands'],
                              self.tables['discounts'],
                              self.tables['capacities'],
                              self.tables['previous_alloc'],
                              self.extras['project_settings'],
                              self.tables['baseline_offers'],
                              self.io_facade.solver)

        optimizer.initialize_preprocessor()

        optimizer.initialize_model(self.extras['project_settings'].filters)

        postprocessor = Postprocessor()

        allocations = optimizer.run()
        allocations = postprocessor.run(allocations)

        kpis = postprocessor.compute_kpis(allocations,
                                          self.tables['baseline_offers'],
                                          self.extras['project_settings'].custom_kpis)

        self.tables['allocations'] = postprocessor.add_allocation_ids(allocations)

        self.extras['kpi_table'] = postprocessor.compute_kpi_table(self.tables['allocations'],
                                                                   self.tables['demands'],
                                                                   self.tables['offers'],
                                                                   self.tables['baseline_offers'],
                                                                   self.extras['kpi_definitions'])
        self.extras['summed_kpis'] = postprocessor.compute_summed_kpis(self.tables['allocations'],
                                                                       self.tables['demands'],
                                                                       self.tables['baseline_offers'],
                                                                       self.extras['summed_kpi_definitions'])

        self.output = {'kpis':kpis}

        allocations = postprocessor.handle_incomplete_allocation(allocations,
                                                                self.tables['demands'])

        self.optimizer = optimizer

        self.tables['allocations'] = allocations

        self.io_facade.update_job_progress(int(5/7*100))

    def save_results(self) -> bool:
        postprocessor = Postprocessor()

        allocations = postprocessor.process_table(self.tables['allocations'], 'allocations')

        if self.tables['capacities'] is not None:
            capacities_output = postprocessor.create_capacities_output(allocations,
                                                                       self.tables['capacities'],
                                                                       self.tables['offers'])
        else:
            capacities_output = {}

        general_settings = self.params['scenario_settings'][CONSTRAINT_TYPES.GENERAL_SETTINGS]
        rebates_on = CONSTRAINT_SPECS.REBATES in general_settings
        rebates_on = rebates_on and general_settings[CONSTRAINT_SPECS.REBATES]
        if self.tables['discounts'] is not None and rebates_on:
            discounts_output = self.optimizer.create_discounts_output(self.tables['discounts'])
        else:
            discounts_output = {}
        # TODO: uncomment previous lines when code is fixed!!!


        scenario_output = dict(capacities_output, **discounts_output)

        if self.io_facade.is_job_canceled():
            raise RecordedError("Aborting save results due to user interrupt.")

        self.io_facade.add_scenario_output(scenario_output)
        # First remove old allocations:
        self.io_facade.remove_allocations_db(self.io_facade.scenario_id)
        self.io_facade.store_allocations_db(allocations)

        self.io_facade.remove_scenario_kpis()
        self.io_facade.store_allocation_kpis(self.extras['kpi_table'])
        self.io_facade.store_summed_kpis(self.extras['summed_kpis'])
        self.io_facade.add_job_output(self.output)

        self.io_facade.update_job_progress(int(6/7*100))

    def save_messages(self) -> bool:
        super().save_messages()

        # check whether this optimization job succeeded and
        # update the invalidate and status fields accordingly
        if slogger.success():
            self.io_facade.update_scenario(invalidate=False, status="finished")
        else:
            self.io_facade.update_scenario(invalidate=True, status="invalid")

        self.io_facade.update_job_progress(int(100))


    def get_baseline_scenario_id(self) -> None:
        """
        Extract the scenario id from the scenario parameters if present.
        """
        settings = self.params['scenario_settings']
        if CONSTRAINT_TYPES.GENERAL_SETTINGS not in settings:
            return None
        general_settings = settings[CONSTRAINT_TYPES.GENERAL_SETTINGS]
        if CONSTRAINT_SPECS.BASELINE not in general_settings:
            return None
        if len(general_settings[CONSTRAINT_SPECS.BASELINE]) > 0:
            self.extra_specifiers['baseline_scenario'] = general_settings[CONSTRAINT_SPECS.BASELINE][0]
        return None
