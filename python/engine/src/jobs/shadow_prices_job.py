"""Module containing the shadow prices job class"""

from jobs import Job
from optimization.dual_price_function import DualPriceFunction

class ShadowPricesJob(Job):
    """Module containing the shadow prices job class"""

    def name(self):
        return "Shadow Prices"

    def run(self) -> bool:
        """Execute the main functionality for this job"""
        dual_price_function = DualPriceFunction()
        self.output = dual_price_function.run()

        self.io_facade.update_job_progress(int(5/7*100))

        return 0
