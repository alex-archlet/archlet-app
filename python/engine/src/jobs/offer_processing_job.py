""" Module containing the offer processing job """
from shared.communication import ExecuteTask
from shared.communication import SingletonLogger
from shared.constants import UKEYS, KEYS
from shared.constants import RecordedError
from shared.entities import TableData
from shared.entities import TableHeader
from shared.utilities.db_table_processor import join_offers_demands
from jobs import Job
from table_processors.offer_processing_utils import *
from table_processors.preprocessor import Preprocessor

slogger = SingletonLogger()

class OfferProcessingJob(Job):
    """
    Offer processing job class
    """

    def name(self):
        return "Offer Processing"

    @staticmethod
    def id():
        """
        Return the job type id
        """
        return 9

    def parse_input(self, job_input):
        """ Get all the offer files for the given round id"""
        self.extra_specifiers['round_id'] = job_input['round_id']

        self.io_facade.round_id = self.extra_specifiers['round_id']

        files = self.io_facade.fetch_additional_offer_files(job_input['round_id'])
        for i, file in enumerate(files):
            # Append '_table' so we can use the normal table loading step from the parent class
            self.table_specifiers['offers_'+str(i)+'_table'] = file

        self.io_facade.update_job_progress(int(1/7*100))

    def load_extras(self):
        """
        Load the table header for the offers of this round_id
        """
        offers_header = TableHeader()
        header = self.io_facade.load_table_header_db(table_header=offers_header,
                                                     project_id=self.io_facade.project_id,
                                                     input_by='supplier')

        for col in header.columns:
            col.attribute = True
        self.extras['offers_header'] = header
        self.io_facade.update_job_progress(int(2/7*100))

    def load_tables(self):
        """
        Load offers and demands
        """ 
        table = TableData()
        self.tables['offers'] = [self.io_facade.load_table(TableData(), file) for file in self.table_specifiers.values()]
        

        table = TableData()
        self.tables['demands'] = self.io_facade.load_demands_db(table,
                                                                self.io_facade.round_id)

        table = TableData()

        try:
            self.tables['offers_db'] = self.io_facade.load_offers_db(table,
                                                                     self.io_facade.round_id)
        except RecordedError:
            self.tables['offers_db'] = None

        self.io_facade.update_job_progress(int(3/7*100))

    def preprocess(self):
        """
        Consolidate all the offer tables into one offer table.
        The result is one offers table in self.tables['offers']
        """
        # Load the table headers from the matching field
        with ExecuteTask('parsing table headers of offer files',
                         self.io_facade):
            for table in self.tables['offers']:
                matching = table.file.matchings
                for dic in matching:
                    original_name = self.extras['offers_header'].byid[dic['bid_column']].original_name
                    # original_name = self.extras['offers_header'].get_dic_from_id(dic['bid_column'])['name']
                    if dic['excel_column'] != original_name:
                        table.data.rename(columns={dic['excel_column']: original_name}, inplace=True)
            
        with ExecuteTask('excluding buyer columns',
                         self.io_facade):
            demands_header = self.tables['demands'].header
            item_key = demands_header.byid[demands_header[UKEYS.ITEM_ID]].original_name
            self.tables['offers'] = delete_buyer_columns(self.tables['offers'], self.extras['offers_header'], item_key)

        with ExecuteTask('consolidating all offer files',
                         self.io_facade):
            self.tables['offers'] = concatenate_offers(self.extras['offers_header'], self.tables['offers'], self.table_specifiers.values())

        if self.tables['offers_db'] is not None:
            with ExecuteTask('Using old offer ids',
                             self.io_facade):
                joined_offers = join_offers_demands(self.tables['offers_db'].data,
                                                                    self.tables['demands'].data,
                                                                    self.tables['offers_db'].header[UKEYS.DEMAND_ID],
                                                                    self.tables['demands'].header[UKEYS.DEMAND_ID])
                joined_table = TableData()
                joined_table.data = joined_offers
                joined_table.header = self.tables['offers_db'].header
                dest_list = [UKEYS.SUPPLIER]
                srce_id_list = [UKEYS.SUPPLIER]
                if UKEYS.ITEM_CATEGORY in self.tables['offers'].header.types:
                    dest_list.append(UKEYS.ITEM_CATEGORY)
                    srce_id_list.append(UKEYS.ITEM_CATEGORY)
                dest_list.append(UKEYS.ITEM_ID)
                dest_list = self.tables['offers'].header[dest_list]
                srce_id_list = joined_table.header[srce_id_list] + [self.tables['demands'].header[UKEYS.ITEM_ID]]
                self.tables['offers'].header.add_column(UKEYS.OFFER_ID, UKEYS.OFFER_ID, '')
                self.tables['offers'] = Preprocessor.copy_ids(self.tables['offers'],
                                                              joined_table,
                                                              dest_list,
                                                              srce_id_list,
                                                              UKEYS.OFFER_ID)
                self.tables['offers'] = Preprocessor.copy_header(self.tables['offers'],
                                                                 self.tables['offers_db'])

        self.io_facade.update_job_progress(int(4/7*100))

    def run(self):
        """
        Check the offers table for errors in the data.
        Check for each offer whether it has an existing demand.
        Documentation on the format of the errors and warnings can be found
        in Confluence on the page of the job.
        """

        with ExecuteTask('deleting offers without demand and empty offers',
                         self.io_facade):
            self.tables['offers'] = delete_empty_offers(self.tables['offers'])
            self.tables['offers'] = delete_offers_no_demands(self.tables['offers'], self.tables['demands'])
            self.tables['offers'] = delete_duplicated_offers(self.tables['offers'])

        with ExecuteTask('finding errors in offers',
                         self.io_facade):
            self.errors = []
            self.errors.append(('OFFER_PROCESSING_ERROR_TYPE', check_types(self.tables['offers'])))

        with ExecuteTask('finding warnings in offers',
                         self.io_facade):
            self.warnings = []
            self.warnings.append(('OFFER_PROCESSING_WARNING_EMPTY', find_empty_cells(self.tables['offers'])))
            self.warnings.append(('OFFER_PROCESSING_WARNING_ZERO', find_zero_cells(self.tables['offers'])))
            self.warnings.append(('OFFER_PROCESSING_WARNING_OUTLIER', find_outlier_cells(self.tables['offers'])))

        with ExecuteTask('adding demand ids',
                         self.io_facade):
            demands_mapping = self.io_facade.get_demands_mapping(self.tables['demands'])
            offers_table = self.tables['offers']
            header = offers_table.header
            header.add_column(UKEYS.DEMAND_ID, UKEYS.DEMAND_ID, '')
            offers_table[header[UKEYS.DEMAND_ID]] = offers_table[header[UKEYS.ITEM_ID]].replace(demands_mapping)
            self.tables['offers'] = offers_table

        with ExecuteTask('computing change set of offers',
                         self.io_facade):
            if self.tables['offers_db'] is not None:
                self.changes = compute_changes(self.tables['offers'], self.tables['offers_db'])
            else:
                self.changes = None

        with ExecuteTask('extracting basic project settings',
                         self.io_facade):
            settings = {}
            filters = self.tables['demands'].header[KEYS.REGION]
            # if there are no locations, use item_id to have one filter
            if not filters:
                filters = [self.tables['demands'].header[UKEYS.ITEM_ID]]

            settings['filters'] = filters
            self.extras['project_settings'] = settings

        self.io_facade.update_job_progress(int(5/7*100))

    def save_results(self):
        """
        Save the offers table to the database.
        """
        if self.io_facade.is_job_canceled():
            raise RecordedError("Aborting save results due to user interrupt.")

        with ExecuteTask('Storing the offers',
                         self.io_facade):
            self.tables['offers'] = create_messages(self.errors,
                                                    self.warnings,
                                                    self.changes,
                                                    self.tables['offers'],
                                                    self.extras['offers_header'])
            table = self.tables['offers']
            header = table.header
            if UKEYS.ITEM_ID in header.types:
                del header[UKEYS.ITEM_ID]
                table.header = header
            if self.tables['offers_db'] is not None:
                self.io_facade.update_offers_db(table, self.tables['offers_db'])
            else:
                self.io_facade.store_offers_db(table)
            

        with ExecuteTask('Storing the project settings',
                         self.io_facade):
            self.io_facade.update_project_settings(self.extras['project_settings'])
        self.io_facade.update_job_progress(int(6/7*100))
