"""
File containing the definition of the ExpressionEvaluationJob
"""

import re

import pandas as pd

from typing import Dict
from typing import List
from typing import Tuple

from shared.communication import SingletonLogger
from shared.communication.execute_task import ExecuteTask
from shared.constants import DATA
from shared.constants import KEYS
from shared.constants import RecordedError
from shared.constants import UKEYS
from shared.entities import TableData
from shared.entities import TableHeader
from shared.utilities.cycle_check import reverse_topo_sort
from shared.utilities.db_table_processor import clean_numeric_cols
from shared.utilities.db_table_processor import create_renamers
from shared.utilities.db_table_processor import join_offers_demands
from shared.utilities.db_table_processor import uuid_name_to_bid_column_id
from shared.utilities.expression_handler import ExpressionHandler
from shared.utilities.price_component_utils import create_mapping_prices_volumes
from shared.utilities.price_component_utils import expressions_to_handlers
from shared.utilities.price_component_utils import extract_price_relations
from shared.utilities.price_component_utils import price_structure_from_adj_list
from shared.utilities.price_component_utils import translate_calculations
from shared.utilities.price_component_utils import uuids_to_column_names

from table_processors.expression_evaluation_utils import add_baseline_offers

from jobs import Job

slogger = SingletonLogger()

class ExpressionEvaluationJob(Job):
    """
    Compute the result of the expressions given in the bid columns, and save it to the offers
    """

    def name(self):
        return "Expression Evaluation"

    @staticmethod
    def id():
        """
        Return the job type id
        """
        return 7

    def parse_input(self, job_input):
        """
        Get the round id from job input
        """
        self.io_facade.round_id = job_input['round_id']
        self.extra_specifiers['project_settings'] = ''

        self.io_facade.update_job_progress(int(1/7*100))

    def load_tables(self):
        """
        Load offers and demands
        """
        table = TableData()
        self.tables['offers'] = self.io_facade.load_offers_db(table=table,
                                                              round_id=self.io_facade.round_id)

        table = TableData()
        self.tables['baseline_offers'] = self.io_facade.load_baseline_offers_db(table=table,
                                                                                round_id=self.io_facade.round_id)

        table = TableData()
        self.tables['demands'] = self.io_facade.load_demands_db(table,
                                                                self.io_facade.round_id)

        self.io_facade.update_job_progress(int(3/7*100))

    def preprocess(self):
        """
        Change the column names to uuids, create the mapping from uuid to column type (volume or price), and check that the values in
        these price/volume columns are all numeric
        """
        # Get headers
        offers_header = self.tables['offers'].header
        demands_header = self.tables['demands'].header

        # Get renamers
        renamer, expression_renamer = create_renamers(self.tables['offers'])
        renamer_update, expression_renamer_update = create_renamers(self.tables['demands'])
        renamer.update(renamer_update)
        expression_renamer.update(expression_renamer_update)

        # Create price and volume mappings
        mapping_name_to_type = create_mapping_prices_volumes(self.tables['offers'],
                                                             self.tables['demands'],
                                                             renamer)

        # add baseline offers if there are any
        if self.tables['baseline_offers'] is not None:
            all_offers = add_baseline_offers(self.tables['offers'], self.tables['baseline_offers'])
        else:
            all_offers = self.tables['offers'].data

        # combine offers and demands
        left_on = offers_header[UKEYS.DEMAND_ID]
        right_on = demands_header[UKEYS.DEMAND_ID]
        combined = join_offers_demands(all_offers,
                                       self.tables['demands'].data,
                                       left_on,
                                       right_on)

        table = TableData()
        table.data = combined
        # TODO: check if there exists a function to combine headers
        table.header = offers_header
        table.data.rename(columns=renamer, inplace=True)
        table.trim()

        cols_to_compute = {renamer[col.id]: {"name": col.original_name, "calculation": col.calculation} for col in offers_header.columns if col.calculation is not None}
        # clean table data
        cols_to_clean = set(mapping_name_to_type.keys()) - set(cols_to_compute.keys())
        table = clean_numeric_cols(table, cols_to_clean, generate_warning=True)

        self.extras['mapping_name_to_type'] = mapping_name_to_type
        self.extras['cols_to_compute'] = cols_to_compute
        self.extras['expression_renamer'] = expression_renamer
        self.extras['renamer'] = renamer
        self.tables['table'] = table
        self.io_facade.update_job_progress(int(4/7*100))


    def run(self):
        """
        For each bid column belonging to the offers table that has an expression in the calculation
        field, compute this expression and create the resulting column
        """
        cols_to_compute = {key: value["calculation"] for key, value in self.extras['cols_to_compute'].items()}
        cols_to_compute = translate_calculations(cols_to_compute, self.extras['expression_renamer'])
        translated_expressions = expressions_to_handlers(cols_to_compute, self.extras['mapping_name_to_type'])
        price_relations = extract_price_relations(translated_expressions)
        ordering = reverse_topo_sort(price_relations)
        
        if (len(ordering) > 1) and (ordering[0] == ordering[-1]):
            raise RecordedError(f"The cost calculations contain a cyclic dependency: {ordering[:-1]}"
                                f"Please revise the definitions of the cost calculations")

        # calculate the prices from the expressions:
        table = self.tables['table']
        for col in ordering:
            if self.extras['cols_to_compute'][col]["name"] == "Total Cost":
                # substitute all other expressions :
                total_cost_calc = cols_to_compute[col]
                for col2 in reversed(ordering):
                    if cols_to_compute[col2] is None or cols_to_compute[col2] == '':
                        continue
                    total_cost_calc = total_cost_calc.replace(col2, f" ( {cols_to_compute[col2]} ) ")
                # convert calculation into expression
                total_cost_calc = 'y = ' + total_cost_calc
                expr_handler = ExpressionHandler('table', total_cost_calc, self.extras['mapping_name_to_type'])
                slogger.verify(expr_handler.is_assignment(), \
                        "The expression is not an assignment")
                slogger.verify(expr_handler.has_valid_attribute_names(), \
                        "The expression has invalid attribute names")
                expr_handler.decompose_price_expression()
                table = self.set_total_price(table, expr_handler)
            else:
                expr_handler = translated_expressions[col]
                expr_handler.decompose_price_expression()
                table = self.set_price_component(table, expr_handler, col)

        #create price components structure:
        offers_header = self.tables['offers'].header
        renamer = self.extras['renamer']
        all_prices = [renamer[col_id] for col_id in offers_header[KEYS.PRICE]] + [renamer[offers_header[UKEYS.TOTAL_PRICE]]]
        missing_prices = {price: [] for price in all_prices if price not in price_relations}
        price_relations.update(missing_prices)
        reverse_renamer = {value: key for key, value in self.extras['renamer'].items()}
        translated_relations = uuids_to_column_names(price_relations, self.tables['offers'].header, reverse_renamer)
        result = price_structure_from_adj_list(translated_relations)
        for comp in result:
            if comp['name'] == 'Total Cost':
                comp['name'] = 'Total Price'
        self.extras['price_comp_structure'] = result
        self.tables['table'] = table

        self.io_facade.update_job_progress(int(5/7*100))


    def save_results(self):
        """
        Save offers table in DB
        """
        if self.io_facade.is_job_canceled():
            raise RecordedError("Aborting save results due to user interrupt.")
        offers = self.tables['offers']
        header = offers.header
        table = self.tables['table']
        renamer = self.extras['renamer']
        table.data.set_index(renamer[header[UKEYS.OFFER_ID]], inplace=True)
        # store new offers
        with ExecuteTask('Storing the offers',
                         self.io_facade):
            offers = self.copy_computed_cols(table, offers, self.extras['cols_to_compute'])
            offers.header.add_column(UKEYS.DB_STATUS)
            offers[offers.header[UKEYS.DB_STATUS]] = DATA.PRESENT
            self.io_facade.update_offers_db(offers, offers, is_historic=False)

        if self.tables['baseline_offers'] is not None:
            with ExecuteTask('Storing the baseline offers',
                             self.io_facade):
                baseline = self.tables['baseline_offers']
                baseline = self.copy_computed_cols(table, baseline, self.extras['cols_to_compute'])
                baseline.header.add_column(UKEYS.DB_STATUS)
                baseline[baseline.header[UKEYS.DB_STATUS]] = DATA.PRESENT
                self.io_facade.update_offers_db(baseline, baseline, is_historic=True)

        with ExecuteTask('Storing the price component structure',
                         self.io_facade):
            settings = self.extras['project_settings']
            settings.price_components_structure = self.extras['price_comp_structure']
            self.io_facade.update_project_settings(settings.to_dict())

        with ExecuteTask('Storing the project update time',
                         self.io_facade):
            self.io_facade.update_project_time()

        self.io_facade.update_job_progress(int(6/7*100))


    @staticmethod
    def set_total_price(table: TableData, expr_handler: ExpressionHandler):
        """
        Handles the expression of the total price, by setting the total_volume, total_var_price,
        total_fixed_price and price_total to the offers table
        """
        vol_calculator = expr_handler.get_total_volume_calculator("total_vol")
        var_price_calculator = expr_handler.get_normalized_price_calculator("avg_price")
        fixed_price_calculator = expr_handler.get_fixed_price_calculator("fixed_price")

        dictionary_locals = {'table': table}

        exec(vol_calculator, dictionary_locals)
        exec(var_price_calculator, dictionary_locals)
        exec(fixed_price_calculator, dictionary_locals)

        table['total_volume'] = dictionary_locals['total_vol']
        table['total_var_price'] = dictionary_locals['avg_price']
        table['total_fixed_price'] = dictionary_locals['fixed_price']

        table['Total Cost'] = table['total_volume']*table['total_var_price'] + table['total_fixed_price']
        return table

    @staticmethod
    def set_price_component(table: TableData, expr_handler: ExpressionHandler, price_comp: str):
        """
        Handles the expression of the total price, by only setting the final result e.g. the
        price component values
        """
        original_calculator = expr_handler.get_original_calculator()

        dictionary_locals = {'table': table}

        exec(original_calculator, dictionary_locals)

        table[price_comp] = dictionary_locals['y']

        return table

    @staticmethod
    def copy_computed_cols(table: TableData, destination: TableData, cols_to_compute: Dict[str, Dict[str, str]]) -> TableData:
        header = destination.header
        destination.data.set_index(header[UKEYS.OFFER_ID], inplace=True)
        added_total_price = False
        for col, dct in cols_to_compute.items():
            if dct["name"] == "Total Cost":
                destination[header.byname["Total Cost"].id] = table["Total Cost"]
                dest_cols = header[[UKEYS.TOTAL_VOLUME, UKEYS.NORMALIZED_PRICE, UKEYS.TOTAL_PRICE]]
                table_cols = ['total_volume', 'total_var_price', 'total_fixed_price']
                destination[dest_cols] = table[table_cols]
                added_total_price = True
            else:
                destination[header.byname[dct["name"]].id] = table[col]

        if not added_total_price:
            slogger.error("no total cost was calculated")
            
        destination.data.reset_index(inplace=True)
        return destination
