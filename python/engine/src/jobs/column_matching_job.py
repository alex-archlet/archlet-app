""" Module containing the column matching job class """
from jobs import Job
from shared.constants import RecordedError
from shared.entities import TableData
from shared.entities import TableHeader
from table_processors import ColumnMatcher


class ColumnMatchingJob(Job):
    """
    Column matching job class.

    Compute the similarity between the existing column types and the uploaded columns.
    """

    def name(self):
        return "Column Matching"

    @staticmethod
    def id():
        """
        Return the job type id
        """
        return 6

    def parse_input(self, job_input):
        # Get the entry from the optimization files table for the file to match
        self.table_specifiers['naked_table'] = self.io_facade.fetch_job_file(job_input['file_id'])
        self.extra_specifiers['round_id'] = job_input['round_id']
        self.io_facade.update_job_progress(int(1/7*100))

    def load_extras(self):
        table_header = TableHeader()
        try:
            # Try to load the bid columns for the given bidding round
            # Only load the columns that should be input by supplier
            self.io_facade.load_table_header_db(table_header, self.io_facade.project_id, 'supplier')
            self.extras['table_header'] = table_header
        except RecordedError:
            # When there are no bid columns present, this job will store a set
            # of bid columns based on the column headers of the uploaded file
            self.extras['table_header'] = None

        self.io_facade.update_job_progress(int(2/7*100))

    def load_tables(self):
        naked_table = TableData()
        naked_table = self.io_facade.load_table(naked_table,
                                                self.table_specifiers['naked_table'])
        self.tables['source_row'] = self.io_facade.load_source_row(self.table_specifiers['naked_table'])
        self.tables['naked_table'] = naked_table
        self.io_facade.update_job_progress(int(3/7*100))

    def preprocess(self):
        self.io_facade.update_job_progress(int(4/7*100))

    def run(self):
        column_matcher = ColumnMatcher()
        # fetch the column header or make one if there is none present
        header = self.extras['table_header']
        if header is None:
            # get model and column types
            path = 'model/distiluse-base-multilingual-cased'
            model_path = self.io_facade.fs_manager.absolute_path(path)
            column_types = self.io_facade.load_column_types()
            header = column_matcher.create_template_from_table(self.tables['naked_table'],
                                                               self.tables['source_row'],
                                                               model_path,
                                                               column_types)
            self.extras['table_header'] = header
        else:
            # Set column_header to none so that we don't overwrite the
            # stored column definitions
            self.extras['table_header'] = None
        matching = column_matcher.match_columns(self.tables['naked_table'], header)
        excel_column_previews = column_matcher.get_excel_column_previews(self.tables['naked_table'])
        self.extras['matching_output'] = {'excel_columns': list(self.tables['naked_table'].data.keys()),
                                          'excel_values': excel_column_previews,
                                          'matchings': matching}

        self.io_facade.update_job_progress(int(5/7*100))


    def save_results(self):
        if self.io_facade.is_job_canceled():
            raise RecordedError("Aborting save results due to user interrupt.")
        if self.extras['table_header'] is not None:
            self.io_facade.dump_table_header_db(self.extras['table_header'], '')

        self.io_facade.add_matching_to_file(self.table_specifiers['naked_table'].id, self.extras['matching_output'])
        self.io_facade.update_job_progress(int(6/7*100))
