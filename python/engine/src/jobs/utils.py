""" Utility functions """

from shared.constants import STATUS_MESSAGE_CATEGORY
from shared.communication.io_facade import IOFacade
import pandas as pd
import os
import importlib
from typing import Optional
from shared.entities.table_data import TableData

# def check_matching(series1: pd.Series,
#                    series2: pd.Series,
#                    iofacade: IOFacade,
#                    message: str,
#                    table: pd.DataFrame,
#                    key: str) -> bool:
#     """
#     check if columns from different tables match otherwise issue a warning
#     """
#     mask = ~series1.isin(series2)
#     if any(mask):
#         category = STATUS_MESSAGE_CATEGORY.FILE

def camel(snake_str):
    words = snake_str.split('_')
    return ''.join(map(str.title, words))

def get_job_mapping():
    """
    Parse the job folder to create a mapping from
    job type id to job class
    """

    mapping = {}

    files = os.listdir(os.path.dirname(__file__))
    files = [filename for filename in files if filename.endswith('job.py')]
    classes = [camel(filename.split('.')[0]) for filename in files]

    for filename, classname in zip(files, classes):
        file_loaded = importlib.import_module('jobs.' + filename.split('.')[0])
        class_loaded = getattr(file_loaded, classname)

        mapping[class_loaded.id()] = class_loaded

    return mapping
