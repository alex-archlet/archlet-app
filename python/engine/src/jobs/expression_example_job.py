"""
TODO
"""

import re

import pandas as pd

from shared.communication import SingletonLogger
from shared.constants import KEYS
from shared.constants import RecordedError
from shared.constants import UKEYS
from shared.entities import TableData
from shared.utilities.cycle_check import reverse_topo_sort
from shared.utilities.db_table_processor import clean_numeric_cols
from shared.utilities.db_table_processor import create_renamers
from shared.utilities.db_table_processor import join_offers_demands
from shared.utilities.expression_handler import ExpressionHandler
from shared.utilities.price_component_utils import create_mapping_prices_volumes
from shared.utilities.price_component_utils import expressions_to_handlers
from shared.utilities.price_component_utils import extract_price_relations
from shared.utilities.price_component_utils import translate_calculations

from jobs import Job

slogger = SingletonLogger()

class ExpressionExampleJob(Job):
    """
    Compute the result of the given expression,
    and save it in the database for the frontend
    """

    def name(self):
        return "Expression Example"

    @staticmethod
    def id():
        """
        Return the job type id
        """
        return 8

    def parse_input(self, job_input):
        """
        Get the round id from job input
        """
        self.io_facade.round_id = job_input['round_id']
        self.extras['expression'] = job_input['expression']
        self.io_facade.update_job_progress(int(1/7*100))

    def load_tables(self):
        """
        Load offers and demands
        """
        table = TableData()
        self.tables['offers'] = self.io_facade.load_offers_db(table,
                                                              self.io_facade.round_id)

        table = TableData()
        self.tables['demands'] = self.io_facade.load_demands_db(table,
                                                                self.io_facade.round_id)
        self.io_facade.update_job_progress(int(3/7*100))

    def preprocess(self):
        """
        Change the column names to uuids, create the mapping from uuid to column type
        (volume or price), and check that the values in these price/volume columns
        are all numeric.
        """
        renamer, expression_renamer = create_renamers(self.tables['offers'])
        renamer_update, expression_renamer_update = create_renamers(self.tables['demands'])
        renamer.update(renamer_update)
        expression_renamer.update(expression_renamer_update)

        offers_header = self.tables['offers'].header
        demands_header = self.tables['demands'].header

        left_on = offers_header[UKEYS.DEMAND_ID]
        right_on = demands_header[UKEYS.DEMAND_ID]
        combined = join_offers_demands(self.tables['offers'].data,
                                       self.tables['demands'].data,
                                       left_on,
                                       right_on)

        table = TableData()
        table.data = combined
        table.data.rename(columns=renamer, inplace=True)
        table.trim()

        cols_to_compute = {renamer[col.id]: {"name": col.original_name, "calculation": col.calculation} for col in offers_header.columns if col.calculation is not None}
        # Create price and volume mappings
        mapping_name_to_type = create_mapping_prices_volumes(self.tables['offers'],
                                                             self.tables['demands'],
                                                             renamer)
        # clean table data
        cols_to_clean = set(mapping_name_to_type.keys()) - set(cols_to_compute.keys())
        table = clean_numeric_cols(table, cols_to_clean)


        self.extras['mapping_name_to_type'] = mapping_name_to_type
        self.extras['cols_to_compute'] = cols_to_compute
        self.extras['expression_renamer'] = expression_renamer
        self.extras['renamer'] = renamer
        self.tables['table'] = table
        self.io_facade.update_job_progress(int(4/7*100))

    def run(self):
        """
        Compute the expression given in the job input for every row of the offers
        """
        mapping_name_to_type = self.extras['mapping_name_to_type']
        # Check for cycles in the loaded calculations:
        cols_to_compute = {key: value["calculation"] for key, value in self.extras['cols_to_compute'].items()}
        cols_to_compute = translate_calculations(cols_to_compute, self.extras['expression_renamer'])
        translated_expressions = expressions_to_handlers(cols_to_compute, mapping_name_to_type)
        price_relations = extract_price_relations(translated_expressions)
        ordering = reverse_topo_sort(price_relations)
        
        if (len(ordering) > 1) and (ordering[0] == ordering[-1]):
            raise RecordedError(f"The cost calculations contain a cyclic dependency: {ordering[:-1]}"
                                f"Please revise the definitions of the cost calculations")

        # calculate the prices from the expressions:
        table = self.tables['table']
        for col in ordering:
            expr_handler = translated_expressions[col]
            table = self.compute_value(table, expr_handler, col)

        expression = self.extras['expression']
        for key, value in self.extras['expression_renamer'].items():
            expression = expression.replace(key, value)

        expression = 'y = ' + expression

        expr_handler = ExpressionHandler('table', expression, mapping_name_to_type)

        slogger.verify(expr_handler.is_assignment(), \
             "The expression is not an assignment")
        slogger.verify(expr_handler.has_valid_attribute_names(), \
             "The expression has invalid attribute names")

        table = self.compute_value(table, expr_handler, 'Result')
        self.tables['table'] = table
        self.extras['expression'] = expression

        self.io_facade.update_job_progress(int(5/7*100))

    def save_results(self):
        """
        Save the first rows of the offers table with only the rows being used in the expression,
        and the result
        """
        expression = self.extras['expression']
        table = self.tables['table']
        offers_header = self.tables['offers'].header
        demands_header = self.tables['demands'].header

        item_col = self.extras['renamer'][demands_header[UKEYS.ITEM_ID]]
        # We get rid of the parentheses and spaces to only get a list of the uuids
        cols_to_show = ['Result', item_col] + re.sub(' . ', ',', expression.replace('(', '').replace(')', '')).replace(' ', '').split(',')[1:]

        # Don't show constants in table
        cols_to_show = [x for x in cols_to_show if x in table.data.columns]

        table.data = table[cols_to_show]
        reverse_renamer = {}
        for key, value in self.extras['renamer'].items():
            if key in offers_header.ids:
                reverse_renamer[value] = offers_header.byid[key].original_name
            else:
                reverse_renamer[value] = demands_header.byid[key].original_name

        table.data.rename(columns=reverse_renamer, inplace=True)

        preview_df = table.data.head().round(2)

        dict_df = preview_df.to_dict('records')
        output = {
            'calculated_examples': dict_df,
            'header_calculated_examples': list(preview_df.columns)
        }
        
        if self.io_facade.is_job_canceled():
            raise RecordedError("Aborting save results due to user interrupt.")

        self.io_facade.add_job_output(output)

        self.io_facade.update_job_progress(int(6/7*100))

    @staticmethod
    def compute_value(table: TableData, expr_handler: ExpressionHandler, price_comp: str):
        """
        Compute the column that results of the expression given and add it to the offers
        """
        original_calculator = expr_handler.get_original_calculator()

        dictionary_locals = {'table': table}

        exec(original_calculator, dictionary_locals)

        table[price_comp] = dictionary_locals['y']
        return table
