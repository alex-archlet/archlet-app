""" This module contains functionality to dispatch the correct job """
import os
import socket

from datetime import datetime
from uuid import UUID

from shared.constants import JOB_STATUS
from shared.constants import JOB_TYPE
from shared.constants import RecordedError
from shared.communication import DatabaseConnection
from shared.communication import ExecuteTask
from shared.communication import IOFacade
from shared.communication import SingletonLogger
from shared.utilities.uuid import is_valid_uuid
from jobs.column_matching_job import ColumnMatchingJob
from jobs.expression_evaluation_job import ExpressionEvaluationJob
from jobs.expression_example_job import ExpressionExampleJob
from jobs.file_processing_job import FileProcessingJob
from jobs.offer_processing_job import OfferProcessingJob
from jobs.optimization_job import OptimizationJob
from jobs.utils import get_job_mapping

CONFIG = {}

CONFIG['file_storage'] = {
    "logs_path": os.environ.get("LOGS_PATH"),
    "data_path": os.environ.get("DATA_PATH")
}

JOB_MAPPING = get_job_mapping()

slogger = SingletonLogger()

def execute_job(job_id: str,
                database_connection: DatabaseConnection):
    """
    High-level function that handles all work requests to the engine
    """

    # Logging file name
    timestamp = datetime.utcnow().strftime('%Y.%m.%d_%H:%M:%S.%f')
    datestamp = datetime.utcnow().strftime('%Y_%m_%d')

    log_fname = f'{timestamp}_{socket.gethostname()}_job_{job_id}.log'

    # Create logging folder
    top_dir = CONFIG['file_storage']['logs_path']
    log_dir = f'{top_dir}/{datestamp}'

    # Initialize logging output
    slogger.clear()
    slogger.set_logger_output(logfile=log_fname, logdir=log_dir)

    if not is_valid_uuid(job_id):
        return None

    io_facade = IOFacade(job_id, CONFIG['file_storage']['data_path'])

    # Set the database connection, this way we use the existing connection pool
    io_facade.database_connection = database_connection

    # Execute job
    with ExecuteTask(f'executing job with ID {job_id}',
                     io_facade,
                     task_id=job_id,
                     toplevel=True):
        with ExecuteTask('retrieving information from database',
                         io_facade):
            # fetch job and related data
            job_info = io_facade.fetch_job()
            if (job_info.status_id == JOB_STATUS.CANCELED) or (job_info.status_id == JOB_STATUS.ABORTED):
                raise RecordedError("User interrupt: won't continue executing job")

            # update job status
            io_facade.update_job_status(JOB_STATUS.RUNNING)

        # Depending on job type, call the appropriate function
        job = JOB_MAPPING[job_info.type_id](io_facade)

        with ExecuteTask('Running ' + job.name() + ' job', io_facade, reraise=False):
            # proposition: an additional parse input step:
            with ExecuteTask('parsing input', io_facade):
                job.parse_input(job_info.input)

            # TODO: check if loading of translator is truly in every job present
            with ExecuteTask('loading extras', io_facade):
                job.load_extras()

            with ExecuteTask('loading data tables', io_facade):
                job.load_tables()

            with ExecuteTask('running preprocessing of job', io_facade):
                job.preprocess()

            with ExecuteTask('running main part of the job', io_facade):
                job.run()

            with ExecuteTask('saving results', io_facade):
                job.save_results()

        with ExecuteTask('saving messages to database', io_facade, reraise=False):
            job.save_messages()
