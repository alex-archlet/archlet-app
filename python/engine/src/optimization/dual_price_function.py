# import gurobipy as gp
# from gurobipy import GRB
from collections import defaultdict
import numpy as np

from shared.constants import UKEYS, KEYS

class DualPriceFunction:

    def __init__(self):
        # TODO : dont load variables from state
        self.allocations_data = None #allocations_table.data
        self.model = None # optimization_model.model

    def run(self):

        self.model.optimize()

        self.fixed = self.model.fixed()

        self.fixed.optimize()

        shadow_price = self.fixed.getAttr(GRB.Attr.Pi)
        const_names = [x.ConstrName for x in self.fixed.getConstrs()]

        self.allocations_data.set_index(KEYS.REGION, inplace = True)

        spend = defaultdict(float)

        spend['Global'] = self.allocations_data['total_spent'].sum()

        regions = np.unique(self.allocations_data.index.values)

        for region in regions:
            spend[region] = self.allocations_data['total_spent'][region].sum()

        shadow_prices_segregated = defaultdict(float)

        for i in range(len(shadow_price)):

            if shadow_price[i] != 0:

                if 'max_spend_per_supplier_GLOBAL_' in const_names[i]:
                    shadow_prices_segregated['max_spend_per_supplier_GLOBAL'] += shadow_price[i]
                if 'min_spend_per_supplier_GLOBAL_' in const_names[i]:
                    shadow_prices_segregated['min_spend_per_supplier_GLOBAL'] += shadow_price[i]

                if 'max_percentage_per_supplier_GLOBAL_' in const_names[i]:
                    shadow_prices_segregated['max_percentage_per_supplier_GLOBAL'] += shadow_price[i]*spend['Global']*0.01
                if 'min_percentage_per_supplier_GLOBAL_' in const_names[i]:
                    shadow_prices_segregated['min_percentage_per_supplier_GLOBAL'] += shadow_price[i]*spend['Global']*0.01

                if 'max_percentage_GLOBAL_item_level_' in const_names[i]:
                    shadow_prices_segregated['max_percentage_GLOBAL_item_level'] += shadow_price[i]*0.01
                if 'min_percentage_GLOBAL_item_level_' in const_names[i]:
                    shadow_prices_segregated['max_percentage_GLOBAL_item_level'] += shadow_price[i]*0.01

                for region in regions:
                    if 'max_spend_per_supplier_' + region in const_names[i]:
                        shadow_prices_segregated['max_spend_per_supplier_' + region] += shadow_price[i]
                    if 'min_spend_per_supplier_' + region in const_names[i]:
                        shadow_prices_segregated['min_spend_per_supplier_' + region] += shadow_price[i]

                    if 'max_percentage_per_supplier_' + region in const_names[i]:
                        shadow_prices_segregated['max_percentage_per_supplier_' + region] += shadow_price[i]*spend[region]
                    if 'min_percentage_per_supplier_' + region in const_names[i]:
                        shadow_prices_segregated['min_percentage_per_supplier_' + region] += shadow_price[i]*spend[region]

        self.shadow_prices = dict(shadow_prices_segregated)

        return self.shadow_prices
