"""
Module containing the optimization model builder class.
"""

# pylint: disable=line-too-long
# pylint: disable=invalid-name

from functools import reduce
from typing import List, Tuple, Dict, Optional

import numpy as np

from mip import LinExpr

from shared.constants import CONSTRAINT_SPECS
from shared.constants import CONSTRAINT_TYPES
from shared.constants import UKEYS, KEYS
from shared.constants import OPERATION_TYPE
from shared.constants import VAR_TYPES
from shared.constants import RecordedError

from shared.communication import SingletonLogger

# from . import GurobiWrapper
from . import MIPWrapper

# Indexes of each information in the tuples to make formulas easier to read
SUPPLIER = 0
ITEM = 1
ITEM_TYPE = 2

# Declaration of types
OffersIndexes = List[Tuple[int, int, int]]

slogger = SingletonLogger()

class ModelBuilder:
    """
    This class is the interface to build an optimization model.
    It provides methods to construct the variables, objective function and constraints.
    """

    def __init__(self,
                 matrices: Dict[str, np.ndarray],
                 mapping_dic: Dict[str, Dict],
                 solver: str,
                 constraints: dict,
                 manual_alloc: Optional[np.ndarray] = None) -> None:

        """
        This is the constructor for the MIP_object class.
        This function creates an instance of this object while reading in all the variables to define
        the optimization problem.
        """

        # Main variables for basic scenarios:
        self.n_suppliers = len(mapping_dic['suppliers_list'])
        self.n_items = len(mapping_dic['demands_list'])
        self.n_item_types = len(mapping_dic['item_type_to_id'])
        self.category_to_item = mapping_dic['category_to_item']
        self.supplier_to_id = mapping_dic['supplier_to_id']
        self.item_type_to_id = mapping_dic['item_type_to_id']

        self.costs = matrices['costs']
        self.demanded_volumes = matrices['demanded_volumes']
        self.volumes = matrices['volumes']
        self.soft_constraints = matrices['soft_constraints']
        self.solver = solver
        self.constraints_dic = constraints

        self.offer_tuples = [tuple(elmt) for elmt in np.argwhere(self.costs != -1)]

        self.manual_alloc = manual_alloc

        self.model = None
        self.x = None
        self.y = None
        self.z = None
        self.w_items = None

        self.M = 1e8

    def create_model(self) -> None:
        """
        Create the correct model wrapper to build the optimization model
        """
        if self.solver == 'CBC':
            self.model = MIPWrapper('sourcing_optimizer', self.solver)
        # elif self.solver == 'GRB':
        #     self.model = GurobiWrapper('sourcing_optimizer', self.solver)
        else:
            slogger.error("The specified solver is not supported currently")
            return


#####################################################################
#                       variable builders
#####################################################################


    def create_base_variables(self, split_items: bool = True) -> None:
        """
        Create the allocation variables
        """
        # Define main model vars for basic scenarios:
        var_type = VAR_TYPES.CONTINUOUS if split_items else VAR_TYPES.BINARY
        self.x = np.array([[[self.model.add_variable(f'allocation_{i}_{j}_{k}', type=var_type)\
                             for k in range(self.n_item_types)] for j in range(self.n_items)] for i in range(self.n_suppliers)])

    def create_discounts_variables(self, nbr_rebates: List[int]) -> None:
        """
        Create the variables needed for the discounts optimization
        """
        self.y = np.array([[[[self.model.add_variable(name=f"allocation_{i}_{j}_{k}_{l}", type=VAR_TYPES.CONTINUOUS)\
                 for l in range(nbr_rebates[i])] + [None for _ in range(max(nbr_rebates) - nbr_rebates[i])] for k in range(self.n_item_types)] for j in range(self.n_items)] for i in range(self.n_suppliers)])
        self.z = np.array([[self.model.add_variable(name=f"activation_{j}_{l}")\
                 for l in range(nbr_rebates[j])] + [None for _ in range(max(nbr_rebates) - nbr_rebates[j])] for  j in range(self.n_suppliers)])

#####################################################################
#                  objective/total spend builders
#####################################################################

    def base_total_spend(self, tuples: OffersIndexes = None) -> LinExpr:
        """ Compute the total spend """
        if tuples is None:
            tuples = self.offer_tuples
        return self.model.sum((self.costs[offer]*self.demanded_volumes[offer[ITEM]]*(self.x[offer] + self.manual_alloc[offer]) for offer in tuples))

    def price_comp_base_objective(self,
                                  price_components: List[str],
                                  price_components_array: np.ndarray,
                                  price_comp_to_id: Dict[str, int]) -> LinExpr:
        """
        Base objective function that optimizes with respect to a list of price components
        """
        spend = self.model.sum(price_components_array[offer][price_comp_to_id[price_comp]]*self.demanded_volumes[offer[ITEM]]*self.x[offer]
                               for offer in self.offer_tuples for price_comp in price_components)
        return spend


    def add_partial_regularizer(self, penalties: List[float]) -> LinExpr:
        """
        Add the partial regularizer to the objective
        """
        partial_regularizer = self.model.add_variable(name="partial_regularizer", type=VAR_TYPES.CONTINUOUS)
        self.model.add_constraint(partial_regularizer == self.model.sum((1 - self.w_items[i])*penalties[i] for i in range(self.n_items)))
        return partial_regularizer

    def subtract_discounts(self, discount_buckets, tuples: OffersIndexes = None) -> LinExpr:
        """
        Subtract the discounts values from the objective
        """
        if tuples is None:
            tuples = self.offer_tuples

        unit_discounts = self.model.sum(- self.model.sum(bucket.item_discounts[(j, k)]*self.demanded_volumes[j]*self.y[bucket.supplier_id, j, k, bucket.order] \
                        for (j, k) in bucket.discounted_by if (bucket.supplier_id, j, k) in tuples) for bucket in discount_buckets if bucket.discount_type != 'flat')

        flat_discounts = self.model.sum(-(bucket.discount_amount * self.z[bucket.supplier_id, bucket.order]) for bucket in discount_buckets if bucket.discount_type == 'flat')

        return unit_discounts + flat_discounts

    def subtract_price_comp_discounts(self,
                                      discount_buckets,
                                      price_components,
                                      price_components_array,
                                      price_comp_to_id,
                                      tuples: OffersIndexes = None) -> LinExpr:
        """
        Subtract the price components discounts values from the objective
        """
        if tuples is None:
            tuples = self.offer_tuples

        return self.model.sum(- self.model.sum(sum(price_components_array[bucket.supplier_id, j, k][price_comp_to_id[price_comp]]
                                                   for price_comp in price_components)/self.costs[bucket.supplier_id, j, k]*bucket.item_discounts[(j, k)]*self.demanded_volumes[j]*self.y[bucket.supplier_id, j, k][bucket.order]
                                               for (j, k) in bucket.discounted_by if (bucket.supplier_id, j, k) in tuples) for bucket in discount_buckets)


    def set_objective(self, objective: LinExpr) -> None:
        """
        Set the variable built previously as the objective of the optimization
        """
        self.model.set_objective(objective, 1)

#####################################################################
#                      constraint builders
#####################################################################


    def add_base_constraints(self, partial_allocations: bool = False) -> None:
        """
        Add basic constraints to the model (meet demands ...)
        """

        # make sure whole item is procured.
        if partial_allocations:
            self.w_items = [self.model.add_variable(name=f'activation_item_{i}', type=VAR_TYPES.CONTINUOUS) for i in range(self.n_items)]
            for item in range(self.n_items):
                self.model.add_constraint(self.model.sum((self.x[offer] + self.manual_alloc[offer]) for offer in self.offer_tuples if offer[ITEM] == item) >= self.w_items[item])
                self.model.add_constraint(self.w_items[item] >= 0)
                self.model.add_constraint(self.w_items[item] <= 1)
        else:
            for item_id in range(self.n_items):
                self.model.add_constraint(self.model.sum((self.x[offer] + self.manual_alloc[offer]) for offer in self.offer_tuples if offer[ITEM] == item_id) >= 1)
                self.model.add_constraint(self.model.sum((self.x[offer] + self.manual_alloc[offer]) for offer in self.offer_tuples if offer[ITEM] == item_id) <= 1)

        # Make sure only offered volume can be procured
        for offer in self.offer_tuples:
            self.model.add_constraint((self.x[offer] + self.manual_alloc[offer])*self.demanded_volumes[offer[ITEM]] <= self.volumes[offer])

    def add_item_incumbency(self, baseline_offers: OffersIndexes) -> None:
        """
        Enforce incumbency on an item level : items can only be provided by the same
        supplier as it was in the baseline
        """
        baseline_offers = set((i, j) for i, j, _ in baseline_offers)
        self.model.add_constraint(self.model.sum(self.x[offer] for offer in self.offer_tuples if (offer[SUPPLIER], offer[ITEM]) not in baseline_offers) == 0)

    def add_general_incumbency(self, baseline_offers: OffersIndexes) -> None:
        """
        Enforce general incumbency : only suppliers from the baseline can 
        provide items
        """
        new_suppliers = set(i for i, _, _ in self.offer_tuples).difference(set(i for i, _, _ in baseline_offers))
        activation_new_supp = [self.model.add_variable(name=f"activation_new_supp_{i}") for i in range(self.n_suppliers)]

        for supplier in new_suppliers:
            self.model.add_constraint(self.model.sum(self.x[offer] for offer in self.offer_tuples if offer[SUPPLIER] == supplier) <= activation_new_supp[supplier]*self.M)

        self.model.add_constraint(self.model.sum(activation_new_supp[i] for i in range(self.n_suppliers)) == 0) # Can be changed to <= constraint[value] later

    def add_item_type_constraint(self,
                                 item_type_to_id: Dict[str, int],
                                 item_type_slctd: str) -> None:
        """
        Only take into consideration the offers from wanted item types
        """
        item_type_ids = [item_type_to_id[key] for key in item_type_slctd]
        self.model.add_constraint(self.model.sum(self.x[offer] for offer in self.offer_tuples if offer[ITEM_TYPE] not in item_type_ids) == 0)

    def exclude_suppliers(self, constraint: dict, soft_const_to_id: Dict[str, int]) -> None:
        """
        Exclude the offers that don't match a certain criteria
        """
        _, filtered_offers = self.filtered_offers(constraint[CONSTRAINT_SPECS.FILTERS])

        criteria = constraint[CONSTRAINT_SPECS.CRITERIA]
        value = constraint[KEYS.VALUE]


        if criteria != CONSTRAINT_SPECS.EXCLUDE_ALL:
            criteria = soft_const_to_id[criteria]
            if constraint[OPERATION_TYPE.OPERATION] == OPERATION_TYPE.KEEP_HIGHEST:
                maxs = [float('-inf') for _ in range(self.n_items)]

                def check(offer_tuple):
                    if self.soft_constraints[offer_tuple][criteria] > maxs[offer_tuple[ITEM]]:
                        maxs[offer_tuple[ITEM]] = self.soft_constraints[offer_tuple][criteria]

                any(check(offer_tuple) for offer_tuple in filtered_offers)

                offers = [offer for offer in filtered_offers if self.soft_constraints[offer][criteria] != maxs[offer[ITEM]]]
            elif constraint[OPERATION_TYPE.OPERATION] == OPERATION_TYPE.KEEP_LOWEST:
                mins = [float('inf') for _ in range(self.n_items)]

                def check(offer_tuple):
                    if self.soft_constraints[offer_tuple][criteria] < mins[offer_tuple[ITEM]]:
                        mins[offer_tuple[ITEM]] = self.soft_constraints[offer_tuple][criteria]

                any(check(offer_tuple) for offer_tuple in filtered_offers)

                offers = [offer for offer in filtered_offers if self.soft_constraints[offer][criteria] != mins[offer[ITEM]]]
            else:
                value = float(value)
                if constraint[OPERATION_TYPE.OPERATION] == OPERATION_TYPE.EXCLUDE_BELOW:
                    offers = [offer for offer in filtered_offers if self.soft_constraints[offer][criteria] < value]
                elif constraint[OPERATION_TYPE.OPERATION] == OPERATION_TYPE.EXCLUDE_ABOVE:
                    offers = [offer for offer in filtered_offers if self.soft_constraints[offer][criteria] > value]
                elif constraint[OPERATION_TYPE.OPERATION] == OPERATION_TYPE.EXCLUDE_EXACT:
                    offers = [offer for offer in filtered_offers if self.soft_constraints[offer][criteria] == value]
                else:
                    raise NotImplementedError('Operation not implemented')
        else:
            offers = filtered_offers

        self.model.add_constraint(self.model.sum(self.x[offer] for offer in offers) == 0)

    def add_discounts_constraints(self, discount_buckets):
        """
        Create constraints for each discount bucket.
        """
        for bucket in discount_buckets:
            i = bucket.supplier_id
            l = bucket.order
            unit = bucket.unit
            for j, k in bucket.contributing_to:
                self.model.add_constraint(((self.x[i, j, k] + self.manual_alloc[i, j, k]) - self.y[i, j, k, l]) >= 0)
                self.model.add_constraint((self.z[i, l] - self.y[i, j, k, l]) >= 0)
            for j, k in bucket.discounted_by:
                self.model.add_constraint(((self.x[i, j, k] + self.manual_alloc[i, j, k]) - self.y[i, j, k, l]) >= 0)
                self.model.add_constraint((self.z[i, l] - self.y[i, j, k, l]) >= 0)
            if unit == 'volume':
                self.model.add_constraint(self.model.sum(self.demanded_volumes[j]*(self.x[i, j, k] + self.manual_alloc[i, j, k]) for j, k in bucket.contributing_to) >= self.z[i, l]*bucket.threshold, \
                        f"rebate_threshold_{i}_{l}")
            elif unit == 'spend':
                self.model.add_constraint(self.model.sum(self.costs[i, j, k]*self.demanded_volumes[j]*(self.x[i, j, k] + self.manual_alloc[i, j, k]) for j, k in bucket.contributing_to) >= self.z[i, l]*bucket.threshold, \
                        f"rebate_threshold_{i}_{l}")
            else:
                slogger.error(f"unspecified discount unit: {unit}")
                raise RecordedError

    def add_capacities_constraints(self,
                                   capacities_mappings: dict,
                                   capacities_matrices: Dict[str, np.ndarray]) -> None: #TODO : rewrite
        """
        This function adds the capacity constraints that are given by the offers and capacities
        file, depending on the capacities selected by the user
        """
        max_capacities = capacities_matrices['max_capacities']
        capacities     = capacities_matrices['capacities']
        capacity_to_id = capacities_mappings['capacity_to_id']

        for supplier in range(self.n_suppliers):
            offers = [offer for offer in self.offer_tuples if offer[SUPPLIER] == supplier]
            for capacity in self.constraints_dic[CONSTRAINT_TYPES.GENERAL_SETTINGS]['capacities']:
                k = capacity_to_id[capacity]
                self.model.add_constraint(self.model.sum(capacities[offer][k]*self.x[offer] for offer in offers) <= max_capacities[supplier][k])


    def add_supplier_constraint(self, constraints_dic: dict) -> None:
        """
        This function adds number of suppliers constraints.
        It can be used with mulitple filters
        """

        if not self.check_mutation(constraints_dic, self.add_supplier_constraint):
            return

        filter_offers, _ = self.filtered_offers(constraints_dic[CONSTRAINT_SPECS.FILTERS])

        suppliers = list(set([elmt[SUPPLIER] for elmt in filter_offers]))

        temp_allocations = [self.model.add_variable(name=f"activation_num_suppliers_{i}") for  i in range(self.n_suppliers)]  # TODO : change name

        if constraints_dic[OPERATION_TYPE.OPERATION] == OPERATION_TYPE.MAX:
            for i in suppliers:
                items = [(elmt[ITEM], elmt[ITEM_TYPE]) for elmt in filter_offers if elmt[SUPPLIER] == i]
                for item in items:
                    self.model.add_constraint(self.x[i, item[0], item[1]] <= temp_allocations[i])
            self.model.add_constraint(self.model.sum(temp_allocations[i] for i in range(self.n_suppliers)) <= int(constraints_dic[KEYS.VALUE]))
        elif constraints_dic[OPERATION_TYPE.OPERATION] == OPERATION_TYPE.MIN:
            for i in suppliers:
                items = [(elmt[ITEM], elmt[ITEM_TYPE]) for elmt in filter_offers if elmt[SUPPLIER] == i]
                self.model.add_constraint(self.model.sum(self.x[i, item[0], item[1]] for item in items) >= 0.01*temp_allocations[i])
            self.model.add_constraint(self.model.sum(temp_allocations[i] for i in suppliers) >= int(constraints_dic[KEYS.VALUE]))
        elif constraints_dic[OPERATION_TYPE.OPERATION] == OPERATION_TYPE.EQUAL:
            for i in suppliers:
                items = [(elmt[ITEM], elmt[ITEM_TYPE]) for elmt in filter_offers if elmt[SUPPLIER] == i]
                self.model.add_constraint(self.model.sum(self.x[i, item[0], item[1]] for item in items) >= 0.01*temp_allocations[i])
                for item in items:
                    self.model.add_constraint(self.x[i, item[0], item[1]] <= temp_allocations[i])
            self.model.add_constraint(self.model.sum(temp_allocations[i] for i in suppliers) == int(constraints_dic[KEYS.VALUE]))


    def add_spend_constraint(self, constraints_dic) -> None:
        """
        This function adds absolute spend per supplier constraints.
        It can be used with mulitple filters, and supports the every-supplier constraint
        """

        self.check_mutation(constraints_dic, self.add_spend_constraint)

        _, filter_offers = self.filtered_offers(constraints_dic[CONSTRAINT_SPECS.FILTERS])

        if constraints_dic[OPERATION_TYPE.OPERATION] == OPERATION_TYPE.MAX:
            self.model.add_constraint(self.total_spend(tuples=filter_offers) <= float(constraints_dic['value']))
        elif constraints_dic[OPERATION_TYPE.OPERATION] == OPERATION_TYPE.MIN:
            self.model.add_constraint(self.total_spend(tuples=filter_offers) >= float(constraints_dic['value']))

    def add_volume_constraint(self, constraints_dic: dict) -> None:
        """
        This function adds volume allocation per supplier constraints.
        It can be used with mulitple filters.
        """

        if not self.check_mutation(constraints_dic, self.add_volume_constraint):
            return

        _, filter_offers = self.filtered_offers(constraints_dic[CONSTRAINT_SPECS.FILTERS])

        if constraints_dic[OPERATION_TYPE.OPERATION] == OPERATION_TYPE.MAX:
            self.model.add_constraint(self.model.sum(self.demanded_volumes[offer[ITEM]]*self.x[offer] for offer in filter_offers) <= float(constraints_dic[KEYS.VALUE]))
        elif constraints_dic[OPERATION_TYPE.OPERATION] == OPERATION_TYPE.MIN:
            self.model.add_constraint(self.model.sum(self.demanded_volumes[offer[ITEM]]*self.x[offer] for offer in filter_offers) >= float(constraints_dic[KEYS.VALUE]))


    def add_percentage_constraint(self, constraints_dic: dict) -> None:
        """
        This function adds percentage spend per supplier constraints.
        """

        if not self.check_mutation(constraints_dic, self.add_percentage_constraint):
            return

        filter_offers, supplier_offers = self.filtered_offers(constraints_dic[CONSTRAINT_SPECS.FILTERS])

        if constraints_dic[OPERATION_TYPE.OPERATION] == OPERATION_TYPE.MAX:
            self.model.add_constraint(self.total_spend(tuples=supplier_offers) <= float(constraints_dic[KEYS.VALUE])*0.01*self.total_spend(tuples=filter_offers))
        elif constraints_dic[OPERATION_TYPE.OPERATION] == OPERATION_TYPE.MIN:
            self.model.add_constraint(self.total_spend(tuples=supplier_offers) >= float(constraints_dic[KEYS.VALUE])*0.01*self.total_spend(tuples=filter_offers))

    #####################################################################
    #                      Results getters
    #####################################################################

    def allocations(self) -> np.ndarray:
        """
        This function retrieves and returns the allocations data from the model object.
        """
        get_if_offer = np.vectorize(lambda x: self.model.get_variable(x) if x is not None else 0.)
        allocations = get_if_offer(self.x)
        return allocations + self.manual_alloc

    def supplier_discount(self) -> np.ndarray:
        """
        This function retrieves the indicator variable that conveys if a particular discount was activated. This is
        necessary for the final allocations table to be generated.
        """
        get_offer = np.vectorize(lambda x: self.model.get_variable(x) if x is not None else 0.)
        supplier_discount = get_offer(self.z)
        return supplier_discount


    def item_discounts(self) -> np.ndarray:
        """
        This function retrieves the discount allocations of each item for each bucket.
        """
        get_offer = np.vectorize(lambda x: self.model.get_variable(x) if x is not None else 0.)
        item_discounts = get_offer(self.y)
        return item_discounts


    def get_wrapper(self):
        """
        Return the model
        """
        return self.model

    #####################################################################
    #                      Utils
    #####################################################################

    @staticmethod
    def check_mutation(constraints_dic, func):
        """
        Checks whether the constraint is applied on foreach or forall, if the constraints
        are respected (only one multifilter), and applies it
        """
        if 'mutation' in constraints_dic and constraints_dic['mutation'] == 'for_each':
            multifilters_mask = [int(len(filters) > 1)  if category != UKEYS.SUPPLIER else 0 for category, filters in constraints_dic[CONSTRAINT_SPECS.FILTERS].items()]
            if sum(multifilters_mask) > 1:
                raise RecordedError('Multiple filters can only be set on one category')
            elif sum(multifilters_mask) == 1:
                multifilters_index = multifilters_mask.index(1)
                name_value_pairs = constraints_dic[CONSTRAINT_SPECS.FILTERS].items()
                filter_name = list(name_value_pairs)[multifilters_index][0]
                filter_values = list(name_value_pairs)[multifilters_index][1]
                for value in filter_values:
                    temp_dic = dict(constraints_dic) # Create copy of dict, different reference
                    temp_dic[CONSTRAINT_SPECS.FILTERS][filter_name] = [value]
                    func(temp_dic)
                return False
            else:
                if UKEYS.SUPPLIER in constraints_dic[CONSTRAINT_SPECS.FILTERS] and len(constraints_dic[CONSTRAINT_SPECS.FILTERS][UKEYS.SUPPLIER]) > 1:
                    for supplier in constraints_dic[CONSTRAINT_SPECS.FILTERS][UKEYS.SUPPLIER]:
                        temp_dic = dict(constraints_dic)
                        temp_dic[CONSTRAINT_SPECS.FILTERS][UKEYS.SUPPLIER] = [supplier]
                        func(temp_dic)
                    return False
                else:
                    return True
        else:
            return True

    def filtered_offers(self, filters: dict) -> Tuple[OffersIndexes, OffersIndexes]:
        """
        Return the set of offers based on the filters given in the scenario parameters
        """

        list_sets = [reduce(lambda x, y: x|y, [set(self.category_to_item[category][value]) for value in list_filters])  for category, list_filters in filters.items() if category != UKEYS.SUPPLIER] +\
                    [set(self.category_to_item[category]['All']) for category in self.category_to_item.keys() if category != UKEYS.SUPPLIER]

        filter_offers = list(reduce(lambda x, y: x & y, list_sets))

        filter_offers = list(map(lambda tupl: (self.supplier_to_id[tupl[SUPPLIER]], 
                                               int(tupl[ITEM]),
                                               self.item_type_to_id[tupl[ITEM_TYPE]]),
                                 filter_offers))

        supplier_offers = filter_offers # That way, if no supplier selected, returns the filtered offers

        if UKEYS.SUPPLIER not in filters.keys():
            pass
        else:
            supplier_ids = [self.supplier_to_id[supp] for supp in filters[UKEYS.SUPPLIER]]
            supplier_offers = [supp_item for supp_item in filter_offers if supp_item[SUPPLIER] in supplier_ids]

        return filter_offers, supplier_offers

    #####################################################################
    #                      Unused constraints
    #####################################################################

    def add_item_level_constraint(self, constraints_dic: dict) -> None:
        """
        This function adds number of suppliers and max/min percentage allocation per supplier constraints per item.
        """
        filter_offers, _ = self.filtered_offers(constraints_dic[CONSTRAINT_SPECS.FILTERS])

        items_selected = [i for i in range(self.n_items) if (self.demanded_volumes[i] < float(constraints_dic['max_volume'])
                                                             and self.demanded_volumes[i] > float(constraints_dic['min_volume']))]

        filter_offers = [elmt for elmt in filter_offers if elmt[1] in items_selected]

        if constraints_dic['constraint'] == 'min_supplier':
            temp_activations = [[self.model.add_variable(f"activation_min_suppliers_multi_level_split_{j}_{i}") for j in range(self.n_suppliers)] for i in range(self.n_items)]

            for j in [elmt[0] for elmt in filter_offers]:
                for i in [elmt[1] for elmt in filter_offers if elmt[0] == j]:
                    self.model.add_constraint(self.x[i][j] >= 0.01*temp_activations[i][j])

            for i in [elmt[1] for elmt in filter_offers]:
                suppliers = [elmt[0] for elmt in filter_offers if elmt[1] == i]
                self.model.add_constraint(self.model.sum(temp_activations[i][j] for j in suppliers) >= int(constraints_dic['value']))

        elif constraints_dic['constraint'] == 'max_supplier':
            temp_activations = [[self.model.add_variable(f"activation_max_suppliers_multi_level_split_{j}_{i}") for j in range(self.n_suppliers)] for i in range(self.n_items)]

            for j in [elmt[0] for elmt in filter_offers]:
                for i in [elmt[1] for elmt in filter_offers if elmt[0] == j]:
                    self.model.add_constraint(self.x[i][j] <= temp_activations[i][j])

            for i in [elmt[1] for elmt in filter_offers]:
                suppliers = [elmt[0] for elmt in filter_offers if elmt[1] == i]
                self.model.add_constraint(self.model.sum(temp_activations[i][j] for j in suppliers) <= int(constraints_dic['value']))

        elif constraints_dic['constraint'] == 'min_percentage':
            temp_activations = [[self.model.add_variable(f"activations_min_percentage_multi_level_splits_{i}_{j}_" + str(constraints_dic['value'])) for j in range(self.n_suppliers)] for i in range(self.n_items)]
            for i in set([elmt[1] for elmt in filter_offers]):
                suppliers = set([elmt[0] for elmt in filter_offers if elmt[1] == i])
                for j in suppliers:
                    self.model.add_constraint(self.x[i][j] <= temp_activations[i][j])
                    self.model.add_constraint(self.x[i][j] >= 0.01*float(constraints_dic['value'])*temp_activations[i][j])

        elif constraints_dic['constraint'] == 'max_percentage':
            for j in [elmt[0] for elmt in filter_offers]:
                for i in [elmt[1] for elmt in filter_offers if elmt[0] == j]:
                    self.model.add_constraint(self.x[i][j] <= float(constraints_dic['value'])*0.01)
