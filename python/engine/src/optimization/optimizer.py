"""
TODO
"""
from datetime import datetime
from mip import LinExpr
from typing import Dict
from typing import List

import numpy as np
import pandas as pd

from shared.constants import CONSTRAINT_SPECS
from shared.constants import CONSTRAINT_TYPES
from shared.constants import UKEYS, KEYS
from shared.constants import RecordedError
from shared.constants import UNITS
from shared.entities import DiscountBucket
from shared.entities import TableData
from shared.entities import TableHeader
from shared.entities import ProjectSettings
from shared.communication import SingletonLogger

from . import ModelBuilder

from optimization.optimizer_preprocessor import add_demand_int_id
from optimization.optimizer_preprocessor import add_min_unit_price
from optimization.optimizer_preprocessor import compute_item_discounts
from optimization.optimizer_preprocessor import compute_rebates_per_supplier
from optimization.optimizer_preprocessor import create_basic_mappings
from optimization.optimizer_preprocessor import create_capacities_mappings
from optimization.optimizer_preprocessor import create_discount_buckets
from optimization.optimizer_preprocessor import create_manual_alloc_array
from optimization.optimizer_preprocessor import create_price_comp_mapping
from optimization.optimizer_preprocessor import get_basic_matrices
from optimization.optimizer_preprocessor import get_capacities
from optimization.optimizer_preprocessor import get_partial_alloc_penalties
from optimization.optimizer_preprocessor import get_price_comp_matrix
from optimization.optimizer_preprocessor import get_soft_constraints
from optimization.optimizer_preprocessor import create_allocations_table
from optimization.optimizer_preprocessor import create_baseline_tuples

def crosstab2stacked(crosstab, col_name):

    """
    Converts a pandas cross table dataframe to a normal stacked dataframe.
    """

    # stack and reset index. rename values in the crosstable to given column name.
    stacked = crosstab.stack(dropna=True).reset_index().rename(
        columns={0: col_name})

    return stacked

def cascade_rounding(x):

    """
    Converts an array of float values to an array of int values.
    The sum of the array is maintained to be constant.
    """

    # maintain the cummulative sum of both int and float arrays.
    int_cummulative_sum = 0
    float_cummulative_sum = 0.0
    x_new = [0]*len(x)

    for i, item in enumerate(x):
        x_new[i] = round(float_cummulative_sum + item) - int_cummulative_sum

        # update cummulative sums.
        int_cummulative_sum += x_new[i]
        float_cummulative_sum += item

    return x_new

def cascade_rounding_vec(x):

    """
    Converts a 2d array of float values to an array of int values.
    The sum of the rows is maintained to be constant.
    """

    int_x = [cascade_rounding(x_row) for x_row in x]

    return np.array(int_x)


slogger = SingletonLogger()

class Optimizer:
    """
    TODO
    """
    def __init__(self,
                 params,
                 offers: TableData,
                 demands: TableData,
                 discounts: TableData,
                 capacities: TableData = None,
                 previous_alloc: TableData = None,
                 project_settings: ProjectSettings = ProjectSettings(),
                 baseline_offers: TableData = None,
                 solver: str = 'CBC'):
        """
        This is the constructor for the optimizer class

        Args:
            params
            offers
            demands
            discounts
            capacities
        """
        self.params = params
        self.solver = solver
        self.offers = offers
        self.baseline_offers = baseline_offers
        self.baseline_tuples = None
        self.demands = demands
        self.discounts = discounts
        self.capacities = capacities
        self.previous_alloc = previous_alloc
        self.project_settings = project_settings
        self.builder = None
        self.model = None
        self.preprocessor = None
        self.price_comp_array = None
        self.price_comp_to_id = None

    def initialize_preprocessor(self):
        """
        Initialize the MIP preprocessor for this optimizer.
        """
        start_time = datetime.now()
        self.offers.data.reset_index(inplace=True, drop=True)
        self.demands.data.reset_index(inplace=True, drop=True)

        # Add demand integer ids
        self.demands, self.offers, self.baseline_offers = add_demand_int_id(self.demands,
                                                                            self.offers,
                                                                            self.baseline_offers)
        # add min unit price to offers
        self.offers = add_min_unit_price(self.offers)

        slogger.info('Initialization of optimizer took: ' + str((datetime.now() - start_time).total_seconds()))

    def initialize_model(self, filters: List[str]):
        """
        This function creates the optimization model for scenarios that don't have a saved model file.
        Hence, the constraints are needed to be read from the json.
        """
        start_time = datetime.now()

        demands_header = self.demands.header
        offers_header = self.offers.header

        self.int_volumes_flag = False
        if (self.demands.data[demands_header[UKEYS.TOTAL_VOLUME]].astype(int) == self.demands.data[demands_header[UKEYS.TOTAL_VOLUME]]).all():
            self.int_volumes_flag = True

        # Copy the filters columns of the demands to the offers
        filter_frame = self.demands[filters+[demands_header[UKEYS.DEMAND_ID]]]
        self.offers.data = self.offers.data.merge(filter_frame, left_on=offers_header[UKEYS.DEMAND_ID], right_on=demands_header[UKEYS.DEMAND_ID])
        self.offers.trim()

        slogger.info('Initialization of model: ' + str((datetime.now() - start_time).total_seconds()))

        self.build_model(filters)

        slogger.info('Building the model took: ' + str((datetime.now() - start_time).total_seconds()))

        return True

    def build_model(self, filters: List[str]):
        """
        Build the model and set the basic constraints related to the 
        general settings
        """
        # Get basic mappings, matrices and settings
        mapping_dic = create_basic_mappings(self.demands, self.offers, filters, self.project_settings)
        self.mappings = mapping_dic
        self.discounts_mappings = {}
        self.discounts_matrices = None
        matrices = get_basic_matrices(self.offers, self.demands, mapping_dic, self.project_settings)
        manual_alloc = create_manual_alloc_array(self.previous_alloc, self.offers, mapping_dic)

        if self.baseline_offers is not None:
            self.baseline_tuples = create_baseline_tuples(self.baseline_offers, mapping_dic)

        self.general_settings = self.params[CONSTRAINT_TYPES.GENERAL_SETTINGS] if CONSTRAINT_TYPES.GENERAL_SETTINGS in self.params else {}
        split_items = self.general_settings[CONSTRAINT_SPECS.SPLIT_ITEMS] if CONSTRAINT_SPECS.SPLIT_ITEMS in self.general_settings else False
        partial_allocation = self.general_settings[CONSTRAINT_SPECS.PARTIAL_ALLOCATION] if CONSTRAINT_SPECS.PARTIAL_ALLOCATION in self.general_settings else False
        rebates_applied = self.general_settings[CONSTRAINT_SPECS.REBATES] if CONSTRAINT_SPECS.REBATES in self.general_settings else False
        price_comp_flag = 'price_component' in self.general_settings and self.general_settings['price_component'][0] != 'Total Price'

        # Initialize the model builder and build the basic
        self.builder = ModelBuilder(matrices, mapping_dic, self.solver, self.params, manual_alloc)
        self.builder.create_model()
        self.builder.create_base_variables(split_items)
        self.builder.add_base_constraints(partial_allocation)

        # Compute relevant structures for price components
        if len(self.offers.header[KEYS.PRICE]) != 0:
            self.price_comp_to_id = create_price_comp_mapping(self.offers)
            self.price_comp_array = get_price_comp_matrix(self.offers, mapping_dic['supplier_to_id'], mapping_dic['demands_list'], mapping_dic['item_type_to_id'])
        # Set the base objective
        if price_comp_flag:
            self.price_comps = [self.offers.header.byname[price_comp].id for price_comp in self.general_settings['price_component']] 
            objective = self.builder.price_comp_base_objective(self.price_comps,
                                                               self.price_comp_array,
                                                               self.price_comp_to_id)
        else:
            objective = self.builder.base_total_spend()

        # Update the objective w.r.t. partial allocations
        if partial_allocation:
            partial_penalties = get_partial_alloc_penalties(matrices['costs'], matrices['demanded_volumes'])
            objective = objective + self.builder.add_partial_regularizer(partial_penalties)

        # Set the total spend
        self.builder.total_spend = self.builder.base_total_spend

        # Apply rebates if necessary
        if self.discounts is not None and rebates_applied:
            objective = self.apply_rebates(objective, mapping_dic, price_comp_flag, self.general_settings, matrices['costs'])

        # Set the objective in the model
        self.builder.set_objective(objective)

        # Apply capacities if necessary
        if self.capacities is not None and CONSTRAINT_SPECS.CAPACITIES in self.general_settings and self.general_settings[CONSTRAINT_SPECS.CAPACITIES]:
            capacities_mapping = create_capacities_mappings(self.capacities)
            capacities_matrices = get_capacities(self.offers,
                                                 self.capacities,
                                                 mapping_dic['supplier_to_id'],
                                                 mapping_dic['item_type_to_id'],
                                                 capacities_mapping['id_to_capacity'],
                                                 mapping_dic['suppliers_list'],
                                                 mapping_dic['id_to_supplier'],
                                                 mapping_dic['demands_list'])
            # self.model.add_capacities_constraints(capacities_matrices, capacities_mappings)
            self.builder.add_capacities_constraints(capacities_mapping, capacities_matrices)

        self.matrices = matrices
        self.mapping_dic = mapping_dic

        # Apply all the user defined constraints
        self.add_constraints()

        # TODO: remove when tables are in DB
        

        # Get the created model
        self.model = self.get_model_wrapper()

    def apply_rebates(self, objective, mapping_dic, price_comp_flag, general_settings, costs) -> LinExpr:
        """
        Dummy method that shows how the discount buckets would be passed around.
        """
        discount_buckets = create_discount_buckets(self.discounts,
                                                   self.offers,
                                                   mapping_dic)

        discount_buckets = compute_item_discounts(discount_buckets,
                                                  costs,
                                                  self.price_comp_array,
                                                  self.price_comp_to_id)

        discount_buckets, nbr_rebates = compute_rebates_per_supplier(discount_buckets,
                                                                     len(mapping_dic['supplier_to_id']))

        self.discount_buckets = discount_buckets
        self.nbr_rebates = nbr_rebates

        self.builder.create_discounts_variables(nbr_rebates)
        self.builder.add_discounts_constraints(discount_buckets)
        self.builder.total_spend = lambda tuples: (self.builder.base_total_spend(tuples) +
            self.builder.subtract_discounts(discount_buckets, tuples))

        if price_comp_flag:
            objective = objective + self.builder.subtract_price_comp_discounts(discount_buckets,
                                                                                  self.price_comps,
                                                                                  self.price_comp_array,
                                                                                  self.price_comp_to_id)
        else:
            objective = objective + self.builder.subtract_discounts(discount_buckets)
               
        return objective

    def add_constraints(self):
        """
        Add all constraints not related to the general settings
        """
        if CONSTRAINT_TYPES.LIMIT_SPEND in self.params.keys():
            for constraints_dic in self.params[CONSTRAINT_TYPES.LIMIT_SPEND]:                    
                if constraints_dic[UNITS.UNIT] == UNITS.VOLUME:
                    self.builder.add_volume_constraint(constraints_dic)
                elif constraints_dic[UNITS.UNIT] == UNITS.PERCENTAGE:
                    self.builder.add_percentage_constraint(constraints_dic)
                elif constraints_dic[UNITS.UNIT] == UNITS.SPEND:
                    self.builder.add_spend_constraint(constraints_dic)

        if CONSTRAINT_TYPES.NUMBER_OF_SUPPLIERS in self.params.keys():
            for constraints_dic in self.params[CONSTRAINT_TYPES.NUMBER_OF_SUPPLIERS]:
                self.builder.add_supplier_constraint(constraints_dic)

        if CONSTRAINT_TYPES.MULTI_SPLITS in self.params.keys():
            for constraints_dic in self.params[CONSTRAINT_TYPES.MULTI_SPLITS]:
                self.builder.add_item_level_constraint(constraints_dic)

        if CONSTRAINT_TYPES.VOLUME_PER_SUPPLIER in self.params.keys():
            for constraints_dic in self.params[CONSTRAINT_TYPES.VOLUME_PER_SUPPLIER]:
                self.builder.add_volume_per_supplier_constraint(constraints_dic)

        general_settings = self.params['general_settings']

        if CONSTRAINT_SPECS.INCUMBENT in general_settings and general_settings[CONSTRAINT_SPECS.INCUMBENT] and self.baseline_tuples is not None:
            if general_settings[CONSTRAINT_SPECS.INCUMBENT][0] == CONSTRAINT_SPECS.ITEM_INCUMBENCY:
                self.builder.add_item_incumbency(self.baseline_tuples)
            if general_settings[CONSTRAINT_SPECS.INCUMBENT][0] == CONSTRAINT_SPECS.GENERAL_INCUMBENCY:
                self.builder.add_general_incumbency(self.baseline_tuples)

        if CONSTRAINT_TYPES.EXCLUDED_SUPPLIERS in self.params:
            for constraints_dic in self.params[CONSTRAINT_TYPES.EXCLUDED_SUPPLIERS]:
                self.builder.exclude_suppliers(constraints_dic, self.mapping_dic['soft_constraint_to_id'])


        if CONSTRAINT_SPECS.ITEM_TYPE in general_settings and general_settings[CONSTRAINT_SPECS.ITEM_TYPE]:
            self.builder.add_item_type_constraint(self.mapping_dic['item_type_to_id'], general_settings[CONSTRAINT_SPECS.ITEM_TYPE])

    def get_model_wrapper(self):
        """
        Return the model built by the ModelBuilder
        """
        return self.builder.get_wrapper()


    def run(self) -> TableData:
        """ Run the optimization and return the allocations"""
        start_time = datetime.now()
        self.model.optimize()
        slogger.info('Optimizing the model took: ' + str((datetime.now() - start_time).total_seconds()))
        start_time = datetime.now()
        if not self.model.check_status():
            slogger.warning("Model optimization didn't finish successfully")
        allocations = self.export_allocation_data()

        slogger.info('Exporting allocations took: ' + str((datetime.now() - start_time).total_seconds()))
        return allocations


    def export_allocation_data(self) -> TableData:
        """
        This function exports the allocations data to the state allocations table.
        """
        self.allocations = self.builder.allocations()
        manual_allocations = np.array(self.builder.manual_alloc)
        self.item_discounts = None
        rebates = self.general_settings[CONSTRAINT_SPECS.REBATES] if CONSTRAINT_SPECS.REBATES in self.general_settings else False
        rebates = self.discounts is not None and rebates
        if rebates:
            self.supplier_discount = self.builder.supplier_discount()
            self.item_discounts = self.builder.item_discounts().astype('float')

            for bucket in self.discount_buckets:
                i = bucket.supplier_id
                l = bucket.order
                if bucket.discount_type == 'flat':
                    total_spend_bucket = sum(self.allocations[i, j, k]*self.matrices['costs'][i, j, k] for (j, k) in bucket.contributing_to)
                    for (j, k) in bucket.contributing_to:
                        self.item_discounts[i, j, k, l] = self.supplier_discount[i, l]*self.allocations[i, j, k]*(self.matrices['costs'][i, j, k]/total_spend_bucket)*bucket.discount_amount/self.matrices['demanded_volumes'][j]
                else:
                    for j, k in bucket.discounted_by:
                        self.item_discounts[i, j, k, l] = self.item_discounts[i, j, k, l]*bucket.item_discounts[(j, k)]

        allocations_table = create_allocations_table(self.allocations,
                                                     manual_allocations,
                                                     self.matrices['demanded_volumes'],
                                                     self.offers,
                                                     self.demands,
                                                     self.mapping_dic,
                                                     self.item_discounts,
                                                     self.discounts)

        return allocations_table

    def create_discounts_output(self, discounts: TableData) -> Dict:
        """
        Create a dictionary to display information about the reached discounts
        """

        output_df = pd.DataFrame(columns=['supplier',
                                          'bundle',
                                          'target_value',
                                          'unit',
                                          'reached_value',
                                          'total_discount'])
        # calculate the summary row for each bucket
        for bucket in self.discount_buckets:
            info = self.calculate_bucket_info(bucket)
            output_df = output_df.append(info, ignore_index=True)

        # keep only relevant discount for multi-level ones
        output_df = self.simplify_multi_bundles(output_df)

        # update discounts table to merge with description, threshold and discount amount
        discounts[UKEYS.BUCKET_ID] = discounts[UKEYS.BUCKET_ID].str.replace(r'_\d$', '')
        output_df = output_df.merge(discounts[[UKEYS.BUCKET_ID, UKEYS.DESCRIPTION, UKEYS.THRESHOLD]],
                                    how='left',
                                    left_on=['bundle', 'target_value'],
                                    right_on=[UKEYS.BUCKET_ID, UKEYS.THRESHOLD])

        # use description to be displayed whenever possible                          
        output_df['output_bundle'] = output_df.apply(lambda x: x[UKEYS.DESCRIPTION] if x[UKEYS.DESCRIPTION] is not None else x['bundle'],
                                                     axis=1)
        # remove and rename columns to meet output format
        output_df.drop(columns=['bundle', UKEYS.BUCKET_ID, UKEYS.DESCRIPTION, UKEYS.THRESHOLD], inplace=True)
        output_df.rename(columns={'output_bundle': 'bundle'}, inplace=True)

        return {'discounts': output_df.to_dict(orient='records')}


    def calculate_bucket_info(self, bucket: DiscountBucket) -> Dict:
        """
        For the given discount bucket calculate the output information

        Args:
            - bucket: the input discount bucket

        Returns:
            - Dict: {
                        'supplier': <supplier name>
                        'bundle': <bundle name>
                        'target_vale': <bucket threshold>
                        'unit': <discount unit>
                        'reached_value': <allocated volume or spend for items in this bucket>
                        'total_discount': <total discount achieved>
                    }

        """
        i = bucket.supplier_id
        l = bucket.order
        dem_vols = self.matrices['demanded_volumes']
        costs = self.matrices['costs']

        if bucket.unit == 'volume':
            reached = sum(self.allocations[i, j, k]*dem_vols[j]
                          for (j, k) in bucket.contributing_to)
        elif bucket.unit == 'spend':
            reached = sum(self.allocations[i, j, k]*dem_vols[j]*costs[i, j, k]
                            for (j, k) in bucket.contributing_to)

        total_discount = sum(self.item_discounts[i, j, k, l]*dem_vols[j]
                                for (j, k) in bucket.discounted_by)
        unit_discount = bucket.discount_amount if total_discount > 0 else 0

        info = {
            'supplier': self.mappings['id_to_supplier'][bucket.supplier_id],
            'bundle': bucket.bucket_id,
            'target_value': bucket.threshold,
            'unit': bucket.unit,
            'type': bucket.discount_type,
            'reached_value': reached,
            'total_discount': total_discount,
            'discount_amount': unit_discount
        }
        return info

    def simplify_multi_bundles(self, output_df: pd.DataFrame) -> pd.DataFrame:
        """
        Convert the given dataframe with multi bundles to one with only one level per bucket.
        """
        mask = output_df['bundle'].str.contains('_\d$')
        if not any(mask):
            return output_df
        # remove _N from bundle names that were added internally in optimizer preprocessor
        output_df.loc[mask, 'bundle'] = output_df.loc[mask, 'bundle'].str.replace(r'_\d$', '')

        # loop over multi-level discounts and keep relevant level
        multi_bundles = output_df.loc[output_df['bundle'].duplicated(keep=False), 'bundle'].unique()
        for bundle in multi_bundles:
            mask = output_df['bundle'] == bundle
            relevant_bundle_mask = output_df.loc[mask, 'target_value'] < output_df.loc[mask, 'reached_value']

            # set default relevant to first level
            relevant_bundle = relevant_bundle_mask.index[0]

            # if any discount was reached, sum reached discounts in highest one
            if any(relevant_bundle_mask):
                relevant_bundle = relevant_bundle_mask.where(relevant_bundle_mask==True).last_valid_index()
                summed_discount = sum(output_df.loc[(mask & relevant_bundle_mask), 'total_discount'])
                output_df.loc[relevant_bundle, 'total_discount'] = summed_discount

            # drop discount levels that are not relevant
            drop_indices = list(relevant_bundle_mask.index)
            drop_indices.remove(relevant_bundle)
            output_df = output_df.drop(drop_indices)
        return output_df
