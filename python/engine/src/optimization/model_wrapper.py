from abc import ABC, abstractmethod
from pathlib import Path

from shared.constants import VAR_TYPES

class ModelWrapper(ABC):

    @abstractmethod
    def add_constraint(self, constraint, name_constraint: str = ''):
        pass

    @abstractmethod
    def sum(self, expression): # TODO : not sure about the typing
        pass

    @abstractmethod
    def set_objective(self, objective, sense: int): # TODO : not sure about the typing
        pass

    @abstractmethod
    def add_variable(self, name: str, type: str = VAR_TYPES.BINARY):
        pass

    @abstractmethod
    def optimize(self):
        pass

    @abstractmethod
    def check_status(self):
        pass

    @abstractmethod
    def get_variable(self, variable): # TODO : not sure about the typing
        pass

    @abstractmethod
    def get_variable_by_name(self, name: str):
        pass

    @abstractmethod
    def read(self, path: Path):
        pass

    @abstractmethod
    def write(self, path: Path):
        pass