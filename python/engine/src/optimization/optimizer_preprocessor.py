"""
File containing all the functions used to create the matrices and mappings used
to create the optimization problem
"""

from collections import defaultdict
from typing import Dict
from typing import List
from typing import AbstractSet
from typing import Tuple

import numpy as np
from numpy import NaN
import pandas as pd

from shared.constants import UKEYS, KEYS
from shared.entities import DiscountBucket
from shared.entities import TableData
from shared.entities import TableHeader

from shared.constants import RecordedError

def add_demand_int_id(demands: TableData,
                      offers: TableData,
                      baseline_offers: TableData) -> Tuple[TableData, TableData, TableData]:
    """
    Add an additional integer demand id to the offers and demands table.
    """
    # Define id adder function for the demands table
    demands_header = demands.header

    int_id_df = demands.data.reset_index()
    demands_header.add_column(UKEYS.DEMAND_INT_ID, UKEYS.DEMAND_INT_ID, '')
    demands[demands_header[UKEYS.DEMAND_INT_ID]] = int_id_df.index
    demands.header = demands_header

    demand_to_int_id = dict(zip(demands[demands_header[UKEYS.DEMAND_ID]], demands[demands_header[UKEYS.DEMAND_INT_ID]]))
    

    offers.header.add_column(UKEYS.DEMAND_INT_ID)
    offers[offers.header[UKEYS.DEMAND_INT_ID]] = offers.data.apply(lambda x: demand_to_int_id[x[offers.header[UKEYS.DEMAND_ID]]], axis=1)

    if baseline_offers is not None:
        baseline_offers.header.add_column(UKEYS.DEMAND_INT_ID)
        baseline_offers[baseline_offers.header[UKEYS.DEMAND_INT_ID]] = baseline_offers.data.apply(lambda x: demand_to_int_id[x[baseline_offers.header[UKEYS.DEMAND_ID]]], axis=1)

    return demands, offers, baseline_offers

def create_basic_mappings(demands: TableData, offers: TableData, filters: List[str], project_settings) -> dict:
    """
    Create the sorted demands and suppliers lists, with mappings from names to indexes, and reverse mappings

    Returns:
        Dictionary : "demands_list" -> List
                     "suppliers_list" -> List
                     "supplier_to_id" -> Dictionary
                     "id_to_supplier" -> Dictionary
                     "item_to_id" -> Dictionary
                     "id_to_item" -> Dictionary
                     "category_to_item" -> Dictionary
    """
    demands_header = demands.header
    offers_header = offers.header

    demands_list = sorted(demands[demands_header[UKEYS.DEMAND_INT_ID]].unique())
    suppliers_list = sorted(list(offers[offers_header[UKEYS.SUPPLIER]].unique()))

    supplier_to_id = dict(zip(suppliers_list, range(len(suppliers_list))))
    id_to_supplier = {value: key for key, value in supplier_to_id.items()}

    item_to_id = dict(zip(demands_list, range(len(demands_list))))
    id_to_item = {value: key for key, value in item_to_id.items()}

    item_name_to_id = dict(map(tuple, demands[demands_header[[UKEYS.ITEM_ID, UKEYS.DEMAND_INT_ID]]].values))
    id_to_item_name = {value: key for key, value in item_name_to_id.items()}

    offers_df = offers[offers_header[[UKEYS.SUPPLIER, UKEYS.DEMAND_INT_ID, UKEYS.ITEM_CATEGORY]]]
    filters_df = offers[filters]

    item_type_list = sorted(offers[offers_header[UKEYS.ITEM_CATEGORY]].unique())
    item_type_to_id = dict(zip(item_type_list, range(len(item_type_list))))
    id_to_item_type = {value: key for key, value in item_type_to_id.items()}

    soft_const_to_id = {offers_header.byid[col_id].original_name: counter for counter, col_id in enumerate(project_settings.soft_constraints)}
    id_to_soft_const = {v: k for k, v in soft_const_to_id.items()}

    category_to_item = defaultdict(lambda: {})
    category_cols = zip(filters, [demands_header.byid[col_id].original_name for col_id in filters])

    for (category_col, category_name) in category_cols:
        category_to_item[category_name]['All'] = []
        for category_val in offers[category_col].unique():
            df = offers_df[filters_df[category_col] == category_val]
            category_to_item[category_name][category_val] = set(df.itertuples(index=False, name=None))
            category_to_item[category_name]['All'] += category_to_item[category_name][category_val]

    return {'demands_list': demands_list,
            'suppliers_list': suppliers_list,
            'supplier_to_id': supplier_to_id,
            'id_to_supplier': id_to_supplier,
            'item_to_id': item_to_id,
            'id_to_item': id_to_item,
            'item_name_to_id': item_name_to_id,
            'id_to_item_name': id_to_item_name,
            'category_to_item': category_to_item,
            'item_type_list': item_type_list,
            'item_type_to_id': item_type_to_id,
            'id_to_item_type': id_to_item_type,
            'soft_constraint_to_id':soft_const_to_id,
            'id_to_soft_constraint':id_to_soft_const}


def get_basic_matrices(offers, demands, mapping_dic, project_settings):
    """
    Get the costs, offered volumes and demanded volumes dataframes
    """
    demands_list = mapping_dic['demands_list']
    matrices = {}
    matrices['costs'] = get_costs(offers, demands_list, mapping_dic['supplier_to_id'], mapping_dic['item_type_to_id'])
    matrices['demanded_volumes'] = get_demanded_volumes(demands, demands_list)
    matrices['volumes'] = get_offered_volumes(offers,
                                              demands_list,
                                              mapping_dic['supplier_to_id'],
                                              mapping_dic['item_type_to_id'])
    matrices['soft_constraints'] = get_soft_constraints(offers,
                                                        demands_list,
                                                        mapping_dic['supplier_to_id'],
                                                        mapping_dic['item_type_to_id'],
                                                        project_settings.soft_constraints)
    return matrices


def create_baseline_tuples(baseline_offers, mapping_dic):
    """
    Create a list of tuples matching the baseline offers to apply incumbency
    """
    header = baseline_offers.header
    result = []
    def add_offer_tuple(row):
        try:
            result.append((mapping_dic['supplier_to_id'][row[header[UKEYS.SUPPLIER]]],
                           row[header[UKEYS.DEMAND_INT_ID]],
                           None))
        except:
            pass

    baseline_offers.data.apply(add_offer_tuple, axis=1)
    return result


def get_costs(offers: TableData,
              demands_list: list,
              supplier_to_index: Dict[str, int],
              item_type_to_index: Dict[str, int]) -> np.ndarray:
    """
    # Extract the total costs per item per supplier from the offers table

    Returns:
        Numpy 3D Array : shape = [n_suppliers, n_items, n_item_types]
    """
    header = offers.header

    costs = offers.get_3d_crosstab(header[UKEYS.NORMALIZED_PRICE],
                                   supplier_to_index,
                                   demands_list,
                                   item_type_to_index)

    costs[costs == 0] = -1
    return costs

def get_soft_constraints(offers: TableData,
                         demands_list: List[int],
                         supplier_to_index: Dict[str, int],
                         item_type_to_index: Dict[str, int],
                         custom_columns: List[str]) -> np.ndarray:
    """
    # Extract the value of the soft constraints for each item/supplier/item type combination    
    """
    soft_constraints = offers.get_4d_crosstab(custom_columns,
                                              supplier_to_index,
                                              demands_list,
                                              item_type_to_index)
    return soft_constraints

def get_offered_volumes(offers: TableData,
                        demands_list: list,
                        supplier_to_index: Dict[str, int],
                        item_type_to_index: Dict[str, int]) -> np.ndarray:
    """
    Extract the offered volume per item per supplier from the offers table

    Returns:
        Numpy 3D Array : shape = [n_suppliers, n_items, n_item_types]
    """
    header = offers.header
    volumes = offers.get_3d_crosstab(header[UKEYS.TOTAL_VOLUME],
                                     supplier_to_index,
                                     demands_list,
                                     item_type_to_index)
    return volumes


def get_demanded_volumes(demands: TableData, demands_list: list) -> np.ndarray:
    """
    Extract the demanded volume per item per from the demands table

    Returns:
        Numpy 1D Array : shape = [n_items]
    """
    header = demands.header
    demands_df = demands.data
    demands_df = demands_df.set_index(header[UKEYS.DEMAND_INT_ID], inplace=False)
    demands_df = demands_df.reindex(demands_list)
    volumes = demands_df[header[UKEYS.TOTAL_VOLUME]].to_numpy().astype('float')
    return volumes


def create_price_comp_mapping(offers: TableData):
    """
    Create the mapping for the price components

    Args:
        offers: the offers table

    Returns:
        dictionary of price component to index
    """
    header = offers.header
    return {price_comp:index for index, price_comp in enumerate(header[KEYS.PRICE])}


def get_price_comp_matrix(offers, supplier_to_index: dict, demands_list: list, item_type_to_index: dict) -> np.ndarray:
    """
    Get the price components matrix
    """
    header = offers.header

    price_comps = offers.get_4d_crosstab(header[KEYS.PRICE],
                                         supplier_to_index,
                                         demands_list,
                                         item_type_to_index)
    price_comps[price_comps == 0] = -1
    return price_comps

def transform_incremental_buckets(discounts: TableData) -> Tuple[TableData, Dict]:
    """
    Transform the buckets with multiple thresholds to a set of unique buckets
    that will be applied on top of each other.

    Returns:
        - modified discounts table
        - dictionary: original bucket_name -> list[new_bucket_name1, ...]
    """
    header = discounts.header
    groups = discounts.data.groupby(header[UKEYS.BUCKET_ID])
    result = pd.DataFrame()
    mapping = dict()
    for name, group in groups:
        if len(group) == 1:
            result = pd.concat([result, group], ignore_index=True)
            continue
        # Set result columns correctly
        sorted_group = group.sort_values(header[UKEYS.THRESHOLD])
        suffix_list = ['_' + str(i) for i in range(len(sorted_group))]
        diffs_list = sorted_group[header[UKEYS.DISCOUNT_AMOUNT]].diff()
        diffs_list.iloc[0] = sorted_group[header[UKEYS.DISCOUNT_AMOUNT]].iloc[0]
        sorted_group[header[UKEYS.BUCKET_ID]] = sorted_group[header[UKEYS.BUCKET_ID]] + suffix_list
        sorted_group[header[UKEYS.DISCOUNT_AMOUNT]] = diffs_list
        # Add everything to output structures
        new_names = list(sorted_group[header[UKEYS.BUCKET_ID]])
        mapping[name] = new_names
        result = pd.concat([result, sorted_group], ignore_index=True)
    discounts.data = result
    discounts.trim()
    return discounts, mapping


def add_discount_bucket(row,
                        discounts_header: TableHeader,
                        offers_header: TableHeader,
                        supplier_to_id: Dict,
                        discounted_by: Dict[str, AbstractSet],
                        contributing_to: Dict[str, AbstractSet],
                        result: List[DiscountBucket]) -> None:
    """
    create a discount bucket from a row of the discounts table
    and append it to result.
    """
    bucket = DiscountBucket(row[discounts_header[UKEYS.BUCKET_ID]])
    bucket.threshold       = row[discounts_header[UKEYS.THRESHOLD]]
    bucket.discount_amount = row[discounts_header[UKEYS.DISCOUNT_AMOUNT]]
    bucket.discount_type   = row[discounts_header[UKEYS.DISCOUNT_TYPE]]
    bucket.supplier_id     = supplier_to_id[row[discounts_header[UKEYS.SUPPLIER]]]
    bucket.discounted_by   = list(discounted_by[bucket.supplier_id][bucket.bucket_id])
    bucket.contributing_to = list(contributing_to[bucket.supplier_id][bucket.bucket_id])
    bucket.unit            = row[discounts_header[UKEYS.DISCOUNT_UNIT]]
    bucket.price_component = row[discounts_header[UKEYS.PRICE_COMPONENT]]
    result.append(bucket)


def compute_items_per_bucket(item_to_buckets: Dict, bucket_mapping: Dict, supplier_to_id: Dict[str, int]) -> Dict:
    """
    Given a dictionary item_id -> buckets
    Compute the relation: bucket_id -> list
    """
    result = defaultdict(lambda: defaultdict(set))
    for (supplier, item_id, item_type), buckets in item_to_buckets.items():
        if len(buckets) == 0:
            continue
        for bucket in buckets:
            supplier_id = supplier_to_id[supplier]
            to_add = [bucket]
            # make sure to include all the new names to add
            if bucket in bucket_mapping:
                to_add = bucket_mapping[bucket]
            for name in to_add:
                result[supplier_id][name] = result[supplier_id][name] | set([(item_id, item_type)])
    return result


def create_discount_buckets(discounts: TableData,
                            offers: TableData,
                            mapping_dic: Dict) -> List[DiscountBucket]:
    """
    Create a list of discount bucket definitions.
    """
    # Get header and UKEYS
    discounts_header = discounts.header
    offers_header = offers.header
    demand_key = offers_header[UKEYS.DEMAND_INT_ID]
    item_type_key = offers_header[UKEYS.ITEM_CATEGORY]
    contrib_key = offers_header[UKEYS.CONTRIBUTING_TO]
    discounted_key = offers_header[UKEYS.DISCOUNTED_BY]
    supplier_key = offers_header[UKEYS.SUPPLIER]

    # get the items contributing per bucket.
    supplier_to_id = mapping_dic['supplier_to_id']
    item_type_to_id = mapping_dic['item_type_to_id']
    buckets_per_item = offers[[supplier_key, demand_key, item_type_key, contrib_key, discounted_key]]
    buckets_per_item[item_type_key] = buckets_per_item[item_type_key].replace(item_type_to_id)
    buckets_per_item = buckets_per_item.set_index([supplier_key, demand_key, item_type_key]).to_dict()

    discounts, bucket_mapping = transform_incremental_buckets(discounts)
    items_discounted_per_rebate = compute_items_per_bucket(buckets_per_item[discounted_key],
                                                           bucket_mapping,
                                                           supplier_to_id)
    items_contributing_per_rebate = compute_items_per_bucket(buckets_per_item[contrib_key],
                                                             bucket_mapping,
                                                             supplier_to_id)
    result = list()
    discounts.data.apply(lambda row: add_discount_bucket(row,
                                                         discounts_header,
                                                         offers_header,
                                                         mapping_dic['supplier_to_id'],
                                                         items_discounted_per_rebate,
                                                         items_contributing_per_rebate,
                                                         result),
                         axis=1)

    return result


def compute_item_discounts(bucket_list: List[DiscountBucket],
                           costs: np.ndarray,
                           price_components_array: np.ndarray,
                           price_comp_to_id: Dict[str, str]) -> List[DiscountBucket]:
    """
    Add the specific discount amounts for each item in this bucket
    based on the discount unit
    """
    result = []
    for bucket in bucket_list:
        i = bucket.supplier_id
        if bucket.discount_type == 'percentage':
            if bucket.price_component is not None and bucket.price_component not in ['None', 'nan']:
                if price_components_array is None:
                    raise RecordedError('Optimization on non existing price component')
                bucket.item_discounts = {(j, k): (price_components_array[i, j, k][price_comp_to_id[bucket.price_component]]\
                                                *bucket.discount_amount*0.01) for (j, k) in bucket.discounted_by}
            else:
                bucket.item_discounts = {(j, k): costs[i, j, k]*bucket.discount_amount*0.01 for (j, k) in bucket.discounted_by}
        else:
            bucket.item_discounts = {(j, k): bucket.discount_amount for j, k in bucket.discounted_by}
        result.append(bucket)

    return result


def compute_rebates_per_supplier(bucket_list: List[DiscountBucket],
                                 nb_suppliers: int) -> Tuple[List[DiscountBucket], List[int]]:
    """
    Compute a list that indicates for each supplier how many discounts buckets it has
    and add an order to each bucket.
    """
    result = [0 for _ in range(nb_suppliers)]
    new_bucket_list = []
    for bucket in bucket_list:
        bucket.order = result[bucket.supplier_id]
        result[bucket.supplier_id] += 1
        new_bucket_list.append(bucket)
    return new_bucket_list, result


def create_capacities_mappings(capacities: TableData) -> dict:
    """
    Create the lists and mappings necessary for the capacities

    Returns:
        capacity_to_id: dictionary
        id_to_capacity: dictionary
    """
    cap_header = capacities.header
    capacity_to_id = {cap_name: counter for counter, cap_name in enumerate([cap_header.byid[col_id].original_name for col_id in cap_header[KEYS.CAPACITY]])}
    id_to_capacity = {v:k for k,v in capacity_to_id.items()}
    return {'capacity_to_id': capacity_to_id,
            'id_to_capacity': id_to_capacity}

def add_min_unit_price(offers):
    """
    Some new fields are used by the optimization algorithm, initialize them
    here.
    """
    # TO DO: check whether this field is still necessary when post processing will happen mostly in front end
    header = offers.header

    # Get indices of minimum unit price offers for each demand ID
    offers.data[header[UKEYS.NORMALIZED_PRICE]] = offers.data[header[UKEYS.NORMALIZED_PRICE]].apply(lambda x: float(x) if not isinstance(x, str) else 0)
    idx = offers[header[[UKEYS.DEMAND_INT_ID, UKEYS.NORMALIZED_PRICE]]].groupby([header[UKEYS.DEMAND_INT_ID]])[header[UKEYS.NORMALIZED_PRICE]].idxmin()

    if any(idx == NaN):
        idx = offers[header[[UKEYS.DEMAND_INT_ID, UKEYS.RAW_PRICE]]].groupby([header[UKEYS.DEMAND_INT_ID]])[header[UKEYS.RAW_PRICE]].idxmin()

    # Select the columns whose data we want to add to the allocations table
    keys = header[[UKEYS.DEMAND_INT_ID, UKEYS.NORMALIZED_PRICE]]

    header.add_column(UKEYS.MIN_UNIT_PRICE)
    min_allocations = offers.data.iloc[idx][keys].copy()
    min_allocations.rename(
        columns={header[UKEYS.NORMALIZED_PRICE]: header[UKEYS.MIN_UNIT_PRICE]}, inplace=True)

    # Use the lowest unit price determined by the allocation table and add
    # it to the offers table for the computation of bucket potentials
    offers.data = pd.merge(left=offers.data,
                           right=min_allocations,
                           left_on=header[UKEYS.DEMAND_INT_ID],
                           right_on=header[UKEYS.DEMAND_INT_ID],
                           how='inner')

    return offers

def get_capacities(offers: TableData,
                   capacities: TableData,
                   supplier_to_id: Dict[int, str],
                   item_type_to_id: Dict[int, str],
                   id_to_capacity: Dict[int, str],
                   suppliers_list,
                   id_to_supplier,
                   demands_list: List[str]):
    """
    Extract the capacity matrices from the offers
    """
    header = offers.header
    capacity_names = [header.byname[cap_name].id for cap_name in id_to_capacity.values()]
    capacity = offers.get_4d_crosstab(capacity_names, supplier_to_id, demands_list, item_type_to_id)
    capacity[np.isnan(capacity)] = 0
    max_capacities = get_max_capacities(capacities, suppliers_list, id_to_supplier, id_to_capacity)

    return {'capacities': capacity,
            'max_capacities': max_capacities}

def get_max_capacities(capacities, suppliers_list, id_to_supplier, id_to_capacity):
    """
    Get the maximum available capacities from the capacities table
    """
    cap_header = capacities.header
    n_suppliers = len(suppliers_list)

    n_capacities = len(id_to_capacity)
    capacities.data = capacities.data.fillna(0)
    capacities.trim()
    max_capacities = np.zeros(shape=(n_suppliers, n_capacities))
    for index_supplier in range(n_suppliers):
        for index_capacity in range(n_capacities):
            max_capacities[index_supplier, index_capacity] = capacities[cap_header.byname[id_to_capacity[index_capacity]].id]\
                                    [capacities[cap_header[UKEYS.SUPPLIER]] == id_to_supplier[index_supplier]].values[0]

    # max_capacities[np.isnan(max_capacities)] = 0

    return max_capacities


def get_partial_alloc_penalties(costs, demanded_volumes):
    """
    Compute the penalties that'll be used for the partial allocations
    """
    costs[costs == -1] = NaN

    means = np.nanmean(costs, axis=(0, 2))

    means[np.where(np.isnan(means))] = np.nanmax(means)*10 if not np.isnan(means).all() else 1e8

    penalties = (means*demanded_volumes)*10

    return penalties
        

def create_manual_alloc_array(allocations: TableData,
                              offers: TableData,
                              mapping_dic: dict) -> np.ndarray:
    """
    Create a 3D numpy array matching the shape of the allocations matrix that 
    indicates whether or not a certain offer has been manually enforced
    """

    manual_alloc = np.zeros((len(mapping_dic['suppliers_list']), len(mapping_dic['demands_list']), len(mapping_dic['item_type_list'])))

    if allocations is None:
        return manual_alloc

    supplier_to_id = mapping_dic['supplier_to_id']
    item_type_to_id = mapping_dic['item_type_to_id']

    offers_header = offers.header
    allocations_header = allocations.header

    offers_data = offers.data
    allocations_data = allocations.data

    df = offers_data.merge(allocations_data, how='right', left_on=offers_header[UKEYS.OFFER_ID], right_on=allocations_header[UKEYS.OFFER_ID])

    df = df[df['is_manual'] == True]

    def fill(row):
        manual_alloc[supplier_to_id[row[offers_header[UKEYS.SUPPLIER]]], row[offers_header[UKEYS.DEMAND_INT_ID]], item_type_to_id[row[offers_header[UKEYS.ITEM_CATEGORY]]]] = 1

    df.apply(fill, axis=1)

    return manual_alloc


def create_allocations_table(allocations: np.ndarray,
                             manual_alloc: np.ndarray,
                             demanded_volumes: np.ndarray,
                             offers: TableData,
                             demands: TableData,
                             mapping_dic: dict,
                             item_discounts: np.ndarray,
                             discounts: TableData) -> TableData:
    """
    Generate the allocations table from the matrices returned by the optimizer
    """
    offers_header = offers.header

    id_to_supplier = mapping_dic['id_to_supplier']
    id_to_item = mapping_dic['id_to_item_name']
    id_to_item_type = mapping_dic['id_to_item_type']

    indices_allocations = [tuple(elmt) for elmt in np.argwhere(allocations != 0)]

    vol_allocations = allocations * demanded_volumes[np.newaxis, :, np.newaxis]

    allocations_df = pd.DataFrame(columns=offers_header[[UKEYS.DEMAND_INT_ID, UKEYS.SUPPLIER, UKEYS.ITEM_CATEGORY, UKEYS.TOTAL_VOLUME]] + ['allocations'])

    for counter, index_alloc in enumerate(indices_allocations):
        allocations_df.loc[counter] = [index_alloc[1],
                                       id_to_supplier[index_alloc[0]],
                                       id_to_item_type[index_alloc[2]],
                                       vol_allocations[index_alloc],
                                       allocations[index_alloc]]

    merge_cols = [UKEYS.DEMAND_INT_ID, UKEYS.SUPPLIER, UKEYS.ITEM_CATEGORY]

    cols_kept = [UKEYS.NORMALIZED_PRICE, UKEYS.MIN_UNIT_PRICE, UKEYS.OFFER_ID, UKEYS.DEMAND_ID]

    allocations_df = allocations_df.merge(offers[offers_header[cols_kept + merge_cols]],
                                          on=offers_header[merge_cols],
                                          how='left')

    allocations_header = TableHeader()

    # add_dummy_col = lambda key: allocations_header.add_column(key, key, '', col_id=offers_header[key])
    add_dummy_col = lambda key: allocations_header.add_column(key, offers_header[key], key)

    any(add_dummy_col(key) for key in merge_cols + cols_kept + [UKEYS.TOTAL_VOLUME])

    allocations_header.add_column(UKEYS.UNIT_PRICE, UKEYS.UNIT_PRICE, '')
    allocations_df[allocations_header[UKEYS.UNIT_PRICE]] = allocations_df[allocations_header[UKEYS.NORMALIZED_PRICE]]

    def check_if_manual(row):
        id_supplier = mapping_dic['supplier_to_id'][row[allocations_header[UKEYS.SUPPLIER]]]
        id_item = row[allocations_header[UKEYS.DEMAND_INT_ID]]
        item_type_id = mapping_dic['item_type_to_id'][row[allocations_header[UKEYS.ITEM_CATEGORY]]]
        return bool(manual_alloc[id_supplier][id_item][item_type_id])

    allocations_df['is_manual'] = allocations_df.apply(check_if_manual, axis=1)
    
    allocations_header.add_column(UKEYS.DISCOUNT_AMOUNT, UKEYS.DISCOUNT_AMOUNT, '')
    allocations_df[allocations_header[UKEYS.DISCOUNT_AMOUNT]] = 0
    if item_discounts is not None:
        allocations_df = allocations_df.merge(offers[offers_header[[UKEYS.SUPPLIER, UKEYS.DEMAND_INT_ID, UKEYS.ITEM_CATEGORY, UKEYS.CONTRIBUTING_TO, UKEYS.DISCOUNTED_BY]]],
                                              on=offers_header[merge_cols])
        any(add_dummy_col(key) for key in [UKEYS.DISCOUNTED_BY, UKEYS.CONTRIBUTING_TO])

        discounts_matrix = np.sum(item_discounts, axis=3)

        indices_discounts = [tuple(elmt) for elmt in np.argwhere(discounts_matrix != 0)]

        for index in indices_discounts:
            allocations_df.loc[(allocations_df[allocations_header[UKEYS.SUPPLIER]] == id_to_supplier[index[0]])
                              & (allocations_df[allocations_header[UKEYS.DEMAND_INT_ID]] == index[1])
                              & (allocations_df[allocations_header[UKEYS.ITEM_CATEGORY]] == id_to_item_type[index[2]]),
                              allocations_header[UKEYS.DISCOUNT_AMOUNT]] = discounts_matrix[index]

        allocations_df[allocations_header[UKEYS.UNIT_PRICE]] = allocations_df[allocations_header[UKEYS.UNIT_PRICE]] \
                                                - allocations_df[allocations_header[UKEYS.DISCOUNT_AMOUNT]]

    allocations_table_data = TableData()
    allocations_table_data.data = allocations_df
    allocations_table_data.trim()
    allocations_table_data.header = allocations_header

    return allocations_table_data
