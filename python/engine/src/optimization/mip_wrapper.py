from __future__ import annotations
from mip.model import Model, xsum, minimize, maximize
from mip.constants import BINARY, CONTINUOUS, INTEGER, OptimizationStatus
from pathlib import Path
from typing import TYPE_CHECKING

from . import ModelWrapper
from shared.constants import VAR_TYPES, RecordedError
from shared.communication import SingletonLogger

if TYPE_CHECKING:
    pass

MIP_TYPE = {VAR_TYPES.BINARY: BINARY,
            VAR_TYPES.CONTINUOUS: CONTINUOUS,
            VAR_TYPES.INT: INTEGER}

slogger = SingletonLogger()

class MIPWrapper(ModelWrapper):
    """ Wrapper of the mip Model"""
    def __init__(self, name: str = 'sourcing_optimizer', solver: str = 'CBC'):
        self.solver = solver
        self.name = name
        self.model = Model(name=name, solver_name=solver)
        self.model.max_seconds = 60

    def add_constraint(self, constraint: bool, name_constraint: str = ''):
        self.model += constraint, name_constraint

    def sum(self, expression): # TODO : not sure about the typing
        return xsum(expression)

    def set_objective(self, objective, sense: int): # TODO : not sure about the typing
        if sense == 1:
            obj = minimize(objective)
        else:
            obj = maximize(objective)
        self.model.objective = obj

    def add_variable(self, name: str, type: str = VAR_TYPES.BINARY, **kwargs):
        return self.model.add_var(name=name, var_type=MIP_TYPE[type], **kwargs)

    def optimize(self):
        self.status = self.model.optimize()

    def check_status(self):
        if self.status == OptimizationStatus.OPTIMAL:
            return True
        elif self.status == OptimizationStatus.FEASIBLE:
            slogger.debug("Optimization reached the timeout")
            return True
        elif self.status == OptimizationStatus.INFEASIBLE:
            slogger.error(f"Infeasible constraints have been set. Please relax constraints.", save_message=True)
            raise RecordedError('Optimization infeasible : relax constraints')
        else:
            slogger.error(f"Unwanted optimization output:  {self.status}")
            raise RecordedError

    def get_variable(self, variable): # TODO : not sure about the typing
        return variable.x

    def get_variable_by_name(self, name: str):
        return self.model.var_by_name(name)

    def read(self, path: Path):
        """
        Wrapper for the read function.
        Args:
         - path: Path object that stores the location of the file to load from
        """
        self.model.read(str(path))

    def write(self, path: Path):
        """
        Wrapper for the write function.
        Args:
         - path: Path object that stores the location of the file to store the model
        """
        self.model.write(str(path))
