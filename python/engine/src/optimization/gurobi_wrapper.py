from __future__ import annotations

import gurobipy as gp
import json
import time

from datetime import datetime
from gurobipy import Model, quicksum, GRB, LinExpr
from pathlib import Path
from typing import TYPE_CHECKING

from . import ModelWrapper
from shared.constants import VAR_TYPES, RecordedError
from shared.communication import SingletonLogger

if TYPE_CHECKING:
    pass

GRB_TYPE = {VAR_TYPES.BINARY: GRB.BINARY,
            VAR_TYPES.CONTINUOUS: GRB.CONTINUOUS,
            VAR_TYPES.INT: GRB.INTEGER}

def setupbatchenv():

    env = gp.Env(empty=True)

    # No network communication happened up to this point.  This will happen
    # once the caller invokes the start() method of the returned Env object.

    return env

slogger = SingletonLogger()


class GurobiWrapper(ModelWrapper):
    """ Wrapper of the Gurobi Model"""
    def __init__(self, name: str = 'sourcing_optimizer', solver: str = 'GRB'):
        self.solver = solver
        self.name = name
        self.env = setupbatchenv().start()
        self.model = Model(name=name, env=self.env)
        self.variables = None

    def add_constraint(self, constraint: bool, name_constraint: str = ''):
        self.model.addConstr(constraint, name=name_constraint)

    def sum(self, expression: LinExpr):
        return quicksum(expression)

    def set_objective(self, objective: LinExpr, sense: int):
        self.model.setObjective(objective, sense)

    def add_variable(self, name: str, type: str = VAR_TYPES.BINARY):
        var = self.model.addVar(name=name, vtype=GRB_TYPE[VAR_TYPES.CONTINUOUS])
        var.VTag = name
        return var

    def optimize(self):

        # self.batch_id =
        self.model.optimize()

        # maxwaittime = 60

        # with gp.Batch(self.batch_id, self.env) as batch:
        #     starttime = datetime.now()
        #     while batch.BatchStatus == GRB.BATCH_SUBMITTED:
        #         # Abort this batch if it is taking too long
        #         curtime = datetime.now()
        #         if (curtime - starttime).total_seconds() > maxwaittime:
        #             batch.abort()
        #             slogger.error("optimization job timed out")
        #             break

        #         # Wait for two seconds
        #         time.sleep(2)

        #         # Update the resident attribute cache of the Batch object with the
        #         # latest values from the cluster manager.
        #         batch.update()

        #         # If the batch failed, we retry it
        #         if batch.BatchStatus == GRB.BATCH_FAILED:
        #             batch.retry()


    def check_status(self) -> bool:
        status = self.model.status
        if status == GRB.OPTIMAL:
            self.raw_vars = self.model.getVars()
            return True
        elif status == GRB.INFEASIBLE:
            slogger.error(f"Infeasible constraints have been set. Please relax constraints.", save_message=True)
            raise RecordedError('Optimization infeasible : relax constraints')
        elif status == GRB.UNBOUNDED:
            slogger.error(f"")
        else:
            raise RecordedError(f"Unexpected status received: {status}")
        # Uncomment next part when compute server is up and running
        # with gp.Batch(self.batch_id, self.env) as batch:
        #     if batch.BatchStatus == GRB.BATCH_COMPLETED:
        #         sol = json.loads(batch.getJSONSolution())
        #         status = sol["SolutionInfo"]["Status"]
        #         if status == GRB.OPTIMAL:
        #             self.raw_vars = sol["Vars"]
        #             return True
        #         elif status == GRB.INFEASIBLE:
        #             slogger.error(f"Infeasible constraints have been set. Please relax constraints.")
        #             raise RecordedError('Optimization infeasible : relax constraints')
        #         else:
        #             raise RecordedError
        #     else:
        #         raise RecordedError

    def get_unbounded_ray(self):
        varis = self.model.getVars()
        ray = {var.VTag: {"value": var.X, "ray": var.UnbdRay} for var in varis}
        return ray


    def get_variable(self, variable) -> float: # TODO : not sure about the typing
        if self.variables is None:
            self.variables = {var.VarName: float(var.X) for var in self.raw_vars}
        if variable.VTag not in self.variables:
            return 0
        return self.variables[variable.VTag]

    def get_variable_by_name(self, name: str):
        var = self.model.getVarByName(name)
        var.VTag = name
        return var

    def read(self, path: Path):
        self.model = gp.read(str(path), env=self.env)

    def write(self, path: Path):
        self.model.write(str(path))
