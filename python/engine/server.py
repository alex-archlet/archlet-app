"""
RESTful API server
"""

import json
import os
import socket
import time

from datetime import datetime

from shared.communication import DatabaseConnection
from shared.communication import ExecuteTask
from shared.communication import IOFacade
from shared.communication import QueueConnection
from shared.communication import SingletonLogger
from shared.constants import RecordedError
from shared.constants import SEVERITY
from main import execute_job

from shared.communication.singleton_logger import SingletonLogger


###########################################################################
# Read environment variables
###########################################################################
config = {}

# File storage
config['file_storage'] = {
    "logs_path": os.environ.get("LOGS_PATH")
}

# RabbitMQ
config['queue'] = {
    "hostname": os.environ.get("QUEUE_SERVICE_HOST"),
    "port":     os.environ.get("QUEUE_SERVICE_PORT"),
    "queue":    os.environ.get("QUEUE_NAME"),
    "connection_attempts":  int(os.environ.get("QUEUE_CONNECTION_ATTEMPTS")),
    "connection_intervals": int(os.environ.get("QUEUE_CONNECTION_INTERVALS"))
}

# Database
config['database'] = {
    "host": os.environ.get("DB_HOST"),
    "port": os.environ.get("DB_PORT"),
    "dbname": os.environ.get("DB_NAME"),
    "user": os.environ.get("DB_USER"),
    "password": os.environ.get("DB_PASSWORD")
}

slogger = SingletonLogger()
###############################################################################
# Helper functions
###############################################################################
def is_open(host, port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        s.connect((host, int(port)))
        s.shutdown(2)
        return True
    except:
        return False

def write_readiness_file():
    with open("/app/healthy_pod", "a") as f:
        f.write("pod is healthy")

###############################################################################
# Main function
###############################################################################
def run_server():

    ###########################################################################
    # Set logging output options
    ###########################################################################

    # Logging file name
    timestamp = datetime.utcnow().strftime('%Y.%m.%d_%H:%M:%S.%f')
    datestamp = datetime.utcnow().strftime('%Y_%m_%d')

    log_fname = f'{timestamp}_{socket.gethostname()}_server.log'

    # Create logging folder
    top_dir = config['file_storage']['logs_path']
    log_dir = f'{top_dir}/{datestamp}'

    # Initialize logging output
    slogger.clear()
    slogger.set_logger_output(logfile=log_fname, logdir=log_dir)


    # Silence excessive logging from pika
    slogger.set_logger_severity('pika', SEVERITY.WARNING)

    slogger.info(f"Engine pod running on host {socket.gethostname()}")

    # Create dummy io_facade, to pass to outermost execute task instances
    io_facade = IOFacade(-1, '')

    db_connection = DatabaseConnection(**config['database'])

    with ExecuteTask('running engine server', io_facade, toplevel=True):
        #######################################################################
        # Connect to RabbitMQ
        #######################################################################
        with ExecuteTask(f'connecting to RabbitMQ', io_facade):
            # Create queue connection
            queue_connection = QueueConnection()
            queue_connection.initialize(**config["queue"])

            # Silence excessive logging from pika
            slogger.set_logger_severity('pika', SEVERITY.WARNING)

        #######################################################################
        # Connect to database
        #######################################################################
        # with ExecuteTask('connecting to database'):
        #     state.connect_to_database(**config['database'])


        write_readiness_file()

        #######################################################################
        # Create and setup queue and callback
        #######################################################################
        with ExecuteTask(f'listening for requests', io_facade):
            # Callback function that gets called when a new message is received
            def callback(ch, method, properties, body):
                slogger.debug(f"Received message {body}")

                try:
                    body = json.loads(body)
                except Exception as e:
                    slogger.error(f'Invalid job request: {body}')
                    raise e

                if 'job_id' not in body:
                    slogger.error(f'Incomplete job request: {body}')
                    raise RecordedError

                job_id = body['job_id']

                slogger.info(f"Handling request for job with id <{job_id}>")
                execute_job(job_id=job_id,
                            database_connection=db_connection)
                slogger.info(f"Done handling request <{job_id}>")

                # Send back acknowledgement job was completed to task queue
                ch.basic_ack(delivery_tag = method.delivery_tag)

            # Listen for incoming message
            slogger.info('Waiting for messages...')
            queue_connection.start_consuming_from_queue(callback)
            slogger.info('Stopped listening for messages')


    db_connection.close_sessions()


if __name__ == "__main__":
    run_server()
