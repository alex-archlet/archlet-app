"""
Simple command line tool to run jobs directly
Give the job id as sole argument.
"""
import argparse
import os

from main import execute_job
from shared.communication.database_connection import DatabaseConnection
config = {}
config['database'] = {
    "host": os.environ.get("DB_HOST"),
    "port": os.environ.get("DB_PORT"),
    "dbname": os.environ.get("DB_NAME"),
    "user": os.environ.get("DB_USER"),
    "password": os.environ.get("DB_PASSWORD")
}

if __name__ == "__main__":

    # Parse arguments
    parser = argparse.ArgumentParser(description='CLI for Archlet App engine')

    parser.add_argument('job_id', help="PK of the job entry in the" \
        " PostgreSQL database that will be execute")

    args = parser.parse_args()

    # Execute job
    uid = os.urandom(5).hex()
    database_connection = DatabaseConnection(**config['database'])
    execute_job(job_id=args.job_id, database_connection=database_connection)
