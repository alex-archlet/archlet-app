"""
Testing suite for the database connection
"""
import factory
import numpy
import os
import pytest
import random
import uuid

from datetime import datetime
from functools import wraps
from sqlalchemy import Column
from sqlalchemy import Integer
from sqlalchemy import Unicode
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from shared.communication.database_classes import Base
from shared.communication.database_classes import BranchesDB
from shared.communication.database_classes import CompaniesDB
from shared.communication.database_classes import JobInfoDB
from shared.communication.database_classes import JobStatusesDB
from shared.communication.database_classes import JobTypesDB
from shared.communication.database_classes import OptimizationFilesDB
from shared.communication.database_classes import ProjectCategoriesDB
from shared.communication.database_classes import ProjectCurrenciesDB
from shared.communication.database_classes import ProjectTypesDB
from shared.communication.database_classes import ProjectUnitsDB
from shared.communication.database_classes import ProjectsDB
from shared.communication.database_classes import RolesDB
from shared.communication.database_classes import ScenariosDB
from shared.communication.database_classes import TABLES
from shared.communication.database_connection import DatabaseConnection
from shared.communication import IOFacade
from shared.constants import RecordedError


# temp see where does this go
PARAMS_DB = {
    "host": os.environ.get("DB_HOST_TEST"),
    "port": os.environ.get("DB_PORT_TEST"),
    "dbname": os.environ.get("DB_NAME_TEST"),
    "user": os.environ.get("DB_USER_TEST"),
    "password": os.environ.get("DB_PASSWORD_TEST")
}

###########################################################
#                   DB Factories
###########################################################


class CompanyFactory(factory.alchemy.SQLAlchemyModelFactory):
    """
    Factory class to create fake company entries in the DB
    """
    id = factory.Faker('uuid4')
    name = factory.Faker('name')
    created_at = factory.Faker('date')
    updated_at = factory.Faker('date')

    class Meta:
        model = CompaniesDB

class ProjectTypesFactory(factory.alchemy.SQLAlchemyModelFactory):
    """
    Factory class to create fake project type entries in the DB
    """
    id = factory.Sequence(lambda n: n)
    name = 'Project Type'
    created_at = factory.LazyFunction(datetime.now)
    updated_at = factory.LazyFunction(datetime.now)

    class Meta:
        model = ProjectTypesDB

class ProjectCategoriesFactory(factory.alchemy.SQLAlchemyModelFactory):
    """
    Factory class to create fake project category entries in the DB
    """
    id = factory.Sequence(lambda n: n)
    name = 'Project Category'
    created_at = factory.LazyFunction(datetime.now)
    updated_at = factory.LazyFunction(datetime.now)
    type_id = factory.SubFactory(ProjectTypesFactory)

    class Meta:
        model = ProjectCategoriesDB

class ProjectCurrenciesFactory(factory.alchemy.SQLAlchemyModelFactory):
    """
    Factory class to create fake project currency entries in the DB
    """
    id = factory.Sequence(lambda n: n)
    code = factory.Faker('currency')
    created_at = factory.LazyFunction(datetime.now)
    updated_at = factory.LazyFunction(datetime.now)

    class Meta:
        model = ProjectCurrenciesDB

class ProjectUnitsFactory(factory.alchemy.SQLAlchemyModelFactory):
    """
    Factory class to create fake project unit entries in the DB
    """
    id = factory.Sequence(lambda n: n)
    code = 'PU'
    name = 'Project Unit'
    created_at = factory.LazyFunction(datetime.now)
    updated_at = factory.LazyFunction(datetime.now)

    class Meta:
        model = ProjectUnitsDB

class RolesFactory(factory.alchemy.SQLAlchemyModelFactory):
    """
    Factory class to create fake role entries in the DB.
    """
    id = factory.Sequence(lambda n: n)
    name = factory.Faker('name')
    permissions = ['not so many']
    created_at = factory.LazyFunction(datetime.now)
    updated_at = factory.LazyFunction(datetime.now)

    class Meta:
        model = RolesDB

class BranchesFactory(factory.alchemy.SQLAlchemyModelFactory):
    """
    Factory class to create fake branch entries in the DB.
    """
    id = factory.Faker('uuid4')
    account_type = factory.Faker('job')
    json = {}
    location = factory.Faker('country')
    name = factory.Faker('name')
    solver = 'CBC'
    created_at = factory.LazyFunction(datetime.now)
    updated_at = factory.LazyFunction(datetime.now)
    company_id = factory.SubFactory(CompanyFactory)
    role_id = factory.SubFactory(RolesFactory)

    class Meta:
        model = BranchesDB

class ProjectsFactory(factory.alchemy.SQLAlchemyModelFactory):
    """
    Factory class to create fake project entries in the DB
    """
    id = factory.Faker('uuid4')
    name = factory.Faker('name')
    status = 'Status'
    deleted_at = factory.LazyFunction(datetime.now)
    created_at = factory.LazyFunction(datetime.now)
    updated_at = factory.LazyFunction(datetime.now)
    category_id = factory.SubFactory(ProjectCategoriesFactory)
    unit_id = factory.SubFactory(ProjectUnitsFactory)
    currency_id = factory.SubFactory(ProjectCurrenciesFactory)
    branch_id = factory.SubFactory(BranchesFactory)

    class Meta:
        model = ProjectsDB

class JobStatusFactory(factory.alchemy.SQLAlchemyModelFactory):
    """
    Factory class to create fake job status entries in the DB
    """
    id = factory.Sequence(lambda n: n)
    name = 'Job Status'
    created_at = factory.LazyFunction(datetime.now)
    updated_at = factory.LazyFunction(datetime.now)

    class Meta:
        model = JobStatusesDB

class JobTypeFactory(factory.alchemy.SQLAlchemyModelFactory):
    """
    Factory class to create fake job type entries in the DB
    """
    id = factory.Sequence(lambda n: n)
    name = 'Job Type'
    created_at = factory.LazyFunction(datetime.now)
    updated_at = factory.LazyFunction(datetime.now)

    class Meta:
        model = JobTypesDB

class ScenariosFactory(factory.alchemy.SQLAlchemyModelFactory):
    """
    Factory class to create fake scenario entries in the DB
    """
    id = factory.Faker('uuid4')
    invalidate = False
    is_default = True
    json = {}
    last_optimization = factory.LazyFunction(datetime.now)
    name = 'Scenario'
    status = 'processed'
    deleted_at = factory.LazyFunction(datetime.now)
    created_at = factory.LazyFunction(datetime.now)
    updated_at = factory.LazyFunction(datetime.now)
    branch_id = factory.SubFactory(BranchesFactory)

    class Meta:
        model = ScenariosDB

class JobFactory(factory.alchemy.SQLAlchemyModelFactory):
    """
    Factory class to create fake job entries in the DB
    """
    id = factory.Faker('uuid4')
    input = {}
    output = {}
    progress = 0
    created_at = factory.LazyFunction(datetime.now)
    updated_at = factory.LazyFunction(datetime.now)
    scenario_id = factory.SubFactory(ScenariosFactory)
    project_id = factory.SubFactory(ProjectsFactory)
    type_id = factory.SubFactory(JobStatusFactory)
    status_id = factory.SubFactory(JobTypeFactory)
    branch_id = factory.SubFactory(BranchesFactory)

    class Meta:
        model = JobInfoDB




class OptimizationFilesFactory(factory.alchemy.SQLAlchemyModelFactory):
    """
    Factory class to create fake optimization file entries in the DB.
    """
    id = factory.Faker('uuid4')
    content_hash = factory.Faker('md5')
    description = factory.Faker('sentence')
    file_extension = factory.Faker('file_extension')
    file_path = factory.Faker('file_path')
    file_type = factory.Faker('file_extension')
    file_version = factory.Faker('random_digit')
    name = factory.Faker('name')
    created_at = factory.LazyFunction(datetime.now)
    updated_at = factory.LazyFunction(datetime.now)
    project_id = factory.SubFactory(ProjectsFactory)
    scenario_id = factory.SubFactory(ScenariosFactory)
    branch_id = factory.SubFactory(BranchesFactory)

    class Meta:
        model = OptimizationFilesDB


###########################################################
#                   Test Fixtures
###########################################################


@pytest.fixture(scope='module')
def database_url():
    """
    Test fixture returning a database url
    """
    return (f"postgresql+psycopg2://{PARAMS_DB['user']}:{PARAMS_DB['password']}"
            f"@{PARAMS_DB['host']}:{PARAMS_DB['port']}/{PARAMS_DB['dbname']}")


@pytest.fixture(scope='module')
def engine(database_url):
    """
    Test fixture returning an SqlAlchemy engine.
    Moreover it makes sure that the whole test suite is started
    with a correct and empty schema.
    """
    engine = create_engine(database_url)
    Base.metadata.drop_all(bind=engine)
    Base.metadata.create_all(bind=engine)
    return engine


@pytest.fixture(scope='module')
def session_factory(engine):
    """
    Test fixture returning an SqlAlchemy session
    """
    return sessionmaker(bind=engine)


@pytest.fixture
def session(session_factory):
    """
    Test fixture that yields a session and closes it after use.
    """
    session = session_factory()
    yield session
    session.close()

@pytest.fixture
def clean_db(session_factory):
    """
    Test fixture to clean the db before a test
    """
    session = session_factory()
    print("Cleaning db")
    for table in TABLES:
        session.query(table).delete(synchronize_session=False)
        session.commit()
        result = session.query(table).all()
        assert len(result) == 0
    session.close()

@pytest.fixture
def role(session):
    """
    Test fixture to create a role in the db
    """
    RolesFactory._meta.sqlalchemy_session = session
    role_id = random.randint(0,100)
    RolesFactory.create(id=role_id)
    session.commit()
    return role_id

@pytest.fixture
def branch(session, role, company):
    """
    Test fixture to create a branch in the db
    """
    BranchesFactory._meta.sqlalchemy_session = session
    branch_id = uuid.uuid4().hex
    BranchesFactory.create(id=branch_id, role_id=role, company_id=company)
    session.commit()
    return branch_id

@pytest.fixture
def scenario(session, branch):
    """
    Test fixture to create a scenario in the db
    """
    ScenariosFactory._meta.sqlalchemy_session = session
    scen_id = uuid.uuid4().hex
    ScenariosFactory.create(id=scen_id, branch_id=branch)
    session.commit()
    return (scen_id, branch)

@pytest.fixture
def project_type(session):
    """
    Test fixture to create a project type in the db
    """
    ProjectTypesFactory._meta.sqlalchemy_session = session
    type_id = random.randint(0,100)
    ProjectTypesFactory.create(id=type_id)
    session.commit()
    return type_id

@pytest.fixture
def project_category(session, project_type):
    """
    Test fixture to create a project category in the db
    """
    ProjectCategoriesFactory._meta.sqlalchemy_session = session
    cat_id = random.randint(0,100)
    type_id = project_type
    ProjectCategoriesFactory.create(id=cat_id, type_id=type_id)
    session.commit()
    return cat_id

@pytest.fixture
def project_currency(session):
    """
    Test fixture to create a project currency in the db
    """
    ProjectCurrenciesFactory._meta.sqlalchemy_session = session
    cur_id = random.randint(0, 100)
    ProjectCurrenciesFactory.create(id=cur_id)
    session.commit()
    return cur_id

@pytest.fixture
def project_unit(session):
    """
    Test fixture to create a project unit in the db
    """
    ProjectUnitsFactory._meta.sqlalchemy_session = session
    unit_id = random.randint(0, 100)
    ProjectUnitsFactory.create(id=unit_id)
    session.commit()
    return unit_id

@pytest.fixture
def project(session, project_unit, project_currency, project_category, branch):
    """
    Test fixture to create a project in the db
    """
    ProjectsFactory._meta.sqlalchemy_session = session
    proj_id = uuid.uuid4().hex
    ProjectsFactory.create(id=proj_id, unit_id=project_unit, currency_id=project_currency,
                           category_id=project_category, branch_id=branch)
    session.commit()
    return (proj_id, branch)

@pytest.fixture
def job_status(session):
    """
    Test fixture to create a job status in the db
    """
    JobStatusFactory._meta.sqlalchemy_session = session
    status_id = random.randint(0, 100)
    JobStatusFactory.create(id=status_id)
    session.commit()
    return status_id

@pytest.fixture
def job_type(session):
    """
    Test fixture to create a job type in the db
    """
    JobTypeFactory._meta.sqlalchemy_session = session
    type_id = random.randint(0, 100)
    JobTypeFactory.create(id=type_id)
    session.commit()
    return type_id

@pytest.fixture
def put_job(session, scenario, project, job_status, job_type, branch):
    """
    Test fixture to create a job in the db
    """
    JobFactory._meta.sqlalchemy_session = session
    job_id = uuid.uuid4().hex
    JobFactory.create(id=job_id, scenario_id=scenario[0], project_id=project[0],
                      status_id=job_status, type_id=job_type, branch_id=branch)
    session.commit()
    return (job_id, branch)

@pytest.fixture
def company(session):
    """
    Test fixture to create a company in the db
    """
    CompanyFactory._meta.sqlalchemy_session = session
    comp_id = uuid.uuid4().hex
    CompanyFactory.create(id=comp_id)
    session.commit()
    return comp_id


@pytest.fixture
def optimization_file(session, scenario, project, branch):
    """
    Test fixture to create an optimization file in the db
    """
    OptimizationFilesFactory._meta.sqlalchemy_session = session
    file_id = uuid.uuid4().hex
    OptimizationFilesFactory.create(id=file_id,
                                    scenario_id=scenario[0],
                                    project_id=project[0],
                                    branch_id=branch)
    session.commit()
    return (file_id, branch)

@pytest.fixture
def job_with_file(session, scenario, project, job_status, job_type, optimization_file, branch):
    """
    Test fixture to create a job with an optimization file in the db
    """
    JobFactory._meta.sqlalchemy_session = session
    job_id = uuid.uuid4().hex
    input_json = {'files': {"optimization_file": optimization_file[0]}}
    JobFactory.create(id=job_id, scenario_id=scenario[0], project_id=project[0],
                      status_id=job_status, type_id=job_type, input=input_json,
                      branch_id=branch)
    session.commit()
    return (job_id, optimization_file[0], branch)

@pytest.fixture
def database_connection():
    """
    Test fixture to create a db connection
    """
    return DatabaseConnection(PARAMS_DB['user'],
                              PARAMS_DB['password'],
                              PARAMS_DB['host'],
                              PARAMS_DB['port'],
                              PARAMS_DB['dbname'])


###########################################################
#                       Tests
###########################################################


def test_fetch_job(clean_db, put_job, database_connection):
    """
    Test that checks whether a job is fetched correctly from
    the database
    """
    job_id, branch_id = put_job
    io_facade = IOFacade(job_id, '')
    io_facade.database_connection = database_connection
    result = io_facade.fetch_job()
    assert uuid.UUID(result.id).hex == uuid.UUID(job_id).hex
    assert uuid.UUID(result.branch_id).hex == uuid.UUID(branch_id).hex

def test_fetch_job_with_file(clean_db, job_with_file, database_connection):
    """
    Test that input files are extracted correctly when fetching a job
    """
    job_id, optimization_file_id, branch_id = job_with_file
    io_facade = IOFacade(job_id, '')
    io_facade.branch_id = branch_id
    io_facade.database_connection = database_connection
    result = io_facade.fetch_job()
    files = io_facade.fetch_job_files(result.input['files'])
    assert uuid.UUID(files['optimization_file'].id).hex == uuid.UUID(optimization_file_id).hex
    assert files['optimization_file'].name is not None
    assert files['optimization_file'].file_path is not None
    assert files['optimization_file'].file_extension is not None
    assert files['optimization_file'].file_version is not None

def test_fetch_project(clean_db, project, database_connection):
    """
    Test that checks whether a project is fetched correctly from
    the database
    """
    project_id, branch_id = project
    io_facade = IOFacade('', '')
    io_facade.database_connection = database_connection
    io_facade.project_id = project_id
    io_facade.branch_id = branch_id
    result = io_facade.fetch_project()
    assert uuid.UUID(result.id).hex == uuid.UUID(project_id).hex

def test_fetch_scenario(clean_db, scenario, database_connection):
    """
    Test that checks whether a scenario is fetched correctly from
    the database
    """
    scenario_id, branch_id = scenario
    io_facade = IOFacade('', '')
    io_facade.database_connection = database_connection
    io_facade.scenario_id = scenario_id
    io_facade.branch_id = branch_id
    result = io_facade.fetch_scenario()
    assert uuid.UUID(result.id).hex == uuid.UUID(scenario_id).hex

def test_fetch_file(clean_db, optimization_file, database_connection):
    """
    Test fetching an optimization file
    """
    file_id, branch_id = optimization_file
    io_facade = IOFacade('', '')
    io_facade.database_connection = database_connection
    io_facade.branch_id = branch_id
    result = io_facade.fetch_job_file(file_id)
    assert uuid.UUID(result.id).hex == uuid.UUID(file_id).hex

def test_failed_job_fetch(clean_db, put_job, database_connection):
    """
    Test that the correct exception is thrown when a job entry
    is not present
    """
    io_facade = IOFacade(uuid.uuid4().hex, '')
    io_facade.database_connection = database_connection
    with pytest.raises(RecordedError):
        io_facade.fetch_job()

def test_failed_scenario_fetch(clean_db, scenario, database_connection):
    """
    Test that the correct exception is thrown when a scenario entry
    is not present
    """
    _, branch_id = scenario
    io_facade = IOFacade(uuid.uuid4().hex, '')
    io_facade.database_connection = database_connection
    io_facade.branch_id = branch_id
    io_facade.scenario_id = uuid.uuid4().hex
    with pytest.raises(RecordedError):
        io_facade.fetch_scenario()

def test_failed_project_fetch(clean_db, project, database_connection):
    """
    Test that the correct exception is thrown when a project entry
    is not present
    """
    _, branch_id = project
    io_facade = IOFacade(uuid.uuid4().hex, '')
    io_facade.database_connection = database_connection
    io_facade.branch_id = branch_id
    io_facade.project_id = uuid.uuid4().hex
    with pytest.raises(RecordedError):
        io_facade.fetch_project()

def test_failed_file_fetch(clean_db, optimization_file, database_connection):
    """
    Test that the correct exception is thrown when a file entry
    is not present
    """
    _, branch_id = optimization_file
    io_facade = IOFacade(uuid.uuid4().hex, '')
    io_facade.database_connection = database_connection
    io_facade.branch_id = branch_id
    with pytest.raises(RecordedError):
        io_facade.fetch_job_file(uuid.uuid4().hex)


def test_add_job_output(clean_db, put_job, database_connection):
    """
    Test for updating the output of a job
    """
    job_id, branch_id = put_job
    io_facade = IOFacade(job_id, '')
    io_facade.database_connection = database_connection
    io_facade.branch_id = branch_id
    io_facade.add_job_output({'messages': ['message1', 'message2']})
    result = io_facade.fetch_job()
    assert 'messages' in result.output

def test_update_job_status(clean_db, put_job, job_status, database_connection):
    """
    Test for updating the job status
    """
    job_id, branch_id = put_job
    io_facade = IOFacade(job_id, '')
    io_facade.database_connection = database_connection
    io_facade.branch_id = branch_id
    io_facade.update_job_status(job_status)
    result = io_facade.fetch_job()
    assert result.status_id == job_status

def test_update_job_progress(clean_db, put_job, database_connection):
    job_id, branch_id = put_job
    io_facade = IOFacade(job_id, '')
    io_facade.database_connection = database_connection
    io_facade.branch_id = branch_id
    io_facade.update_job_progress(3)
    result = io_facade.fetch_job()
    assert result.progress == 3

def test_add_job_output_exception(clean_db, put_job, database_connection):
    """
    Test for updating output of a job with a database exception
    """
    job_id, branch_id = put_job
    io_facade = IOFacade(job_id, '')
    io_facade.database_connection = database_connection
    io_facade.branch_id = branch_id
    with pytest.raises(RuntimeError):
        io_facade.add_job_output({'files': numpy.int64(3)})

def test_fetch_job_output(clean_db, put_job, database_connection):
    """
    Test for updating a job output in the database
    """
    job_id, branch_id = put_job
    io_facade = IOFacade(job_id, '')
    io_facade.database_connection = database_connection
    io_facade.branch_id = branch_id
    result = io_facade.fetch_job_output()
    assert result == {}

def test_latest_file_version(clean_db, optimization_file, database_connection):
    """
    Test for fetching the latest file version
    """
    _, branch_id = optimization_file
    io_facade = IOFacade('', '')
    io_facade.database_connection = database_connection
    io_facade.branch_id = branch_id
    result = io_facade.latest_file_version("test_file", "csv", "/something")
    assert result == 0
