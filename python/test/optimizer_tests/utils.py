import pandas as pd
import pathlib
import pytest

from engine.src.optimization import Optimizer
from engine.src.table_processors import Preprocessor
from shared.communication import IOFacade
from shared.communication.database_connection import DBFile as File
from shared.constants import CONSTRAINT_SPECS
from shared.constants import CONSTRAINT_TYPES
from shared.constants import UKEYS, KEYS
from shared.constants import OPERATION_TYPE
from shared.constants import RecordedError
from shared.constants import SEVERITY
from shared.entities import TableData
from shared.entities import Translator



@pytest.fixture()
def state_object():
    def _state_object():
        iofacade = IOFacade(0, './')
        file_path = './resources'
        name = 'offers_table'
        version = 3
        extension = '.csv'
        file = File(file_path, name, version, extension)
        offers = TableData()
        iofacade.load_table(offers, file)

        # offers.header.set_types('region', KEYS.FILTER)
        offers.header.set_type(['Logistics Cost', 'Material Cost', 'Production Cost'] , KEYS.PRICE)

        name = 'demands_table'
        version = 1
        extension = '.csv'
        file = File(file_path,name,version,extension)
        demands = TableData()
        iofacade.load_table(demands, file)
        name = 'discounts_table'
        version = 2
        extension = '.csv'
        file = File(file_path,name,version,extension)
        discounts = TableData()
        iofacade.load_table(discounts, file)

        preprocessor = Preprocessor()
        offers.add_uuid_names()
        demands.add_uuid_names()
        discounts.add_uuid_names()
        offers = preprocessor.process_table(offers, 'offers')
        demands = preprocessor.process_table(demands, 'demands')
        discounts = preprocessor.process_table(discounts, 'discounts')
        discounts = preprocessor.clean_discounts_table(discounts, ['volume'], ['unit', 'percentage'], offers.header)
        solver = 'CBC'

        return offers, demands, discounts
    return _state_object

@pytest.fixture()
def state_flat_discounts():
    def _state_object():
        iofacade = IOFacade(0, './')
        file_path = './resources'
        name = 'offers_table'
        version = 2
        extension = '.csv'
        file = File(file_path, name, version, extension)
        offers = TableData()
        iofacade.load_table(offers, file)

        # offers.header.set_types('region', KEYS.FILTER)
        offers.header.set_type(['Logistics Cost', 'Material Cost', 'Production Cost'] , KEYS.PRICE)

        name = 'demands_table'
        version = 1
        extension = '.csv'
        file = File(file_path,name,version,extension)
        demands = TableData()
        iofacade.load_table(demands, file)
        name = 'discounts_table'
        version = 1
        extension = '.csv'
        file = File(file_path,name,version,extension)
        discounts = TableData()
        iofacade.load_table(discounts, file)

        preprocessor = Preprocessor()
        offers.add_uuid_names()
        demands.add_uuid_names()
        discounts.add_uuid_names()
        offers = preprocessor.process_table(offers, 'offers')
        demands = preprocessor.process_table(demands, 'demands')
        discounts = preprocessor.process_table(discounts, 'discounts')
        discounts = preprocessor.clean_discounts_table(discounts, ['volume'], ['unit', 'percentage', 'flat'], offers.header)
        solver = 'CBC'

        return offers, demands, discounts
    return _state_object


@pytest.fixture()
def state_object_lower_volumes():
    def _state_object():
        iofacade = IOFacade(0, './')
        file_path = './resources'
        name = 'offers_table'
        version = 4
        extension = '.csv'
        file = File(file_path, name, version, extension)
        offers = TableData()
        iofacade.load_table(offers, file)
        offers.header.set_type('price_usd', KEYS.PRICE)
        name = 'demands_table'
        version = 1
        extension = '.csv'
        file = File(file_path,name,version,extension)
        demands = TableData()
        iofacade.load_table(demands, file)
        name = 'discounts_table'
        version = 2
        extension = '.csv'
        file = File(file_path,name,version,extension)
        discounts = TableData()
        iofacade.load_table(discounts, file)

        preprocessor = Preprocessor()
        offers.add_uuid_names()
        demands.add_uuid_names()
        discounts.add_uuid_names()
        offers = preprocessor.process_table(offers, 'offers')
        demands = preprocessor.process_table(demands, 'demands')
        discounts = preprocessor.process_table(discounts, 'discounts')
        discounts = preprocessor.clean_discounts_table(discounts, ['volume'], ['unit', 'percentage'], offers.header)

        solver = 'CBC'

        return offers, demands, discounts
    return _state_object


@pytest.fixture()
def state_object_capacities(state_object):
    def _state_object():

        iofacade = IOFacade(0, './')
        file_path = './resources/capacities'

        name = 'offers_table'
        version = 8
        extension = '.csv'
        file = File(file_path, name, version, extension)
        offers = TableData()
        iofacade.load_table(offers, file)

        offers.header.set_type(['capacity1', 'capacity2', 'capacity3', 'capacity4', 'capacity5', 'capacity6', 'capacity7', 'capacity8'], KEYS.CAPACITY)
        offers.header.set_type(['Personal','Baustellensicherung','Technikoption','Signalisierung','Technische Sicherung'] , KEYS.PRICE)

        # offers.header.set_types(KEYS.CAPACITY, 'capacity1', 'capacity2', 'capacity3', 'capacity4', 'capacity5', 'capacity6', 'capacity7', 'capacity8')
        offers.header.add_column(UKEYS.ITEM_CATEGORY)
        # offers.header.add_column(KEYS.ITEM_CATEGORY, KEYS.ITEM_CATEGORY, KEYS.ITEM_CATEGORY)
        offers[offers.header[UKEYS.ITEM_CATEGORY]] = 'standard'

        name = 'demands_table'
        version = 8
        extension = '.csv'
        file = File(file_path,name,version,extension)
        demands = TableData()
        iofacade.load_table(demands, file)

        preprocessor = Preprocessor()

        offers.add_uuid_names()
        demands.add_uuid_names()
        offers = preprocessor.process_table(offers, 'offers')
        demands = preprocessor.process_table(demands, 'demands')

        discounts = None

        solver = 'CBC'

        name = 'capacities_table'
        version = 8
        extension = '.csv'
        file = File(file_path, name, version, extension)
        capacities = TableData()
        iofacade.load_table(capacities, file)

        capacities.header.set_type(['capacity1', 'capacity2', 'capacity3', 'capacity4', 'capacity5', 'capacity6', 'capacity7', 'capacity8'], KEYS.CAPACITY)
        # capacities.header.set_types(KEYS.CAPACITY, 'capacity1', 'capacity2', 'capacity3', 'capacity4', 'capacity5', 'capacity6', 'capacity7', 'capacity8')
        capacities.add_uuid_names()

        return offers, demands, discounts, capacities
    return _state_object


@pytest.fixture()
def state_object_translator(state_object):
    def _state_object():
        file_path = './resources'

        # Get basic objects from default state object
        offers, demands, discounts = state_object()

        # read translator
        name = 'archlet_rawmat_translator'
        version = 1
        extension = '.json'
        file_name = f'{name}_v{version}{extension}'
        translator = Translator(file_name, file_path)

        return offers, demands, discounts, translator

    return _state_object


@pytest.fixture()
def params():
    def _params(value = None, key1 = None, key2 = None):
        params = {"name":"Test","general_settings":{"rebates":True, "split_items":True, "capacity":False, "partial_allocation":False},"price_split":[],"max_suppliers":{},
        "scenario_type":"custom","excluded_suppliers":[],"multi_splits":[],
        "max_spend_percentage":{"spend":{},"volume":{},"capacity":{},"percentage":{}},
        "volume_per_supplier":[]}

        if key1 is None and key2 is None:
            pass
        elif key2 is None:
            if isinstance(params[key1], list):
                params[key1].append(value)
            else:
                params[key1] = value
        else:
            params[key1][key2] = value
        return params
    return _params


@pytest.fixture()
def chemicals_translator():
    # read translator
    file_path = './resources'
    name = 'archlet_rawmat_translator'
    version = 2
    extension = '.json'
    file_name = f'{name}_v{version}{extension}'
    translator = Translator(file_name, file_path)
