from .utils import *
from .optimizer_test_baseline import BASELINE, add_demand_column


""" ------------------------------ SPEND TESTS ------------------------------ """

def test_max_spend_region(state_object, params):
    excluded_combos = [('Asia', 'Reliance')]
    for region in BASELINE['spend'].keys():
        for supplier in BASELINE['spend'][region].keys():
            # Get data
            offers, demands, discounts = state_object()
            demands_header = demands.header
            filters = [demands_header.byname['region'].id]

            # Set constraint
            if supplier == 'Every-Supplier' or (region, supplier) in excluded_combos:
                continue
            max_spend = BASELINE['spend'][region][supplier] * (4/5)
            parameters = params()
            parameters['limit_spend'] = [{'filters':{'region':[region], 'supplier':[supplier]}, 'unit':'spend', 'mutation': 'for_all', 'value':max_spend, 'operation':'max'}]
            
            # Run optimization
            optimizer = Optimizer(parameters, offers, demands, discounts)
            optimizer.initialize_preprocessor()
            assert optimizer.initialize_model(filters) == True
            allocations_table = optimizer.run()
            allocations_table = add_demand_column(allocations_table, demands, 'region')
            alloc_header = allocations_table.header

            costs = allocations_table[alloc_header[UKEYS.TOTAL_VOLUME]].multiply(allocations_table[alloc_header['unit_price']])
            spend_region_supplier = costs[(allocations_table[alloc_header.byname['region'].id] == region) & (allocations_table[alloc_header['supplier']] == supplier)]
            total_spend_region_supplier = spend_region_supplier.sum()
            relative_error = (total_spend_region_supplier - max_spend)*100/max_spend
            assert relative_error < 2,\
                   f'Spend of {supplier} in {region} is {relative_error} superior to the max spend'
            del parameters

def test_max_spend_region_invalid(state_object, params):
    # Get data
    offers, demands, discounts = state_object()
    demands_header = demands.header
    filters = [demands_header.byname['region'].id]

    # Set constraint
    max_spend = 0
    region = 'Europe'
    supplier1 = 'Mitsubishi'
    supplier2 = 'AirLiquide'
    supplier3 = 'BASF'
    # Parameters necessary so 1 item can't be bought : at least 3 suppliers per item
    parameters = params()
    parameters['limit_spend'] = [{'filters':{'region':[region], 'supplier':[supplier1]}, 'unit':'spend', 'mutation': 'for_all', 'value':max_spend, 'operation':'max'}]
    parameters['limit_spend'].append({'filters':{'region':[region], 'supplier':[supplier2]}, 'unit':'spend', 'mutation': 'for_all', 'value':max_spend, 'operation':'max'})
    parameters['limit_spend'].append({'filters':{'region':[region], 'supplier':[supplier3]}, 'unit':'spend', 'mutation': 'for_all', 'value':max_spend, 'operation':'max'})
    
    # Run optimization
    optimizer = Optimizer(parameters, offers, demands, discounts)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(filters) == True
    # assert optimizer.validate_constraints() == False # TODO : fix that
    with pytest.raises(RecordedError) as e:
        optimizer.run()

def test_min_spend_region(state_object, params):
    for region in BASELINE['spend'].keys():
        for supplier in BASELINE['spend'][region].keys():
            # Get data
            offers, demands, discounts = state_object()
            demands_header = demands.header
            filters = [demands_header.byname['region'].id]

            # Set constraint
            if supplier == 'Every-Supplier' or region == 'All':
                continue
            min_spend = BASELINE['spend'][region][supplier] * (11/10)
            if min_spend >= BASELINE['max_possible_spend'][region][supplier]:
                continue
            parameters = params()
            parameters['limit_spend'] = [{'filters':{'region':[region], 'supplier':[supplier]}, 'unit':'spend', 'mutation': 'for_all', 'value':min_spend, 'operation':'min'}]
            
            # Run optimization
            optimizer = Optimizer(parameters, offers, demands, discounts)
            optimizer.initialize_preprocessor()
            assert optimizer.initialize_model(filters) == True
            allocations_table = optimizer.run()
            allocations_table = add_demand_column(allocations_table, demands, 'region')
            alloc_header = allocations_table.header

            costs = allocations_table[alloc_header[UKEYS.TOTAL_VOLUME]].multiply(allocations_table[alloc_header['unit_price']])
            # spend_region_supplier = costs[(allocations_table[alloc_header.byname['region'].id] == region) & (allocations_table[alloc_header['supplier']] == supplier)]
            spend_region_supplier = costs[(allocations_table[alloc_header['supplier']] == supplier)]
            total_spend_region_supplier = spend_region_supplier.sum()
            relative_error = (min_spend - total_spend_region_supplier)*100/min_spend
            assert relative_error < 1,\
                   f'Spend of {supplier} in {region} is {relative_error} inferior to the min spend'
            del parameters

def test_min_spend_region_invalid(state_object, params):
    # Get data
    offers, demands, discounts = state_object()
    demands_header = demands.header
    filters = [demands_header.byname['region'].id]

    # Set constraint
    min_spend = 10000000000
    region = 'Asia'
    supplier = 'SABIC'
    parameters = params()
    parameters['limit_spend'] = [{'filters':{'region':[region], 'supplier':[supplier]}, 'unit':'spend', 'mutation': 'for_all', 'value':min_spend, 'operation':'min'}]
    
    # Run optimization
    optimizer = Optimizer(parameters, offers, demands, discounts)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(filters) == True
    # assert optimizer.validate_constraints() == False # TODO : fix that
    with pytest.raises(RecordedError) as e:
        optimizer.run()

def test_max_spend_global(state_object, params):
    for supplier in BASELINE['spend']['All'].keys():
        # Get data
        offers, demands, discounts = state_object()
        demands_header = demands.header
        filters = [demands_header.byname['region'].id]

        # Set constraint
        all_regions = demands[demands_header.byname['region'].id].unique()
        max_spend = BASELINE['spend']['All'][supplier] * (19/20)
        parameters = params()
        parameters['limit_spend'] = [{'filters':{'region': all_regions, 'supplier':[supplier]}, 'unit':'spend', 'value':max_spend, 'mutation': 'for_all', 'operation':'max'}]
        
        # Run optimization
        optimizer = Optimizer(parameters, offers, demands, discounts)
        optimizer.initialize_preprocessor()
        assert optimizer.initialize_model(filters) == True
        allocations_table = optimizer.run()
        alloc_header = allocations_table.header

        costs = allocations_table[alloc_header[UKEYS.TOTAL_VOLUME]].multiply(allocations_table[alloc_header['unit_price']])
        spend_supplier = costs[(allocations_table[alloc_header['supplier']] == supplier)]
        total_spend_supplier = spend_supplier.sum()
        relative_error = (total_spend_supplier - max_spend)*100/max_spend
        assert relative_error < 2,\
               f'Spend of {supplier} globally is {relative_error} superior to the max spend'
        del parameters

def test_min_spend_global(state_object, params):
    for supplier in BASELINE['spend']['All'].keys():
        # Get data
        offers, demands, discounts = state_object()
        demands_header = demands.header
        filters = [demands_header.byname['region'].id]

        # Set constraint
        all_regions = demands[demands_header.byname['region'].id].unique()
        min_spend = BASELINE['spend']['All'][supplier] * (21/20)
        parameters = params()
        parameters['limit_spend'] = [{'filters':{'region': all_regions, 'supplier': [supplier]}, 'unit':'spend', 'mutation': 'for_all', 'value':min_spend, 'operation':'min'}]

        # Run optimization
        optimizer = Optimizer(parameters, offers, demands, discounts)
        optimizer.initialize_preprocessor()
        assert optimizer.initialize_model(filters) == True
        allocations_table = optimizer.run()
        alloc_header = allocations_table.header

        costs = allocations_table[alloc_header[UKEYS.TOTAL_VOLUME]].multiply(allocations_table[alloc_header['unit_price']])
        spend_supplier = costs[(allocations_table[alloc_header['supplier']] == supplier)]
        total_spend_supplier = spend_supplier.sum()
        relative_error = (min_spend - total_spend_supplier)*100/min_spend
        assert relative_error < 1,\
               f'Spend of {supplier} globally is {relative_error} inferior to the min spend'
        del parameters

def test_min_spend_global_invalid(state_object, params):
    # Get data
    offers, demands, discounts = state_object()
    demands_header = demands.header
    filters = [demands_header.byname['region'].id]

    # Set constraint
    all_regions = demands[demands_header.byname['region'].id].unique()
    min_spend = 1000000000
    supplier = 'SABIC'
    parameters = params()
    parameters['limit_spend'] = [{'filters':{'region': all_regions, 'supplier': [supplier]}, 'unit':'spend', 'value':min_spend, 'operation':'min'}]
    
    # Run optimization
    optimizer = Optimizer(parameters, offers, demands, discounts)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(filters) == True
    with pytest.raises(RecordedError) as e:
        optimizer.run()


def test_min_spend_everysupplier(state_object, params):
    # Get data
    offers, demands, discounts = state_object()
    demands_header = demands.header
    offers_header = offers.header
    filters = [demands_header.byname['region'].id]

    # Set constraint
    all_suppliers = list(offers[offers_header['supplier']].unique())
    all_suppliers.remove('AirLiquide')
    all_suppliers.remove('Linde')
    all_suppliers.remove('Lyondell')
    min_spend = 100000
    region = 'Asia'
    parameters = params()
    parameters['limit_spend'] = [{'filters':{'region': [region], 'supplier': all_suppliers}, 'unit':'spend', 'mutation': 'for_each', 'value':min_spend, 'operation':'min'}]

    # Run optimization
    optimizer = Optimizer(parameters, offers, demands, discounts)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(filters) == True
    allocations_table = optimizer.run()
    allocations_table = add_demand_column(allocations_table, demands, 'region')
    alloc_header = allocations_table.header
    
    allocations_region = allocations_table[allocations_table[alloc_header.byname['region'].id] == region]
    costs = allocations_region[alloc_header[UKEYS.TOTAL_VOLUME]].multiply(allocations_region[alloc_header['unit_price']])
    suppliers = allocations_region[alloc_header['supplier']].unique()
    print('Baseline : ', BASELINE['spend'][region])
    for supplier in suppliers:
        print('Spend for supplier {} : {}'.format(supplier, costs[allocations_region[alloc_header['supplier']] == supplier].sum()))
