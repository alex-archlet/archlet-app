from .utils import *
from .optimizer_test_baseline import BASELINE, add_demand_column


""" ------------------------------ PERCENTAGE TESTS ------------------------------ """


def test_max_percentage_global(state_object, params):
    for supplier in BASELINE['percentage']['All'].keys():
        # Get data
        offers, demands, discounts = state_object()
        demands_header = demands.header
        filters = [demands_header.byname['region'].id]

        # Set constraint
        all_regions = demands[demands_header.byname['region'].id].unique()
        max_percentage = BASELINE['percentage']['All'][supplier] * (19/20)
        parameters = params()
        parameters['limit_spend'] = [{'filters':{'region': all_regions, 'supplier': [supplier]}, 'unit':'percentage', 'mutation': 'for_all', 'value':max_percentage, 'operation':'max'}]

        # Run Optimization
        optimizer = Optimizer(parameters, offers, demands, discounts)
        optimizer.initialize_preprocessor()
        assert optimizer.initialize_model(filters) == True
        allocations_table = optimizer.run()

        alloc_header = allocations_table.header
        costs = allocations_table[alloc_header[UKEYS.TOTAL_VOLUME]].multiply(allocations_table[alloc_header['unit_price']])
        total_cost = costs.sum()
        spend_supplier = costs[(allocations_table[alloc_header['supplier']] == supplier)]
        total_spend_supplier = spend_supplier.sum()
        percentage_supplier = (total_spend_supplier/total_cost)*100
        relative_error = (percentage_supplier - max_percentage)*100/max_percentage
        assert relative_error < 1,\
               f'Percentage of {supplier} globally is {relative_error} superior to the max percentage'
        del parameters

def test_min_percentage_global(state_object, params):
    for supplier in BASELINE['percentage']['All'].keys():
         # Get data
        offers, demands, discounts = state_object()
        demands_header = demands.header
        filters = [demands_header.byname['region'].id]

        # Set constraint
        all_regions = demands[demands_header.byname['region'].id].unique()
        min_percentage = BASELINE['percentage']['All'][supplier] * (21/20)
        parameters = params()
        parameters['limit_spend'] = [{'filters':{'region': all_regions, 'supplier': [supplier]}, 'unit':'percentage', 'value':min_percentage, 'operation':'min'}]
        
        # Run optimization
        optimizer = Optimizer(parameters, offers, demands, discounts)
        optimizer.initialize_preprocessor()
        assert optimizer.initialize_model(filters) == True
        allocations_table = optimizer.run()
        alloc_header = allocations_table.header
        costs = allocations_table[alloc_header[UKEYS.TOTAL_VOLUME]].multiply(allocations_table[alloc_header['unit_price']])
        total_cost = costs.sum()
        spend_supplier = costs[(allocations_table[alloc_header['supplier']] == supplier)]
        total_spend_supplier = spend_supplier.sum()
        percentage_supplier = (total_spend_supplier/total_cost)*100
        relative_error = (min_percentage - percentage_supplier)*100/min_percentage
        assert relative_error < 2,\
               f'Percentage of {supplier} globally is {relative_error} inferior to the min percentage'
        del parameters

def test_min_percentage_global_invalid(state_object, params):
    # Get data
    offers, demands, discounts = state_object()
    demands_header = demands.header
    filters = [demands_header.byname['region'].id]

    # Set constraint
    all_regions = demands[demands_header.byname['region'].id].unique()
    min_percentage = 1000000000
    supplier = 'SABIC'
    parameters = params()
    parameters['limit_spend'] = [{'filters':{'region': all_regions, 'supplier': [supplier]}, 'unit':'percentage', 'value':min_percentage, 'operation':'min', 'mutation': 'for_all'}]
    
    # Run optimization
    optimizer = Optimizer(parameters, offers, demands, discounts)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(filters) == True
    with pytest.raises(RecordedError) as e:
        optimizer.run()


def test_max_percentage_region(state_object, params):
    for region in BASELINE['percentage'].keys():
        for supplier in BASELINE['percentage'][region].keys():
            # Get data
            offers, demands, discounts = state_object()
            demands_header = demands.header
            filters = [demands_header.byname['region'].id]

            # Set constraint
            if supplier == 'Every-Supplier' or region == 'All':
                continue
            max_percentage = BASELINE['percentage'][region][supplier] * (19/20)
            parameters = params()
            parameters['limit_spend'] = [{'filters':{'region': [region], 'supplier': [supplier]}, 'unit':'percentage', 'mutation': 'for_each', 'value':max_percentage, 'operation':'max'}]
            
            # Run optimization
            optimizer = Optimizer(parameters, offers, demands, discounts)
            optimizer.initialize_preprocessor()
            assert optimizer.initialize_model(filters) == True
            allocations_table = optimizer.run()
            allocations_table = add_demand_column(allocations_table, demands, 'region')
            alloc_header = allocations_table.header
            costs = allocations_table[alloc_header[UKEYS.TOTAL_VOLUME]].multiply(allocations_table[alloc_header['unit_price']])
            total_cost = costs.sum()
            spend_supplier_region = costs[(allocations_table[alloc_header['supplier']] == supplier) & (allocations_table[alloc_header.byname['region'].id] == region)]
            total_spend_supplier_region = spend_supplier_region.sum()
            percentage_supplier_region = (total_spend_supplier_region/total_cost)*100
            relative_error = (percentage_supplier_region - max_percentage)*100/max_percentage
            assert relative_error < 1,\
                   f'Percentage of {supplier} in {region} is {relative_error} superior to the max percentage'

            del parameters


def test_min_percentage_region(state_object, params):
    for region in BASELINE['percentage'].keys():
        for supplier in BASELINE['percentage'][region].keys():
            # Get data
            offers, demands, discounts = state_object()
            demands_header = demands.header
            filters = [demands_header.byname['region'].id]

            # Set constraint
            if supplier == 'Every-Supplier' or region == 'All':
                continue
            min_percentage = BASELINE['percentage'][region][supplier] * (11/10)
            parameters = params()
            parameters['limit_spend'] = [{'filters':{'region': [region], 'supplier': [supplier]}, 'unit':'percentage', 'value':min_percentage, 'mutation': 'for_each', 'operation':'min'}]
            
            # Run optimization
            optimizer = Optimizer(parameters, offers, demands, discounts)
            optimizer.initialize_preprocessor()
            assert optimizer.initialize_model(filters) == True
            allocations_table = optimizer.run()
            allocations_table = add_demand_column(allocations_table, demands, 'region')
            alloc_header = allocations_table.header
            costs = allocations_table[alloc_header[UKEYS.TOTAL_VOLUME]].multiply(allocations_table[alloc_header['unit_price']])

            total_cost = costs[allocations_table[alloc_header.byname['region'].id] == region].sum()
            spend_supplier_region = costs[(allocations_table[alloc_header['supplier']] == supplier) & (allocations_table[alloc_header.byname['region'].id] == region)]
            total_spend_supplier_region = spend_supplier_region.sum()
            percentage_supplier_region = (total_spend_supplier_region/total_cost)*100
            relative_error = (min_percentage - percentage_supplier_region)*100/min_percentage
            assert relative_error < 1,\
                   f'Percentage of {supplier} globally is {relative_error} inferior to the min percentage'

            del parameters

def test_min_percentage_region_invalid(state_object, params):
    # Get data
    offers, demands, discounts = state_object()
    demands_header = demands.header
    filters = [demands_header.byname['region'].id]

    # Set constraint
    supplier = ['Clariant']
    region = ['Europe']
    min_percentage = 90
    parameters = params()
    parameters['limit_spend'] = [{'filters':{'region': region, 'supplier': supplier}, 'unit':'percentage', 'value':min_percentage, 'mutation': 'for_each', 'operation':'min'}]
    
    # Run optimization
    optimizer = Optimizer(parameters, offers, demands, discounts)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(filters) == True
    with pytest.raises(RecordedError) as e:
        optimizer.run()

def test_min_percentage_everysupplier(state_object, params):
    # Get data
    offers, demands, discounts = state_object()
    demands_header = demands.header
    offers_header = offers.header
    filters = [demands_header.byname['region'].id]

    # Set constraint
    min_percentage = 2
    all_suppliers = list(offers[offers_header['supplier']].unique())
    all_suppliers.remove('AirLiquide')
    all_suppliers.remove('Linde')
    all_suppliers.remove('Lyondell')
    region = 'Asia'
    parameters = params()
    parameters['limit_spend'] = [{'filters':{'region': [region], 'supplier': all_suppliers}, 'unit':'percentage', 'value':min_percentage, 'mutation': 'for_each', 'operation':'min'}]
    
    # Run optimization
    optimizer = Optimizer(parameters, offers, demands, discounts)
    optimizer.initialize_preprocessor()
    optimizer.initialize_model(filters)
    allocations_table = optimizer.run()

    # Check validity
    allocations_table = add_demand_column(allocations_table, demands, 'region')
    alloc_header = allocations_table.header
    allocations_region = allocations_table[allocations_table[alloc_header.byname['region'].id] == region]
    costs = allocations_region[alloc_header[UKEYS.TOTAL_VOLUME]].multiply(allocations_region[alloc_header['unit_price']])
    suppliers = allocations_region[alloc_header['supplier']].unique()
    total_cost = costs.sum()
    for supplier in suppliers:
        spend_supplier_region = costs[(allocations_region[alloc_header['supplier']] == supplier)]
        total_spend_supplier_region = spend_supplier_region.sum()
        percentage_supplier_region = (total_spend_supplier_region/total_cost)*100
        print('Supplier {} : {} %'.format(supplier, percentage_supplier_region))
