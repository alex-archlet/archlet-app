from .utils import *
from .optimizer_test_baseline import BASELINE, add_demand_column


""" ------------------------------ SUPPLIERS TESTS ------------------------------ """

# TODO : CHANGE AFTER REFACTORING : excluding the suppliers should be done in MIP_preprocessor
# def test_excluded_supplier(state_object, params):
# 	params['excluded_suppliers'].append('SABIC') #Included in the solution. Has to chnage if table changes
# 	# Check that the optimizer runs with this constraint
# 	optimizer = Optimizer(state_object, params)
# 	assert optimizer.initialize_model() == True
# 	allocations_table = optimizer.run()
#
# 	# Check that the constraint is respected
# 	allocations_table = state_object.allocations_table.data
# 	suppliers = allocations_table['supplier'].unique()


# Right now this isn't tested because by default all available suppliers are used
def test_min_suppliers_global(state_object, params):
    # Get data
    offers, demands, discounts = state_object()
    demands_header = demands.header
    filters = [demands_header.byname['region'].id]

    # Set constraint
    if BASELINE['suppliers']['All'] == BASELINE['nbr_suppliers_offers']['All']:
        min_suppliers = 5
    else:
        min_suppliers = BASELINE['suppliers']['All'] + 1

    all_regions = demands[demands_header.byname['region'].id].unique()
    parameters = params()
    parameters['number_of_suppliers'] = [{'filters':{'region':all_regions}, 'value':min_suppliers, 'mutation': 'for_all', 'operation':'min'}]

    # Check that the optimizer runs properly
    optimizer = Optimizer(parameters, offers, demands, discounts)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(filters) == True
    allocations_table = optimizer.run()
    alloc_header = allocations_table.header
    # Check that the constraint is respected
    nbr_suppliers = len(allocations_table[alloc_header['supplier']].unique())
    assert nbr_suppliers >= min_suppliers


def test_min_suppliers_global_invalid(state_object, params):
    # Get data
    offers, demands, discounts = state_object()
    demands_header = demands.header
    filters = [demands_header.byname['region'].id]

    # Set constraint
    min_suppliers = BASELINE['nbr_suppliers_offers']['All'] + 1

    all_regions = demands[demands_header.byname['region'].id].unique()
    parameters = params()
    parameters['number_of_suppliers'] = [{'filters':{'region':all_regions}, 'value':min_suppliers, 'mutation': 'for_all', 'operation':'min'}]

    # Check that the optimizer runs properly
    optimizer = Optimizer(parameters, offers, demands, discounts)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(filters) == True
    with pytest.raises(RecordedError) as e:
        optimizer.run()


def test_max_suppliers_global(state_object, params):
    # Get data
    offers, demands, discounts = state_object()
    demands_header = demands.header
    filters = [demands_header.byname['region'].id]

    # Set constraint
    max_suppliers = BASELINE['suppliers']['All'] - 1

    all_regions = demands[demands_header.byname['region'].id].unique()
    parameters = params()
    parameters['number_of_suppliers'] = [{'filters':{'region':all_regions}, 'value':max_suppliers, 'mutation': 'for_all', 'operation':'max'}]
    
    # Check that the optimizer runs properly
    optimizer = Optimizer(parameters, offers, demands, discounts)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(filters) == True
    allocations_table = optimizer.run()
    alloc_header = allocations_table.header

    # Ceck that the constraint is respected
    nbr_suppliers = len(allocations_table[alloc_header['supplier']].unique())
    assert nbr_suppliers <= max_suppliers


def test_max_suppliers_global_invalid(state_object, params):
    # Get data
    offers, demands, discounts = state_object()
    demands_header = demands.header
    filters = [demands_header.byname['region'].id]

    # Set constraint
    max_suppliers = 1

    all_regions = demands[demands_header.byname['region'].id].unique()
    parameters = params()
    parameters['number_of_suppliers'] = [{'filters':{'region':all_regions}, 'value':max_suppliers, 'mutation': 'for_all', 'operation':'max'}]

    # Check that the optimizer fails
    optimizer = Optimizer(parameters, offers, demands, discounts)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(filters) == True
    with pytest.raises(RecordedError) as e:
        optimizer.run()


def test_max_suppliers_region(state_object, params):
    for region in BASELINE['suppliers'].keys():
        # Get data
        offers, demands, discounts = state_object()
        demands_header = demands.header
        filters = [demands_header.byname['region'].id]

        max_suppliers = BASELINE['suppliers'][region] - 1

        parameters = params()
        parameters['number_of_suppliers'] = [{'filters':{'region':[region]}, 'value':max_suppliers, 'mutation': 'for_all', 'operation':'max'}]

        # Check that the optimizer runs properly
        optimizer = Optimizer(parameters, offers, demands, discounts)
        optimizer.initialize_preprocessor()
        assert optimizer.initialize_model(filters) == True
        allocations_table = optimizer.run()
        allocations_table = add_demand_column(allocations_table, demands, 'region')
        alloc_header = allocations_table.header
        # Ceck that the constraint is respected
        nbr_suppliers = len(allocations_table[allocations_table[alloc_header.byname['region'].id] == region][alloc_header['supplier']].unique())
        assert nbr_suppliers <= max_suppliers, f'Region : {region}'


def test_max_suppliers_region_invalid(state_object, params):
    for region in BASELINE['suppliers'].keys():
        # Get data
        offers, demands, discounts = state_object()
        demands_header = demands.header
        filters = [demands_header.byname['region'].id]

        if region == 'All':
            continue

        max_suppliers = BASELINE['suppliers'][region] - 9

        parameters = params()
        parameters['number_of_suppliers'] = [{'filters':{'region':[region]}, 'value':max_suppliers, 'mutation': 'for_all', 'operation':'max'}]

        # Check that the optimizer runs properly
        optimizer = Optimizer(parameters, offers, demands, discounts)
        optimizer.initialize_preprocessor()
        assert optimizer.initialize_model(filters) == True
        with pytest.raises(RecordedError) as e:
            optimizer.run()


def test_min_suppliers_region(state_object, params):
    for region in BASELINE['suppliers'].keys():
        # Get data
        offers, demands, discounts = state_object()
        demands_header = demands.header
        filters = [demands_header.byname['region'].id]
        
        # Set constraint
        if region == 'All':
            region_constraint = demands[demands_header.byname['region'].id].unique()
        else:
            region_constraint = [region]
        if BASELINE['suppliers'][region] == BASELINE['nbr_suppliers_offers'][region]:
            continue
        else:
            min_suppliers = BASELINE['suppliers'][region] + 1

        parameters = params()
        parameters['number_of_suppliers'] = [{'filters':{'region':region_constraint}, 'value':min_suppliers, 'mutation': 'for_all', 'operation':'min'}]

        # Check that the optimizer runs properly
        optimizer = Optimizer(parameters, offers, demands, discounts)
        optimizer.initialize_preprocessor()
        assert optimizer.initialize_model(filters) == True
        allocations_table = optimizer.run()
        allocations_table = add_demand_column(allocations_table, demands, 'region')
        alloc_header = allocations_table.header

        # Check that the constraint is respected
        nbr_suppliers = len(allocations_table[allocations_table[alloc_header.byname['region'].id] == region][alloc_header['supplier']].unique())
        assert nbr_suppliers >= min_suppliers, f'Region : {region}'


def test_min_suppliers_region_invalid(state_object, params):
    # Get data
    offers, demands, discounts = state_object()
    demands_header = demands.header
    filters = [demands_header.byname['region'].id]

    # Set constraint
    min_suppliers = 100
    region = 'Asia'

    parameters = params()
    parameters['number_of_suppliers'] = [{'filters':{'region':[region]}, 'value':min_suppliers, 'mutation': 'for_all', 'operation':'min'}]

    # Check that the optimizer runs properly
    optimizer = Optimizer(parameters, offers, demands, discounts)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(filters) == True
    with pytest.raises(RecordedError) as e:
        optimizer.run()


def test_max_suppliers_multi_filters(state_object, params):
    # Get data
    offers, demands, discounts = state_object()
    demands_header = demands.header
    filters = [demands_header.byname['region'].id, demands_header['Type']]

    # Set constraint
    max_suppliers = 3
    region = 'Asia'
    typ = 'Standard'

    parameters = params()
    parameters['number_of_suppliers'] = [{'filters':{'region': [region], 'Type': [typ]}, 'value':max_suppliers, 'mutation': 'for_all', 'operation':'max'}]

    # Check that the optimizer runs properly
    optimizer = Optimizer(parameters, offers, demands, discounts)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(filters) == True
    allocations_table = optimizer.run()
    allocations_table = add_demand_column(allocations_table, demands, 'region')
    allocations_table = add_demand_column(allocations_table, demands, 'Type')
    alloc_header = allocations_table.header

    # Check that the constraint is respected
    nbr_suppliers = len(allocations_table[(allocations_table[alloc_header.byname['region'].id] == region) & (allocations_table[alloc_header['Type']] == typ)][alloc_header['supplier']].unique())
    assert nbr_suppliers <= max_suppliers, f'Region : {region}, Type: {typ}'


def test_max_suppliers_multi_region(state_object, params):
    # Get data
    offers, demands, discounts = state_object()
    demands_header = demands.header
    filters = [demands_header.byname['region'].id, demands_header['Type']]

    # Set constraint
    max_suppliers = 3
    regions = ['Asia', 'Europe']
    typ = 'Standard'

    parameters = params()
    parameters['number_of_suppliers'] = [{'filters':{'region': regions, 'Type': [typ]}, 'value':max_suppliers, 'mutation': 'for_all', 'operation':'max'}]

    # Check that the optimizer runs properly
    optimizer = Optimizer(parameters, offers, demands, discounts)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(filters) == True
    allocations_table = optimizer.run()
    allocations_table = add_demand_column(allocations_table, demands, 'region')
    allocations_table = add_demand_column(allocations_table, demands, 'Type')
    alloc_header = allocations_table.header

    # Check that the constraint is respected
    nbr_suppliers = len(allocations_table[(allocations_table[alloc_header.byname['region'].id].isin(regions)) & (allocations_table[alloc_header['Type']] == typ)][alloc_header['supplier']].unique())
    assert nbr_suppliers <= max_suppliers, f'Region : {regions}, Type: {typ}'


def test_min_suppliers_multi_region(state_object, params):
    # Get data
    offers, demands, discounts = state_object()
    demands_header = demands.header
    filters = [demands_header.byname['region'].id, demands_header['Type']]

    # Set constraint
    min_suppliers = 6
    region = 'Asia'
    typ = 'Standard'

    parameters = params()
    parameters['number_of_suppliers'] = [{'filters':{'region': [region], 'Type': [typ]}, 'value':min_suppliers, 'mutation': 'for_all', 'operation':'min'}]

    # Check that the optimizer runs properly
    optimizer = Optimizer(parameters, offers, demands, discounts)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(filters) == True
    allocations_table = optimizer.run()
    allocations_table = add_demand_column(allocations_table, demands, 'region')
    allocations_table = add_demand_column(allocations_table, demands, 'Type')
    alloc_header = allocations_table.header

    # Check that the constraint is respected
    nbr_suppliers = len(allocations_table[(allocations_table[alloc_header.byname['region'].id] == region) & (allocations_table[alloc_header['Type']] == typ)][alloc_header['supplier']].unique())
    assert nbr_suppliers >= min_suppliers, f'Region : {region}, Type: {typ}'
