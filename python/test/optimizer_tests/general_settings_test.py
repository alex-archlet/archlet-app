from .utils import *
from .optimizer_test_baseline import BASELINE, add_demand_column
from math import isclose

""" ------------------------------ WITH/WITHOUT REBATES TESTS ------------------------------ """

def test_without_rebates(state_object, params):
    offers, demands, discounts = state_object()
    filters = [demands.header.byname['region'].id]
    parameters = params()
    parameters['general_settings']['rebates'] = False
    optimizer = Optimizer(parameters, offers, demands, discounts)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(filters) == True
    allocations_table = optimizer.run()

    alloc_header = allocations_table.header
    alloc_header.add_column('cost', 'cost', '')
    allocations_table[alloc_header['cost']] = allocations_table[alloc_header[UKEYS.TOTAL_VOLUME]].multiply(allocations_table[alloc_header['unit_price']])
    total_cost = allocations_table[alloc_header['cost']].sum()

    assert total_cost == BASELINE['total_cost_without_rebates']

@pytest.mark.skip(reason="Update input format")
def test_with_rebates(state_object, params):
    offers, demands, discounts = state_object()
    filters = [demands.header.byname['region'].id]
    parameters = params()
    optimizer = Optimizer(parameters, offers, demands, discounts)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(filters) == True
    allocations_table = optimizer.run()

    allocations_table['cost'] = allocations_table[UKEYS.TOTAL_VOLUME].multiply(allocations_table['unit_price'])
    total_cost = allocations_table['cost'].sum()

    baseline = BASELINE['total_cost_without_rebates']

    assert total_cost < baseline,\
            f'Total cost with rebates {total_cost} is superior to the cost without rebates {baseline}'


def test_lower_offered_volume(state_object_lower_volumes, params):
    offers, demands, discounts = state_object_lower_volumes()
    filters = [demands.header.byname['region'].id]
    parameters = params()
    print(offers.header.types)
    optimizer = Optimizer(parameters, offers, demands, discounts)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(filters) == True
    allocations_table = optimizer.run()
    alloc_header = allocations_table.header
    assert allocations_table[(allocations_table[alloc_header[UKEYS.SUPPLIER]] == 'Clariant') & (allocations_table[alloc_header[UKEYS.DEMAND_ID]] == 0)][alloc_header[UKEYS.TOTAL_VOLUME]].values[0] <= 10

""" ------------------------------ CAPACITY TESTS ------------------------------ """
@pytest.mark.skip(reason="There's a lot to update for this test")
def test_partial_allocation_and_capacities(state_object_capacities, params):
    offers, demands, discounts, capacities = state_object_capacities()
    filters = [demands.header.byname['region'].id]
    parameters = params()
    parameters['general_settings']['rebates'] = False
    parameters['general_settings']['capacity'] = True
    parameters['general_settings']['split_items'] = False
    parameters['general_settings']['partial_allocation'] = True

    parameters['general_settings']['capacities'] = [capacities.header.byid[col_id].original_name for col_id in capacities.header[KEYS.CAPACITY]]

    optimizer = Optimizer(parameters, offers, demands, discounts, capacities)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(filters) == True
    allocations_table = optimizer.run()

    capacity_names = [capacities.header.byid[col_id].original_name for col_id in capacities.header[KEYS.CAPACITY]]

    for capacity in capacity_names:
        allocations_table = add_demand_column(allocations_table, offers, capacity, 'offer_id')

    allocations = allocations_table.data
    alloc_header = allocations_table.header

    alloc_capacities = alloc_header[KEYS.CAPACITY]

    allocations[capacity_names] = allocations[alloc_capacities].replace('', 0).multiply(allocations[alloc_header[UKEYS.TOTAL_VOLUME]], axis='index')

    temp_df = allocations.groupby(alloc_header['supplier'])[capacity_names].sum()

    renamer = {col_id: col_name for col_id, col_name in zip(capacity_cols, capacity_names)}
    capacities.data = capacities.data.rename(columns=renamer)
    capacities.trim()
    cap = capacities.data.set_index(capacities.header['supplier'])[capacity_names].reindex(temp_df.index)

    assert (temp_df <= cap).eq(True).all(axis=None)


def test_price_comp_optimization(state_object, params):
    offers, demands, discounts = state_object()
    filters = [demands.header.byname['region'].id]
    header = offers.header
    parameters = params()
    parameters['general_settings']['price_component'] = ['Logistics Cost']

    optimizer = Optimizer(parameters, offers, demands, discounts)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(filters) == True
    allocations_table = optimizer.run()
    alloc_header = allocations_table.header
    alloc_header.add_column('cost', 'cost', '')
    allocations_table[alloc_header['cost']] = allocations_table[alloc_header[UKEYS.TOTAL_VOLUME]].multiply(allocations_table[alloc_header['unit_price']])
    total_cost = allocations_table[alloc_header['cost']].sum()

    baseline = BASELINE['total_cost_with_rebates']

    assert total_cost > baseline


def test_flat_discounts(state_flat_discounts, params):
    offers, demands, discounts = state_flat_discounts()
    filters = [demands.header.byname['region'].id]
    parameters = params()
    optimizer = Optimizer(parameters, offers, demands, discounts)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(filters) == True
    allocations_table = optimizer.run()

    alloc_header = allocations_table.header
    allocations_table['cost'] = allocations_table[alloc_header[UKEYS.TOTAL_VOLUME]].multiply(allocations_table['unit_price'])
    total_cost = allocations_table['cost'].sum()

    baseline = BASELINE['total_cost_with_rebates']

    assert isclose(baseline-100000, total_cost, rel_tol=0.01)
