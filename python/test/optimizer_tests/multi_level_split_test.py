from .utils import *
from .optimizer_test_baseline import add_demand_column
from engine.src.table_processors import Filter


""" ------------------------------ MULTI-LEVEL SPLITS ------------------------------ """


@pytest.mark.skip(reason="Currently not used")
def test_volume_constraints_valid_min_supplier(state_object, params):
    # volume constraint dict
    max_volume = 1000
    min_volume = 50
    n_min_supplier = 2
    volume_constraints_dict = {
        'max_volume': max_volume,
        'min_volume': min_volume,
        'constraint': 'min_supplier',
        'value': n_min_supplier,
        'filters':{}
    }

    #for region in BASELINE['suppliers'].keys():
    for region in ['Europe']:
        volume_constraints_dict['filters'].byname['region'].id = region

        # get parameters
        parameters = params(volume_constraints_dict, 'multi_splits')
        parameters['general_settings']['split_items'] = True
        parameters['general_settings']['capacity'] = False

        offers, demands, discounts = state_object()
        filters = [demands.header.byname['region'].id]

        # Check that optimizer runs properly
        optimizer = Optimizer(parameters, offers, demands, discounts)
        optimizer.initialize_preprocessor()
        assert optimizer.initialize_model(filters) == True

        allocations_table = optimizer.run()
        alloc_header = allocations_table.header
        allocations_table = add_demand_column(allocations_table, demands, 'region')
        header = offers.header

        # Check if constraints were satisified
        mask = (allocations_table[alloc_header.byname['region'].id] == region) &\
               (allocations_table[alloc_header[UKEYS.OFFER_ID]].isin(offers[(offers[header[KEYS.VOLUME]] <= max_volume) &
                                                                (offers[header[KEYS.VOLUME]] >= min_volume)][header[UKEYS.OFFER_ID]]))

        allocations_table = allocations_table[mask]

        # If satisfied, every item would at least appear n_min_supplier times in allocation table
        valid = all(allocations_table[alloc_header[UKEYS.ITEM_ID]].value_counts() >= n_min_supplier)
        assert valid == True


@pytest.mark.skip(reason="Currently not used")
def test_volume_constraints_valid_max_percentage(state_object, params):
    # volume constraint dict
    max_volume = 1000
    min_volume = 50
    max_percentage = 90
    volume_constraints_dict = {
        'max_volume': max_volume,
        'min_volume': min_volume,
        'constraint': 'max_percentage',
        'value': max_percentage,
        'filters': {}
    }

    #for region in BASELINE['suppliers'].keys():
    for region in ['Europe']:
        volume_constraints_dict['filters'].byname['region'].id = region

        # get parameters
        parameters = params(volume_constraints_dict, 'multi_splits')
        parameters['general_settings']['split_items'] = True
        parameters['general_settings']['capacity'] = False

        offers, demands, discounts = state_object()
        filters = [demands.header.byname['region'].id]

        # Check that optimizer runs properly
        optimizer = Optimizer(parameters, offers, demands, discounts)
        optimizer.initialize_preprocessor()
        assert optimizer.initialize_model(filters) == True

        allocations_table = optimizer.run()
        alloc_header = allocations_table.header
        allocations_table = add_demand_column(allocations_table, demands, 'region')
        header = offers.header

        # Check if constraints were satisified
        mask = (allocations_table[alloc_header.byname['region'].id] == region) &\
               (allocations_table[alloc_header[UKEYS.OFFER_ID]].isin(offers[(offers[header[KEYS.VOLUME]] <= max_volume) &
                                                                (offers[header[KEYS.VOLUME]] >= min_volume)][header[UKEYS.OFFER_ID]]))

        allocations_table = allocations_table[mask]

        # If max percentage is set, all affected itesm would at least have
        # two different suppliers
        for offer_id, volume in allocations_table[alloc_header[[UKEYS.OFFER_ID, KEYS.VOLUME]]].values:
            percentage = (volume/float(offers[offers[header[UKEYS.OFFER_ID]] == offer_id][header[KEYS.VOLUME]].values))*100
            assert percentage <= max_percentage*1.01


@pytest.mark.skip(reason="Currently not used")
def test_volume_constraints_valid_min_percentage(state_object, params):
    # volume constraint dict
    max_volume = 1000
    min_volume = 50
    min_percentage = 30
    volume_constraints_dict = {
        'max_volume': max_volume,
        'min_volume': min_volume,
        'constraint': 'min_percentage',
        'value': min_percentage,
        'filters':{}
    }

    n_min_supplier = 2
    volume_constraints_supp = {
        'max_volume': max_volume,
        'min_volume': min_volume,
        'constraint': 'min_supplier',
        'value': n_min_supplier,
        'filters':{}
    }

    #for region in BASELINE['suppliers'].keys():
    for region in ['Europe']:
        volume_constraints_dict['filters'].byname['region'].id = region
        volume_constraints_supp['filters'].byname['region'].id = region

        # get parameters
        parameters = params(volume_constraints_dict, 'multi_splits')
        parameters['multi_splits'].append(volume_constraints_supp)
        parameters['general_settings']['split_items'] = True
        parameters['general_settings']['capacity'] = False

        offers, demands, discounts = state_object()
        filters = [demands.header.byname['region'].id]

        # Check that optimizer runs properly
        optimizer = Optimizer(parameters, offers, demands, discounts)
        optimizer.initialize_preprocessor()
        assert optimizer.initialize_model(filters) == True

        allocations_table = optimizer.run()
        alloc_header = allocations_table.header
        allocations_table = add_demand_column(allocations_table, demands, 'region')

        # Check if constraints were satisified
        header = offers.header
        mask = (allocations_table[alloc_header.byname['region'].id] == region) &\
               (allocations_table[alloc_header[UKEYS.OFFER_ID]].isin(offers[(offers[header[KEYS.VOLUME]] <= max_volume) &
                                                                (offers[header[KEYS.VOLUME]] >= min_volume)][header[UKEYS.OFFER_ID]]))

        allocations_table = allocations_table[mask]

        # If max percentage is set, all affected items would at least have
        # two different suppliers
        for offer_id, volume in allocations_table[alloc_header[[UKEYS.OFFER_ID, KEYS.VOLUME]]].values:
            percentage = (volume/float(offers[offers[header[UKEYS.OFFER_ID]] == offer_id][header[KEYS.VOLUME]].values))*100
            assert percentage >= min_percentage*0.99



@pytest.mark.skip(reason="Currently not used")
def test_volume_constraints_invalid(state_object, params):
    # invalid volume constraint dict
    max_volume = 1000
    min_volume = 50
    volume_constraints_dict = {
        'max_volume': max_volume,
        'min_volume': min_volume,
        'constraint': 'min_supplier',
        'value': 5,
        'filters': {'region': 'Europe'}
    }

    # get parameters
    parameters = params(volume_constraints_dict, 'multi_splits')

    offers, demands, discounts = state_object()
    filters = [demands.header.byname['region'].id]

    # Check that optimizer intialization runs properly
    optimizer = Optimizer(parameters, offers, demands, discounts)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(filters) == True

    # Check that invalidation is encountered
    with pytest.raises(RecordedError) as e:
        optimizer.run()
