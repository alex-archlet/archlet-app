from .utils import *
from engine.src.table_processors import Filter
from shared.constants import CONSTRAINT_TYPES
from shared.constants import UKEYS, KEYS
from shared.constants import OPERATION_TYPE
from shared.constants import UNITS

""" ------------------------------ VOLUME PER SUPPLIER TESTS ------------------------------ """

@pytest.mark.skip(reason="Currently not used")
def test_volume_per_supplier_min_spend(state_object, params):

    offers, demands, discounts = state_object()
    filters=[demands.header.byname['region'].id]
    parameters = params()

    item = "Item_0001"
    supplier = "Clariant"
    min_value = 100

    parameters[CONSTRAINT_TYPES.VOLUME_PER_SUPPLIER].append({UKEYS.ITEM: item,
                                                             UKEYS.SUPPLIER: supplier,
                                                             UNITS.UNIT: UNITS.SPEND,
                                                             OPERATION_TYPE.OPERATION: OPERATION_TYPE.MIN,
                                                             KEYS.VALUE: min_value})

    optimizer = Optimizer(parameters, offers, demands, discounts)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(filters) == True
    allocations_table = optimizer.run()
    alloc_header = allocations_table.header

    costs = allocations_table[alloc_header['volume']].multiply(allocations_table[alloc_header['unit_price']])

    item_supp_alloc = costs[(allocations_table[alloc_header[UKEYS.ITEM_ID]] == item) & (allocations_table[alloc_header[UKEYS.SUPPLIER]] == supplier)]

    assert not item_supp_alloc.empty

    assert item_supp_alloc.sum() > 0.99*min_value


@pytest.mark.skip(reason="Currently not used")
def test_volume_per_supplier_min_percentage(state_object, params):

    offers, demands, discounts = state_object()
    filters=[demands.header.byname['region'].id]
    parameters = params()

    item = "Item_0002"
    supplier = "DuPont"
    min_value = 10

    parameters[CONSTRAINT_TYPES.VOLUME_PER_SUPPLIER].append({UKEYS.ITEM: item,
                                                             UKEYS.SUPPLIER: supplier,
                                                             UNITS.UNIT: UNITS.PERCENTAGE,
                                                             OPERATION_TYPE.OPERATION: OPERATION_TYPE.MIN,
                                                             KEYS.VALUE: min_value})

    optimizer = Optimizer(parameters, offers, demands, discounts)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(filters) == True
    allocations_table = optimizer.run()
    alloc_header = allocations_table.header

    costs = allocations_table[alloc_header['volume']].multiply(allocations_table[alloc_header['unit_price']])

    item_supp_alloc = costs[(allocations_table[alloc_header[UKEYS.ITEM_ID]] == item) & (allocations_table[alloc_header[UKEYS.SUPPLIER]] == supplier)]

    assert not item_supp_alloc.empty

    assert item_supp_alloc.sum() > 0.99*min_value


@pytest.mark.skip(reason="Currently not used")
def test_volume_per_supplier_min_volume(state_object, params):

    offers, demands, discounts = state_object()
    filters=[demands.header.byname['region'].id]
    parameters = params()

    item = "Item_0003"
    supplier = "BASF"
    min_value = 10

    parameters[CONSTRAINT_TYPES.VOLUME_PER_SUPPLIER].append({UKEYS.ITEM: item,
                                                             UKEYS.SUPPLIER: supplier,
                                                             UNITS.UNIT: UNITS.VOLUME,
                                                             OPERATION_TYPE.OPERATION: OPERATION_TYPE.MIN,
                                                             KEYS.VALUE: min_value})

    optimizer = Optimizer(parameters, offers, demands, discounts)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(filters) == True
    allocations_table = optimizer.run()
    alloc_header = allocations_table.header

    costs = allocations_table[alloc_header['volume']].multiply(allocations_table[alloc_header['unit_price']])

    item_supp_alloc = costs[(allocations_table[alloc_header[UKEYS.ITEM_ID]] == item) & (allocations_table[alloc_header[UKEYS.SUPPLIER]] == supplier)]

    assert not item_supp_alloc.empty

    assert item_supp_alloc.sum() > 0.99*min_value
