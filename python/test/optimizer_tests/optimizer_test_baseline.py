import pandas as pd

from collections import defaultdict
from engine.src.optimization import Optimizer
from engine.src.table_processors import Preprocessor
from shared.communication import IOFacade
from shared.communication.database_connection import DBFile as File
from shared.constants import UKEYS, KEYS
from shared.constants import RecordedError
from shared.entities import TableData

BASELINE = {}

def initialize_state():
    iofacade = IOFacade(0, './')
    file_path = './resources'
    name = 'offers_table'
    version = 3
    extension = '.csv'
    file = File(file_path, name, version, extension)
    offers = TableData()
    iofacade.load_table(offers, file)
    offers.header.set_type(['Logistics Cost', 'Material Cost', 'Production Cost'], KEYS.PRICE)
    name = 'demands_table'
    version = 1
    extension = '.csv'
    file = File(file_path,name,version,extension)
    demands = TableData()
    iofacade.load_table(demands, file)
    name = 'discounts_table'
    version = 2
    extension = '.csv'
    file = File(file_path,name,version,extension)
    discounts = TableData()
    iofacade.load_table(discounts, file)

    preprocessor = Preprocessor()
    offers.add_uuid_names()
    demands.add_uuid_names()
    discounts.add_uuid_names()
    offers = preprocessor.process_table(offers, 'offers')
    demands = preprocessor.process_table(demands, 'demands')
    discounts = preprocessor.process_table(discounts, 'discounts')
    discounts = preprocessor.clean_discounts_table(discounts, ['volume'], ['unit', 'percentage'], offers.header)

    solver = 'CBC'

    return offers, demands, discounts

def add_demand_column(table, demands_table, col_name, id_column='demand_id'):
    header = table.header
    demands_header = demands_table.header
    header.add_column(col_name, col_id=demands_header.byname[col_name].id, original_name=col_name)
    demands_table.data.set_index(demands_header[id_column], inplace=True)
    table.data.set_index(header[id_column], inplace=True)
    table[header[col_name]] = demands_table[demands_header[col_name]]
    table.data.reset_index(inplace=True)
    table.header = header
    demands_table.data.reset_index(inplace=True)
    return table

params = {"name":"Test","general_settings":{"rebates":False, "capacity":False, "split_items":True, "partial_allocation":False},"price_split":[],"max_suppliers":{},
"scenario_type":"custom","excluded_suppliers":[],"volume_constraints":[],
"max_spend_percentage":{"spend":{},"volume":{},"capacity":{},"percentage":{}}}

offers_table, demands, discounts = initialize_state()

optimizer = Optimizer(params, offers_table, demands, discounts)
optimizer.initialize_preprocessor()
demands_header = demands.header
filters=[demands_header.byname['region'].id]
print('---------------')
print(demands_header.byname['region'].id)
print('---------------')
optimizer.initialize_model(filters)
allocations_table = optimizer.run()

header = allocations_table.header
header.add_column('cost', 'cost', '')
allocations_table[header['cost']] = allocations_table[header[UKEYS.TOTAL_VOLUME]].multiply(allocations_table[header['unit_price']])
TOTAL_COST_NO_REBATES = allocations_table[header['cost']].sum()

BASELINE['total_cost_without_rebates'] = TOTAL_COST_NO_REBATES

del optimizer

NBR_SUPPLIERS_OFFERS = {}
SUPPLIERS_OFFERS = {}
VOLUME_AVAILABLE = defaultdict(dict)
MAX_POSSIBLE_SPEND = defaultdict(dict)

VOLUME_AVAILABLE['All'] = defaultdict(lambda : 0)


offers_header = offers_table.header
offers_header.add_column(KEYS.UNKNOWN, col_id=demands_header.byname['region'].id, original_name='region')
print(offers_header.names)
print(offers_header.byname['region'].id)
print(offers_table.data.keys())
for region in offers_table[offers_header.byname['region'].id].unique():
    offers_region = offers_table[(offers_table[offers_header.byname['region'].id] == region)]
    for supplier in offers_region[offers_header['supplier']].unique():
        VOLUME_AVAILABLE['All'][supplier] += offers_region[offers_region[offers_header['supplier']] == supplier][offers_header[UKEYS.TOTAL_VOLUME]].sum()
        VOLUME_AVAILABLE[region][supplier] = offers_region[offers_region[offers_header['supplier']] == supplier][offers_header[UKEYS.TOTAL_VOLUME]].sum()
        MAX_POSSIBLE_SPEND[region][supplier] = offers_region[offers_region[offers_header['supplier']] == supplier][offers_header['raw_price']].multiply(offers_region[offers_region[offers_header['supplier']] == supplier][offers_header[UKEYS.TOTAL_VOLUME]]).sum()
    NBR_SUPPLIERS_OFFERS[region] = len(offers_table[offers_header['supplier']][offers_table[offers_header.byname['region'].id] == region].unique())
    NBR_SUPPLIERS_OFFERS['All'] = len(offers_table[offers_header['supplier']].unique())
    SUPPLIERS_OFFERS[region] = offers_table[offers_header['supplier']][offers_table[offers_header.byname['region'].id] == region].unique()
    SUPPLIERS_OFFERS['All'] = offers_table[offers_header['supplier']].unique()

BASELINE['nbr_suppliers_offers'] = NBR_SUPPLIERS_OFFERS
BASELINE['list_suppliers_offers'] = SUPPLIERS_OFFERS
BASELINE['volume_available'] = VOLUME_AVAILABLE
BASELINE['max_possible_spend'] = MAX_POSSIBLE_SPEND

offers, demands, discounts = initialize_state()
params['general_settings']['rebates'] = True

optimizer = Optimizer(params, offers, demands, discounts)
optimizer.initialize_preprocessor()
demands_header = demands.header
filters=[demands_header.byname['region'].id]
optimizer.initialize_model(filters)
allocations_table = optimizer.run()

header = allocations_table.header
header.add_column('cost', 'cost', '')
allocations_table[header['cost']] = allocations_table[header[UKEYS.TOTAL_VOLUME]].multiply(allocations_table[header['unit_price']])

TOTAL_COST_WITH_REBATES = allocations_table[header['cost']].sum()

BASELINE['total_cost_with_rebates'] = TOTAL_COST_WITH_REBATES

SPEND_SUPPLIER_REGION = defaultdict(dict)
PERCENTAGE_SUPPLIER_REGION = defaultdict(dict)
VOLUME_SUPPLIER_REGION = defaultdict(dict)
SUPPLIERS = {}

SUPPLIERS['All'] = len(allocations_table[header['supplier']].unique())
allocations_table = add_demand_column(allocations_table, demands, 'region')

print(allocations_table[demands_header.byname['region'].id])

for region in allocations_table[header.byname['region'].id].unique():
    allocations_region = allocations_table[allocations_table[header.byname['region'].id] == region]
    suppliers_region = allocations_region[header['supplier']].unique()
    SUPPLIERS[region] = len(suppliers_region)
    for supplier in suppliers_region:
        allocations_supplier_region = allocations_region[allocations_region[header['supplier']] == supplier]
        SPEND_SUPPLIER_REGION[region][supplier] = allocations_supplier_region[header['cost']].sum()
        VOLUME_SUPPLIER_REGION[region][supplier] = allocations_supplier_region[header[UKEYS.TOTAL_VOLUME]].sum()
        PERCENTAGE_SUPPLIER_REGION[region][supplier] = (SPEND_SUPPLIER_REGION[region][supplier]/TOTAL_COST_WITH_REBATES)*100
    SPEND_SUPPLIER_REGION[region]['Every-Supplier'] = allocations_region[header['cost']].sum()
    VOLUME_SUPPLIER_REGION[region]['Every-Supplier'] = allocations_region[header[UKEYS.TOTAL_VOLUME]].sum()
    PERCENTAGE_SUPPLIER_REGION[region]['Every-Supplier'] = (SPEND_SUPPLIER_REGION[region]['Every-Supplier']/TOTAL_COST_WITH_REBATES)*100


for supplier in allocations_table[header['supplier']].unique():
    VOLUME_SUPPLIER_REGION['All'][supplier] = sum([vol_region[supplier] for vol_region in VOLUME_SUPPLIER_REGION.values() if supplier in vol_region.keys()])
    SPEND_SUPPLIER_REGION['All'][supplier] = sum([spend_region[supplier] for spend_region in SPEND_SUPPLIER_REGION.values() if supplier in spend_region.keys()])
    PERCENTAGE_SUPPLIER_REGION['All'][supplier] = sum([percentage_region[supplier] for percentage_region in PERCENTAGE_SUPPLIER_REGION.values() if supplier in percentage_region.keys()])

BASELINE['volume'] = VOLUME_SUPPLIER_REGION
BASELINE['spend'] = SPEND_SUPPLIER_REGION
BASELINE['percentage'] = PERCENTAGE_SUPPLIER_REGION
BASELINE['suppliers'] = SUPPLIERS
