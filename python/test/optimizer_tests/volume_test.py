from .utils import *
from .optimizer_test_baseline import BASELINE, add_demand_column


""" ------------------------------ VOLUME TESTS ------------------------------ """

def test_max_volume(state_object, params):
    for region in BASELINE['volume'].keys():
        for supplier in BASELINE['volume'][region].keys():
            if region == 'NorthAmerica' and supplier == 'INEOS': # TODO : debug this exception
                continue
            if supplier == 'Every-Supplier':
                continue
            max_volume = BASELINE['volume'][region][supplier] * (4/5)
            parameters = params()
            parameters['limit_spend'] = [{'filters':{'region':[region], 'supplier':[supplier]}, 'unit':'volume', 'value':max_volume, 'operation':'max'}]
            offers, demands, discounts = state_object()
            filters=[demands.header.byname['region'].id]
            optimizer = Optimizer(parameters, offers, demands, discounts)
            optimizer.initialize_preprocessor()
            assert optimizer.initialize_model(filters) == True
            allocations_table = optimizer.run()
            allocations_table = add_demand_column(allocations_table, demands, 'region')
            alloc_header = allocations_table.header

            volumes = allocations_table[alloc_header['total_volume']]
            if region != 'All':
                volumes_region_supplier = volumes[(allocations_table[alloc_header.byname['region'].id] == region) & (allocations_table[alloc_header['supplier']] == supplier)]
            else:
                volumes_region_supplier = volumes[allocations_table[alloc_header['supplier']] == supplier]
            total_volume_region_supplier = volumes_region_supplier.sum()
            relative_error = (total_volume_region_supplier - max_volume)*100/max_volume

            assert relative_error < 1,\
                   f'Volume of {supplier} in {region} is {relative_error} superior to the max volume'
            del parameters


def test_min_volume(state_object, params):
    for region in BASELINE['volume'].keys():
        for supplier in BASELINE['volume'][region].keys():
            # Get data
            offers, demands, discounts = state_object()
            demands_header = demands.header
            filters = [demands_header.byname['region'].id]

            # Set constraints
            if supplier == 'Every-Supplier':
                continue
            min_volume = BASELINE['volume'][region][supplier] * (5/4)
            if min_volume >= BASELINE['volume_available'][region][supplier] - 0.1:
                continue
            parameters = params()
            parameters['limit_spend'] = [{'filters':{'region': [region], 'supplier': [supplier]}, 'unit':'volume', 'mutation': 'for_all', 'value':min_volume, 'operation':'min'}]
            
            # Run optimization
            optimizer = Optimizer(parameters, offers, demands, discounts)
            optimizer.initialize_preprocessor()
            assert optimizer.initialize_model(filters) == True
            allocations_table = optimizer.run()
            allocations_table = add_demand_column(allocations_table, demands, 'region')
            alloc_header = allocations_table.header

            volumes = allocations_table[alloc_header['total_volume']]
            if region != 'All':
                volumes_region_supplier = volumes[(allocations_table[alloc_header.byname['region'].id] == region) & (allocations_table[alloc_header['supplier']] == supplier)]
            else:
                volumes_region_supplier = volumes[(allocations_table[alloc_header['supplier']] == supplier)]
            total_volume_region_supplier = volumes_region_supplier.sum()
            relative_error = (min_volume - total_volume_region_supplier)*100/min_volume

            assert relative_error < 1,\
                   f'Volume of {supplier} in {region} is {relative_error}% inferior to the min volume ({total_volume_region_supplier} for {min_volume} wanted)'
            del parameters

def test_min_volume_region_invalid(state_object, params):
    # Get data
    offers, demands, discounts = state_object()
    demands_header = demands.header
    filters = [demands_header.byname['region'].id]

    # Set constraint
    min_volume = 1000000
    region = 'Asia'
    supplier = 'SABIC'
    parameters = params()
    parameters['limit_spend'] = [{'filters':{'region':[region], 'supplier':[supplier]}, 'unit':'volume', 'value':min_volume, 'operation':'min'}]

    # Run optimization
    optimizer = Optimizer(parameters, offers, demands, discounts)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(filters) == True
    # assert optimizer.validate_constraints() == False # should it be false or not
    with pytest.raises(RecordedError) as e:
        optimizer.run()


def test_max_volume_global_invalid(state_object, params):
    # Get data
    offers, demands, discounts = state_object()
    demands_header = demands.header
    filters = [demands_header.byname['region'].id]

    # Set constraint 
    max_volume = 0
    # 3 suppliers needed to cause invalidity : all items are available from at least 3 suppliers
    all_regions = demands[demands_header.byname['region'].id].unique()
    suppliers = ['Clariant', 'DuPont', 'SABIC']
    parameters = params()
    parameters['limit_spend'] = [{'filters':{'region': all_regions, 'supplier': suppliers}, 'unit':'volume', 'value':max_volume, 'operation':'max'}]

    # Run optimization
    optimizer = Optimizer(parameters, offers, demands, discounts)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(filters) == True
    with pytest.raises(RecordedError) as e:
        optimizer.run()

def test_min_volume_global_invalid(state_object, params):
    # Get data
    offers, demands, discounts = state_object()
    demands_header = demands.header
    filters = [demands_header.byname['region'].id]

    # Set constraint
    all_regions = demands[demands_header.byname['region'].id].unique()
    supplier = 'BASF'
    min_volume = 1000000000
    parameters = params()
    parameters['limit_spend'] = [{'filters':{'region': all_regions, 'supplier': [supplier]}, 'unit':'volume', 'value':min_volume, 'operation':'min'}]

    # Run optimization
    optimizer = Optimizer(parameters, offers, demands, discounts)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(filters) == True
    with pytest.raises(RecordedError) as e:
        optimizer.run()

# @pytest.mark.skip(reason='Everysupplier not implemented yet for volumes')
# def test_min_volume_everysupplier(state_object, params):
# 	min_volume = 2000
# 	region = 'Asia'
# 	parameters = params({region:{'Every-Supplier':{'at_least':min_volume}, 'Reliance':{'at_least':100}}}, 'max_spend_percentage', 'volume')
# 	state, offers, demands, discounts = state_object()
# 	optimizer = Optimizer(state, parameters, offers, demands, discounts)
# 	optimizer.initialize_preprocessor()
# 	assert optimizer.initialize_model() == True
# 	allocations_table = optimizer.run()
#
# 	allocations_region = allocations_table[allocations_table.byname['region'].id == region]
# 	suppliers = allocations_region['supplier'].unique()
# 	for supplier in suppliers:
# 		volume_supplier_region = allocations_region[allocations_region['supplier'] == supplier]['volume'].sum()
# 		if supplier == 'Reliance':
# 			assert volume_supplier_region >= 99 # TODO
# 		else:
# 			assert volume_supplier_region >= min_volume*99/100 # TODO
