from .utils import *
from engine.src.table_processors import Filter
from shared.entities.project_settings import ProjectSettings


""" ------------------------------ EXCLUDE SUPPLIER ------------------------------ """
def test_exclude_supplier(state_object, params):
    offers, demands, discounts = state_object()
    demands_header = demands.header
    filters = [demands_header.byname['item'].id]
    excluded_supplier = 'SABIC'

    parameters = params()
    parameters['excluded_suppliers'] = [ {
        'value': None,
        'filters': {'supplier': [excluded_supplier]},
        'criteria': CONSTRAINT_SPECS.EXCLUDE_ALL,
        'mutation': None,
        'operation': None
    }]


    project_settings = ProjectSettings()
    project_settings.filters = filters
        
    all_params = {'scenario_settings': parameters }

        # get list of regions for supplier
        # Check that the filter runs properly
    job_filter = Filter(offers, None, demands, None)
    assert job_filter.run(all_params, project_settings) == True

        # Check that optimizer runs properly
    optimizer = Optimizer(parameters, offers, demands, discounts, project_settings=project_settings)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(project_settings.filters) == True
    allocations_table = optimizer.run()

    allocated_suppliers = allocations_table.data[allocations_table.header[UKEYS.SUPPLIER]].tolist()

        # Check that excluded supplier is not in allocation
    fail_value = 'The supplier was included in the allocations!'
    assert (excluded_supplier not in allocated_suppliers), f'Failed due to: {fail_value}'


""" ------------------------------ EXCLUDE SUPPLIER FAIL ------------------------------ """
def test_exclude_supplier_fail(state_object, params):
    offers, demands, discounts = state_object()
    demands_header = demands.header
    filters = [demands_header.byname['item'].id]
    # Item_0001 cannot be allocated if all 3 of these are missing
    excluded_suppliers = ['SABIC','DuPont','Clariant']

    parameters = params()
    parameters['excluded_suppliers'] = [ {
        'value': None,
        'filters': {'supplier': excluded_suppliers},
        'criteria': CONSTRAINT_SPECS.EXCLUDE_ALL,
        'mutation': None,
        'operation': None
    }]


    project_settings = ProjectSettings()
    project_settings.filters = filters
        
        
    all_params = {'scenario_settings': parameters }

        # get list of regions for supplier
        # Check that the filter runs properly
    job_filter = Filter(offers, None, demands, None)
    assert job_filter.run(all_params, project_settings) == True

        # Check that optimizer runs properly
    optimizer = Optimizer(parameters, offers, demands, discounts, project_settings=project_settings)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(project_settings.filters) == True
    with pytest.raises(RecordedError) as e:
        optimizer.run()


""" ------------------------------ EXCLUDE SUPPLIER ON MULTI-REGION--------------- """
def test_exclude_supplier_region(state_object, params):
    offers, demands, discounts = state_object()
    demands_header = demands.header
    filters = [demands_header.byname['region'].id]
    excluded_supplier = 'SABIC'

    parameters = params()

    regions_list = ['Asia', 'Europe']
    
    for region in regions_list:
        
        offers, _, _ = state_object()

        project_settings = ProjectSettings()
        project_settings.filters = filters

        parameters['general_settings'] = { 'partial_allocation': True }
        parameters['excluded_suppliers'] = [ {
            'value': None,
            'filters': {
                'supplier': [excluded_supplier],
                'region': [region]
            },
            'criteria': CONSTRAINT_SPECS.EXCLUDE_ALL,
            'mutation': None,
            'operation': None
        }]


        all_params = {'scenario_settings': parameters }

        # get list of regions for supplier
        # Check that the filter runs properly
        job_filter = Filter(offers, None, demands, None)
        assert job_filter.run(all_params, project_settings) == True

        # Check that optimizer runs properly
        optimizer = Optimizer(parameters, offers, demands, discounts, project_settings=project_settings)
        optimizer.initialize_preprocessor()
        assert optimizer.initialize_model(project_settings.filters) == True
        allocations_table = optimizer.run()

        # find demands that are of specific region
        mask_demand = demands.data[filters[0]] == region
        demands_series = demands.data[demands_header[UKEYS.DEMAND_ID]][mask_demand]

        # Find related allocations and check that excluded supplier is not in allocation for the specific regions
        mask_alloc = allocations_table.data[allocations_table.header[UKEYS.DEMAND_ID]].isin(demands_series)
        allocated_suppliers = allocations_table.data[allocations_table.header[UKEYS.SUPPLIER]][mask_alloc].unique().tolist()
        
        fail_value = 'The supplier was included in the allocations for the particular region!'
        assert (excluded_supplier not in allocated_suppliers), f'Failed due to: {fail_value}'


""" ------------------------------ EXCLUDE SUPPLIER ON MULTI-REGION FAIL--------------- """
def test_exclude_supplier_region_fail(state_object, params):
    offers, demands, discounts = state_object()
    demands_header = demands.header
    filters = [demands_header.byname['region'].id]
    excluded_supplier = 'SABIC'

    parameters = params()
    parameters['excluded_suppliers'] = [ {
        'value': None,
        'filters': {},
        'criteria': CONSTRAINT_SPECS.EXCLUDE_ALL,
        'mutation': None,
        'operation': None
    }]

    regions_list = ["NorthAmerica", "Europe", "Asia", "SouthAmerica"]
    
    for region in regions_list:
        offers, _, _ = state_object()

        project_settings = ProjectSettings()
        project_settings.filters = filters
        
        param_filters = {
            'supplier': [excluded_supplier],
            'Region': [region]
        }

        parameters['filters'] = param_filters
        all_params = {'scenario_settings': parameters }

        # get list of regions for supplier
        # Check that the filter runs properly
        job_filter = Filter(offers, None, demands, None)
        assert job_filter.run(all_params, project_settings) == True

        # Check that optimizer runs properly
        optimizer = Optimizer(parameters, offers, demands, discounts, project_settings=project_settings)
        optimizer.initialize_preprocessor()
        assert optimizer.initialize_model(project_settings.filters) == True
        with pytest.raises(RecordedError) as e:
            optimizer.run()


""" ------------------------------ KEEP HIGHEST RISKSCORE --------------- """
def test_exclude_supplier_keep_highest(state_object, params):

    offers, demands, discounts = state_object()
    demands_header = demands.header
    filters = [demands_header.byname['item'].id]
    item = 'Item_0003'

    # Keep highest RiskScore supplier for 'Item_0003'
    select_item_mask = (offers.data[offers.header[UKEYS.ITEM_ID]] == item)
    max_riskscore = offers.data.loc[select_item_mask, offers.header["RiskScore"]].max()
    highest_risk_score_mask = (offers.data[offers.header["RiskScore"]] == max_riskscore)
    supplier_max_riskscore = offers.data.loc[highest_risk_score_mask & select_item_mask, offers.header.byname[UKEYS.SUPPLIER].id].tolist()[0]


    parameters = params()
    parameters['excluded_suppliers'] = [ {
        'value':None,
        'filters': {},
        'criteria':'RiskScore',
        'mutation': None,
        'operation':'keep_highest'
    }]
    
    
    project_settings = ProjectSettings()
    project_settings.filters = filters
    project_settings.soft_constraints = [offers.header.byname[CONSTRAINT_SPECS.RISK_SCORE].id]
    
    
    all_params = {'scenario_settings': parameters }


    # Check that the filter runs properly
    job_filter = Filter(offers, None, demands, None)
    assert job_filter.run(all_params, project_settings) == True

    # Check that optimizer runs properly
    optimizer = Optimizer(parameters, offers, demands, discounts, project_settings=project_settings)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(project_settings.filters) == True
    allocations_table = optimizer.run()
    allocations_table.data = allocations_table.data.merge(demands[demands.header[[UKEYS.ITEM_ID, UKEYS.DEMAND_ID]]], left_on=allocations_table.header[UKEYS.DEMAND_ID], right_on=demands.header[UKEYS.DEMAND_ID])
    allocations_table.header.add_column(UKEYS.ITEM_ID, col_id=demands.header.byid[demands.header[UKEYS.ITEM_ID]].id)
    resulted_supplier = allocations_table.data.loc[allocations_table.data[allocations_table.header[UKEYS.ITEM_ID]] == item,allocations_table.header.byname[UKEYS.SUPPLIER].id].tolist()
    
    # Check if highest RiskScore supplier is in allocation for 'Item_0003'
    fail_value = 'The highest RiskScore supplier was not in the allocation for the desired item.'
    assert supplier_max_riskscore in resulted_supplier, f'Failed due to: {fail_value}'

""" ------------------------------ KEEP HIGHEST RISKSCORE FAIL --------------- """
def test_exclude_supplier_keep_highest_fail(state_object, params):

    offers, demands, discounts = state_object()
    demands_header = demands.header
    filters = [demands_header.byname['item'].id]
    item = 'Item_0003'
    # INEOS has the highest RiskScore, so it should be allocated
    excluded_supplier = 'SABIC'

    # Keep highest RiskScore supplier for 'Item_0003'
    select_item_mask = (offers.data[offers.header[UKEYS.ITEM_ID]] == item)
    max_riskscore = offers.data.loc[select_item_mask, offers.header["RiskScore"]].max()
    highest_risk_score_mask = (offers.data[offers.header["RiskScore"]] == max_riskscore)
    supplier_max_riskscore = offers.data.loc[highest_risk_score_mask & select_item_mask, offers.header.byname[UKEYS.SUPPLIER].id].tolist()[0]


    parameters = params()
    parameters['excluded_suppliers'] = [ {
        'value':None,
        'filters': {'supplier': [excluded_supplier]},
        'criteria':'RiskScore',
        'mutation': None,
        'operation':'keep_highest'
    }]
    
    
    project_settings = ProjectSettings()
    project_settings.filters = filters
    project_settings.soft_constraints = [offers.header.byname[CONSTRAINT_SPECS.RISK_SCORE].id]
    
    
    all_params = {'scenario_settings': parameters }


    # Check that the filter runs properly
    job_filter = Filter(offers, None, demands, None)
    assert job_filter.run(all_params, project_settings) == True

    # Check that optimizer runs properly
    optimizer = Optimizer(parameters, offers, demands, discounts, project_settings=project_settings)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(project_settings.filters) == True
    allocations_table = optimizer.run()
    allocations_table.data = allocations_table.data.merge(demands[demands.header[[UKEYS.ITEM_ID, UKEYS.DEMAND_ID]]], left_on=allocations_table.header[UKEYS.DEMAND_ID], right_on=demands.header[UKEYS.DEMAND_ID])
    allocations_table.header.add_column(UKEYS.ITEM_ID, col_id=demands.header.byid[demands.header[UKEYS.ITEM_ID]].id)
    
    resulted_supplier = allocations_table.data.loc[allocations_table.data[allocations_table.header[UKEYS.ITEM_ID]] == item,allocations_table.header.byname[UKEYS.SUPPLIER].id].tolist()
    
    # Check if highest RiskScore supplier is in allocation for 'Item_0003'
    fail_value = 'The highest RiskScore supplier was in the allocation for the desired item.'
    assert supplier_max_riskscore not in resulted_supplier, f'Failed due to: {fail_value}'


""" ------------------------------ KEEP LOWEST RISKSCORE --------------- """
def test_exclude_supplier_keep_lowest(state_object, params):

    offers, demands, discounts = state_object()
    demands_header = demands.header
    filters = [demands_header.byname['item'].id]
    item = 'Item_0003'

    # Keep lowest RiskScore supplier for 'Item_0003'
    select_item_mask = (offers.data[offers.header[UKEYS.ITEM_ID]] == item)
    min_riskscore = offers.data.loc[select_item_mask, offers.header["RiskScore"]].min()
    lowest_risk_score_mask = (offers.data[offers.header["RiskScore"]] == min_riskscore)
    supplier_min_riskscore = offers.data.loc[lowest_risk_score_mask & select_item_mask, offers.header.byname[UKEYS.SUPPLIER].id].tolist()[0]


    parameters = params()
    parameters['excluded_suppliers'] = [ {
        'value':None,
        'filters': {},
        'criteria':'RiskScore',
        'mutation': None,
        'operation':'keep_lowest'
    }]
    
    
    project_settings = ProjectSettings()
    project_settings.filters = filters
    project_settings.soft_constraints = [offers.header.byname[CONSTRAINT_SPECS.RISK_SCORE].id]
    
    
    all_params = {'scenario_settings': parameters }


    # Check that the filter runs properly
    job_filter = Filter(offers, None, demands, None)
    assert job_filter.run(all_params, project_settings) == True

    # Check that optimizer runs properly
    optimizer = Optimizer(parameters, offers, demands, discounts, project_settings=project_settings)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(project_settings.filters) == True
    allocations_table = optimizer.run()
    allocations_table.data = allocations_table.data.merge(demands[demands.header[[UKEYS.ITEM_ID, UKEYS.DEMAND_ID]]], left_on=allocations_table.header[UKEYS.DEMAND_ID], right_on=demands.header[UKEYS.DEMAND_ID])
    allocations_table.header.add_column(UKEYS.ITEM_ID, col_id=demands.header.byid[demands.header[UKEYS.ITEM_ID]].id)
    
    resulted_supplier = allocations_table.data.loc[allocations_table.data[allocations_table.header[UKEYS.ITEM_ID]] == item,allocations_table.header.byname[UKEYS.SUPPLIER].id].tolist()
    
    # Check if lowest RiskScore supplier is in allocation for 'Item_0003'
    fail_value = 'The lowest RiskScore supplier was not in the allocation for the desired item.'
    assert supplier_min_riskscore in resulted_supplier, f'Failed due to: {fail_value}'


""" ------------------------------ KEEP LOWEST RISKSCORE FAIL --------------- """
def test_exclude_supplier_keep_lowest_fail(state_object, params):

    offers, demands, discounts = state_object()
    demands_header = demands.header
    filters = [demands_header.byname['item'].id]
    item = 'Item_0003'
    # SABIC has the lowest RiskScore, so it should be allocated
    excluded_supplier = 'DuPont'

    # Keep lowest RiskScore supplier for 'Item_0003'
    select_item_mask = (offers.data[offers.header[UKEYS.ITEM_ID]] == item)
    min_riskscore = offers.data.loc[select_item_mask, offers.header["RiskScore"]].min()
    lowest_risk_score_mask = (offers.data[offers.header["RiskScore"]] == min_riskscore)
    supplier_min_riskscore = offers.data.loc[lowest_risk_score_mask & select_item_mask, offers.header.byname[UKEYS.SUPPLIER].id].tolist()[0]


    parameters = params()
    parameters['excluded_suppliers'] = [ {
        'value':None,
        'filters': {'supplier': [excluded_supplier]},
        'criteria':'RiskScore',
        'mutation': None,
        'operation':'keep_lowest'
    }]
    
    
    project_settings = ProjectSettings()
    project_settings.filters = filters
    project_settings.soft_constraints = [offers.header.byname[CONSTRAINT_SPECS.RISK_SCORE].id]
    
    
    all_params = {'scenario_settings': parameters }


    # Check that the filter runs properly
    job_filter = Filter(offers, None, demands, None)
    assert job_filter.run(all_params, project_settings) == True

    # Check that optimizer runs properly
    optimizer = Optimizer(parameters, offers, demands, discounts, project_settings=project_settings)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(project_settings.filters) == True
    allocations_table = optimizer.run()
    allocations_table.data = allocations_table.data.merge(demands[demands.header[[UKEYS.ITEM_ID, UKEYS.DEMAND_ID]]], left_on=allocations_table.header[UKEYS.DEMAND_ID], right_on=demands.header[UKEYS.DEMAND_ID])
    allocations_table.header.add_column(UKEYS.ITEM_ID, col_id=demands.header.byid[demands.header[UKEYS.ITEM_ID]].id)
    resulted_supplier = allocations_table.data.loc[allocations_table.data[allocations_table.header[UKEYS.ITEM_ID]] == item,allocations_table.header.byname[UKEYS.SUPPLIER].id].tolist()
    
    # Check if lowest RiskScore supplier is in allocation for 'Item_0003'
    fail_value = 'The lowest RiskScore supplier was in the allocation for the desired item.'
    assert supplier_min_riskscore not in resulted_supplier, f'Failed due to: {fail_value}'

""" ------------------------------ EXCLUDE EXACT RISKSCORE --------------- """
def test_exclude_supplier_exact_riskscore(state_object, params):

    offers, demands, discounts = state_object()
    demands_header = demands.header
    filters = [demands_header.byname['item'].id]
    supplier_with_70_riskscore = 'SABIC'
    item = 'Item_0003'

    select_supplier = (offers.data[offers.header[UKEYS.ITEM_ID]] == item) & (offers.data[offers.header["RiskScore"]] == 70)
    supplier_with_70_riskscore = offers.data.loc[select_supplier, offers.header[UKEYS.SUPPLIER]].tolist()


    # Exclude exact RiskScore equal to supplier with 70 for 'Item_0003'
    parameters = params()
    parameters['excluded_suppliers'] = [ {
        'value':70,
        'filters': {},
        'criteria':'RiskScore',
        'mutation': None,
        'operation':'exclude_exact'
    }]
    
    
    project_settings = ProjectSettings()
    project_settings.filters = filters
    project_settings.soft_constraints = [offers.header.byname[CONSTRAINT_SPECS.RISK_SCORE].id]
    
    
    all_params = {'scenario_settings': parameters }


    # Check that the filter runs properly
    job_filter = Filter(offers, None, demands, None)
    assert job_filter.run(all_params, project_settings) == True

    # Check that optimizer runs properly
    optimizer = Optimizer(parameters, offers, demands, discounts, project_settings=project_settings)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(project_settings.filters) == True
    allocations_table = optimizer.run()
    allocations_table.data = allocations_table.data.merge(demands[demands.header[[UKEYS.ITEM_ID, UKEYS.DEMAND_ID]]], left_on=allocations_table.header[UKEYS.DEMAND_ID], right_on=demands.header[UKEYS.DEMAND_ID])
    allocations_table.header.add_column(UKEYS.ITEM_ID, col_id=demands.header.byid[demands.header[UKEYS.ITEM_ID]].id)
    
    resulted_supplier = allocations_table.data.loc[allocations_table.data[allocations_table.header[UKEYS.ITEM_ID]] == item,allocations_table.header.byname[UKEYS.SUPPLIER].id].tolist()[0]
    # Check if the RiskScore supplier in allocation for 'Item_0003' in not the one with RiskScore 70
    fail_value = 'The exact RiskScore supplier was in the allocation for the desired item.'
    assert resulted_supplier not in supplier_with_70_riskscore, f'Failed due to: {fail_value}'

""" ------------------------------ EXCLUDE EXACT RISKSCORE FAIL --------------- """
def test_exclude_supplier_exact_riskscore_fail(state_object, params):

    offers, demands, discounts = state_object()
    demands_header = demands.header
    filters = [demands_header.byname['item'].id]
    excluded_suppliers = (offers.data[offers.header.byname[UKEYS.SUPPLIER].id].unique()).tolist()

    # Exclude all suppliers for item that has all risk scores equal to 80
    offers.data.loc[(offers.data[offers.header[UKEYS.ITEM_ID]] == 'Item_0075') & (offers.data[offers.header[UKEYS.SUPPLIER]] == 'Reliance'), offers.header[CONSTRAINT_SPECS.RISK_SCORE]] = 80

    parameters = params()
    parameters['excluded_suppliers'] = [ {
        'value':80,
        'filters': {'supplier': excluded_suppliers},
        'criteria':'RiskScore',
        'mutation': None,
        'operation':'exclude_exact'
    }]
    
    
    project_settings = ProjectSettings()
    project_settings.filters = filters
    project_settings.soft_constraints = [offers.header.byname[CONSTRAINT_SPECS.RISK_SCORE].id]
    
    
    all_params = {'scenario_settings': parameters }


    # Check that the filter runs properly
    job_filter = Filter(offers, None, demands, None)
    assert job_filter.run(all_params, project_settings) == True

    # Check that optimizer runs properly
    optimizer = Optimizer(parameters, offers, demands, discounts, project_settings=project_settings)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(project_settings.filters) == True
    with pytest.raises(RecordedError) as e:
        optimizer.run()



""" ------------------------------ EXCLUDE ABOVE RISKSCORE --------------- """
def test_exclude_supplier_above_riskscore(state_object, params):

    offers, demands, discounts = state_object()
    demands_header = demands.header
    filters = [demands_header.byname['item'].id]
    item = 'Item_0003'

    select_supplier = (offers.data[offers.header[UKEYS.ITEM_ID]] == item) & (offers.data[offers.header["RiskScore"]] > 80)
    supplier_with_above80_riskscore = offers.data.loc[select_supplier, offers.header[UKEYS.SUPPLIER]].tolist()


    # Exclude exact RiskScore equal to supplier with 70 for 'Item_0003'
    parameters = params()
    parameters['excluded_suppliers'] = [ {
        'value':80,
        'filters': {},
        'criteria':'RiskScore',
        'mutation': None,
        'operation':'exclude_above'
    }]
    
    
    project_settings = ProjectSettings()
    project_settings.filters = filters
    project_settings.soft_constraints = [offers.header.byname[CONSTRAINT_SPECS.RISK_SCORE].id]
    
    
    all_params = {'scenario_settings': parameters }


    # Check that the filter runs properly
    job_filter = Filter(offers, None, demands, None)
    assert job_filter.run(all_params, project_settings) == True

    # Check that optimizer runs properly
    optimizer = Optimizer(parameters, offers, demands, discounts, project_settings=project_settings)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(project_settings.filters) == True
    allocations_table = optimizer.run()
    allocations_table.data = allocations_table.data.merge(demands[demands.header[[UKEYS.ITEM_ID, UKEYS.DEMAND_ID]]], left_on=allocations_table.header[UKEYS.DEMAND_ID], right_on=demands.header[UKEYS.DEMAND_ID])
    allocations_table.header.add_column(UKEYS.ITEM_ID, col_id=demands.header.byid[demands.header[UKEYS.ITEM_ID]].id)
    
    resulted_supplier = allocations_table.data.loc[allocations_table.data[allocations_table.header[UKEYS.ITEM_ID]] == item,allocations_table.header.byname[UKEYS.SUPPLIER].id].tolist()
    # Check if the RiskScore supplier in allocation for 'Item_0003' in not the one with RiskScore 70
    fail_value = 'The supplier with RiskScore above the given value was in the allocation for the desired item.'
    assert (resulted_supplier not in supplier_with_above80_riskscore), f'Failed due to: {fail_value}'

""" ------------------------------ EXCLUDE ABOVE RISKSCORE FAIL --------------- """
def test_exclude_supplier_above_riskscore_fail(state_object, params):

    offers, demands, discounts = state_object()
    demands_header = demands.header
    filters = [demands_header.byname['item'].id]
    excluded_suppliers = (offers.data[offers.header.byname[UKEYS.SUPPLIER].id].unique()).tolist()

    # Exclude exact RiskScore equal to supplier with 70 for 'Item_0003'
    parameters = params()
    parameters['excluded_suppliers'] = [ {
        'value':10,
        'filters': {'supplier': excluded_suppliers},
        'criteria':'RiskScore',
        'mutation': None,
        'operation':'exclude_above'
    }]
    
    
    project_settings = ProjectSettings()
    project_settings.filters = filters
    project_settings.soft_constraints = [offers.header.byname[CONSTRAINT_SPECS.RISK_SCORE].id]
    
    
    all_params = {'scenario_settings': parameters }


    # Check that the filter runs properly
    job_filter = Filter(offers, None, demands, None)
    assert job_filter.run(all_params, project_settings) == True

    # Check that optimizer runs properly
    optimizer = Optimizer(parameters, offers, demands, discounts, project_settings=project_settings)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(project_settings.filters) == True
    with pytest.raises(RecordedError) as e:
        optimizer.run()

""" ------------------------------ EXCLUDE BELOW RISKSCORE --------------- """
def test_exclude_supplier_below_riskscore(state_object, params):

    offers, demands, discounts = state_object()
    demands_header = demands.header
    filters = [demands_header.byname['item'].id]
    item = 'Item_0003'

    select_supplier = (offers.data[offers.header[UKEYS.ITEM_ID]] == item) & (offers.data[offers.header["RiskScore"]] < 80)
    supplier_with_below90_riskscore = offers.data.loc[select_supplier, offers.header[UKEYS.SUPPLIER]].tolist()


    # Exclude exact RiskScore equal to supplier with 70 for 'Item_0003'
    parameters = params()
    parameters['excluded_suppliers'] = [ {
        'value':90,
        'filters': {},
        'criteria':'RiskScore',
        'mutation': None,
        'operation':'exclude_below'
    }]
    
    
    project_settings = ProjectSettings()
    project_settings.filters = filters
    project_settings.soft_constraints = [offers.header.byname[CONSTRAINT_SPECS.RISK_SCORE].id]
    
    
    all_params = {'scenario_settings': parameters }


    # Check that the filter runs properly
    job_filter = Filter(offers, None, demands, None)
    assert job_filter.run(all_params, project_settings) == True

    # Check that optimizer runs properly
    optimizer = Optimizer(parameters, offers, demands, discounts,  project_settings=project_settings)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(project_settings.filters) == True
    allocations_table = optimizer.run()
    allocations_table.data = allocations_table.data.merge(demands[demands.header[[UKEYS.ITEM_ID, UKEYS.DEMAND_ID]]], left_on=allocations_table.header[UKEYS.DEMAND_ID], right_on=demands.header[UKEYS.DEMAND_ID])
    allocations_table.header.add_column(UKEYS.ITEM_ID, col_id=demands.header.byid[demands.header[UKEYS.ITEM_ID]].id)
    
    resulted_supplier = allocations_table.data.loc[allocations_table.data[allocations_table.header[UKEYS.ITEM_ID]] == item,allocations_table.header.byname[UKEYS.SUPPLIER].id].tolist()
    # Check if the RiskScore supplier in allocation for 'Item_0003' in not the one with RiskScore 70
    fail_value = 'The supplier with RiskScore below the given value was in the allocation for the desired item.'
    assert (resulted_supplier not in supplier_with_below90_riskscore), f'Failed due to: {fail_value}'

""" ------------------------------ EXCLUDE BELOW RISKSCORE FAIL --------------- """
def test_exclude_supplier_below_riskscore_fail(state_object, params):

    offers, demands, discounts = state_object()
    demands_header = demands.header
    filters = [demands_header.byname['item'].id]
    excluded_suppliers = (offers.data[offers.header.byname[UKEYS.SUPPLIER].id].unique()).tolist()

    # Exclude exact RiskScore equal to supplier with 70 for 'Item_0003'
    parameters = params()
    parameters['excluded_suppliers'] = [ {
        'value':150,
        'filters': { 'supplier': excluded_suppliers },
        'criteria':'RiskScore',
        'mutation': None,
        'operation':'exclude_below'
    }]
    
    
    project_settings = ProjectSettings()
    project_settings.filters = filters
    project_settings.soft_constraints = [offers.header.byname[CONSTRAINT_SPECS.RISK_SCORE].id]
    
    
    all_params = {'scenario_settings': parameters }


    # Check that the filter runs properly
    job_filter = Filter(offers, None, demands, None)
    assert job_filter.run(all_params, project_settings) == True

    # Check that optimizer runs properly
    optimizer = Optimizer(parameters, offers, demands, discounts, project_settings=project_settings)
    optimizer.initialize_preprocessor()
    assert optimizer.initialize_model(project_settings.filters) == True
    with pytest.raises(RecordedError) as e:
        optimizer.run()