import pandas as pd
import numpy as np
import pathlib
import pytest

from engine.src.table_processors import Preprocessor
from engine.src.table_processors import offer_processing_utils
from shared.communication import external_api_caller
from shared.communication import IOFacade
from shared.communication import SingletonLogger
from shared.communication.database_connection import DBFile
from shared.constants import UKEYS, KEYS
from shared.constants import RecordedError
from shared.constants import SEVERITY
from shared.entities import TableData
from shared.entities import TableHeader
from shared.entities import Translator
from test.utils import *

slogger = SingletonLogger()

def load_file(file_path, name, version=1, extension='.xlsx'):
	iofacade = IOFacade(0, './')

	file = DBFile(file_path, name, version, extension, None, None)
	table = TableData()
	iofacade.load_table(table, file)

	return table

################################################################################################
#								   FIXTURES
################################################################################################


@pytest.fixture()
def preprocessor():
	preprocessor = Preprocessor()

	return preprocessor


@pytest.fixture()
def state_object(preprocessor):
	def _state_object():

		# read files
		file_path = './resources/inputfiles'
		offers = load_file(file_path, 'Offers')
		demands = load_file(file_path, 'Demands')
		discounts = load_file(file_path, 'Discounts', version=2)

		# create tables
		tables = {}
		tables['offers'] = offers
		tables['demands'] = demands
		tables['discounts'] = discounts

		# read translator
		file_path = './resources'
		name = 'archlet_rawmat_translator'
		version = 1
		extension = '.json'
		file_name = f'{name}_v{version}{extension}'
		translator = Translator(file_name, file_path)

		return preprocessor, tables, translator
	return _state_object


@pytest.fixture()
def state_object_duties(state_object):
	def _state_object():
		file_path = './resources/inputfiles'

		# Get basic objects from default state object
		preprocessor, tables, translator = state_object()

		# read duties file
		file_path = './resources/inputfiles'
		duties = load_file(file_path, 'Duties')

		# add to table
		tables['duties'] = duties

		return preprocessor, tables, translator
	return _state_object


@pytest.fixture()
def state_object_currencies(state_object):
	def _state_object():
		file_path = './resources/inputfiles'

		# Get basic objects from default state object
		preprocessor, tables, translator = state_object()

		# read currencies file
		file_path = './resources/inputfiles'
		currencies = load_file(file_path, 'Currencies')

		# add to table
		tables['currencies'] = currencies

		return preprocessor, tables, translator
	return _state_object


@pytest.fixture
def simple_offers_td():
	datafr = pd.DataFrame(data={'item':['item_1','item_1', 'item_1', 'item_2', 'item_2'],
								'supplier':['supA', 'supC','supB','supA','supB'],
								'category':['type1','type2','type1','type2','type2'],
								UKEYS.RAW_PRICE:[600, 500, 300, 400, 500],
								'price_usd':[600, 500, 300, 400, 500],
								UKEYS.NORMALIZED_PRICE:[600, 500, 300, 400, 500],
								UKEYS.SUPPLIER_COUNTRY: ['Ukraine', 'Belarus', 'Poland', 'Ukraine', 'Belarus'],
								UKEYS.CLIENT_COUNTRY: ['USA', 'USA', 'USA', 'Germany', 'Germany'],
								'currency': ['USD', 'EUR', 'USD', 'USD', 'EUR'],
								UKEYS.PAYMENT_DURATION: [90, 110, 75, 90, 75]
								})

	table = TableData()
	table.data = datafr
	table.trim()
	add_header(table)
	table.header.set_type('price_usd', KEYS.PRICE)
	table.header.set_type('currency', KEYS.CURRENCY)
	return table


@pytest.fixture
def simple_demands_td():
	datafr = pd.DataFrame(data={UKEYS.DEMAND_ID: ['item_1', 'item_2'],
								UKEYS.ITEM_ID: ['this is it', 'and go'],
								UKEYS.SUPPLIER: ['supB', 'supA'],
								UKEYS.TOTAL_PRICE: [550, 389],
								UKEYS.TOTAL_VOLUME: [100, 200],
								KEYS.REGION: ['GE', 'CH']})

	table = TableData()
	table.data = datafr
	table.trim()
	add_header(table)
	return table


@pytest.fixture
def simple_currencies_td():
	datafr = pd.DataFrame(data={'currency': ['USD', 'EUR'],
								UKEYS.RATE_USD_TO_CURR: [1.00, 0.92]})

	table = TableData()
	table.data = datafr
	table.trim()
	add_header(table)

	table.header.set_type('currency', KEYS.CURRENCY)

	return table


@pytest.fixture
def simple_duties_td():
	datafr = pd.DataFrame(data={UKEYS.SUPPLIER_COUNTRY: ['Ukraine', 'Belarus', 'Poland', 'Ukraine', 'Belarus'],
								UKEYS.CLIENT_COUNTRY: ['USA', 'USA', 'USA', 'Germany', 'Germany'],
								UKEYS.DUTY_PERCENT: [0.05, 0.07, 0.03, 0.10, 0.12]})

	table = TableData()
	table.data = datafr
	table.trim()
	add_header(table)

	return table


################################################################################################
#								   TESTS
################################################################################################

""" ------------------------------ UNIT TEST ----------------------------------------------- """


def test_currency_conversion(simple_offers_td, simple_currencies_td, preprocessor):
	offers = preprocessor._convert_currencies(simple_offers_td, simple_currencies_td)

	# check if some prices have been changed due to conversion
	header = offers.header
	valid = any(offers[header[UKEYS.RAW_PRICE]] != offers['price_usd'])
	assert valid == True


def test_duties_normalization(simple_offers_td, simple_duties_td, preprocessor):
	# This copying of the data is necessary due to the fact that price_usd is assumed
	# to be an internally available column during the normalization of the offers
	simple_offers_td['price_usd'] = simple_offers_td[simple_offers_td.header[KEYS.PRICE][0]]
	offers = preprocessor._normalize_duties(simple_offers_td, simple_duties_td)

	header = offers.header
	# check if some prices have been changed due to duties
	valid = any(offers[header[UKEYS.NORMALIZED_PRICE]] != offers['price_usd'])
	assert valid == True


def test_payment_term_normalization(simple_offers_td, preprocessor):
	header = simple_offers_td.header
	prices_before_norm = simple_offers_td[header[UKEYS.NORMALIZED_PRICE]].copy()
	offers = preprocessor._add_time_value_money(simple_offers_td,
												paymentnorm_day = 90,
												paymentnorm_perc = 1,
												interest_type = 'linear')

	# check if some prices have been decreased due to payment terms
	valid = any(offers[header[UKEYS.NORMALIZED_PRICE]] < prices_before_norm)
	assert valid == True

	# check if some prices have been increased due to payment terms
	valid = any(offers[header[UKEYS.NORMALIZED_PRICE]] > prices_before_norm)
	assert valid == True


def test_extract_baseline_offers(simple_demands_td, preprocessor):
	offers_header = TableHeader()
	offers_header.add_column(UKEYS.NORMALIZED_PRICE)
	offers_header.add_column(UKEYS.SUPPLIER)
	# offers_header.add_column('price_comp1', KEYS.PRICE, '')
	offers_header.add_column(KEYS.PRICE, 'price_comp1')
	# offers_header.add_column('yearly volume', KEYS.VOLUME, '')
	offers_header.add_column(UKEYS.TOTAL_VOLUME, 'yearly volume')
	offers_header.add_column(UKEYS.DEMAND_ID)
	offers_header.add_column(KEYS.REGION)
	offers_header.add_column(UKEYS.ITEM_ID)
	offers_header.add_column(UKEYS.TOTAL_PRICE)
	baseline_offers, simple_demands_td = preprocessor.extract_baseline(simple_demands_td, offers_header)
	baseline_offers.data.set_index(offers_header[UKEYS.DEMAND_ID], inplace=True)
	offers_data = baseline_offers.data
	assert offers_data.loc['item_1', offers_header[UKEYS.TOTAL_PRICE]] == 550
	assert offers_data.loc['item_1', offers_header[UKEYS.SUPPLIER]] == 'supB'
	assert offers_data.loc['item_2', offers_header[UKEYS.TOTAL_PRICE]] == 389
	assert offers_data.loc['item_2', offers_header[UKEYS.SUPPLIER]] == 'supA'

""" ------------------------------ INTEGRATION TESTS WITH DISCOUNTS TABLE ------------------ """

def test_discounts_valid(state_object):
	# get object
	preprocessor, tables, translator = state_object()

	# test translation stage
	tables = preprocessor.translate_tables(translator, tables)

	# test data integrity stage
	assert preprocessor.check_data_integrity(tables=tables,
											 has_discount=True) == True

	# test offer normalization
	offers = preprocessor.normalize_offers(tables, translator.expression) == True


def test_discounts_invalid_offer(state_object):
	# get object
	preprocessor, tables, translator = state_object()

	# test translation stage
	tables = preprocessor.translate_tables(translator, tables)

	# update offers
	header = tables['offers'].header
	tables['offers'].data.loc[0, header[UKEYS.ITEM_ID]] = 'Item_0000'
	original_length = len(tables['offers'].data)

	# test data integrity stage
	assert preprocessor.check_data_integrity(tables=tables,
											 has_discount=True) == True

	# check that invalid offer has been removed
	valid = any(tables['offers'][header[UKEYS.ITEM_ID]] == 'Item_0000')
	assert (valid == False) and (original_length > len(tables['offers'].data))

""" ------------------------------ INTEGRATION TESTS WITH DUTIES TABLE --------------------- """

def test_duties_valid(state_object_duties):
	# get object
	preprocessor, tables, translator = state_object_duties()

	# test translation stage
	tables = preprocessor.translate_tables(translator, tables)

	# test data integrity stage
	assert preprocessor.check_data_integrity(tables=tables,
											 has_discount=True) == True

	# test offer normalization
	offers = preprocessor.normalize_offers(tables, translator.expression) == True

	# check if duty was considered
	valid = UKEYS.DUTY_PERCENT in tables['offers'].header.types
	assert valid == True


def test_duties_warning(state_object_duties):
	# get object
	preprocessor, tables, translator = state_object_duties()

	# test translation stage
	tables = preprocessor.translate_tables(translator, tables)

	# update offers with invalid country
	header = tables['offers'].header
	tables['offers'].data.loc[0, header[UKEYS.SUPPLIER_COUNTRY]] = 'Zurich'

	# test data integrity stage
	assert preprocessor.check_data_integrity(tables=tables,
											 has_discount=True) == True

	# test offer normalization
	offers = preprocessor.normalize_offers(tables, translator.expression) == True

	# check if any error message has been written
	warning = False
	print(slogger.messages)
	for message in slogger.messages:
		if message.severity == SEVERITY.WARNING:
			warning = True
	assert warning


""" ------------------------------ INTEGRATION TESTS WITH CURRENCIES TABLE ----------------- """
def test_currencies_valid(state_object_currencies):
	# get object
	preprocessor, tables, translator = state_object_currencies()

	# test translation stage
	tables = preprocessor.translate_tables(translator, tables)

	# test data integrity stage
	assert preprocessor.check_data_integrity(tables=tables,
											 has_discount=True) == True

	# test offer normalization
	offers = preprocessor.normalize_offers(tables, translator.expression) == True

	# check if duty was considered
	valid = KEYS.CURRENCY in tables['offers'].header.types
	assert valid == True


def test_currencies_warning(state_object_currencies):
	# get object
	preprocessor, tables, translator = state_object_currencies()

	# test translation stage
	tables = preprocessor.translate_tables(translator, tables)

	# update currencies with invalid currency
	header = tables['currencies'].header
	tables['currencies'].data.loc[0, header[KEYS.CURRENCY][0]] = 'Franc'

	# test data integrity stage
	assert preprocessor.check_data_integrity(tables=tables,
											 has_discount=True) == True

	# test offer normalization
	offers = preprocessor.normalize_offers(tables, translator.expression) == True

	# check if any error message has been written
	warning = False
	print(slogger.messages)
	for message in slogger.messages:
		print(message.text)
		print(message.severity)
		if message.severity == SEVERITY.WARNING:
			warning = True
	assert warning
