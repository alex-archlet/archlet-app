"""
Testing suite for the preprocessor
"""
import pytest

import pandas as pd

from engine.src.table_processors import Preprocessor
from shared.communication import SingletonLogger
from shared.constants import UKEYS, KEYS
from shared.constants import RecordedError
from shared.entities import TableData
from shared.entities import TableHeader
from test.utils import add_header

slogger = SingletonLogger()

@pytest.fixture
def allocations():
    table = TableData()
    datafr = pd.DataFrame(data={UKEYS.DEMAND_ID:['item_1', 'item_2', 'item_3', 'item_3'],
                                UKEYS.OFFER_ID:['offer1', 'offer2', 'offer6', 'offer7'],
                                KEYS.VOLUME:[100, 50, 80, 30],
                                UKEYS.UNIT_PRICE:[25, 31, 62, 73],
                                UKEYS.SUPPLIER:['MR A', 'Seniora B', 'MRS C', 'Seniora B']})
    table.data = datafr
    table.trim()
    add_header(table)
    return table


@pytest.fixture
def offers_header():
    table = TableData()
    datafr = pd.DataFrame(data={UKEYS.ITEM_ID:['B1', 'B2', 'B3', 'B3'],
                                UKEYS.SUPPLIER:['Arsenium', 'Celenium', 'Strontium', 'Strontium'],
                                UKEYS.TOTAL_VOLUME:[1000, 1200, 900, 1300],
                                UKEYS.TOTAL_PRICE:[.5, .6, .4, .7]})
    table.data = datafr
    table.trim()
    add_header(table)
    return table.header


@pytest.fixture
def discounts():
    table = TableData()
    datafr = pd.DataFrame(data={UKEYS.BUCKET_ID:['B1', 'B2', 'B3', 'B3'],
                                UKEYS.SUPPLIER:['Arsenium', 'Celenium', 'Strontium', 'Strontium'],
                                UKEYS.THRESHOLD:[1000, 1200, 900, 1300],
                                UKEYS.DISCOUNT_AMOUNT:[.5, .6, .4, .7]})
    table.data = datafr
    table.trim()
    add_header(table)
    return table

@pytest.fixture
def discount_units():
    return ['volume', 'spend']


@pytest.fixture
def discount_types():
    return ['percentage', 'flat', 'unit']


def test_get_baseline_offers(allocations):
    preprocessor = Preprocessor()
    result = preprocessor.get_baseline_offers(allocations)
    header = result.header
    expected_columns = [UKEYS.DEMAND_ID, UKEYS.OFFER_ID, KEYS.VOLUME, UKEYS.SUPPLIER, UKEYS.NORMALIZED_PRICE]
    for col in expected_columns:
        assert col in header.types

    result.data.set_index(header[UKEYS.OFFER_ID], inplace=True)
    assert result.data.loc['offer1', header[UKEYS.NORMALIZED_PRICE]] == 25


def test_clean_discounts_table(discounts, discount_units, discount_types, offers_header):
    preprocessor = Preprocessor()
    result = preprocessor.clean_discounts_table(discounts, discount_units, discount_types, offers_header)
    expected_columns = [UKEYS.DISCOUNT_UNIT, UKEYS.DISCOUNT_AMOUNT, UKEYS.PRICE_COMPONENT, UKEYS.VOLUME_NAME]
    print(result.header.columns)
    for col in expected_columns:
        assert col in result.header.types


def test_clean_discounts_table_failed(discounts, discount_units, discount_types, offers_header):
    preprocessor = Preprocessor()
    discounts.header.add_column(UKEYS.DISCOUNT_UNIT, UKEYS.DISCOUNT_UNIT, '')
    discounts[discounts.header[UKEYS.DISCOUNT_UNIT]] = 'something not fitting'
    result = preprocessor.clean_discounts_table(discounts, discount_units, discount_types, offers_header)
    assert slogger.failed()


def test_clean_discounts_table_error(discounts, discount_units, discount_types, offers_header):
    preprocessor = Preprocessor()
    del discounts.header[UKEYS.DISCOUNT_AMOUNT]
    with pytest.raises(RecordedError):
        result = preprocessor.clean_discounts_table(discounts, discount_units, discount_types, offers_header)
