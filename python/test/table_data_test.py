# Disable pylint unnecessary warnings
# pylint: disable=C0111
# pylint: disable=W0621

import numpy as np
import pandas as pd
import pytest

from shared.entities import TableData
from shared.entities import TableHeader

###########################################################
#                   Test Fixtures
###########################################################


@pytest.fixture
def numbers():
    """ Fixture returning an array of numbers """
    return [1, 2, 3]


@pytest.fixture
def letters():
    """ Fixture returning an array of letters """
    return ['a', 'b', 'c']


@pytest.fixture
def values():
    """ Fixture returning an array of string values """
    return ['val1', 'val2', 'val3']


@pytest.fixture
def data_frame(numbers, letters):
    """ Fixture returning a pandas data frame with two columns """
    data_frame = pd.DataFrame(list(zip(numbers, letters)),
                              columns=['nbrs', 'ltrs'])
    return data_frame

def simple_header(simple_table):
    header = TableHeader()
    for col in simple_table.data.keys().tolist():
        header.add_column(col, col, '')
    simple_table.header = header


@pytest.fixture
def simple_table(data_frame):
    """ Fixture returning a TableData object populated with data_frame """
    table = TableData()
    table.data = data_frame

    simple_header(table)
    return table

@pytest.fixture
def dupl_table():
    """
    Fixture returning a TableData object with a duplicate row
    in nbrs and ltrs columns, but not in all columns.
    """

    table = TableData()
    table.data = pd.DataFrame(list(zip([1, 2, 3, 1],
                                       ['a', 'b', 'c', 'a'],
                                       ['val1', 'val2', 'val3', 'val4'])),
                              columns=['nbrs', 'ltrs', 'values'])
    return table

###########################################################
#                       Tests
###########################################################


def test_string_repr(simple_table):
    """ Test the to string method for a simple table data object """
    expected = '   nbrs ltrs\n0     1    a\n1     2    b\n2     3    c'
    assert simple_table.__str__() == expected


def test_get_data_from_frame(simple_table, numbers):
    """ Test getter for the dataframe contained in a table data object """
    assert simple_table['nbrs'].equals(pd.Series(numbers))


def test_set_data_in_frame(simple_table, values):
    """ Test setter for the dataframe """
    simple_table['new_col'] = values
    assert simple_table['new_col'].equals(pd.Series(values))


def test_del_data_in_frame(simple_table, values, numbers, letters):
    """ Test deleting one and multiple columns """
    simple_table['new_col'] = values
    del simple_table['nbrs']
    expected = pd.DataFrame(list(zip(letters, values)),
                            columns=['ltrs', 'new_col'])
    assert simple_table.data.equals(expected)
    simple_table['nbrs'] = numbers
    del simple_table[['nbrs', 'new_col']]
    expected = pd.DataFrame(letters, columns=['ltrs'])
    assert simple_table.data.equals(expected)


def test_get_keys(simple_table):
    """ Test to get the column names of a table data object"""
    assert np.array_equal(simple_table.data.keys(), ['nbrs', 'ltrs'])


def test_drop_null_rows(simple_table):
    """ Test dropping rows that contain a null value: NaN or empty string """
    simple_table['new_col'] = [np.nan, 'val2', '']
    simple_table.drop_null_rows('new_col')
    assert simple_table['new_col'].equals(pd.Series(['val2'], [1]))
    assert simple_table['ltrs'].equals(pd.Series(['b'], [1]))


def test_convert_to_lists(simple_table):
    """ Test converting a string to a list of variables """
    simple_table['new_col'] = ['val1,val2,val3', 'other_val', '']
    expected = pd.Series([['val1', 'val2', 'val3'], ['other_val'], []])
    assert simple_table.convert_to_lists('new_col').equals(expected)


def test_to_string(simple_table):
    """ Test converting a list of values to a string """
    simple_table['new_col'] = [['val1', 'val2', 'val3'], ['other_val'], []]
    expected = pd.Series(['val1,val2,val3', 'other_val', ''])
    assert simple_table.to_string('new_col', False).equals(expected)


def test_trim_dataframe(simple_table):
    # adapt data
    raw_data = simple_table.data.copy()
    raw_data['ltrs'] = ['a\n', '  b', 'c  ']
    raw_data.columns = [' nbrs', 'ltrs \n ']

    df_trimmed = simple_table.trim()

    assert simple_table.data.equals(df_trimmed)
