"""
Testing suite for the postprocessor
"""
import json
import os
import pandas as pd
import pytest

from engine.src.table_processors.postprocessor import Postprocessor
from shared.constants import PATHS
from shared.entities import TableData
from test.utils import add_header

###########################################################
#                       Test Fixtures
###########################################################

@pytest.fixture
def allocation_table():
    data = pd.DataFrame(data={'item_id': ['item1', 'item2','item3', 'item2'],
                               'offered_volume': [100, 90, 100, 30],
                               'demanded_volume': [100, 120, 100, 120],
                               'total_spend': [300, 240, 520, 120],
                               'weight_offered': [.2, .2, .3, .1],
                               'weight_demanded': [.2, .2, .2, .2],
                               'payment_terms_offer': [80, 100, 90, 80],
                               'payment_terms_demand': [80, 80, 80, 80]})

    table = TableData()
    table.data = data
    table.trim()
    add_header(table)
    return table

@pytest.fixture
def postprocessor():
    return Postprocessor()


###########################################################
#                       Tests
###########################################################

@pytest.mark.skip(reason='With the latest change of the data structure, add_kpi_column is not yet implemented correctly')
@pytest.mark.parametrize("expression,col_name,value",
                         [('total_weight_diff = (weight_offered - weight_demanded) * offered_volume','total_weight_diff',10),
                          ('cashflow_impact = (payment_terms_offer - payment_terms_demand)*.2*total_spend','cashflow_impact',1040)])
def test_add_kpi_column(postprocessor, allocation_table, expression, col_name, value):
    allocation_table, result_name = postprocessor.add_kpi_column(allocation_table, expression)
    assert col_name == result_name
    allocation_table.data.set_index('total_spend', inplace=True)
    assert round(allocation_table.data.loc[520, col_name],2) == value


def test_error_enums():
    """
    Check that all error UKEYS from the python side are present in the language files from
    the frontend
    """
    with open('python/engine/src/' + PATHS.ERROR_ENUM_PATH) as json_file:
        python_json = json.load(json_file)

    directory = 'web-app/frontend/src/locales/'

    subdirs = next(os.walk(directory))[1]

    subdirs.remove('test')

    for subdir in subdirs:
        with open(directory + subdir + '/' + subdir + '.json') as json_file:
            translator = json.load(json_file)
            difference = set(python_json.keys()).difference(set(translator.keys()))
            assert not difference, f'Missing {difference} UKEYS in {subdir}'
