"""
Testing suite for the table header
"""
import pytest

from shared.constants import RecordedError
from shared.entities import TableHeader
from test.utils import compare_iterables


###########################################################
#                   Test Fixtures
###########################################################

@pytest.fixture
def simple_header():
    header = TableHeader()
    # columns = {'col1': {'name': 'user_col1', 'type': 'VOL', 'id': '1234', 'calc':None},
    #            'column2': {'name': 'datafr col', 'type': 'PRICE', 'id': 'd34af9', 'calc':None},
    #            '3rd_one': {'name': 'das lange Deutsche kolum', 'type': 'LANG', 'id': 'deadbeef', 'calc':None}}
    header.add_column('col1', '1234', 'user_col1')
    header.add_column('column2', 'd34af9', 'datafr col')
    header.add_column('3rd_one', 'deadbeef', 'das lange Deutsche kolum')

    # header.set_header(columns)
    return header

###########################################################
#                       Tests
###########################################################

def test_add_column(simple_header):
    """ Test adding a column functionality """
    simple_header.add_column("python_name", col_id='a3e4f5')
    assert simple_header['python_name'] == 'a3e4f5'

def test_remove_column(simple_header):
    """ Test removing of column functionality """
    del simple_header['3rd_one']
    # simple_header.remove_column('3rd_one')
    assert '3rd_one' not in simple_header.types
    assert 'das lange Deutsche kolum' not in simple_header.names


def test_rename_column(simple_header):
    """ Test renaming a column """
    simple_header.set_type('datafr col', 'col2')
    # simple_header.rename_column('column2', 'col2')
    assert 'col2' in simple_header.types
    assert 'column2' not in simple_header.types


def test_get_all_columns(simple_header):
    result = simple_header.types
    expected = ['col1', 'column2', '3rd_one']
    assert len(result) == 3
    compare_iterables(expected, result)


def test_get_all_user_defined_columns(simple_header):
    result = simple_header.names
    expected = ['user_col1', 'datafr col', 'das lange Deutsche kolum']
    assert len(result) == 3
    compare_iterables(expected, result)


@pytest.mark.skip(reason="not implemented yet")
def test_get_price_components():
    pass


def test_reverse_translate(simple_header):
    assert simple_header.byname['datafr col'].type == 'column2'

def test_get_column_id(simple_header):
    assert simple_header['3rd_one'] == 'deadbeef'
