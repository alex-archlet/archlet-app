"""
Testing functionality of the expression evaluator.

Questions:
- what to do when the given expression string cannot be parsed by ast.parse?
    --> catch and pass on the exception?
- what to do with the deep copy code in ExpressionHandler?
    --> This is done to avoid that names are substituted more than once
"""
import ast
import pandas
import pathlib
import pytest

from shared.utilities.expression_handler import AssignmentCheckNodeVisitor
from shared.utilities.expression_handler import AttributeNameCheckVisitor
from shared.utilities.expression_handler import VolumeAndPriceVisitor
from shared.utilities.expression_handler import ExpressionHandler


###########################################################
#                       Test Fixtures
###########################################################

@pytest.fixture(scope="module")
def long_expression():
    """" Returns a string with a long cost calculation in python code """
    return "total_price = vol1*price1 + fixed_price + " +\
        "vol2*(price1-discount1) + fixed_cost"

@pytest.fixture(scope="module")
def long_tree(long_expression):
    """ Returns an ast tree object from the long expression """
    return ast.parse(long_expression, mode='exec')

@pytest.fixture
def wrong_long_tree():
    """
    Returns an ast tree object that contains names that are not
    in the mapping long_mapping
    """
    wrong_expression = "norm_price = vol1*price1 + volume2*price3"
    return ast.parse(wrong_expression, mode='exec')

@pytest.fixture(scope="module")
def long_mapping():
    """ Mapping that defines the components of the expression """
    return {'price1': 'PRICE',
            'price2': 'PRICE',
            'price3': 'PRICE',
            'discount1': 'PRICE_DISCOUNT',
            'vol1': 'VOL',
            'vol2': 'VOL',
            'vol3': 'VOL',
            'fixed_cost': 'PRICE',
            'fixed_price': 'PRICE'}

@pytest.fixture(scope="module")
def mapping_values():
    """
    Returns a dictionary with values for the variables defined
    in the long mapping
    """
    return {'price1': 5,
            'price2': 7,
            'price3': 4,
            'discount1': .2,
            'vol1': 300,
            'vol2': 350,
            'vol3': 200,
            'fixed_cost': 1000,
            'fixed_price': 50}


@pytest.fixture(scope="module")
def no_assignment():
    """ Expression without an assignment """
    expression = "alpha + beta - x"
    return ast.parse(expression, mode='exec')

@pytest.fixture(scope="module")
def offers_table():
    """
    Returns a table that contains the content of an actual offers table
    """
    path = pathlib.Path('./').absolute()
    path = path /'resources'/ 'offers_table_expression.csv'

    offers = pandas.read_csv(path)
    return offers

@pytest.fixture(scope="module")
def assignment_checker():
    """
    Returns an node visitor that checks whether an expression is an assignment
    """
    return AssignmentCheckNodeVisitor()

@pytest.fixture(scope="module")
def long_name_checker(long_mapping):
    """
    Returns a node visitor that checks whether an expression has correct names
    as specified in long_mapping
    """
    return AttributeNameCheckVisitor(long_mapping)

@pytest.fixture(scope="module")
def expression_handle(long_expression, long_mapping):
    """
    Returns an expression handler object to extract volume,
    variable price and fixed price.
    This handler is setup to use the long mapping and decompose
    the long expression.
    """
    return ExpressionHandler("offers", long_expression, long_mapping)

###########################################################
#                       Tests
###########################################################

def test_assignment_check_true(assignment_checker, long_tree):
    """ Check that long_tree is an assignment """
    assert assignment_checker.visit(long_tree)

def test_assignment_check_false(assignment_checker, no_assignment):
    """ Check that no_assignment is not an assignment """
    assert not assignment_checker.visit(no_assignment)

def test_attribute_name_check_true(long_name_checker, long_tree):
    """
    Check that long_tree has correct names according to
    long_mapping
    """
    assert long_name_checker.visit(long_tree)

def test_attribute_name_check_false(long_name_checker, wrong_long_tree):
    """
    Check that wrong_long_tree does not have correct names according
    to long_mapping
    """
    assert not long_name_checker.visit(wrong_long_tree)

@pytest.mark.parametrize("expression,exp_vol,exp_avg_price,exp_f_price",
                         [("tot_price = price1 * vol1",
                           300, 5, 0),
                          ("tot_price = 3 * vol2 * price2 + fixed_cost",
                           350, 21, 1000),
                          (("tot_price = 2 * (vol1 * price1 * discount1 + fixed_price) + "
                            "2 * (fixed_cost + 3 * (vol2 * price2 + vol3 * price3))"),
                           850, 23.65, 2100),
                          ("tot_price = vol1*price1 + vol2*(price1 - discount1) + fixed_price",
                           650, 4.89, 50),
                          ("tot_price = (fixed_price + vol1*price1) + (vol3*price3 + fixed_cost)",
                           500, 4.6, 1050),
                          ("tot_price = (vol1*price2 + fixed_cost)/price1",
                           300, 1.4, 200),
                          ("tot_price = (vol2*price3)/discount1",
                            350, 20, 0)])
def test_vol_extraction(long_mapping,
                        mapping_values,
                        expression,
                        exp_vol,
                        exp_avg_price,
                        exp_f_price):
    """
    Check that the provided expression is properly decomposed into:
     - a volume calculation,
     - a variable average price calculation, and
     - a fixed price calculation
    """
    visitor = VolumeAndPriceVisitor(long_mapping)
    visitor.visit(ast.parse(expression))

    tot_vol = "tot_vol"
    avg_price = "avg_price"
    f_price = "f_price"

    volume_tree = visitor.get_volume_tree(tot_vol)
    avg_price_tree = visitor.get_norm_price_tree(avg_price)
    fixed_price_tree = visitor.get_fixed_price_tree(f_price)

    print(ast.dump(volume_tree))
    print(ast.dump(avg_price_tree))
    print(ast.dump(fixed_price_tree))

    vol_calculator = compile(volume_tree, '<string>', mode='exec')
    avg_price_calculator = compile(avg_price_tree, '<string>', mode='exec')
    fixed_price_calculator = compile(fixed_price_tree, '<string>', mode='exec')

    exec(vol_calculator, mapping_values)
    exec(avg_price_calculator, mapping_values)
    exec(fixed_price_calculator, mapping_values)

    assert mapping_values[tot_vol] == exp_vol
    assert int(mapping_values[avg_price]) == int(exp_avg_price)
    assert mapping_values[f_price] == exp_f_price

@pytest.mark.parametrize("expression",
                         ["tot_price = (vol1 + fixed_price) * (vol2*price2 + fixed_cost)",
                          "tot_price = (vol1*price3)**3",
                          "tot_price = (vol1 + price2) / (vol2 + fixed_cost)"])
def test_vol_extraction_exceptions(long_mapping, expression):
    """ Check that the given expressions raise not implemented errors """
    visitor = VolumeAndPriceVisitor(long_mapping)
    with pytest.raises(NotImplementedError):
        visitor.visit(ast.parse(expression))

@pytest.mark.parametrize("expression,expected",
                         [("someName = hallo * verdikke",'someName'),
                          ("another_column = 123 % 3 + internal_col",'another_column')])
def test_get_assignment_name(expression, expected):
    handler = ExpressionHandler('table1', expression, [])
    assert handler.assignment_name == expected

@pytest.mark.parametrize("expression,expected_result",
                         [("new_col = (vol1*price2)", 2100),
                          ("new_col = (vol1+vol2)*price1 + vol3 * vol2", 73250),
                          ("new_col = (price1 ** 2 + price2) * vol1", 9600)])
def test_original_expression_execution(long_mapping, mapping_values, expression, expected_result):
    handler = ExpressionHandler('some_table', expression, long_mapping)
    assert handler.is_assignment()
    assert handler.has_valid_attribute_names()

    calculator = handler.get_original_calculator()

    dictionary_locals = {'some_table': mapping_values}
    exec(calculator, dictionary_locals)
    assert dictionary_locals[handler.assignment_name] == expected_result

def test_expression_handler(expression_handle, mapping_values):
    """ Test the expression handler object """
    assert expression_handle.is_assignment()
    assert expression_handle.has_valid_attribute_names()
    expression_handle.decompose_price_expression()

    vol1 = mapping_values['vol1']
    vol2 = mapping_values['vol2']
    price1 = mapping_values['price1']
    discount1 = mapping_values['discount1']
    fixed_price = mapping_values['fixed_price']
    fixed_cost = mapping_values['fixed_cost']

    tot_vol = "tot_vol"
    avg_price = "avg_price"
    f_price = "f_price"

    vol_calculator = expression_handle.get_total_volume_calculator(tot_vol)
    var_price_calculator = expression_handle.get_normalized_price_calculator(avg_price)
    fixed_price_calculator = expression_handle.get_fixed_price_calculator(f_price)

    offers = mapping_values
    dictionary_locals = {'offers': offers}

    exec(vol_calculator, dictionary_locals)
    exec(var_price_calculator, dictionary_locals)
    exec(fixed_price_calculator, dictionary_locals)

    assert dictionary_locals[tot_vol] == (vol1 + vol2)
    assert dictionary_locals[avg_price] == (
        vol1*price1 + vol2*(price1-discount1))/(vol1 + vol2)
    assert dictionary_locals[f_price] == (fixed_price + fixed_cost)

def test_vol_extraction_with_pandas_execution(offers_table):
    """ Test the expression handler object with an actual offers table """
    expression = "price = offer_id + demand_id*raw_price"

    mapping = {'price': 'PRICE',
               'offer_id': 'PRICE',
               'demand_id': 'VOL',
               'raw_price': 'PRICE'}

    expression_handle = ExpressionHandler("offers", expression, mapping)
    assert expression_handle.is_assignment()
    assert expression_handle.has_valid_attribute_names()

    expression_handle.decompose_price_expression()

    tot_vol = "tot_vol"
    avg_price = "avg_price"
    f_price = "f_price"

    vol_calculator = expression_handle.get_total_volume_calculator(tot_vol)
    var_price_calculator = expression_handle.get_normalized_price_calculator(avg_price)
    fixed_price_calculator = expression_handle.get_fixed_price_calculator(f_price)

    dictionary_locals = {'offers': offers_table}
    exec(vol_calculator, dictionary_locals)
    exec(var_price_calculator, dictionary_locals)
    exec(fixed_price_calculator, dictionary_locals)

    assert dictionary_locals[tot_vol].equals(offers_table['demand_id'])
    assert dictionary_locals[avg_price].equals(offers_table['raw_price']*\
        offers_table['demand_id']/offers_table['demand_id'])
    assert dictionary_locals[f_price].equals(offers_table['offer_id'])
