import pandas as pd
import pathlib
import pytest

from engine.src.table_processors import Filter
from shared.constants import CONSTRAINT_SPECS
from shared.constants import UKEYS, KEYS
from shared.constants import OPERATION_TYPE
from shared.constants import RecordedError
from shared.constants import SEVERITY
from shared.entities import TableData
from shared.entities import TableHeader
from shared.entities import Translator
from test.utils import add_header

################################################################################################
#								   FIXTURES
################################################################################################

@pytest.fixture
def simple_offers_td():
    datafr = pd.DataFrame(data={UKEYS.DEMAND_ID:['item_1', 'item_1', 'item_1', 'item_2', 'item_2', 'item_3', 'item_3'],
                                UKEYS.ITEM_ID:['item_1', 'item_1', 'item_1', 'item_2', 'item_2', 'item_3', 'item_3'],
                                'supplier':['supA', 'supC','supB','supA','supB','supB','supD'],
                                'category':['type1','type2','type1','type2','type2','type2','type2'],
                                'region': ['reg1', 'reg1', 'reg1', 'reg2', 'reg2', 'reg1', 'reg1'],
                                'type': ['type1', 'type1', 'type1', 'type2', 'type2', 'type2', 'type2'],
                                'quality': [100, 80, 80, 100, 90, 80, 95],
                                UKEYS.NORMALIZED_PRICE: [600, 500, 300, 400, 500, 420, 510],
                                'price_usd':[600, 500, 300, 400, 500, 420, 510],
                                KEYS.VOLUME:[10, 12, 8, 10, 10, 12, 14]
                                })

    table = TableData()
    table.data = datafr
    add_header(table)

    return table

@pytest.fixture
def simple_baseline_td():
    datafr = pd.DataFrame(data={UKEYS.DEMAND_ID:['item_1', 'item_2', 'item_3'],
                                UKEYS.ITEM_ID:['item_1', 'item_2', 'item_3'],
                                'supplier':['supA', 'supC', 'supB'],
                                'category':['type1','type2','type1'],
                                'region': ['reg1', 'reg2', 'reg1'],
                                'type': ['type1', 'type2', 'type1'],
                                'quality': [90, 90, 90],
                                UKEYS.NORMALIZED_PRICE: [600, 500, 300],
                                'price_usd':[600, 500, 300],
                                KEYS.VOLUME:[12, 10, 14]
                                })

    table = TableData()
    table.data = datafr
    add_header(table)
    return table

@pytest.fixture
def simple_demands_td():
    datafr = pd.DataFrame(data={UKEYS.DEMAND_ID:['item_1', 'item_2', 'item_3'],
                                UKEYS.ITEM_ID:['item_1', 'item_2', 'item_3'],
                                UKEYS.SUPPLIER:['supA', 'supC','supB'],
                                'category':['type1','type2','type1'],
                                KEYS.VOLUME:[12, 10, 14],
                                KEYS.PRICE:[600, 500, 300]
                                })

    table = TableData()
    table.data = datafr
    add_header(table)

    return table
