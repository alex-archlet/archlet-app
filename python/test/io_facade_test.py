import pytest


from shared.communication import IOFacade, DatabaseConnection
from shared.communication.database_classes import JobInfoDB, BranchesDB



branch = BranchesDB(id = 1, solver="GRB")
job_info = JobInfoDB(id = 1, project_id=1, branch_id=1, branch = branch)


""" ------------------ FETCH JOB TEST ------------------ """

def test_fetch_job(monkeypatch):
    
    def mock_job(schema = JobInfoDB, query: dict = { "id" : 1 }):
        return job_info

    io_facade = IOFacade('', '')
    monkeypatch.setattr(io_facade, "database_connection", DatabaseConnection('','','',8000,''))
    monkeypatch.setattr(io_facade.database_connection, "fetch_one_or_none", mock_job)

    assert io_facade.fetch_job() == mock_job()
    assert io_facade.project_id == job_info.project_id
    assert io_facade.branch_id == job_info.branch_id
    assert io_facade.solver == job_info.branch.solver