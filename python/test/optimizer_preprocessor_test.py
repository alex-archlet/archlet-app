import pandas as pd
import pytest

from shared.entities import TableData
from engine.src.optimization.optimizer_preprocessor import compute_items_per_bucket
from engine.src.optimization.optimizer_preprocessor import transform_incremental_buckets
from engine.src.optimization.optimizer_preprocessor import create_discount_buckets

from .utils import add_header, compare_iterables


@pytest.fixture
def discounts():
    discount_frame = pd.DataFrame(data={'bucket_id':['B1', 'B2', 'B2', 'B3', 'B3', 'B3'],
                                        'supplier': ['supA', 'supA', 'supA', 'supB', 'supB', 'supB'],
                                        'threshold': [100, 100, 150, 200, 100, 150],
                                        'discount_amount': [.2, .1, .3, .3, .1, .15],
                                        'discount_type': ['proportional', 'flat', 'flat', '%', '%', '%'],
                                        'discount_unit': ['volume', 'spend', 'spend', 'volume', 'volume', 'volume'],
                                        'price_component': ['Total Price', 'Total Price', 'Total Price', 'Total Price', 'Total Price', 'Total Price']})

    table = TableData()
    table.data = discount_frame
    table.trim()
    add_header(table)
    return table

@pytest.fixture
def tiny_offers():
    frame = pd.DataFrame(data={'item': ['item_1', 'item_2'],
                               'demand_int_id': [0, 1],
                               'contributing_to': [['B1', 'B2'], ['B1', 'B3']],
                               'discounted_by': [['B1', 'B2'], ['B3']],
                               'supplier': ['supA', 'supB'],
                               'item_type': ['standard', 'standard']})

    table = TableData()
    table.data = frame
    table.trim()
    add_header(table)
    return table

@pytest.fixture
def mapping_dic(tiny_offers):
    unique_suppliers = tiny_offers[tiny_offers.header['supplier']]
    supplier_to_id = {sup: i for i,sup in enumerate(unique_suppliers)}
    item_type_to_id = {'standard': 0}
    return {'supplier_to_id': supplier_to_id,
            'item_type_to_id': item_type_to_id}


@pytest.fixture
def buckets_per_item():
    result = dict()
    result[('sup_a', 'item1', 'standard')] = ['B1', 'B3']
    result[('sup_a', 'item2', 'standard')] = ['B2', 'B4', 'B8']
    result[('sup_a', 'item3', 'standard')] = ['B3', 'B5']
    result[('sup_d', 'item4', 'standard')] = ['B6', 'B7']
    return result

@pytest.fixture
def supplier_to_id(buckets_per_item):
    suppliers = [pair[0] for pair in buckets_per_item.keys()]
    suppliers = set(suppliers)
    result = {sup: i for i, sup in enumerate(suppliers)}
    return result

@pytest.fixture
def bucket_mapping():
    result = dict()
    result['B3'] = ['B3_first', 'B3_second']
    result['B6'] = ['level1_for_6', 'level3_for_6', 'second_level']
    return result

def test_compute_items_per_bucket(buckets_per_item, bucket_mapping, supplier_to_id):
    result = compute_items_per_bucket(buckets_per_item, bucket_mapping, supplier_to_id)
    compare_iterables([('item1', 'standard'), ('item3', 'standard')], result[supplier_to_id['sup_a']]['B3_first'])
    compare_iterables([('item4', 'standard')], result[supplier_to_id['sup_d']]['B7'])

def test_transform_incremental_buckets(discounts):
    result, mapping = transform_incremental_buckets(discounts)
    compare_iterables(['B3_0', 'B3_1', 'B3_2'], mapping['B3'])
    assert 'B2_0' in result[result.header['bucket_id']].values
    result.data.set_index(result.header['bucket_id'], inplace=True)
    assert result.data.loc['B3_0', result.header['discount_amount']] - .1 < 0.001
    assert result.data.loc['B3_1', result.header['discount_amount']] - .05 < 0.001
    assert result.data.loc['B3_2', result.header['discount_amount']] - .15 < 0.001

def test_create_discount_buckets(discounts, tiny_offers, mapping_dic):
    result = create_discount_buckets(discounts, tiny_offers, mapping_dic)
    assert len(result) == 6