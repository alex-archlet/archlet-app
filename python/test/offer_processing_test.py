import pandas as pd
import numpy as np
from numpy import NaN
import pytest

from shared.entities import TableData
from shared.entities import TableHeader
from engine.src.table_processors import offer_processing_utils
from shared.communication import external_api_caller
from shared.constants import RecordedError, KEYS, OUTLIER, UKEYS
from test.utils import add_header, compare_iterables, simple_demands_td, simple_offers_td


@pytest.fixture
def simple_outlier_td():
    # initial table data
    datafr = pd.DataFrame(data={"price":[75, 156, 153, 100, 146, 0, 168, 1000, 116, 153],
								"currency": ["EUR", "EUR", "EUR", "EUR", "EUR", "EUR", "EUR", "EUR", "EUR", "EUR"]
								})

    table = TableData()
    table.data = datafr
    table.trim()
    add_header(table)
    table.header.set_type("price", KEYS.PRICE)
    table.header.set_type("currency", KEYS.CURRENCY)

    # expected dataframe
    final_datafr = pd.DataFrame(data={
                                "price":[NaN, NaN, NaN, NaN, NaN, NaN, NaN, 'OFFER_PROCESSING_WARNING_OUTLIER', NaN, NaN],
                                "currency": [NaN, NaN, NaN, NaN, NaN, NaN, NaN, NaN, NaN, NaN]
                                })

    return table, final_datafr


def mock_get_currency_rates(from_currency: str):
    ##
        #   Mocked Output:
            #   STATUS 200: A dictionary of exchange rates from EUR to other currencies
            #   ELSE:       Stringified error message
    ##
    if from_currency == 'EUR':
        return { "CHF": 1.0726, "USD": 1.1821, "GBP": 0.90273, "EUR": 1 }
    else:
        return f"Base {from_currency} is not supported."


def test_get_rates(monkeypatch, from_currency: str = "EUR"):
    ##
        #   Get rate from "EUR" to "USD"
            #   Expected return: A dictionary with a single of conversion rate from EUR to USD
    ##
    monkeypatch.setattr(external_api_caller, "get_currency_rates", mock_get_currency_rates)

    rates_to_currency = offer_processing_utils.get_rates(["EUR"], "USD")
    assert rates_to_currency == { 'EUR': 1.1821 }


def test_currency_conversion(monkeypatch):
    ##
        #   Convert unique currencies "EUR", "USD" and "GBP" to "USD" in a given price column
            #   Expected return: A pd.Series of the converted price column
    ##
    def mock_get_rates(unique_currencies = pd.Series(["EUR", "USD", "GBP"]), to_currency= "USD"):
        return { "EUR": 1.1821, "GBP": 1.3095, "USD": 1 }

    monkeypatch.setattr(offer_processing_utils, "get_rates", mock_get_rates)

    price_column = pd.Series([130, 100, 90, 75, 95, 80, 98, 111, 105, 1000])
    currency_column = pd.Series(["EUR", "EUR", "USD", "USD", "USD", "USD", "USD", "USD", "GBP", "USD"])
    result = list(offer_processing_utils.convert_to_currency(price_column, currency_column))
    expected = [153.673, 118.21, 90, 75, 95, 80, 98, 111, 137.4975, 1000]
    compare_iterables(expected, result)


def test_outlier_detection(simple_outlier_td):
    ##
        #   Find outliers in a given table data
            #   Expected return: A dataframe consisting of found outliers and NaNs
    ##
    table, final_table = simple_outlier_td
    actual = offer_processing_utils.find_outlier_cells(table)[KEYS.PRICE].to_list()
    expected = list(final_table[KEYS.PRICE])
    compare_iterables(list(expected), list(actual))


def test_delete_offers_no_demands(simple_offers_td, simple_demands_td):
    # Test that no offers are removed if data is matching
    original_offers = simple_offers_td.data.copy()
    offers = offer_processing_utils.delete_offers_no_demands(simple_offers_td, simple_demands_td)

    assert len(original_offers) == len(offers.data)


def test_delete_offers_no_demands_drop(simple_offers_td, simple_demands_td):
    # Test that offers are removed if data is not fully matching
    original_offers = simple_offers_td.data.copy()
    offers_data = simple_offers_td.data
    offers_data.loc[1, simple_offers_td.header[UKEYS.ITEM_ID]] = 'item_4'
    simple_offers_td.data = offers_data

    offers = offer_processing_utils.delete_offers_no_demands(simple_offers_td, simple_demands_td)
    assert len(original_offers) != len(offers.data)


def test_delete_duplicated_offers(simple_offers_td, simple_demands_td):
    # Test that no offers are removed if there are no duplicated offers
    original_offers = simple_offers_td.data.copy()
    offers = offer_processing_utils.delete_duplicated_offers(simple_offers_td)

    assert len(original_offers) == len(offers.data)


def test_delete_duplicated_offers_drop(simple_offers_td, simple_demands_td):
    # Test that  offers are removed if there are duplicated offers
    simple_offers_td.data = pd.concat([simple_offers_td.data]*2, ignore_index=True)
    original_offers = simple_offers_td.data.copy()
    offers = offer_processing_utils.delete_duplicated_offers(simple_offers_td)

    assert len(original_offers) != len(offers.data)


def test_delete_empty_offers(simple_offers_td, simple_demands_td):
    # Test that no offers are removed if there are no duplicated offers
    original_offers = simple_offers_td.data.copy()
    offers = offer_processing_utils.delete_empty_offers(simple_offers_td)

    assert len(original_offers) == len(offers.data)


def test_delete_empty_offers_drop(simple_offers_td, simple_demands_td):
    # Test that offers are removed if data is not fully matching
    original_offers = simple_offers_td.data.copy()
    offers_data = simple_offers_td.data
    offers_data.loc[1, simple_offers_td.header[KEYS.PRICE]] = 0
    simple_offers_td.data = offers_data

    offers = offer_processing_utils.delete_empty_offers(simple_offers_td)
    assert len(original_offers) != len(offers.data)
