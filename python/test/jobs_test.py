# import os
# import pytest
# from engine.src.main import execute_job

# @pytest.fixture
# def DB_los14():
#     type_to_id = {
#         'processing_job': '32833500-9729-4c50-9ba1-26c18f475713',
#         'project_info_job': '8f3088ac-1384-48d9-901f-48db4c620c37',
#         'optimization_ms': 'c3aa1e25-0088-438c-9ad2-e85f3ebef042',
#         'optimization_custom': '69aebf6b-759f-4ca1-af7a-8c3568284d36',
#         'create_scenario_job': '34f72fc4-5023-43c2-a2c5-6f33b4333f6e',
#         'postprocessing_job': '3d57f260-3470-4a72-83cc-c3379a6245b9'
#     }
#     return type_to_id

# @pytest.fixture
# def resins():
#     type_to_id = {
#         'processing_job': '295af48d-5c41-4d35-9e29-a3c5215d3a90',
#         'project_info_job': 'b30a5409-61a3-48f5-9389-e66b090eadf9',
#         'optimization_ms': '7fefbd79-9bcb-43c3-9a0e-f3b85d927a84',
#         'optimization_custom': '3b3d124a-d891-4efb-b391-755e97205579',
#         'create_scenario_job': 'c50fa7cb-159f-42fb-b7ab-7d376a4292a2',
#         'postprocessing_job': '2c7b9e9e-11a1-4fa8-a7ba-54752ecacbbf'
#     }
#     return type_to_id

# @pytest.mark.skip(reason="Outdated DB")
# def test_db_los14(DB_los14):
#     # Execute job
#     for name, id in DB_los14.items():
#         print('Starting ', name)
#         uid = os.urandom(5).hex()
#         execute_job(uid=uid, job_id=id)

# @pytest.mark.skip(reason="Outdated DB")
# def test_resins(resins):
#     # Execute job
#     for name, id in resins.items():
#         print('Starting ', name)
#         uid = os.urandom(5).hex()
#         execute_job(uid=uid, job_id=id)
