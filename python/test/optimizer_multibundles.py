import pandas as pd
import pathlib
import pytest
import random

from collections import defaultdict

from engine.src.optimization import Optimizer
from engine.src.table_processors import Preprocessor
from shared.communication import IOFacade
from shared.communication.database_connection import DBFile as File
from shared.constants import UKEYS, KEYS
from shared.constants import RecordedError
from shared.entities import TableData


@pytest.fixture()
def state_object():
	def _state_object():
		iofacade = IOFacade(0, './')
		file_path = './resources/'
		name = 'offers_table'
		version = 1
		extension = '.csv'
		file = File(file_path, name, version, extension)
		offers = TableData()
		iofacade.load_table(offers, file)
		name = 'demands_table'
		version = 1
		extension = '.csv'
		file = File(file_path,name,version,extension)
		demands = TableData()
		iofacade.load_table(demands, file)
		name = 'discounts_table'
		version = 1
		extension = '.csv'
		file = File(file_path,name,version,extension)
		discounts = TableData()
		iofacade.load_table(discounts, file)

		preprocessor = Preprocessor()
		offers = preprocessor.process_table(offers, 'offers')
		demands = preprocessor.process_table(demands, 'demands')
		discounts = preprocessor.process_table(discounts, 'discounts')

		solver = 'CBC'

		return offers, demands, discounts
	return _state_object

def test(state_object):
	offers, demands, discounts = state_object()

	possible_brands = ['brand1', 'brand2', 'brand3']
	offers['brand'] = ''
	offers['brand'] = offers['brand'].apply(lambda x: random.choice(possible_brands))

	empty_params = defaultdict(lambda: None, {"name":"Test","rebates":True, "capacity":False, "split_items":True, "price_split":[],"max_suppliers":{},
												"scenario_type":"custom","excluded_suppliers":[],"volume_constraints":[],
												"max_spend_percentage":{"spend":{},"volume":{},"capacity":{},"percentage":{}}})

	optimizer = Optimizer(empty_params, offers, demands, discounts)
	assert optimizer.initialize_model() == True
	allocations = optimizer.run()

	# costs = allocations_table['volume'].multiply(allocations_table['unit_price'])
	# spend_baseline = costs[(allocations_table['brand'] == 'brand2') & (allocations_table.byname['region'].id == 'Asia')].sum()

	# volume_baseline = allocations_table['volume'][(allocations_table['brand'] == 'brand2') & (allocations_table.byname['region'].id == 'Asia')].sum()

	num_supplier_baseline = len(allocations[(allocations['brand'] == 'brand2') & (allocations.byname['region'].id == 'Asia')]['supplier'].unique())

	# params = {"name":"Test","rebates":True, "capacity":False, "split_items":True, "price_split":[],"max_suppliers":{},
	# 		"scenario_type":"custom","excluded_suppliers":[],"volume_constraints":[],
	# 		"max_spend_percentage":{"spend":{},"volume":{},"capacity":{},"percentage":{}},
	# 		'volume':[{'bundles':{'brand':'brand2', 'region':'Asia'}, 'operation':'at_most', 'value':(8/10)*volume_baseline}]}

	# params = {"name":"Test","rebates":True, "capacity":False, "split_items":True, "price_split":[],"max_suppliers":{},
	# 		"scenario_type":"custom","excluded_suppliers":[],"volume_constraints":[],
	# 		"max_spend_percentage":{"spend":{},"volume":{},"capacity":{},"percentage":{}},
	# 		'spend':[{'bundles':{'brand':'brand2', 'region':'Asia'}, 'operation':'at_most', 'value':(8/10)*spend_baseline}]}


	# params = {"name":"Test","rebates":True, "capacity":False, "split_items":True, "price_split":[],"max_suppliers":{},
	# 		"scenario_type":"custom","excluded_suppliers":[],"volume_constraints":[],
	# 		"max_spend_percentage":{"spend":{},"volume":{},"capacity":{},"percentage":{}},
	# 		'number_of_suppliers':[{'filters':{'region':'Asia'}, 'operation':'At most', 'value':6}]}


	params = {'name': 'tdkhjloihuyvh,bn', 'volume': [], 'rebates': True, 'incumbent': False, 'item_type': [], 'capacities': [], 'limit_spend': [], 'price_split': [], 'split_items': True, 'multi_splits': [], 'scenario_type': 'custom', 'excluded_suppliers': [],
	 'number_of_suppliers': [{'value': '6', 'filters': {'region': 'Asia'}, 'operation': 'At most'}], 'volume_per_supplier': []}

	optimizer = Optimizer(params, offers, demands, discounts)
	assert optimizer.initialize_model() == True
	allocations_table = optimizer.run()

	# volume_constraint = allocations_table['volume'][(allocations_table['brand'] == 'brand2') & (allocations_table.byname['region'].id == 'Asia')].sum()

	# costs = allocations_table['volume'].multiply(allocations_table['unit_price'])
	# spend_constraint = costs[(allocations_table['brand'] == 'brand2') & (allocations_table.byname['region'].id == 'Asia')].sum()
	#
	# print('\nVolume for items with brand2 and in Asia : ', volume_baseline)
	#
	# print('Volume for items with brand2 and in Asia with constraint: ', volume_constraint)

	# print('\nSpend for items with brand2 and in Asia : ', spend_baseline)
	#
	# print('Spend for items with brand2 and in Asia with constraint: ', spend_constraint)

	num_supplier = len(allocations_table[(allocations_table['brand'] == 'brand2') & (allocations_table.byname['region'].id == 'Asia')]['supplier'].unique())

	print('\nNumber of suppliers for items with brand2 and in Asia : ', num_supplier_baseline)

	print('Number of suppliers for items with brand2 and in Asia with constraint: ', num_supplier)
