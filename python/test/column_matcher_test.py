"""
Test suite for the column matcher table processor
"""
import numpy as np
import pandas as pd
import pytest

from engine.src.table_processors import ColumnMatcher
from shared.entities import TableData
from shared.entities import TableHeader
from test.utils import compare_iterables


###########################################################
#                   Test Fixtures
###########################################################

@pytest.fixture
def table_data():
    table = TableData()
    data = pd.DataFrame(data={'colum 1':['item 1', 'item 2', 'item 3', 'item 4'],
                              'second col': ['hallo', 'ik', 'ben', 'hier'],
                              'branch': ['lada', 'delorean', 'rover', 'saab'],
                              'volume': [100, 85, 12, 40],
                              'name': [None, None, None, None]})
    table.data = data
    table.trim()
    return table


@pytest.fixture
def table_header():
    header = TableHeader()
    # columns = {'col1': {'name': 'colum 1', 'type': 'ITEM', 'id': '4fdeab98', 'calc':None},
    #            'col2': {'name': 'second col', 'type': 'TEXT', 'id': '1234', 'calc':None},
    #            'col3': {'name': 'branch', 'type': 'CATEGORY', 'id': '54321', 'calc':None}}
    header.add_column('col1', '4fdeab98', 'colum 1')
    header.add_column('col2', '1234', 'second col')
    header.add_column('col3', '54321', 'branch')

    # header.set_header(columns)
    return header


@pytest.fixture
def row_source():
    row_src = ['ChemCorp', 'ChemCorp', 'ChemCorp', 'Supplier', 'Supplier']
    return np.asarray(row_src)


@pytest.fixture
def transformed_row_source():
    row_src = ['buyer', 'buyer', 'buyer', 'supplier', 'supplier']
    return np.asarray(row_src)


@pytest.fixture
def matcher():
    return ColumnMatcher()


###########################################################
#                       Tests
###########################################################

# TO DO: remove skip once the column matcher has a stable interface
@pytest.mark.skip(reason="column matcher interface is still under construction")
def test_create_template_from_table(matcher, table_data):
    result = matcher.create_template_from_table(table_data)
    expected = ['colum 1', 'second col', 'branch']
    compare_iterables(expected, result)


def test_match_columns(matcher, table_data, table_header):
    result = matcher.match_columns(table_data, table_header)
    expected = [{'matching': True,
                 'bid_column': '4fdeab98',
                 'excel_column': 'colum 1',
                 'message': ''},
                 {'matching': True,
                 'bid_column': '1234',
                 'excel_column': 'second col',
                 'message': ''},
                 {'matching': True,
                 'bid_column': '54321',
                 'excel_column': 'branch',
                 'message': ''},]
    compare_iterables(expected, result)


###########################################################
#                       Unit Tests
###########################################################

def test_check_type_column(matcher, table_data):
    # get column types
    resulting_types = table_data.data.apply(matcher._check_type_column, axis=0)
    expected = ['text', 'text', 'text', 'number', 'unknown']

    assert all(resulting_types == expected)

@pytest.mark.skip(reason="model cannot be downloaded")
def test_get_input_by_list(matcher, row_source, model_path='model/distiluse-base-multilingual-cased'):
    # cget input_by
    resulting_input_by = matcher._get_input_by_list(row_source, model_path)
    expected = ['buyer', 'buyer', 'buyer', 'supplier', 'supplier']

    assert all(resulting_input_by == expected)


def test_get_colname_to_source_mapping(matcher, table_data, transformed_row_source):
    # get columns
    columns = table_data.data.columns.astype(str)
    resulting_mapping = matcher._get_colname_to_source_mapping(columns, transformed_row_source)

    expected = {'colum 1': 'buyer', 'second col': 'buyer', 'branch': 'buyer', 'volume': 'supplier', 'name': 'supplier'}

    assert resulting_mapping == expected
