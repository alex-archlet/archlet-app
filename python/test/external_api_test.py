import pytest
from urllib3.connectionpool import HTTPConnectionPool
import responses
import requests

from shared.constants import RecordedError
from shared.communication import external_api_caller
from engine.src.table_processors.offer_processing_utils import get_rates


""" -------------------- REAL API CALLS CATCHER -------------------- """

@pytest.fixture(autouse=True)
def no_http_requests(monkeypatch):
    ## 
        #   Ensures that during test writing, we are not executing
		#   any HTTP requests by accident. If we do so, a RuntimeError
		#   will be raised!
    ##
    def urlopen_mock(self, method, url, *args, **kwargs):
        raise RuntimeError(
            f"The test was about to {method} {self.scheme}://{self.host}{url}"
        )

    monkeypatch.setattr(
        "urllib3.connectionpool.HTTPConnectionPool.urlopen", urlopen_mock
    )


""" ------------------ GET CURRENCY RATES TESTS ------------------ """


def mock_json_response(from_currency: str):
    ##
        #   Mocked Output:
            #   STATUS 200: A dictionary of exchange rates from EUR to other currencies
            #   ELSE:       Stringified error message
    ##
    if from_currency == 'EUR':
        return { "rates": { "CHF": 1.0726, "USD": 1.1821, "GBP": 0.90273, "EUR": 1 } }
    else:
        return f"Base {from_currency} is not supported."


@responses.activate
def test_convert_to_EUR_currency(from_currency: str = "EUR"):
    ##
        #   Conversion to "EUR" test
            #   Expected return: A dictionary of conversion rates from EUR to other currencies
    ##
    query = f"https://api.exchangeratesapi.io/latest?base={from_currency}"
    responses.add(
        responses.GET, query,
        json=mock_json_response(from_currency),
    )
    
    response = external_api_caller.get_currency_rates(from_currency)
    assert response == mock_json_response(from_currency)["rates"]


@responses.activate
def test_convert_to_Eurro_currency(from_currency: str = "Eurro"):
    ##
        #   Conversion to "Eurro" test
            #   Expected return: A stringified error message
    ##
    query = f"https://api.exchangeratesapi.io/latest?base={from_currency}"
    responses.add(
		responses.GET, query,
		body=requests.exceptions.HTTPError(mock_json_response(from_currency)),
        status=400
	)
    with pytest.raises(requests.exceptions.HTTPError) as error:
	    requests.get(query)
    assert str(error.value) == mock_json_response(from_currency)
