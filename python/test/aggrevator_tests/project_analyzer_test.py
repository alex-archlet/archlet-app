"""
Testing suite for the project analyzer

The two main responsibilities of the project analyzer are calculating
the project info dictionary and the price heat map.
"""
import numpy as np
import pandas as pd
import pytest
import simplejson

from engine.src.table_processors import Preprocessor
from shared.utilities.project_analyzer import ProjectAnalyzer
from shared.constants import KEYS, UKEYS
from shared.entities import ProjectSettings
from test.utils import *


###########################################################
#                       Test Fixtures
###########################################################


@pytest.fixture
def project_settings(translator, tables):
    """
    Returns a translator
    """
    raw_settings = Preprocessor.extract_project_settings(translator, tables['demands'].header, tables['offers'].header)
    settings = ProjectSettings()
    for setting, value in raw_settings.items():
        setattr(settings, setting, value)
    return settings


@pytest.fixture
def simple_offers_df():
    datafr = pd.DataFrame(data={'item':['item_1','item_1', 'item_1', 'item_2', 'item_2'],
                                'supplier':['supA', 'supA','supB','supA','supB'],
                                'category':['type1','type2','type1','type2','type2'],
                                'total_price':[600, 500, 300, 400, 500],
                                'comp1': [30, 40, 50, 30, 20],
                                'comp2': [110, 100, 93, 250, 230],
                                'country': ['Ukraine', 'Belarus', 'Poland', 'Ukraine', 'Belarus'],
                                'lang': ['EN', 'FR', 'EN', 'RU', 'FR']})
    return datafr


@pytest.fixture
def price_comps_def():
    return [{"name": "Total Price", "parent": "ROOT"},
            {"name": "price comp1", "parent": "Total Price"},
            {"name": "price comp2", "parent": "price_comp 3"},
            {"name": "price_comp 3", "parent": "Total Price"},
            {"name": "grand child 1", "parent": "price comp2"},
            {"name": "grand child 2", "parent": "price comp2"},
            {"name": "price comp 5", "parent": "price_comp 3"}]


@pytest.fixture
def analyzer():
    return ProjectAnalyzer()


###########################################################
#                       helper function
###########################################################


def compare_lists(expected, result):
    for entry in expected:
        assert entry in result


###########################################################
#                       Tests
###########################################################


def test_filters_list(simple_offers_df, analyzer):
    """ test extraction of filters list """
    filter_cols = ['country', 'lang']
    countries = ['Ukraine', 'Belarus', 'Poland']
    langs = ['EN', 'FR', 'RU']
    expected = [{'name': 'country', 'values': countries},
                {'name': 'lang', 'values': langs}]
    list_of_filters = analyzer._filters_list(simple_offers_df, filter_cols)
    assert len(list_of_filters) == 2
    compare_lists(expected, list_of_filters)


def test_item_types(simple_offers_df, analyzer):
    """ test extraction of item types """
    result = analyzer._item_types(simple_offers_df, 'category')
    expected = ['type1', 'type2']
    assert len(result) == 2
    compare_lists(expected, result)


def test_price_components_list(analyzer, price_comps_def):
    result = analyzer._price_components_list(price_comps_def)
    assert len(result) == 7
    expected = ["Total Price",
                "price comp1",
                "price comp2",
                "price_comp 3",
                "grand child 1",
                "grand child 2",
                "price comp 5"]
    compare_lists(expected, result)


def test_project_info(analyzer, tables, project_settings):
    result = analyzer.project_info(tables['offers'], tables['baseline'], tables['demands'], project_settings)
    assert 'items'in result and result['items'] is not None
    assert 'num_items' in result and result['num_items'] is not None
    assert 'suppliers' in result and result['suppliers'] is not None
    assert 'num_suppliers' in result and result['num_suppliers'] is not None
    assert 'filters' in result and result['filters'] is not None
    assert 'criteria' in result and result['criteria'] is not None
    assert 'price_components' in result and result['price_components'] is not None
    assert 'item_types' in result and result['item_types'] is not None
    assert 'custom_columns' in result and result['custom_columns'] is not None


def test_add_min_max_prices(simple_offers_df):
    result = ProjectAnalyzer._add_min_max_prices(simple_offers_df,
                                                identifier_list=['item'],
                                                price_components=['total_price','comp1'])

    new_cols = ['total_price_min',
                'total_price_max',
                'comp1_min',
                'comp1_max']
    # Check that all new columns are present in result
    for col in new_cols:
        assert col in result.columns
    result = result.set_index(['item', 'supplier', 'category'])
    # Do a random check of a few elements
    assert result.loc['item_1', 'supA', 'type1']['total_price_min'] == 300
    assert result.loc['item_2', 'supA', 'type2']['total_price_max'] == 500
    assert result.loc['item_1', 'supB', 'type1']['comp1_min'] == 30


def test_add_price_deviations(simple_offers_df):
    ids = ['item']
    price_comps = ['total_price','comp1','comp2']
    result = ProjectAnalyzer._add_min_max_prices(simple_offers_df,
                                                identifier_list=ids,
                                                price_components=price_comps)
    result = ProjectAnalyzer._add_price_deviations(result, price_components=price_comps)
    new_cols = ['total_price_deviation',
                'comp1_deviation',
                'comp2_deviation']
    for col in new_cols:
        assert col in result.columns
    result = result.set_index(['item', 'supplier', 'category'])
    assert result.loc['item_1', 'supA', 'type2']['total_price_deviation'] == np.round((500-300)/300,2)
    assert result.loc['item_2', 'supA', 'type2']['comp2_deviation'] == np.round((250-230)/230,2)


def test_add_dict_column(simple_offers_df):
    ids = ['item', 'supplier', 'category']
    price_comps = ['total_price','comp1']
    heatmap = simple_offers_df[ids].copy()
    result = ProjectAnalyzer._add_dict_column(heatmap, simple_offers_df, ids, price_comps, 'prices')
    assert 'prices' in result.columns
    result = result.set_index(ids)
    print(result)
    assert result.loc['item_1', 'supB', 'type1']['prices'] == {'total_price': 300, 'comp1': 50}


def test_add_deviations_dict(simple_offers_df):
    ids = ['item', 'supplier', 'category']
    price_comps = ['comp2','comp1']
    heatmap = simple_offers_df[ids].copy()
    simple_offers_df = ProjectAnalyzer._add_min_max_prices(simple_offers_df, ['item'], price_comps)
    simple_offers_df = ProjectAnalyzer._add_price_deviations(simple_offers_df,price_comps)
    result = ProjectAnalyzer._add_deviations_dict(heatmap, simple_offers_df, ids, price_comps)
    assert 'deviations' in result.columns
    result = result.set_index(ids)
    print(result)
    assert result.loc['item_2', 'supA', 'type2']['deviations'] == {'comp1': np.round((30 - 20)/20,2), 'comp2': np.round((250-230)/230,2)}
    assert result.loc['item_2', 'supB', 'type2']['deviations'] == {'comp1': 0, 'comp2': 0}


def test_add_suppliers_dict(simple_offers_df):
    ids = ['item', 'supplier', 'category']
    price_comps = ['total_price','comp1']
    # create a heatmap source dataframe
    heatmap = simple_offers_df[ids].copy()
    simple_offers_df = ProjectAnalyzer._add_min_max_prices(simple_offers_df, ['item'], price_comps)
    simple_offers_df = ProjectAnalyzer._add_price_deviations(simple_offers_df,price_comps)
    heatmap = ProjectAnalyzer._add_deviations_dict(heatmap, simple_offers_df, ids, price_comps)
    heatmap = ProjectAnalyzer._add_dict_column(heatmap, simple_offers_df, ids, ['country'], 'manufacturing_places')
    # Create item level dataframe
    item_df = pd.DataFrame(data=simple_offers_df['item'].unique(), columns=['item'])
    item_df = ProjectAnalyzer._add_suppliers_dict(item_df, heatmap, 'item', 'category', 'supplier')
    assert 'suppliers' in item_df.columns
    item_df = item_df.set_index('item')
    assert item_df.loc['item_2']['suppliers']['supA']['type2']['deviations'] == {'total_price': 0, 'comp1': 0.5}
    assert item_df.loc['item_2']['suppliers']['supB']['type2']['manufacturing_places'] == {'country': 'Belarus'}


def test_min_max_prices(simple_offers_df):
    result = ProjectAnalyzer._get_min_max_prices(simple_offers_df, ['item'], ['total_price', 'comp1', 'comp2'])
    new_cols = ['total_price_min',
                'total_price_max',
                'comp1_min',
                'comp1_max',
                'comp2_min',
                'comp2_max']
    for col in new_cols:
        assert col in result.columns
    result = result.set_index('item')
    assert result.loc['item_1']['total_price_min'] == 300
    assert result.loc['item_1']['comp1_max'] == 50
    assert result.loc['item_2']['comp2_min'] == 230


def test_add_total_deviations(simple_offers_df):
    result = ProjectAnalyzer._get_min_max_prices(simple_offers_df, ['item'], ['comp2'])
    result = ProjectAnalyzer._add_total_deviations(result, ['comp2'])
    assert 'comp2_deviation' in result.columns
    result = result.set_index('item')
    assert result.loc['item_1']['comp2_deviation'] == np.round((110-93)/93,2)


def test_add_best_prices_dict(simple_offers_df):
    simple_offers_df = simple_offers_df.rename(columns={'total_price': UKEYS.TOTAL_PRICE})
    min_max_df = ProjectAnalyzer._get_min_max_prices(simple_offers_df, ['item'], [UKEYS.TOTAL_PRICE, 'comp1', 'comp2'])
    item_df = pd.DataFrame(data=simple_offers_df['item'].unique(), columns=['item'])
    item_df = ProjectAnalyzer._add_best_prices_dict(item_df, simple_offers_df, 'item', ['comp1', UKEYS.TOTAL_PRICE])
    assert 'best_prices' in item_df.columns
    item_df = item_df.set_index('item')
    assert item_df.loc['item_2']['best_prices'][UKEYS.TOTAL_PRICE] == 400
    assert item_df.loc['item_2']['best_prices']['comp1'] == 30


def test_add_overall_deviations_dict(simple_offers_df):
    min_max_df = ProjectAnalyzer._get_min_max_prices(simple_offers_df, ['item','category'], ['comp2'])
    deviations_df = ProjectAnalyzer._add_total_deviations(min_max_df, ['comp2'])
    item_df = pd.DataFrame(data=simple_offers_df[['item','category']].drop_duplicates(), columns=['item','category'])
    item_df = ProjectAnalyzer._add_deviations_dict(item_df, deviations_df, ['item','category'], ['comp2'])
    assert 'deviations' in item_df.columns
    item_df = item_df.set_index(['item', 'category'])
    assert item_df.loc['item_1', 'type1']['deviations']['comp2'] == np.round((110-93)/93,2)


def test_add_historic_prices():
    item_df = pd.DataFrame(data=['item 1', 'item 2', 'item 3'], columns=['item'])
    historic_df = item_df
    historic_prices_df = pd.DataFrame(data={'item': ['item 1', 'item 2', 'item 3'],
                                            'supplier': ['sup 1', 'sup 2', 'sup 1'],
                                            'price': [40, 34, 50]})
    result = ProjectAnalyzer._add_baseline_dict(item_df, historic_prices_df, 'item', ['price'], [])
    assert 'historic_suppliers' in result.columns
    result = result.set_index('item')

    assert result.loc['item 1']['historic_suppliers']['prices']['price'] == 40
    assert result.loc['item 2']['historic_suppliers']['prices']['price'] == 34
    assert result.loc['item 3']['historic_suppliers']['prices']['price'] == 50


@pytest.mark.skip(reason='test data format has to be reviewed regarding rep. of the total spend')
def test_price_heatmap(analyzer, tables, project_settings):
    result = analyzer.price_heatmap(tables['offers'], tables['baseline'], tables['demands'], project_settings)
    first_item = result[0]
    assert 'item_id' in first_item and first_item['item_id'] is not None
    assert 'best_prices' in first_item and first_item['best_prices'] is not None
    assert 'deviations' in first_item and first_item['deviations'] is not None
    assert 'historic_prices' in first_item and first_item['historic_prices'] is not None
    assert 'volumes' in first_item and first_item['volumes'] is not None

