"""
Testing suite for the pivot table calculator

"""
import numpy as np
import pandas as pd
import pytest
import simplejson

from pathlib import Path

from shared.constants import KEYS, UKEYS
from aggrevator.src.v1.routes.pivot_table_service import calculate_pivot_table
from test.utils import *


CALCS = ['max', 'mean', 'min', 'sum', 'wavg']

def test_simple_pivot_table(tables):
    # prepare inputs
    offers_table = tables['offers']
    demands_table = tables['demands']
    result_columns = offers_table.header[KEYS.PRICE]
    group_by = [offers_table.header[UKEYS.SUPPLIER]]

    # calucluate pivot table
    grouped = calculate_pivot_table(offers_table, demands_table, group_by, result_columns)

    # check for correct number of first level columns
    assert len(grouped.columns.levels[0]) == len(group_by) + len(CALCS) 
    # check that all calculation types are covered
    assert len(CALCS) == len(list(set(CALCS) & set(grouped.columns.levels[0])))
    # check for correct result columns
    assert len(result_columns) == len(list(set(result_columns) & set(grouped.columns.levels[1])))


def test_multi_col_pivot_table(tables):
    # prepare inputs
    offers_table = tables['offers']
    demands_table = tables['demands']

    offers_header = offers_table.header
    result_columns = offers_table.header[KEYS.PRICE]
    group_by = [offers_table.header[UKEYS.SUPPLIER], offers_table.header.byname['CountryDestination'].id]
    
    # calucluate pivot table
    grouped = calculate_pivot_table(offers_table, demands_table, group_by, result_columns)

    # check for correct number of first level columns
    assert len(grouped.columns.levels[0]) == len(group_by) + len(CALCS) 
    # check that all calculation types are covered
    assert len(CALCS) == len(list(set(CALCS) & set(grouped.columns.levels[0])))
    # check for correct result columns
    assert len(result_columns) == len(list(set(result_columns) & set(grouped.columns.levels[1])))
