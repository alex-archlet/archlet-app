import pytest

from shared.utilities.price_component_utils import price_components_structure

@pytest.fixture
def price_comps_def():
    return [{"name": "Total Price", "parent": "ROOT"},
            {"name": "price comp1", "parent": "Total Price"},
            {"name": "price comp2", "parent": "price_comp 3"},
            {"name": "price_comp 3", "parent": "Total Price"},
            {"name": "grand child 1", "parent": "price comp2"},
            {"name": "grand child 2", "parent": "price comp2"},
            {"name": "price comp 5", "parent": "price_comp 3"}]


def test_price_components_structure(price_comps_def):
    result = price_components_structure(price_comps_def)
    assert len(result) == 1
    assert result[0]['name'] == 'Total Price'
    assert len(result[0]['children']) == 2


def test_price_components_structure_list():
    price_comps = ['price1', 'price2', 'price3']
    result = price_components_structure(price_comps)
    assert len(result) == 1
    assert result[0]['name'] == 'Total Price'
    assert len(result[0]['children']) == 3
