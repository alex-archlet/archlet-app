""" Module that contains helper functions for the tests """
import pytest
import pandas as pd

from pathlib import Path

from shared.communication import IOFacade
from shared.communication import DBFile
from shared.constants import KEYS, UKEYS
from shared.entities import TableData
from shared.entities import TableHeader
from shared.entities import Translator

from engine.src.table_processors import Preprocessor

def compare_iterables(expected, result):
    for entry in expected:
        assert entry in result

def add_header(table):
    header = TableHeader()
    renamer = {}
    for col in table.data.keys():
        header.add_column(col, col, col)
        renamer[col] = header[col]
    # table.set_data(table.data.rename(columns=renamer))
    table.header = header


# Aggregator utils
@pytest.fixture
def root_folder():
    """
    Returns the root folder for the data
    """
    return Path('./resources/chemicals')


@pytest.fixture
def translator(root_folder):
    fname = "archlet_rawmat_translator_v1.json"
    return Translator(fname=fname, fdir=root_folder)


# Small test data
@pytest.fixture
def simple_offers_td():
    datafr = pd.DataFrame(data={UKEYS.ITEM_ID:['item_1','item_1', 'item_1', 'item_2', 'item_2'],
                                'supplier':['supA', 'supC','supB','supA','supB'],
                                'category':['type1','type2','type1','type2','type2'],
                                UKEYS.RAW_PRICE:[600, 500, 300, 400, 500],
                                KEYS.PRICE:[600, 500, 300, 400, 500],
                                'price_usd':[600, 500, 300, 400, 500],
                                UKEYS.NORMALIZED_PRICE:[600, 500, 300, 400, 500],
                                UKEYS.SUPPLIER_COUNTRY: ['Ukraine', 'Belarus', 'Poland', 'Ukraine', 'Belarus'],
                                UKEYS.CLIENT_COUNTRY: ['USA', 'USA', 'USA', 'Germany', 'Germany'],
                                KEYS.CURRENCY: ['USD', 'EUR', 'USD', 'USD', 'EUR'],
                                UKEYS.PAYMENT_DURATION: [90, 110, 75, 90, 75]
                                })

    table = TableData()
    table.data = datafr
    table.trim()
    add_header(table)

    return table


@pytest.fixture
def simple_demands_td():
    datafr = pd.DataFrame(data={UKEYS.DEMAND_ID: ['item_1', 'item_2'],
                                UKEYS.ITEM_ID: ['item_1', 'item_2'],
                                UKEYS.SUPPLIER: ['supB', 'supA'],
                                KEYS.PRICE: [550, 389],
                                KEYS.VOLUME: [100, 200],
                                KEYS.REGION: ['GE', 'CH']})

    table = TableData()
    table.data = datafr
    table.trim()
    add_header(table)
    return table


# Full data set
@pytest.fixture
def tables(root_folder, translator):
    """
    Returns an offers table
    """
    iofacade = IOFacade(0, './')
    file_path = './resources/chemicals'
    name = 'Offers'
    version = 1
    extension = '.xlsx'
    file = DBFile(file_path, name, version, extension)
    offers = TableData()
    iofacade.load_table(offers, file)
    name = 'Demands'
    version = 1
    extension = '.xlsx'
    file = DBFile(file_path,name,version,extension)
    demands = TableData()
    iofacade.load_table(demands, file)
    name = 'Discounts'
    version = 2
    extension = '.xlsx'
    file = DBFile(file_path,name,version,extension)
    discounts = TableData()
    iofacade.load_table(discounts, file)

    preprocessor = Preprocessor()

    tables = {'offers': offers,
              'demands': demands,
              'discounts': discounts}

    tables = preprocessor.translate_tables(translator,
                                           tables)
    print(demands.header.types)
    tables['baseline'], tables['demands'] = preprocessor.extract_baseline(tables['demands'], tables['offers'].header)

    # add demand_id to offers
    offers_header = tables['offers'].header
    demands_header = tables['demands'].header
    tables['offers'].data.set_index(offers_header[UKEYS.ITEM_ID], inplace=True)
    tables['demands'].data.set_index(demands_header[UKEYS.ITEM_ID], inplace=True)
    
    offers[offers_header[UKEYS.DEMAND_ID]] = demands[demands_header[UKEYS.DEMAND_ID]]

    tables['offers'].data.reset_index(inplace=True)
    tables['demands'].data.reset_index(inplace=True)

    return tables
