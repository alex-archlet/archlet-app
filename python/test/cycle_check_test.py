import pytest
from shared.utilities.cycle_check import contains_cycle
from shared.utilities.cycle_check import dfs_cycle
from shared.utilities.cycle_check import dfs_reverse_topo_sort
from shared.utilities.cycle_check import reverse_topo_sort


@pytest.mark.parametrize("graph,start_node,output",
                         [({'a': ['b', 'c']}, 'a', ['a']),
                          ({'a': ['a']}, 'a', ['a', 'a']),
                          ({'a': ['b', 'c', 'd'], 'b': ['a']}, 'a', ['a', 'b', 'a']),
                          ({'a': ['b', 'c'], 'b': ['c', 'd', 'e', 'f'], 'd': ['g', 'h'], 'f': ['g', 'k']}, 'a', ['d', 'f', 'b', 'a']),
                          ({'d': ['g', 'h'], 'f': ['g', 'k'], 'a': ['d', 'f']}, 'd', ['d']),
                          ({'d': ['g', 'h'], 'f': ['g', 'k'], 'a': ['d', 'f'], 'h': ['q', 'r'], 'q': ['a']}, 'd', ['d', 'a', 'q', 'h', 'd']),
                          ({'q': ['a'], 'b': ['b']}, 'q', ['q']),
                          ({'q': ['a'], 'b': ['b']}, 'b', ['b', 'b'])])
def test_dfs_reverse_topo_sort(graph, start_node, output):
    ordering = []
    result = dfs_reverse_topo_sort(start_node, None, graph, {}, set(), ordering)
    if result:
        assert result == output
    else:
        assert ordering == output

@pytest.mark.parametrize("graph,output",
                         [({'a': ['b', 'c']}, ['a']),
                          ({'a': ['a']}, ['a', 'a']),
                          ({'a': ['b', 'c', 'd'], 'b': ['a']}, ['a', 'b', 'a']),
                          ({'a': ['b', 'c'], 'b': ['c', 'd', 'e', 'f'], 'd': ['g', 'h'], 'f': ['g', 'k']}, ['d', 'f', 'b', 'a']),
                          ({'d': ['g', 'h'], 'f': ['g', 'k'], 'a': ['d', 'f']}, ['d', 'f', 'a']),
                          ({'d': ['g', 'h'], 'f': ['g', 'k'], 'a': ['d', 'f'], 'h': ['q', 'r'], 'q': ['a']}, ['d', 'a', 'q', 'h', 'd']),
                          ({'q': ['a'], 'b': ['b']}, ['b', 'b'])])
def test_reverse_topo_sort(graph, output):
    result = reverse_topo_sort(graph)
    assert result == output



@pytest.mark.parametrize("graph,start_node,has_cycle",
                         [({'a': ['b', 'c']}, 'a', False),
                          ({'a': ['a']}, 'a', True),
                          ({'a': ['b', 'c', 'd'], 'b': ['a']}, 'a', True),
                          ({'a': ['b', 'c'], 'b': ['c', 'd', 'e', 'f'], 'd': ['g', 'h'], 'f': ['g', 'k']}, 'a', False),
                          ({'d': ['g', 'h'], 'f': ['g', 'k'], 'a': ['d', 'f']}, 'd', False),
                          ({'d': ['g', 'h'], 'f': ['g', 'k'], 'a': ['d', 'f'], 'h': ['q', 'r'], 'q': ['a']}, 'd', True),
                          ({'q': ['a'], 'b': ['b']}, 'q', False),
                          ({'q': ['a'], 'b': ['b']}, 'b', True)])
def test_dfs_cycle(graph, start_node, has_cycle):
    result = dfs_cycle(start_node, None, graph, {}, set())
    if has_cycle:
        assert result is not False
        assert result[0] == result[-1]
        current = result[-1]
        for node in result[-2::-1]:
            assert node in graph[current]
            current = node
    else:
        assert result == has_cycle


@pytest.mark.parametrize("graph,has_cycle",
                         [({'a': ['b', 'c']}, False),
                          ({'a': ['a']}, True),
                          ({'a': ['b', 'c', 'd'], 'b': ['a']}, True),
                          ({'a': ['b', 'c'], 'b': ['c', 'd', 'e', 'f'], 'd': ['g', 'h'], 'f': ['g', 'k']}, False),
                          ({'d': ['g', 'h'], 'f': ['g', 'k'], 'a': ['d', 'f']}, False),
                          ({'d': ['g', 'h'], 'f': ['g', 'k'], 'a': ['d', 'f'], 'h': ['q', 'r'], 'q': ['a']}, True),
                          ({'q': ['a'], 'b': ['b']}, True)])
def test_contains_cycle(graph, has_cycle):
    result = contains_cycle(graph)
    if has_cycle:
        assert result is not False
        assert result[0] == result[-1]
        current = result[-1]
        for node in result[-2::-1]:
            assert node in graph[current]
            current = node
    else:
        assert result == has_cycle
