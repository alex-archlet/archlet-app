class Singleton(type):
    """
    Metaclass Singleton

    _instances: metaclass attribute, which maps a Class to a unique instance of the Singleton metaclass
    __call__:   metaclass instance method, which makes instances of the Singleton metaclass callable
                (i.e. each time Class() is called, the call is converted to Singleton.__call__(Class))


    Sample usage:

        class Class(metaclass=Singleton):
            pass

        instance1 = Class()                     :  First call, creates a new instance
        instance2 = Class()                     :  Second call, returns the created instance
        assert instance1 is instance2 == True   :  This passes as they are the same unique instance
    
    """

    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in Singleton._instances:
            Singleton._instances[cls] = super().__call__(*args, **kwargs)
        return Singleton._instances[cls]

