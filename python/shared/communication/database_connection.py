"""
Database connection
"""
from __future__ import annotations
import itertools

import time

import csv

import hashlib
import inflect
import pandas as pd
import re
import simplejson as json
import traceback
import uuid

from datetime import datetime
from functools import reduce
from functools import wraps
from io import StringIO
from pathlib import Path
from sqlalchemy import create_engine
from sqlalchemy.orm import Session
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.sql.expression import bindparam
from sqlalchemy.sql.expression import text

from typing import Any
from typing import Callable
from typing import Dict
from typing import List
from typing import TYPE_CHECKING
from typing import Union
from collections.abc import Iterable

from shared.communication.database_classes import AllocationsDB
from shared.communication.database_classes import Base
from shared.communication.database_classes import BidColumnsDB
from shared.communication.database_classes import BiddingRoundsDB
from shared.communication.database_classes import BranchesDB
from shared.communication.database_classes import ColumnTypesDB
from shared.communication.database_classes import DemandsDB
from shared.communication.database_classes import DiscountsDB
from shared.communication.database_classes import JobInfoDB
from shared.communication.database_classes import OffersDB
from shared.communication.database_classes import OptimizationFilesDB
from shared.communication.database_classes import ProjectKpiDefsDB
from shared.communication.database_classes import ProjectsDB
from shared.communication.database_classes import ScenariosDB
from shared.communication.database_classes import ScenarioKpisDB
from shared.communication.database_classes import TranslatorsDB
from shared.constants import UKEYS, KEYS
from shared.constants import RecordedError
from shared.communication import SingletonLogger

if TYPE_CHECKING:
    from shared.entities import TableData
    from shared.entities import Column

DatabaseClass = Union[AllocationsDB,
                      BidColumnsDB,
                      BiddingRoundsDB,
                      BranchesDB,
                      ColumnTypesDB,
                      DemandsDB,
                      JobInfoDB,
                      OffersDB,
                      OptimizationFilesDB,
                      ProjectsDB,
                      ScenariosDB,
                      TranslatorsDB]


slogger = SingletonLogger()

def peek(iterable: Iterable) -> Union[None, Iterable]:
    """
    Function used to check if a generator is empty or not :
    If it is empty None is returned, otherwise the generator is returned
    """
    try:
        first = next(iterable)
    except StopIteration:
        return None
    return itertools.chain([first], iterable)

##############################################################################
# file helper class
###############################################################################
class DBFile:
    def __init__(self,
                 file_path: str,
                 name: str,
                 file_version: int,
                 file_extension: str,
                 sheet_name: str = None,
                 row_number: int = None,
                 round_id: str = None,
                 description:str = None,
                 matchings: dict = None,
                 id: str = None) -> None:
        self.file_path = file_path
        self.name = name
        self.file_version = file_version
        self.file_extension = file_extension
        self.sheet_name = sheet_name
        self.row_number = row_number
        self.round_id = round_id
        self.description = description
        # Fetch only the actual matching information from the matchings field
        self.matchings = matchings['matchings'] if matchings is not None else None
        self.id = id

    def __str__(self) -> str:
        return f"The db file object with file_path={self.file_path}, name={self.name}, file_version={self.file_version}, file_extension={self.file_extension}, sheet_name={self.sheet_name}, row_number={self.row_number}, round_id={self.round_id}, description={self.description}, matchings={self.matchings}, id={self.id}"

###############################################################################
# Database connection class
###############################################################################
class DatabaseConnection:
    """
    Wrapper class to handle all read/write calls to the PostgreSQL database
    """
    def __init__(self, user, password, host, port, dbname):

        # Connect to database
        self.url = f"postgresql+psycopg2://{user}:{password}@{host}:{port}/{dbname}"

        self.engine = create_engine(self.url,
                                    json_serializer=lambda dict: json.dumps(dict, ignore_nan=True),
                                    pool_size=2)

        # Bind the engine to the mapping defined in Base
        Base.metadata.bind = self.engine

        # Create sessions
        self.rw_session = Session(self.engine) # Read/write session

        self.buffer = []
        # TODO: where do we set the buffer size?
        self.buffer_size = 1000

    ########################################################
    # ------------------------- UTILS -------------------- #
    ########################################################

    def commit_or_rollback(func: Callable[..., Any]) -> Callable[..., Any]: # pylint: disable=no-self-argument
        """
        Wrapper to catch exceptions encountered during commit.
        A rollback is issued to make sure database state is the same as before
        The exception occurred.
        """
        @wraps(func)
        def new_func(self, *args, **kwargs):
            result = func(self, *args, **kwargs)
            try:
                self.rw_session.commit()
                return result
            except Exception as e:
                self.rw_session.rollback()
                slogger.error(f"DB error: {str(e)}")
                raise RuntimeError("error occurred during db write")
        return new_func


    def is_buffer_full(self) -> bool:
        """
        Return whether the amount of elements in the buffer has reached the buffer length.
        """
        return len(self.buffer) >= self.buffer_size


    def append_to_buffer(self, element: DatabaseClass) -> None:
        """
        Append the given element to the buffer.
        """
        self.buffer.append(element)

    @commit_or_rollback
    def flush_buffer(self) -> None:
        """
        Add all the elements in the buffer to the database and empty the buffer
        """
        if len(self.buffer) == 0:
            return
        self.rw_session.bulk_save_objects(self.buffer)
        self.buffer = []

    ##################################################################################
    # ------------------------- GENERIC LOADING/STORING METHODS -------------------- #
    ##################################################################################

    @commit_or_rollback
    def update_one_or_none(self, dbclass, query_param: dict, attributes: dict):
        """
        Fetch a record from the database according to the given query parameters and
        check that there is only one existing record
        Update this record with the new attributes
        """
        obj = self.rw_session.query(dbclass).filter_by(**query_param).one_or_none()
        if obj is None:
            slogger.error(f'{dbclass.__name__} not found in database for query parameters, or there were multiple matches :\n{query_param}')
            raise RecordedError

        for attribute, value in attributes.items():
            if not hasattr(obj, attribute):
                slogger.error(f'Column {attribute} doesn\'t exist in table {dbclass.__name__}')
                raise RecordedError
            else:
                setattr(obj, attribute, value)

    def fetch_one_or_none(self, dbclass, query_param: dict):
        """
        Fetch a record from the database according to the given query parameters and
        check that there is only one existing record
        """
        obj = self.rw_session.query(dbclass).filter_by(**query_param).one_or_none()
        if obj is None:
            slogger.warning(f'{dbclass.__name__} not found in database for query parameters, or there were multiple matches :\n{query_param}')
            raise RecordedError
        else:
            self.rw_session.expire(obj)
            return obj


    def fetch_all(self, dbclass, query_param: dict):
        """
        Fetch all records from the given db table matching the query
        """
        result = self.rw_session.query(dbclass).filter_by(**query_param).all()
        if len(result) == 0:
            slogger.warning(f'{dbclass.__name__} not found in database for query parameters: \n{query_param}')
            raise RecordedError
        else:
            for obj in result:
                self.rw_session.expire(obj)
            return result


    @commit_or_rollback
    def delete_rows(self, dbclass, query_param: dict) -> None:
        """
        Delete all rows from the given table that match the query parameters
        """
        count = self.rw_session.query(dbclass).filter_by(**query_param).delete(synchronize_session=False)
        return count

    def fetch_job_files(self, file_def: dict, branch_id: str) -> dict:
        """
        Fetch all the files that are stored in this jobs input
        """
        files = {}
        for fname, file_id in file_def.items():
            if isinstance(file_id, str):
                fetched_file = self.fetch_one_or_none(OptimizationFilesDB,
                                                      {'id': file_id, 'branch_id': branch_id})
                files[fname] = DBFile(file_path=fetched_file.file_path,
                                      name=fetched_file.name,
                                      file_version=fetched_file.file_version,
                                      file_extension=fetched_file.file_extension,
                                      sheet_name=fetched_file.sheet_name,
                                      row_number=fetched_file.row_number,
                                      round_id=fetched_file.round_id,
                                      description=fetched_file.description,
                                      id=fetched_file.id)
            else:
                slogger.error(f"Invalid type for storing file {type(file_id)}")
                raise RecordedError

        return files

    def fetch_project_jobs_by_id(self, project_id: str, job_type_id: int) -> List[JobInfoDB]:
        """
        Fetch all jobs for this project that match the job type
        """
        return self.rw_session.query(JobInfoDB).filter_by(project_id=project_id, type_id=job_type_id).all()


    def fetch_project_kpi_defs(self, project_id: str = None) -> List[ProjectKpiDefsDB]:
        """
        Get the kpi definitions associated with the given project id.
        If the project is is None, return the default kpis.
        """
        if project_id is None:
            result = self.rw_session.query(ProjectKpiDefsDB).filter_by(is_default=True).all()

            if result is None or len(result) == 0:
                slogger.error(f"No default kpis where found")
                raise RecordedError
        else:
            result = self.rw_session.query(ProjectKpiDefsDB).filter_by(project_id=project_id).all()

            if result is None or len(result) == 0:
                slogger.debug(f"No custom kpis where found")

        return result

    def fetch_files(self, branch_id: str, **kwargs) -> OptimizationFilesDB:
        """
        Given a branch id and an optional set of filter conditions,
        fetch the relevant entries from the database.
        """
        condition_string = ','.join([f"{cond} is {value}" for cond, value in kwargs.items()])

        slogger.debug("Fetching entry in database for file with conditions: " + condition_string)

        if kwargs is None:
            kwargs = {}

        kwargs['branch_id'] = branch_id
        for cond, value in kwargs.items():
            if not hasattr(OptimizationFilesDB, cond):
                slogger.error(f"The attribute {cond} does not exist in the optimization files table")
                raise RecordedError
        query = self.rw_session.query(OptimizationFilesDB).filter_by(**kwargs)

        result = query.all()

        if len(result)==0:
            slogger.error("No match in database for file with conditions " + condition_string)
            raise RecordedError

        return result

    def update_allocations_table(self, round_id: str, scenario_id: str, allocations: TableData) -> None:
        """
        Update the allocations table from a given scenario when it's rerun
        """
        rows = self.rw_session.query(AllocationsDB).filter_by(scenario_id=scenario_id)

        for row in rows:
            table_row = allocations[allocations[UKEYS.OFFER_ID] == row.offer_id]
            slogger.verify(table_row.shape[0] == 1, \
                     f'More than on allocation was found for scenario {scenario_id} and offer {row.offer_id}')
            self.update_allocation_row(row, table_row)


    @commit_or_rollback
    def update_allocation_row(self, db_row: AllocationsDB, table_row: pd.DataFrame) -> None:
        """
        Update a given row of the allocations table
        """
        for colname in table_row.columns:
            if hasattr(db_row, colname):
                setattr(db_row, colname, table_row[colname])
            else:
                slogger.warning(f'Column {colname} present in the allocations table but not in the database')

    def fetch_column_types(self) -> ColumnTypesDB:
        """
        Fetch all column types

        Returns:
            List of column types
        """
        slogger.debug(f"Fetching all column types in database")

        column_types = pd.read_sql_query(f"SELECT * FROM column_types ",
                                         self.engine,
                                         parse_dates=['created_at, updated_at'])

        if column_types is None:
            slogger.warning(f"No column types in database")
            raise RecordedError

        return column_types

    def fetch_round_id(self,  **kwargs) -> str:
        """
        Fetch the latest round_id associated to this project.

        Args:
            **kwargs: can only include:
                project_id: find a round id associated to this project id
                status_id: find a round with the specified status

        Returns:
            uuid of the latest round associated with this project or
            False if none exist
        """
        slogger.debug(f"Fetching bidding rounds associated with project id {kwargs['project_id']}")
        bidding_round = self.rw_session.query(BiddingRoundsDB)\
                                       .filter_by(**kwargs)\
                                       .order_by(BiddingRoundsDB.start_at.desc())\
                                       .first()

        if bidding_round is None:
            return False

        return str(bidding_round.id)


    def fetch_all_column_types(self) -> List[ColumnTypesDB]:
        """
        Fetch all the column types
        """
        slogger.debug("Fetching the column types in the database.")
        result = self.rw_session.query(ColumnTypesDB).all()
        if result is None or len(result) == 0:
            slogger.warning("No bid column types were found")

        for row in result:
            self.rw_session.expire(row)

        return result

    def fetch_bid_columns(self, project_id: str, **kwargs) -> BidColumnsDB:
        """
        Fetch all the bidding round columns associated with the given
        bidding round id.

        Args:
            project_id: the project id to fetch

        Returns:
            List of bidding columns

        Throws:
            RecordedError when no bid column is present for the given
            bidding round id.
        """
        slogger.debug(f"Fetching bid columns in database for project id {project_id}")

        kwargs['project_id'] = project_id
        result = self.rw_session.query(BidColumnsDB).filter_by(**kwargs).all()

        if result is None or len(result) == 0:
            slogger.warning(f"No bid columns in database for project id {project_id}")
            raise RecordedError

        for row in result:
            self.rw_session.expire(row)

        return result


    # TO DO: provide default values for all the nullable columns
    def store_bid_column(self,
                         col: Column,
                         input_by: str,
                         column_type_to_id: Dict[str, str],
                         project_id: str) -> None:
        """
        Store a bid column in the database.

        Args:
            column_id: the id of the column in the database (a uuid)
            name: the name of the column
            input_by: 'supplier' or 'buyer'
            **kwargs: can only include:
                description,
                calculation,
                is_default,
                is_mandatory,
                is_visible,
                project_id,
                type_id,
                order,
                limit_min,
                limit_max,
                single_choice
        """
        timestamp = datetime.now()
        column = BidColumnsDB(id=col.id,
                              name=col.original_name,
                              input_by=input_by,
                              type_id=column_type_to_id[col.type],
                              calculation=col.calculation,
                              project_id=project_id,
                              created_at=timestamp,
                              updated_at=timestamp)

        matching_result = col.matching_result
        if matching_result is not None:
            column.input_by = matching_result._input_by
            column.initial_type_id = column_type_to_id[matching_result.initial_type]
            column.matching_confidence = matching_result.score
            column.initial_matching_confidence = matching_result.score
            column.order = matching_result.order

        self.append_to_buffer(column)
        if self.is_buffer_full():
            self.flush_buffer()

    def latest_file_version(self, filename: str, extension: str, directory: str, branch_id: str):
        """
        Given a file object, return the latest version number of this file in
        the database
        TODO: what happens when a file actually does not exist?
        """
        slogger.debug(f'Looking for latest file version for {extension} file with name {filename} in {directory}')

        files = self.rw_session.query(OptimizationFilesDB).filter_by(name=str(filename),
                                                                     file_extension=str(extension),
                                                                     file_path=str(directory),
                                                                     branch_id=branch_id).all()

        if not files:
            return 0

        for file in files:
            self.rw_session.expire(file)
        return max([f.file_version for f in files])

    @commit_or_rollback
    def add_new_file_entry(self,
                           name: str,
                           directory: str,
                           extension: str,
                           description: str,
                           filetype: str,
                           version: int,
                           abs_file_path: Path,
                           branch_id: str,
                           project_id: str = None,
                           scenario_id: str = None,
                           round_id: str = None) -> str:
        """
        Add new file to files table

        Args:
            name
            directory
            extension
            description
            filetype
            version
            abs_file_path
            branch_id
            project_id
            scenario_id

        Returns:
            the id of the stored file entry
        """

        # Check the file actually exists
        if not abs_file_path.exists():
            slogger.error(f"File {abs_file_path} does not exist, cannot " \
                "create database entry")

        # Compute MD5 sum reading file in increments of 4096 bytes
        hash_md5 = hashlib.md5()
        with open(abs_file_path, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        content_hash = hash_md5.hexdigest()

        # Current timestamp
        current_timestamp = datetime.now()

        file_id = uuid.uuid4().hex

        # Create new file
        rw_file = OptimizationFilesDB(id=file_id,
                                      name=name,
                                      content_hash=content_hash,
                                      description=description,
                                      file_type=filetype,
                                      file_extension=extension,
                                      file_path=directory,
                                      file_version=version,
                                      created_at=current_timestamp,
                                      updated_at=current_timestamp,
                                      project_id=project_id,
                                      scenario_id=scenario_id,
                                      branch_id=branch_id,
                                      round_id=round_id)

        # Add into database
        self.rw_session.add(rw_file)

        return file_id

    @commit_or_rollback
    def delete_by_id(self, dbclass, id_list: List[str]):
        """
        Delete the entries from dbclass which have an id from the given list
        """
        count = self.rw_session.query(dbclass).filter(dbclass.id.in_(id_list)).delete(synchronize_session=False)
        return count


    def fetch_table(self, table_name: str, round_id: str, **kwargs):
        """
        Fetch all the entries from table_name with the specified round id
        """
        query_string = f"SELECT * FROM {table_name} WHERE round_id = '{round_id}'"
        for key, value in kwargs.items():
            query_string = f"{query_string} AND {key} = '{value}'"
        slogger.debug(f"Querying: {query_string}")
        result = pd.read_sql_query(query_string,
                                   self.engine,
                                   parse_dates=['created_at, updated_at'], chunksize=1000)
        result = peek(result)
        if result is None:
            raise RecordedError(f'No {table_name} were found for round_id {round_id}')
        else:
            final_result = reduce(lambda x, y: x.append(y, ignore_index=True), result)
            return final_result

    def store_offers_table(self, table: TableData, round_id: str, is_processed: bool, copy: bool = False) -> None:
        """
        Alternative method for storing offers using PostgreSQL copy.
        """
        header = table.header
        database_columns = ['price_total', 'demand_id', 'supplier', 'total_var_price', 'total_volume', 'total_fixed_price']
        table_columns = header[[UKEYS.TOTAL_PRICE, UKEYS.DEMAND_ID, UKEYS.SUPPLIER, UKEYS.NORMALIZED_PRICE, UKEYS.TOTAL_VOLUME, UKEYS.TOTAL_FIXED_PRICE]]
        to_store = pd.DataFrame()
        to_store[database_columns] = table[table_columns]
        to_store['is_historic'] = table['is_historic']
        to_store['optimization_file_id'] = table['optimization_file_id']

        timestamp = datetime.now()
        to_store['created_at'] = timestamp
        to_store['updated_at'] = timestamp
        to_store['round_id'] = round_id
        to_store['is_processed'] = is_processed
        to_store['attributes'] = table['attributes'].apply(lambda dic: json.dumps(dic, ignore_nan=True))
        to_store['processing_messages'] = table['processing_messages'].apply(lambda dict: json.dumps(dict, ignore_nan=True))

        # print(to_store['attributes'][0])
        # print('-----------------------------------')
        # print(json.dumps(to_store['attributes'][0], ignore_nan=True))

        # exit()
        if copy:
            # id is necessary to store as well to later do the in place update
            to_store['id'] = table[header[UKEYS.OFFER_ID]]
            self.store_table('offers_copy', to_store)
        else:
            self.store_table('offers', to_store)


    def update_offers_table(self, table: TableData, round_id: str, is_processed: bool) -> None:
        """
        Update the entries in the database of the offers table.
        """
        with self.engine.connect() as conn:
            # offers_copy.create(bind=conn)
            stmt = text("DROP TABLE IF EXISTS offers_copy")
            conn.execute(stmt)
            stmt = text("CREATE TABLE offers_copy as (select * from offers limit 0)")
            conn.execute(stmt)
        try:
            self.store_offers_table(table, round_id, is_processed, copy=True)
            with self.engine.connect() as conn:
                offers_attrs = [attr for attr in OffersDB.__dict__ if attr[0] != '_']
                offers_attrs.remove('id')
                stmt = text(f"""UPDATE offers SET
                                {", ".join([f"{attr} = offers_copy.{attr}" for attr in offers_attrs])}
                                FROM offers_copy
                                WHERE offers.id = offers_copy.id""")

                conn.execute(stmt)
        finally:
            with self.engine.connect() as conn:
                stmt = text("DROP TABLE offers_copy")
                conn.execute(stmt)


    def fetch_allocations_table(self, scenario_id: str) -> pd.DataFrame:
        """
        Load an allocations table for the given scenario id.
        """
        query_string = (f"SELECT alloc.id, alloc.offer_id, alloc.demand_id, alloc.volume, "
                        f"alloc.total_price, offer.supplier, alloc.is_manual "
                        f"from allocations as alloc "
                        f"left join offers as offer on alloc.offer_id = offer.id "
                        f"where alloc.scenario_id = '{scenario_id}'")

        slogger.debug(f"Querying: {query_string}")
        result = pd.read_sql_query(query_string,
                                   self.engine,
                                   chunksize=1000)
        result = peek(result)
        if result is None:
            raise RecordedError(f'No allocations were found for scenario id {scenario_id}')
        else:
            final_result = reduce(lambda x, y: x.append(y, ignore_index=True), result)
            return final_result

    def fetch_extended_allocations_table(self, scenario_id: str) -> pd.DataFrame:
        """
        Load an allocations table with the offers.attributes merged
        """
        query_string = (f"SELECT alloc.id, alloc.offer_id, alloc.demand_id, alloc.volume, "
                        f"alloc.total_price, offer.supplier, alloc.is_manual, offer.attributes "
                        f"from allocations as alloc "
                        f"left join offers as offer on alloc.offer_id = offer.id "
                        f"where alloc.scenario_id = '{scenario_id}'")

        slogger.debug(f"Querying: {query_string}")
        result = pd.read_sql_query(query_string,
                                   self.engine,
                                   chunksize=1000)
        result = peek(result)
        if result is None:
            raise RecordedError(f"No allocations were found for scenario id {scenario_id}")
        else:
            final_result = reduce(lambda x, y: x.append(y, ignore_index=True), result)
            return final_result

    def store_all_allocation_kpis(self, kpi_table: pd.DataFrame, scenario_id: str) -> None:
        """
        Store the kpis for the given scenario id.

        Args:
            - kpi_table: data frame with columns equal to kpi_def_ids and no index
            - scenario_id: the id of the scenario
        """
        kpi_table = kpi_table.set_index('id')
        to_store = kpi_table.stack()
        to_store = to_store.reset_index().rename(columns={'id': 'allocation_id', 'level_1': 'project_kpi_def_id', 0: 'value'})
        to_store['scenario_id'] = scenario_id

        self.store_table('scenario_kpis', to_store)


    def store_allocation_kpi(self, scenario_id, kpi_def_id, value, **kwargs):
        """
        Create a new entry in the scenario kpis table with the given columns.
        """
        timestamp = datetime.now()

        scenario_kpi = ScenarioKpisDB(scenario_id=scenario_id,
                                      project_kpi_def_id=kpi_def_id,
                                      value=value)

        for key, value in kwargs.items():
            if hasattr(scenario_kpi, key):
                setattr(scenario_kpi, key, value)
            else:
                slogger.warning(f"The attribute {key} does not exist in the scenario kpis table")

        self.append_to_buffer(scenario_kpi)
        if self.is_buffer_full():
            self.flush_buffer()

    @commit_or_rollback
    def store_kpi_def(self, name: str, calculation: str, **kwargs):
        """
        Create a new entry in the project kpi definition table with the given columns
        """
        timestamp = datetime.now()

        project_kpi = ProjectKpiDefsDB(name=name,
                                       calculation=calculation,
                                       is_default=False,
                                       description='',
                                       created_at=timestamp,
                                       updated_at=timestamp)

        for key, value in kwargs.items():
            if hasattr(project_kpi, key):
                setattr(project_kpi, key, value)
            else:
                slogger.warning(f"The attribute {key} does not exist in the project kpi definition table")

        self.rw_session.add(project_kpi)

    def store_allocations_table(self, allocations_table: TableData, round_id: str, scenario_id: str) -> None:
        """
        Store the allocations table.
        """
        header = allocations_table.header
        database_columns = ['id', 'offer_id', 'demand_id', 'volume', 'total_price', 'is_manual']
        table_columns = header[[UKEYS.ALLOCATION_ID, UKEYS.OFFER_ID, UKEYS.DEMAND_ID, UKEYS.TOTAL_VOLUME, UKEYS.TOTAL_SPENT]] + ['is_manual']
        to_store = pd.DataFrame()
        to_store[database_columns] = allocations_table[table_columns]
        timestamp = datetime.now()
        to_store['created_at'] = timestamp
        to_store['updated_at'] = timestamp
        to_store['round_id'] = round_id
        to_store['scenario_id'] = scenario_id

        self.store_table('allocations', to_store)

    def store_discounts_table(self, discounts_table, round_id) -> None:
        """
        Store the discounts table
        """
        header = discounts_table.header
        database_columns = ['bucket_id', 'threshold', 'discount_amount', 'discount_type', 'discount_unit', 'supplier', 'volume_name', 'price_component', 'description']
        table_columns = header[[UKEYS.BUCKET_ID, UKEYS.THRESHOLD, UKEYS.DISCOUNT_AMOUNT, UKEYS.DISCOUNT_TYPE, UKEYS.DISCOUNT_UNIT, UKEYS.SUPPLIER, UKEYS.VOLUME_NAME, UKEYS.PRICE_COMPONENT, UKEYS.DESCRIPTION]]
        to_store = pd.DataFrame()
        to_store[database_columns] = discounts_table[table_columns]
        timestamp = datetime.now()
        to_store['created_at'] = timestamp
        to_store['updated_at'] = timestamp
        to_store['round_id'] = round_id

        self.store_table('discounts', to_store)

    def fetch_discount_types(self):
        """
        """
        query_string = f"SELECT * FROM discount_types"
        result = pd.read_sql_query(query_string,
                                   self.engine)
        result = result.set_index('name')['id'].to_dict()
        return result


    def fetch_discount_units(self):
        """
        """
        query_string = f"SELECT * FROM discount_units"
        result = pd.read_sql_query(query_string,
                                   self.engine)
        result = result.set_index('name')['id'].to_dict()
        return result

    @commit_or_rollback
    def store_allocation(self,
                         round_id: str,
                         scenario_id: str,
                         offer_id: str,
                         demand_id: str,
                         volume: float,
                         total_price: str,
                         is_manual: bool,
                         **kwargs) -> None:
        """
        Create a new entry in the allocations table with the given columns.
        """
        timestamp = datetime.now()

        allocation = AllocationsDB(round_id=round_id,
                                   scenario_id=scenario_id,
                                   offer_id=offer_id,
                                   demand_id=demand_id,
                                   volume=volume,
                                   total_price=total_price,
                                   is_manual=is_manual,
                                   created_at=timestamp,
                                   updated_at=timestamp)

        for key, value in kwargs.items():
            if hasattr(allocation, key):
                setattr(allocation, key, value)
            else:
                slogger.warning(f"The attribute {key} does not exist in the allocations table")

        self.rw_session.add(allocation)

    def store_demands_table(self,
                            table: TableData,
                            round_id: str,
                            branch_id: str,
                            copy: bool = False) -> Dict[str, str]:
        """
        Store the complete demands table to the database.
        """
        header = table.header
        database_columns = ['item', 'id', 'volume']
        table_columns = header[[UKEYS.ITEM_ID, UKEYS.DEMAND_ID, UKEYS.TOTAL_VOLUME]]
        to_store = pd.DataFrame()
        to_store[database_columns] = table[table_columns]

        timestamp = datetime.now()
        to_store['created_at'] = timestamp
        to_store['updated_at'] = timestamp
        to_store['round_id'] = round_id
        to_store['branch_id'] = branch_id
        to_store['attributes'] = table['attributes'].apply(lambda dic: json.dumps(dic, ignore_nan=True))

        if copy:
            self.store_table('demands_copy', to_store)
        else:
            self.store_table('demands', to_store)

        # demands_mapping = {}
        # # Add a db entry for each row and put the resulting id in the mapping
        # # TODO: extract the lambda function for more readability
        # table_data.data.apply(lambda row: demands_mapping.update({row[item_column]: self.store_demand(round_id, row[item_column], branch_id, attributes=row['attributes'], id=row[id_column], volume=row[volume_column])}), axis=1)
        # return demands_mapping


    def update_demands_table(self, table: TableData, round_id: str, branch_id: str) -> None:
        """
        Update the given entries in the demands table.
        """
        with self.engine.connect() as conn:
            stmt = text("DROP TABLE IF EXISTS demands_copy")
            conn.execute(stmt)
            stmt = text("CREATE TABLE demands_copy as (select * from demands limit 0)")
            conn.execute(stmt)

        try:
            self.store_demands_table(table, round_id, branch_id, copy=True)

            with self.engine.connect() as conn:
                demands_attrs = [attr for attr in DemandsDB.__dict__ if attr[0] != '_']
                stmt = text(f"""UPDATE demands SET
                            {", ".join([f"{attr} = demands_copy.{attr}" for attr in demands_attrs])}
                            FROM demands_copy
                            WHERE demands.id = demands_copy.id
                            """)
                conn.execute(stmt)
        finally:
            with self.engine.connect() as conn:
                stmt = text("DROP TABLE IF EXISTS demands_copy")
                conn.execute(stmt)



    def store_demand(self, round_id, item, branch_id, **kwargs):
        """
        Create a new entry in the demands table with the given columns.
        """
        timestamp = datetime.now()

        if 'id' in kwargs:
            demand_uuid = kwargs['id']
        else:
            demand_uuid = str(uuid.uuid4())

        demand = DemandsDB(id=demand_uuid,
                           round_id=round_id,
                           item=item,
                           branch_id=branch_id,
                           created_at=timestamp,
                           updated_at=timestamp)

        for key, value in kwargs.items():
            if hasattr(demand, key):
                setattr(demand, key, value)
            else:
                slogger.warning(f"The attribute {key} does not exist in the offers table")

        self.rw_session.add(demand)

        return demand_uuid

    @commit_or_rollback
    def add_bidding_round(self, round_id: str, name: str, branch_id: str, **kwargs) -> None:
        """
        Create a new entry in the bidding rounds table
        """
        timestamp = datetime.now()

        bidding_round = BiddingRoundsDB(id=round_id,
                                        name=name,
                                        branch_id=branch_id,
                                        created_at=timestamp,
                                        updated_at=timestamp)

        for key, value in kwargs.items():
            if hasattr(bidding_round, key):
                setattr(bidding_round, key, value)
            else:
                slogger.warning(f"The attribute {key} does not exist in the bidding rounds table")

        self.rw_session.add(bidding_round)


    @commit_or_rollback
    def update_bidding_round(self, round_id: str, **kwargs) -> None:
        """
        Update the bidding round using the provided keyword arguments
        """
        timestamp = datetime.now()

        bidding_round = self.rw_session.query(BiddingRoundsDB).filter_by(id=round_id).one_or_none()

        if bidding_round is None:
            slogger.error(f"No match in database for bidding_round with id {round_id}")
            raise RecordedError

        for key, value in kwargs.items():
            if hasattr(bidding_round, key):
                setattr(bidding_round, key, value)
            else:
                slogger.warning(f"The attribute {key} does not exist in the bidding rounds table")

        bidding_round.updated_at = timestamp

    def close_sessions(self):
        """
        Close the sessions.
        Note: after issuing this function, none of the read or write
        functionality will work.
        """
        self.rw_session.close()
        self.rw_session.bind.dispose()


    def store_table(self,
                    tablename: str,
                    table: pd.DataFrame) -> None:
        """
        Store the dataframe in the database table
        """
        csv_io = StringIO()

        table.to_csv(csv_io, header=False, index=False, na_rep='\\N', sep='\t', quoting=csv.QUOTE_NONE)

        # output = csv_io.getvalue()
        # output = output.replace('"', '\"')
        # csv_io.seek(0)
        # csv_io.write(output)

        # print(csv_io.getvalue())

        csv_io.seek(0)

        temp_con = self.engine.raw_connection()
        temp_cur = temp_con.cursor()
        try:
            temp_cur.copy_from(csv_io, tablename, sep='\t', null='\\N', columns=list(table.columns))
            temp_con.commit()
        except:
            temp_con.rollback()
            traceback.print_stack()
            traceback.print_exc()
            raise RecordedError(f"Something went wrong when storing {tablename}")
        finally:
            temp_con.close()
