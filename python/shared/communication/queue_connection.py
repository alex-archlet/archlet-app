###############################################################################
# Import dependencies
###############################################################################
import pika
import time
import socket

from shared.communication import SingletonLogger

slogger = SingletonLogger()

###############################################################################
# QueueConnectionClass
###############################################################################
class QueueConnection:
    def __init__(self):
        """
        Constructor for the QueueConnection class
        """

        # Initialize connection and channel to None
        self.channel = None
        self.queue   = None


    def connection_is_open(self, hostname, port):
        """
        Check whether the host is accepting TCP connections on the specified 
        port.

        Return:
            bool: True if host is accepting connections.
        """

        # Create TCP socket
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        try:
          s.connect((hostname, int(port)))
          s.shutdown(2)
          return True
        except:
          return False


    def initialize(self, hostname, port, queue, connection_attempts, connection_intervals):
        """
        Attempt to connect to queue for specified number of times at specified
        frequency. In the end returns whether connection was successful.

        Return:
            bool: True is connection was successful.
        """

        # While waiting for RabbitMQ to start up, check whether it is accepting
        # connections on the specified port
        for attempt_number in range(connection_attempts):

            # Check if RabbitMQ is accepting connections
            if self.connection_is_open(hostname, port) == True:
                slogger.info(f"Queue port is open, attempting to connect")
                break

            # Tried to connect too many times unsuccessfully, abort
            if attempt_number == connection_attempts:
                slogger.error(
                    f"Attempt to connect to RabbitMQ "
                    f"({attempt_number+1}/{connection_attempts}) failed, quitting!")
                return False

            # Connection attempt failed but will try again
            else:
                slogger.warning(
                    f"Attempt to connect to RabbitMQ "
                    f"({attempt_number+1}/{connection_attempts}) failed, retrying...")
                time.sleep(connection_intervals)

        # Once port is open, try to connect
        connection_params = pika.ConnectionParameters(hostname, port)
        connection        = pika.BlockingConnection(connection_params)
        self.channel      = connection.channel()

        # Connect to queue containing job requests
        # NOTE: If the queue does not yet exist it will be created here
        self.queue = self.channel.queue_declare(queue=queue, durable=True)

        return True


    def get_num_messages_in_queue(self):
        """
        Get the number of messages currently in the queue.

        Return:
            int: Number of messages currently in the queue
        """
        return self.queue.method.message_count


    def start_consuming_from_queue(self, callback):
        """
        Start consuming from the specified queue. This is a blocking function.
        Until the connection is closed we consume messages one by one and call
        the callback function everytime a new message is processed.

        Args:
            callback (func): The callback function to be executed when a new
                message is processed.
        """

        # Set callback and configure the channel so we only consume one message
        # at a time from the queue, i.e. we wait until the callback execution
        # is completed before consuming the next message 
        self.channel.basic_consume(queue=self.queue.method.queue, on_message_callback=callback)
        self.channel.basic_qos(prefetch_count=1)

        # Start consuming messages
        slogger.info(f"Starting to consume message from queue...")
        self.channel.start_consuming()
        slogger.info(f"Stopped consuming messages from queue")
