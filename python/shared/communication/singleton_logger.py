"""
Logger

This class collects all messages destined to the user and the execution state
of a task or process.
"""

import os
import sys
import time
import logging
import json
from datetime import datetime

from typing import List, Dict, Union

from shared.constants import SEVERITY
from shared.constants import STATUS_MESSAGE_CATEGORY
from shared.constants import RecordedError
from shared.communication.status_message import StatusMessage
from shared.abstract_entities import Singleton


class SingletonLogger(metaclass=Singleton):
    def __init__(self):

        self._logger = logging.getLogger()
        self._logger.setLevel(logging.NOTSET)
        self.messages = []
        self._logger.handlers = []


    def set_logger_output(self, logfile: str, logdir: str, pipe=sys.stdout) -> None:
        """
        This is the constructor for the logger class. This class is a wraparound
        for the Python 'logging' module

        Args:
            logfile (str): Name of logging file, if not provided log is only output
                to pipe
            logdir (str): Path of directory containing log file
        """
        # Log format

        # Create handlers for file and stream output
        self.messages = []
        self._logger.handlers = []

        fmt       = "%(asctime)s.%(msecs)03d [%(name)s] [%(levelname)s] %(message)s"
        datefmt   = "%Y.%m.%d %H:%M:%S"
        formatter = logging.Formatter(fmt=fmt, datefmt=datefmt)

        # Set log time as UTC
        logging.Formatter.converter = time.gmtime
        # Setup stream handler (i.e. print to command line)
        # NOTE: We set this priority as INFO
        if pipe is not None:
            stream_handler = logging.StreamHandler(stream=pipe)
            stream_handler.setFormatter(formatter)
            stream_handler.setLevel(logging.INFO)
            self._logger.addHandler(stream_handler)

        # Setup file handler (i.e. log to file)
        # NOTE: We set this priority as NOTSET
        if logfile is not None:

            # Make file directory and get file path
            logdir = '.' if logdir is None else logdir
            os.makedirs(logdir, exist_ok=True)
            fpath = os.path.join(logdir, logfile)

            # Setup handler
            # NOTE: "mode='w'" means we clear the file
            file_handler = logging.FileHandler(filename=fpath, mode='w')
            file_handler.setFormatter(formatter)
            file_handler.setLevel(logging.NOTSET)
            self._logger.addHandler(file_handler)

            self.info(f'Logging to file successfully setup in: {fpath}')


    def set_logger_severity(self, logger: logging.Logger, severity: str) -> None:
        """
        Set the severity of a logge to a specific value.

        Args:
            logger (str): Name of the logger whose severity should be set
            severity (constants.SEVERITY): Severity level to use
        """

        # Make sure we use a valid severity level
        if severity not in SEVERITY.values:
            raise ValueError('Invalid log severity: {}'.format(severity))

        # Match to logging package severities
        if severity == SEVERITY.DEBUG:
            severity = logging.DEBUG
        elif severity == SEVERITY.INFO:
            severity = logging.INFO
        elif severity == SEVERITY.WARNING:
            severity = logging.WARNING
        elif severity == SEVERITY.ERROR:
            severity = logging.ERROR
        else:
            raise ValueError(f'Logging severity level {severity} is not implemented')

        # Set severity
        logging.getLogger(logger).setLevel(severity)


    def log(self, severity: str, message: str) -> None:
        """
        Log a message to logger

        Args:
            severity (str): SEVERITY.values severity level
            message (str): Message to log
        """

        # Make sure valid message
        # if not isinstance(message, str):
        #     raise ValueError('Invalid log message type {}'.format(type(message)))

        # Make sure we use a valid severity level
        if severity not in SEVERITY.values:
            raise ValueError(f'Invalid log severity: {severity}')

        if severity == SEVERITY.DEBUG:
            self._logger.debug(message)
        elif severity == SEVERITY.INFO:
            self._logger.info(message)
        elif severity == SEVERITY.WARNING:
            self._logger.warning(message)
        elif severity == SEVERITY.ERROR:
            self._logger.error(message)
        else:
            raise ValueError(f'Logging severity level {severity} is not implemented')


    def debug(self, message: str, **kwargs) -> None:
        """
        Log message with 'debug' severity level

        Args:
            message (str)
        """
        self.log(SEVERITY.DEBUG, message)


    def info(self, message: str, **kwargs) -> None:
        """
        Log message with 'info' severity level
        and record 'info' level status update

        Args:
            message (str)
        """
        self.log(SEVERITY.INFO, message)
        self.add(SEVERITY.INFO, message, **kwargs)


    def warning(self, message: str, **kwargs) -> None:
        """
        Log message with 'warning' severity level
        and record 'warning' level status update

        Args:
            message (str)
        """
        self.log(SEVERITY.WARNING, message)
        self.add(SEVERITY.WARNING, message, **kwargs)


    def error(self, message: str, save_message: bool=False, **kwargs) -> None:
        """
        Log message with 'error' severity level
        and record 'error' level status update

        Args:
            message (str)
        """
        self.log(SEVERITY.ERROR, message)
        if save_message:
            self.add(SEVERITY.ERROR, message, **kwargs)


    def verify(self, expression: bool, message: str, **kwargs) -> None:
        """
        Log message with 'error' severity level
        and record 'error' level status update
        if the expression is not confirmed

        Args:
            expression (bool)
            message (str)
        """
        if not expression:
            self.error(message, **kwargs)


    def add(self, severity: str, message: str, category: str = None, **kwargs) -> None:
        """
        Record status update

        Args:
            severity (str): severity level in SEVERITY.values
            message (str): body of status update
        """

        # Check if valid severity level
        if severity not in SEVERITY.values:
            self.log.error(f'Invalid status severity: {severity}')
            raise ValueError

        # Check if valid message
        if not isinstance(message, str):
            self.log.error(f'Invalid log message type {type(message)}')
            raise ValueError

        # Create status message from input parameters
        if category is None:
            entry = StatusMessage(
                timestamp = datetime.now(),
                severity  = severity,
                category  = STATUS_MESSAGE_CATEGORY.DEFAULT,
                text      = message
            )
        else:
            entry = StatusMessage(
                timestamp = datetime.now(),
                severity  = severity,
                category  = category,
                text      = message,
                **kwargs
            )

        self.messages.append(entry)


    def failed(self) -> bool:
        """
        Check if an error occurred

        Returns:
            bool: True if an error occurred
        """
        def f(message):
            return message.severity
        return SEVERITY.ERROR in map(f, self.messages)


    def success(self) -> bool:
        """
        Cehck if an error occurred

        Returns:
            bool: True if no errors occurred
        """
        return not self.failed()


    def to_string(self) -> List[str]:
        """
        Dump all messages to array of strings
        """
        return [message.to_string() for message in self.messages]


    def to_dict(self) -> Dict[str, List[dict]]:
        """
        Dump all messages to JSON-friendly dictionary
        """
        return {'messages': [message.to_dict() for message in self.messages]}


    def clear(self) -> None:
        """
        Clear status updates.
        """
        self.messages = []
