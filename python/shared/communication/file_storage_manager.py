"""
File storage manager
"""
from __future__ import annotations
from datetime import datetime
from pathlib import Path
from typing import TYPE_CHECKING

import numpy as np
import pandas as pd
import pickle

from shared.communication import DBFile
from shared.communication import SingletonLogger
from shared.constants import UKEYS, KEYS
from shared.constants import RecordedError
from shared.entities import TableHeader

if TYPE_CHECKING:
    from shared.entities import TableData


slogger = SingletonLogger()


###############################################################################
# File storage class
###############################################################################
class FileStorageManager:
    """
    Wrapper class to handle all read/write calls to file storage
    """
    def __init__(self, root: str) -> None:
        self.root = root


    def absolute_path(self, path: str) -> str:
        """
        Get the absolute path of a file or directory given the relative path
        """
        return Path(self.root, path)


    def job_folder(self, job):
        """
        Given a job entry from the database, return the appropriate folder name
        to store data files
        """
        timestamp = job.updated_at.strftime('%Y.%m.%d-%H.%M.%S.%f')
        return Path("jobs", str(timestamp)+"_"+str(job.id))


    def file_name(self, name: str, version: str, extension: str):
        """
        Given an entry name, version and extension, return the appropriate file
        name in the file storage
        """
        return Path(name + "_v" + str(version) + extension)


    def file_path(self, file: DBFile, abs: bool):
        """
        Given a database file object, get the relative path to the file.
        """
        directory = file.file_path
        filename = self.file_name(file.name, file.file_version, file.file_extension)

        if abs == True:
            directory = self.absolute_path(directory)

        return Path(directory, filename)


    def get_processed_files_directory(self, company_id, branch_id, project_id, scenario_id: str = None):
        """ construct the file path for storage in file system """
        path = Path('companies', str(company_id),
                    'branches',  str(branch_id),
                    'projects',  str(project_id))

        # also append the scenario location when a scenario id is provided
        if scenario_id is not None:
            path = path / 'scenarios' / str(scenario_id)
        return path


    def load_table(self, table: TableData, file: OptimizationFilesDB) -> TableData:
        """
        Given a database entry from the optimization files table, load the data
        from the specified file
        """
        table.file = file

        # Check that the file didn't go missing
        abspath = self.file_path(file, abs=True)
        if not abspath.exists():
            slogger.error(f"Could not find file: {abspath}")
            raise RecordedError

        # Load data from file
        if file.file_extension == ".pkl":
            table.data = pd.read_pickle(abspath)
            table.trim()
        elif file.file_extension == ".xlsx":
            kwargs = {}
            if file.sheet_name is not  None and file.sheet_name != 'false':
                xl = pd.ExcelFile(abspath)
                if file.sheet_name in xl.sheet_names:
                    kwargs['sheet_name'] = file.sheet_name
            if file.row_number is not None:
                kwargs['skiprows'] = file.row_number - 1
            table.data = pd.read_excel(abspath, **kwargs)
            table.trim()
        elif file.file_extension == ".csv":
            table.data = pd.read_csv(abspath)
            table.trim()
        else:
            # slogger.error(f"Unsupported extension to load file from: {file.file_extension}")
            slogger.error(f"the file that went wrong: {str(file)}")
            raise RecordedError

        # TODO: make explicit?
        # Construct a table header:
        header = TableHeader()
        for col in table.data.keys():
            header.add_column(col, original_name=col, attribute=True)
        table.header = header

        return table


    def load_source_row(self, file: DBFile):
        # Check that the file didn't go missing
        abspath = self.file_path(file, abs=True)
        if not abspath.exists():
            slogger.error(f"Could not find file: {abspath}", save_message=True)
            raise RecordedError

        if file.file_extension == ".xlsx":
            kwargs = {}
            if file.sheet_name is not  None:
                xl = pd.ExcelFile(abspath)
                if file.sheet_name in xl.sheet_names:
                    kwargs['sheet_name'] = file.sheet_name
            if file.row_number is not None:
                kwargs['skiprows'] = file.row_number - 2

            data = pd.read_excel(abspath, header=None, **kwargs)

            # check if source row is valid
            if kwargs['skiprows'] and kwargs['skiprows'] < 0:
                source_row = np.repeat('', (len(data.columns)))
            else:
                source_row = data.loc[[0]].values[0]
        else:
            slogger.error(f"Unsupported extension to load file from: {file.file_extension}", save_message=True)
            raise RecordedError

        return source_row


    def write_table(self,
                    table_data: TableData,
                    file_name: str,
                    directory: str,
                    extension: str) -> str:
        """
        Write a table to the file storage system.

        Args:
            - table_data: the table to store
            - file_name: the name of the file in which the table is stored
            - directory: the relative path from root to location of this table

        Return:
            - The absolute file path where the table data is stored
        """
        # Get the absolute path from the root
        abs_dir_path  = self.absolute_path(directory)
        abs_file_path = Path(abs_dir_path, file_name)

        # Make sure the directory exists
        if not abs_dir_path.exists():
            slogger.info(f"Directory {abs_dir_path} does not exist, creating it")
            abs_dir_path.mkdir(parents=True)

        # Store the internal column names:
        header = table_data.header
        renamer = {col.id: col.original_name for col in header.columns}
        table_data.data = table_data.data.rename(columns=renamer)

        # Save the file to file storage
        if extension == ".pkl":
            table_data.data.to_pickle(abs_file_path)
        elif extension == ".xlsx":
            table_data.data.to_excel(abs_file_path)
        elif extension == ".csv":
            table_data.data.to_csv(abs_file_path)
        else:
            slogger.error(f"Unsupported extension to save file as: {extension}", save_message=True)
            raise RecordedError

        slogger.info(f"Saved file: {abs_file_path}")

        return abs_file_path
