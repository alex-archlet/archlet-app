"""
Status message
"""
from datetime import datetime

from shared.constants import Namespace
from shared.constants import RecordedError
from shared.constants import SEVERITY
from shared.constants import STATUS_MESSAGE_CATEGORY


ATTRIBUTES = {
    STATUS_MESSAGE_CATEGORY.FILE:     ['filename', 'filetype', 'location'],
    STATUS_MESSAGE_CATEGORY.SCENARIO: ['card', 'item'],
    STATUS_MESSAGE_CATEGORY.DEFAULT:  [],
}



class StatusMessage:
    def __init__(self, timestamp: datetime, severity: str, category: str, text: str, **kwargs) -> None:
        """
        Create a status message, based on the category of the message we expect
        different parameters as defined by the respective message category.

        Args:
            timestamp (datetime.datetime): Timestamp for message
            severity (str): Severity value from constants.SEVERITY
            category (str): Status message category from STATUS_MESSAGE_CATEGORIES
            text (str): Text content of the status message
            kwargs (dict): Contains all extra attributes specified in ATTRIBUTES
        """

        # Check that the kwargs UKEYS correspond to the ones defined in the category
        # of the status message
        if set(kwargs.keys()) != set(ATTRIBUTES[category]):

            # Extra key
            for key in kwargs.keys():
                if key not in ATTRIBUTES[category]:
                    raise RecordedError(f"Invalid '{category} status message' attribute '{key}'")

            # Missing key
            for key in ATTRIBUTES[category]:
                if key not in kwargs.keys():
                    raise RecordedError(f"Missing '{category} status message' attribute '{key}'")


        # Finally, assign members
        self.__dict__  = kwargs
        self.text      = text
        self.severity  = severity
        self.category  = category
        self.timestamp = timestamp


    def to_string(self) -> str:
        """
        Dump message to human-friendly string message
        """

        sstring = '{:9s}'.format('[{}]'.format(self.severity.upper()))
        tstring = self.timestamp.strftime('%H:%M:%S.%f')
        mstring = self.text

        return '{} {} {}'.format(tstring, sstring, mstring)


    def to_dict(self) -> dict:
        """
        Dump to JSON-friendly dictionary
        """

        serialize_values = {
            'timestamp': self.timestamp.strftime('%Y.%m.%d %H:%M:%S.%f'),
            'severity':  self.severity,
            'category':  self.category,
            'message':   self.text
        }

        for key in ATTRIBUTES[self.category]:
            serialize_values[key] = getattr(self, key)

        return serialize_values
