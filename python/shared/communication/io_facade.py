"""
Definition of IOFacade, a wrapper for all communication related functions
"""

from __future__ import annotations

import os
import pandas as pd
import simplejson as json
import uuid

from collections import defaultdict
from datetime import datetime
from functools import wraps
from pathlib import Path
from time import time
from typing import Any
from typing import Callable
from typing import Dict
from typing import List
from typing import TYPE_CHECKING
from typing import Union

from shared.communication import DBFile
from shared.communication import DatabaseConnection
from shared.communication import FileStorageManager
from shared.communication import SingletonLogger
from shared.communication.database_classes import AllocationsDB
from shared.communication.database_classes import BidColumnsDB
from shared.communication.database_classes import BiddingRoundsDB
from shared.communication.database_classes import DemandsDB
from shared.communication.database_classes import DiscountsDB
from shared.communication.database_classes import JobInfoDB
from shared.communication.database_classes import OffersDB
from shared.communication.database_classes import OptimizationFilesDB
from shared.communication.database_classes import ProjectKpiDefsDB
from shared.communication.database_classes import ProjectsDB
from shared.communication.database_classes import ScenarioKpisDB
from shared.communication.database_classes import ScenariosDB
from shared.constants import DATA
from shared.constants import JOB_STATUS
from shared.constants import KEYS
from shared.constants import RecordedError
from shared.constants import UKEYS
from shared.entities import ProjectSettings
from shared.entities import TableData
from shared.entities import TableHeader
from shared.entities import Translator

from uuid import UUID

from .iofacade_processing import default_cols_to_table

if TYPE_CHECKING:
    from shared.communication.database_classes import BranchesDB

# temp see where does this go
PARAMS_DB = {
    "host": os.environ.get("DB_HOST"),
    "port": os.environ.get("DB_PORT"),
    "dbname": os.environ.get("DB_NAME"),
    "user": os.environ.get("DB_USER"),
    "password": os.environ.get("DB_PASSWORD")
}

slogger = SingletonLogger()

class IOFacade:
    """ Wrapper for all communication classes """
    def __init__(self, job_id: int, root: str) -> None:
        self.fs_manager = FileStorageManager(root)
        self.database_connection = None
        self.job_id = job_id
        self.project_id: str = None
        self.scenario_id: str = None
        self.round_id: str = None
        self.user_id = None
        self.branch_id = -1 # has to be set when the job is fetched for the first time
        self.solver = None
        self.job_status = -1

    def initialize_db_connection(self):
        """ Create a database connection """
        if self.database_connection is None:
            slogger.info("Creating a new database connection")
            self.database_connection = DatabaseConnection(**PARAMS_DB)


    ####################################################################################
    # ------------------------- DECORATORS DEFINITION ----------------------- #
    ####################################################################################

    def flush_after(func: Callable[..., Any]) -> Callable[..., Any]: # pylint: disable=no-self-argument
        """
        Decorator to flush the database buffer after
        execution of the wrapped function.
        """
        @wraps(func)
        def flushing_func(self, *args, **kwargs):
            result = func(self, *args, **kwargs)
            self.database_connection.flush_buffer()
            return result
        return flushing_func


    def measure_time(message: str) -> Callable[..., Any]: # pylint: disable=no-self-argument
        """
        Decorator to time the execution of a loading/storing method and log it in the stdout
        """
        def inner_dec(func):
            @wraps(func)
            def timed_func(self, *args, **kwargs):
                t_start = time()
                result = func(self, *args, **kwargs)
                slogger.debug(f'{message} took {time() - t_start} seconds')
                return result
            return timed_func
        return inner_dec


    ####################################################################################
    # ------------------------- JOB/PROJECT LOADING/STORING ----------------------- #
    ####################################################################################


    def clean(self) -> None:
        """ close the database session """
        if self.database_connection is not None:
            self.database_connection.close_sessions()

    def determine_mode(self) -> str:
        """
        Checks the database jobs to determine if the project is from the bid importer
        or manual upload
        """
        jobs = self.database_connection.fetch_project_jobs_by_id(self.project_id, 6)
        if jobs:
            return 'bid-importer'
        else:
            return 'manual-upload'

    def get_messages(self):
        """ Retrieve the messages stored in the logger """
        return slogger.to_dict()['messages']

    def fetch_job(self) -> JobInfoDB:
        """ wrapper for database_connection fetch_job """
        job = self.database_connection.fetch_one_or_none(JobInfoDB, {'id': self.job_id})
        self.project_id = job.project_id
        self.branch_id = job.branch_id
        self.solver = job.branch.solver
        self.user_id = job.user_id
        self.job_status = job.status_id
        if hasattr(job, 'scenario_id'):
            self.scenario_id = job.scenario_id
        if hasattr(job, 'round_id'):
            self.round_id = job.round_id
        return job


    def is_job_canceled(self) -> bool:
        """
        Check whether this job is canceled
        """
        job = self.database_connection.fetch_one_or_none(JobInfoDB, {'id': self.job_id})
        self.job_status = job.status_id
        return (job.status_id == JOB_STATUS.CANCELED) | (job.status_id == JOB_STATUS.ABORTED)


    def fetch_job_file(self, file_id: str):
        """
        Wrapper for database_connection fetch file
        """
        return self.database_connection.fetch_one_or_none(OptimizationFilesDB,
                                                          {'id': file_id, 'branch_id': self.branch_id})

    def add_matching_to_file(self, file_id: str, matching: dict):
        """
        Store the mapping in the optimization files table
        """
        self.database_connection.update_one_or_none(OptimizationFilesDB,
                                                    {'id': file_id},
                                                    {'matchings': matching})

    def fetch_job_files(self, job_input):
        """ Wrapper for database_connection fetch_job_files """
        return self.database_connection.fetch_job_files(job_input, self.branch_id)


    def fetch_additional_offer_files(self, round_id):
        """
        Return a list of file objects representing the uploaded offer files.
        """
        files = self.database_connection.fetch_files(self.branch_id,
                                                     round_id=round_id,
                                                     file_type='bid_import',
                                                     deleted_at=None)
        # TO DO: parse into DBFile objects: also copy the matching section
        result = []
        for file in files:
            db_file = DBFile(file_path=file.file_path,
                             name=file.name,
                             file_version=file.file_version,
                             file_extension=file.file_extension,
                             sheet_name=file.sheet_name,
                             row_number=file.row_number,
                             round_id=file.round_id,
                             description=file.description,
                             matchings=file.matchings,
                             id=file.id)
            result.append(db_file)
        return result

    def fetch_project(self) -> ProjectsDB:
        """ wrapper for database_connection fetch_project """
        return self.database_connection.fetch_one_or_none(ProjectsDB, {'id': self.project_id, 'branch_id': self.branch_id})

    def fetch_project_settings(self) -> ProjectSettings:
        """ Get the project settings from the current project """
        project = self.fetch_project()
        settings = ProjectSettings()
        if project.settings is None:
            return settings
        for setting, value in project.settings.items():
            setattr(settings, setting, value)
        return settings


    def fetch_round_id(self, status_id=None) -> str:
        """ Wrapper for database_connection fetch_round_id """
        self.round_id = self.database_connection.fetch_round_id(project_id=self.project_id,
                                                                status_id=status_id,
                                                                deleted_at=None)
        return self.round_id

    def update_project_settings(self, settings: dict) ->None:
        """Update the settings to the projects table in the database """
        self.database_connection.update_one_or_none(ProjectsDB,
                                                    {'id': self.project_id, 'branch_id': self.branch_id},
                                                    {'settings': settings, 'updated_at': datetime.now()})

    def update_project_time(self) -> None:
        """Update the time of the projects table in the database """
        self.database_connection.update_one_or_none(ProjectsDB,
                                                    {'id': self.project_id, 'branch_id': self.branch_id},
                                                    {'updated_at': datetime.now()})

    def fetch_scenario(self) -> ScenariosDB:
        """ wrapper for database_connection fetch_scenario """
        return self.database_connection.fetch_one_or_none(ScenariosDB, {'id': self.scenario_id, 'branch_id': self.branch_id})

    def fetch_custom_kpi_defs(self) -> Dict[str, str]:
        """
        Get the definitions of the kpis for this project

        Returns:
            dictionary containing custom kpis
        """
        result = self.database_connection.fetch_project_kpi_defs(self.project_id)
        if result is None:
            return {}
        custom_kpis = {}
        for kpi_def in result:
            custom_kpis[kpi_def.id] = kpi_def.calculation
        return custom_kpis


    def fetch_default_kpi_defs(self) -> Dict[str, str]:
        """
        Get the definitions of the default kpis
        """
        result = self.database_connection.fetch_project_kpi_defs(None)
        default_kpis = {}
        for kpi_def in result:
            default_kpis[kpi_def.id] = kpi_def.name
        return default_kpis


    def fetch_translator(self) -> Translator:
        """ wrapper for database_connection fetch_file """
        job = self.fetch_job() # This is duplicated, already done when initializing the job in main.py
        try:
            file_id = self.database_connection.fetch_one_or_none(OptimizationFilesDB,
                                                                 {'project_id': self.project_id, 'branch_id': self.branch_id, 'file_path': 'translators'})
        except RecordedError:
            # If a translator isn't found for the project, we fetch the default branch translator
            file_id = self.database_connection.fetch_one_or_none(OptimizationFilesDB,
                                                                 {'id': job.input['translator']})
        filepath = self.fs_manager.file_path(file_id, abs=True)
        translator = Translator(filepath)
        return translator


    def update_job_status(self, status: int) -> None:
        """ wrapper for database_connection update_job_status """
        if (self.job_status != JOB_STATUS.CANCELED) and (self.job_status != JOB_STATUS.ABORTED):
            self.database_connection.update_one_or_none(JobInfoDB,
                                                        {'id': self.job_id, 'branch_id': self.branch_id},
                                                        {'status_id': status, 'updated_at': datetime.now()})
            self.job_status = status

    def update_job_progress(self, progress: int) -> None:
        """ wrapper for database_connection update_job_progress """
        self.database_connection.update_one_or_none(JobInfoDB,
                                                    {'id': self.job_id, 'branch_id': self.branch_id},
                                                    {'progress': progress, 'updated_at': datetime.now()})


    def add_job_output(self, output: dict) -> None:
        """ wrapper for database_connection add_job_output """
        self.database_connection.update_one_or_none(JobInfoDB,
                                                    {'id': self.job_id, 'branch_id': self.branch_id},
                                                    {'output': output, 'updated_at': datetime.now()})

    def fetch_job_output(self) -> dict:
        """ wrapper for database_connection fetch_job_output """
        job = self.database_connection.fetch_one_or_none(JobInfoDB,
                                                         {'id': self.job_id, 'branch_id': self.branch_id})

        return job.output

    def update_scenario(self, invalidate: bool, status: str):
        """ wrapper for database_connection update_scenario """
        self.database_connection.update_one_or_none(ScenariosDB,
                                                    {'id': self.scenario_id, 'branch_id': self.branch_id},
                                                    {'status': status, 'invalidate': invalidate, 'updated_at': datetime.now()})

    def add_scenario_output(self, scenario_output: dict):
        """ wrapper for database_connection add_scenario_output """
        self.database_connection.update_one_or_none(ScenariosDB,
                                                    {'id': self.scenario_id, 'branch_id': self.branch_id},
                                                    {'output': scenario_output, 'updated_at': datetime.now()})

    def latest_file_version(self,
                            filename: str,
                            extension: str,
                            directory: str) -> str:
        """ wrapper for database_connection latest_file_version """
        max_version = self.database_connection.latest_file_version(filename,
                                                                   extension,
                                                                   directory,
                                                                   self.branch_id)
        return max_version


    def get_project_branch(self) -> BranchesDB:
        """ wrapper for database_connection get_project_branch """
        project = self.database_connection.fetch_one_or_none(ProjectsDB, {'id': self.project_id})
        return project.branch

    def add_new_file_entry(self,
                           name: str,
                           directory: Path,
                           extension: str,
                           description: str,
                           filetype: str,
                           version: int,
                           abs_file_path: Path,
                           round_id: str = None):
        """ wrapper for database_connection add_new_file_entry """
        ro_file = self.database_connection.add_new_file_entry(name=name,
                                                            directory=str(directory),
                                                            extension=extension,
                                                            description=description,
                                                            filetype=filetype,
                                                            version=version,
                                                            abs_file_path=abs_file_path,
                                                            branch_id=self.branch_id,
                                                            project_id=self.project_id,
                                                            scenario_id=self.scenario_id,
                                                            round_id=round_id)
        return ro_file


    def get_files_directory(self, directory):
        """ construct the file path for storage in file system """
        branch = self.get_project_branch()

        rel_dir = self.fs_manager.get_processed_files_directory(branch.company.id,
                                                                self.branch_id,
                                                                self.project_id,
                                                                self.scenario_id)
        rel_dir = rel_dir / directory
        return rel_dir


    def create_bidding_round_if_needed(self):
        """
        Create a bidding round if there is none associated with this project.
        """
        if self.round_id is not None:
            return
        self.round_id = self.database_connection.fetch_round_id(project_id=self.project_id)
        if self.round_id == False:
            self.round_id = str(uuid.uuid4())
            # Get project name
            project = self.fetch_project()
            round_name = f"{project.name} - temporary round"
            self.database_connection.add_bidding_round(self.round_id,
                                                       round_name,
                                                       self.branch_id,
                                                       project_id=self.project_id,
                                                       start_at=datetime.now(),
                                                       status_id=1)


    def set_bidding_round_finished(self):
        """
        Set the current bidding round to finished.
        """
        self.database_connection.update_bidding_round(self.round_id,
                                                      status_id=4)


    def load_table(self, table: TableData, file: DBFile) -> TableData:
        """
        Given a database entry from the optimization files table, load the data
        from the specified file
        """
        return self.fs_manager.load_table(table, file)


    def load_source_row(self, file: DBFile):
        """
        Load the row of the given Excel file that contains the input_by information
        """
        return self.fs_manager.load_source_row(file)


    def dump_table(self,
                   table_data: TableData,
                   name: str,
                   directory: str,
                   description: str,
                   extension: str = ".csv"):
        """
        Given a name, description and extension, save a Table Data object
        to the file storage system. Moreover, a database entry is created
        """
        rel_dir = self.get_files_directory(directory)

        # determine the version of the file
        latest_version = self.latest_file_version(filename=name,
                                                  directory=directory,
                                                  extension=extension)
        version = 0 if latest_version is None else latest_version+1

        # file name in file storage
        file_name = self.fs_manager.file_name(name, version, extension)

        # Write to file storage
        abs_file_path = self.fs_manager.write_table(table_data,
                                                    file_name,
                                                    rel_dir,
                                                    extension)

        # Finally, create a corresponding entry in the database
        file_id = self.add_new_file_entry(name=name,
                                          version=version,
                                          directory=rel_dir,
                                          extension=extension,
                                          description=description,
                                          filetype="TableData",
                                          abs_file_path=abs_file_path,
                                          round_id=self.round_id)

        slogger.info(f"Added new entry to optimization files database with id {file_id}")

        return file_id

    ####################################################################################
    # ------------------------- DISCOUNTS STORING/LOADING METHODS -------------------- #
    ####################################################################################

    @flush_after
    def remove_discounts(self) -> None:
        """
        Remove the discounts for a specific round id
        """
        self.database_connection.delete_rows(DiscountsDB, {'round_id': self.round_id})


    @measure_time('Storing the discounts')
    @flush_after
    def store_discounts(self, table: TableData) -> None:
        """
        Store the discounts table in the database
        """
        start_time = datetime.now()
        header = table.header

        discount_units = self.database_connection.fetch_discount_units()
        discount_types = self.database_connection.fetch_discount_types()

        table[header[UKEYS.DISCOUNT_UNIT]] = table[header[UKEYS.DISCOUNT_UNIT]].replace(discount_units)
        table[header[UKEYS.DISCOUNT_TYPE]] = table[header[UKEYS.DISCOUNT_TYPE]].replace(discount_types)

        self.database_connection.store_discounts_table(table, self.round_id)



    @measure_time('Loading the discounts')
    def load_discounts(self, table: TableData) -> None:
        """
        Load the discounts table corresponding to this round id
        """
        try:
            data = self.database_connection.fetch_table('discounts', self.round_id)
        except RecordedError:
            return None

        discount_units = self.database_connection.fetch_discount_units()
        discount_types = self.database_connection.fetch_discount_types()

        data[UKEYS.DISCOUNT_UNIT] = data[UKEYS.DISCOUNT_UNIT].replace({v:k for k,v in discount_units.items()})
        data[UKEYS.DISCOUNT_TYPE] = data[UKEYS.DISCOUNT_TYPE].replace({v:k for k,v in discount_types.items()})
        data[UKEYS.PRICE_COMPONENT] = data[UKEYS.PRICE_COMPONENT].astype('str')

        table.data = data
        table.trim()
        header = TableHeader()
        for col in table.data.keys():
            header.add_column(col, col, '', col)
        table.header = header
        return table


    def get_discount_units(self):
        """
        return a mapping discount_unit_name -> discount_unit_id
        """
        return self.database_connection.fetch_discount_units()


    def get_discount_types(self):
        """
        return a mapping discount_type_name -> discount_type_id
        """
        return self.database_connection.fetch_discount_types()

    ####################################################################################
    # ------------------------- KPIs STORING/LOADING METHODS ----------------------- #
    ####################################################################################

    def remove_scenario_kpis(self):
        """
        Remove the scenario kpis from the DB
        """
        self.database_connection.delete_rows(ScenarioKpisDB, {'scenario_id': self.scenario_id})


    @measure_time('Storing the allocations kpis')
    @flush_after
    def store_allocation_kpis(self, table: pd.DataFrame) -> None:
        """
        Store a table with allocation level kpis
        """
        self.database_connection.store_all_allocation_kpis(table, self.scenario_id)


    @measure_time('Storing the summed kpis')
    @flush_after
    def store_summed_kpis(self, summed_kpis: Dict[str, str]) -> None:
        """
        Store the summed kpis for the current scenario
        """
        for kpi_id, value in summed_kpis.items():
            self.database_connection.store_allocation_kpi(self.scenario_id,
                                                          kpi_id,
                                                          value)





    def remove_project_kpi_defs(self) -> None:
        """
        Remove the kpi definitions connected to the current project
        """
        self.database_connection.delete_rows(ProjectKpiDefsDB, {'project_id': self.project_id})

    def store_kpi_defs(self, kpi_definitions) -> None:
        """
        Store the given kpi definitions to the database.
        """
        for name, calculation in kpi_definitions.items():
            self.database_connection.store_kpi_def(name, calculation, project_id=self.project_id)

    ####################################################################################
    # ------------------------- ALLOC STORING/LOADING METHODS ----------------------- #
    ####################################################################################

    @measure_time('Storing the allocations')
    def store_allocations_db(self, table: TableData) -> None:
        """
        Store an allocations table in the database
        """
        self.database_connection.store_allocations_table(table, self.round_id, self.scenario_id)


    @staticmethod
    def _rename_allocations_columns(allocations: pd.DataFrame,
                                    header: TableHeader) -> pd.DataFrame:
        """
        Cast the DB column names to the keys used in the engine
        """
        allocations.rename(columns={'id':header[UKEYS.ALLOCATION_ID],
                                    'volume':UKEYS.TOTAL_VOLUME,
                                    'total_price':UKEYS.TOTAL_SPENT},
                           inplace=True)
        return allocations

    def load_allocations_db(self, table: TableData, scenario_id: str) -> TableData:
        """
        Load an allocations table from the database.
        """
        header = TableHeader()
        header = self.create_allocations_header(header)

        allocations = self.database_connection.fetch_allocations_table(scenario_id=scenario_id)

        allocations = self._rename_allocations_columns(allocations, header)

        table.data = allocations
        table.trim()
        table.header = header
        table.add_uuid_names()
        table[header[UKEYS.DEMAND_ID]] = table[header[UKEYS.DEMAND_ID]].astype('str')
        header.add_column(UKEYS.UNIT_PRICE, original_name=UKEYS.UNIT_PRICE)
        # header.add_column(UKEYS.UNIT_PRICE)
        table[header[UKEYS.UNIT_PRICE]] = table[header[UKEYS.TOTAL_SPENT]] / table[header[UKEYS.TOTAL_VOLUME]]
        # Filter out allocations without an offer
        mask = table[header[UKEYS.OFFER_ID]].notna()
        table.data = table[mask]
        table[header[UKEYS.OFFER_ID]] = table[header[UKEYS.OFFER_ID]].astype('str')
        table[header[UKEYS.DEMAND_ID]] = table[header[UKEYS.DEMAND_ID]].astype('str')
        header.add_column(UKEYS.UNIT_PRICE, original_name=UKEYS.UNIT_PRICE)
        # header.add_column(UKEYS.UNIT_PRICE)
        table[header[UKEYS.UNIT_PRICE]] = table[header[UKEYS.TOTAL_SPENT]] / table[header[UKEYS.TOTAL_VOLUME]]
        table.trim()
        return table


    def load_extended_allocations_db(self, table: TableData, scenario_id: str) -> TableData:
        """
        Load an allocations table with the offers attributes
        """
        header = TableHeader()
        header = self.load_table_header_db(header, self.project_id, 'supplier')

        allocations_df = self.database_connection.fetch_extended_allocations_table(scenario_id=scenario_id)
        allocations_df = allocations_df[allocations_df['offer_id'].notna()]

        flat_df = pd.DataFrame(list(allocations_df['attributes']))
        result_df = pd.DataFrame()

        for col in [col.id for col in header.columns if col.attribute]:
            try:
                result_df[col] = flat_df[col]
            except KeyError:
                col_name = header.byid[col].original_name
                slogger.warning(f"The column {col_name} is not present in the attributes loaded from the database")

        # allocations_df.drop(columns=[''], inplace=True)

        # translator = {
        #     'total_price': UKEYS.TOTAL_SPENT,
        #     'volume': UKEYS.TOTAL_VOLUME,
        #     'demand_id': UKEYS.DEMAND_ID,
        #     'offer_id': UKEYS.OFFER_ID,
        #     'supplier': UKEYS.SUPPLIER
        # }

        # result_df, header = default_cols_to_table(allocations_df, result_df, header, translator)

        # extend the header with the allocation specific columns.
        header.add_column(UKEYS.TOTAL_SPENT)
        header.add_column(UKEYS.TOTAL_VOLUME)
        header.add_column(UKEYS.DEMAND_ID)
        header.add_column(UKEYS.OFFER_ID)
        result_df[header[UKEYS.TOTAL_SPENT]] = allocations_df['total_price']
        result_df[header[UKEYS.TOTAL_VOLUME]] = allocations_df['volume']
        result_df[header[UKEYS.DEMAND_ID]] = allocations_df['demand_id'].astype('str')
        result_df[header[UKEYS.OFFER_ID]] = allocations_df['offer_id']
        result_df[header[UKEYS.SUPPLIER]] = allocations_df['supplier']
        # header.add_column(UKEYS.UNIT_PRICE, UKEYS.UNIT_PRICE, KEYS.PRICE)

        header.add_column(UKEYS.UNIT_PRICE)
        result_df[header[UKEYS.UNIT_PRICE]] = result_df[header[UKEYS.TOTAL_SPENT]] / result_df[header[UKEYS.TOTAL_VOLUME]]

        table.data = result_df
        table.trim()
        table.header = header
        return table


    def remove_allocations_db(self, scenario_id):
        """
        Remove all the allocations that are connected to the given
        scenario id.
        """
        self.database_connection.delete_rows(AllocationsDB, {'scenario_id': scenario_id})

    @staticmethod
    def create_allocations_header(header: TableHeader):
        """
        Create the generic allocations table header : always contains the same columns, with the names
        from the database columns
        """
        # The names are strings since they have to correspond to the column
        # names in the database, therefore a change
        # in the value of the UKEYS should not break loading the allocations.
        header.add_column(UKEYS.TOTAL_SPENT, original_name=UKEYS.TOTAL_SPENT)
        header.add_column(UKEYS.TOTAL_VOLUME, original_name=UKEYS.TOTAL_VOLUME)
        header.add_column(UKEYS.DEMAND_ID, original_name=UKEYS.DEMAND_ID)
        header.add_column(UKEYS.OFFER_ID, original_name=UKEYS.OFFER_ID)
        header.add_column(UKEYS.SUPPLIER, original_name=UKEYS.SUPPLIER)
        header.add_column(UKEYS.ALLOCATION_ID, original_name=UKEYS.ALLOCATION_ID)

        return header

    def clean_allocations_db(self):
        """
        For all the allocations tables connected to this project, remove rows that
        """
        try:
            rounds = self.database_connection.fetch_all(BiddingRoundsDB, {"project_id": self.project_id})
            rounds = [str(bid_round.id) for bid_round in rounds]
        except RecordedError:
            slogger.info("No rounds connected to the current project: finished cleaning")
            return
        for bid_round_id in rounds:
            self.database_connection.delete_rows(AllocationsDB, {"round_id": bid_round_id, "demand_id": None})

    ####################################################################################
    # ------------------------- OFFERS STORING/LOADING METHODS ----------------------- #
    ####################################################################################

    @flush_after
    def store_baseline_offers_db(self, table: TableData) -> None:
        """
        Store baseline offers in the offers table
        """
        self.store_offers_db(table, is_historic=True)


    def load_baseline_offers_db(self,
                                table: TableData,
                                round_id: str,
                                load_automatic_columns: bool = False) -> TableData:
        """
        Load a baseline offers table

        Args:
            table: a TableData object to add the baseline offers to
            round_id: the round id to fetch the data from
            load_automatic_columns: boolean whether automatic columns should be fetched

        Returns:
            TableData object with baseline offer data
        """
        try:
            table = self.load_offers_db(table,
                                        round_id,
                                        is_historic=True,
                                        load_automatic_columns=load_automatic_columns)
            return table
        except RecordedError:
            slogger.warning(f"No baseline offers found for round id {round_id}")
            return None

    @measure_time("updating the offers")
    @flush_after
    def update_offers_db(self, table: TableData, old_table: TableData, is_historic: bool = False) -> None:
        """
        Update an offers table in the database.

        For the current project id there are three types of offers possible:
            1. offers that are already present in the database -> these get updated
            2. offers that were in the database but not in the current table -> get deleted
            3. offers that are in the table but not in the database -> get added.
        """
        header = table.header

        table = self._add_attributes_column(table)
        table = self.clean_offers_for_storing(table)
        table['is_historic'] = is_historic
        update_mask = table[header[UKEYS.DB_STATUS]] == DATA.PRESENT
        insert_mask = table[header[UKEYS.DB_STATUS]] == DATA.NEW
        delete_mask = ~old_table[old_table.header[UKEYS.OFFER_ID]].isin(table[header[UKEYS.OFFER_ID]])
        delete_list = old_table[delete_mask][old_table.header[UKEYS.OFFER_ID]].to_list()

        update_data = table[update_mask]
        store_data = table[insert_mask]
        table.data = update_data
        self.database_connection.update_offers_table(table, self.round_id, False)
        table.data = store_data
        self.database_connection.store_offers_table(table, self.round_id, False)
        self.database_connection.delete_by_id(OffersDB, delete_list)



    @measure_time('Storing the offers')
    @flush_after
    def store_offers_db(self, table: TableData, is_historic: bool = False) -> None:
        """
        Store an offers table in the database.
        """
        header = table.header

        table.data = table.data.replace({'"': ''}, regex=True)

        # Put all the columns in one 'attributes' column
        table = self._add_attributes_column(table)
        table = self.clean_offers_for_storing(table)

        self.database_connection.delete_rows(OffersDB, {'round_id': self.round_id, 'is_historic': is_historic})

        table.header = header

        table['is_historic'] = is_historic

        self.database_connection.store_offers_table(table, self.round_id, False)


    # ---------------------- SHOULD BE DONE IN FILE PROCESSING --------------------
    @staticmethod
    def clean_offers_for_storing(table: TableData) -> TableData:
        """
        Check if all the needed columns are present in the offers table before storing.
        """
        header = table.header

        if 'processing_messages' not in table.data.keys():
            table['processing_messages'] = pd.Series([{} for _ in range(table.data.shape[0])])
        if 'optimization_file_id' not in table.data.keys():
            table['optimization_file_id'] = None
        if UKEYS.TOTAL_PRICE not in header.types:
            header.add_column(UKEYS.TOTAL_PRICE, UKEYS.TOTAL_PRICE, '')
            table[header[UKEYS.TOTAL_PRICE]] = None
        if UKEYS.TOTAL_FIXED_PRICE not in header.types:
            header.add_column(UKEYS.TOTAL_FIXED_PRICE, UKEYS.TOTAL_FIXED_PRICE, '')
            table[header[UKEYS.TOTAL_FIXED_PRICE]] = 0
        if UKEYS.NORMALIZED_PRICE not in header.types:
            "komt hier"
            header.add_column(UKEYS.NORMALIZED_PRICE, UKEYS.NORMALIZED_PRICE, '')
            table[header[UKEYS.NORMALIZED_PRICE]] = -1
        if UKEYS.TOTAL_VOLUME not in header.types:
            header.add_column(UKEYS.TOTAL_VOLUME, UKEYS.TOTAL_VOLUME, '')
            table[header[UKEYS.TOTAL_VOLUME]] = -1

        table.header = header
        return table
    # ------------------------------------------------------------------------------


    def load_offers_db(self,
                       table: TableData,
                       round_id: str,
                       is_historic: bool = False,
                       load_automatic_columns: bool = False) -> TableData:
        """
        Load an offers table from the database.

        Args:
            table: a TableData object to add the offers data to
            round_id: the round id to fetch the data from
            is_historic: boolean whether to read baseline offers or current offers

        Returns:
            TableData object with offers data
        """
        # Load the table header based on the project_id
        header = TableHeader()
        header = self.load_table_header_db(header, self.project_id, 'supplier')

        # Load the contents of the table based on the round_id
        offers_df = self.database_connection.fetch_table('offers', round_id, is_historic=is_historic)

        # Put some extra validation steps here!
        # Parse the contents of the attributes field into separate columns depending on the header
        flat_df = pd.DataFrame(list(offers_df['attributes']))
        result_df = pd.DataFrame()

        offers_df.drop(columns=['created_at', 'updated_at', 'created_by',
                                'updated_by', 'is_processed', 'attributes',
                                'is_historic', 'processing_messages', 'supplier_id',
                                'price_total', 'round_id', 'optimization_file_id'], inplace=True)

        columns_to_load = [col.id for col in header.columns if col.attribute]

        for col in columns_to_load:
            try:
                result_df[col] = flat_df[col]
            except KeyError:
                col_name = header.byid[col].original_name
                slogger.warning(f"The column {col_name} is not present in the attributes loaded from the database")

        translator = {UKEYS.DEMAND_ID:UKEYS.DEMAND_ID,
                     'id':UKEYS.OFFER_ID,
                     'total_var_price':UKEYS.NORMALIZED_PRICE,
                     'supplier':UKEYS.SUPPLIER,
                     'total_volume':UKEYS.TOTAL_VOLUME,
                     'total_fixed_price':UKEYS.TOTAL_FIXED_PRICE,
                     'currency': KEYS.CURRENCY}

        result_df, header = default_cols_to_table(offers_df, result_df, header, translator)

        if UKEYS.ITEM_CATEGORY not in header.types:
            header.add_column(UKEYS.ITEM_CATEGORY)
            result_df[header[UKEYS.ITEM_CATEGORY]] = 'standard'

        table.data = result_df
        table.header = header

        return table

    @staticmethod
    def _add_attributes_column(table) -> TableData:
        """
        Add an attributes column to this table
        """
        header = table.header
        helper = table[[col.id for col in header.columns if col.attribute]]#.fillna('null')
        table['attributes'] = helper.to_dict(orient='records')
        return table

    ####################################################################################
    # ------------------------- DEMANDS STORING/LOADING METHODS ----------------------- #
    ####################################################################################

    @measure_time("updating the demands in the database")
    def update_demands_db(self, table: TableData, old_table: TableData) -> None:
        """
        Update a demands table in the database.

        For the current project id there are three types of demands possible:
            1. demands that are already present in the database -> these get updated
            2. demands that were in the database but not in the current table -> get deleted
            3. demands that are in the table but not in the database -> get added.
        """
        header = table.header
        table = self._add_attributes_column(table)
        update_mask = table[header[UKEYS.DB_STATUS]] == DATA.PRESENT
        insert_mask = table[header[UKEYS.DB_STATUS]] == DATA.NEW
        delete_mask = ~old_table[old_table.header[UKEYS.DEMAND_ID]].isin(table[header[UKEYS.DEMAND_ID]])
        delete_list = old_table[delete_mask][old_table.header[UKEYS.DEMAND_ID]].to_list()

        update_data = table[update_mask]
        store_data = table[insert_mask]
        table.data = update_data
        self.database_connection.update_demands_table(table, self.round_id, self.branch_id)
        table.data = store_data
        self.database_connection.store_demands_table(table, self.round_id, self.branch_id)
        self.database_connection.delete_by_id(DemandsDB, delete_list)


    def load_demands_db(self, table: TableData, round_id: str) -> TableData:
        """
        Load a demands table from the database.

        Args:
            table: a TableData object to add the demands data to
            project_id: the project id to fetch the bid columns (table header) from
            round_id: the round id to fetch the data from

        Returns:
            TableData object with demands data
        """
        header = TableHeader()
        header = self.load_table_header_db(header, self.project_id, 'buyer')
        # header.add_column(UKEYS.DEMAND_ID, UKEYS.DEMAND_ID, UKEYS.DEMAND_ID)
        # header.add_column(UKEYS.TOTAL_VOLUME, original_name='Volume')

        # load the contents of the table
        demands_df = self.database_connection.fetch_table('demands', round_id)

        flat_df = pd.DataFrame(list(demands_df['attributes']))

        demands_df.drop(columns=['branch_id', 'round_id', 'attributes', 'created_by', 'updated_by',
                                 'created_at', 'updated_at', 'optimization_file_id'], inplace=True)

        translator = {
            'id': UKEYS.DEMAND_ID,
            'volume': UKEYS.TOTAL_VOLUME,
            'item': UKEYS.ITEM_ID
        }

        flat_df, header = default_cols_to_table(demands_df, flat_df, header, translator)

        table.data = flat_df
        table.trim()
        table.header = header
        return table

    @measure_time('Storing the demands')
    def store_demands_db(self, table: TableData) -> dict:
        """
        Store a demands table in the database.
        """
        table.data = table.data.replace({'"': ''}, regex=True)

        table = self._add_attributes_column(table)

        self.database_connection.delete_rows(DemandsDB, {'round_id': self.round_id})

        self.database_connection.store_demands_table(table, self.round_id, self.branch_id)

        # start_time = datetime.now()
        # demands_mapping = self.database_connection.store_demands_table(table,
        #                                                                self.round_id,
        #                                                                self.branch_id,
        #                                                                item_column=header[UKEYS.ITEM_ID],
        #                                                                id_column=header[UKEYS.DEMAND_ID],
        #                                                                volume_column=header[UKEYS.TOTAL_VOLUME])
        # return demands_mapping

    @staticmethod
    def get_demands_mapping(demands: TableData) -> dict:
        """
        Create the demands mapping from the demands table
        """
        return dict(map(tuple, demands[demands.header[[UKEYS.ITEM_ID, UKEYS.DEMAND_ID]]].values))

    ####################################################################################
    # ------------------------- THeader STORING/LOADING METHODS ----------------------- #
    ####################################################################################

    def load_column_types(self):
        """
        Wrapper for loading all column types
        """
        column_types = self.database_connection.fetch_column_types()
        return column_types

    def load_table_header_db(self,
                             table_header: TableHeader,
                             project_id: str,
                             input_by) -> TableHeader:
        """
        Load a table header from the database.

        Args:
            table_header: empty TableHeader object
            project_id: the project for which the bid columns are fetched
            input_by: string specifying whether demands (buyer) or offers (supplier) data is read

        Returns:
            TableHeader containing the bid columns for the specified
            project

        Throws:
            RecordedError when no bid column is present for the given round id
        """
        # Use the following translations for the unique internal columns:
        bid_col_type_to_internal_key = {
            'item': UKEYS.ITEM_ID,
            'currency': KEYS.CURRENCY,
            'supplier': UKEYS.SUPPLIER,
            'item_type': UKEYS.ITEM_CATEGORY,
            'discounted_by': UKEYS.DISCOUNTED_BY,
            'contributing_to': UKEYS.CONTRIBUTING_TO,
            'price_total': UKEYS.TOTAL_PRICE
        }

        bid_columns = self.database_connection.fetch_bid_columns(project_id, input_by=input_by)
        for col in bid_columns:
            if col.type_id is None:
                table_header.add_column(KEYS.UNKNOWN,
                                        col.id,
                                        col.name,
                                        attribute=True)
                continue
            if str(col.type.type) in bid_col_type_to_internal_key.keys():
                internal_key = bid_col_type_to_internal_key[str(col.type.type)]
                table_header.add_column(internal_key,
                                        col.id,
                                        col.name,
                                        calculation=col.calculation,
                                        attribute=True)
                continue
            table_header.add_column(str(col.type.type),
                                    col.id,
                                    col.name,
                                    calculation=col.calculation,
                                    attribute=True)

        return table_header


    @flush_after
    def dump_table_header_db(self,
                             table_header: TableHeader,
                             input_by: str) -> None:
        """
        Store a table header in the database.

        Args:
            table_header: the table header to store
            project_id: the project id
        """
        internal_only_columns = [UKEYS.DEMAND_ID,
                                 UKEYS.OFFER_ID,
                                 UKEYS.TOTAL_VOLUME,
                                 UKEYS.NORMALIZED_PRICE,
                                 UKEYS.TOTAL_FIXED_PRICE]
        columns = table_header.columns
        column_types = self.database_connection.fetch_all_column_types()
        column_types_to_id = defaultdict(lambda : None, {col_type.type: col_type.id for col_type in column_types})

        for col in columns:
            if col.type in internal_only_columns:
                continue

            self.database_connection.store_bid_column(col,
                                                      input_by,
                                                      column_types_to_id,
                                                      self.project_id)


    def delete_table_header_db(self,
                               project_id : str,
                               input_by : str) -> None:
        """
        Remove any bid column entries from the database that match
        the project id and input_by criteria.

        Args:
            project_id: the uuid that identifies the respective project
            input_by: either 'supplier' or 'buyer'
        """
        self.database_connection.delete_rows(BidColumnsDB, {'project_id': project_id, 'input_by': input_by})
