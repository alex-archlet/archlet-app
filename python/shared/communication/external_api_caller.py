import requests
from typing import Union
from shared.constants import RecordedError


def get_currency_rates(from_currency: str) -> Union[dict, str]:
    """
        Get current currency conversion rates for a currency of choice

        Args:
            from_currency: the initial currency to be converted

        Output:
            STATUS 200: A dictionary of exchange rates to other currencies
            ELSE:       Stringified error message
    """
    query = f"https://api.exchangeratesapi.io/latest?base={from_currency}"
    response = requests.get(
        query,
        headers = {'Content-Type':'application/json'}
    )
    try:
        response.raise_for_status()
    except requests.exceptions.HTTPError as res_error:
        return str(res_error)
    
    return response.json()["rates"]
