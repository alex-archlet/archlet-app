import pandas as pd

from uuid import UUID

from typing import List, Dict, Union

from shared.entities import TableData
from shared.entities import TableHeader

from shared.constants import UKEYS


def default_cols_to_table(db_table: pd.DataFrame,
                          table: pd.DataFrame,
                          header: TableHeader,
                          translator: Dict[str, str] = None) -> TableData:
    """
    Translate the DB column names to engine keys, and copies the content
    of the DB table. Also casts UUIDs to strings
    """

    columns = db_table.columns

    if translator is not None:
        translated_columns = [translator[column] for column in db_table.columns]
    else:
        translated_columns = db_table.columns

    for column in translated_columns:
        if column not in header.types:
            header.add_column(column)

    for column, translated_column in zip(columns, translated_columns):
        table[header[translated_column]] = db_table[column]

    table = table.transform(lambda x: x.astype('str') if isinstance(x.iloc[0], UUID) else x)

    return table, header
