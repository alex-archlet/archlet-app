"""
Execute task

Context manager for running task and collecting results
"""
import os
import json
import time
import traceback as tb
import sentry_sdk

from pathlib import Path

from shared.communication.io_facade import IOFacade
from shared.communication import SingletonLogger
from shared.constants import JOB_STATUS
from shared.constants import RecordedError

slogger = SingletonLogger()

def before_send(event, hint):
    # ignore stdout/stderr logging to sentry
    if event.get('logger', None) == 'root' or not hint.get('exc_info'):
        return None
    return event

SENTRY_DSN = os.environ.get("SENTRY_DSN")
ENVIRONMENT_STRING = os.environ.get("ENVIRONMENT_STRING")
directory_path = Path(__file__).absolute().parent.parent
with open(directory_path / "release.json", "r") as json_f:
    RELEASE_VERSION = json.load(json_f)['version']

if ENVIRONMENT_STRING:
    sentry_sdk.init(
        dsn=SENTRY_DSN,
        environment=ENVIRONMENT_STRING,
        before_send=before_send,
        release=f'archlet-app@{RELEASE_VERSION}'
    )

class ExecuteTask:
    def __init__(self, task_name: str, io_facade: IOFacade, task_id = None, toplevel=False, reraise=None):
        """

        Args:
            task_name (str): Display name of the task displayed to the user
        """
        self.name     = task_name
        self.task_id  = task_id
        self.toplevel = toplevel
        self.io_facade = io_facade

        if reraise is None:
            self.reraise = not toplevel
        else:
            self.reraise = reraise



    def success(self):
        message = '{} successfully completed, took {:5.2f}s'\
            .format(self.name.capitalize(), self.tend - self.tstart)
        slogger.info(message)

        # If this is the top level task, update job status, send status update
        # and close state
        if self.toplevel == True:
            self.io_facade.update_job_status(JOB_STATUS.SUCCEEDED)


    def unknown_error(self, exception_type, exception_value, traceback):
        message = 'An unknown error occured during {}'.format(self.name)
        slogger.error(message)

        # Log traceback but don't show this to user
        message = tb.format_exception(exception_type, exception_value, traceback)
        slogger.debug(''.join(message))


    def finished_with_errors(self):
        message = '{} finished with errors, took {:5.2f}s'\
            .format(self.name.capitalize(), self.tend - self.tstart)
        slogger.error(message)

        # If this is the top level task, send status update and close state
        if self.toplevel == True:
            self.io_facade.update_job_status(JOB_STATUS.ABORTED)


    def did_not_finish(self):
        message = '{} aborted'.format(self.name.capitalize())
        slogger.error(message)

        # If this is the top level task, update job status, send status update
        # and close state
        if self.toplevel == True:
            self.io_facade.update_job_status(JOB_STATUS.ABORTED)


    def __enter__(self):
        slogger.info(f"Starting: {self.name}")
        self.tstart = time.time()


    def __exit__(self, exception_type, exception_value, traceback):
        self.tend = time.time()

        # Task successfully completed, inform the user
        if exception_type is None and slogger.success():
            self.success()
            return

        # Task failed but no exception was raised which means it was completed
        # but unsuccessfully
        if exception_type is None:
            self.finished_with_errors()

        # An exception was raised so the task did not finish
        else:
            # An unknown exception occured, record this information
            if exception_type is not RecordedError:
                self.unknown_error(exception_type, exception_value, traceback)
            self.did_not_finish()

        if exception_type is not RecordedError:
            with sentry_sdk.push_scope() as scope:
                scope.set_extra("job_id", self.io_facade.job_id)
                scope.set_extra("project_id", self.io_facade.project_id)
                scope.set_extra("scenario_id", self.io_facade.scenario_id)
                scope.set_extra("round_id", self.io_facade.round_id)
                scope.set_extra("branch_id", self.io_facade.branch_id)
                scope.set_extra("solver", self.io_facade.solver)
                scope.set_extra("user_id", self.io_facade.user_id)
                scope.set_extra("mode", self.io_facade.determine_mode())
                sentry_sdk.capture_exception(exception_value)

        # If reraise is false, return True, i.e. suppress any exceptions
        if not self.reraise:
            return True

        # If we get here the task has failed but we already recorded all the
        # information we could about it so we raise a 'RecordedError' meaning
        # subsequent tasks should be aborted
        raise RecordedError
