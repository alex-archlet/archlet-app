"""
Definition of the classes that are imported from the database
"""

# Disable missing class docstring and too few public methods warnings
# pylint: disable=R0903
# pylint: disable=C0115

from datetime import date
from sqlalchemy import ARRAY
from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import Date
from sqlalchemy import Float
from sqlalchemy import ForeignKey
from sqlalchemy import Integer
from sqlalchemy import JSON
from sqlalchemy import String
from sqlalchemy import Text
from sqlalchemy import null
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.schema import FetchedValue
from sqlalchemy.ext.mutable import MutableDict

Base = declarative_base()

class BidRoundStatusesDB(Base):
    __tablename__ = 'bid_round_statuses'
    id = Column(Integer, primary_key=True, server_default=FetchedValue())
    name = Column(String, nullable=False)
    created_at = Column(Date, nullable=False)
    updated_at = Column(Date, nullable=False)

class ColumnTypesDB(Base):
    __tablename__ = 'column_types'
    id = Column(Integer, primary_key=True)
    type = Column(String, nullable=False)
    text = Column(String, nullable=False)
    is_default = Column(Boolean, default=False)
    created_at = Column(Date, nullable=False)
    updated_at = Column(Date, nullable=False)
    example = Column(String)
    input_by = Column(String)
    is_unique = Column(Boolean)

class CompaniesDB(Base):
    __tablename__ = 'companies'
    id = Column(UUID, primary_key=True, server_default=FetchedValue())
    # json = Column(JSONB, default=JSONB.NULL) TODO: disappeared? TODO: disappeared?
    name = Column(String, unique=True, nullable=False)
    created_at = Column(Date, nullable=False)
    updated_at = Column(Date, nullable=False)

class JobStatusesDB(Base):
    __tablename__ = 'job_statuses'
    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)
    created_at = Column(Date, nullable=False)
    updated_at = Column(Date, nullable=False)

class JobTypesDB(Base):
    __tablename__ = 'job_types'
    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)
    created_at = Column(Date, nullable=False)
    updated_at = Column(Date, nullable=False)

class ProjectBiddingTypesDB(Base):
    __tablename__ = 'project_bidding_types'
    id = Column(Integer, primary_key=True)
    code = Column(String, nullable=False)
    name = Column(String, nullable=False)
    created_at = Column(Date, nullable=False)
    updated_at = Column(Date, nullable=False)

class ProjectCurrenciesDB(Base):
    __tablename__ = 'project_currencies'
    id = Column(Integer, primary_key=True)
    code = Column(String, nullable=False, unique=True)
    created_at = Column(Date, nullable=False)
    updated_at = Column(Date, nullable=False)

class ProjectTypesDB(Base):
    __tablename__ = 'project_types'
    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)
    created_at = Column(Date, nullable=False)
    updated_at = Column(Date, nullable=False)

class ProjectUnitsDB(Base):
    __tablename__ = 'project_units'
    id = Column(Integer, primary_key=True)
    code = Column(String, nullable=False, unique=True)
    name = Column(String, nullable=False)
    created_at = Column(Date, nullable=False)
    updated_at = Column(Date, nullable=False)

class ProjectCategoriesDB(Base):
    __tablename__ = 'project_categories'
    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)
    created_at = Column(Date, nullable=False)
    updated_at = Column(Date, nullable=False)
    type_id = Column(ForeignKey('project_types.id'), nullable=False)
    type = relationship(ProjectTypesDB, primaryjoin=type_id == ProjectTypesDB.id)

class RolesDB(Base):
    __tablename__ = 'roles'
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    permissions = Column(ARRAY(Text), default=null)
    created_at = Column(Date, nullable=False)
    updated_at = Column(Date, nullable=False)

class BranchesDB(Base):
    __tablename__ = 'branches'
    id = Column(UUID, primary_key=True, server_default=FetchedValue())
    account_type = Column(String, nullable=False)
    json = Column(JSONB, default=JSONB.NULL)
    location = Column(String, nullable=False)
    name = Column(String, unique=True)
    solver = Column(String, nullable=False, default='CBC')
    created_at = Column(Date, nullable=False)
    updated_at = Column(Date, nullable=False)
    company_id = Column(ForeignKey('companies.id'), nullable=False)
    company = relationship(CompaniesDB, primaryjoin=company_id == CompaniesDB.id)
    role_id = Column(ForeignKey('roles.id'))
    role = relationship(RolesDB, primaryjoin=role_id == RolesDB.id)

class ProjectsDB(Base):
    __tablename__ = 'projects'
    id = Column(UUID, primary_key=True, server_default=FetchedValue())
    # json = Column(JSONB, default=JSONB.NULL) TODO: disappeared?
    name = Column(String, nullable=False)
    status = Column(String, nullable=False)
    deleted_at = Column(Date, nullable=False)
    created_at = Column(Date, nullable=False)
    updated_at = Column(Date, nullable=False)
    category_id = Column(ForeignKey('project_categories.id'), nullable=False)
    category = relationship(ProjectCategoriesDB,
                            primaryjoin=category_id == ProjectCategoriesDB.id)
    settings = Column(JSONB, default=JSONB.NULL)
    unit_id = Column(ForeignKey('project_units.id'), nullable=False)
    unit = relationship(ProjectUnitsDB, primaryjoin=unit_id == ProjectUnitsDB.id)
    currency_id = Column(ForeignKey('project_currencies.id'), nullable=False)
    currency = relationship(ProjectCurrenciesDB,
                            primaryjoin=currency_id == ProjectCurrenciesDB.id)
    branch_id = Column(ForeignKey('branches.id'), nullable=False)
    branch = relationship(BranchesDB, primaryjoin=branch_id == BranchesDB.id)
    bidding_type_id = Column(ForeignKey('project_bidding_types.id'))
    bidding_type = relationship(ProjectBiddingTypesDB,
                                primaryjoin=bidding_type_id == ProjectBiddingTypesDB.id)

class UsersDB(Base):
    __tablename__ = 'users'
    id = Column(UUID, primary_key=True, server_default=FetchedValue())
    account_type = Column(String, nullable=False)
    email = Column(String, nullable=False)
    first_name = Column(String, nullable=False)
    # json = Column(JSONB, default=JSONB.NULL) TODO: disappeared?
    last_login = Column(Date, default=date.today())
    last_name = Column(String, nullable=False)
    notes = Column(String, default=null)
    password_hash = Column(String, nullable=False)
    password_salt = Column(String, nullable=False)
    # preferences = Column(JSONB, default=JSONB.NULL) TODO: disappeared?
    total_login = Column(Integer, nullable=False, default=0)
    username = Column(String, nullable=False, unique=True)
    created_at = Column(Date, nullable=False)
    updated_at = Column(Date, nullable=False)
    branch_id = Column(ForeignKey('branches.id'), nullable=False)
    branch = relationship(BranchesDB, primaryjoin=branch_id == BranchesDB.id)
    role_id = Column(ForeignKey('roles.id'))
    role = relationship(RolesDB, primaryjoin=role_id == RolesDB.id)

class BiddingRoundsDB(Base):
    __tablename__ = 'bidding_rounds'
    id = Column(UUID, primary_key=True, server_default=FetchedValue())
    name = Column(String, nullable=False)
    description = Column(Text)
    start_at = Column(Date)
    end_at = Column(Date)
    reminder_at = Column(Date)
    created_at = Column(Date, nullable=False)
    updated_at = Column(Date, nullable=False)
    project_id = Column(ForeignKey('projects.id'))
    project = relationship(ProjectsDB, primaryjoin=project_id == ProjectsDB.id)
    status_id = Column(ForeignKey('bid_round_statuses.id'))
    status = relationship(BidRoundStatusesDB, primaryjoin=status_id == BidRoundStatusesDB.id)
    created_by = Column(ForeignKey('users.id'))
    creator = relationship(UsersDB, primaryjoin=created_by == UsersDB.id)
    updated_by = Column(ForeignKey('users.id'))
    updator = relationship(UsersDB, primaryjoin=updated_by == UsersDB.id)
    deleted_at = Column(Date)
    settings = Column(JSONB, default=JSONB.NULL)
    branch_id = Column(ForeignKey('branches.id'), nullable=False)
    branch = relationship(BranchesDB, primaryjoin=branch_id == BranchesDB.id)

class BidColumnsDB(Base):
    __tablename__ = 'bid_columns'
    id = Column(UUID, primary_key=True, server_default=FetchedValue())
    name = Column(String, nullable=False)
    description = Column(Text)
    calculation = Column(String)
    is_automatic = Column(Boolean, default=False)
    is_mandatory = Column(Boolean, default=False)
    is_visible = Column(Boolean, default=True)
    created_at = Column(Date, nullable=False)
    updated_at = Column(Date, nullable=False)
    input_by = Column(String, nullable=False)
    project_id = Column(ForeignKey('projects.id'))
    project = relationship(ProjectsDB, primaryjoin=project_id == ProjectsDB.id)
    type_id = Column(ForeignKey('column_types.id'))
    type = relationship(ColumnTypesDB, primaryjoin=type_id == ColumnTypesDB.id)
    order = Column(Integer)
    limit_min = Column(Float)
    limit_max = Column(Float)
    single_choice = Column(ARRAY(Text))
    initial_type_id = Column(ForeignKey('column_types.id'))
    matching_confidence = Column(Float, nullable=True)
    initial_matching_confidence = Column(Float, nullable=True)

class ScenariosDB(Base):
    __tablename__ = 'scenarios'
    id = Column(UUID, primary_key=True, server_default=FetchedValue())
    invalidate = Column(Boolean, nullable=False)
    is_default = Column(Boolean, default=False)
    json = Column(JSONB, default=JSONB.NULL)
    last_optimization = Column(Date, default=null)
    name = Column(String, nullable=False)
    # optimization_algorithm = Column(String, nullable=False) TODO: disappeared?
    status = Column(String, nullable=False)
    deleted_at = Column(Date)
    created_at = Column(Date)
    updated_at = Column(Date)
    round_id = Column(ForeignKey('bidding_rounds.id'))
    round = relationship(BiddingRoundsDB, primaryjoin=round_id == BiddingRoundsDB.id)
    project_id = Column(ForeignKey('projects.id'))
    project = relationship(ProjectsDB, primaryjoin=project_id == ProjectsDB.id)
    branch_id = Column(ForeignKey('branches.id'), nullable=False)
    branch = relationship(BranchesDB, primaryjoin=branch_id == BranchesDB.id)
    output = Column(JSONB, default=JSONB.NULL)

class DemandsDB(Base):
    __tablename__ = 'demands'
    id = Column(UUID, primary_key=True, server_default=FetchedValue())
    item = Column(String, nullable=False)
    volume = Column(Float)
    attributes = Column(MutableDict.as_mutable(JSONB))
    created_at = Column(Date, nullable=False)
    updated_at = Column(Date, nullable=False)
    created_by = Column(ForeignKey('users.id'))
    _creator = relationship(UsersDB, primaryjoin=created_by == UsersDB.id)
    updated_by = Column(ForeignKey('users.id'))
    _updator = relationship(UsersDB, primaryjoin=updated_by == UsersDB.id)
    round_id = Column(ForeignKey('bidding_rounds.id'))
    _round = relationship(BiddingRoundsDB, primaryjoin=round_id == BiddingRoundsDB.id)
    branch_id = Column(ForeignKey('branches.id'), nullable=False)
    _branch = relationship(BranchesDB, primaryjoin=branch_id == BranchesDB.id)

class JobInfoDB(Base):
    __tablename__ = 'jobs'
    id = Column(UUID, primary_key=True, server_default=FetchedValue())
    input = Column(MutableDict.as_mutable(JSON), nullable=False)
    output = Column(MutableDict.as_mutable(JSON), nullable=False)
    progress = Column(Integer, nullable=False, default=0)
    created_at = Column(Date, nullable=False)
    updated_at = Column(Date, nullable=False)
    scenario_id = Column(ForeignKey('scenarios.id'))
    scenario = relationship(ScenariosDB, primaryjoin=scenario_id == ScenariosDB.id)
    project_id = Column(ForeignKey('projects.id'))
    project = relationship(ProjectsDB, primaryjoin=project_id == ProjectsDB.id)
    type_id = Column(ForeignKey('job_types.id'), nullable=False)
    type = relationship(JobTypesDB, primaryjoin=type_id == JobTypesDB.id)
    status_id = Column(ForeignKey('job_statuses.id'), nullable=False)
    status = relationship(JobStatusesDB, primaryjoin=status_id == JobStatusesDB.id)
    round_id = Column(ForeignKey('bidding_rounds.id'))
    bidding_round = relationship(BiddingRoundsDB, primaryjoin=round_id == BiddingRoundsDB.id)
    branch_id = Column(ForeignKey('branches.id'))
    branch = relationship(BranchesDB, primaryjoin=branch_id == BranchesDB.id)
    user_id = Column(ForeignKey('users.id'))
    user = relationship(UsersDB, primaryjoin=user_id == UsersDB.id)

class OffersDB(Base):
    __tablename__ = 'offers'
    id = Column(UUID, primary_key=True, server_default=FetchedValue())
    total_volume = Column(Float)
    total_fixed_price = Column(Float)
    total_var_price = Column(Float)
    is_processed = Column(Boolean, nullable=False)
    is_historic = Column(Boolean, nullable=False)
    processing_messages = Column(MutableDict.as_mutable(JSONB))
    price_total = Column(Float)
    supplier = Column(String)
    # item_type = Column(String, nullable=False)
    attributes = Column(MutableDict.as_mutable(JSONB))
    created_at = Column(Date, nullable=False)
    updated_at = Column(Date, nullable=False)
    created_by = Column(ForeignKey('users.id'))
    _creator = relationship(UsersDB, primaryjoin=created_by == UsersDB.id)
    updated_by = Column(ForeignKey('users.id'))
    _updator = relationship(UsersDB, primaryjoin=updated_by == UsersDB.id)
    supplier_id = Column(ForeignKey('users.id'))
    _supplier = relationship(UsersDB, primaryjoin=supplier_id == UsersDB.id)
    round_id = Column(ForeignKey('bidding_rounds.id'))
    _round = relationship(BiddingRoundsDB, primaryjoin=round_id == BiddingRoundsDB.id)
    demand_id = Column(ForeignKey('demands.id'))
    _demand = relationship(DemandsDB, primaryjoin=demand_id == DemandsDB.id)
    optimization_file_id = Column(ForeignKey('optimization_files.id'))

class AllocationsDB(Base):
    __tablename__ = 'allocations'
    id = Column(UUID, primary_key=True, server_default=FetchedValue())
    volume = Column(Float, nullable=False)
    total_price = Column(Float, nullable=False)
    offer_id = Column(ForeignKey('offers.id'))
    is_manual = Column(Boolean, default=False)
    offer = relationship(OffersDB, primaryjoin=offer_id == OffersDB.id)
    demand_id = Column(ForeignKey('demands.id'))
    demand = relationship(DemandsDB, primaryjoin=demand_id == DemandsDB.id)
    scenario_id = Column(ForeignKey('scenarios.id'))
    scenario = relationship(ScenariosDB, primaryjoin=scenario_id == ScenariosDB.id)
    round_id = Column(ForeignKey('bidding_rounds.id'))
    bidding_round = relationship(BiddingRoundsDB,
                                 primaryjoin=round_id == BiddingRoundsDB.id)
    created_at = Column(Date, nullable=False)
    updated_at = Column(Date, nullable=False)


class OptimizationFilesDB(Base):
    __tablename__ = 'optimization_files'
    id = Column(UUID, primary_key=True, server_default=FetchedValue())
    content_hash = Column(String, nullable=False)
    description = Column(String)
    file_extension = Column(String, nullable=False)
    file_path = Column(String, nullable=False)
    file_type = Column(String, nullable=False)
    file_version = Column(Integer)
    sheet_name = Column(String)
    row_number = Column(Integer)
    # is_deleted = Column(Boolean, default=False) TODO: disappeared?
    # json = Column(JSONB, default=JSONB.NULL) TODO: disappeared?
    name = Column(String, nullable=False)
    created_at = Column(Date, nullable=False)
    updated_at = Column(Date, nullable=False)
    deleted_at = Column(Date)
    project_id = Column(ForeignKey('projects.id'))
    project = relationship(ProjectsDB, primaryjoin=project_id == ProjectsDB.id)
    scenario_id = Column(ForeignKey('scenarios.id'))
    scenario = relationship(ScenariosDB, primaryjoin=scenario_id == ScenariosDB.id)
    round_id = Column(ForeignKey('bidding_rounds.id'))
    bidding_round = relationship(BiddingRoundsDB,
                                 primaryjoin=round_id == BiddingRoundsDB.id)
    branch_id = Column(ForeignKey('branches.id'), nullable=False)
    branch = relationship(BranchesDB, primaryjoin=branch_id == BranchesDB.id)
    matchings = Column(MutableDict.as_mutable(JSONB))

class ProjectAccessesDB(Base):
    __tablename__ = 'project_accesses'
    id = Column(Integer, primary_key=True)
    access_type = Column(String, nullable=False)
    json = Column(JSONB, default=JSONB.NULL)
    project_id = Column(ForeignKey('projects.id'), nullable=False)
    project = relationship(ProjectsDB, primaryjoin=project_id == ProjectsDB.id)
    user_id = Column(ForeignKey('users.id'), nullable=False)
    user = relationship(UsersDB, primaryjoin=user_id == UsersDB.id)

class SupplierAccessesDB(Base):
    __tablename__ = 'supplier_accesses'
    access_type = Column(String, nullable=False)
    round_id = Column(ForeignKey('bidding_rounds.id'), primary_key=True)
    round = relationship(BiddingRoundsDB, primaryjoin=round_id == BiddingRoundsDB.id)
    user_id = Column(ForeignKey('users.id'), primary_key=True)
    user = relationship(UsersDB, primaryjoin=user_id == UsersDB.id)

    created_at = Column(Date, nullable=False)
    updated_at = Column(Date, nullable=False)

class TranslatorsDB(Base):
    __tablename__ = 'translators'
    id = Column(UUID, primary_key=True, server_default=FetchedValue())
    # json = Column(JSONB, default=JSONB.NULL) TODO: disappeared?
    name = Column(String, unique=True)
    created_at = Column(Date, nullable=False)
    updated_at = Column(Date, nullable=False)
    category_id = Column(ForeignKey('project_categories.id'), nullable=False)
    category = relationship(ProjectCategoriesDB,
                            primaryjoin=category_id == ProjectCategoriesDB.id)
    optimization_file_id = Column(ForeignKey('optimization_files.id'), nullable=False)
    optimization_file = relationship(OptimizationFilesDB,
                                     primaryjoin=optimization_file_id == OptimizationFilesDB.id)
    branch_id = Column(ForeignKey('branches.id'), nullable=False)
    branch = relationship(BranchesDB, primaryjoin=branch_id == BranchesDB.id)

class ProjectKpiDefsDB(Base):
    __tablename__ = 'project_kpi_defs'
    id = Column(UUID, primary_key=True, server_default=FetchedValue())
    name = Column(String, nullable=False)
    description = Column(String, nullable=False)
    calculation = Column(String, nullable=False)
    is_default = Column(Boolean)
    project_id = Column(ForeignKey('projects.id'))
    project = relationship(ProjectsDB, primaryjoin=project_id == ProjectsDB.id)
    created_at = Column(Date, nullable=False)
    updated_at = Column(Date, nullable=False)

class ScenarioKpisDB(Base):
    __tablename__ = 'scenario_kpis'
    id = Column(UUID, primary_key=True, server_default=FetchedValue())
    value = Column(Float, nullable=False, default=0.0)
    project_kpi_def_id = Column(ForeignKey('project_kpi_defs.id'), nullable=False)
    project_kpi_def = relationship(ProjectKpiDefsDB, primaryjoin=project_kpi_def_id == ProjectKpiDefsDB.id)
    allocation_id = Column(ForeignKey('allocations.id'))
    allocation = relationship(AllocationsDB, primaryjoin=allocation_id == AllocationsDB.id)
    scenario_id = Column(ForeignKey('scenarios.id'), nullable=False)
    scenario = relationship(ScenariosDB, primaryjoin=scenario_id == ScenariosDB.id)

class DiscountsDB(Base):
    __tablename__ = 'discounts'
    id = Column(UUID, primary_key=True, server_default=FetchedValue())
    bucket_id = Column(String, nullable=False)
    threshold = Column(Float, nullable=False)
    discount_amount = Column(Float, nullable=False)
    discount_type = Column(String, nullable=False)
    unit = Column(String, nullable=False)
    supplier = Column(String)
    volume_name = Column(String)
    price_component = Column(String)
    round_id = Column(ForeignKey('bidding_rounds.id'), nullable=False)
    round = relationship(BiddingRoundsDB, primaryjoin=round_id==BiddingRoundsDB.id)


# TODO: the order of following list is such that the tables can be
# deleted without having dependency issues, probably there is an
# on_delete cascade setting or so
TABLES = [ScenarioKpisDB,
          ProjectKpiDefsDB,
          TranslatorsDB,
          SupplierAccessesDB,
          ProjectAccessesDB,
          OptimizationFilesDB,
          AllocationsDB,
          OffersDB,
          JobInfoDB,
          DemandsDB,
          ScenariosDB,
          BidColumnsDB,
          BiddingRoundsDB,
          UsersDB,
          ProjectsDB,
          BranchesDB,
          RolesDB,
          ProjectCategoriesDB,
          ProjectUnitsDB,
          ProjectTypesDB,
          ProjectCurrenciesDB,
          ProjectBiddingTypesDB,
          JobTypesDB,
          JobStatusesDB,
          CompaniesDB,
          ColumnTypesDB,
          BidRoundStatusesDB]
