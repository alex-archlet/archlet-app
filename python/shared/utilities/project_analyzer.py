'''
Project analytizer class
'''
import simplejson

from collections import defaultdict
from datetime import datetime
from typing import Dict
from typing import List
from typing import Tuple

import numpy as np
import pandas as pd

from shared.constants import KEYS
from shared.constants import RecordedError
from shared.constants import UKEYS
from shared.communication import SingletonLogger
from shared.entities.project_settings import ProjectSettings
from shared.entities.table_data import TableData
from shared.entities.table_header import TableHeader
from shared.utilities.db_table_processor import clean_all_numeric_cols
from shared.utilities.price_component_utils import price_list_from_structure

def to_dict_records(df):
    columns = df.columns.tolist()
    result = [dict(zip(columns, row)) for row in df.itertuples(index=False, name=None)]
    return result

slogger = SingletonLogger()

class ProjectAnalyzer:
    def __init__(self):
        '''
        This is the constructor for the project analyzer class.

        Args:

        '''

        pass


    def _filters_list(self, offers_df, filter_columns):
        """
        Construct a list of filters.

        format:
        [
            {
                "name": <filter column name>,
                "values": <values for filter>
            },
            ...
        ]
        """
        filters = [{'name': filter_name, 'values': list(offers_df[filter_name].drop_duplicates())} for filter_name in filter_columns]
        return filters


    def _item_types(self, offers_df, item_type_key):
        """
        Create a list of the item types.
        """
        return list(offers_df[item_type_key].drop_duplicates())


    def _price_components_list(self, price_components_definition):
        """
        extract the names of all the price components from the definition
        and return them in a simple list
        """
        # there are three possible structures for price_components_definition:
        #   - empty list
        #   - list with simple string values
        #   - list with dictionaries: {"name": <name">, "parent": <parent_name>}
        if len(price_components_definition) == 0:
            return [UKEYS.TOTAL_PRICE]
        if isinstance(price_components_definition[0], str):
            return price_components_definition + [UKEYS.TOTAL_PRICE]
        return [price_comp['name'] for price_comp in price_components_definition]


    def _filters_map(self, offers_df, filter_columns, supplier_key):
        """
        Create a mapping of all the filter values:
        [
            {
                <filter_name>: <filter value>,
                ...
            }
        ]
        """
        cols = filter_columns
        cols.append(supplier_key)
        filter_map_df = offers_df[cols].drop_duplicates()
        return to_dict_records(filter_map_df)


    def empty_project_info(self):
        '''
        Return empty project info dictionary
        '''

        # Create empty parameters
        params = {}

        return params


    def project_info(self,
                     offers: TableData,
                     baseline_offers: TableData,
                     demands: TableData,
                     project_settings: ProjectSettings):
        '''
        Given the offers matrix, return a dictionary containing the main infos of a project
        '''
        start_time = datetime.now()

        offers_df = offers.data.copy()
        header = offers.header

        demands_df = demands.data.copy()
        demands_header = demands.header

        # Create item and suppliers list
        items_list = list(demands_df[demands_header[UKEYS.ITEM_ID]].drop_duplicates())
        suppliers_list = list(offers_df[header[UKEYS.SUPPLIER]].drop_duplicates())

        filter_uuids = project_settings.filters
        filter_names = [demands_header.byid[col_id].original_name for col_id in filter_uuids]

        # Copy filters to offers
        offers_df.set_index(header[UKEYS.DEMAND_ID], inplace=True)
        demands_df.set_index(demands_header[UKEYS.DEMAND_ID], inplace=True)
        for uuid, name in zip(filter_uuids, filter_names):
            offers_df[name] = demands_df[uuid]
        offers_df.reset_index(inplace=True)
        demands_df.reset_index(inplace=True)

        item_key = 'item_id'
        # Make sure that in the output there are again the names as defined in constants.py and user defined filters
        renamer = {}
        renamer[header[UKEYS.SUPPLIER]] = UKEYS.SUPPLIER
        renamer[header[UKEYS.ITEM_ID]] = item_key
        offers_df = offers_df.rename(columns=renamer)

        # get filters.
        filters = self._filters_list(offers_df, filter_names)

        # get item types
        if UKEYS.ITEM_CATEGORY not in header.types:
            item_types = ["default"]
        else:
            item_types = self._item_types(offers_df, header[UKEYS.ITEM_CATEGORY])

        # get price components
        price_comp_uuids = header[KEYS.PRICE]

        # get price component structure
        if project_settings.price_components_structure is not None:
            price_components_structure = project_settings.price_components_structure
        else:
            raise RecordedError("There is no price component structure present:"
                                " make sure to finish the data upload completely")

        price_components = price_list_from_structure(price_components_structure)
        # make sure that Total Price is first in list
        price_components.insert(0, price_components.pop(price_components.index('Total Price')))

        criteria = [header.byid[col_id].original_name for col_id in project_settings.soft_constraints]
        custom_columns = [header.byid[col_id].original_name for col_id in project_settings.custom_columns]
        try:
            capacities = [header.byid[col_id].original_name for col_id in header[KEYS.CAPACITY]]
        except KeyError:
            capacities = []
        second_axis = [header.byid[col_id].original_name for col_id in project_settings.second_axis]


        volumes = [header.byid[col_id].original_name for col_id in header[KEYS.VOLUME]]
        if not volumes:
            volumes = [demands_header.byid[col_id].original_name for col_id in demands_header[KEYS.VOLUME]]

        # try:
        #     volumes = header[KEYS.VOLUME]
        # except KeyError:
        #     volumes = []
        # volumes = [header.byid[col_id].original_name for col_id in volumes]
        # if len(volumes) == 0:
        #     volumes = [header[UKEYS.TOTAL_VOLUME]]
            # volumes = [demands_header.byid[col_id].original_name for col_id in demands_header[KEYS.VOLUME]]

        filters_map = self._filters_map(offers_df, filter_names, UKEYS.SUPPLIER)

        # adding flags for baseline and incumbent
        has_historic_prices = baseline_offers is not None and not baseline_offers.data.empty
        if has_historic_prices:
            baseline_suppliers_list = list(baseline_offers.data[header[UKEYS.SUPPLIER]].drop_duplicates())
            has_incumbent_prices = bool(set(baseline_suppliers_list) & set(suppliers_list))
        else:
            has_incumbent_prices = False

        # Collect parameters
        params = {
            'items': items_list,
            'num_items': len(items_list),
            'suppliers': suppliers_list,
            'num_suppliers': len(suppliers_list),
            'filters': filters,
            'criteria': criteria,
            'volumes': volumes,
            'price_components': price_components,
            'price_components_structure': price_components_structure,
            'item_types': item_types,
            'second_axis': second_axis,
            'custom_columns': custom_columns,
            'filters_map': filters_map,
            'capacities': capacities,
            'has_historic_prices': has_historic_prices,
            'has_incumbent_prices': has_incumbent_prices
        }
        self._report_and_update_time('Calculating project info', start_time)
        return params


    @staticmethod
    def _add_min_max_prices(dataframe, identifier_list, price_components):
        """
        For each price component <comp> add two new columns:
        - label of first column: (<comp>, 'min')
        - contents of first column: the minimum of <comp> in the groups
            specified by identifier_list
        - label of second column: (<comp>, 'max')
        - contents of second column: the maximum of <comp> in the groups
            specified by identifier_list
        """
        df = dataframe[identifier_list + price_components]
        grouped = df.groupby(identifier_list)
        dataframe = dataframe.set_index(identifier_list)
        dataframe[[f"{comp}_min" for comp in price_components]] = grouped.aggregate(np.min)[price_components]
        dataframe[[f"{comp}_max" for comp in price_components]] = grouped.aggregate(np.max)[price_components]
        return dataframe.reset_index()


    @staticmethod
    def _add_price_deviations(dataframe, price_components):
        """
        For each price component <comp> add a new column:
        - label of column: (<comp>, 'deviation')
        - contents of column: the deviation of <comp> to
            (<comp>, 'min')

        Note: throws KeyError if (<comp>, 'min') is not present in
            the given dataframe
        """
        renamer = {f"{comp}_min":comp for comp in price_components}
        mins = dataframe[[f"{comp}_min" for comp in price_components]].rename(columns=renamer)
        dataframe[[f"{comp}_deviation" for comp in price_components]] = np.round(dataframe[price_components].sub(mins).div(mins), 2)
        return dataframe


    @staticmethod
    def _add_dict_column(heatmap, dataframe_source, identifier_list, source_column_list, heatmap_column_name):
        """
        Add a new column to heatmap:
        - label of column: heatmap_column_name
        - contents of column: a dictionary of source_column_list

        The list of identifier columns should uniquely identify an offer
        """
        if not isinstance(identifier_list, list):
            identifier_list = [identifier_list]

        source_df = dataframe_source
        source_df[heatmap_column_name] = to_dict_records(source_df[source_column_list])
        heatmap = heatmap.merge(source_df[[heatmap_column_name] + identifier_list],
                                how='left',
                                on=identifier_list)

        return heatmap


    @staticmethod
    def _add_prices_dict(heatmap, dataframe_prices, identifier_list, price_components):
        """
        Add a new column to heatmap:
        - label of column: 'prices'
        - contents of column: a dictionary of the price components

        The list of identifier columns should uniquely identify an offer
        """
        return ProjectAnalyzer._add_dict_column(heatmap, dataframe_prices, identifier_list, price_components, 'prices')


    @staticmethod
    def _add_deviations_dict(heatmap, dataframe_deviations, identifier_list, price_components):
        """
        Add a new column to heatmap:
        - label of column: 'deviations'
        - contents of column: a dictionary of the deviations of price components

        The list of identifier columns should uniquely identify an offer
        """
        # Make sure that the two dataframes have the same index.
        heatmap = heatmap.set_index(identifier_list)
        deviations_df = dataframe_deviations.set_index(identifier_list)
        # heatmap['deviations'] = deviations_df.apply(lambda row: {price_comp: row[f"{price_comp}_deviation"] for price_comp in price_components}, axis=1)
        deviations_df = deviations_df[[col for col in deviations_df.columns if col.endswith('deviation')]]
        deviations_df.columns = [col.replace('_deviation', '') for col in deviations_df.columns]

        heatmap['deviations'] = to_dict_records(deviations_df)

        return heatmap.reset_index()


    @staticmethod
    def _add_filters_dict(heatmap, dataframe_filters, identifier_list, filter_list):
        """
        Add a new column to heatmap:
        - label of column: 'filters'
        - contents of column: a dictionary of the filters for the offer

        The list of identifier columns should uniquely identify an offer
        """
        return ProjectAnalyzer._add_dict_column(heatmap, dataframe_filters, identifier_list, filter_list, 'filters')


    @staticmethod
    def _add_suppliers_columns_dict(heatmap, dataframe_cols, identifier_list, dict_key, columns_to_add, item_key):
        """
        Args:
            heatmap: calculated heatmap so far
            dataframe_cols: dataframe to take the data from
            identifier_list: key level
            dict_key: label on new column
            columns_to_add: values to add to new column
            item_key: identifier

        Add a new column to heatmap:
        - label of column: dict key
        - contents of column: a dictionary of the custom columns

        If custom_columns is the empty list the value for each row
        is an empty dictionary

        The list of identifier columns should uniquely identify an offer
        """
        if len(columns_to_add) == 0:
            heatmap[dict_key] = heatmap[item_key].apply(lambda x: {})
            return heatmap
        return ProjectAnalyzer._add_dict_column(heatmap, dataframe_cols, identifier_list, columns_to_add, dict_key)


    @staticmethod
    def _add_volumes_dict(heatmap, dataframe_volumes, identifier_list, volumes):
        """
        Add a new column to heatmap:
        - label of column: 'volumes'
        - contents of column: a dictionary of the volumes

        The list of identifier columns should uniquely identify an offer
        """
        return ProjectAnalyzer._add_dict_column(heatmap, dataframe_volumes, identifier_list, volumes, 'volumes')


    @staticmethod
    def _add_suppliers_dict(item_df, source_df, item_key, category_key, supplier_key):
        """
        Add a new column to item_df:
        - label of column: 'suppliers'
        - contents of column: a dictionary of the offers per supplier

        The list of identifier columns should uniquely identify an item
        """
        item_df = item_df.set_index(item_key)
        source_df = source_df.set_index([item_key, supplier_key, category_key])
        result = defaultdict(lambda: defaultdict(dict))
        tuple_to_record = source_df.to_dict(orient='index')
        for key, item in tuple_to_record.items():
            result[key[0]][key[1]][key[2]] = item
        item_df['suppliers'] = pd.Series(result)

        return item_df.reset_index()


    @staticmethod
    def _add_baseline_dict(item_df, baseline_df, item_key, price_components, second_axis):
        """
        TODO
        """
        historic_df = baseline_df[item_key].to_frame()
        historic_df = ProjectAnalyzer._add_prices_dict(historic_df, baseline_df, item_key, price_components)
        historic_df = ProjectAnalyzer._add_suppliers_columns_dict(historic_df, baseline_df, item_key, 'second_axis', second_axis, item_key)
        historic_df['supplier'] = baseline_df[UKEYS.SUPPLIER]
        
        # add dict to item_df
        cols_to_add = list(historic_df.columns)
        cols_to_add.remove(item_key)

        return ProjectAnalyzer._add_dict_column(item_df, historic_df, [item_key], cols_to_add, 'historic_suppliers')


    @staticmethod
    def _add_incumbent_dict(item_df, baseline_df, helper_df, item_key, price_components, second_axis):
        """
        TODO
        """
        # Get offers of incumbent suppliers
        baseline_item_supplier_combos = baseline_df[item_key].astype(str) + baseline_df[UKEYS.SUPPLIER].astype(str)
        offer_item_supplier_combos = helper_df[item_key].astype(str) + helper_df[UKEYS.SUPPLIER].astype(str)
        mask = (offer_item_supplier_combos).isin(baseline_item_supplier_combos)
        mask2 = (baseline_item_supplier_combos).isin(offer_item_supplier_combos)

        # Keep only cheapest incumbent if there are multiple ones
        helper_df = helper_df[mask].sort_values(UKEYS.TOTAL_PRICE, ascending=True).drop_duplicates(item_key)
        incumbent_df = helper_df[[item_key, UKEYS.SUPPLIER]]
        incumbent_df = ProjectAnalyzer._add_prices_dict(incumbent_df, helper_df, item_key, price_components)
        incumbent_df = ProjectAnalyzer._add_suppliers_columns_dict(incumbent_df, helper_df, item_key, 'second_axis', second_axis, item_key)
        incumbent_df['supplier'] = helper_df[UKEYS.SUPPLIER]

        # add dict to item_df and return it
        cols_to_add = list(incumbent_df.columns)
        cols_to_add.remove(item_key)

        return ProjectAnalyzer._add_dict_column(item_df, incumbent_df, item_key, cols_to_add, 'incumbent_suppliers')


    @staticmethod
    def _get_min_max_prices(dataframe, identifier_list, price_components):
        """
        For each price component <comp> create two columns:
        - label of first column: (<comp>, 'min')
        - contents of first column: the minimum of <comp> in the groups
            specified by identifier_list
        - label of second column: (<comp>, 'max')
        - contents of second column: the maximum of <comp> in the groups
            specified by identifier_list
        """
        df = dataframe[identifier_list + price_components]
        grouped = df.groupby(identifier_list)
        result = pd.DataFrame(data=dataframe[identifier_list].drop_duplicates(), columns=identifier_list)
        result = result.set_index(identifier_list)
        result[[f"{comp}_min" for comp in price_components]] = grouped.aggregate(np.min)[price_components]
        result[[f"{comp}_max" for comp in price_components]] = grouped.aggregate(np.max)[price_components]
        return result.reset_index()


    @staticmethod
    def _add_total_deviations(dataframe, price_components):
        """
        Add for each price component <comp> a new column:
        - label of column: '<comp>_deviation')
        - contents of column: the total deviation of <comp>:
            (row[(<comp>, 'max')] - row[(<comp>, 'min')])/row[(<comp>, 'min')]
        """
        mins = dataframe[[f"{comp}_min" for comp in price_components]].rename(columns={f"{comp}_min":comp for comp in price_components})
        dataframe = dataframe.rename(columns={f"{comp}_max": comp for comp in price_components})
        dataframe[[f"{comp}_deviation" for comp in price_components]] = np.round(dataframe[price_components].sub(mins).div(mins), 2)
        return dataframe


    @staticmethod
    def _add_best_prices_dict(item_df, source_df, identifier_list, price_components):
        """
        Add a new column to item_df:
        - label of column: 'best_prices'
        - contents of column: a dictionary of the best prices
            for each price component

        The list of identifier columns should uniquely identify an item
        """

        # get lowest prices for each item
        source_df = source_df.sort_values(UKEYS.TOTAL_PRICE, ascending=True).drop_duplicates(identifier_list)

        # add best prices column
        item_df = ProjectAnalyzer._add_dict_column(item_df, source_df, identifier_list, price_components, 'best_prices')

        # add best price supplier
        item_df = item_df.set_index(identifier_list)
        source_df = source_df.set_index(identifier_list)
        item_df['best_price_supplier'] = source_df[UKEYS.SUPPLIER]

        return item_df.reset_index()

    def price_heatmap(self,
                      offers: TableData,
                      baseline_offers: TableData,
                      demands: TableData,
                      project_settings: ProjectSettings):
        """
        Calculate the price heatmap from the offers and demands tables.

        INVARIANTS:
            - a dataframe passed to a helper function CANNOT have an index
            - a dataframe received from a helper function DOES NOT have an index
        """
        # TODO: check that offers don't have an index?
        start_time = datetime.now()

        header = offers.header
        demands_header = demands.header
        item_key = 'item_id'

        price_components = [header.byid[col_id].original_name for col_id in header[KEYS.PRICE]]
        custom_columns = [header.byid[col_id].original_name for col_id in project_settings.custom_columns]
        second_axis = [header.byid[col_id].original_name for col_id in project_settings.second_axis]
        volumes = [header.byid[col_id].original_name for col_id in header[KEYS.VOLUME]]
        filter_ids = project_settings.filters
        filters = [demands_header.byid[filter_id].original_name for filter_id in filter_ids]

        if len(volumes) == 0:
            
            volumes = [demands_header.byid[col_id].original_name for col_id in demands_header[KEYS.VOLUME]]
            slogger.verify(len(volumes) >= 1, \
                 "Empty volumes in the price heatmap calculation")
            for volume in volumes:
                header.add_column(KEYS.VOLUME, original_name=volume)
                offers[header.byname[volume].id] = demands[demands_header.byname[volume].id]

        frames = self._get_helper_dataframes(offers,
                                             baseline_offers,
                                             demands,
                                             price_components,
                                             filters,
                                             filter_ids,
                                             custom_columns,
                                             second_axis,
                                             volumes)

        helper_df = frames[0]
        baseline_df = frames[1]
        demands_df = frames[2]

        # Add the total unit price separately
        price_components.append(UKEYS.TOTAL_PRICE)

        helper_df = self._add_min_max_prices(helper_df, [item_key], price_components)
        helper_df = self._add_price_deviations(helper_df, price_components)

        start = self._report_and_update_time('calculating min and deviations for helper', start_time)

        # resulting dictionary will be built with three key levels: item, supplier, category
        keys = [item_key, UKEYS.ITEM_CATEGORY, UKEYS.SUPPLIER]

        # adding supplier-level information
        heatmap_df = helper_df[keys]
        heatmap_df = self._add_prices_dict(heatmap_df, helper_df, keys, price_components)
        heatmap_df = self._add_deviations_dict(heatmap_df, helper_df, keys, price_components)
        heatmap_df = self._add_filters_dict(heatmap_df, helper_df, keys, filters)
        heatmap_df = self._add_suppliers_columns_dict(heatmap_df, helper_df, keys, 'custom_columns', custom_columns, item_key)
        heatmap_df = self._add_suppliers_columns_dict(heatmap_df, helper_df, keys, 'second_axis', second_axis, item_key)
        heatmap_df = self._add_volumes_dict(heatmap_df, helper_df, keys, volumes)

        start = self._report_and_update_time('adding supplier_level dicts', start)

        item_min_max_frame = self._get_min_max_prices(helper_df, [item_key], price_components)
        item_level_prices = self._add_total_deviations(item_min_max_frame, price_components)
        item_filter_df = helper_df.groupby(item_key).first().reset_index()

        start = self._report_and_update_time('calculating min and deviations for item level', start)
        
        item_df = pd.DataFrame(data=helper_df[item_key].drop_duplicates(), columns=[item_key])
        item_df = self._add_suppliers_dict(item_df, heatmap_df, item_key, UKEYS.ITEM_CATEGORY, UKEYS.SUPPLIER)
        item_df = self._add_best_prices_dict(item_df, helper_df, item_key, price_components)
        item_df = self._add_deviations_dict(item_df, item_level_prices, item_key, price_components)
        item_df = self._add_filters_dict(item_df, item_filter_df, item_key, filters)
        item_df = self._add_volumes_dict(item_df, demands_df, item_key, volumes)

        if baseline_df is not None:
            # prepare historic_df dict
            item_df = self._add_baseline_dict(item_df, baseline_df, item_key, price_components, second_axis)

            # prepare incumbent dict
            item_df = self._add_incumbent_dict(item_df, baseline_df, helper_df, item_key, price_components, second_axis)

        else:
            item_df['historic_prices'] = None

        self._report_and_update_time('adding item levels dicts', start)
        self._report_and_update_time('calculating heatmap', start_time)

        return simplejson.loads(simplejson.dumps(to_dict_records(item_df), ignore_nan=True))
        
        
    @staticmethod
    def _get_helper_dataframes(offers: TableData,
                               baseline_offers: TableData,
                               demands: TableData,
                               price_components: List[str],
                               filters: List[str],
                               filter_ids: List[str],
                               custom_columns: List[str],
                               second_axis: List[str],
                               volumes: List[str]) -> Tuple[pd.DataFrame, ...]:
        """
        Prepare the data from the offers and demands tables

        Args:
            offers
            baseline_offers
            demands
            price_components

        Returns:
            offers_df
            baseline_df
            demands_df
        """
        # clean offers and demands
        offers = clean_all_numeric_cols(offers)
        demands = clean_all_numeric_cols(demands)

        offers_df = offers.data
        demands_df = demands.data
        offers_header = offers.header
        demands_header = demands.header

        item_key = 'item_id'

        # make sure the offers_df has a category column
        if UKEYS.ITEM_CATEGORY not in offers_header.types:
            offers_df[UKEYS.ITEM_CATEGORY] = 'default'
        else:
            offers_df = offers_df.rename(columns={offers_header[UKEYS.ITEM_CATEGORY]: UKEYS.ITEM_CATEGORY})

        # Copy demands filters to offers:
        offers_df.set_index(offers_header[UKEYS.DEMAND_ID], inplace=True)
        demands_df.set_index(demands_header[UKEYS.DEMAND_ID], inplace=True)
        for filtr, filtr_id in zip(filters, filter_ids):
            offers_df[filtr] = demands_df[filtr_id]
        offers_df.reset_index(inplace=True)
        demands_df.reset_index(inplace=True)

        # make sure there are no duplicate column names 
        offers_cols = list((set(price_components) | set(custom_columns) | set(second_axis) | set(volumes)) - set(filters))
        offers_renamer = ProjectAnalyzer._build_renamer(offers_cols, offers_header)
        offers_renamer[offers_header[UKEYS.SUPPLIER]] = UKEYS.SUPPLIER
        offers_renamer[offers_header[UKEYS.ITEM_ID]] = item_key
        
        # Note: now special treatment of the total price
        # When switching to using the actually computed total price
        # remove this line
        offers_renamer[offers_header[UKEYS.NORMALIZED_PRICE]] = UKEYS.TOTAL_PRICE
        offers_df = offers_df.rename(columns=offers_renamer)

        # clean offers from zeros and remove offers with price zero
        offers_df[price_components] = offers_df[price_components].replace(0, np.nan)
        mask = offers_df[UKEYS.TOTAL_PRICE] != 0
        offers_df = offers_df[mask]

        demands_renamer = ProjectAnalyzer._build_renamer(volumes, demands_header)
        demands_renamer[demands_header[UKEYS.ITEM_ID]] = item_key
        demands_df = demands_df.rename(columns=demands_renamer)

        baseline_df = None
        if baseline_offers is not None and not baseline_offers.data.empty:
            baseline_df = baseline_offers.data.copy()
            baseline_header = baseline_offers.header

            # make sure there are no duplicate column names when getting baseline offers in
            baseline_cols = list(set(price_components) | set(second_axis))
            baseline_renamer = ProjectAnalyzer._build_renamer(baseline_cols, baseline_header)
            baseline_renamer[baseline_header[UKEYS.SUPPLIER]] = UKEYS.SUPPLIER
            baseline_renamer[baseline_header[UKEYS.ITEM_ID]] = item_key
            # Same note as above with normal offers!
            baseline_renamer[baseline_header[UKEYS.NORMALIZED_PRICE]] = UKEYS.TOTAL_PRICE
            baseline_df = baseline_df.rename(columns=baseline_renamer)
            baseline_df[price_components] = baseline_df[price_components].replace(0, np.nan)
            
            # clean offers from zeros and remove offers with price zero
            mask = baseline_df[UKEYS.TOTAL_PRICE] != 0
            baseline_df = baseline_df[mask]

        return offers_df, baseline_df, demands_df


    @staticmethod
    def _build_renamer(column_names: List[str], header: TableHeader) -> Dict[str, str]:
        renamer = {}
        for col in column_names:
            renamer[header.byname[col].id] = col
            # renamer[header.get_dic_from_name(col)['id']] = col
        return renamer


    def _report_and_update_time(self, processing_step: str, start_time: datetime) -> datetime:
        """
        Report how long the processing step took that started at start_time
        and return the current time
        """
        duration = str((datetime.now() - start_time).total_seconds())
        slogger.info(f"{processing_step} took {duration} seconds")
        return datetime.now()


