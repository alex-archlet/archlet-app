"""
Module containing utilities regarding computation of kpis.
"""
from typing import Dict
from typing import Tuple

from shared.constants import KEYS
from shared.entities import TableData
from shared.entities import TableHeader

# typing aliases:
Str_Dict = Dict[str, str]
SUMMARY_KPIS = ['number_of_changes', 'items_allocated', 'winning_bidders']


def create_kpi_frame(allocations: TableData):
    """
    Create an empty kpi table with the allocation ids as only column.
    """
    kpi_frame = pd.DataFrame(data={'id': allocations[header[UKEYS.ALLOCATION_ID]]})
    return kpi_frame

def get_default_calculation(default_kpi_name):
    """
    Return the internal string representation of the calculation of the given default name.
    """
    pass

def prepare_kpi_definitions(default_kpis: Str_Dict,
                            custom_kpis: Str_Dict) -> Tuple[Str_Dict, Str_Dict]:
    """
    Split the dictionaries of default and custom kpi definitions
    in a dictionary of allocation level kpis and a dictionary
    of summary kpis.

    Returns:
        Tuple(Dict(<kpi_uuid> -> <kpi_definition>), Dict(<kpi_name> -> <kpi_uuid>))
    """
    kpi_defs = custom_kpis
    sum_kpi_defs = {}
    for kpi_id, kpi_name in default_kpis.items():
        if kpi_name in SUMMARY_KPIS:
            sum_kpi_defs[kpi_name] = kpi_id
        else:
            kpi_defs[kpi_id] = kpi_name

    return kpi_defs, sum_kpi_defs


def create_user_renamer(header: TableHeader, table_name: str) -> Str_Dict:
    """
    Create a renamer for the given table.
    """
    prefix = table_name[0]
    renamer = {f"{table_name}.'{col.type}'": f"{prefix}{col.id}" for col in header.columns}
    renamer.update({f"{table_name}.'{col.original_name}'": f"{prefix}{col.id}" for col in header.columns})
    return renamer


def create_uuid_renamer(header: TableHeader, table_name: str):
    """
    Create a renamer for the given table

    Returns:
        Dict(prefix<col_uuid> -> prefix<col_uuid_underscore>)
    """
    prefix = table_name[0]
    renamer = {f"{prefix}{col.id}": f"{prefix}{col.id.replace('-','_')}" for col in header.columns}
    return renamer


def create_uuid_table_renamer(header: TableHeader, table_name: str) -> Str_Dict:
    """
    Create a renamer for the given table.
    Returns:
        Dict(<col_uuid> -> prefix<col_uuid_underscore>)
    """
    prefix = table_name[0]
    renamer = {col.id: f"{prefix}{col.id.replace('-','_')}" for col in header.columns}
    return renamer


def contains_baseline(expression: str) -> bool:
    """
    Check whether the given expression contains an element from the baseline table.
    """
    expression_elements = expression.split(' ')
    for element in expression_elements:
        if element[0] == 'b':
            return True
    return False
