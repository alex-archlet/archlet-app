from __future__ import annotations
"""
Module that contains utility to check for a cycle in a directed graph

A directed graph is represented by its adjacency list, a dictionary of the following form:
<node 1 name>: [<name of neighbor 1>, <name of neighbor 2>, ...],
<node 2 name>: ...
"""

def reverse_topo_sort(digraph: Dict[str, List[str]]) -> List[str]:
    """
    Find a reverse topological sorting, or a cycle of the given digraph.

    A topological sorting of a DAG is any ordering of the vertices of the graph,
    such that for each edge u -> v, u appears before v in the ordering.

    The distinction in the output for a cycle or sorting is made by checking whether
    the first and last node in the output are the same.

    The sorting that is returned doesn't contain the leaves.
    """
    ordering = []
    visited = {}
    recursion_stack = set()
    for node in digraph:
        if node not in visited:
            result = dfs_reverse_topo_sort(node,
                                           None,
                                           digraph,
                                           visited,
                                           recursion_stack,
                                           ordering)
            if result:
                return result
    return ordering


def dfs_reverse_topo_sort(current_node,
                          current_parent,
                          digraph,
                          visited,
                          recursion_stack,
                          ordering) -> Union[list[str], bool]:
    """
    Depth first search traversal to find a reverse topological sorting.

    Args
        current_node: the node that is currently being explored
        current_parent: a parent of the node that is being explored
        digraph: the graph that we are traversing
        visited: a dictionary of nodes that are already visited with a parent as value
        recursion_stack: the nodes that are currently active in the recursion
        ordering: list of the current reverse topological ordering

    Returns:
        Any cycle when there is one in the digraph reachable from the current node
        False otherwise
    """
    if current_node in recursion_stack:
        # We are visiting a node in the recursion stack: found a cycle
        cycle = [current_node]
        parent = current_parent
        while parent != current_node:
            cycle.append(parent)
            parent = visited[parent]
        cycle.append(parent)
        return cycle

    if current_node in visited:
        # a previously visited node not in the recursion stack: no cycle here
        return False

    # Mark current node
    visited[current_node] = current_parent

    if current_node not in digraph:
        # the current node is a leaf node:
        return False

    # Add current node to recursion stack
    recursion_stack.add(current_node)

    result = False
    # visit all neighbors
    for neighbor in digraph[current_node]:
        result = dfs_reverse_topo_sort(neighbor,
                                       current_node,
                                       digraph,
                                       visited,
                                       recursion_stack,
                                       ordering)
        if result:
            break

    # Recursion ended: remove the current node
    recursion_stack.remove(current_node)

    # Append at the end of the ordering
    ordering.append(current_node)
    return result


def contains_cycle(digraph: Dict[str, List[str]]) -> Union[list[str], bool]:
    """
    Check if the given directed graph contains a cycle.
    """
    visited = {}
    recursion_stack = set()
    result = False
    for node in digraph:
        if node not in visited:
            result = dfs_cycle(node, None, digraph, visited, recursion_stack)
            if result:
                break
    return result


def dfs_cycle(current_node: str,
              current_parent: str,
              digraph: dict[str, list[str]],
              visited: dict[str],
              recursion_stack: set[str]) -> Union[list[str], bool]:
    """
    Depth first search traversal of a digraph, that checks for a cycle.
    When a node is being explored that is already visited, we found a cycle.

    Args
        current_node: the node that is currently being explored
        current_parent: a parent of the node that is being explored
        digraph: the graph that we are traversing
        visited: a dictionary of nodes that are already visited with a parent as value
        recursion_stack: the nodes that are currently active in the recursion

    Returns:
        Any cycle when there is one in the digraph reachable from the current node
        False otherwise
    """
    if current_node in recursion_stack:
        cycle = [current_node]
        parent = current_parent
        while parent != current_node:
            cycle.append(parent)
            parent = visited[parent]
        cycle.append(parent)
        return cycle
    if current_node in visited:
        return False

    visited[current_node] = current_parent
    if current_node not in digraph:
        return False

    recursion_stack.add(current_node)

    result = False
    for node in digraph[current_node]:
        result = dfs_cycle(node, current_node, digraph, visited, recursion_stack)
        if result:
            break
    recursion_stack.remove(current_node)
    return result
