"""
Profiling module. Contains a context manager to execute profiling on a block of code.
"""
import cProfile
import io
import pstats
import contextlib

@contextlib.contextmanager
def profiled():
    """
    Profiling context manager
    """
    profiler = cProfile.Profile()
    profiler.enable()
    yield
    profiler.disable()
    stream = io.StringIO()
    stats = pstats.Stats(profiler, stream=stream).sort_stats('cumulative')
    stats.print_stats()
    # uncomment this to see who's calling what
    # ps.print_callers()
    print(stream.getvalue())
