from __future__ import annotations
from typing import TYPE_CHECKING

import pandas as pd

from shared.constants import UKEYS, KEYS
from shared.constants import RecordedError
from shared.communication import SingletonLogger

if TYPE_CHECKING:
    from shared.entities.table_data import TableData
    from shared.entities.table_header import TableHeader


slogger = SingletonLogger()


def uuid_name_to_bid_column_id(column: str) -> str:
    """
    Switch single internal uuid column name to its matching bid column id
    """
    return column.replace('_', '-').replace('l', '')


def create_renamers(table: TableData):
    table_header = table.header
    table_renamer = {col.id: 'l' + col.id.replace('-', '_') for col in table_header.columns}
    expression_renamer = {col.id.replace('-', '_'): 'l' + col.id.replace('-', '_') for col in table_header.columns}
    return table_renamer, expression_renamer


def join_offers_demands(offers: pd.DataFrame, demands: pd.DataFrame, left_on: str, right_on: str) -> pd.DataFrame:
    """
    Join the offers and demands tables on the given columns.

    Returns:
        a table data object with the columns of demands and offers and no header.
    """
    offers_data = offers.set_index(left_on)
    demands_data = demands.set_index(right_on)

    table_data = offers_data.join(demands_data)

    return table_data


def add_item_ids(offers: TableData, demands: TableData) -> TableData:
    """
    Add the item id to the offers

    Args:
        offers table
        demands table

    Return:
        modified offers table
    """
    offers_header = offers.header
    demands_header = demands.header

    item_id_present = UKEYS.ITEM_ID in offers_header.types
    item_id_missing = UKEYS.ITEM_ID not in demands_header.types
    if (item_id_present or item_id_missing):
        slogger.error(f"Cannot add item id to offers."\
                      f"Already present in offers: {item_id_present}."\
                      f" Missing in demands: {item_id_missing}")
        raise RecordedError

    offers.data.set_index(offers_header[UKEYS.DEMAND_ID], inplace=True)
    demands.data.set_index(demands_header[UKEYS.DEMAND_ID], inplace=True)

    offers_header.add_column(UKEYS.ITEM_ID, attribute=False)
    offers[offers_header[UKEYS.ITEM_ID]] = demands[demands_header[UKEYS.ITEM_ID]]

    offers.data.reset_index(inplace=True)
    demands.data.reset_index(inplace=True)

    return offers


def clean_numeric_cols(table: TableData,
                       cols: list,
                       generate_warning=False) -> TableData:
    """
    Cleans numeric columns of table data object

    Args:
        table: TableData object
        cols: columns to be cleaned
        generate_warning: optional boolean if warnings are generated

    Returns:
        cleaned TableData object
    """
    table_header = table.header

    for col_id in cols:
        # generate warning for strings that are changed to zeros
        if generate_warning:
            # generate warmings only for true strings and not NaNs
            table[col_id] = table[col_id].fillna(0)
            mask = pd.to_numeric(table[col_id], errors='coerce').notnull()
            if not mask.all():
                # get bid column
                bid_column_id = uuid_name_to_bid_column_id(col_id)
                # assuming price column comes from supplier
                column_name = table_header.byid[bid_column_id].original_name

                message = f'In total {(~mask).sum()} text values were changed to zero for column {column_name}.'
                slogger.warning(message)

        # change strings via NaN to 0
        table[col_id] = pd.to_numeric(table[col_id], errors='coerce')
        table[col_id] = table[col_id].fillna(0)

    return table



def clean_all_numeric_cols(table: TableData) -> TableData:
    """
    Clean TableData object from non-numeric values in numeric column
    """
    header = table.header
    numeric_cols = list(set(header[KEYS.PRICE]) |
                        set(header[KEYS.NUMBER]) |
                        set(header[KEYS.CAPACITY]))

    table = clean_numeric_cols(table, numeric_cols, False)

    return table
