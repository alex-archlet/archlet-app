""" module containing static utility function for anything relating to a table data instance """
from __future__ import annotations
import re

from typing import Dict
from typing import List
from typing import TYPE_CHECKING

from shared.constants import UKEYS, KEYS
from shared.utilities.db_table_processor import create_renamers
from shared.utilities.expression_handler import ExpressionHandler
from shared.communication import SingletonLogger

if TYPE_CHECKING:
    from shared.entities import TableHeader

ROOT_NAME = 'ROOT'

slogger = SingletonLogger()

def price_components_structure(price_components_definition):
    """
    Build the structured price components list from the given definition.

    Return:
        List of price components. Each price component has the
        following structure:
        {
            "name": <name of price comp>
            "children": [<list of children comps>]
        }
    """
    # there are three possible structures for price_components_definition:
    #   - empty list
    #   - list with simple string values
    #   - list with dictionaries: {"name": <name">, "parent": <parent_name>}
    if len(price_components_definition) == 0:
        return [{"name": UKEYS.TOTAL_PRICE, "children": []}]
    if isinstance(price_components_definition[0], str):
        return [{"name": UKEYS.TOTAL_PRICE,
                 "children": [{"name": comp_name, "children": []} for comp_name in price_components_definition]}]

    # Create all the necessary components:
    components = {comp['name']: {"name": comp['name'], "children": []} for comp in price_components_definition}

    # Create the hierarchical structure and add level zero components
    # to the result list
    result = []
    for comp in price_components_definition:
        if comp['parent'] == ROOT_NAME:
            result.append(components[comp['name']])
        else:
            components[comp['parent']]['children'].append(components[comp['name']])

    return result


def price_structure_from_adj_list(price_comp_adj_list: Dict[str, List[str]]) -> List[Dict[str, str]]:
    """
    Create price components structure from adj list
    """
    components = {comp: {"name": comp, "children": []} for comp in price_comp_adj_list}
    has_parent = set()
    for parent in price_comp_adj_list:
        has_parent |= set(price_comp_adj_list[parent])
        for child in price_comp_adj_list[parent]:
            components[parent]['children'].append(components[child])
    roots = set(price_comp_adj_list.keys()) - has_parent
    result = []
    for root in roots:
        result.append(components[root])
    return result

def create_mapping_prices_volumes(offers, demands, renamer: Dict[str, str]) -> dict:
    """
    Create price and volume mappings

    Args:
        offers: offers TableData object
        demands: demands TableData object

    Returns:
        dict with mappings for prices and volumes
    """
    offers_header = offers.header
    demands_header = demands.header

    mapping_prices = {renamer[price_id]: 'PRICE' for price_id in offers_header[KEYS.PRICE]}
    mapping_vols = {renamer[vol_id]: 'VOL' for vol_id in offers_header[KEYS.VOLUME]}

    mapping_prices.update({renamer[price_id]: 'PRICE' for price_id in demands_header[KEYS.PRICE]})
    mapping_vols.update({renamer[vol_id]: 'VOL' for vol_id in demands_header[KEYS.VOLUME]})

    return dict(mapping_prices, **mapping_vols)


def column_names_to_uuids(price_comp_struct: List[Dict[str, str]],
                          header: TableHeader) -> List[Dict[str, str]]:
    """
    Translate the column names to their corresponding uuids
    in the given price component structure.
    """
    for element in price_comp_struct:
        element['name'] = header[header.reverse_translate(element['name'])]
        element['children'] = column_names_to_uuids(element['children'], header)
    return price_comp_struct


def uuids_to_column_names(price_comp_adj_list: Dict[str, List[str]],
                           header: TableHeader,
                           reverse_renamer: Dict[str, str]) -> Dict[str, List[str]]:
    """
    Translate the uuids in the adjacency list to the corresponding column names
    """
    result = {}
    for key, value_list in price_comp_adj_list.items():
        translated_values = [header.byid[reverse_renamer[value]].original_name for value in value_list]
        result[header.byid[reverse_renamer[key]].original_name] = translated_values
    return result


def create_price_component_calculations(price_comp_structure: List[Dict[str, str]],
                                        calculations: Dict[str, str]) -> Dict[str, str]:
    """
    Extract from the structured price components the expressions
    to compute them

    Note: the assumption here is that a parent is the sum of its direct children.
    """
    for el in price_comp_structure:
        if len(el['children']) == 0:
            calculations[el['name']] = el['name']
        else:
            calculations = create_price_component_calculations(el['children'], calculations)
            calculations[el['name']] = ' + '.join([child['name'] for child in el['children']])
    return calculations


def is_proper_calc(name, calc):
    """
    Return whether the calculation is non trivial for the specified name.
    """
    return calc is not None and calc != [] and calc != [name]


def translate_calculations(calculations: Dict[str, str],
                           renamer: Dict[str, str]) -> Dict[str, str]:
    result = {}
    for col, expression in calculations.items():
        if expression is None or expression == '':
            result[col] = ''
        for key, value in renamer.items():
            expression = expression.replace(key, value)
        result[col] = expression
    return result


def expressions_to_handlers(calculations: Dict[str, str],
                            mapping_name_to_type: Dict[str, str]) -> Dict[str, ExpressionHandler]:
    """
    translate the expressions from the calculations dictionary
    and return ExpressionHandlers of these translations.
    """
    result = {}
    for col, expression in calculations.items():
        if expression is None or expression == '':
            result[col] = ''
            continue

        expression = 'y = ' + expression

        expr_handler = ExpressionHandler('table', expression, mapping_name_to_type)

        slogger.verify(expr_handler.is_assignment(), \
                "The expression is not an assignment")
        slogger.verify(expr_handler.has_valid_attribute_names(), \
                "The expression has invalid attribute names")
        
        result[col] = expr_handler

    return result

def extract_price_relations(expression_dict: Dict[str, ExpressionHandler]) -> Dict[str, List[str]]:
    """

    """
    result = {}
    for col, expr_handler in expression_dict.items():
        expr_handler.decompose_price_expression()
        prices = expr_handler.get_price_list()
        result[col] = prices
    return result


def price_list_from_structure(price_comp_struct: List[Dict[str, str]]) -> List[str]:
    """
    Create a list of price components from the given price components
    structure
    """
    # First add top level price components
    result = [price_comp['name'] for price_comp in price_comp_struct]
    for price_comp in price_comp_struct:
        result += price_list_from_structure(price_comp['children'])
    return result
