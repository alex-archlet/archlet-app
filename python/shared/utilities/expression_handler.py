'''
Python module to analyze text expressions
'''
from collections import deque
from typing import Dict
from typing import List

import ast
import copy
import sys
import types

from shared.communication import SingletonLogger

VOLUME_IDENTIFIER = 'VOL'
PRICE_IDENTIFIER = 'PRICE'

slogger = SingletonLogger()

class AssignmentCheckNodeVisitor(ast.NodeVisitor):
    """
    A visitor that checks if an AST node is an assignment

    Public methods:
        visit(node)
    """

    def __init__(self):
        self.is_assignment = False
        self._assignment_name = None

    @property
    def assignment_name(self) -> str:
        return self._assignment_name


    def visit_Module(self, node: ast.AST) -> bool:
        """Visit all the statements in the body and return
        the logical and of the results."""
        result = True
        for stmt in node.body:
            result &= self.visit(stmt)
        return result

    def visit_Assign(self, node: ast.AST) -> bool:
        """
        Visit an assignment node and return true if there is exactly
        one assignment.
        """
        self.is_assignment = True
        if len(node.targets) > 1:
            return False
        self.visit(node.targets[0])
        return True

    def visit_Name(self, node: ast.AST) -> bool:
        """
        visit a name node and set assignment name if this is an assignment.
        """
        if self.is_assignment:
            self._assignment_name = node.id
        return False

    def generic_visit(self, node: ast.AST) -> bool:
        """Visit any other node than assignment and return false."""
        return False


class AttributeNameCheckVisitor(ast.NodeVisitor):
    """
    A visitor that checks whether all the name nodes that appear in
    the syntax tree have names that occur in the given list.

    Public methods:
        visit(node)
    """

    def __init__(self, attribute_names: List[str]):
        """instantiate collection of attribute names"""
        self.attribute_names = attribute_names

    def visit_Name(self, node: ast.AST) -> bool:
        """Check if the id of the Name node is valid."""
        if node.id not in self.attribute_names:
            slogger.debug(f"False {node.id} name")
            return False
        return True

    def visit_Assign(self, node: ast.AST) -> bool:
        """Only visit the value part of an assign node"""
        return self.visit(node.value)

    def generic_visit(self, node: ast.AST) -> bool:
        """Visit all children and return logical and of the results"""
        result = True
        for child in ast.iter_child_nodes(node):
            result &= self.visit(child)
        return result


class NameConvertVisitor(ast.NodeTransformer):
    """
    A visitor that transforms the id of a name node such that it is in the form:
    table_name['node.id']

    Public methods:
        visit(node)
    """

    def __init__(self, table_name: str):
        """Initialize the table name to use."""
        self.table_name = table_name

    def visit_Assign(self, node: ast.AST) -> ast.AST:
        """Only visit the value part of an assign node"""
        value = self.visit(node.value)
        return ast.Assign(targets=node.targets, value=value)

    def visit_Name(self, node: ast.AST) -> ast.AST:
        """Update each name node to be a subscript node for the given table"""
        return ast.Subscript(value=ast.Name(id=self.table_name, ctx=node.ctx),
                             slice=ast.Index(value=ast.Constant(node.id)),
                             ctx=node.ctx)


class VolumeAndPriceVisitor(ast.NodeVisitor):
    """
    A visitor that constructs new AST's that can be used to calculate
    the total volume afterwards.

    Note: before getting the specific calculation trees,
    it is necessary to call the 'visit()' method on an AST expression

    Public methods:
        visit(node)
        get_volume_tree(total_volume_name)
        get_norm_price_tree(normalized_price_name)
        get_fixed_price_tree(fixed_price_name)
    """
    def __init__(self, attribute_mapping: Dict[str, str]):
        # two stacks:
        # - index 0 --> vol related expressions
        # - index 1 --> everything else
        self.attribute_mapping = attribute_mapping
        self.stacks = [deque(), deque()]
        self.main_stack = deque()
        self.vol_tree = None
        self.price_nodes = set()

    #######################################################
    # methods to retrieve the relevant trees
    #######################################################
    @staticmethod
    def _create_assign(target_name, value_tree: ast.AST) -> ast.AST:
        """Create an module with one assign node"""
        name = ast.Name(id=target_name, ctx=ast.Store())
        assign = ast.Assign(targets=[name], value=value_tree)
        # Necessary since AST module of python changes over time
        # (3.7 and 3.8 have slightly different syntax here)
        slogger.verify(sys.version_info[0] >= 3, "Python version is not 3")
        if sys.version_info[1] < 8:
            module = ast.Module(body=[assign])
        else:
            module = ast.Module(body=[assign], type_ignores=[])
        return module

    def get_volume_tree(self, total_volume_name: str) -> ast.AST:
        """
        return a correctly indented AST of the total volume calculation
        """
        return ast.fix_missing_locations(
            self._create_assign(total_volume_name, self.vol_tree))

    def get_norm_price_tree(self, normalized_price_name: str) -> ast.AST:
        """
        Return a correctly indented AST of the normalized price calculation
        """
        if len(self.stacks[0]) == 0:
            raise ValueError("The total cost calculation must include a volume column")
        complete_norm_price = ast.BinOp(left=self.stacks[0].pop(),
                                        op=ast.Div(),
                                        right=self.vol_tree)
        return ast.fix_missing_locations(
            self._create_assign(normalized_price_name, complete_norm_price))

    def get_fixed_price_tree(self, fixed_price_name: str) -> ast.AST:
        """
        Return a correctly indented AST of the fixed price calculation
        """
        node = ast.Constant(value=0) if len(self.stacks[1]) == 0 else self.stacks[1].pop()
        return ast.fix_missing_locations(
            self._create_assign(fixed_price_name, node))

    def get_price_nodes(self) -> List[str]:
        """
        Return a list of the node names with type PRICE.
        """
        return list(self.price_nodes)

    #######################################################
    # node visitor helpers
    #######################################################
    def _append_vol_stack(self, node: ast.AST) -> None:
        self.main_stack.append([0])
        self.stacks[0].append(node)

    def _append_const_stack(self, node: ast.AST) -> None:
        self.main_stack.append([1])
        self.stacks[1].append(node)

    def _combine_nodes(self, stack_id: int) -> None:
        right = self.stacks[stack_id].pop()
        left = self.stacks[stack_id].pop()
        self.stacks[stack_id].append(ast.BinOp(left=left, op=ast.Add(), right=right))

    def _negate_node(self, stack_id: int) -> None:
        node = self.stacks[stack_id].pop()
        self.stacks[stack_id].append(ast.UnaryOp(op=ast.USub(), operand=node))

    def _substract_nodes(self, stack_id: int) -> None:
        right = self.stacks[stack_id].pop()
        left = self.stacks[stack_id].pop()
        self.stacks[stack_id].append(ast.BinOp(left=left, op=ast.Sub(), right=right))

    def _multiply_nodes(self, left: ast.AST, right: ast.AST, stack_id: int) -> None:
        self.stacks[stack_id].append(ast.BinOp(left=left, op=ast.Mult(), right=right))

    def _divide_nodes(self, left: ast.AST, right: ast.AST, stack_id: int) -> None:
        self.stacks[stack_id].append(ast.BinOp(left=left, op=ast.Div(), right=right))

    def _add_to_vol_tree(self, node: ast.AST) -> None:
        """Add node to the volume tree"""
        if self.vol_tree is None:
            new_node = node
        else:
            new_node = ast.BinOp(left=node, op=ast.Add(), right=self.vol_tree)
        self.vol_tree = new_node

    #######################################################
    # node visitor methods
    #######################################################

    def visit_Module(self, node: ast.AST) -> None:
        """ Visit all the statements in this module node """
        for stmt in node.body:
            self.visit(stmt)

    def visit_Assign(self, node: ast.AST) -> None:
        """Only visit the value part of an assign node"""
        self.visit(node.value)

    def visit_Name(self, node: ast.AST) -> None:
        """
        Check if the name node is of volume type and add it
        to the correct stack.
        Moreover if it is of volume type, add it to the volume tree.
        """
        if self.attribute_mapping[node.id] == VOLUME_IDENTIFIER:
            self._append_vol_stack(node)
            self._add_to_vol_tree(node)
        else:
            self._append_const_stack(node)
        if self.attribute_mapping[node.id] == PRICE_IDENTIFIER:
            self.price_nodes |= {node.id}

    def visit_Constant(self, node: ast.AST) -> None:
        """ Append this constant node to the constants stack """
        self._append_const_stack(node)

    def visit_Num(self, node: ast.AST) -> None:
        """ Append this number node to the constants stack """
        self._append_const_stack(node)

    def visit_BinOp(self, node: ast.AST) -> None:
        """
        Check the operator type and delegate to the correct visitor.
        For now only additions and multiplications are supported.
        Throws: NotImplementedError when called on a node with a different
        operator type than addition or multiplication.
        """
        self.visit(node.left)
        self.visit(node.right)
        if isinstance(node.op, ast.Add):
            self._visit_add()
        elif isinstance(node.op, ast.Mult):
            self._visit_mult()
        elif isinstance(node.op, ast.Sub):
            self._visit_sub()
        elif isinstance(node.op, ast.Div):
            self._visit_div()
        else:
            raise NotImplementedError(f"The operation {str(node.op)}"
                                      "is not implemented yet")

    def _visit_add(self) -> None:
        # retrieve the stack indexes where the next operands are stored
        right_node_locs = self.main_stack.pop()
        left_node_locs = self.main_stack.pop()

        # If both in left and right locs the same stack id is present,
        # Then combine the last two nodes on that stack into one
        locs = right_node_locs + left_node_locs
        if locs.count(0) == 2:
            self._combine_nodes(0)
        if locs.count(1) == 2:
            self._combine_nodes(1)

        # Put the locations that are written to back in the main stack
        main_stack_put = []
        if 0 in locs:
            main_stack_put.append(0)
        if 1 in locs:
            main_stack_put.append(1)
        self.main_stack.append(main_stack_put)

    def _visit_sub(self) -> None:
        right_node_locs = self.main_stack.pop()
        left_node_locs = self.main_stack.pop()

        main_stack_put = []
        if 0 in left_node_locs or 0 in right_node_locs:
            main_stack_put.append(0)
        if 1 in left_node_locs or 1 in right_node_locs:
            main_stack_put.append(1)
        self.main_stack.append(main_stack_put)

        if 0 in left_node_locs and 0 in right_node_locs:
            self._substract_nodes(0)
            right_node_locs.remove(0)

        if 1 in left_node_locs and 1 in right_node_locs:
            self._substract_nodes(1)
            right_node_locs.remove(1)

        for loc in right_node_locs:
            self._negate_node(loc)

    def _visit_mult(self) -> None:
        # retrieve the stack indexes where the next operands are stored
        right_node_locs = self.main_stack.pop()
        left_node_locs = self.main_stack.pop()

        # Retrieve the nodes from their respective stacks
        right_nodes = []
        for right_loc in right_node_locs:
            right_nodes.append(self.stacks[right_loc].pop())

        left_nodes = []
        for left_loc in left_node_locs:
            left_nodes.append(self.stacks[left_loc].pop())

        # Prepare list that will contain the stack ids that where written to
        main_stack_put = []

        # Execute the multiplications
        for i, left_loc in enumerate(left_node_locs):
            for j, right_loc in enumerate(right_node_locs):
                # We don't support the multiplication of two terms that
                # both contain a volume
                if (left_loc | right_loc) == 0:
                    raise NotImplementedError("It looks like you try to multiply "
                                              "two volumes together -_-")
                if (left_loc & right_loc) == 0:
                    self._multiply_nodes(left_nodes[i], right_nodes[j], 0)
                    main_stack_put.append(0)
                else:
                    self._multiply_nodes(left_nodes[i], right_nodes[j], 1)
                    main_stack_put.append(1)
        # Put stack ids back on the main stack
        self.main_stack.append(main_stack_put)

    def _visit_div(self) -> None:
        # retrieve the stack indexes where the next operands are stored
        right_node_locs = self.main_stack.pop()
        left_node_locs = self.main_stack.pop()

        if 0 in right_node_locs:
            raise NotImplementedError("It looks like you are trying to divide "
                                      "by a volume expression -_-")
        slogger.verify(len(right_node_locs) == 1, \
                    "The right node stack indicies are not exactly 1")

        # get nodes from correct stacks
        right_node = self.stacks[right_node_locs[0]].pop()
        left_nodes = []
        for left_loc in left_node_locs:
            left_nodes.append(self.stacks[left_loc].pop())

        # execute division
        main_stack_put = []
        for i, left_loc in enumerate(left_node_locs):
            if left_loc == 0:
                self._divide_nodes(left_nodes[i], right_node, 0)
                main_stack_put.append(0)
            else:
                self._divide_nodes(left_nodes[i], right_node, 1)
                main_stack_put.append(1)
        self.main_stack.append(main_stack_put)

    def generic_visit(self, node: ast.AST) -> None:
        """
        Raise an exception when any other node then
        BinOp, Constant or Name is encountered
        """
        raise NotImplementedError(f"looks like this type of node {node}\
         is not yet fully supported")


class ExpressionHandler:
    """
    This class provides an interface to check an expression for correctness
    and to decompose a price expression into the following components:
     - a normalized price
     - the total volume
     - the total fixed price

    Public methods:
        is_assignment()
        has_valid_attribute_names()
        decompose_price_expression()
        get_total_volume_calculator(total_volume_name)
        get_normalized_price_calculator(normalized_price_name)
        get_fixed_price_calculator(fixed_price_name)
    """

    def __init__(self, table_name: str, expression: str, mapping: Dict[str, str]):
        """Initialize this handler with a table name, expression and mapping"""
        self.mapping = mapping
        self.table_name = table_name
        self._expression = None
        self._expression_code = None
        self.expression = expression
        self.expression_decomposer = None

    @property
    def expression(self) -> str:
        """ Property containing the input expression """
        return self._expression

    @expression.setter
    def expression(self, new_expression: str) -> None:
        self._expression = new_expression
        self._expression_code = ast.parse(new_expression, mode='exec')

    @property
    def assignment_name(self) -> str:
        assignment_checker = AssignmentCheckNodeVisitor()
        assignment_checker.visit(self._expression_code)
        return assignment_checker.assignment_name

    def is_assignment(self) -> bool:
        """Check if the expression is an assignment"""
        assignment_checker = AssignmentCheckNodeVisitor()
        return assignment_checker.visit(self._expression_code)

    def has_valid_attribute_names(self) -> bool:
        """Check if the expression has valid attribute names"""
        attribute_name_checker = AttributeNameCheckVisitor(self.mapping)
        return attribute_name_checker.visit(self._expression_code)

    def decompose_price_expression(self) -> None:
        """Extract the total volume, normalized price and fixed price
        calculations from the expression"""
        if self.expression_decomposer is None:
            expression_decomposer = VolumeAndPriceVisitor(self.mapping)
            expression_decomposer.visit(self._expression_code)
            self.expression_decomposer = expression_decomposer

    def replace_names(self, tree: ast.AST) -> ast.AST:
        """Replace the names to access the given table"""
        name_converter = NameConvertVisitor(self.table_name)
        return ast.fix_missing_locations(name_converter.visit(tree))

    def get_price_list(self) -> List[str]:
        """
        Return a list comprising all the price nodes of the expression
        """
        return self.expression_decomposer.get_price_nodes()

    def get_original_calculator(self) -> types.CodeType:
        """
        Return a code object that computes the original expression

        Returns:
            A compiled code object that can be executed with
            the exec() function
        """
        return compile(self.replace_names(copy.deepcopy(self._expression_code)),
                       '<string>',
                       mode='exec')

    def get_total_volume_calculator(self, total_volume_name: str) -> types.CodeType:
        """
        Return a code object that computes the total volume

        Args:
            total_volume_name: the name to use for calculating
            the total volume

        Returns:
            A compiled code object that can be executed with
            the exec() function
        """
        vol_tree = self.expression_decomposer.get_volume_tree(total_volume_name)

        return compile(self.replace_names(copy.deepcopy(vol_tree)),
                       '<string>',
                       mode='exec')

    def get_normalized_price_calculator(self, normalized_price_name: str) -> types.CodeType:
        """
        Return a code object that computes the normalized price

        Args:
            normalized_price_name: the name to use for calculating
            the normalized price

        Returns:
            A compiled code object that can be executed with
            the exec() function
        """

        norm_price_tree = self.expression_decomposer.get_norm_price_tree(normalized_price_name)
        return compile(self.replace_names(copy.deepcopy(norm_price_tree)),
                       '<string>',
                       mode='exec')

    def get_fixed_price_calculator(self, fixed_price_name: str) -> types.CodeType:
        """
        Return a code object that computes the fixed price

        Args:
            fixed_price_name: the name to use for calculating
            the fixed price

        Returns:
            A compiled code object that can be executed with
            the exec() function
        """
        fix_price_tree = self.expression_decomposer.get_fixed_price_tree(fixed_price_name)
        return compile(self.replace_names(copy.deepcopy(fix_price_tree)),
                       '<string>',
                       mode='exec')
