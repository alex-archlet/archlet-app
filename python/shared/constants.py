"""
Constant definitions
"""

# pylint: skip-file

import copy


class RecordedError(BaseException):
    pass

class Namespace:
    """
    Class to help keep constants organised in namespaces inspired from C++,
    note that namespaces can be nested as the members of __dict__ are not
    restricted to string values.
    """
    def __init__(self, display_name, **kwargs):
        self.__dict__ = kwargs

        # Use copy so we don't add values, keys, error_message and display_name
        self.values   = copy.copy(list(kwargs.values()))
        self.keys     = copy.copy(list(kwargs.keys()))

        self.error_message = '"{{}}" is not a valid {}'.format(display_name)
        self.display_name  = display_name

    # def __getattr__(self, attr):
    #     raise KeyError(self.error_message.format(attr))

###############################################################################
# TableData keys
###############################################################################
KEYS = Namespace(
    display_name  = 'table data key',

    VALUE          = 'value',

    # Demand specification
    
    VOLUME         = 'volume',
    # Offer specification
    
    REGION           = 'location',
    CAPACITY         = 'capacity',
    FILTER           = 'filter',
    PRICE            = 'price',
    CURRENCY         = 'currency',
    NUMBER          = 'number',

    UNKNOWN = 'unknown',

    SPEND   = 'spend',
    PERCENT = 'percent',
    )

UKEYS = Namespace(
    display_name = 'Unique keys',

    RATE_USD_TO_CURR = 'rate_usd_to_curr',
    DUTY_PERCENT     = 'duty_percent',
    DEMAND_ID      = 'demand_id',
    ITEM_ID        = 'item',
    CLIENT_COUNTRY = 'client_country',
    OFFER_ID         = 'offer_id',
    SUPPLIER         = 'supplier',
    SUPPLIER_COUNTRY = 'supplier_country',
    ITEM_CATEGORY    = 'item_type',

    RAW_PRICE         = 'raw_price',
    UNIT_PRICE        = 'unit_price',
    NORMALIZED_PRICE  = 'normalized_price',
    MIN_UNIT_PRICE    = 'min_unit_price',
    TOTAL_PRICE       = 'Total Price',

    DISCOUNTED_BY   = 'discounted_by',
    CONTRIBUTING_TO = 'contributing_to',

    PAYMENT_DURATION = 'payment_duration',

    TOTAL_VOLUME = 'total_volume',
    TOTAL_FIXED_PRICE = 'total_fixed_price',

    IS_HISTORIC = 'is_historic',

    DEMAND_INT_ID = 'demand_int_id',

    # Allocation specification
    ALLOCATION_ID = 'allocation_id',

    TOTAL_SPENT = 'total_spent',
    ALLOCATED_VOLUME = 'allocated_volume',
    TOTAL_DISCOUNT = 'total_discount',

    # Discount specification
    BUCKET_ID       = 'bucket_id',
    DESCRIPTION     = 'description',
    THRESHOLD       = 'threshold',
    DISCOUNT_AMOUNT = 'discount_amount',
    DISCOUNT_TYPE   = 'discount_type',
    DISCOUNT_UNIT   = 'discount_unit',
    VOLUME_NAME     = 'volume_name',
    PRICE_COMPONENT = 'price_component',

    # Data base related
    DB_STATUS = 'data_status',

    # Backwards compatibility
    SUPPLIER_ID = 'supplier',
    PRIOR_SUPPLIER = 'supplier',
    PRIOR_PRICE = 'total_price'
    )

###############################################################################
# Paths
###############################################################################
PATHS = Namespace(
    display_name = 'paths',

    REFERENCE_PATH = 'internal_data/column_types_reference.json',
    ERROR_ENUM_PATH = 'internal_data/error_enum.json')

###############################################################################
# Operation types
###############################################################################
OPERATION_TYPE = Namespace(
    display_name  = 'operation type',

    OPERATION       = 'operation',

    MIN           = 'min',
    MAX           = 'max',
    EQUAL         = 'equal',

    # exclude suppliers operations
    EXCLUDE_ABOVE = 'exclude_above',
    EXCLUDE_BELOW = 'exclude_below',
    EXCLUDE_EXACT = 'exclude_exact',
    KEEP_HIGHEST  = 'keep_highest',
    KEEP_LOWEST   = 'keep_lowest')

###############################################################################
# Operation types
###############################################################################
UNITS = Namespace(
    display_name  = 'unit type',

    UNIT        = 'unit',
    VOLUME      = 'volume',
    SPEND       = 'spend',
    PERCENTAGE  = 'percentage')

###############################################################################
# Status severity levels
###############################################################################
SEVERITY = Namespace(
    display_name  = 'severity level',

    DEBUG   = 'debug',
    INFO    = 'info',
    WARNING = 'warning',
    ERROR   = 'error')

###############################################################################
# Job types
###############################################################################
JOB_TYPE = Namespace(
    display_name  = 'job type',

    FILE_PROCESSING  = 'file_processing',
    CREATE_SCENARIO  = 'create_scenario',
    PROJECT_INFO     = 'project_info',
    OPTIMIZATION     = 'optimization',
    POSTPROCESSING   = 'postprocessing',
    SHADOW_PRICES    = 'shadow_prices',
    COLUMN_MATCHING  = 'column_matching',
    EXPRESSION_EVAL  = 'expression_evaluation',
    EXPRESSION_EXAM  = 'expression_example',
    OFFER_PROCESSING = 'offer_processing')

###############################################################################
# Job statuses
###############################################################################
JOB_STATUS = Namespace(
    display_name  = 'job status',

    PENDING     = 1,
    RUNNING     = 2,
    CANCELED    = 3,
    ABORTED     = 4,
    SUCCEEDED    = 5)

###############################################################################
# Scenario types
###############################################################################
SCENARIO_TYPE = Namespace(
    display_name  = 'scenario type',

    TYPE      = 'scenario_type',
    CUSTOM    = 'custom',
    CHERRY    = 'cherry',
    NO_CHANGE = 'no_change',
    BASELINE  = 'baseline')

###############################################################################
# Constraints specifications
###############################################################################
CONSTRAINT_TYPES = Namespace(
    display_name = 'constraint types',

    GENERAL_SETTINGS    = 'general_settings',
    LIMIT_SPEND         = 'limit_spend',
    MULTI_SPLITS        = 'multi_splits',
    EXCLUDED_SUPPLIERS  = 'excluded_suppliers',
    NUMBER_OF_SUPPLIERS = 'number_of_suppliers',
    VOLUME_PER_SUPPLIER = 'volume_per_supplier'
)

CONSTRAINT_SPECS = Namespace(
    display_name = 'constraint specs',

    FILTERS         = 'filters',

    # general settings specs
    CAPACITIES          = 'capacities',
    GENERAL_INCUMBENCY  = 'general',
    INCUMBENT           = 'incumbent',
    ITEM_INCUMBENCY     = 'item_level',
    ITEM_TYPE           = 'item_type',
    PARTIAL_ALLOCATION  = 'partial_allocation',
    REBATES             = 'rebates',
    SPLIT_ITEMS         = 'split_items',
    VOLUME              = 'volume',
    BASELINE            = 'scenario_id',

    # excluded suppliers specs
    EXCLUDE_ALL     = 'Exclude All',
    RISK_SCORE      = 'RiskScore',

    CRITERIA        = 'criteria')

###############################################################################
# Variable types for the optimization model
###############################################################################
VAR_TYPES = Namespace(
    display_name = 'variable types',

    BINARY = "BINARY",
    INT = "INT",
    CONTINUOUS = "CONTINUOUS")

###############################################################################
# Categories of status messages
###############################################################################

STATUS_MESSAGE_CATEGORY = Namespace(
    display_name = 'status message category',

    FILE     = 'file',
    SCENARIO = 'scenario',
    DEFAULT  = 'default'
)

###############################################################################
# Outlier detection metrics
###############################################################################

OUTLIER = Namespace(
    display_name = 'outlier detection metrics',

    FIRST_QUANTILE = 0.25,
    THIRD_QUANTILE = 0.75,
    SOFT_DEVIATION = 1.5,
    HARD_DEVIATION = 3,
    ALPHA = 0.05
)

DATA = Namespace(
    display_name = 'status of data in database',

    PRESENT = 'present',
    NEW = 'new',
    OLD = 'old'
)
