"""
Module containing Data Class containing all the fields of a discount bucket
"""
from dataclasses import dataclass
from dataclasses import field
from typing import List
from typing import Dict

@dataclass
class DiscountBucket:
    """
    Data Class representing a discount bucket
    """
    _bucket_id: str
    _threshold: float = -1
    _discount_amount: float = 0
    _discount_type: str = 'proportional'
    _unit: str = 'volume'
    _price_component: str = 'Total Price'
    _supplier_id: int = -1
    _contributing_to: List[str] = field(default_factory=list)
    _discounted_by: List[str] = field(default_factory=list)
    _order: int = 0
    _item_discounts: Dict[int, float] = field(default_factory=dict)


    @property
    def bucket_id(self):
        """
        Get the id of this discount bucket
        """
        return self._bucket_id


    @bucket_id.setter
    def bucket_id(self, buck_id: str) -> None:
        """
        Set the id of this discount bucket
        """
        self._bucket_id = buck_id


    @property
    def threshold(self):
        """
        Get the threshold of this discount bucket
        """
        return self._threshold


    @threshold.setter
    def threshold(self, threshold: float) -> None:
        """
        Set the threshold of this discount bucket
        """
        self._threshold = threshold


    @property
    def discount_amount(self):
        """
        Get the discount amount of this discount bucket
        """
        return self._discount_amount


    @discount_amount.setter
    def discount_amount(self, disc_amount: float) -> None:
        """
        Set the discount amount of this discount bucket
        """
        self._discount_amount = disc_amount


    @property
    def discount_type(self) -> str:
        """
        Get the discount type of this discount bucket
        """
        return self._discount_type


    @discount_type.setter
    def discount_type(self, disc_type: str) -> None:
        """
        Set the discount type of this discount bucket
        """
        self._discount_type = disc_type


    @property
    def unit(self):
        """
        Get the unit of this discount bucket
        """
        return self._unit


    @unit.setter
    def unit(self, unit: str) -> None:
        """
        Set the unit of this discount bucket
        """
        self._unit = unit


    @property
    def price_component(self):
        """
        Get the price component name of this discount bucket
        """
        return self._price_component


    @price_component.setter
    def price_component(self, price_comp: str) -> None:
        """
        Set the price component name of this discount bucket
        """
        self._price_component = price_comp


    @property
    def supplier_id(self) -> int:
        """
        Get the supplier id for this discount bucket
        """
        return self._supplier_id


    @supplier_id.setter
    def supplier_id(self, sup_id: int) -> None:
        """
        Set the supplier id for this discount bucket
        """
        self._supplier_id = sup_id


    @property
    def contributing_to(self) -> List[str]:
        """
        Get the list of items contributing to this discount bucket
        """
        return self._contributing_to


    @contributing_to.setter
    def contributing_to(self, contr_to: List[str]) -> None:
        """
        Set the list of items contributing to this discount bucket
        """
        self._contributing_to = contr_to


    @property
    def discounted_by(self) -> List[str]:
        """
        Get the list of items discounted by this discount bucket
        """
        return self._discounted_by


    @discounted_by.setter
    def discounted_by(self, disc_by: List[str]) -> None:
        """
        Set the list of items discounted by this discount bucket
        """
        self._discounted_by = disc_by


    @property
    def order(self) -> int:
        """
        Get the order of this bucket.

        The order of a bucket is its rank in the list of buckets of one supplier.
        """
        return self._order


    @order.setter
    def order(self, order: int) -> None:
        """
        Set the order of this bucket.
        """
        self._order = order


    @property
    def item_discounts(self) -> Dict[int, float]:
        """
        Get the item discounts of this bucket.
        """
        return self._item_discounts


    @item_discounts.setter
    def item_discounts(self, disc_amounts: Dict[int, float]) -> None:
        """
        Set the item discounts of this bucket.
        """
        self._item_discounts = disc_amounts


    def __repr__(self):
        return (f"Discount bucket with:\n"
                f"supplier id: {self.supplier_id}\n"
                f"order: {self.order}\n"
                f"bucket id: {self.bucket_id}\n"
                f"threshold: {self.threshold}\n"
                f"price_component: {self.price_component}\n"
                f"discount_amount: {self.discount_amount}\n"
                f"contributing to: {self.contributing_to}\n"
                f"discounted by: {self.discounted_by}\n"
                f"item discount amounts: {self.item_discounts}")
    
    
