"""
Translator class
"""

import json
import os

from shared.constants import UKEYS, KEYS
from shared.constants import RecordedError
from shared.communication import SingletonLogger

slogger = SingletonLogger()

class Translator:
    """
    Translator class
    """
    def __init__(self, fname, fdir=None):
        """
        Create an object that translates customer-specific information to
        customer-invariant format, i.e. standardize column names.

        Args:
            fname (str): Name of JSON file to initialize translator with.
            fdir (str): Path to JSON file, if not provided assume '.'
        """
        # Get full JSON file path
        fdir = '.' if fdir is None else fdir
        fpath = os.path.join(fdir, fname)

        # Make sure the file exists
        if not os.path.exists(fpath):
            slogger.error('Unable to find file {}'.format(fpath))
            raise RecordedError

        # Intialize from JSON file
        self.initialize_dictionary(fpath)

    def __getitem__(self, cls):
        """
        Get the customer-invariant column name from the customer-specific one.

        Args:
            cls: Name of TableData class for which we would like to get the
                customer-specific to customer-invariant column name mappings.
        """
        if cls not in self.dictionary:
            slogger.debug('Class {} unknown to translator'.format(cls))
            raise ValueError
        return self.dictionary[cls]

    def initialize_dictionary(self, fpath):
        """
        Initialize customer-specific to customer-invariant mappings.

        Args:
            fpath (str): Path to customer-specific JSON file.
        """

        self.dictionary = {}

        # Load from JSON
        try:
            with open(fpath, 'r') as json_file:
                self.dictionary = json.load(json_file)
        except:
            slogger.error(f"Invalid JSON translator file {fpath}")
            raise RecordedError

        # Convert "<VALUE>" to UKEYS.<VALUE>
        for classname, keys in self.dictionary.items():
            # ignore file types
            if classname == 'FileType':
                continue

            # Safe normalization parameters inside translator
            if classname == 'Normalization':
                # get normalization expression
                if 'Expression' in keys:
                    self.expression = keys['Expression']
                else:
                    self.expression = ""

                # get soft constraints
                if 'Soft_Constraints' in keys:
                    self.soft_constraints = keys['Soft_Constraints']
                else:
                    self.soft_constraints = []

                # get custom columns
                if 'Custom_Columns' in keys:
                    self.custom_columns = keys['Custom_Columns']
                else:
                    self.custom_columns = []

                # get second analytics axis
                if 'Second_Axis' in keys:
                    self.second_axis = keys['Second_Axis']
                else:
                    self.second_axis = []

                # get price components
                if 'Price_Components' in keys:
                    self.price_components = keys['Price_Components']
                else:
                    self.price_components = []

                # get filters
                if 'Filters' in keys:
                    self.filters = keys['Filters']
                else:
                    self.filters = []

                # get volumes
                if 'Volumes' in keys:
                    self.volumes = keys['Volumes']
                else:
                    self.volumes = [KEYS.VOLUME]

                # get capacities
                if 'Capacities' in keys:
                    self.capacities = keys['Capacities']
                else:
                    self.capacities = []

                if 'Custom_Kpis' in keys:
                    self.custom_kpis = keys['Custom_Kpis']
                else:
                    self.custom_kpis = {}
                continue

            # # Check we have valid key values as values in the JSON data
            # for val in UKEYS.values():
            #     if val not in UKEYS.UKEYS:
            #         slogger.error('Invalid customer-specific JSON file')
            #         slogger.debug('Invalid UKEYS value {}'.format(val))
            #         raise RecordedError

            self.dictionary[classname] = {}
            for key, val in keys.items():

                # Backwards compatibility
                if classname == "DemandsTable":
                    # if val == 'VOLUME':
                    #     val = 'TOTAL_VOLUME'
                    if val == 'PRICE':
                        val = 'TOTAL_PRICE'
                    elif val == 'PRIOR_PRICE':
                        val = 'TOTAL_PRICE'

                if hasattr(UKEYS, val):
                    self.dictionary[classname][key] = UKEYS.__getattribute__(val)
                elif hasattr(KEYS, val):
                    self.dictionary[classname][key] = KEYS.__getattribute__(val)
                else:
                    self.dictionary[classname][key] = val

            # self.dictionary[classname] = {key: UKEYS.__getattribute__(val) for key, val in UKEYS.items()}
