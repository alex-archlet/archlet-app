"""
TableData class

This class is meant to hold data in a tabular form that is generally imported
from a client's Excel file. The current implementation is essentially a wrapper
for the Pandas DataFrame class with a little added functionality.
"""
from __future__ import annotations

import numpy as np
import os
import pandas as pd
import uuid

from collections.abc import Iterable
from dataclasses import dataclass
from dataclasses import field
from math import isnan
from typing import Dict
from typing import List
from typing import Optional
from typing import TYPE_CHECKING
from typing import Tuple
from typing import Union

from shared.constants import UKEYS, KEYS

from shared.constants import RecordedError
from shared.constants import STATUS_MESSAGE_CATEGORY
from shared.entities import TableHeader
from shared.communication import SingletonLogger

if TYPE_CHECKING:
    from shared.communication import DBFile
    from shared.entities import Translator


slogger = SingletonLogger()

@dataclass
class TableData:
    """
    Data Class representing a table data


    Args:
        file (obj): File object to load the data from, currently only Excel
            format is supported

    Raises:
        RecordedError
    """

    _data: pd.DataFrame = None
    _header: TableHeader = None
    _name: str = ''
    _file = None
    _filename: str = _name
    _filetype: str = _name


    @property
    def data(self):
        """
        Get the DataFrame wrapped by this object
        """
        return self._data


    @data.setter
    def data(self, data_frame: pd.DataFrame) -> None:
        """
        Set the input DataFrame as attribute
        """
        self._data = data_frame

    @property
    def header(self):
        """
        Get the DataFrame wrapped by this object
        """
        return self._header


    @header.setter
    def header(self, header: TableHeader) -> None:
        """
        Set the input DataFrame as attribute
        """
        self._header = header


    @property
    def file(self) -> DBFile:
        """
        Return the attribute file
        """
        return self._file


    @file.setter
    def file(self, file: DBFile) -> None:
        """
        Set the file attribute from the input DBFile
        """
        self._file = file


    @property
    def filename(self) -> str:
        """
        Return the attribute filename
        """
        return self._filename


    @filename.setter
    def filename(self, file: DBFile) -> None:
        """
        Set the filename attribute from the input DBFile
        """
        self._filename = file.name


    @property
    def filetype(self) -> str:
        """
        Return the attribute filetype
        """
        return self._filetype


    @filetype.setter
    def filetype(self, filetype: str) -> str:
        """
        Set the attribute filetype
        """
        self._filetype = filetype


    @property
    def name(self) -> str:
        """
        Return the attribute name
        """
        return self._name


    @name.setter
    def name(self, name: str) -> str:
        """
        Set the attribute name
        """
        self._name = name



    def initialize(self,
                   translator: Translator = None,
                   tablename: str = None,
                   colnames: List[str] = None) -> None:
        """
        This function initializes the data in the table once it has been loaded
        from a file.

        Args:
            translator (Translator): If provided, gives required columns and
                names to rename them to.
            colnames (list): List of column names required, if some of these
                columns are missing we raise an exception.
        """

        #######################################################################
        # Check for missing columns
        #######################################################################

        # Put together set of required columns
        required_columns = set()
        if colnames is not None:
            required_columns |= set(colnames)
        if translator is not None:
            tablename = tablename.capitalize() + 'Table'
            self.filetype = translator['FileType'][tablename]
            required_columns |= set(translator[tablename].keys())

        # Check if we are missing any columns
        missing_columns = required_columns - set(self.data.columns)
        if missing_columns != set():

            for missing_column in missing_columns:
                slogger.error(
                    f"Missing column '{missing_column}' in file {self.filename}",
                    save_message=True,
                    category = STATUS_MESSAGE_CATEGORY.FILE,
                    filename=self.name,
                    filetype=self.filetype,
                    location = None
                )

            raise RecordedError

        #######################################################################
        # Rename columns to client-invariant names
        #######################################################################
        if translator is None:
            return

        self.parse_header(translator, tablename)


    def parse_header(self,
                     translator: Translator,
                     tablename: str) -> None:
        """
        Parse the column header of tablename from the translator.
        """
        # Rename all the column names of the data frame:
        self.add_uuid_names()

        # Translate column names from translator
        self.translate_column_names(translator, tablename)

        if translator.capacities and tablename in ['CapacitiesTable', 'OffersTable']:
            self.header.set_type(translator.capacities, KEYS.CAPACITY)

        if tablename == 'OffersTable':
            self.header.add_column(UKEYS.NORMALIZED_PRICE, UKEYS.NORMALIZED_PRICE, '')

        if translator.price_components and tablename == 'OffersTable':
            if isinstance(translator.price_components[0], dict):
                price_comp = [dic['name'] for dic in translator.price_components if dic['name'] != 'Total Price']
            else:
                price_comp = translator.price_components

            self.header.set_type(price_comp, KEYS.PRICE)

        if (translator.soft_constraints or translator.second_axis) and tablename == 'OffersTable':
            cols = list(set(translator.soft_constraints) | set(translator.second_axis))
            self.header.set_type(cols, KEYS.NUMBER)

        if translator.volumes and tablename in ['OffersTable', 'DemandsTable']:
            self.header.set_type(translator.volumes, KEYS.VOLUME)


    def add_uuid_names(self) -> None:
        """
        Put the uuids as the column names in the data frame
        """
        cols = self.header.columns
        renamer = {col.original_name: col.id for col in cols}
        self.data.rename(columns=renamer, inplace=True)


    def translate_column_names(self,
                               translator: Translator,
                               tablename: str):
        """ Translate the column names according to the translator """
        for from_name, to_name in translator[tablename].items():
            self.header.set_type(from_name, to_name)

    def __str__(self) -> str:
        return self.data.__str__()


    def __getitem__(self, key: str) -> pd.Series:
        return self.data[key]


    def __setitem__(self,
                    key: str,
                    value: Union[list, pd.Series]) -> None:
        self.data[key] = value


    def __delitem__(self, keys: Union[List[str], str]) -> None:
        # UKEYS = to_iterable(UKEYS)
        # self.data = self.data[[key for key in self.keys() if key not in UKEYS]]
        self.data.drop(columns=keys, inplace=True)


    def __copy__(self):
        """
        Shallow copy method
        """
        new_table = type(self)()
        new_table.__dict__.update(self.__dict__)
        return new_table


    def drop_null_rows(self, col: str) -> None:
        """
        Keep only rows where self.data[colname] is not null and not empty
        """
        mask = (self.data[col].notnull()) & (self.data[col] != "")

        # If any rows are dropped, warn user
        if not mask.all():
            slogger.warning('Duplicate rows detected, dropping them')

        self.data = self.data[mask]


    def convert_to_lists(self,
                         col: str,
                         inplace: bool = False) -> pd.Series:
        """
        Some columns are imported as comma-separated lists, however we usually
        want to convert these to arrays. Here we take in a column name and
        convert the cells in this column to arrays when possible.

        Args:
            col (str): Name of column whose cells we wish to convert to lists.
            inplace (bool): If true, operation is done in-place

        Returns:
            Pandas.Series: If inplace is False, return the parsed column.
        """

        def f(cell):
            if isinstance(cell, str) and cell != '':
                return cell.split(',')
            elif isinstance(cell, list):
                return cell

            # TODO: Here we catch None, NaN and others, should check explicitly
            #       for each one instead and return error if unexpected input
            return []

        if not inplace:
            return self.data[col].apply(f)
        else:
            self.data[col] = self.data[col].apply(f)


    def to_string(self,
                  col: str,
                  inplace: bool = True) -> pd.Series:
        """
        Some fields are in the TableData's we wish to print are iterables and
        before we print them to a file we want to convert these fields to comma
        separated formatting. Note that we only convert iterables to comma
        seperated, otherwise we leave the cells 'as-is'.

        Args:
            col (str): Column whose cells we convert to strings.

        Return:
            Pandas.Series: Column where the entries of the original column have
                been converted to human-friendly strings
        """

        def f(cell):
            if isinstance(cell, str):
                return cell
            if isinstance(cell, Iterable):
                return ','.join([str(k) for k in cell])
            if cell is None or isnan(cell):
                return ''
            slogger.debug('Unable to convert {} to string'.format(cell))
            raise ValueError

        if not inplace:
            return self.data[col].apply(f)
        else:
            self.data[col] = self.data[col].apply(f)


    def set_default_column(self, column_name, value=None, type=''):
        """
        Check if column_name exists in this header and add it with the default value if not.
        """
        if column_name not in self.header.types:
            self.header.add_column(column_name, column_name, type) # TODO: change that
            self.data[self.header[column_name]] = value


    def issue_warnings(self,
                       mask: pd.Series,
                       message: str,
                       key: str,
                       drop: bool = False) -> bool:
        """
        check if columns from different tables match otherwise issue a warning
        """
        if any(mask):
            category = STATUS_MESSAGE_CATEGORY.FILE

            def issue_warning(row):
                slogger.warning(message,
                                   category=category,
                                   filename=self.filename,
                                   filetype=self.filetype,
                                   location=row[key])

            self.data[mask].apply(func=issue_warning, axis=1)

            if drop:
                slogger.debug('Dropping rows')
                self.data = self.data[~mask]

        return False


    def issue_errors(self,
                     mask: pd.Series,
                     message: str,
                     key: str,
                     drop: bool = False,
                     raise_error: bool = False) -> bool:
        """
        check if columns from different tables match otherwise issue a warning
        """
        if any(mask):
            category = STATUS_MESSAGE_CATEGORY.FILE

            def issue_error(row):
                if key is None:
                    location = None
                else:
                    location = row[key]

                slogger.error(message,
                                 save_message=True,
                                 category=category,
                                 filename=self.filename,
                                 filetype=self.filetype,
                                 location=location)

            self.data[mask].apply(func=issue_error, axis=1)

            if drop:
                slogger.debug('Dropping rows')
                self.data = self.data[~mask]

            if raise_error:
                raise RecordedError

        return False


    def add_unique_ids(self, key):
        """
        Assign a unique ID using the uuid functionality.

        Raises: KeyError when there is no column present with the name of key
        """
        # Add an ID column if such a columns does not already exists.
        f = lambda x: str(uuid.uuid4())
        self.data[key] = self.data.apply(f, axis=1)


    def get_3d_crosstab(self,
                        column: str,
                        supplier_to_index: Dict[int, str],
                        demands_list: List[str],
                        item_type_to_index: Dict[int, str]) -> np.ndarray:
        """
        Create a 3D crosstab giving the value of the given column for each supplier/item/item type combination
        """
        data = self.data.copy()
        data = data.set_index([self.header[UKEYS.SUPPLIER], self.header[UKEYS.DEMAND_INT_ID], self.header[UKEYS.ITEM_CATEGORY]])
        data = data[[column]]
        tuple_to_criterias = data.to_dict(orient='index')
        crosstab = np.zeros(shape=(len(supplier_to_index), len(demands_list), len(item_type_to_index)))
        for key, dic in tuple_to_criterias.items():
            crosstab[supplier_to_index[key[0]], key[1], item_type_to_index[key[2]]] = dic[column]
        return crosstab


    def get_4d_crosstab(self,
                        columns: List[str],
                        supplier_to_index: Dict[int, str],
                        demands_list: List[str],
                        item_type_to_index: Dict[int, str]) -> np.ndarray:
        """
        Create a 4D crosstab giving the value of each column for each supplier, item and item_type
        """
        data = self.data.copy()
        data = data.set_index([self.header[UKEYS.SUPPLIER], self.header[UKEYS.DEMAND_INT_ID], self.header[UKEYS.ITEM_CATEGORY]])
        data = data[columns]
        tuple_to_criterias = data.to_dict(orient='index')
        crosstab = np.zeros(shape=(len(supplier_to_index), len(demands_list), len(item_type_to_index), len(columns)))
        for key, dic in tuple_to_criterias.items():
            for counter, col_id in enumerate(columns):
                crosstab[supplier_to_index[key[0]], key[1], item_type_to_index[key[2]], counter] = dic[col_id]
        return crosstab


    def trim(self):
        """
        Removes all white spaces and new lines from dataframe

        Args:
            data_frame (DataFrame): df to trim

        Returns:
            DataFrame: trimmed dataframe
        """
        to_replace = [r"\\t|\\n|\\r|\s\s+", r"\t|\n|\r|\s\s+"]
        replaced_by = [" ", " "]

        # trim headers and remove new lines
        self.data.columns = self.data.columns.str.strip()
        self.data.columns = self.data.columns.str.replace(pat=to_replace[0], repl=replaced_by[0], regex=True)

        # trim data and remove new lines
        self.data = self.data.applymap(lambda x: x.strip() if isinstance(x, str) else x)
        self.data = self.data.replace(to_replace=to_replace, value=replaced_by, regex=True, inplace=False)

        return self.data
