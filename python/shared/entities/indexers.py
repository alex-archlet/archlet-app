from __future__ import annotations

from dataclasses import dataclass
from dataclasses import field

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from .new_table_header import TableHeader

class IdIndexer:
    def __init__(self, table_header: TableHeader) -> None:
        """

        """
        self._columns = table_header._columns

    def __getitem__(self, arg: Union[str, List[str]]) -> Union[str, List[str]]:
        if isinstance(arg, list):
            return [self.__getitem__(colname) for colname in arg]
        elif isinstance(arg, str):
            matching_cols = [column for column in self._columns if column.id == arg]
            if len(matching_cols) == 0:
                raise KeyError(f"No match was found in table header for the key {arg}")
            else:
                return matching_cols[0]
        else:
            raise TypeError(f"The table header needs to be accessed with a string or a list of strings. A {type(arg)} was passed")

class NameIndexer:
    def __init__(self, table_header: TableHeader) -> None:
        """

        """
        self._columns = table_header._columns

    def __getitem__(self, arg: Union[str, List[str]]) -> Union[str, List[str]]:
        if isinstance(arg, list):
            return [self.__getitem__(colname) for colname in arg]
        elif isinstance(arg, str):
            matching_cols = [column for column in self._columns if column.original_name == arg]
            if len(matching_cols) == 0:
                raise KeyError(f"No match was found in table header for the key {arg}")
            else:
                return matching_cols[0]
        else:
            raise TypeError(f"The table header needs to be accessed with a string or a list of strings. A {type(arg)} was passed")
