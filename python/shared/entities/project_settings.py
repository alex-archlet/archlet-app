"""
Module containing Data class to store the project settings
"""
from dataclasses import dataclass
from dataclasses import field
from typing import Any
from typing import Dict
from typing import List


@dataclass
class ProjectSettings:
    """
    Data class to store the project settings
    """
    _filters: List[str] = field(default_factory=list)
    _custom_columns: List[str] = field(default_factory=list)
    _soft_constraints: List[str] = field(default_factory=list)
    _custom_kpis: List[str] = field(default_factory=list)
    _price_components_structure: Dict[str, Any] = None
    _default_volume_column: str = ''
    _second_axis: List[str] = field(default_factory=list)


    @property
    def filters(self) -> List[str]:
        """
        Get the filters of these project settings
        """
        return self._filters

    @filters.setter
    def filters(self, filters_list: List[str]) -> None:
        """
        Set the filters of these project settings
        """
        self._filters = filters_list

    @property
    def custom_columns(self):
        """
        Get the custom columns of these project settings
        """
        return self._custom_columns

    @custom_columns.setter
    def custom_columns(self, columns: List[str]) -> None:
        """
        Set the custom columns of these project settings
        """
        self._custom_columns = columns

    @property
    def soft_constraints(self):
        """
        Get the soft constraints of these project settings
        """
        return self._soft_constraints

    @soft_constraints.setter
    def soft_constraints(self, constraints: List[str]) -> None:
        """
        Set the soft constraints of these project settings
        """
        self._soft_constraints = constraints

    @property
    def custom_kpis(self):
        """
        Get the custom kpis of these project settings
        """
        return self._custom_kpis

    @custom_kpis.setter
    def custom_kpis(self, kpis: List[str]) -> None:
        """
        Set the custom kpis of these project settings
        """
        self._custom_kpis = kpis

    @property
    def price_components_structure(self):
        """
        Get the price components structure
        """
        return self._price_components_structure

    @price_components_structure.setter
    def price_components_structure(self, structure: Dict[str, Any]):
        """
        Set the price components structure
        """
        self._price_components_structure = structure

    @property
    def default_volume_column(self) -> None:
        """
        Get the default volume column
        """
        return self._default_volume_column

    @default_volume_column.setter
    def default_volume_column(self, col: str) -> None:
        """
        Set the default volume column
        """
        self._default_volume_column = col

    @property
    def second_axis(self) -> List[str]:
        """
        Get the project settings
        """
        return self._second_axis

    @second_axis.setter
    def second_axis(self, second_axis: List[str]) -> None:
        """
        Set the project settings
        """
        self._second_axis = second_axis

    def to_dict(self):
        """
        Return a dictionary representation of the project settings
        """
        return {"filters": self.filters,
                "custom_columns": self.custom_columns,
                "soft_constraints": self.soft_constraints,
                "custom_kpis": self.custom_kpis,
                "price_components_structure": self.price_components_structure,
                "default_volume_column": self.default_volume_column,
                "second_axis": self.second_axis}
