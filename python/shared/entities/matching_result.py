from dataclasses import dataclass
from dataclasses import field

@dataclass
class MatchingResult:
    """
    Dataclass containing the results of the column matching
    """
    _score: float
    _input_by: str
    _type: str
    _initial_score: float
    _initial_type: str
    _order: int

    @property
    def score(self) -> float:
        return self._score

    @score.setter
    def score(self, score: float) -> None:
        self._score = score

    @property
    def type(self) -> str:
        return self._type

    @type.setter
    def type(self, type_col: str) -> None:
        self._type = type_col

    @property
    def initial_score(self) -> float:
        return self._initial_score

    @initial_score.setter
    def initial_score(self, score: float) -> None:
        self._initial_score = score

    @property
    def initial_type(self) -> str:
        return self._initial_type

    @initial_type.setter
    def initial_type(self, type_col: str) -> None:
        self._initial_type = type_col

    @property
    def order(self) -> int:
        return self._order

    @order.setter
    def order(self, order: int) -> None:
        self._order = order
