from dataclasses import dataclass
from dataclasses import field

from .matching_result import MatchingResult

@dataclass
class Column:
    """
    Dataclass containing all information on a column
    """
    # _internal_name: str
    _type: str
    _id: str
    _original_name: str
    _attribute: bool
    _input_by: str
    _calculation: str
    _matching_result: MatchingResult

    # @property
    # def internal_name(self) -> str:
    #     return self._internal_name

    # @internal_name.setter
    # def internal_name(self, name: str) -> None:
    #     self._internal_name = name

    @property
    def type(self) -> str:
        return self._type

    @type.setter
    def type(self, col_type: str) -> None:
        self._type = col_type

    @property
    def id(self) -> str:
        return self._id

    @id.setter
    def id(self, col_id: str) -> None:
        self._id = col_id

    @property
    def original_name(self) -> str:
        return self._original_name

    @original_name.setter
    def original_name(self, name: str) -> None:
        self._original_name = name

    @property
    def attribute(self) -> bool:
        return self._attribute

    @attribute.setter
    def attribute(self, attribute: bool) -> None:
        self._attribute = attribute

    @property
    def input_by(self) -> str:
        return self._input_by
    
    @input_by.setter
    def input_by(self, input_by: str) -> None:
        self._input_by = input_by

    @property
    def calculation(self) -> str:
        return self._calculation

    @calculation.setter
    def calculation(self, calc: str):
        self._calculation = calc

    @property
    def matching_result(self) -> MatchingResult:
        return self._matching_result

    @matching_result.setter
    def matching_result(self, matching_result: MatchingResult) -> None:
        self._matching_result = matching_result
        self._type = matching_result.type # There, it actually depends on whether it's unique or not. if it is, it's set as internal name
