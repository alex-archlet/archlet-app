from dataclasses import dataclass
from dataclasses import field

import uuid

from typing import List, Union

from .column import Column
from .matching_result import MatchingResult
from .indexers import IdIndexer
from .indexers import NameIndexer
from shared.constants import UKEYS, KEYS
from shared.communication import SingletonLogger
from shared.constants import RecordedError

import copy

slogger = SingletonLogger()

@dataclass
class TableHeader:
    """
    Dataclass containing the table header
    """
    _columns: List[Column] = field(default_factory=list)

    @property
    def columns(self) -> List[Column]:
        return self._columns

    @columns.setter
    def columns(self, columns: List[Column]) -> None:
        self._columns = columns

    @property
    def input_by(self) -> None:
        raise AttributeError("This attribute cannot be accessed")

    @input_by.setter
    def input_by(self, inputter: str) -> None:
        for column in self._columns:
            column.input_by = inputter

    def __getitem__(self, arg: Union[str, List[str]]) -> Union[str, List[str]]:
        if isinstance(arg, list):
            # If multiple UKEYS are passed, we check that they are all unique
            mask_not_unique = [(colname not in UKEYS.values) for colname in arg]
            if any(mask_not_unique):
                invalid_keys=[colname for colname, mask_val in zip(arg, mask_not_unique) if mask_val]
                raise KeyError(f"The table header was accessed with multiple UKEYS, but these are non unique : {invalid_keys}")
            else:
                return [self.__getitem__(colname) for colname in arg]
        elif isinstance(arg, str):
            matching_cols = [column.id for column in self._columns if column.type == arg]
            if arg in UKEYS.values:
                if len(matching_cols) == 0:
                    raise KeyError(f"No match was found in table header for the key {arg}")
                else:
                    return matching_cols[0]
            elif arg in KEYS.values:
                if len(matching_cols) == 0:
                    slogger.debug(f"No match was found in table header for the key {arg}")
                    return []
                else:
                    return matching_cols
            else:
                return matching_cols[0]
        else:
            raise TypeError(f"The table header needs to be accessed with a string or a list of strings. A {type(arg)} was passed")

    def __delitem__(self, arg: Union[str, List[str]]):
        if isinstance(arg, list):
            for val in arg:
                self.__delitem__(val)
        elif isinstance(arg, str):
            for column in self._columns:
                if column.type == arg:
                    self._columns.remove(column)
        else:
            raise TypeError(f"The table header needs to be accessed with a string or a list of strings. A {type(arg)} was passed")

    # def __iter__(self):
    #     for column in self._columns:
    #         yield column.type

    def add_column(self,
                   col_type: str,
                   col_id: str = None,
                   original_name: str = '',
                   attribute: bool = False,
                   input_by: str = 'supplier',
                   calculation: str = None,
                   matching_result: MatchingResult = None):

        """
        Add a new column to the table header
        """
        # This has to be done there instead of default arguments otherwise it takes one random uuid
        # at the beginning of the execution and uses it for every column
        if col_id is None:
            col_id = str(uuid.uuid4())

        self._columns.append(Column(col_type,
                                    col_id,
                                    original_name,
                                    attribute,
                                    input_by,
                                    calculation,
                                    matching_result))

    def set_type(self, original_name: Union[List[str], str], col_type: str) -> None:
        """

        """
        if isinstance(original_name, str):
            original_name = [original_name]

        for column in self._columns:
            if column.original_name in original_name:
                column.type = col_type

    @property
    def byid(self) -> IdIndexer:
        """

        """
        return IdIndexer(self)

    @property
    def byname(self) -> NameIndexer:
        """

        """
        return NameIndexer(self)

    @property
    def ids(self) -> List[str]:
        """

        """
        return [column.id for column in self._columns]

    @property
    def names(self) -> List[str]:
        """

        """
        return [column.original_name for column in self._columns]

    @property
    def types(self) -> List[str]:
        """

        """
        return [column.type for column in self._columns]

    def copy(self):
        """

        """
        return copy.deepcopy(self)
