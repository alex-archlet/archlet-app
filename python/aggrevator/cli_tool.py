"""
Simple command line tool to run the aggregator locally
"""

import argparse
import json

from src.v1.routes.project_info_service import handle_project_info
from src.v1.routes.pivot_table_service import handle_pivot_table

if __name__ == "__main__":

    # Parse arguments
    parser = argparse.ArgumentParser(description='CLI for Archlet App aggreveator')

    parser.add_argument('project_id', help="PK of the project entry in the" \
        " PostgreSQL database that will be execute")

    parser.add_argument('endpoint', help="Endpoint that should be executed: pi (project info) or pp (price pivot)")

    args = parser.parse_args()

    # Call endpoint
    if args.endpoint == 'pp':
        # data can be taken from pivot_table backend call
        data = {"group_by": ["0cd9300e-226b-4003-b251-6c3f3d3a947f"],
                "result_columns": ["44dba352-523b-479e-8fd9-2c9ea119ec6a",
                                   "ed099acc-4cef-4377-9189-66590688b144",
                                   "414aa59d-e2d0-416e-8ce0-9f4c1cd9ba40",
                                   "74c0a55e-3140-4def-a42c-cbbc9b1e4859"]}
        group_by = data['group_by']
        result_columns = data['result_columns']
        handle_pivot_table(args.project_id, group_by, result_columns)
    else:
        result = handle_project_info(args.project_id)

        with open('pi.json', 'w') as fp:
            json.dump(result, fp)
