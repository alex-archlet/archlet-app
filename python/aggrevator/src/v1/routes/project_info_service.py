"""
Module with the functionality to produce the project info output
"""
import os
import socket

from datetime import datetime

from shared.communication import ExecuteTask
from shared.communication import IOFacade
from shared.communication import SingletonLogger
from shared.entities import TableData
from shared.utilities.db_table_processor import add_item_ids
from shared.utilities.project_analyzer import ProjectAnalyzer

# Logs path
top_dir = os.environ.get("LOGS_PATH")

slogger = SingletonLogger()

def handle_project_info(project_id):
    """
    Calculate the project info output for the given project_id.

    Args:
        project_id: uuid of project
    """

    ###########################################################################
    # Set logging output options
    ###########################################################################
    # Logging file name
    timestamp = datetime.utcnow().strftime('%Y.%m.%d_%H:%M:%S.%f')
    datestamp = datetime.utcnow().strftime('%Y_%m_%d')

    log_fname = f'{timestamp}_{socket.gethostname()}_{project_id}_project_info.log'

    # Create logging folder
    log_dir = f'{top_dir}/{datestamp}'

    # Initialize logging output
    slogger.clear()
    slogger.set_logger_output(logfile=log_fname, logdir=log_dir)

    ###########################################################################
    # Create IOFacade
    ###########################################################################

    io_facade = IOFacade(-1, '')
    io_facade.initialize_db_connection()
    io_facade.project_id = project_id
    io_facade.branch_id = io_facade.get_project_branch().id
    round_id = io_facade.fetch_round_id(status_id=4)

    ###########################################################################
    # Handle Project Info
    ###########################################################################
    analyzer = ProjectAnalyzer()

    if round_id is False:
        # When no round id present, return an empty project info
        slogger.info('No data found for this project, returning empty result')
        project_info = analyzer.empty_project_info()
        heatmap = []
        io_facade.clean()
        return {'project_info': project_info, 'price_heatmap': heatmap}

    result = None

    with ExecuteTask(f"Running project info service for project {project_id}", io_facade, reraise=False):
        with ExecuteTask("Loading tables", io_facade):
            offers_table = TableData()
            offers_table = io_facade.load_offers_db(offers_table, round_id)

            baseline_table = TableData()
            baseline_table = io_facade.load_baseline_offers_db(baseline_table, round_id)

            demands_table = TableData()
            demands_table = io_facade.load_demands_db(demands_table, round_id)

        with ExecuteTask("Loading project settings", io_facade):
            project_settings = io_facade.fetch_project_settings()

        with ExecuteTask("Preprocessing offers", io_facade):
            # preprocess offers and baseline offers if there are any
            offers_table = add_item_ids(offers_table,
                                        demands_table)
            if baseline_table is not None:
                baseline_table = add_item_ids(baseline_table,
                                              demands_table)

        with ExecuteTask("Calculating price heatmap and project info", io_facade):
            project_info = analyzer.project_info(offers_table,
                                                 baseline_table,
                                                 demands_table,
                                                 project_settings)

            heatmap = analyzer.price_heatmap(offers_table,
                                             baseline_table,
                                             demands_table,
                                             project_settings)

        with ExecuteTask("Creating output", io_facade):
            # construct output
            output = {
                'project_info': project_info,
                'price_heatmap': heatmap
            }
            result = output

    if result is None:
        project_info = analyzer.empty_project_info()
        heatmap = []
        result = {'project_info': project_info, 'price_heatmap': heatmap}

    io_facade.clean()
    return result
