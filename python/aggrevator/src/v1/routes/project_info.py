"""
Module for the project info route
"""
from flask import Blueprint

from .project_info_service import handle_project_info
from shared.utilities.uuid import is_valid_uuid

project_info_api = Blueprint('project_info_api', __name__)

@project_info_api.route("/<project_id>")
def project_info_handler(project_id):
    """
    Handle a project info request
    """
    if not is_valid_uuid(project_id):
        raise Exception("invalid UUID provided in url")

    result = handle_project_info(project_id)

    return result
