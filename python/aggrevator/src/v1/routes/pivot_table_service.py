"""
Module with the functionality to produce a pivot table
"""
import numpy as np
import os
import pandas as pd
import simplejson as json
import socket

from datetime import datetime
from typing import List

from shared.communication import ExecuteTask
from shared.communication import IOFacade
from shared.communication import SingletonLogger
from shared.constants import UKEYS, KEYS, RecordedError
from shared.entities import TableData
from shared.utilities.db_table_processor import clean_all_numeric_cols

# Logs path
top_dir = os.environ.get("LOGS_PATH")

slogger = SingletonLogger()

def calculate_pivot_table(offers_table: TableData,
                          demands_table: TableData,
                          group_by: List[str],
                          result_columns: List[str]):
    """
    Calculate a pivot table for the given data and columns.

    Args:

    """
    offers_df = offers_table.data.set_index(offers_table.header[UKEYS.DEMAND_ID])
    demands_df = demands_table.data.set_index(demands_table.header[UKEYS.DEMAND_ID])

    table = offers_df.join(demands_df)
    wavg_table = table.copy()

    # Get first volume (default volume) from the offers to calculate weighed average prices based on offered volume
    # If there are no offered volumes, demanded volumes are used
    if KEYS.VOLUME in offers_table.header.types:
        default_volume = offers_table.header[KEYS.VOLUME][0]
    elif UKEYS.TOTAL_VOLUME in offers_table.header.types:
        default_volume = offers_table.header[UKEYS.TOTAL_VOLUME]

    # Select correct columns:
    all_cols = list(set(group_by) | set(result_columns))
    wavg_cols = list(set(group_by) | set(result_columns) | set([default_volume]))

    # Calculate pivot table
    table = table[all_cols].fillna(0)
    grouped = pd.pivot_table(table, values=result_columns, index=group_by, aggfunc=[min, max, np.mean, sum])

    # Calculate weighted average, taking care of potential divided by zeros
    wavg_table = wavg_table[wavg_cols].fillna(0)
    zero_series = pd.Series([0 for _ in result_columns], result_columns)
    wavg_table = wavg_table.groupby(group_by).apply(lambda x: zero_series if np.sum(x[default_volume]) == 0 else pd.Series(np.average(x[result_columns], weights=x[default_volume], axis=0), result_columns))
    wavg_table.columns = pd.MultiIndex.from_product([['wavg'], wavg_table.columns])

    # Combine output
    grouped = pd.concat([grouped, wavg_table], axis=1)
    grouped.reset_index(inplace=True)

    return grouped


def handle_pivot_table(project_id: str, group_by: List[str], result_columns: List[str]):
    """
    Return a pivot table for the given project_id.

    Args:
        project_id: id for which the pivot table should be calculated
        group_by: the columns that are used to group by
        result_columns: the columns that should appear in the output
    """

    ###########################################################################
    # Set logging output options
    ###########################################################################
    # Logging file name
    timestamp = datetime.utcnow().strftime('%Y.%m.%d_%H:%M:%S.%f')
    datestamp = datetime.utcnow().strftime('%Y_%m_%d')

    log_fname = f'{timestamp}_{socket.gethostname()}_{project_id}_pivot_table.log'

    # Create logging folder
    log_dir = f'{top_dir}/{datestamp}'

    # Initialize logging output
    slogger.clear()
    slogger.set_logger_output(logfile=log_fname, logdir=log_dir)

    ###########################################################################
    # Create IOFacade
    ###########################################################################

    io_facade = IOFacade(-1, '')
    io_facade.initialize_db_connection()
    io_facade.project_id = project_id
    io_facade.branch_id = io_facade.get_project_branch().id
    round_id = io_facade.fetch_round_id(status_id=4)
    result = None

    if not round_id:
        slogger.warning("No data found for this project")
        return json.dumps([])

    with ExecuteTask(f"Calculating pivot table for project {project_id}", io_facade, reraise=False):
        with ExecuteTask("Loading tables", io_facade):
            offers_table = TableData()
            offers_table = io_facade.load_offers_db(offers_table, round_id)

            demands_table = TableData()
            demands_table = io_facade.load_demands_db(demands_table, round_id)

            # clean offers and demands
            offers_table = clean_all_numeric_cols(offers_table)
            demands_table = clean_all_numeric_cols(demands_table)

        with ExecuteTask("Calculating pivot table", io_facade):
            grouped = calculate_pivot_table(offers_table, demands_table, group_by, result_columns)

        with ExecuteTask("Returning result", io_facade):
            # Create output dictionaries
            grouped['dictionary'] = grouped.apply(lambda row: {**{index_col: row[index_col][0] for index_col in group_by}, **{comp: {'min': row[('min', comp)], 'max': row[('max', comp)], 'mean': row[('mean', comp)], 'sum': row[('sum', comp)], 'wavg': row[('wavg', comp)]} for comp in result_columns}}, axis=1)
            result = json.dumps(grouped['dictionary'].tolist())

    if result is None:
        result = json.dumps([])

    io_facade.clean()
    return result
