"""
Module for the pivot table route
"""
from flask import Blueprint
from flask import request

from .pivot_table_service import handle_pivot_table
from shared.utilities.uuid import is_valid_uuid

pivot_table_api = Blueprint('pivot_table_api', __name__)


# @pivot_table_api.route("/<project_id>")
@pivot_table_api.route("/<project_id>", methods=['POST'])
def pivot_table_handler(project_id):
    """
    Handle a pivot table request
    """
    if not is_valid_uuid(project_id):
        raise Exception("invalid UUID provided in url")
    
    # extract params from request
    request_json = request.get_json()
    group_by = request_json.get('group_by')
    result_columns = request_json.get('result_columns')

    # get pivot table
    result = handle_pivot_table(project_id, group_by, result_columns)

    return result
