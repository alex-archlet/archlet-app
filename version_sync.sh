#!/bin/bash
 
declare -a StringArray=("web-app/backend/package.json" "web-app/frontend/package.json" "web-email/package.json" "python/shared/release.json")
 
export VERSION_NUMBER=$(node -p "require('./package.json').version")

# Iterate the string array using for loop
for val in ${StringArray[@]}; do
  npx json -I -f $val -e "this.version=\"${VERSION_NUMBER}\""
done

