# Archlet App

![Coverage](https://bitbucket.org/archletapp/archlet-app/downloads/coverage.svg)

## 1.Setup a virtual environment
This describes the simplest way to setup your virtual environment on purpose.
Feel free to check out pipenv & virtualenv alternatives for easier use.

### 1.1 Why do we have to do this exactly?
This is the first step to prevent "it works on my computer" type of problems.
It guarantees everyone using the repository are using the same version of each
package, and dependencies of different repositories are isolated.

### 1.2 Quickstart
```
# cd to archlet-o
cd path/to/archlet-o

# create a virtual environment
python3 -m venv venv # this will be ignored by git due to .gitignore

# install contents of requirements file to this virtual environment
./venv/bin/pip3 install -r ./engine/requirements.txt

# Now either make sure Pycharm uses this virtual environment
# Or if you prefer commandline, instead of python3 use
./venv/bin/python3 main.py

# To install stuff on virtual environment
./venv/bin/pip3 package_name

# To update requirements file so others will also know your package version
./venv/bin/pip3 freeze > requirements.txt
```

## 2. Setup Visual Studio Code
_explain why & how sometime_
